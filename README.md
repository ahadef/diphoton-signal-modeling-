# 0. Download and compile the package
$git clone ssh://git@gitlab.cern.ch:7999/ahadef/diphoton-signal-modeling-.git

$ cd HGGFitter/Fitters/

$ source ./setup.sh

$ rc compile

# 1. Setup 
$ cd HGGFitter/Fitters/

$ source setup.sh 

# 2. Create suitable files
$ cd HGGFitter/HGGfitter/Tools/CreateSamples/Inclusive/

$ source loop.sh

N.B: depending on your sample, you should update the code to extract the mass value from the * * sample file using  out_name_2

N.B: The current signal selection corresponding to high mass range from 200 GeV to 5 TeV 

output files (including photon conversion categorisation): ../../SignalParametrizationRun2/dummyMCinputSampleFolder/MC2019/HGGfitter_format_input/NWA


# 3. Run single fit
$ cd HGGFitter/run/

$ source single_fit.sh


N.B:  I am using different datacards for every mass point in order to assign different set of initial values for every mass point to get better fit.

N.B: in case you are using option "9999" to run for all mass points, do not forget to update the file "HGGFitter/Tools/SignalParametrizationRun2/MC2019/MC16.txt" with the list of your mass points.

output: ../datacards/LowHighMassRun2/SP/MC2019/ (plots, output_inclusive)



# 4. Plot every DSCB parameter vs mX (signal paremertisation )
$cd HGGfitter/Tools/SignalParametrizationRun2/MC2019/

$root -l -b -q 'plotSignalParametrizationMC15.C(0,1,"inclusive")' >log_validation_inclusive.txt

options: "inclusive", "2conv", "2unconv", "1conv"

# 4. signal parametrisation
Get the signal parametrisation from the fit results printed in the file "log_validation_inclusive.txt" (I am using 1st order poly function for all parameters).

# 5. Validation: draw global fit using signal parameterisation
$cd HGGFitter/run/

update "multiple_input_MG_test_HM.dat" with the output from the fit (see step 4).

$source global_fit.sh

results in run/globalfit/

# 6. Systematics
# 6.1 Experimental  Systematics: 
$cd HGGFitter/Systematics/Experimental/

$root -l -b -q test_sys_resolution.C
# 6.2.  Closure and production modes Systematics: Injection Tests
First step: Calculate S/B ratio manually, the number of signal is estimated by assuming a fiducial cross-section of twice the expected limit for each mass point (S = 2 . (σfid . Br) . L . Cx). The number of background for each mass point is caluclate using a background template_yyOnly_fixStitch as following:

$cd HGGFitter/Systematics/InjectionTest/compute_N_B

$source setup.sh

$root -l -b -q 'compute_N_background(200)'

run this script for each mass point.

Second step: Update the vectors 'mass', 'S' and 'B' at: "HGGfitter/Systematics/InjectionTest/BuildToyData/SignalInjectionTest.C" by the numbers calculated in the first step. Then run:

$root -l -b -q '../Systematics/InjectionTest/BuildToyData/SignalInjectionTest.C("MG")'

$root -l -b -q '../Systematics/InjectionTest/BuildToyData/SignalInjectionTest.C("ttH")'



