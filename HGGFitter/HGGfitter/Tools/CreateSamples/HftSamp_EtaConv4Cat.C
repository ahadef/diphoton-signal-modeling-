/*=============================================================*/
/* Convert MxAOD spin0 sample mc15b to HFitter suitable format */
/* for Higgs eta and conv categories:                          */
/*        : BBUU = barel barel unconv unconv                   */
/*        : NBUU = at least one non-barel, UU                  */
/*        : BBCR = barel barel, at least one converted         */
/*        : NBCR = at least one non-barel, at least one conv   */
/* Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>     */
/*=============================================================*/
void HftSamp_EtaConv4Cat(const char* filename){
 
  char file_name[1000], output_name[1000], output_name_BBUU[1000], output_name_NBUU[1000], output_name_BBCR[1000], output_name_NBCR[1000];
  sprintf(file_name,"/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC160030/Signal/%s",filename);
  stringstream ss;
  string out_name;
  string out_name_1;
  string out_name_fin;
  ss<< filename;
  ss>>out_name;

  //now PowhegPy8_ggH750_W6p.MxAOD.p2470.h011.root
  std::size_t found = out_name.find_first_of("_\\"); 
  out_name_1=out_name.substr(found+1);
  //std::size_t found2 = out_name_1.find_first_of("_m\\");
  std::size_t found2 = out_name_1.find_first_of(".M\\");
  out_name_fin=out_name_1.substr(0,found2);
  out_name_fin="mc15_13TeV_"+out_name_fin;
  const char * out_name_to_use = out_name_fin.c_str();
  cout<< out_name_to_use << endl;


  TFile * file_data = TFile::Open(file_name);
  TTree *tree= (TTree*)file_data->Get("CollectionTree");

  tree->SetBranchStatus("*", 0);
  tree->SetBranchStatus("HGamEventInfoAuxDyn*", 1);
  tree->SetBranchStatus("HGamPhotonsAuxDyn*", 1);
  Float_t mass, event_weight;
  Char_t accepted, Dalitz;
  
  std::vector<int> * convStat =0;
  std::vector<float> * etaphot =0;
  Double_t eta_cut=0.6;

  /////////// NOMINAL ///////////////
  //* //HGamEventInfo
  tree->SetBranchStatus("HGamEventInfoAuxDyn*", 1);
  tree->SetBranchAddress("HGamPhotonsAuxDyn.conversionType",&convStat);
  tree->SetBranchAddress("HGamPhotonsAuxDyn.eta_s2",&etaphot);
  tree->SetBranchAddress("HGamEventInfoAuxDyn.m_yy",&mass);
  tree->SetBranchAddress("HGamEventInfoAuxDyn.isPassedLowHighMyy",&accepted);
  tree->SetBranchAddress("HGamEventInfoAuxDyn.isDalitz",&Dalitz);
  tree->SetBranchAddress("HGamEventInfoAuxDyn.weight",&event_weight);
  sprintf(output_name,"/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC160050/HGGfitter_format_input/%s.root",out_name_to_use);
  sprintf(output_name_BBUU,"/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC160050/HGGfitter_format_input/%sBBUU.root",out_name_to_use);
  sprintf(output_name_NBUU,"/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC160050/HGGfitter_format_input/%sNBUU.root",out_name_to_use);
  sprintf(output_name_BBCR,"/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC160050/HGGfitter_format_input/%sBBCR.root",out_name_to_use);
  sprintf(output_name_NBCR,"/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC160050/HGGfitter_format_input/%sNBCR.root",out_name_to_use);
  //*

  Float_t mgg, weight, weightN, eta1=0, eta2=0; 
  Double_t random;
  Long_t entries;
  Float_t Nevt=0, Norm=0, Norm_sum=0;
  Float_t Nevt_BBUU=0, Norm_BBUU=0, Norm_sum_BBUU=0, Nevt_NBUU=0, Norm_NBUU=0, Norm_sum_NBUU=0,Nevt_BBCR=0, Norm_BBCR=0, Norm_sum_BBCR=0, Nevt_NBCR=0, Norm_NBCR=0, Norm_sum_NBCR=0;
  Int_t ev;
  int conv1=0, conv2=0; 

  entries= tree->GetEntries();
  cout<< "Nentries in MxAOD: " << entries << endl;
  for (Int_t i=0; i<entries; i++) {
    tree->GetEntry(i);  

    conv1=convStat->at(0);    conv2=convStat->at(1);
    eta1=etaphot->at(0);    eta2=etaphot->at(1);
    if((int)accepted==1 && (int)Dalitz==0){
      //BB
      if(TMath::Abs( eta1)<eta_cut &&  TMath::Abs( eta2)<eta_cut){       
	//BBUU
	if(conv1==0 &&   conv2==0){       
	  Norm_sum_BBUU+=event_weight;
	  Nevt_BBUU++;
	}
	//BBCR
	if((  conv1!=0 ) || ( conv2!=0) ){       //
	  Norm_sum_BBCR+=event_weight;
	  Nevt_BBCR++;
	}
      }//end of BB 

      //NB
      if( TMath::Abs( eta1)>=eta_cut || TMath::Abs( eta2)>=eta_cut ){
	//NBUU
	if(conv1==0 &&   conv2==0){       
	  Norm_sum_NBUU+=event_weight;
	  Nevt_NBUU++;
	}
	//NBCR
	if((  conv1!=0 ) || ( conv2!=0) ){       //
	  Norm_sum_NBCR+=event_weight;
	  Nevt_NBCR++;
	}
      }//end of NB 
      
      Norm_sum+=event_weight;
      Nevt++;
    }//end of accepted
  }//end of loop
  
  Norm=Nevt/Norm_sum;
  Norm_BBUU=Nevt_BBUU/Norm_sum_BBUU;
  Norm_NBUU=Nevt_NBUU/Norm_sum_NBUU;
  Norm_BBCR=Nevt_BBCR/Norm_sum_BBCR;
  Norm_NBCR=Nevt_NBCR/Norm_sum_NBCR;
  cout<< "Nevent total: "<<Nevt <<" for BBUU: "<< Nevt_BBUU <<";  for NBUU: " << Nevt_NBUU  <<" for BBCR: "<< Nevt_BBCR <<";  for NBCR: " << Nevt_NBCR << endl;
  cout<< eta_cut <<" fSig_BBUU = " << Nevt_BBUU/Nevt << " fSig_NBUU = " << Nevt_NBUU/Nevt <<" fSig_BBCR = " << Nevt_BBCR/Nevt << " fSig_NBCR = " << Nevt_NBCR/Nevt << endl;

  //*
  TFile f_cBBUU(output_name_BBUU,"recreate");
  TTree cBBUU("tree","HGGfitter format tree");
  cBBUU.Branch("mgg",&mgg,"mgg/F");
  cBBUU.Branch("weight",&weight,"weight/F");
  cBBUU.Branch("weightN",&weightN,"weightN/F");

  for (Int_t i=0; i<entries; i++) {
    tree->GetEntry(i);
    conv1=convStat->at(0);    conv2=convStat->at(1);
    eta1=etaphot->at(0);    eta2=etaphot->at(1);
    if((int)accepted==1 && (int)Dalitz==0){
      if(TMath::Abs( eta1)<eta_cut &&  TMath::Abs( eta2)<eta_cut){       //cout << "BB: - eta1: "<< eta1 << ", eta2: "<< eta2<< endl;
	//BBUU
	if(conv1==0 &&   conv2==0){       
	  mgg=mass/1000; 	weight=event_weight;
	  weightN=event_weight*Norm_BBUU;
	  cBBUU.Fill();
	}
      }
    }
  }
  cBBUU.Write();

  mgg=0; weight=0; weightN=0; eta1=0; eta2=0;
  TFile f_cBBCR(output_name_BBCR,"recreate");
  TTree cBBCR("tree","HGGfitter format tree");
  cBBCR.Branch("mgg",&mgg,"mgg/F");
  cBBCR.Branch("weight",&weight,"weight/F");
  cBBCR.Branch("weightN",&weightN,"weightN/F");

  for (Int_t i=0; i<entries; i++) {
    tree->GetEntry(i);
    conv1=convStat->at(0);    conv2=convStat->at(1);
    eta1=etaphot->at(0);    eta2=etaphot->at(1);
    if((int)accepted==1 && (int)Dalitz==0){
      if(TMath::Abs( eta1)<eta_cut &&  TMath::Abs( eta2)<eta_cut){       //cout << "BB: - eta1: "<< eta1 << ", eta2: "<< eta2<< endl;
	//BBCR
	if((  conv1!=0 ) || ( conv2!=0) ){       //
	  mgg=mass/1000; 	weight=event_weight;
	  weightN=event_weight*Norm_BBCR;
	  cBBCR.Fill();
	}
      }
    }
  }
  cBBCR.Write();


  mgg=0; weight=0; weightN=0; eta1=0; eta2=0;
  TFile f_cNBUU(output_name_NBUU,"recreate");
  TTree cNBUU("tree","HGGfitter format tree");
  cNBUU.Branch("mgg",&mgg,"mgg/F");
  cNBUU.Branch("weight",&weight,"weight/F");
  cNBUU.Branch("weightN",&weightN,"weightN/F");
  //entries= tree->GetEntries();
  for (Int_t i=0; i<entries; i++) {
    tree->GetEntry(i);
    conv1=convStat->at(0);    conv2=convStat->at(1);
    eta1=etaphot->at(0);    eta2=etaphot->at(1);
    if((int)accepted==1 && (int)Dalitz==0){
      if( TMath::Abs( eta1)>=eta_cut || TMath::Abs( eta2)>=eta_cut ){
	//NBUU
	if(conv1==0 &&   conv2==0){       
	  mgg=mass/1000; 	weight=event_weight;
	  weightN=event_weight*Norm_NBUU;
	  cNBUU.Fill();
	}
      }
    }
  }
  cNBUU.Write();

  mgg=0; weight=0; weightN=0; eta1=0; eta2=0;
  TFile f_cNBCR(output_name_NBCR,"recreate");
  TTree cNBCR("tree","HGGfitter format tree");
  cNBCR.Branch("mgg",&mgg,"mgg/F");
  cNBCR.Branch("weight",&weight,"weight/F");
  cNBCR.Branch("weightN",&weightN,"weightN/F");
  //entries= tree->GetEntries();
  for (Int_t i=0; i<entries; i++) {
    tree->GetEntry(i);
    conv1=convStat->at(0);    conv2=convStat->at(1);
    eta1=etaphot->at(0);    eta2=etaphot->at(1);
    if((int)accepted==1 && (int)Dalitz==0){
      if( TMath::Abs( eta1)>=eta_cut || TMath::Abs( eta2)>=eta_cut ){
	//NBCR
	if((  conv1!=0 ) || ( conv2!=0) ){       
	  mgg=mass/1000; 	weight=event_weight;
	  weightN=event_weight*Norm_NBCR;
	  cNBCR.Fill();
	}
      }
    }
  }
  cNBCR.Write();

  //*/
}
