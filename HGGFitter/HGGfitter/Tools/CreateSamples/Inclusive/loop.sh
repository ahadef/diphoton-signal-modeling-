#!/bin/bash

#### Narrow Width files
#FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16a/Nominal/mc16a.aMCnloPy8_X0toyy_*_NW.*
FILES=/eos/atlas/user/i/inomidis/HGam/MxAOD_h024/merged/Nominal/*_NW.* 

###
###systematic mc 16a NW
#FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16a/PhotonSys/
#FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16a/PhotonSys/mc16a.aMCnloPy8_X0toyy_5000_NW.MxAODPhotonSys.e6917_s3126_r9364_p3705.h024.root 
###
#### LargeWidth files
#FILES=/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024/mc16a/Nominal/mc16a.aMCnloPy8_X0toyy_*_W10p.*

for f in $FILES
do 
     name="${f}"
	    echo $name
	    root -q -l 'HftSamp_MGScalar.C("'$name'","mc16")'    # options: mc16, mc16a, mc16d
done

