#!/bin/bash
# grab formulas from Multiple Fit
# Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>  

#=========================================================================#
#                             Description                                 #
# f=a*x + b
# 1) read output datacard to find values of function constants: 
#                                                            a,b
# 2) read input datacard, to get formulas which used for parametrization, save them to Formulas_$prod.txt to fit single points in "plotSignalParametrization.C":
#                                                            f_i=a_i*x+ b_i 
# 3) exchange constants in formulas with fitted values - MultipleFitFormulas_$prod.txt
#=========================================================================#

### Note: 
#Arguments are accessed inside a script using the variables $1, $2, $3, etc.,
# where $1 refers to the first argument, $2 to the second argument, and so on.
#========================================================================#
 
prod="$1" 
#prod="MG"
CAT="$2"

cd ../../../datacards/LowHighMassRun2/SP/MC2019/output_$CAT
rm MultipleFitFormulas_$prod.txt

FILES=multiple_*$prod*.dat
for f in $FILES 
#for f in $(ls -1v *$prod* )
do
    echo "Processing $f file..."

    # grab mCBpar0 value with eror
    mCBpar0=`grep -r 'mCBpar0' $f`
    if [[ $mCBpar0 == *"mCBpar0 ="* ]]
    then
	mCBpar0_val_raw=${mCBpar0#*'mCBpar0 = '*}
	mCBpar0_val=${mCBpar0_val_raw%%' +/-'*}
	mCBpar0_err_raw=${mCBpar0#*'+/- '*}
	mCBpar0_err=${mCBpar0_err_raw%%' L('*}
    fi

    # grab mCBpar1 value with eror
    mCBpar1=`grep -r 'mCBpar1' $f`
    if [[ $mCBpar1 == *"mCBpar1 ="* ]]
    then
	mCBpar1_val_raw=${mCBpar1#*'mCBpar1 = '*}
	mCBpar1_val=${mCBpar1_val_raw%%' +/-'*}
	mCBpar1_err_raw=${mCBpar1#*'+/- '*}
	mCBpar1_err=${mCBpar1_err_raw%%' L('*}
    fi


    # grab mCBpar2 value with eror
    mCBpar2=`grep -r 'mCBpar2' $f`
    if [[ $mCBpar2 == *"mCBpar2 ="* ]]
    then
	mCBpar2_val_raw=${mCBpar2#*'mCBpar2 = '*}
	mCBpar2_val=${mCBpar2_val_raw%%' +/-'*}
	mCBpar2_err_raw=${mCBpar2#*'+/- '*}
	mCBpar2_err=${mCBpar2_err_raw%%' L('*}
    fi
    if [[ $mCBpar2 == *""* ]]
    then
	mCBpar2_val="0"
	mCBpar2_err="0"
    fi


    # grab sCBpar0 value with eror
    sCBpar0=`grep -r 'sCBpar0' $f`
    if [[ $sCBpar0 == *"sCBpar0 ="* ]]
    then
	sCBpar0_val_raw=${sCBpar0#*'sCBpar0 = '*}
	sCBpar0_val=${sCBpar0_val_raw%%' +/-'*}
	sCBpar0_err_raw=${sCBpar0#*'+/- '*}
	sCBpar0_err=${sCBpar0_err_raw%%' L('*}
    fi

    # grab sCBpar1 value with eror
    sCBpar1=`grep -r 'sCBpar1' $f`
    if [[ $sCBpar1 == *"sCBpar1 ="* ]]
    then
	sCBpar1_val_raw=${sCBpar1#*'sCBpar1 = '*}
	sCBpar1_val=${sCBpar1_val_raw%%' +/-'*}
	sCBpar1_err_raw=${sCBpar1#*'+/- '*}
	sCBpar1_err=${sCBpar1_err_raw%%' L('*}
    fi

    # grab aLopar0 value with eror
    aLopar0=`grep -r 'aLopar0' $f`
    if [[ $aLopar0 == *"aLopar0 ="* ]]
    then
	aLopar0_val_raw=${aLopar0#*'aLopar0 = '*}
	aLopar0_val=${aLopar0_val_raw%%' +/-'*}
	aLopar0_err_raw=${aLopar0#*'+/- '*}
	aLopar0_err=${aLopar0_err_raw%%' L('*}
    fi

    # grab aLopar1 value with error
    aLopar1=`grep -r 'aLopar1' $f`
    if [[ $aLopar1 == *"aLopar1 ="* ]]
    then
	aLopar1_val_raw=${aLopar1#*'aLopar1 = '*}
	aLopar1_val=${aLopar1_val_raw%%' +/-'*}
	aLopar1_err_raw=${aLopar1#*'+/- '*}
	aLopar1_err=${aLopar1_err_raw%%' L('*}
    fi

    # grab aLopar2 value with eror
    aLopar2=`grep -r 'aLopar2' $f`
    if [[ $aLopar2 == *"aLopar2 ="* ]]
    then
	aLopar2_val_raw=${aLopar2#*'aLopar2 = '*}
	aLopar2_val=${aLopar2_val_raw%%' +/-'*}
	aLopar2_err_raw=${aLopar2#*'+/- '*}
	aLopar2_err=${aLopar2_err_raw%%' L('*}
    fi
    if [[ $aLopar2 = "" ]]
    then
	aLopar2_val="0"
	aLopar2_err="0"
    fi

    # grab aHipar0 value with eror
    aHipar0=`grep -r 'aHipar0' $f`
    if [[ $aHipar0 == *"aHipar0 ="* ]]
    then
	aHipar0_val_raw=${aHipar0#*'aHipar0 = '*}
	aHipar0_val=${aHipar0_val_raw%%' +/-'*}
	aHipar0_err_raw=${aHipar0#*'+/- '*}
	aHipar0_err=${aHipar0_err_raw%%' L('*}
    fi

    # grab aHipar1 value with eror
    aHipar1=`grep -r 'aHipar1' $f`
    if [[ $aHipar1 == *"aHipar1 ="* ]]
    then
	aHipar1_val_raw=${aHipar1#*'aHipar1 = '*}
	aHipar1_val=${aHipar1_val_raw%%' +/-'*}
	aHipar1_err_raw=${aHipar1#*'+/- '*}
	aHipar1_err=${aHipar1_err_raw%%' L('*}
    fi

    #change formula for alphaHi, beed to add one more parameter
    # grab aHipar1 value with eror
    aHipar2=`grep -r 'aHipar2' $f`
    if [[ $aHipar2 == *"aHipar2 ="* ]]
    then
	aHipar2_val_raw=${aHipar2#*'aHipar2 = '*}
	aHipar2_val=${aHipar2_val_raw%%' +/-'*}
	aHipar2_err_raw=${aHipar2#*'+/- '*}
	aHipar2_err=${aHipar2_err_raw%%' L('*}
    fi
    if [[ $aHipar2 = "" ]]
    then
	aHipar2_val="0"
	aHipar2_err="0"
    fi

     echo   $mCBpar0_val $'\t'  $mCBpar1_val $'\t' $mCBpar2_val $'\n'   $sCBpar0_val  $'\t' $sCBpar1_val$'\t' " 0 " $'\n' $aLopar0_val $'\t' $aLopar1_val $'\t' $aLopar2_val  $'\n' $aHipar0_val  $'\t' $aHipar1_val $'\t' $aHipar2_val  > MultipleParameters_$prod.txt


done

cd ../
FILES=multiple_input_*$prod*.dat
for f in $FILES 
#for f in $(ls -1v *$prod* )
do
    echo "Processing $f file..."

    # grab delta 
    delta_form=`grep -r 'formula delta = ' $f`
    if [[ $delta_form == "formula delta ="* ]]
    then
	delta_form=${delta_form#'formula delta = '*}
	delta_form=${delta_form//'mX'/'x'}
	#echo $delta_form

    fi

    # grab dM through cbPeak 
    cbPeak_form=`grep -r 'formula cbPeak    =' $f`
    if [[ $cbPeak_form == "formula cbPeak    = "* ]]
    then
	cbPeak_form=${cbPeak_form#'formula cbPeak    = '*}
	cbPeak_form=${cbPeak_form//'delta'/$delta_form}
	#echo $cbPeak_form
	dM_form=${cbPeak_form//'+ mX'/' '}
	dM_form_mult=${dM_form//'mCBpar0'/'[0]'}
	dM_form_mult=${dM_form_mult//'mCBpar1'/'[1]'}
	dM_form_mult=${dM_form_mult//'mCBpar2'/'[2]'}
	#dM_form_mult=${dM_form//'mCBpar0'/$mCBpar0_val}
	#dM_form_mult=${dM_form_mult//'mCBpar1'/$mCBpar1_val}
	#dM_form_mult=${dM_form_mult//'mCBpar2'/$mCBpar2_val}
	#echo $dM_form
	#echo $dM_form_mult
    fi

    # grab cbSigma 
    cbSigma_form=`grep -r 'formula cbSigma   =' $f`
    if [[ $cbSigma_form == "formula cbSigma   ="* ]]
    then
	cbSigma_form=${cbSigma_form#'formula cbSigma   ='*}
	cbSigma_form=${cbSigma_form//'delta'/$delta_form}
	cbSigma_form_mult=${cbSigma_form//'sCBpar0'/'[0]'}
	cbSigma_form_mult=${cbSigma_form_mult//'sCBpar1'/'[1]'}
	#cbSigma_form_mult=${cbSigma_form//'sCBpar0'/$sCBpar0_val}
	#cbSigma_form_mult=${cbSigma_form_mult//'sCBpar1'/$sCBpar1_val}
	#echo $cbSigma_form
	#echo $cbSigma_form_mult
    fi

    # grab cbAlphaLo "formula cbAlphaLo ="
    cbAlphaLo_form=`grep -r 'formula cbAlphaLo =' $f | head -1`
    #echo $cbAlphaLo_form
    if [[ $cbAlphaLo_form == "formula"* ]]
    then
	cbAlphaLo_form=${cbAlphaLo_form#'formula cbAlphaLo ='*}
	cbAlphaLo_form=${cbAlphaLo_form//'delta'/$delta_form}
	cbAlphaLo_form=${cbAlphaLo_form%'//'*}
	cbAlphaLo_form=${cbAlphaLo_form//'//'/''}
	#cbAlphaLo_form=${cbAlphaLo_form//'mX'/'x'}
	cbAlphaLo_form=${cbAlphaLo_form//'delta'/$delta_form}
	cbAlphaLo_form_mult=${cbAlphaLo_form//'aLopar0'/'[0]'}
	cbAlphaLo_form_mult=${cbAlphaLo_form_mult//'aLopar1'/'[1]'}
	cbAlphaLo_form_mult=${cbAlphaLo_form_mult//'aLopar2'/'[2]'}
	#cbAlphaLo_form_mult=${cbAlphaLo_form_mult//'aLopar2'/'0'}
	#cbAlphaLo_form_mult=${cbAlphaLo_form//'aLopar0'/$aLopar0_val}
	#cbAlphaLo_form_mult=${cbAlphaLo_form_mult//'aLopar1'/$aLopar1_val}
	#cbAlphaLo_form_mult=${cbAlphaLo_form_mult//'aLopar2'/$aLopar2_val}
	#echo $cbAlphaLo_form
	#echo $cbAlphaLo_form_mult
    fi

    # grab cbAlphaHi "formula cbAlphaHi ="
    cbAlphaHi_form=`grep -r 'formula cbAlphaHi =' $f | head -1`
    #echo $cbAlphaHi_form
    if [[ $cbAlphaHi_form == "formula"* ]]
    then
	# cbAlphaHi_form=${cbAlphaHi_form#'formula cbAlphaHi ='*}
	# cbAlphaHi_form=${cbAlphaHi_form//'delta'/$delta_form}
	# cbAlphaHi_form_mult=${cbAlphaHi_form//'aHipar0'/'[0]'}
	# cbAlphaHi_form_mult=${cbAlphaHi_form_mult//'aHipar1'/'[1]'}

    #change formula for alphaHi, need to add one more parameter

	cbAlphaHi_form=${cbAlphaHi_form#'formula cbAlphaHi ='*}
	cbAlphaHi_form=${cbAlphaHi_form//'delta'/$delta_form}
	cbAlphaHi_form=${cbAlphaHi_form%'//'*}
	cbAlphaHi_form=${cbAlphaHi_form//'//'/''}
	#cbAlphaHi_form=${cbAlphaHi_form//'mX'/'x'}
	cbAlphaHi_form=${cbAlphaHi_form//'delta'/$delta_form}
	cbAlphaHi_form_mult=${cbAlphaHi_form//'aHipar0'/'[0]'}
	cbAlphaHi_form_mult=${cbAlphaHi_form_mult//'aHipar1'/'[1]'}
	cbAlphaHi_form_mult=${cbAlphaHi_form_mult//'aHipar2'/'[2]'}

	#cbAlphaHi_form_mult=${cbAlphaHi_form//'aHipar0'/$aHipar0_val}
	#cbAlphaHi_form_mult=${cbAlphaHi_form_mult//'aHipar1'/$aHipar1_val}
	#echo $cbAlphaHi_form
	#echo $cbAlphaHi_form_mult
    fi
    echo    $dM_form_mult $'\n' $cbSigma_form_mult  $'\n' $cbAlphaLo_form_mult  $'\n' $cbAlphaHi_form_mult > output_$CAT/MultipleFitFormulas_$prod.txt
done
