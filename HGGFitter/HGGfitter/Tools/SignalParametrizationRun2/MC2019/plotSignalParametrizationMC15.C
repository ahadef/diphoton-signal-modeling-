/*=============================================================*/
/* Plot parameters of Signal Parametrization obtained from     */
/* single and multiple fits                                    */
/* Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>     */
/*=============================================================*/

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TString.h"
#include "TF1.h"
#include <iostream>
#include <fstream>
//#include <string.h>

#include "AtlasStyle.C"
#include "AtlasUtils.C"

using namespace std;

void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);

void plotSignalParametrizationMC15(Int_t profIt=0, Int_t all_steps=1, TString cat ="inclusive") {

  //gROOT->Reset();
  SetAtlasStyle();
  //gStyle->SetPalette(1);

  TString prod_name[5] = { "MG", "VBF", "ttH", "ZH", "WH" };

  if(all_steps!=0){
  system("./searchFitValuesSingleMC15.sh "+prod_name[profIt]+" "+cat); 
  }
  /*================================================================================*/
  /*                               Single Fit Values                                */
  /*================================================================================*/

  //Check how many entries we have for particulat production mode
  unsigned int number_of_lines = 0;
  TString file_name = "../../../datacards/LowHighMassRun2/SP/MC2019/output_"+cat+"/SingleFitVal_"+prod_name[profIt]+".txt";
  const char *name = file_name.Data();
  std::cout<< name << endl;
  FILE *infile = fopen(name,"r");
  if (infile==NULL)
    {
      std::cout<< "Can't open file with fit values for single points:  " << name << endl;
      std::cout<< "You can try run same code with second argument !=0 to initiate reading datacards of single fits (in case they are exist) " << name << endl;
      return;
    }

  int ch;
  while (EOF != (ch=getc(infile)))
    if ('\n' == ch)
      ++number_of_lines;
  printf("%u\n", number_of_lines);

  Float_t point, dmHiggs_val, dmHiggs_err, cbSigma_val, cbSigma_err, cbAlphaLo_val, cbAlphaLo_err, cbAlphaHi_val, cbAlphaHi_err;

  const unsigned int size = number_of_lines;
  cout<< size<< endl;
  Float_t point_v[size], point_err_v[size], dmHiggs_val_v[size], dmHiggs_err_v[size], cbSigma_val_v[size], cbSigma_err_v[size], cbAlphaLo_val_v[size], cbAlphaLo_err_v[size], cbAlphaHi_val_v[size], cbAlphaHi_err_v[size];

  std::ifstream vInput;
  vInput.open(name);
  for(int i=0; i<number_of_lines; i++)
    {
      vInput >> point >> dmHiggs_val >> dmHiggs_err >> cbSigma_val >> cbSigma_err >> cbAlphaLo_val >> cbAlphaLo_err >> cbAlphaHi_val >> cbAlphaHi_err;
      point_v[i] = point; 
      point_err_v[i] = 0; 
      dmHiggs_val_v[i] = dmHiggs_val; 
      dmHiggs_err_v[i] = dmHiggs_err; 
      cbSigma_val_v[i] = cbSigma_val; 
      cbSigma_err_v[i] = cbSigma_err; 
      cbAlphaLo_val_v[i] = cbAlphaLo_val; 
      cbAlphaLo_err_v[i] = cbAlphaLo_err; 
      cbAlphaHi_val_v[i] = cbAlphaHi_val; 
      cbAlphaHi_err_v[i] = cbAlphaHi_err;
      //cout<<  "point: "<<point<< "; dm val: "<< dmHiggs_val<< " +- "<< dmHiggs_err<< endl;
    }
  vInput.close();

  if(all_steps!=0){
  system("./searchFormulasMultipleMC15.sh "+prod_name[profIt]+" "+cat); 
  }

  /*================================================================================*/
  /*                             Multiple Fit Values                                */
  /*================================================================================*/

  int mass_range_max=6000 , mass_range_min=40;

  int fit_range_max , fit_range_min;
  fit_range_max=mass_range_max;
  fit_range_min=mass_range_min;

  TF1* fa[8];
  char  formula_name[128];
  TString line;
  char formula[128];

  TString file_name_multiple = "../../../datacards/LowHighMassRun2/SP/MC2019/output_"+cat+"/MultipleFitFormulas_"+prod_name[profIt]+".txt";
  const char *name_multiple = file_name_multiple.Data();
  std::cout<< name_multiple << endl;
  std::ifstream vInput_f;
  vInput_f.open(name_multiple);

  TString list_param_multiple = "../../../datacards/LowHighMassRun2/SP/MC2019/output_"+cat+"/MultipleParameters_"+prod_name[profIt]+".txt";
  const char *param_multiple = list_param_multiple.Data();
  std::cout<< param_multiple << endl;
  std::ifstream vInput_m;
  vInput_m.open(param_multiple);
  Float_t par0, par1, par2;
  for(int i=0; i<4; i++)
    {
      vInput_f.getline(formula,128,'\n');
      cout<< " ----------------- FORMULA ---------------"<<endl;
      cout<< formula<<endl;
    

      vInput_m >> par0 >> par1 >> par2;
      cout<< "p0: "<< par0<< "; p1: "<< par1<< "; p2: "<< par2 <<endl;

      sprintf(formula_name,"form_%01d",i);      
      fa[i] = new TF1(formula_name,formula,mass_range_min,mass_range_max);
      fa[i]->SetParameter(0, par0);
      fa[i]->SetParameter(1, par1);
      fa[i]->SetParameter(2, par2);

      //
      sprintf(formula_name,"fit_form_%01d",i);      
      fa[i+4] = new TF1(formula_name,formula,fit_range_min,fit_range_max);
      fa[i+4]->SetParameter(0, par0);

    }



  //TCanvas* canv_FitParams = new TCanvas("FitParams", "FitParams",10,10,800,600);
  TCanvas* canv_FitParams_1 = new TCanvas("FitParams_1", "FitParams_1",10,10,800,600);
  TCanvas* canv_FitParams_2 = new TCanvas("FitParams_2", "FitParams_2",10,10,800,600);
  TCanvas* canv_FitParams_3 = new TCanvas("FitParams_3", "FitParams_3",10,10,800,600);
  TCanvas* canv_FitParams_4 = new TCanvas("FitParams_4", "FitParams_4",10,10,800,600);
  //canv_FitParams->Divide(2,2);

  canv_FitParams_1->cd();
  TGraphErrors*  gr0 = new TGraphErrors(size,point_v,dmHiggs_val_v, point_err_v, dmHiggs_err_v );
  gr0->SetMarkerSize(0.9);
  gr0->SetMarkerColor(4);
  gr0->SetLineColor(4);
  gr0->SetMinimum(-10);
  gr0->SetMaximum(20);
  gr0->GetXaxis()->SetTitle("m_{X} [ GeV]");
  gr0->GetYaxis()->SetTitle("#Delta m_{H} [ GeV]");
  gr0->Draw("PA");

  fa[0]->SetLineColor(2);
  //fa[0]->Draw("same");

  fa[4]->SetLineStyle(3);
  fa[4]->SetLineColor(4);
  gr0->Fit(fa[4],"same","",fit_range_min, fit_range_max);

      TLegend* leg = new TLegend(0.2, 0.6, 0.45, 0.8); 
     // if( cat!="inclusive" && i==2)      leg = new TLegend(0.3, 0.22, 0.55, 0.42); 
      leg->SetBorderSize(0);
      leg->SetFillColor(0);
      leg->SetTextSize(0.04);
      // leg->AddEntry(gr0, "mc15C, MadGraph5 spin0 samples, H-cuts,", "");
      leg->AddEntry(gr0, "mc16, MG spin0 samples, ", "");
      leg->AddEntry(gr0, "h024, NWA", "");
      leg->AddEntry(gr0, "single fits", "lep");
      leg->AddEntry(fa[7], "single fit parametrization", "l");
      //leg->AddEntry(fa[3], "multiple fit parametrization", "l");           
      leg->Draw("same");
      ATLASLabel(0.2,0.85,"Internal Simulation",1,0.05);

  canv_FitParams_1->Print("../../../datacards/LowHighMassRun2/SP/MC2019/plots/FinalFit_"+prod_name[profIt]+"_"+cat+"_deltaM.pdf");

  canv_FitParams_2->cd();
  TGraphErrors*  gr1 = new TGraphErrors(size,point_v,cbSigma_val_v, point_err_v, cbSigma_err_v );
  gr1->SetMarkerSize(0.9);
  gr1->SetMarkerColor(4);
  gr1->SetLineColor(4);
  gr1->SetMinimum(0.5);
  gr1->SetMaximum(35);
  gr1->GetXaxis()->SetTitle("m_{X} [ GeV]");
  gr1->GetYaxis()->SetTitle("#sigma_{CB} [ GeV]");
  gr1->Draw("PA");

  fa[1]->SetLineColor(2);
  //fa[1]->Draw("same");

  fa[5]->SetLineStyle(3);
  fa[5]->SetLineColor(4);
  gr1->Fit(fa[5],"same","",fit_range_min, fit_range_max);

  leg = new TLegend(0.2, 0.6, 0.45, 0.8); 
     // if( cat!="inclusive" && i==2)      leg = new TLegend(0.3, 0.22, 0.55, 0.42); 
      leg->SetBorderSize(0);
      leg->SetFillColor(0);
      leg->SetTextSize(0.04);
      // leg->AddEntry(gr0, "mc15C, MadGraph5 spin0 samples, H-cuts,", "");
      leg->AddEntry(gr0, "mc16, MG spin0 samples, ", "");
      leg->AddEntry(gr0, "h024, NWA", "");
      leg->AddEntry(gr0, "single fits", "lep");
      leg->AddEntry(fa[7], "single fit parametrization", "l");
      //leg->AddEntry(fa[3], "multiple fit parametrization", "l");           
      leg->Draw("same");
      ATLASLabel(0.2,0.85,"Internal Simulation",1,0.05);

  canv_FitParams_2->Print("../../../datacards/LowHighMassRun2/SP/MC2019/plots/FinalFit_"+prod_name[profIt]+"_"+cat+"_sigma.pdf");

  canv_FitParams_3->cd();
  TGraphErrors* gr2 = new TGraphErrors(size,point_v,cbAlphaLo_val_v, point_err_v, cbAlphaLo_err_v );
  gr2->SetMarkerSize(0.9);
  gr2->SetMarkerColor(4);
  gr2->SetLineColor(4);
  gr2->SetMinimum(0.5);
  gr2->SetMaximum(3.0);
  if(cat=="2conv") gr2->SetMinimum(1.0);
  if(cat=="2unconv") gr2->SetMaximum(3.1);
  gr2->GetXaxis()->SetTitle("m_{X} [ GeV]");
  gr2->GetYaxis()->SetTitle("#alpha_{Low}");
  gr2->Draw("PA");

  fa[2]->SetLineColor(2);
  //fa[2]->Draw("same");

  fa[6]->SetLineStyle(3);
  fa[6]->SetLineColor(4);
  gr2->Fit(fa[6],"same","",fit_range_min, fit_range_max);
  leg = new TLegend(0.2, 0.6, 0.45, 0.8); 
     // if( cat!="inclusive" && i==2)      leg = new TLegend(0.3, 0.22, 0.55, 0.42); 
      leg->SetBorderSize(0);
      leg->SetFillColor(0);
      leg->SetTextSize(0.04);
      // leg->AddEntry(gr0, "mc15C, MadGraph5 spin0 samples, H-cuts,", "");
      leg->AddEntry(gr0, "mc16, MG spin0 samples, ", "");
      leg->AddEntry(gr0, "h024, NWA", "");
      leg->AddEntry(gr0, "single fits", "lep");
      leg->AddEntry(fa[7], "single fit parametrization", "l");
      //leg->AddEntry(fa[3], "multiple fit parametrization", "l");           
      leg->Draw("same");
      ATLASLabel(0.2,0.85,"Internal Simulation",1,0.05);
  canv_FitParams_3->Print("../../../datacards/LowHighMassRun2/SP/MC2019/plots/FinalFit_"+prod_name[profIt]+"_"+cat+"_alphaLow.pdf");

  canv_FitParams_4->cd();

  TGraphErrors*  gr3 = new TGraphErrors(size,point_v,cbAlphaHi_val_v, point_err_v, cbAlphaHi_err_v );
  gr3->SetMarkerSize(0.9);
  gr3->SetMarkerColor(4);
  gr3->SetLineColor(4);
  gr3->SetMinimum(1.2);
  gr3->SetMaximum(3.0);
  if(cat=="2unconv") gr3->SetMaximum(3.5);
  if(cat=="1conv") gr3->SetMinimum(1.0);
  gr3->GetXaxis()->SetTitle("m_{X} [ GeV]");
  gr3->GetYaxis()->SetTitle("#alpha_{High}");
  gr3->Draw("PA");

  fa[3]->SetLineColor(2);
  //fa[3]->Draw("same");

  fa[7]->SetLineStyle(3);
  fa[7]->SetLineColor(4);
  gr3->Fit(fa[7],"same","",fit_range_min, fit_range_max);

      leg = new TLegend(0.2, 0.6, 0.45, 0.8); 
     // if( cat!="inclusive" && i==2)      leg = new TLegend(0.3, 0.22, 0.55, 0.42); 
      leg->SetBorderSize(0);
      leg->SetFillColor(0);
      leg->SetTextSize(0.04);
      // leg->AddEntry(gr0, "mc15C, MadGraph5 spin0 samples, H-cuts,", "");
      leg->AddEntry(gr0, "mc16, MG spin0 samples, ", "");
      leg->AddEntry(gr0, "h024, NWA", "");
      leg->AddEntry(gr0, "single fits", "lep");
      leg->AddEntry(fa[7], "single fit parametrization", "l");
      //leg->AddEntry(fa[3], "multiple fit parametrization", "l");           
      leg->Draw("same");
      ATLASLabel(0.2,0.85,"Internal Simulation",1,0.05);

  canv_FitParams_4->Print("../../../datacards/LowHighMassRun2/SP/MC2019/plots/FinalFit_"+prod_name[profIt]+"_"+cat+"_alphaHigh.pdf");

/*
  char point_name[128], fit_name[128], ratio_name[128];

  for(int i=0; i<4; i++)
    {
      canv_FitParams->cd(i+1);
      sprintf(point_name,"gr%01d",i);      
      sprintf(fit_name,"fa[%01d",i);      
      sprintf(formula_name,"form_%01d",i);      

      cout<< point_name<< endl;

    }
*/

  //canv_FitParams->Print("../../../datacards/LowHighMassRun2/SP/MC2019/plots/FinalFit_"+prod_name[profIt]+"_"+cat+"_test.pdf");

}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  //l.SetTextSize(tsize); 
  l.SetNDC();
  //l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    //p.SetTextFont(42);
    p.SetTextColor(color);
    //p.SetTextSize(tsize);
    p.DrawLatex(x+delx*1.1,y,text);
    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}
