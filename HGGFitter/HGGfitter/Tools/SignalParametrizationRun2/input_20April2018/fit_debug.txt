
Processing plotSignalParametrizationMC15.C(0,1)...

Applying ATLAS style settings...

point:  60 dm:  0.20275  +-  0.036004  sig:  0.97094  +-  0.042249  alphaLow:  1.4852  +-  0.12430  alphaHigh:  1.3680  +-  0.12289
point:  80 dm:  0.096124  +-  0.023135  sig:  1.3291  +-  0.027303  alphaLow:  1.5533  +-  0.058800  alphaHigh:  1.4282  +-  0.071851
point:  100 dm:  0.065410  +-  0.022542  sig:  1.5104  +-  0.024604  alphaLow:  1.6038  +-  0.052541  alphaHigh:  1.5541  +-  0.068551
../../../datacards/LowHighMassRun2/SP/MC165504/output/SingleFitVal_MG.txt
3
3
Processing multiple_MG.dat file...
Processing multiple_input_MG_change.dat file...
Processing multiple_input_MG.dat file...
../../../datacards/LowHighMassRun2/SP/MC165504/output/MultipleFitFormulas_MG.txt
../../../datacards/LowHighMassRun2/SP/MC165504/output/MultipleParameters_MG.txt
 ----------------- FORMULA ---------------
([0] + [1]*((x - (100))/100) + [2]*((x - (100))/100)*((x - (100))/100) ) 
p0: 0.064927; p1: 0.051782; p2: 0.9801
 ----------------- FORMULA ---------------
 ([0] + [1]*((x - (100))/100)) 
p0: 1.5289; p1: 1.263; p2: 0
 ----------------- FORMULA ---------------
 ([0] + [1]*((x - (100))/100) + [2]*((x - (100))/100)*((x - (100))/100)) 
p0: 1.6285; p1: 1.402; p2: 3.2179
 ----------------- FORMULA ---------------
 ([0] + [1]*((x - (100))/100) + [2]*((x - (100))/100)*((x - (100))/100))
p0: 1.5859; p1: 2.2312; p2: 4.8867
 FCN=1.51606e-30 FROM MINOS     STATUS=FAILURE       140 CALLS         479 TOTAL
                     EDM=6.06414e-30    STRATEGY= 1  ERROR MATRIX UNCERTAINTY 100.0 per cent
  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           6.54100e-02   2.25420e-02   2.77554e-17   0.00000e+00
   2  p1           0.00000e+00   6.00000e-01  -0.00000e+00   0.00000e+00
   3  p2           0.00000e+00   1.41421e+00   1.41421e+00   0.00000e+00
 FCN=6.49897e-20 FROM MINOS     STATUS=FAILURE        84 CALLS         219 TOTAL
                     EDM=2.59959e-19    STRATEGY= 1  ERROR MATRIX UNCERTAINTY 100.0 per cent
  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.51040e+00   2.46040e-02   6.27232e-12   0.00000e+00
   2  p1           0.00000e+00   6.00000e-01   6.00000e-01   0.00000e+00
 FCN=8.89707e-22 FROM MINOS     STATUS=FAILURE       140 CALLS         479 TOTAL
                     EDM=3.55883e-21    STRATEGY= 1  ERROR MATRIX UNCERTAINTY 100.0 per cent
  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.60380e+00   5.25410e-02  -1.56719e-12   0.00000e+00
   2  p1           0.00000e+00   6.00000e-01  -0.00000e+00   0.00000e+00
   3  p2           0.00000e+00   1.41421e+00   1.41421e+00   0.00000e+00
 FCN=2.02538e-21 FROM MINOS     STATUS=FAILURE       140 CALLS         479 TOTAL
                     EDM=8.10154e-21    STRATEGY= 1  ERROR MATRIX UNCERTAINTY 100.0 per cent
  EXT PARAMETER                APPROXIMATE        STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.55410e+00   6.85510e-02   3.08509e-12   0.00000e+00
   2  p1           0.00000e+00   6.00000e-01  -0.00000e+00   0.00000e+00
   3  p2           0.00000e+00   1.41421e+00   1.41421e+00   0.00000e+00
gr0
gr1
gr2
gr3
