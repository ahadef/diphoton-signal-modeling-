using namespace TreeReader;
void mergeMC_sep( TString WidthType="NW", TString VarType="", TString VarWay="",int prodId=0, TString category = "inclusive") {

  std::vector<int> massPoints;

  TString sample[5] = { "MG", "VBFH", "ttH", "ZH", "WH" };
  TString generator[5] = { "PowHeg", "PowHeg", "Pythia", "Pythia", "Pythia" };

  std::cout << " -------------MERGING sample " << sample[prodId] << std::endl;

  if(prodId==0)
    { //MG

       massPoints.push_back(60);
       massPoints.push_back(70);
       massPoints.push_back(80);
       massPoints.push_back(90);
       massPoints.push_back(100);
       massPoints.push_back(110);
       massPoints.push_back(120);
       massPoints.push_back(140);
       massPoints.push_back(160);
       massPoints.push_back(180);
       massPoints.push_back(200);
      //massPoints.push_back(1000);
      //massPoints.push_back(1200);
      //massPoints.push_back(1600);
      //massPoints.push_back(2000);
      //massPoints.push_back(2400);
      //MG
    }


  MultiReader input, output;
  TString myweightNbPU = "weightNbPU";
  TString myweightN = "weightN";
  TString myweight = "weight";
  TString mymass = "mgg";
  
  input.AddFloat("weightNbPU");
  input.AddFloat("weightN");
  input.AddFloat("weight");
  input.AddFloat("mgg");

  output.AddDouble("mgg_m60");
  output.AddDouble("mgg_m70");
  output.AddDouble("mgg_m80");
  output.AddDouble("mgg_m90");
  output.AddDouble("mgg_m100");
  output.AddDouble("mgg_m110");
  output.AddDouble("mgg_m120");
  output.AddDouble("mgg_m140");
  output.AddDouble("mgg_m160");
  output.AddDouble("mgg_m180");
  output.AddDouble("mgg_m200");
  //output.AddDouble("mgg_m800");  
  //output.AddDouble("mgg_m1000");
  //output.AddDouble("mgg_m1200");
  //output.AddDouble("mgg_m1600");
  //output.AddDouble("mgg_m2000");
  //output.AddDouble("mgg_m2400");
  output.AddDouble("weight");
  output.AddDouble("weightN");
  output.AddDouble("weightNbPU");
  output.AddInt("mHCat_idx");

  TString BaseFileName = "../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/";

  if(VarType!=""){
    BaseFileName = "../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/systematics/"+VarType+"/"+VarWay+"/";
  }

  TString outputFileName = BaseFileName+"merged/gen16_13TeV_" + sample[prodId] + "_merged.root";
  if(category=="2conv")   outputFileName = BaseFileName+"merged/gen16_13TeV_" + sample[prodId] + "_merged_2conv.root";
  if(category=="1conv")   outputFileName = BaseFileName+"merged/gen16_13TeV_" + sample[prodId] + "_merged_1conv.root";
  if(category=="2unconv")   outputFileName = BaseFileName+"merged/gen16_13TeV_" + sample[prodId] + "_merged_2unconv.root";
  if(WidthType!="NW"){outputFileName = BaseFileName+"merged/gen16_13TeV_" + sample[prodId] + "_W"+WidthType+"_merged.root";}
  TFile* outFile = 0;
  TTree* outTree;
  outTree = TreeAbsReader::NewTree("tree", outputFileName, &outFile);
  output.WriteTo(outTree);

  Double_t Npoints=massPoints.size();
  std::cout<< Npoints<<"; last: "<<massPoints[Npoints-1]<< endl;
  for (unsigned int i = 0; i < massPoints.size(); i++) 
    {//genData_MG1400_W10p.root
	TString inputFileName;
      if(category=="inclusive")inputFileName = BaseFileName+"mc16a_13TeV_" + sample[prodId] + Form("%02d", massPoints[i]) + ".root";
      else if(category=="2conv")inputFileName = BaseFileName+"mc16a_13TeV_" + sample[prodId] + Form("%02d", massPoints[i]) + "_2conv.root";
      else if(category=="1conv")inputFileName = BaseFileName+"mc16a_13TeV_" + sample[prodId] + Form("%02d", massPoints[i]) + "_1conv.root";
      else if(category=="2unconv")inputFileName = BaseFileName+"mc16a_13TeV_" + sample[prodId] + Form("%02d", massPoints[i]) + "_2unconv.root";
      else cout<<"ERROR:	"<<category<<"-->  This category does not exist!"<<endl;
  //mc15_13TeV_MG200_NW.root
      if(WidthType!="NW"){inputFileName = BaseFileName+"mc16a_13TeV_" + sample[prodId] + Form("%02d", massPoints[i]) + "_W"+WidthType+".root";}
      TFile* inFile = 0;
      TTree* inTree = TreeAbsReader::GetTree("tree", inputFileName, &inFile);
      //TTree* inTree = TreeAbsReader::GetTree("tree_unconv", inputFileName, &inFile);
      input.ReadFrom(inTree);
      std::cout << "Processing point " << massPoints[i] << ", " << inTree->GetEntries() << " events." << endl;
      output.Int("mHCat_idx")->SetValue(i);

      
      for (unsigned int k = 0; k < inTree->GetEntries(); k++) 
	{
	  inTree->GetEntry(k);

	  output.Double("mgg_m60")->SetValue(i == 2 ? input.Float(mymass)->GetValue() : 60);
	  output.Double("mgg_m70")->SetValue(i == 3 ? input.Float(mymass)->GetValue() : 70);
	  output.Double("mgg_m80")->SetValue(i == 4 ? input.Float(mymass)->GetValue() : 80);
	  output.Double("mgg_m90")->SetValue(i == 5 ? input.Float(mymass)->GetValue() : 90);
	  output.Double("mgg_m100")->SetValue(i == 6 ? input.Float(mymass)->GetValue() : 100);
	  output.Double("mgg_m110")->SetValue(i == 7 ? input.Float(mymass)->GetValue() : 110);
	  output.Double("mgg_m120")->SetValue(i == 8 ? input.Float(mymass)->GetValue() : 120);
	  output.Double("mgg_m140")->SetValue(i == 9 ? input.Float(mymass)->GetValue() : 140);
	  output.Double("mgg_m160")->SetValue(i == 10 ? input.Float(mymass)->GetValue() : 160);
	  output.Double("mgg_m180")->SetValue(i == 11 ? input.Float(mymass)->GetValue() : 180);
	  output.Double("mgg_m200")->SetValue(i == 12 ? input.Float(mymass)->GetValue() : 200);
	  //output.Double("mgg_m800")->SetValue(i == 3 ? input.Float(mymass)->GetValue() : 800);
	  //output.Double("mgg_m1000")->SetValue(i == 4 ? input.Float(mymass)->GetValue() : 1000);
	  //output.Double("mgg_m1200")->SetValue(i == 5 ? input.Float(mymass)->GetValue() : 1200);
	  //output.Double("mgg_m1600")->SetValue(i == 6 ? input.Float(mymass)->GetValue() : 1600);
	  //output.Double("mgg_m2000")->SetValue(i == 7 ? input.Float(mymass)->GetValue() : 2000);
	  //output.Double("mgg_m2400")->SetValue(i == 8 ? input.Float(mymass)->GetValue() : 2400);

	  //////output.Double("mgg_m2000")->SetValue(i == 6 ? input.Float(mymass)->GetValue() : 2000);
	  //output.Double("mgg_m2400")->SetValue(i == 7 ? input.Float(mymass)->GetValue() : 2400);

	  output.Double("weight")->SetValue(input.Float(myweight)->GetValue()); 
	  output.Double("weightN")->SetValue(input.Float(myweightN)->GetValue()); 
	  output.Double("weightNbPU")->SetValue(input.Float(myweightNbPU)->GetValue()); 
	  outTree->Fill();
	}
	    
    }


  std::cout <<" quite the loop"<< endl;
  outFile->cd();
  outTree->Write();
  std::cout <<" write the tree  " << outputFileName << endl;
  //delete outFile;

}
