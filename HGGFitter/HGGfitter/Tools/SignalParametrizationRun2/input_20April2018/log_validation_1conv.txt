
Processing plotSignalParametrizationMC15.C(0,1,"1conv")...

Applying ATLAS style settings...

point:  40 dm:  0.016535  +-  0.067144  sig:  0.55540  +-  0.065624  alphaLow:  1.5467  +-  0.28143  alphaHigh:  0.86252  +-  0.16171
point:  50 dm:  0.13556  +-  0.057418  sig:  0.91549  +-  0.065679  alphaLow:  1.7525  +-  0.21606  alphaHigh:  1.0340  +-  0.10079
point:  60 dm:  0.24016  +-  0.050565  sig:  1.2240  +-  0.057942  alphaLow:  1.6380  +-  0.20733  alphaHigh:  1.7383  +-  0.25048
point:  70 dm:  0.077586  +-  0.050581  sig:  1.2964  +-  0.068101  alphaLow:  1.3787  +-  0.11590  alphaHigh:  1.2783  +-  0.14049
point:  80 dm:  0.12918  +-  0.040409  sig:  1.4761  +-  0.038552  alphaLow:  1.6377  +-  0.092291  alphaHigh:  1.6104  +-  0.12811
point:  90 dm:  0.097017  +-  0.045167  sig:  1.4818  +-  0.065573  alphaLow:  1.4323  +-  0.10202  alphaHigh:  1.3900  +-  0.14207
point:  100 dm:  0.16539  +-  0.042381  sig:  1.7061  +-  0.039604  alphaLow:  1.6212  +-  0.081081  alphaHigh:  1.6671  +-  0.13211
point:  110 dm:  0.049109  +-  0.044749  sig:  1.6675  +-  0.060976  alphaLow:  1.4996  +-  0.096609  alphaHigh:  1.4398  +-  0.12643
point:  120 dm:  0.10178  +-  0.044024  sig:  1.8441  +-  0.044315  alphaLow:  1.4751  +-  0.077953  alphaHigh:  1.7470  +-  0.11793
point:  140 dm:  0.11687  +-  0.045836  sig:  2.0913  +-  0.044588  alphaLow:  1.6500  +-  0.087390  alphaHigh:  1.8589  +-  0.14573
point:  160 dm:  0.043773  +-  0.049436  sig:  2.1326  +-  0.058842  alphaLow:  1.4820  +-  0.076175  alphaHigh:  1.4715  +-  0.095596
point:  180 dm:  0.16811  +-  0.050180  sig:  2.6101  +-  0.039659  alphaLow:  1.7782  +-  0.087998  alphaHigh:  6.1775  +-  3.0560
point:  200 dm:  0.15364  +-  0.053521  sig:  2.3802  +-  0.065986  alphaLow:  1.4569  +-  0.071394  alphaHigh:  1.4563  +-  0.093354
../../../datacards/LowHighMassRun2/SP/MC165504/output_1conv/SingleFitVal_MG.txt
13
13
Processing multiple_MG_1conv.dat file...
Processing multiple_input_MG.dat file...
../../../datacards/LowHighMassRun2/SP/MC165504/output_1conv/MultipleFitFormulas_MG.txt
../../../datacards/LowHighMassRun2/SP/MC165504/output_1conv/MultipleParameters_MG.txt
 ----------------- FORMULA ---------------
([0] + [1]*((x - (100))/100) ) 
p0: 0.12264; p1: -0.0499093; p2: 0
 ----------------- FORMULA ---------------
 ([0] + [1]*((x - (100))/100)) 
p0: 1.5964; p1: 0.93715; p2: 0
 ----------------- FORMULA ---------------
 ([0] + [1]*((x - (100))/100) ) 
p0: 1.5051; p1: 0.041928; p2: 0
 ----------------- FORMULA ---------------
 ([0] + [1]*((x - (100))/100))
p0: 1.4936; p1: 0.076279; p2: 0
 FCN=16.5925 FROM MINOS     STATUS=SUCCESSFUL     12 CALLS          77 TOTAL
                     EDM=5.77835e-10    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.17250e-01   1.36468e-02  -2.24916e-07   0.00000e+00
   2  p1           1.17290e-03   3.03459e-02   3.03459e-02   0.00000e+00
 FCN=93.5054 FROM MINOS     STATUS=SUCCESSFUL     10 CALLS          87 TOTAL
                     EDM=3.23559e-21    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.60356e+00   1.47896e-02  -0.00000e+00   8.77863e-06
   2  p1           1.12504e+00   3.16531e-02   3.16531e-02   3.53190e-06
 FCN=17.9698 FROM MINOS     STATUS=SUCCESSFUL      8 CALLS         106 TOTAL
                     EDM=9.89687e-22    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.54770e+00   3.22700e-02  -0.00000e+00   1.07959e-06
   2  p1           3.54359e-03   6.06663e-02   6.06663e-02   2.26649e-10
 FCN=42.4361 FROM MINOS     STATUS=SUCCESSFUL     10 CALLS          91 TOTAL
                     EDM=3.82755e-22    STRATEGY= 1      ERROR MATRIX ACCURATE 
  EXT PARAMETER                                   STEP         FIRST   
  NO.   NAME      VALUE            ERROR          SIZE      DERIVATIVE 
   1  p0           1.40981e+00   3.72369e-02   0.00000e+00  -6.00335e-07
   2  p1           2.42913e-01   7.10048e-02   7.10048e-02   3.19092e-11
gr0
gr1
gr2
gr3
