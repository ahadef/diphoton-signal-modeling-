#!/bin/bash

#From HGGfitter/Tools/SignalParametrizationRun2/MC165504 run script: 

root -l -b -q 'plotSignalParametrizationMC15.C(0,1,"inclusive")' >log_validation_inclusive.txt
#root -l -b -q 'plotSignalParametrizationMC15.C(0,1,"2conv")' >log_validation_2conv.txt
#root -l -b -q 'plotSignalParametrizationMC15.C(0,1,"2unconv")' >log_validation_2unconv.txt
#root -l -b -q 'plotSignalParametrizationMC15.C(0,1,"1conv")' >log_validation_1conv.txt
