/*=============================================================*/
/* Signal Parametrisation for Single fit of H->gg distribution */
/* Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>     */
/*=============================================================*/

#include "TString.h"
#include "TCanvas.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
//#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TLine.h"

#include "../AtlasStyle.C"
#include "../AtlasUtils.C"


void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);
void fit_one(double mH = 9999, Int_t mc=165504, TString mc_path_02 = "h016", TString category="inclusive", TString width_val="0", TString Cat="" ,TString VarType="", TString VarWay="",TString prod ="MG", Int_t nice_plot=0);

void comparision_fit_singleMassMC(  Int_t point=9999, Int_t mc=165504, TString mc_path_02 = "h016", TString category="2conv", TString width_val="0", TString Cat="" , TString VarType="", TString VarWay="", Int_t prodIt=0, Int_t nice_plot=0) {
  gROOT->Reset();
  SetAtlasStyle();
  gStyle->SetPalette(1);  
  int numIt=0;//
  //                       0        1         2     3    4
  TString prod_name[5] = { "MG", "VBF", "ttH", "ZH", "WH" };

  std::cout<< "check list of availiable mass points"<< endl;
  Int_t point_infile=0;
  unsigned int number_of_lines = 0;

  // file with list of point availiable for fit
  TString list_of_points, str;
  std::string mc_str = std::to_string(mc);

  list_of_points = prod_name[prodIt]+"_MC_List_of_points.txt";  
  str = "../Tools/SignalParametrizationRun2/MC"+mc_str+"/"+list_of_points;
  cout<< " =======================     !!!!!!!!!!!!!!!!!!!!    ==================  " <<endl;  

  /////////////////////////////////////////////////////////////////////////////////////
  const char *name = str.Data();
  std::cout<< name << endl;

  //*
  FILE *infile = fopen(name,"r");
  int ch;

  while (EOF != (ch=getc(infile)))
    if ('\n' == ch)
      ++number_of_lines;
  printf("%u\n", number_of_lines);

  Int_t vNData = 0;
  ifstream vInput;
  vInput.open(name);
  for(vNData=0; vNData<number_of_lines; vNData++)
    {
      vInput >>point_infile;
      std::cout<< " point: "<< point_infile <<endl;
      char point_name_string[16]; 
      if(point_infile<100){
	//sprintf ( point_name_string, "0%d", point_infile ); // for low mass
	sprintf ( point_name_string, "%d", point_infile );
      }
      else 	      sprintf ( point_name_string, "%d", point_infile );
      
      if(point==9999){// want to loop over all mass points
	fit_one(point_infile,mc,mc_path_02, category,width_val,Cat,VarType,VarWay,prod_name[prodIt],nice_plot);
      }//single fit per each mass point
      
      numIt=0;
      
      if(point_infile==point){//find particulat point
	fit_one(point_infile,mc,mc_path_02, category,width_val,Cat,VarType,VarWay,prod_name[prodIt],nice_plot);
	break;
      }
      if(point_infile!=point && point!=9999){//if we request points which does not exist
	numIt=9999;
      }
      
    }
  vInput.close();
  if(numIt==9999){//protection, if we request points which does not exist
    std::cout << "Sorry, there are no files corresponding to mass points you request ("<< point<<"). Try another one!" << endl;	
    return;
  }

}

void fit_one(double mH = 9999 , Int_t mc=165504, TString mc_path_02 = "h016", TString category="inclusive", TString width_val="0", TString Cat="" , TString VarType="", TString VarWay="", TString prod ="MG",Int_t nice_plot=0  ) {
  TString point, mc_path;
  point.Form("%1.0f",mH);
  mc_path.Form("%1.0d",mc);
 
  TString test, str, indir, indir_02, file_name, file_name_02, datacard_input,  datacard_input_02, datacard_output;
  TString weight_to_use="";
  TString label_plot1,label_plot, label_plot_type,  outname_plot,   datacard_path, datacard_path_02;

  int MCtype; // which MC sample to use, choose path to files 
  MCtype=mc; 
  weight_to_use="weightN";

  //h015:
  /*if(==165502 ){
    label_plot1 = "mc15c, MG5 h015, spin0";
    label_plot_type="H-cuts, with PU "+Cat; weight_to_use="weightN"; prod="MG";
    }
  */

  TLatex tag;
  tag.SetNDC(); // setting coordinates
  tag.SetTextSize(0.3);


  indir = "../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC"+mc_path+"/HGGfitter_format_input/";
  indir_02 = "../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC"+mc_path_02+"/HGGfitter_format_input/";
  file_name= indir + "mc16a_13TeV_"+prod+point+Cat+".root";
  file_name_02= indir_02 + "mc16a_13TeV_"+prod+point+Cat+".root";

  datacard_path="../datacards/LowHighMassRun2/SP/MC"+mc_path+"/";
  datacard_path_02="../datacards/LowHighMassRun2/SP/MC"+mc_path_02+"/";

  label_plot= label_plot_type +" NWA "+point+"  "+Cat+" "+category; 
  outname_plot=datacard_path+"comparision_plots/single_"+prod+point+ "_"+ Cat+".pdf";
  datacard_input =datacard_path+"single_input.dat";
  datacard_input_02 =datacard_path_02+"single_input.dat";

  datacard_output =datacard_path+"output_"+category+"/single_"+prod+point+Cat+".dat";

  if(category!="inclusive") {
    file_name= indir + "mc16a_13TeV_"+prod+point+Cat+"_"+category+".root";
    file_name_02= indir_02 + "mc16a_13TeV_"+prod+point+Cat+"_"+category+".root";

    label_plot= label_plot_type +" NWA "+point+"  "+Cat+" "+category; 
    outname_plot=datacard_path+"comparision_plots/single_"+prod+point+ "_"+ Cat+category+".pdf";
    datacard_input =datacard_path+"single_input_"+category+".dat";
    datacard_input_02 =datacard_path_02+"single_input_"+category+".dat";

    datacard_output =datacard_path+"output_"+category+"/single_"+prod+point+"_"+Cat+category+".dat";
  }


  ////////////////////////////////////////
  /*          SYSTEMATICS        */
  ///////////////////////////////////////
  if(VarType!="" && VarWay!=""){     
    label_plot=label_plot_type+" NWA, "+VarType+" "+VarWay+" "+prod+point+Cat; 
    file_name= indir +"systematics/" + VarType+"/"+VarWay+"/mc16a_13TeV_"+prod+point+Cat+".root";
    datacard_output =datacard_path+"/Systematics/output"+category+"/single_"+prod+point+Cat+"_"+VarType+VarWay+".dat";
    outname_plot=datacard_path+"Systematics/plots/single_"+prod+point+Cat+"_"+VarType+VarWay+".pdf";
  }
  
 
  //set fitrange ±20%m
  int mHup;  int mHlow;
  mHup = mH*1.2;   mHlow = mH*0.8;
  //set #of bins
  int nBins = (mHup - mHlow) /5;
  if(width_val=="0" || width_val=="0.2" || width_val=="0.7" || width_val=="001"){
    mHup = mH*1.1;   mHlow = mH*0.9;  
    nBins = (mHup - mHlow)*2;
  }
  if(mH>400) {    nBins = (mHup - mHlow);  }
  if(mH>1000){    nBins = (mHup - mHlow)/2;  }
  if(mH>1500){    nBins = (mHup - mHlow)/4;  }
  
  if(nice_plot==1){
    mHup = mH*1.2;   mHlow = mH*0.65;
    if(width_val=="0"){
      mHup = mH*1.05;   mHlow = mH*0.9;  
    }
  }
  

  std::cout << "-------- mH: " << mH << "; up: "<< mHup <<"; mHlow: "<< mHlow <<";  n_bins= "<<nBins<<  ";   weights to use: " << weight_to_use << endl; 

  TFile *file0 = TFile::Open(file_name);
  TFile *file02 = TFile::Open(file_name_02);

  TTree*  tree = (TTree*) file0->Get("tree");
  TH1F *h_data = new TH1F("h_data", "", nBins, mHlow, mHup);
  TH1F *h1 = new TH1F("h1","title",100,-1,2);
  float reco_weight=1;
  tree->SetBranchAddress(weight_to_use, &reco_weight);  
  for(int i=0; i<tree->GetEntries(); i++)
    {
      tree->GetEntry(i); 
      h1->Fill(reco_weight);
    }
  if(h1->GetMean()<1e-10){
    cout<< "===========  Too small weight "<< h1->GetMean()<<"  ============" << endl;
    return;
  }
  Double_t Nentr= tree->GetEntries();


  //Load datacard
  auto hgg = HftModelBuilder::Create("hgg", datacard_input);
  auto hgg_02 = HftModelBuilder::Create("hgg", datacard_input_02);

  //Open file to fit
  hgg->Dependent("mgg")->setBins(nBins); 
  hgg_02->Dependent("mgg")->setBins(nBins);  
 
  //modify datacard on-the-fly to set input mass
  hgg->Dependent("mgg")->setRange(mHlow, mHup);
  hgg_02->Dependent("mgg")->setRange(mHlow, mHup);

  hgg->Var("mX")->setVal(mH);
  hgg_02->Var("mX")->setVal(mH);

  cout<< " =======================     !!!!!!!!!!!!!!!!!!!!    ==================  " <<endl;  
  cout<< "                      File_I       " <<file_name<<" " <<endl; 
  cout<< "                      File_II       " <<file_name_02<<" " <<endl;  
  cout<< "                      dc_I       " <<datacard_input<<" " <<endl;
  cout<< "                      dc_II       " <<datacard_input_02<<" " <<endl;  
  
  cout<< "                      dc_O       " <<datacard_output<<" " <<endl;  

  //open datacard
  auto sigData = HftData::Open(file_name, "tree", *hgg, "", weight_to_use);
  auto sigData_02 = HftData::Open(file_name_02, "tree", *hgg_02, "", weight_to_use);

  //Fit and save output datacard
  hgg->FitData( *sigData, "W+"); //W+ take in account weights
  hgg_02->FitData( *sigData_02, "W+"); //W+ take in account weights

  hgg->FreeParameters().Print("V");
  hgg_02->FreeParameters().Print("V");

  /*
  //Add protaction against hit the boundary fit: 
  Double_t var_val=0, var_min=0, var_max=0, vars_size=0,fit_state=1;
  vars_size =  hgg->FreeParameters().getSize();
  RooRealVar* valM0;

  for(int it=1; it<8; it++){
    valM0=(RooRealVar*)hgg->Parameters().at(it);
    var_val = valM0->getVal();
    var_max = valM0->getMax();
    var_min = valM0->getMin();
    cout<< "######   var: " << var_val << ", min: "<< var_min << " - max "<< var_max<< endl;
    if(var_val<var_min*1.05 || var_val>var_max*0.95){
      cout<< "Par "<< it << " hits boundary! "<< endl;
      fit_state=0;
      break;
    }
  }
  
  if(fit_state==1){    
    //hgg->WriteCard(datacard_output);
  }
  else cout<< "Parameter hit the bound, redo the fit!!! " << endl;
  */
  TCanvas *callpt = new TCanvas("callpt", "", 30, 60, 1000, 1000);
  TPad *pad1 = new TPad("pad1", "pad1", 0, 0.29, 1, 1);
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.33);
  pad1->SetBottomMargin(0.11);
  pad1->SetBorderMode(0);
  pad2->SetTopMargin(0.017);
  pad2->SetBottomMargin(0.25);
  pad2->SetBorderMode(0);
  pad1->Draw();
  pad2->Draw();


  pad1->cd();
  //gPad->SetLogy();
  tag.SetTextSize(0.04);
  auto  p_02=hgg_02->Plot("mgg", *sigData_02, "SAME");
  auto  p=hgg->Plot("mgg", *sigData);

  RooHist* hdata20 = p->getHist("mgg_data");
  RooHist* hdata20_02 = p_02->getHist("mgg_data");
  RooCurve* hbkg = p->getCurve("mgg_pdf_Background");
  RooCurve* hbkg_02 = p_02->getCurve("mgg_pdf_Background");
  //p->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  p->GetXaxis()->SetTitle("m_{X} [GeV]");
  p->SetMinimum(0.01);
  p->SetLineColor(kRed);
  p->SetFillColor(kRed);
  p->SetMarkerColor(kRed); 
  //p_02->SetLineColor(kRed);
  //p_02->SetFillColor(kRed);
  hbkg->SetLineWidth(0);
  hdata20->SetMarkerSize(1.2);
  //hdata20_02->SetMarkerSize(1.2);
  ATLASLabel(0.2,0.85,"Simulation",1,0.05);
  
  tag.DrawLatex(0.2, 0.77, label_plot);  
  tag.DrawLatex(0.2, 0.72, label_plot1);  
  
  pad2->cd();
  RooHist* hresid = p->residHist("mgg_data", "mgg_pdf",true);
  RooHist* hdata = p->getHist("mgg_data");
  auto pp = hgg->Dependent("mgg")->frame();
  pp->addPlotable(hresid, "p");
  pp->SetTitle("");
  pp->GetYaxis()->SetLabelSize(0.09);
  pp->GetXaxis()->SetLabelSize(0.01);
  pp->GetXaxis()->SetTitleSize(0.01);
  pp->GetXaxis()->SetLabelOffset(1);
  
  const unsigned  int npoints = hresid->GetN();
  
  //TH1F* h_res = new TH1F("h_res", "", nBins, mHlow, mHup);
  Float_t res_val[npoints],res_err[npoints],mass_val[npoints],mass_err[npoints];
  for (int i = 0; i < npoints; i++) 
    {
      res_val[i] = hresid->GetY()[i ];
      res_err[i] = hresid->GetErrorY(i ); 
      mass_val[i] = hresid->GetX()[i ];
      mass_err[i] = hresid->GetErrorX(i ); 
    }
  
  TGraphErrors*  h_rat = new TGraphErrors(npoints,mass_val,res_val,mass_err,res_err );
  h_rat->SetMarkerColor(1);
  h_rat->SetMarkerSize(0.3);
  h_rat->SetLineColor(1);
  h_rat->GetYaxis()->SetLabelSize(0.08);
  h_rat->GetXaxis()->SetLabelSize(0.08);
  h_rat->GetXaxis()->SetTitleSize(0.08);
  h_rat->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  h_rat->GetYaxis()->SetTitle("Pull Distribution");
  h_rat->GetYaxis()->SetTitleSize(0.08);
  h_rat->GetYaxis()->SetTitleOffset(0.6);
  h_rat->GetYaxis()->SetRangeUser(-5, 5);
  h_rat->GetXaxis()->SetRangeUser(mHlow, mHup);

  TLine *l = new TLine(mHlow, 0, mHup, 0);
  l->SetLineColor(kBlue);
  h_rat->Draw();
  l->Draw();
  callpt->Print(outname_plot);   
  
  float Norm=0;
  if(nice_plot==1){
    Norm=1/Nentr;
    cout<< " Norm " <<Norm << " (N="<<Nentr<<")" << endl;
    callpt = new TCanvas("callpt", "", 30, 60, 800, 600);
    
    p=hgg->Plot("mgg", *sigData,Form("PlotScale(Relative,%g ):DataScale(%g )",Norm,Norm));
    hdata20 = p->getHist("mgg_data");
    hbkg = p->getCurve("mgg_pdf_Background");    
    RooCurve* hfitcurve = p->getCurve("mgg_pdf_Signal");
    hbkg->SetLineWidth(0);
    hdata20->SetMarkerSize(1);
    
    int n = hfitcurve->GetN(); double* y_ax = hfitcurve->GetY(); double* x_ax = hfitcurve->GetX(); int locmax = TMath::LocMax(n,y_ax);
    Double_t max = TMath::MaxElement(n,y_ax);

    tag.SetTextSize(0.05);

    cout<< "max  "<<     max <<endl;
    p->SetMaximum(max*1.35); //NWA  
    p->SetMinimum(0);
    p->GetXaxis()->SetTitleSize(0.06);
    p->GetYaxis()->SetTitleSize(0.06);
    p->GetYaxis()->SetTitleOffset(1.2);
    p->GetXaxis()->SetTitleOffset(1.1);
    p->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
    p->GetYaxis()->SetTitle("1/N dN/dm_{#gamma#gamma} / 1 GeV");
    ATLASLabel(0.2,0.85,"Simulation Internal",1,0.05);
    //ATLASLabel(0.2,0.85,"Simulation",1,0.05);
    

    label_plot="#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma";
    tag.DrawLatex(0.2, 0.78, label_plot);
    tag.DrawLatex(0.2, 0.71, "Spin-0 Selection");

    tag.SetTextSize(0.045);
    label_plot="m_{X} = "+point+" GeV";
    tag.DrawLatex(0.2, 0.64, label_plot);
    label_plot="#Gamma_{X}/m_{X} = "+width_val+"%";
    if(width_val=="0"){
      label_plot="#Gamma_{X} = 4 MeV";}
    tag.DrawLatex(0.2, 0.57, label_plot);
  }
  

}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
    
    p.DrawLatex(x+delx*0.7,y,text);
    //p.DrawLatex(x+delx*1.1,y,text);
    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}
