/*=============================================================*/
/* Plot parameters of Signal Parametrization obtained from     */
/* single and multiple fits                                    */
/* Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>     */
/*=============================================================*/

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TCanvas.h"
#include "TString.h"
#include "TF1.h"
#include <iostream>
#include <fstream>

//#include "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/AtlasStyle.C"
//#include "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/AtlasUtils.C"

void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);

using namespace std;

void variation_old(Int_t prodIt=0, Int_t all_steps=0, TString region = "") {

  gROOT->Reset();
  gROOT->LoadMacro("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/SP_run2/AtlasStyle.C");
  gROOT->LoadMacro("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/SP_run2/AtlasUtils.C");

  SetAtlasStyle();
  gStyle->SetPalette(1);

  TString prod_name[5] = { "MG", "VBFH", "ttH", "ZH", "WH" };
  TString syst_type[2] = { "Resolution","Scale_ALL" };
  TString syst_way[2] = { "Up","Down" };

  /*================================================================================*/
  /*                             Multiple Fit Values                                */
  /*================================================================================*/
  Float_t par0, par1, par2;
  Int_t formNum=0, wayIt=0, typeIt=0, counter=0;
  TCanvas* canv_sCB = new TCanvas("sCB", "sCB",10,10,800,600);
  TCanvas* canv_dM = new TCanvas("dM", "dM",10,10,800,600);
  TCanvas* canv_FitParams = new TCanvas("FitParams", "FitParams",10,610,800,600);
  canv_FitParams->Divide(2,2);

  //for( prodIt=0; prodIt<5; prodIt++)

  //original:
  TF1* fa[20];
  char  formula[128], formula_name[128];
  TString file_name_multiple = "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/output/MultipleFitFormulas_"+prod_name[prodIt]+".txt";
  const char *name_multiple = file_name_multiple.Data();
  std::cout<< name_multiple << endl;
  ifstream myfile (name_multiple);

  TString list_param_multiple = "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/output/MultipleParameters_"+prod_name[prodIt]+region+".txt";
  const char *param_multiple = list_param_multiple.Data();
  std::cout<< param_multiple << endl;
	  
  ifstream vInput;
  vInput.open(param_multiple);

  int mass_range_max=2200 , mass_range_min=40;
  int fit_range_max , fit_range_min;
  for(int formIt=0;formIt<4;formIt++)
    { 
      vInput >> par0 >> par1 >> par2;
      myfile.getline(formula,128,'\n'); 
      cout<< " ----------------- FORMULA ---------------"<<endl;
      cout<< formula<<endl;
      cout<< "p0: "<< par0<< "; p1: "<< par1<< "; p2: "<< par2 <<endl;
      sprintf(formula_name,"form_%01d",formNum);      
      cout<< "---------name:  " << formula_name<<endl;
      fa[formNum] = new TF1(formula_name,formula,mass_range_min,mass_range_max);
      fa[formNum]->SetParameter(0, par0);
      fa[formNum]->SetParameter(1, par1);
      fa[formNum]->SetParameter(2, par2);
      formNum++;
    }
  /*
  canv_FitParams->cd(1);
  fa[0]->SetMinimum(-20);
  fa[0]->SetMaximum(18);
  fa[0]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[0]->GetYaxis()->SetTitle("#Delta m_{H} [GeV]");
  fa[0]->Draw();

  canv_FitParams->cd(2);
  fa[1]->SetMinimum(0);
  //fa[1]->SetMaximum(13);
  fa[1]->SetMaximum(40);
  fa[1]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[1]->GetYaxis()->SetTitle("#sigma_{CB} [GeV]");
  fa[1]->Draw();

  canv_FitParams->cd(3);
  fa[2]->SetMinimum(0.5);
  fa[2]->SetMaximum(2.4);
  fa[2]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[2]->GetYaxis()->SetTitle("#alpha_{Low}");
  fa[2]->Draw();

  canv_FitParams->cd(4);
  fa[3]->SetMinimum(0.8);
  fa[3]->SetMaximum(2.8);
  fa[3]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[3]->GetYaxis()->SetTitle("#alpha_{High}");
  fa[3]->Draw();


  //*/
  canv_sCB->cd();
  fa[1]->SetMinimum(0);
  //fa[1]->SetMaximum(13);
  fa[1]->SetMaximum(40);
  fa[1]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[1]->GetYaxis()->SetTitle("#sigma_{CB} [GeV]");
  fa[1]->Draw();

  counter=1;
  //formNum=3;
  //SYSTEMATIC      
      cout<< " !!!!!!!!!!!----------------- SYSTEMATIC --------------- !!!!!!!!"<<endl;

  for( typeIt=0; typeIt<2; typeIt++)
    {
      for( wayIt=0; wayIt<2; wayIt++)
	{
	  //TF1* fa[20];
	  //char  formula_name[128];
	  TString line;//MultipleParameters_MG_Resolution_Down.txt
	  TString file_name_multiple = "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/output/MultipleFitFormulas_"+prod_name[prodIt]+".txt";
	  //TString file_name_multiple = "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/output/MultipleFitFormulas_"+prod_name[prodIt]+"_"+syst_type[typeIt]+"_"+syst_way[wayIt]+".txt";
	  const char *name_multiple = file_name_multiple.Data();
	  std::cout<< name_multiple << endl;
	  ifstream myfile (name_multiple);


	  TString list_param_multiple = "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/output/MultipleParameters_"+prod_name[prodIt]+"_"+syst_type[typeIt]+"_"+syst_way[wayIt]+".txt";
	  const char *param_multiple = list_param_multiple.Data();
	  std::cout<< param_multiple << endl;
	  
	  ifstream vInput;
	  vInput.open(param_multiple);

	  int mass_range_max=2600 , mass_range_min=0;
	  int fit_range_max , fit_range_min;
	  for(int formIt=0;formIt<4;formIt++)
	    { 
	      vInput >> par0 >> par1 >> par2;
	      myfile.getline(formula,128,'\n'); 
	      cout<< " ----------------- FORMULA ---------------"<<endl;
	      cout<< formula<<endl;
	      cout<< "p0: "<< par0<< "; p1: "<< par1<< "; p2: "<< par2 <<endl;
	      sprintf(formula_name,"form_%01d",formNum);      
	      fa[formNum] = new TF1(formula_name,formula,mass_range_min,mass_range_max);
	      cout<< "sys:  " << syst_type[typeIt] << ", " << syst_way[wayIt] <<endl;

	      cout<< "---------nameForm:  " << formula_name<<endl;
	      fa[formNum]->SetParameter(0, par0);
	      fa[formNum]->SetParameter(1, par1);
	      fa[formNum]->SetParameter(2, par2);
	      formNum++;
	    }

	  


	  canv_FitParams->cd(1);
	  if(counter==0)
	    {
	      fa[0]->SetMinimum(-1.5);
	      fa[0]->SetMaximum(1.5);
	      fa[0]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
	      fa[0]->GetYaxis()->SetTitle("#Delta m_{H} [GeV]");
	      fa[0]->Draw();
	    }
	  fa[0+4*counter]->SetLineColor(counter+1);
	  fa[0+4*counter]->Draw("same");

	  canv_FitParams->cd(2);
	  if(counter==0)
	    {
	      fa[1]->SetMinimum(0);
	      fa[1]->SetMaximum(40);
	      fa[1]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
	      fa[1]->GetYaxis()->SetTitle("#sigma_{CB} [GeV]");
	      fa[1]->Draw();
	    }
	  fa[1+4*counter]->SetLineColor(counter+1);
	  fa[1+4*counter]->Draw("same");

	  canv_FitParams->cd(3);
	  if(counter==0)
	    {
	      fa[2]->SetMinimum(0.5);
	      fa[2]->SetMaximum(2.4);
	      fa[2]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
	      fa[2]->GetYaxis()->SetTitle("#alpha_{Low}");
	      fa[2]->Draw();
	    }
	  fa[2+4*counter]->SetLineColor(counter+1);
	  fa[2+4*counter]->Draw("same");


	  canv_FitParams->cd(4);
	  if(counter==0)
	    {
	      fa[3]->SetMinimum(0.8);
	      fa[3]->SetMaximum(2.8);
	      fa[3]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
	      fa[3]->GetYaxis()->SetTitle("#alpha_{High}");
	      fa[3]->Draw();
	    }
	  fa[3+4*counter]->SetLineColor(counter+1);
	  fa[3+4*counter]->Draw("same");

	  counter++;
	  


	}//loop over way - up or down
    }//loop over type of systematic scale or resolution




  for(int i=0; i<4; i++)
    {
      canv_FitParams->cd(i+1);
      sprintf(formula_name,"form_%01d",i);      

      TLegend* leg = new TLegend(0.3, 0.72, 0.5, 0.87); 
      if(i>1){
	leg = new TLegend(0.3, 0.22, 0.5, 0.37); 
      }
      leg->SetBorderSize(0);
      leg->SetFillColor(0);
      leg->SetTextSize(0.04);
      leg->AddEntry(fa[0], "Nominal", "l");           
      leg->AddEntry(fa[4], "ResUp", "l");           
      leg->AddEntry(fa[8], "ResDown", "l");           
      leg->AddEntry(fa[12], "ScaleUp", "l");           
      leg->AddEntry(fa[16], "ScaleDown", "l");           
      //leg->AddEntry(fa[16], "WH", "l");           
      leg->Draw("same");


    }

  //canv_FitParams->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/plots/variation.pdf");
  //canv_FitParams->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/output/variation.C");

  mass_range_min=40;

  //sigmaCB separately
  canv_sCB->cd();


  TPad *pad1 = new TPad("pad1", "pad1", 0, 0, 1, 1.0);
  pad1->SetBottomMargin(0.415); // Upper and lower plot are joined
  pad1->Draw();             // Draw the upper pad: pad1
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.4);
  pad2->SetTopMargin(0);
  pad2->SetBottomMargin(0.2);
  pad2->Draw();
  pad1->cd();               // pad1 becomes the current pad

  fa[1]->SetMinimum(0);
  fa[1]->SetMaximum(35);
  //fa[1]->GetXaxis()->SetRangeUser(0,mass_range_max);
  fa[1]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[1]->GetYaxis()->SetTitle("#sigma_{CB} [GeV]");
  fa[1]->Draw();
  fa[1]->GetXaxis()->SetLabelSize(0.);
  fa[1]->GetYaxis()->SetLabelSize(0.035);
	  
  fa[5]->SetLineColor(2);
  fa[5]->Draw("same");
  fa[9]->SetLineColor(3);
  fa[9]->Draw("same");

  cout<< " ==================================================================== sigma  200 = "<<  fa[1]->Eval(200) << "  500 = "<<  fa[1]->Eval(500)   << "  700 = "<<  fa[1]->Eval(700)  << "  2000 = "<<  fa[1]->Eval(2000)   << endl;

  TLatex tag;
  tag.SetNDC(); // setting coordinates
  tag.SetTextSize(0.3);
  
  ATLASLabel(0.2,0.87,"Simulation Internal",1,0.04);
  tag.SetTextSize(0.04);
  label_plot="#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma";
  tag.DrawLatex(0.2, 0.82, label_plot);
  tag.DrawLatex(0.2, 0.77, "#Gamma_{X} = 4 MeV");
  tag.DrawLatex(0.2, 0.72, "MadGraph5 samples");
  tag.DrawLatex(0.2, 0.67, "Spin-0 Selection");


  TLegend* leg = new TLegend(0.6, 0.77, 0.8, 0.87); 
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  leg->SetTextSize(0.04);
  //leg->AddEntry(fa[0], "MC15B, Higgs cuts, NWA","");           
  leg->AddEntry(fa[1], "Nominal", "l");           
  leg->AddEntry(fa[4], "Resolution Up", "l");           
  leg->AddEntry(fa[8], "Resolution Down", "l");           
  leg->Draw("same");
  gPad->Update(); 

  pad2->cd();  

  //TF1* fa[20];

  TF1* f_nom = new TF1("nom","(form_1-form_1)/form_1",mass_range_min,mass_range_max);
  //TF1* f_nom = new TF1("nom",fa[1],mass_range_min,mass_range_max);
  //TF1 *f_nom0 =(TF1*)fa[1]->Clone("f_nom0");
  //TF1* f_nom = new TF1("nom","(f_nom0-f_nom0)/f_nom0",mass_range_min,mass_range_max);
  f_nom->SetMinimum(-0.6);
  f_nom->SetMaximum(0.6);
  f_nom->Draw();
  //f_nom->Print("all");

  f_nom->GetYaxis()->SetTitle("relative difference #sigma_{CB}");
  f_nom->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  f_nom->GetXaxis()->SetLabelSize(0.09);
  f_nom->GetYaxis()->SetLabelSize(0.09);

  f_nom->GetXaxis()->SetTitleOffset(0.9);
  f_nom->GetYaxis()->SetTitleOffset(0.7);

  f_nom->GetXaxis()->SetTitleSize(0.09);
  f_nom->GetYaxis()->SetTitleSize(0.09);


  TF1* f_up = new TF1("up","(form_5-form_1)/form_1",mass_range_min,mass_range_max);

  //TF1* f_up = new TF1("up","form_5",mass_range_min,mass_range_max);
  f_up->SetLineColor(2);
  f_up->Draw("same");

  //TF1* f_down = new TF1("down","form_9",mass_range_min,mass_range_max);
  
  TF1* f_down = new TF1("down","(form_9-form_1)/form_1",mass_range_min,mass_range_max);
  f_down->SetLineColor(3);
  f_down->Draw("same");

  cout<< " ===============  RESOLUTION h015 ============ +- 200 = "<<  form_1->Eval(200) << " +- " <<  f_up->Eval(200)*100<< " / "<< f_down->Eval(200)*100 << "  500 = "<< form_1->Eval(500) << " +- " <<  f_up->Eval(500)*100<< " / "<< f_down->Eval(500)*100   << "  700 = "<<form_1->Eval(700) << " +- " <<  f_up->Eval(700)*100<< " / "<< f_down->Eval(700)*100  << "  2000 = "<<  form_1->Eval(2000) << " +- " << f_up->Eval(2000)*100<< " / "<< f_down->Eval(2000)*100 << endl;

  canv_sCB->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/plots/Res_Variation.pdf");
  //canv_sCB->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/plots/Res_Variation.C");
  //////////////

  //energy scale
  //
  //*
  canv_dM->cd();

  TPad *padS1 = new TPad("padS1", "padS1", 0, 0, 1, 1.0);
  padS1->SetBottomMargin(0.415); // Upper and lower plot are joined
  padS1->Draw();             // Draw the upper padS: padS1
  padS1->cd();               // padS1 becomes the current padS

  fa[0]->SetMinimum(-25);
  fa[0]->SetMaximum(25);
  fa[0]->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fa[0]->GetYaxis()->SetTitle("#Delta m_{H} [GeV]");
  fa[0]->Draw();
  fa[0]->GetXaxis()->SetLabelSize(0);
  fa[0]->GetYaxis()->SetLabelSize(0.035);
	  
  fa[12]->SetLineColor(4);
  fa[12]->Draw("same");
  fa[16]->SetLineColor(kYellow-3);
  fa[16]->Draw("same");

  ATLASLabel(0.2,0.88,"Simulation Internal",1,0.04);
  tag.SetTextSize(0.04);
  label_plot="#sqrt{s} = 13 TeV, X #rightarrow #gamma #gamma, #Gamma_{X} = 4 MeV";
  tag.DrawLatex(0.2, 0.83, label_plot);
  //tag.DrawLatex(0.2, 0.77, "#Gamma_{X} = 4 MeV");
  tag.DrawLatex(0.2, 0.78, "MadGraph5 samples");
  tag.DrawLatex(0.2, 0.73, "Spin-0 Selection");

  TLegend* leg_scale = new TLegend(0.2, 0.43, 0.4, 0.62); 
  leg_scale->SetBorderSize(0);
  leg_scale->SetFillColor(0);
  leg_scale->SetTextSize(0.04);
  //leg_scale->AddEntry(fa[0], "MC15B, Higgs cuts, NWA","");           
  leg_scale->AddEntry(fa[0], "Nominal", "l");           
  leg_scale->AddEntry(fa[12], "Scale Up", "l");           
  leg_scale->AddEntry(fa[16], "Scale Down", "l");           
  leg_scale->Draw("same");
  gPad->Update(); 

  TPad *padS2 = new TPad("padS2", "padS2", 0, 0, 1, 0.4);
  padS2->SetTopMargin(0);
  padS2->SetBottomMargin(0.2);
  padS2->Draw();
  padS2->cd();  

  //TF1* fa[20];

  TF1* fs_nom = new TF1("nom","(form_1-form_1)/form_1",mass_range_min,mass_range_max);
  fs_nom->SetMinimum(-0.014);
  fs_nom->SetMaximum(0.014);
  fs_nom->Draw();

  fs_nom->GetYaxis()->SetTitle("relative difference #Delta m_{H} ");
  fs_nom->GetXaxis()->SetTitle("m_{#gamma #gamma} [GeV]");
  fs_nom->GetXaxis()->SetLabelSize(0.09);
  fs_nom->GetYaxis()->SetLabelSize(0.09);

  fs_nom->GetXaxis()->SetTitleOffset(0.9);
  fs_nom->GetYaxis()->SetTitleOffset(0.7);

  fs_nom->GetXaxis()->SetTitleSize(0.09);
  fs_nom->GetYaxis()->SetTitleSize(0.09);

  //scale up dM
  //cout << "Scale up:     " << form_12 << endl;
  TF1* fs_up = new TF1("fs_up","(form_12-form_0)/x",mass_range_min,mass_range_max);
  //TF1* fs_up = new TF1("up","form_16",mass_range_min,mass_range_max);
  fs_up->SetLineColor(4);
  fs_up->Draw("same");

  //scale down dM
  TF1* fs_down = new TF1("fs_down","(form_16-form_0)/x",mass_range_min,mass_range_max);
  fs_down->SetLineColor(kYellow-3);
  fs_down->Draw("same");

  cout<< " ++++++++++++++++++++++ SCALE h015 ++++++++++++++++= +- 200 = "<< form_0->Eval(200) << " +- " << fs_up->Eval(200)*100<< " / "<< fs_down->Eval(200)*100 << "  500 = "<<   form_0->Eval(500) << " +- " << fs_up->Eval(500)*100<< " / "<< fs_down->Eval(500)*100   << "  700 = "<<  form_0->Eval(700) << " +- " << fs_up->Eval(700)*100<< " / "<< fs_down->Eval(700)*100  << "  2000 = "<<   form_0->Eval(2000) << " +- " << fs_up->Eval(2000)*100<< " / "<< fs_down->Eval(2000)*100 << endl;


  canv_dM->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/plots/Scale_Variation.pdf");
  //canv_dM->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/165504/Systematics/output/Scale_Variation.C");
  //*/

}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
    p.DrawLatex(x+delx*0.85,y,text);
    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}
