#!/bin/bash
# collect Single Fit values
# Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>  

VarType="$1" 
VarWay="$2" 
prod="MG"

cd ../../../../datacards/LowHighMassRun2/SP/165504/Systematics/output
file=SingleFitVal_$prod
file+='_'
file+=$VarType
file+='_'
file+=$VarWay
file+='.txt'
rm $file

FILES=single_*$prod*$VarType*$VarWay*
#for f in $FILES 
for f in $(ls -1v single_*$prod*$VarType*$VarWay* )
do
    #echo "Processing $f file..."
    search_E=${f#*$prod}
    point=${search_E%%'_'*}
    #echo $point

    # find line containing    

    # grab dmX value with eror
    dmX=`grep -r 'dmX' $f`
    #echo $dmX
    if [[ $dmX == *"dmX ="* ]]
    then
	dmX_val_raw=${dmX#*'dmX = '*}
	dmX_val=${dmX_val_raw%%' +/-'*}
	dmX_err_raw=${dmX#*'+/- '*}
	dmX_err=${dmX_err_raw%%' L('*}
    fi
    
    # grab cbSigma value with eror
    cbSigma=`grep -r 'cbSigma' $f`
    #echo $cbSigma
    if [[ $cbSigma == *"cbSigma ="* ]]
    then
	cbSigma_val_raw=${cbSigma#*'cbSigma = '*}
	cbSigma_val=${cbSigma_val_raw%%' +/-'*}
	cbSigma_err_raw=${cbSigma#*'+/- '*}
	cbSigma_err=${cbSigma_err_raw%%' L('*}
    fi

    # grab cbAlphaLo value with eror
    cbAlphaLo=`grep -r 'cbAlphaLo' $f`
    #echo $cbAlphaLo
    if [[ $cbAlphaLo == *"cbAlphaLo ="* ]]
    then
	cbAlphaLo_val_raw=${cbAlphaLo#*'cbAlphaLo = '*}
	cbAlphaLo_val=${cbAlphaLo_val_raw%%' +/-'*}
	cbAlphaLo_err_raw=${cbAlphaLo#*'+/- '*}
	cbAlphaLo_err=${cbAlphaLo_err_raw%%' L('*}
    fi

    # grab cbAlphaHi value with eror
    cbAlphaHi=`grep -r 'cbAlphaHi' $f`
    #echo $cbAlphaHi
    if [[ $cbAlphaHi == *"cbAlphaHi ="* ]]
    then
	cbAlphaHi_val_raw=${cbAlphaHi#*'cbAlphaHi = '*}
	cbAlphaHi_val=${cbAlphaHi_val_raw%%' +/-'*}
	cbAlphaHi_err_raw=${cbAlphaHi#*'+/- '*}
	cbAlphaHi_err=${cbAlphaHi_err_raw%%' L('*}
    fi
	echo "point: " $point "dm: "$dmX_val " +- " $dmX_err " sig: " $cbSigma_val " +- " $cbSigma_err  " alphaLow: " $cbAlphaLo_val " +- " $cbAlphaLo_err  " alphaHigh: " $cbAlphaHi_val " +- " $cbAlphaHi_err

    echo  $point $'\t' $dmX_val $'\t' $dmX_err $'\t' $cbSigma_val $'\t' $cbSigma_err $'\t' $cbAlphaLo_val $'\t' $cbAlphaLo_err  $'\t' $cbAlphaHi_val $'\t' $cbAlphaHi_err  >> $file

done
