#!/bin/bash
# grab formulas from Multiple Fit
# Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>  

#=========================================================================#
#                             Description                                 #
# f=a*x + b
# 1) read output datacard to find values of function constants: 
#                                                            a,b
# 2) read input datacard, to get formulas which used for parametrization, save them to Formulas_$prod.txt to fit single points in "plotSignalParametrization.C":
#                                                            f_i=a_i*x+ b_i 
# 3) exchange constants in formulas with fitted values - MultipleFitFormulas_$prod.txt
#=========================================================================#


VarType="$1" 
VarWay="$2" 
prod="MG"

cd ../../../../datacards/LowHighMassRun2/SP/165504/Systematics/output
file=MultipleParameters_$prod
file+='_'
file+=$VarType
file+='_'
file+=$VarWay
file+='.txt'

varname=$VarType
varname+=$VarWay
varname+='_'
varname+=$prod

FILES=multiple_$varname.dat
for f in $FILES 
#for f in $(ls -1v *$prod* )
do
    echo "Processing $f file..."
    #search_E=${f#*$prod}
    #point=${search_E%%'.dat'*}
    #echo $point

    # grab mCBpar0 value with eror
    mCBpar0=`grep -r 'mCBpar0' $f`
    if [[ $mCBpar0 == *"mCBpar0 ="* ]]
    then
	mCBpar0_val_raw=${mCBpar0#*'mCBpar0 = '*}
	mCBpar0_val=${mCBpar0_val_raw%%' +/-'*}
	mCBpar0_err_raw=${mCBpar0#*'+/- '*}
	mCBpar0_err=${mCBpar0_err_raw%%' L('*}
    fi

    # grab mCBpar1 value with eror
    mCBpar1=`grep -r 'mCBpar1' $f`
    if [[ $mCBpar1 == *"mCBpar1 ="* ]]
    then
	mCBpar1_val_raw=${mCBpar1#*'mCBpar1 = '*}
	mCBpar1_val=${mCBpar1_val_raw%%' +/-'*}
	mCBpar1_err_raw=${mCBpar1#*'+/- '*}
	mCBpar1_err=${mCBpar1_err_raw%%' L('*}
    fi


    # grab mCBpar2 value with eror
    mCBpar2=`grep -r 'mCBpar2' $f`
    if [[ $mCBpar2 == *"mCBpar2 ="* ]]
    then
	mCBpar2_val_raw=${mCBpar2#*'mCBpar2 = '*}
	mCBpar2_val=${mCBpar2_val_raw%%' +/-'*}
	mCBpar2_err_raw=${mCBpar2#*'+/- '*}
	mCBpar2_err=${mCBpar2_err_raw%%' L('*}
    fi

    # grab sCBpar0 value with eror
    sCBpar0=`grep -r 'sCBpar0' $f`
    if [[ $sCBpar0 == *"sCBpar0 ="* ]]
    then
	sCBpar0_val_raw=${sCBpar0#*'sCBpar0 = '*}
	sCBpar0_val=${sCBpar0_val_raw%%' +/-'*}
	sCBpar0_err_raw=${sCBpar0#*'+/- '*}
	sCBpar0_err=${sCBpar0_err_raw%%' L('*}
    fi

    # grab sCBpar1 value with eror
    sCBpar1=`grep -r 'sCBpar1' $f`
    if [[ $sCBpar1 == *"sCBpar1 ="* ]]
    then
	sCBpar1_val_raw=${sCBpar1#*'sCBpar1 = '*}
	sCBpar1_val=${sCBpar1_val_raw%%' +/-'*}
	sCBpar1_err_raw=${sCBpar1#*'+/- '*}
	sCBpar1_err=${sCBpar1_err_raw%%' L('*}
    fi

    # grab aLopar0 value with eror
    aLopar0=`grep -r 'aLopar0' $f`
    if [[ $aLopar0 == *"aLopar0 ="* ]]
    then
	aLopar0_val_raw=${aLopar0#*'aLopar0 = '*}
	aLopar0_val=${aLopar0_val_raw%%' +/-'*}
	aLopar0_err_raw=${aLopar0#*'+/- '*}
	aLopar0_err=${aLopar0_err_raw%%' L('*}
    fi

    # grab aLopar1 value with eror
    aLopar1=`grep -r 'aLopar1' $f`
    if [[ $aLopar1 == *"aLopar1 ="* ]]
    then
	aLopar1_val_raw=${aLopar1#*'aLopar1 = '*}
	aLopar1_val=${aLopar1_val_raw%%' +/-'*}
	aLopar1_err_raw=${aLopar1#*'+/- '*}
	aLopar1_err=${aLopar1_err_raw%%' L('*}
    fi

    # grab aLopar2 value with eror
    aLopar2=`grep -r 'aLopar2' $f`
    if [[ $aLopar2 == *"aLopar2 ="* ]]
    then
	aLopar2_val_raw=${aLopar2#*'aLopar2 = '*}
	aLopar2_val=${aLopar2_val_raw%%' +/-'*}
	aLopar2_err_raw=${aLopar2#*'+/- '*}
	aLopar2_err=${aLopar2_err_raw%%' L('*}
    fi
    if [[ $aLopar2 = "" ]]
    then
	aLopar2_val="0"
	aLopar2_err="0"
    fi

    # grab aHipar0 value with eror
    aHipar0=`grep -r 'aHipar0' $f`
    if [[ $aHipar0 == *"aHipar0 ="* ]]
    then
	aHipar0_val_raw=${aHipar0#*'aHipar0 = '*}
	aHipar0_val=${aHipar0_val_raw%%' +/-'*}
	aHipar0_err_raw=${aHipar0#*'+/- '*}
	aHipar0_err=${aHipar0_err_raw%%' L('*}
    fi

    # grab aHipar1 value with eror
    aHipar1=`grep -r 'aHipar1' $f`
    if [[ $aHipar1 == *"aHipar1 ="* ]]
    then
	aHipar1_val_raw=${aHipar1#*'aHipar1 = '*}
	aHipar1_val=${aHipar1_val_raw%%' +/-'*}
	aHipar1_err_raw=${aHipar1#*'+/- '*}
	aHipar1_err=${aHipar1_err_raw%%' L('*}
    fi

    #change formula for alphaHi, beed to add one more parameter
    # grab aHipar1 value with eror
    aHipar2=`grep -r 'aHipar2' $f`
    if [[ $aHipar2 == *"aHipar2 ="* ]]
    then
	aHipar2_val_raw=${aHipar2#*'aHipar2 = '*}
	aHipar2_val=${aHipar2_val_raw%%' +/-'*}
	aHipar2_err_raw=${aHipar2#*'+/- '*}
	aHipar2_err=${aHipar2_err_raw%%' L('*}
    fi
    if [[ $aHipar2 = "" ]]
    then
	aHipar2_val="0"
	aHipar2_err="0"
    fi

     echo   $mCBpar0_val $'\t'  $mCBpar1_val $'\t' $mCBpar2_val $'\n'   $sCBpar0_val  $'\t' $sCBpar1_val$'\t' " 0 " $'\n' $aLopar0_val $'\t' $aLopar1_val $'\t' $aLopar2_val  $'\n' $aHipar0_val  $'\t' $aHipar1_val $'\t' $aHipar2_val  > $file

done
