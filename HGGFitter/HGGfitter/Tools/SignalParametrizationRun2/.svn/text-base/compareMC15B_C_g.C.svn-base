/*=============================================================*/
/* compare geo20 and geo21 for Run1 and  impact of calibration */
/* Supported by: Kirill Grevtsov <kirill.grevtsov@cern.ch>     */
/*=============================================================*/

#include "TString.h"
#include "TCanvas.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
//#include "TCanvas.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TLine.h"

#include "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/AtlasStyle.C"
#include "/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/AtlasUtils.C"

void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);
double FWHM(TGraph* tg, double xmin = 110, double xmax = 140) ;

void compareMC15B_C_g(double mH = 600){

  gROOT->Reset();
  SetAtlasStyle();
  gStyle->SetPalette(1);

  TString point;
  point.Form("%1.0f",mH);
  
  TString test, str, file_name15B, datacard_input15B, datacard_output15B, file_name15C, datacard_input15C, datacard_output15C;
  //TString weight_to_use="weight";
  TString weight_to_use="weightN";
  TString label_plot, label_plot_type,indir,   outname_plot,   datacard_path;
  
  TLatex tag;
  tag.SetNDC(); // setting coordinates
  tag.SetTextSize(0.3);
  
  
  datacard_path="../datacards/LowHighMassRun2/SP/";
  indir = "/afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/";
  file_name15B= indir + "MC162220/HGGfitter_format_input/mc15_13TeV_ggH"+point+"_W0.010p.root"; //MC162220// mc15_13TeV_ggH650_W0.010p.root
  datacard_input15B =datacard_path+"MC162220/single_input.dat";

  //../datacards/LowHighMassRun2/SP/MC162220/LWA/SystLWA/sigFit_input_W0.010p.dat
  ///afs/cern.ch/work/k/kgrevtso/public/HighLowMassHgg_files/MC162220/HGGfitter_format_input/systematics/Scale_ZEESYST/Down/mc15_13TeV_ggH750_W0.010p.root 
  //file_name15C= indir + "MC115B010/HGGfitter_format_input/mc12_8TeV_ggH"+point+".root";
  file_name15C= indir + "MC162230/HGGfitter_format_input/mc15_13TeV_G"+point+"_k001.root"; //mc15_13TeV_G700_k001.root
  datacard_input15C =datacard_path+"MC162230/single_input.dat";
  
  //set fitrange ±20%m
  int mHup;  int mHlow;
  mHup = mH*1.1;   mHlow = mH*0.9;
  int nBins = (mHup - mHlow)/10;

  std::cout << "-------- mH: " << mH << "; up: "<< mHup <<"; mHlow: "<< mHlow <<";  n_bins= "<<nBins<<  ";   weights to use: " << weight_to_use << endl; 
  float sigmaCB15B=0,sigmaCB15C=0;

  TCanvas *callpt = new TCanvas("callpt", "", 30, 60, 1000, 1000);
  TPad *pad1 = new TPad("pad1", "pad1", 0, 0.29, 1, 1);
  TPad *pad2 = new TPad("pad2", "pad2", 0, 0, 1, 0.33);
  pad1->SetBottomMargin(0.11);  pad1->SetBorderMode(0);  pad2->SetTopMargin(0.017);  pad2->SetBottomMargin(0.25);
  pad2->SetBorderMode(0);  pad1->Draw();  pad2->Draw();  pad1->cd();  tag.SetTextSize(0.03);
  auto hgg15B = HftModelBuilder::Create("hgg15B", datacard_input15B);
  hgg15B->Dependent("mgg")->setBins(nBins);
  hgg15B->Var("mX")->setVal(mH);
  hgg15B->Dependent("mgg")->setRange(mHlow, mHup);

  cout<< " =======================     !!!!!!!!   15B   !!!!!!    ==================  " <<endl;  
  cout<< "                 File       " <<file_name15B<<" " <<endl;  
  cout<< "                 dc_I       " <<datacard_input15B<<" " <<endl;  

  auto sigData = HftData::Open(file_name15B, "tree", *hgg15B, "", weight_to_use);
  hgg15B->FitData( *sigData, "W+"); //W+ take in account weights
  p15B=hgg15B->Plot("mgg", *sigData,"SetMarkerSize(2)");
  hgg15B->FreeParameters().Print("V");
  sigmaCB15B= hgg15B->Var("cbSigma")->getVal();
  RooHist* hdata15B = p15B->getHist("mgg_data");
  RooCurve* hfit15B = p15B->getCurve("mgg_pdf");
  hfit15B->SetLineColor(4);
  hdata15B->SetLineColor(4);
  hdata15B->SetMarkerStyle(20);
  hdata15B->SetMarkerColor(4);



  cout<< " =======================     !!!!!!!!   15C   !!!!!!    ==================  " <<endl;  
  cout<< "                 File       " <<file_name15C<<" " <<endl;  
  cout<< "                 dc_I       " <<datacard_input15C<<" " <<endl;  
  auto hgg15C = HftModelBuilder::Create("hgg15C", datacard_input15C);
  hgg15C->Dependent("mgg")->setBins(nBins);
  hgg15C->Var("mX")->setVal(mH);
  hgg15C->Dependent("mgg")->setRange(mHlow, mHup);

  auto sigData15C = HftData::Open(file_name15C, "tree", *hgg15C, "", "weightNbPU");// weight_to_use
  hgg15C->FitData( *sigData15C, "W+"); //W+ take in account weights
  p15C=hgg15C->Plot("mgg", *sigData15C,"SetLineColor(2)");
  RooHist* hdata15C = p15C->getHist("mgg_data");
  RooCurve* hfit15C = p15C->getCurve("mgg_pdf");
  RooCurve* hfitSig15C = p15C->getCurve("mgg_pdf_Signal");
  sigmaCB15C= hgg15C->Var("cbSigma")->getVal();

  hgg15C->FreeParameters().Print("V");

  //find FWHM for tight - pdf hfit15C; handle it as TGrapth 
  int n = hfitSig15C->GetN(); double* y_ax = hfitSig15C->GetY(); double* x_ax = hfitSig15C->GetX(); int locmax = TMath::LocMax(n,y_ax);
  Double_t max = TMath::MaxElement(n,y_ax);

  // //hfitSig15C->GetX()[10]
  // cout << "elements: "<< hfitSig15C->GetN() << "; maximum " << max << ", at =  "<< locmax << "; x val = " << x_ax[locmax] << endl;
  // Double_t x_min=0, x_max=0, fwhm=0;
  // for (j=0;j<n/2;j++) {
  //   if (TMath::AreEqualRel(max/2,y_ax[j],1e-1)) {
  //     printf("Minimun found point %d with x=%g, y=%g\n",j,x_ax[j],y_ax[j]);
  //     x_min=x_ax[j];
  //   }
  // }
  // cout<< "Spline: "<<  hfitSig15C->Eval(max,0,"S") << endl;

  // for (j=n/2;j<n;j++) {
  //   if (TMath::AreEqualRel(max/2,y_ax[j],1e-1)) {
  //     printf("Maximum found point %d with x=%g, y=%g\n",j,x_ax[j],y_ax[j]);
  //     x_max=x_ax[j];
  //     }
  // }
  // fwhm=x_max-x_min;
  // cout<< "mass "<< mH<< "; fwhm = " << fwhm << "; intris res = "<< fwhm/mH<<endl;
  //Double_t xmax = hfit15C->GetX(0.5,150,200);
  //double x2max = hfit15C->GetX(0.5,200,250); 
  double fwhm = 0;
  fwhm=FWHM(hfitSig15C,mHlow,mHup);
  cout<< "find FWHM for tight: " << fwhm << ";  intristic res: " << fwhm/mH << endl;

  hfitSig15C->SetLineColor(2);
  hfit15C->SetLineColor(2);
  hfit15C->SetLineStyle(1);
  hdata15C->SetLineColor(2);
  hdata15C->SetMarkerStyle(25);
  hdata15C->SetMarkerColor(2);
  p15B->Draw("same");

  TLegend* leg = new TLegend(0.65, 0.7, 0.85, 0.87);
  leg->SetBorderSize(0);
  leg->SetFillColor(0);
  leg->SetTextSize(0.04);
  leg->AddEntry(hfit15B, "MC 15B h011", "l");
  leg->AddEntry(hfit15C, "MC 15C h012pre2", "l");
  leg->AddEntry(hfit15C, "no PU weight", "");
  leg->Draw("same");

  //Nice plots
  gPad->SetLogy();
  p15C->SetMarkerSize(1);
  //p15C->SetMaximum(max*1.8);
  //p15C->SetMinimum(1);
  p15C->SetMaximum(1e4);
  p15C->SetMinimum(0.1);
  p15C->GetXaxis()->SetTitleSize(0.06);
  p15C->GetYaxis()->SetTitleSize(0.06);
  p15C->GetYaxis()->SetTitleOffset(1.2);
  p15C->GetXaxis()->SetTitleOffset(1.1);
    
  p15C->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  p15C->GetYaxis()->SetTitle("Events /(10 GeV)");
  //Set label on plot
  ATLASLabel(0.2,0.87,"Simulation Internal",1,0.05);
  tag.SetTextSize(0.045);
  label_plot="#sqrt{s} = 13 TeV, G#rightarrow#gamma#gamma";
  tag.DrawLatex(0.2, 0.8, label_plot);
  tag.DrawLatex(0.2, 0.74, "Spin-2 Selection");
  label_plot="m_{G} = "+point+" GeV";
  tag.DrawLatex(0.2, 0.69, label_plot);  
  label_plot="#kappa/#bar{M_{Pl}} = 0.01";
  tag.DrawLatex(0.2, 0.63, label_plot);

  tag.SetTextSize(0.045);
  tag.DrawLatex(0.2, 0.52, "#sigma_{CB}:");
  char val[128]; 
  sprintf(val,"%1.2f ",sigmaCB15B);
  tag.SetTextColor(4);
  tag.DrawLatex(0.2, 0.47, val);
  tag.SetTextColor(2);
  sprintf(val,"%1.2f ",sigmaCB15C);
  tag.DrawLatex(0.2, 0.42, val);

  pad2->cd();
  RooHist* hresid15B = p15B->residHist("mgg_data", "mgg_pdf",true);
  pp15B = hgg15B->Dependent("mgg")->frame();
  pp15B->addPlotable(hresid15B, "p10");
  pp15B->SetTitle("");
  pp15B->GetYaxis()->SetLabelSize(0.09);
  pp15B->GetXaxis()->SetLabelSize(0.01);
  pp15B->GetXaxis()->SetTitleSize(0.01);
  pp15B->GetXaxis()->SetLabelOffset(1);

  const unsigned  int npoints15B = hresid15B->GetN();
  Float_t res15B_val[npoints15B],res15B_err[npoints15B],mass15B_val[npoints15B],mass15B_err[npoints15B];
  for (int i = 0; i < npoints15B; i++) 
    {
      res15B_val[i] = hresid15B->GetY()[i ];
      res15B_err[i] = hresid15B->GetErrorY(i ); 
      mass15B_val[i] = hresid15B->GetX()[i ];
      mass15B_err[i] = hresid15B->GetErrorX(i ); 
    }

  TGraphErrors*  h_rat15B = new TGraphErrors(npoints15B,mass15B_val,res15B_val,mass15B_err,res15B_err );
  h_rat15B->SetMarkerColor(1);
  h_rat15B->SetMarkerSize(0.3);
  h_rat15B->SetLineColor(1);
  h_rat15B->GetYaxis()->SetLabelSize(0.08);
  h_rat15B->GetXaxis()->SetLabelSize(0.08);
  h_rat15B->GetXaxis()->SetTitleSize(0.08);
  h_rat15B->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  h_rat15B->GetYaxis()->SetTitle("Pull Distribution");
  h_rat15B->GetYaxis()->SetTitleSize(0.08);
  h_rat15B->GetYaxis()->SetTitleOffset(0.6);
  h_rat15B->GetYaxis()->SetRangeUser(-5, 5);
  h_rat15B->GetXaxis()->SetRangeUser(mHlow, mHup);


  TLine *l = new TLine(mHlow, 0, mHup, 0);
  l->SetLineColor(kBlue);
  h_rat15B->SetMarkerColor(4);
  h_rat15B->SetMarkerStyle(25);
  h_rat15B->SetMarkerSize(0.6);
  h_rat15B->SetLineColor(4);
  h_rat15B->Draw();
  l->Draw();

  RooHist* hresid15C = p15C->residHist("mgg_data", "mgg_pdf",true);
  pp15C = hgg15C->Dependent("mgg")->frame();
  pp15C->addPlotable(hresid15C, "p10");

  const unsigned  int npoints15C = hresid15C->GetN();
  Float_t res15C_val[npoints15C],res15C_err[npoints15C],mass15C_val[npoints15C],mass15C_err[npoints15C];
  for (int i = 0; i < npoints15C; i++) 
    {
      res15C_val[i] = hresid15C->GetY()[i ];
      res15C_err[i] = hresid15C->GetErrorY(i ); 
      mass15C_val[i] = hresid15C->GetX()[i ];
      mass15C_err[i] = hresid15C->GetErrorX(i ); 
    }

  TGraphErrors*  h_rat15C = new TGraphErrors(npoints15C,mass15C_val,res15C_val,mass15C_err,res15C_err );
  h_rat15C->SetMarkerColor(2);
  h_rat15C->SetMarkerStyle(25);
  h_rat15C->SetMarkerSize(0.6);
  h_rat15C->SetLineColor(2);
  h_rat15C->Draw("same");

  //callpt->Print(outname_plot);   
  //*/

  //callpt->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/MC160015B/plots/Moriond15B16/suppNote/Run2Scalar_400_NWA.pdf");  
  //gPad->SetLogy();
  //callpt->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/MC160015B/plots/Moriond15B16/suppNote/Run2Scalar_400_NWA_log.pdf");   
  //callpt->Print("/afs/cern.ch/user/k/kgrevtso/private/atlas-kgrevtso/HighLowMassHgg/InjectionTest/Fitters/HGGfitter/datacards/LowHighMassRun2/SP/MC160015B/LWA/plots/Run2Scalar_750_LWA6.C");   
  

}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
    p.DrawLatex(x+delx*1.0,y,text);
    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}

double FWHM(TGraph* tg, double xmin = 110, double xmax = 140) {
  double max = 0; double npoints = 10000;
  for(int i = 0; i < npoints; i++) {
    double xstep = xmin + i*(xmax-xmin)/npoints;
    max = tg->Eval(xstep) > max ? tg->Eval(xstep) : max;        
  }
  double one = -1, two = -1;
  for(int i = 0; i < npoints; i++) {
    double xstep = xmin + i*(xmax-xmin)/npoints;
    if( tg->Eval(xstep) > max/2. && one == -1) one = xstep;
    if( tg->Eval(xstep) < max/2. && two == -1 && one != -1) two = xstep;        
  }
  return two - one;
};
