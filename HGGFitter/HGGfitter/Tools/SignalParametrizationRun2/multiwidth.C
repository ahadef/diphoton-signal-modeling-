#include "/Users/grevtsov/Documents/working_files/AtlasStyle/AtlasStyle.C"
#include "/Users/grevtsov/Documents/working_files/AtlasStyle/AtlasUtils.C"
void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);

void multiwidth(){
  double mass=800;
  double width=0;
  int npoints = 10;

  SetAtlasStyle();

  char filename[1000], name[1000], plotname[1000];
  double width1=width*100;
  TString cardNW = "../datacards/LowHighMassRun2/hfitter_newResRun2_highMass_HiggsNW_2016.dat";
  TString card = "../datacards/LowHighMassRun2/hfitter_newResRun2_highMass_HiggsConvol_2016.dat";

  hgg = HftModelBuilder::Create("hgg", cardNW);  hgg->Var("mX")->setVal(mass);
  RooDataSet* data = hgg->CategoryModel(0)->GenerateAsimov();
  TCanvas *  callpt = new TCanvas("callpt", "", 30, 60, 800, 600);

  Hfitter::HftModel *hgg1[100];
  RooPlot* rp[100];
  for(int i=1; i<npoints;i++){
  sprintf(name,"hgg_%d",i);
  //cout<< i<< ": mass = "<<mass+100*i<< endl;
  hgg1[i] = HftModelBuilder::Create(name, card); hgg1[i]->Var("GammaOverMX")->setVal(width+0.01*i); hgg1[i]->Var("mX")->setVal(mass);  
  rp[i]= hgg1[i]->ComponentModel("Signal")->Plot("mgg",*data,"PlotErrors(None)"); 
  if(i*2<14)  rp[i]->getCurve("mgg_pdf")->SetLineColor(kCyan-10+i*2);//kMagenta
  if(i*2>13)  rp[i]->getCurve("mgg_pdf")->SetLineColor(kAzure-20+i*2);//kMagenta

  }

  p= hgg->ComponentModel("Signal")->Plot("mgg", *data, "PlotErrors(None)");
  p->getCurve("mgg_pdf")->SetLineColor( kTeal);
  TLatex tag;
  tag.SetNDC(); // setting coordinates
  tag.SetTextSize(0.05);
  
  //cout<< "max  "<<     max <<endl;
  //p->SetMaximum(100); //NWA  
  //p->SetMinimum(0);
  p->GetXaxis()->SetTitleSize(0.06);
  p->GetYaxis()->SetTitleSize(0.06);
  p->GetYaxis()->SetTitleOffset(1.2);
  p->GetXaxis()->SetTitleOffset(1.1);
  //p->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  p->GetXaxis()->SetTitle("m_{X} [GeV]");
  p->GetYaxis()->SetTitle("Events / 1 GeV");
  ATLASLabel(0.5,0.85,"Simulation Internal",1,0.05);
  //ATLASLabel(0.2,0.85,"Simulation Preliminary",1,0.05);
  //ATLASLabel(0.2,0.85,"Simulation",1,0.05);
  
  TString width_val,point,label_plot;
  width_val.Form("%1.0f",width1);
  point.Form("%1.0f",mass);
  
  label_plot="#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma";
  tag.DrawLatex(0.5, 0.78, label_plot);
  tag.DrawLatex(0.5, 0.71, "Spin-0 Selection");
  
  tag.SetTextSize(0.045);
  label_plot="m_{X} = "+point+" GeV";
  //tag.DrawLatex(0.2, 0.64, label_plot);
  if(width!=0) label_plot="#Gamma_{X}/m_{X} = "+width_val+"%";
  label_plot="#Gamma_{X}/m_{X} = [0 - 10]%";
  tag.DrawLatex(0.5, 0.64, label_plot);
  //*/    

  for(i=1; i<npoints;i++){
  rp[i]->Draw("same");  //p2->Draw("same");
  }
  //s_plotF=  hggF->ComponentModel(0)->Plot("mgg", *data);
  //Smodel = s_plotF->getCurve("mgg_pdf");
  //Smodel->SetLineColor(2);  
  //s_plot=  hgg->ComponentModel(0)->Plot("mgg", *data);
  //s_plot->SetMinimum(1e-2);
  //s_plot->Draw();
  //s_plotF->Draw("same");


}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
     p.DrawLatex(x+delx*1.1,y,text);

    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}
