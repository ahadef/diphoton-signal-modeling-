#include "TString.h"
#include "TCanvas.h"
#include "RooGlobalFunc.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooGaussian.h"
#include "RooConstVar.h"
#include "TAxis.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "TLatex.h"
#include "TH1F.h"
#include "TLine.h"

#include "../AtlasStyle.C"
#include "../AtlasUtils.C"
void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);

void nicePlot(double mass=200, double width=0){

  SetAtlasStyle();

  char filename[1000];
  double width1=width*100;
  double mass_min, mass_max, mass_bins;
  TString Mass;
  Mass=Form("%1.0f",mass);
  cout<<"mass = "<<Mass<<endl;
  mass_min = mass - mass * 0.1;
  mass_max = mass + mass * 0.1;
  mass_bins = (mass_max - mass_min) * 1;
  cout<<"minimum mass = "<<mass_min<<endl;
  cout<<"maximum mass = "<<mass_max<<endl;
  cout<<" mass bins         = "<<mass_bins<<endl;

  //if(width!=0) sprintf(filename,"reproc2017_20p7/mc15c_samples/scalar_MG/mc15_13TeV_MG%1.0f_W%1.0fp.root",mass, width1);
  if(width!=0) sprintf(filename,"reproc2017_20p7/mc15c_samples/h015/mc15_13TeV_MG%1.0f_W%1.0fp.root",mass, width1);
 // else  sprintf(filename,"../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16_13TeV_MG%1.0f_2unconv.root",mass);
 else  sprintf(filename,"../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC2019/HGGfitter_format_input/NWA/mc16a_13TeV_MG%1.0f.root",mass);

  TFile *file0 = TFile::Open(filename);
  TTree *tree  = (TTree*) file0->Get("tree");
  TH1F   *hist = new TH1F("hist", "hist", 50, mass_min, mass_max);
  tree->Draw("mgg>>hist", "weight");
  float nentries=0;
  nentries=hist->Integral();
  //nentries=tree->GetEntries();
  int nBins=0;
  //TCanvas *callpt = new TCanvas("callpt", "", 30, 60, 1000, 1000);

  //hgg = HftModelBuilder::Create("hgg", "../datacards/LowHighMassRun2/hfitter_newResRun2_highMass_HiggsConvol_ICHEP.dat");
  auto hgg = HftModelBuilder::Create("hgg", "multiple_input_MG_test_HM.dat");
  if(width==0)  hgg = HftModelBuilder::Create("hgg", "multiple_input_MG_test_HM.dat");

  hgg->FreeParameters().Print("V");
   Double_t vars_size =  hgg->FreeParameters().getSize();
  if(width!=0){ hgg->Var("mgg")->setRange(mass*0.65, mass*1.2);
  hgg->Var("mgg")->setBins(mass*0.11);
  nBins=mass*0.11;
  }
  if(width==0){ hgg->Var("mgg")->setRange(mass*0.88, mass*1.1);
    //hgg->Var("mgg")->setBins(mass*0.55);
    hgg->Var("mgg")->setBins(100);
    nBins=100;
  }

  //hgg->Var("xs")->setVal(8);
  hgg->Var("mX")->setVal(mass);
  //if(width!=0){
  //hgg->Var("GammaOverMX")->setVal(width);
  //hgg->Var("GammaOverMX")->setConstant(1);}
  //hgg->Var("dSig")->setConstant(1);
  

  auto data = HftData::Open(filename,"tree",*hgg->ComponentModel("Signal"),"","weight");
  //hgg->ComponentModel("Signal")->Fit(*data);
  //s_plot=  hgg->ComponentModel("Signal")->Plot("mgg", *data);

  float Norm=0;
  Norm=1/nentries; 

  cout<< " Norm " <<Norm << " (N="<<nentries<<")" << endl;
  TCanvas *callpt = new TCanvas("callpt", "", 30, 60, 800, 600);
  
  auto p= hgg->ComponentModel("Signal")->Plot("mgg", *data,Form("PlotScale(Relative,%g ):DataScale(%g )",Norm,Norm));

  
  auto hdata20 = p->getHist("mgg_data");
  hdata20->SetMarkerSize(1);
  
  //*
  int n = hdata20->GetN(); double* y_ax = hdata20->GetY(); double* x_ax = hdata20->GetX(); int locmax = TMath::LocMax(n,y_ax);
  Double_t max = TMath::MaxElement(n,y_ax);
  TLatex tag;
  tag.SetNDC(); // setting coordinates
  tag.SetTextSize(0.05);
  
  cout<< "max  "<<     max <<endl;
  p->SetMaximum(max*1.35); //NWA  
  p->SetMinimum(0);
  p->GetXaxis()->SetTitleSize(0.06);
  p->GetYaxis()->SetTitleSize(0.06);
  p->GetYaxis()->SetTitleOffset(1.2);
  p->GetXaxis()->SetTitleOffset(1.1);
  p->GetXaxis()->SetTitle("m_{#gamma#gamma} [GeV]");
  //if(width!=0)  p->GetYaxis()->SetTitle("1/N dN/dm_{#gamma#gamma} / 5 GeV");
  //if(width==0)  p->GetYaxis()->SetTitle("1/N dN/dm_{#gamma#gamma} / 1 GeV");
  if(width!=0)  p->GetYaxis()->SetTitle("1/N dN/dm_{#gamma#gamma} [GeV^{-1}]");
  if(width==0)  p->GetYaxis()->SetTitle("1/N dN/dm_{#gamma#gamma} [GeV^{-1}]");

  ATLASLabel(0.2,0.85,"Simulation Preliminary",1,0.05);
  //ATLASLabel(0.2,0.85,"Simulation Preliminary",1,0.05);
  //ATLASLabel(0.2,0.85,"Simulation",1,0.05);
  
  TString width_val,point,label_plot, label_plot1;
  width_val.Form("%1.0f",width1);
  point.Form("%1.0f",mass);

  cout<<"chi2 = "<<p->chiSquare()<<endl;
  cout<<"NDF = "<<nBins-5-1<<endl;
  double goodness = p->chiSquare()/(nBins-6);
  cout<<"chi2/NDF = "<<goodness<<endl;
  label_plot1= Form("chi2/NDF =%.2f ", goodness );
  
  label_plot="#sqrt{s} = 13 TeV, X#rightarrow#gamma#gamma";
  tag.DrawLatex(0.2, 0.78, label_plot);
  tag.DrawLatex(0.2, 0.66, label_plot1);
  //tag.DrawLatex(0.2, 0.71, "Spin-0 Selection");
  
  tag.SetTextSize(0.045);
  label_plot="m_{X} = "+point+" GeV";
  tag.DrawLatex(0.2, 0.71, label_plot);
  if(width!=0) label_plot="#Gamma_{X}/m_{X} = "+width_val+"%";
  else label_plot="#Gamma_{X} = 4 MeV";
  //tag.DrawLatex(0.2, 0.57, label_plot);
  //*/ 
  callpt->SaveAs("globalfit/global_fit_MG"+Mass+"_CONFnote.png");
  callpt->SaveAs("globalfit/global_fit_MG"+Mass+"_CONFnote.pdf");  
  callpt->SaveAs("globalfit/global_fit_MG"+Mass+"_CONFnote.eps");
  callpt->SaveAs("globalfit/global_fit_MG"+Mass+"_CONFnote.c");       
}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
     p.DrawLatex(x+delx*1.1,y,text);

    //    p.DrawLatex(x,y,"#sqrt{s}=900GeV");
  }
}
