// $Id: HggPowerLaw.h,v 1.4 2008/03/06 14:52:04 nberger Exp $   

#ifndef ROOT_Hfitter_HggPowerLaw
#define ROOT_Hfitter_HggPowerLaw

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

namespace Hfitter {

   class HggPowerLaw : public RooAbsPdf {

   public:
     
     HggPowerLaw() { }
     HggPowerLaw(const HggPowerLaw& other, const char* name = 0);
     HggPowerLaw(const char* name, const char* title, RooAbsReal& x, RooAbsReal& exponent);
     
     inline virtual ~HggPowerLaw() {}
     
     Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const;
     Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const;
     
     virtual TObject* clone( const char* newname ) const { return new HggPowerLaw( *this, newname ); }

   protected:
      
     RooRealProxy m_x;
     RooRealProxy m_exp;
     
     Double_t evaluate() const;
     
   private:

     ClassDef(Hfitter::HggPowerLaw, 1) 

   };
}

#endif
