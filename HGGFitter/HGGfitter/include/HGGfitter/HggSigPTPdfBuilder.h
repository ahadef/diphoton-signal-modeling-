// $Id: HggSigPTPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggSigPTPdfBuilder
#define ROOT_Hfitter_HggSigPTPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggSigPTPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggSigPTPdfBuilder() { }
    virtual ~HggSigPTPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggSigPTPdfBuilder, 0);
  };
}

#endif
