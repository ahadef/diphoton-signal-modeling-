// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace, Kerstin Tackmann

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggSigMggPTPdfBuilder
#define ROOT_Hfitter_HggSigMggPTPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggSigPTPdfBuilder.h"

namespace Hfitter {

  class HggSigMggPTPdfBuilder : public HftAbsPdfBuilder {

  public:

    HggSigMggPTPdfBuilder() { }

    virtual ~HggSigMggPTPdfBuilder() { }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:

    HggSigMggPdfBuilder m_mggPdfBuilder;
    HggSigPTPdfBuilder m_pTPdfBuilder;

    ClassDef(Hfitter::HggSigMggPTPdfBuilder, 0);
  };
}

#endif
