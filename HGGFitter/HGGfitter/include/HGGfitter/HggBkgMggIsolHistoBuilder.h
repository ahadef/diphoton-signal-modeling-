// Author: Marine Kuna

#ifndef ROOT_Hfitter_HggBkgMggIsolHistoBuilder
#define ROOT_Hfitter_HggBkgMggIsolHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"

namespace Hfitter {
  
  class HggBkgMggIsolHistoBuilder : public HftAbsPdfBuilder {
    
  public:
          
    HggBkgMggIsolHistoBuilder() { }    
    virtual ~HggBkgMggIsolHistoBuilder() { }

    
    void Setup(bool isGam1, bool isGam2, const char* depName = "mgg",
               const char* depName1 = "isol1", const char* depName2 = "isol2", const char* xiName = "xi")
    { m_isGam1 = isGam1 ; m_isGam2 = isGam2 ; m_depName = depName;
      m_depName1 = depName1; m_depName2 = depName2; m_xiName = xiName; }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const; 
    
    ClassDef(Hfitter::HggBkgMggIsolHistoBuilder, 0);

  private:
    
    TString m_depName, m_depName1, m_depName2, m_xiName;
    bool m_isGam1;
    bool m_isGam2;
   
  };
}

#endif
