#ifndef ROOT_Hfitter_HggBkgMggIsolPdfBuilder
#define ROOT_Hfitter_HggBkgMggIsolPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "HGGfitter/HggIsolPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggIsolPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggIsolPdfBuilder();
      
    virtual ~HggBkgMggIsolPdfBuilder() { }

    void Setup(bool isGam1, bool isGam2);

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggBkgMggPdfBuilder m_mggPdfBuilder;
    HggIsolPdfBuilder m_isol1PdfBuilder, m_isol2PdfBuilder;
    
    ClassDef(Hfitter::HggBkgMggIsolPdfBuilder, 0);
  };
}

#endif
