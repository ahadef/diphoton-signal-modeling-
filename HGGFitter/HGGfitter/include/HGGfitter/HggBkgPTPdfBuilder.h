// $Id: HggBkgPTPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgPTPdfBuilder
#define ROOT_Hfitter_HggBkgPTPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgPTPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgPTPdfBuilder() { }   
    virtual ~HggBkgPTPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgPTPdfBuilder, 0);
  };
}

#endif
