// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggExpPolyPdfBuilder
#define ROOT_Hfitter_HggExpPolyPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggExpPolyPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggExpPolyPdfBuilder() { }    
    virtual ~HggExpPolyPdfBuilder() { }

    void Setup(const char* depName = "mgg",
               const char* c1Name = "c1", const char* c2Name = "c2", 
               const char* c3Name = "", const char* c4Name = "", 
               const char* c5Name = "", const char* c6Name = "");

    void Setup(const char* depName, double offset,
              const char* c1Name = "c1", const char* c2Name = "c2", 
              const char* c3Name = "", const char* c4Name = "", 
              const char* c5Name = "", const char* c6Name = "");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggExpPolyPdfBuilder, 0);

  private:
    
    TString m_depName;
    std::vector<TString> m_cNames;
    double m_offset;
  };
}

#endif
