// Author: Marine Kuna

// Higgs signal PDF with isolation

#ifndef ROOT_Hfitter_HggBkgMggDiffIsolExpPolyHistoBuilder
#define ROOT_Hfitter_HggBkgMggDiffIsolExpPolyHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"


#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggBkgMggDiffIsolExpPolyHistoBuilder : public HftAbsPdfBuilder {
    
  public:
    
      
    HggBkgMggDiffIsolExpPolyHistoBuilder() { }    
    virtual ~HggBkgMggDiffIsolExpPolyHistoBuilder() { }

        
    void Setup(bool isGam1, bool isGam2, const char* depName = "mgg",
               const char* depName1 = "isol1", const char* depName2 = "isol2",
               const char* c1ggName = "c1_gg", const char* c1gjName = "c1_gj",
	       const char* c2ggName = "c2_gg", const char* c2gjName = "c2_gj",
	       const char* c3ggName = "", const char* c3gjName = "",
	       const char* c4ggName = "", const char* c4gjName = "",
               const char* c5ggName = "", const char* c5gjName = "",
	       const char* c6ggName = "", const char* c6gjName = "") ;
	       
    void Setup(bool isGam1, bool isGam2, const char* depName, 
              const char* depName1, const char* depName2,
              double offset,
              const char* c1ggName = "c1_gg", const char* c1gjName = "c1_gj",
	      const char* c2ggName = "c2_gg", const char* c2gjName = "c2_gj",
	      const char* c3ggName = "", const char* c3gjName = "",
	      const char* c4ggName = "", const char* c4gjName = "",
              const char* c5ggName = "", const char* c5gjName = "",
	      const char* c6ggName = "", const char* c6gjName = "") ;

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    TString m_depName, m_depName1, m_depName2;
    std::vector<TString> m_cggNames, m_cgjNames;
    double m_offset;
   
    bool m_isGam1;
    bool m_isGam2;
         
    ClassDef(Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder, 0);
  };
}

#endif
