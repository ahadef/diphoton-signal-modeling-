// $Id: HggSigMggPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggSigMggPdfBuilder
#define ROOT_Hfitter_HggSigMggPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggSigMggPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggSigMggPdfBuilder() : m_pdfType(0) { }
    virtual ~HggSigMggPdfBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaName = "cbAlpha", const char* cbNName = "cbN",  
               const char* tailPeakName = "mTail", const char* tailSigmaName = "sigmaTail", const char* tailFractionName = "mggRelNorm", 
               unsigned int pdfType = 0, const char* cat = "");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;    

  private:
    unsigned int m_pdfType;
    TString m_depName, m_cbPeakName, m_cbSigmaName, m_cbAlphaName, m_cbNName;
    TString m_tailPeakName, m_tailSigmaName, m_tailFractionName;
    
    ClassDef(Hfitter::HggSigMggPdfBuilder, 0);
  };
}

#endif
