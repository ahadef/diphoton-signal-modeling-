// Author: Marine Kuna

#ifndef ROOT_Hfitter_HggBkgMggDiffIsolHistoBuilder
#define ROOT_Hfitter_HggBkgMggDiffIsolHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"

namespace Hfitter {
  
  class HggBkgMggDiffIsolHistoBuilder : public HftAbsPdfBuilder {
    
  public:
          
    HggBkgMggDiffIsolHistoBuilder() { }    
    virtual ~HggBkgMggDiffIsolHistoBuilder() { }

    
    void Setup(bool isGam1, bool isGam2, const char* depName = "mgg",
               const char* depName1 = "isol1", const char* depName2 = "isol2",
	       const char* xiggName = "xi_gg", const char* xigjName = "xi_gj")
    { m_isGam1 = isGam1 ; m_isGam2 = isGam2 ; m_depName = depName;
      m_depName1 = depName1; m_depName2 = depName2; m_xiggName = xiggName; m_xigjName = xigjName;}
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const; 
    
    ClassDef(Hfitter::HggBkgMggDiffIsolHistoBuilder, 0);

  private:
    
    TString m_depName, m_depName1, m_depName2, m_xiggName, m_xigjName;
    bool m_isGam1;
    bool m_isGam2;
   
  };
}

#endif
