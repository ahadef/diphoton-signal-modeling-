// $Id: HggSigMggCosThStarPTPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggSigMggCosThStarPTPdfBuilder
#define ROOT_Hfitter_HggSigMggCosThStarPTPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggSigCosThStarPdfBuilder.h"
#include "HGGfitter/HggSigPTPdfBuilder.h"

namespace Hfitter {
  
  class HggSigMggCosThStarPTPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggSigMggCosThStarPTPdfBuilder() { }
      
    virtual ~HggSigMggCosThStarPTPdfBuilder() { }
    
    void Setup(unsigned int cThPdfType = 3) { m_cosThStarPdfBuilder.Setup(cThPdfType); }

    RooAbsPdf* Pdf(const TString& name,  const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggSigMggPdfBuilder m_mggPdfBuilder;
    HggSigCosThStarPdfBuilder m_cosThStarPdfBuilder;
    HggSigPTPdfBuilder m_pTPdfBuilder;

    ClassDef(Hfitter::HggSigMggCosThStarPTPdfBuilder, 0);
  };
}

#endif
