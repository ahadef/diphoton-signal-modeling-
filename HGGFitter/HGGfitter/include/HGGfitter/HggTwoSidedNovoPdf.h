#ifndef ROOT_Hfitter_HggTwoSidedNovoPdf
#define ROOT_Hfitter_HggTwoSidedNovoPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

namespace Hfitter {

class HggTwoSidedNovoPdf : public RooAbsPdf {
public:
  HggTwoSidedNovoPdf() {}
  HggTwoSidedNovoPdf(const char *name, const char *title, RooAbsReal& _m,
               RooAbsReal& _m0, RooAbsReal& _sigma,
               RooAbsReal& _alphaLo, RooAbsReal& _alphaHi);

  HggTwoSidedNovoPdf(const HggTwoSidedNovoPdf& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new HggTwoSidedNovoPdf(*this,newname); }

  inline virtual ~HggTwoSidedNovoPdf() { }

  virtual Int_t getAnalyticalIntegral( RooArgSet& allVars,  RooArgSet& analVars, const char* rangeName=0 ) const;
  virtual Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;

  double expIntegral(double tmin, double tmax, double beta) const;
  double gaussianIntegral(double tmin, double tmax) const;
  
protected:

  RooRealProxy m;
  RooRealProxy m0;
  RooRealProxy sigma;
  RooRealProxy alphaLo;
  RooRealProxy alphaHi;
  RooRealProxy nHi;

  Double_t evaluate() const;

private:

  ClassDef(Hfitter::HggTwoSidedNovoPdf,1) // Crystal Ball lineshape PDF
};

}

#endif
