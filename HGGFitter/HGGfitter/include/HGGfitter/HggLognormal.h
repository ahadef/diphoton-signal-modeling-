// Author: Nicolas Berger
// Trivial modification of RooLognormal

#ifndef Hfitter_HggLognormal
#define Hfitter_HggLognormal

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

namespace Hfitter {

  class HggLognormal : public RooAbsPdf {
    
  public:
    HggLognormal() { }
    
    // The PDF represents a log-normal distribution, i.e. the distribution of a variable whose log is a Gaussian.
    // - expm is exp(Gaussian mean) -- it is also the median of the log-normal, and its mean is expm*exp(sigma^2/2)
    // - sigma is the Gaussian width
    HggLognormal(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _expm, RooAbsReal& _sigma, bool useModifiedSigma = false);
    HggLognormal(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _expm, RooAbsReal& _sigmaDn, RooAbsReal& _sigmaUp, bool useModifiedSigma = false);
    
    HggLognormal(const HggLognormal& other, const char* name = 0);
    virtual TObject* clone(const char* newname) const { return new HggLognormal(*this, newname); }
    
    inline virtual ~HggLognormal() { }

    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const;
    Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const;

    double integral(double xMin, double xMax) const;
    
    Int_t getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t staticInitOK = kTRUE) const;
    void generateEvent(Int_t code);

    double kappaDn() const;
    double kappaUp() const;
    
    static double Kappa(double sigma); // The combination calls "log(kappa)" what we call kappa here...
    static double Sigma(double kappa); // The combination calls "log(kappa)" what we call kappa here...
    
  protected:

    RooRealProxy x;
    RooRealProxy expm;
    RooRealProxy sigmaDn;
    RooRealProxy sigmaUp;
    bool m_useModifiedSigma;
    
    Double_t evaluate() const ;

  private:

    ClassDef(HggLognormal, 1) // log-normal PDF
  };
  
  class HggModifiedLognormal : public HggLognormal
  {
  public:
    HggModifiedLognormal(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _expm, RooAbsReal& _sigma)
      : HggLognormal(name, title, _x, _expm, _sigma, true) { }
    HggModifiedLognormal(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _expm, RooAbsReal& _sigmaDn, RooAbsReal& _sigmaUp)
      : HggLognormal(name, title, _x, _expm, _sigmaDn, _sigmaUp, true) { }
    
    ClassDef(HggModifiedLognormal, 1) // modified log-normal PDF
  };
}

#endif
