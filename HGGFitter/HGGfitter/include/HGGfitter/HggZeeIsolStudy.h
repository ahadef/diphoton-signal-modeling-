#ifndef ROOT_Hfitter_HggZeeIsolStudy
#define ROOT_Hfitter_HggZeeIsolStudy

#include "TString.h"
#include "TTree.h"
#include "RooArgList.h"
#include "RooAbsData.h"

#include <vector>

class RooRealVar;
class RooDataSet;
class RooDataHist;
class RooPlot;
class RooCurve;
class RooHist;
class TH1D;
class TCanvas;
class TVirtualPad;

namespace Hfitter {
  
  class HftModel;

  class HggZeeIsolStudy {
    public:
     static void makeZeeFromEG(const TString& inputFiles, const TString& treeName, const TString& outputName, bool isMC = false);
     static void makeZeeHists(const TString& inputFileName, const TString& treeName, const TString& outputName, 
                              unsigned int massBins, double massLo, double massHi,
                              unsigned int isolBins, double isolLo, double isolHi,
                              const std::vector<double>& bins, double mPeakMin, double mPeakMax, 
                              bool isMC = false);
     static bool analyzeBin(const TString& datacard, const TString& dataName, const TString& mcName, const TString& suffix, 
                            double pt, double peakLo, double peakHi, const TString& outputName, double ptSlope = 0);
     static bool makePlots(const TString& fileRoot, const std::vector<double>& bins, const TString& outputRoot, 
                           double effMin = -1, bool doTrk = false, double err1 = 0.01, double err2 = 0.01, double err3 = 0.01, double err4 = 0.01);

  };
}

#endif
