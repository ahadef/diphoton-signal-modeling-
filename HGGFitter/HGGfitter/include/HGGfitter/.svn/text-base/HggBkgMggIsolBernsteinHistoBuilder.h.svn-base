// Author: Marine Kuna

// Higgs signal PDF with isolation

#ifndef ROOT_Hfitter_HggBkgMggIsolBernsteinHistoBuilder
#define ROOT_Hfitter_HggBkgMggIsolBernsteinHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggBkgMggIsolBernsteinHistoBuilder : public HftAbsPdfBuilder {
    
  public:
          
    HggBkgMggIsolBernsteinHistoBuilder() { }    
    virtual ~HggBkgMggIsolBernsteinHistoBuilder() { }

        
    void Setup(bool isGam1, bool isGam2, const char* depName, const char* depName1, const char* depName2, 
                double xMin, double xMax,
               const char* cs1Name = "cs1", const char* cs2Name = "cs2", 
               const char* cs3Name = "", const char* cs4Name = "", 
               const char* cs5Name = "", const char* cs6Name = "", const char* cs7Name = "") ;
 	       
	       
    void Setup(bool isGam1, bool isGam2, const char* depName, const char* depName1, const char* depName2,
               const char* cs1Name, const char* cs2Name, 
               const char* cs3Name = "", const char* cs4Name = "", 
               const char* cs5Name = "", const char* cs6Name = "", const char* cs7Name = "");


    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    TString m_depName, m_depName1, m_depName2;
    std::vector<TString> m_csNames;
    double m_xMin, m_xMax;
    
    
    bool m_isGam1;
    bool m_isGam2;
    
         
    ClassDef(Hfitter::HggBkgMggIsolBernsteinHistoBuilder, 0);
  };
}

#endif
