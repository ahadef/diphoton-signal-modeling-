// $Id: HggBkgMggCosThStarPTPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggCosThStarPTPdfBuilder
#define ROOT_Hfitter_HggBkgMggCosThStarPTPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "HGGfitter/HggBkgCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgPTPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggCosThStarPTPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggCosThStarPTPdfBuilder() { }    
    virtual ~HggBkgMggCosThStarPTPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;    
  
  private:
    
    HggBkgMggPdfBuilder m_mggPdfBuilder;
    HggBkgCosThStarPdfBuilder m_cosThStarPdfBuilder;
    HggBkgPTPdfBuilder m_pTPdfBuilder;

    ClassDef(Hfitter::HggBkgMggCosThStarPTPdfBuilder, 0);
  };
}

#endif
