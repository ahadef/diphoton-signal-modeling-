// $Id: HggBkgMggDoubleExpPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggDoubleExpPdfBuilder
#define ROOT_Hfitter_HggBkgMggDoubleExpPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggDoubleExpPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggDoubleExpPdfBuilder() { }    
    virtual ~HggBkgMggDoubleExpPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* xi1Name = "xi1", const char* xi2Name = "xi2", const char* fracName = "frac") 
    { m_depName = depName; m_xi1Name = xi1Name; m_xi2Name = xi2Name; m_fracName = fracName; }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggDoubleExpPdfBuilder, 0);
    
  private:
    
    TString m_depName, m_xi1Name, m_xi2Name, m_fracName;
    
  };
}

#endif
