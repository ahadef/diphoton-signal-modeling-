// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBernsteinSmearMPdfBuilder
#define ROOT_Hfitter_HggBernsteinSmearMPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HGGfitter/HggBernsteinPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggBernsteinSmearMPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBernsteinSmearMPdfBuilder() { }    
    virtual ~HggBernsteinSmearMPdfBuilder() { }

    void Setup(const char* depName, const char* dep2Name, double xMin, double xMax,
               const char* cs1Name = "cs1", const char* cs2Name = "", 
               const char* cs3Name = "", const char* cs4Name = "", 
               const char* cs5Name = "", const char* cs6Name = "", const char* cs7Name = "");

    void Setup(const char* depName,
               const char* cs1Name, const char* dep2Name, const char* cs2Name, 
               const char* cs3Name = "", const char* cs4Name = "", 
               const char* cs5Name = "", const char* cs6Name = "", const char* cs7Name = "");

    void Setup(const char* depName, const char* dep2Name, double xMin, double xMax,
               unsigned int order, const char* csNameRoot = "cs");

    void Setup(const char* depName, const char* dep2Name, unsigned int order, const char* csNameRoot = "cs");
               
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBernsteinSmearMPdfBuilder, 0);

  private:

    TString m_dep2Name;
    HggBernsteinPdfBuilder m_base;
    
  };
}

#endif
