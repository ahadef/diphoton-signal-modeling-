// $Id: HggExpPowerPdf.h,v 1.4 2008/03/06 14:52:04 nberger Exp $   

#ifndef ROOT_Hfitter_HggExpPowerPdf
#define ROOT_Hfitter_HggExpPowerPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

namespace Hfitter {

   class HggExpPowerPdf : public RooAbsPdf {

   public:
     
     HggExpPowerPdf() { }
     HggExpPowerPdf(const HggExpPowerPdf& other, const char* name = 0);
     HggExpPowerPdf(const char* name, const char* title, 
                 RooAbsReal& x, RooAbsReal& xi, RooAbsReal& power, RooAbsReal& coeff);
     
     inline virtual ~HggExpPowerPdf() {}
     
     Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const;
     Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const;
     
     virtual TObject* clone( const char* newname ) const { return new HggExpPowerPdf( *this, newname ); }

   protected:
      
     RooRealProxy m_x;
     RooRealProxy m_xi;
     RooRealProxy m_power;
     RooRealProxy m_coeff;
     
     Double_t evaluate() const;
     
   private:

     ClassDef(Hfitter::HggExpPowerPdf, 1) 

   };
}

#endif
