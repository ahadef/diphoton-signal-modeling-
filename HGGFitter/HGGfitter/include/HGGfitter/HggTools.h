// $Id: HftTools.h,v 1.19 2009/01/30 16:25:12 hoecker Exp $   
// Author: Mohamed Aharrouche, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#ifndef ROOT_Hfitter_HggTools
#define ROOT_Hfitter_HggTools

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// HftHGGTools                                                          //
//                                                                      //
// Tools                                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////


#include "TString.h"
#include "TTree.h"
#include "RooArgList.h"
#include "RooAbsData.h"

class RooRealVar;
class RooDataSet;
class RooDataHist;
class RooPlot;
class RooCurve;
class RooHist;
class TH1D;
class TCanvas;
class TVirtualPad;

namespace Hfitter {
  
  class HftModel;

  class HggTools {
    public:
     static void listBranches(TTree* tree, const char* branches = "");
     static RooDataSet* dataSet(const char* fileNames, const HftModel& model, const char* cut = 0,
  			 const char* weightVar = 0);

     static RooDataHist* dataHist(const char* fileNames, const HftModel& model, const char* cut = "");

     static RooPlot* plotVars(Hfitter::HftModel& model, RooAbsData& data,
                       const char* saveTo = "");
   
     static TCanvas* plotCats(HftModel& model, const TString& var, RooAbsData& data, const TString& catName, 
                              HftModel* other, unsigned int nLines = 0, 
                              const RooAbsData::ErrorType& errorType = RooAbsData::Auto,
                              const TString& ranges = "");

     static bool MakeAnimation(std::vector<Hfitter::HftModel*>& models, const std::vector<TString>& labels, 
                               const TString& parName, unsigned int nBins, double parMin, double parMax,
                               const TString& outputFileName, double yMax = -1);

  };
}

#endif
