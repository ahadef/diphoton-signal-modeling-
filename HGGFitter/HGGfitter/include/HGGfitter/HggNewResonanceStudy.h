#ifndef ROOT_Hfitter_HggNewResonanceStudy
#define ROOT_Hfitter_HggNewResonanceStudy

#include "TString.h"
#include "TTree.h"
#include "RooArgList.h"
#include "RooAbsData.h"

#include <vector>

class RooRealVar;
class RooDataSet;
class RooDataHist;
class RooPlot;
class RooCurve;
class RooHist;
class TH1D;
class TCanvas;
class TVirtualPad;
class TGraphAsymmErrors;
class TGraphErrors;

namespace Hfitter {
  
  class HftModel;
  class HftScanStudy;
  class HftParameterStorage;
  
  class HggNewResonanceStudy {
    public:
     static void FitParameter(HftModel* hgg, const TString& varName, const std::vector<TString>& parNames, const TString& dataFileName, 
                              const std::vector<double>& masses, const std::vector<double>& ranges, const TString& outputName, 
                              const TString& treeName = "tree");
     static void FillHighMassExpPoints(std::vector<double>& masses, int range = 0, int version = 0);
     static void FillLowMassExpPoints(std::vector<double>& masses, int version = 99);

     static void FillHighMassObsPoints(std::vector<double>& masses, int range = 0, int version = 0);
     static void FillLowMassObsPoints(std::vector<double>& masses, int version = 99);

     static double GetRange(double mass, HftModel* hgg, int widthHypo = 0);
     static double GetAsimovNBkg(double mX, int widthHypo);

     static bool GetAsimovParamsW0(double mX, double& slope);
     static bool GetAsimovParamsW1(double mX, double& a1, double& a2);
     static bool GetAsimovParamsW3(double mX, double& a1, double& a2);
     static bool GetAsimovParamsW5(double mX, double& c1, double& c2, double& c3, double& c4);

     static double GetNormalization(RooAbsPdf& pdf, RooRealVar& dependent, const TString& range);
     static void GetRanges(const std::vector<double>& masses, HftModel* hgg,  std::vector<double>& ranges, int widthHypo = 0);
     static void PrepareModel(HftModel* hgg, double mass, int widthHypo = 0);
     static HftScanStudy* MakeHighMassLimitStudy(const std::vector<double>& massesObs, const std::vector<double>& massesExp, const TString& datacard, 
                                                 const TString& dataFileName, const TString& output, const TString& fitOptions = "NewRobustOffset", 
                                                 int widthHypo = 0, bool binned = false, bool smallTree = false, 
                                                 bool asimovFromData = false, bool fillSysts = false);
     static HftScanStudy* MakeHighMassP0Study(const std::vector<double>& massesObs, const std::vector<double>& massesExp, const TString& datacard, 
                                              const TString& dataFileName, const TString& output, const TString& fitOptions = "NewRobustOffset", 
                                              int widthHypo = 0, bool binned = false, const TString& weightVar = "");
     
     static bool SetHighMassExpectedNPs(HftParameterStorage& state);
     static void MakeSmallDataFileFromPresel(const TString& inputName, const TString& outputName, bool lowMass = false);
     static void MakeSmallDataFileFromNTUPxAOD(const TString& inputPath, const TString& outputName, bool lowMass = false);
        
     static HftScanStudy* MakeLowMassLimitStudy(const std::vector<double>& massesObs, const std::vector<double>& massesExp, const TString& datacard, 
                                                const TString& dataFileName, const TString& output, const TString& fitOptions = "NewRobustOffset", 
                                                bool binned = false, bool smallTree = false);
     static HftScanStudy* MakeLowMassP0Study(const std::vector<double>& massesObs, const std::vector<double>& massesExp, const TString& datacard, 
                                             const TString& dataFileName, const TString& output, const TString& fitOptions = "NewRobustOffset", 
                                             bool binned = false);
     static bool SetLowMassExpectedNPs(HftParameterStorage& state);

     static unsigned int MergePlots(const TString& highMassName = "LimitHighMass", const TString& lowMassName = "", unsigned int nRegions = 5);
     static bool MergeBkgDecompPlots(const TString& highMassName, const TString& lowMassName, double mSwitch);

     static bool MakeHighMassNPGraphs(const TString& datacard, double mass, const TString& dataFile, const TString& output, bool bkgOnly = false);
     static bool MakeLowMassNPGraphs(const TString& datacard, double mass, const TString& dataFile, const TString& output, bool bkgOnly = false);

     static bool MakeNPGraphs(HftModel& model, RooAbsData& data, const TString& poi, const TString& output, 
                              const TString& fitOptions = "", const TString& exclude = "");

     static bool MakeHighMassSmearedMCHistos(const TString& inputName, const TString& outputName, unsigned int nBins, double mMin, double mMax);
     
     static double ComputeSpuriousSignal(HftModel* hgg, double mX, double range, 
                                         const TString& dataFile, const TString& dataObj, double lumi, double targetLumi, double step = 1, double weight = -999);
     
     static double ComputeSpuriousSignalRange(HftModel* hgg, double mX, double rangeMin, double rangeMax, double rangeStep, 
                                              const TString& dataFile, const TString& dataObj, double lumi, double targetLumi, 
                                              unsigned int nSteps, double weight = -999);
  
     static bool MCSyst(HftModel* model, const TString& nomData, const TString& upData, const TString& dnData, const TString& name,
                        double mMin, double mMax, unsigned int nSteps, const TString& fitOptions, const std::vector<TString>& vars, const TString& output);
  };
}

#endif
