#ifndef ROOT_Hfitter_HggCouplings
#define ROOT_Hfitter_HggCouplings

namespace Hfitter {

  class HggCouplings {

   public:

     static double mu_t  (double kappa_t, double kappa_V);
     static double mu_VBF(double kappa_t, double kappa_V);
     static double mu_VH (double kappa_t, double kappa_V);
  };
}

#endif
