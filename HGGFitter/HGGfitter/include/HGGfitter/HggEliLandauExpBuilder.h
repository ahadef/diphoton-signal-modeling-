// $Id: HggEliLandauExpBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggEliLandauExpBuilder
#define ROOT_Hfitter_HggEliLandauExpBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggEliLandauExpBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggEliLandauExpBuilder() { }
    virtual ~HggEliLandauExpBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* landauMeanName = "landauMean", const char* landauSigmaName = "landauSigma", 
               const char* expSlopeName = "expSlope", const char* landauFracName = "landauFrac");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    TString m_depName, m_landauMeanName, m_landauSigmaName, m_expSlope, m_landauFracName;
    ClassDef(Hfitter::HggEliLandauExpBuilder, 0);
  };
}

#endif
