// Author: Marine Kuna

#ifndef ROOT_Hfitter_HggSigMggIsolHistoBuilder
#define ROOT_Hfitter_HggSigMggIsolHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggSigMggIsolHistoBuilder : public HftAbsPdfBuilder {
    
  public:
    
      
    HggSigMggIsolHistoBuilder() : m_pdfType(0) { }
    virtual ~HggSigMggIsolHistoBuilder() { }

    void Setup(const char* depName = "mgg", const char* depName1 = "isol1", const char* depName2 = "isol2",
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaName = "cbAlpha", const char* cbNName = "cbN",  
               const char* tailPeakName = "mTail", const char* tailSigmaName = "sigmaTail", const char* tailFractionName = "mggRelNorm", 
               unsigned int pdfType = 0, const char* cat = "");


    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    unsigned int m_pdfType;
    TString m_depName, m_depName1, m_depName2, m_cbPeakName, m_cbSigmaName, m_cbAlphaName, m_cbNName;
    TString m_tailPeakName, m_tailSigmaName, m_tailFractionName, m_cat;

    ClassDef(Hfitter::HggSigMggIsolHistoBuilder, 0);
  };
}

#endif
