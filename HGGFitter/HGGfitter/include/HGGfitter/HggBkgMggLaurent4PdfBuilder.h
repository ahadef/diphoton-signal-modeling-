// Author: Elisabeth Petit

// background PDF: Laurent series with 4 terms

#ifndef ROOT_Hfitter_HggBkgMggLaurent4PdfBuilder
#define ROOT_Hfitter_HggBkgMggLaurent4PdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggLaurent4PdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggLaurent4PdfBuilder() { }    
    virtual ~HggBkgMggLaurent4PdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggLaurent4PdfBuilder, 0);
  };
}

#endif
