// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggPolyPdfBuilder
#define ROOT_Hfitter_HggPolyPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggPolyPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggPolyPdfBuilder() { }    
    virtual ~HggPolyPdfBuilder() { }

    void Setup(const char* depName = "mgg",
               const char* c1Name = "c1", const char* c2Name = "", 
               const char* c3Name = "", const char* c4Name = "", 
               const char* c5Name = "", const char* c6Name = "");

    void Setup(const char* depName, double offset,
               const char* c1Name = "c1", const char* c2Name = "", 
               const char* c3Name = "", const char* c4Name = "", 
               const char* c5Name = "", const char* c6Name = "");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggPolyPdfBuilder, 0);

  private:
    
    TString m_depName;
    std::vector<TString> m_cNames;
  };
}

#endif
