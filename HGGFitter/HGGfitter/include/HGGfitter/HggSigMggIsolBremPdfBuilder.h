#ifndef ROOT_Hfitter_HggSigMggIsolBremPdfBuilder
#define ROOT_Hfitter_HggSigMggIsolBremPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggSigMggIsolPdfBuilder.h"

namespace Hfitter {
  
  class HggSigMggIsolBremPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggSigMggIsolBremPdfBuilder();
      
    virtual ~HggSigMggIsolBremPdfBuilder() { }

    void Setup(int brem1, int brem2) { m_brem1 = brem1; m_brem2 = brem2; }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggSigMggIsolPdfBuilder m_mggIsolPdfBuilder;
    bool m_brem1, m_brem2;
    
    ClassDef(Hfitter::HggSigMggIsolBremPdfBuilder, 0);
  };
}

#endif
