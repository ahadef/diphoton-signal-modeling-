// $Id: HggBkgMggExpPowerPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggExpPowerPdfBuilder
#define ROOT_Hfitter_HggBkgMggExpPowerPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggExpPowerPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggExpPowerPdfBuilder() { }    
    virtual ~HggBkgMggExpPowerPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* xiName = "xi", const char* powerName = "power", const char* coeffName = "coeff") 
    { m_depName = depName; m_xiName = xiName; m_powerName = powerName; m_coeffName = coeffName; }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggExpPowerPdfBuilder, 0);
    
  private:
    
    TString m_depName, m_xiName, m_powerName, m_coeffName;
    
  };
}

#endif
