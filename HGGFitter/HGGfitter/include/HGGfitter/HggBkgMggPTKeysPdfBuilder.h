// Author: Kerstin Tackmann

// Higgs bkg PDF

#ifndef ROOT_Hfitter_HggBkgMggPTKeysPdfBuilder
#define ROOT_Hfitter_HggBkgMggPTKeysPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"

namespace Hfitter {

  class HggBkgMggPTKeysPdfBuilder : public HftAbsPdfBuilder {

  public:

    HggBkgMggPTKeysPdfBuilder() { }
    virtual ~HggBkgMggPTKeysPdfBuilder() { }

    void Setup(char* fname, int fmode) { std::cout << "Setup was passed " << fname << "  " << fmode << std::endl; m_fName = fname; m_fMode = fmode;}

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:

    TString m_fName;
    int m_fMode;

    ClassDef(Hfitter::HggBkgMggPTKeysPdfBuilder, 0);
  };
}

#endif
