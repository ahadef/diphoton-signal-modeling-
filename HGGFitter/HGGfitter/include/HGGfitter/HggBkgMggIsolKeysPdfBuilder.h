// Author: Marine Kuna

#ifndef ROOT_Hfitter_HggBkgMggIsolKeysPdfBuilder
#define ROOT_Hfitter_HggBkgMggIsolKeysPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBkgMggPdfBuilder.h"
/*#include "HGGfitter/HggIsolPdfBuilder.h"*/

namespace Hfitter {
  
  class HggBkgMggIsolKeysPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggIsolKeysPdfBuilder();
    virtual ~HggBkgMggIsolKeysPdfBuilder() { }

   /*void Setup(bool isGam1, bool isGam2);*/
    void Setup(char* fname, int fmode) { std::cout << "Setup was passed " << fname << "  " << fmode << std::endl; m_fName = fname; m_fMode = fmode;}

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggBkgMggPdfBuilder m_mggPdfBuilder;
    TString m_fName;
    int m_fMode;
    
    ClassDef(Hfitter::HggBkgMggIsolKeysPdfBuilder, 0);
  };
}

#endif
