
#ifndef ROOT_Hfitter_HggResolHelper
#define ROOT_Hfitter_HggResolHelper

#include "TTree.h"
#include "TH2D.h"
#include "TString.h"

namespace Hfitter {

  class HggResolHelper {

   public:

    HggResolHelper(TTree* tree, const TString& var,
                   int nBins, double varMin, double varMax,
                   const TString& cut = "", const TString& title = "")
      : m_tree(tree), m_var(var), m_title(title), m_cut(cut),
        m_nBins(nBins), m_varMin(varMin), m_varMax(varMax) { }
      
    virtual ~HggResolHelper() { }
      
    TH1D* resolutionPlot();
      
    TH1D* occupancy;
    
   private:
     TTree* m_tree;
     TString m_var, m_title, m_cut;
     int m_nBins;
     double m_varMin, m_varMax;
  };
}

#endif
