// $Id: HggBkgMggBiasPdfBuilder.h,v 1.1 2007/01/16 09:53:14 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggBiasPdfBuilder
#define ROOT_Hfitter_HggBkgMggBiasPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggBiasPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggBiasPdfBuilder() { }
    
    virtual ~HggBkgMggBiasPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;    

    ClassDef(Hfitter::HggBkgMggBiasPdfBuilder, 0);
  };
}

#endif
