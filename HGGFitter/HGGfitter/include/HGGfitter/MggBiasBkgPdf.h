// $Id: MggBiasBkgPdf.h,v 1.4 2008/03/06 14:52:04 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs background PDF: f(x) = Exp(c*(x-offset)) --> good choice is: Exp(-0.02*(x-90))

#ifndef ROOT_Hfitter_MggBiasBkgPdf
#define ROOT_Hfitter_MggBiasBkgPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

namespace Hfitter {

   class MggBiasBkgPdf : public RooAbsPdf {

   public:
     
     MggBiasBkgPdf() { }
     MggBiasBkgPdf( const MggBiasBkgPdf& other, const char* name = 0 );
     MggBiasBkgPdf( const char* name, const char* title, 
                    RooAbsReal& mgg, RooAbsReal& c, RooAbsReal& offset, RooAbsReal& bias );
     
     inline virtual ~MggBiasBkgPdf() {}
     
     Int_t    getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0 ) const;
     Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;
     
     virtual TObject* clone( const char* newname ) const { return new MggBiasBkgPdf( *this, newname ); }

   protected:
      
     RooRealProxy m_mgg;
     RooRealProxy m_xi;
     RooRealProxy m_offset;
     RooRealProxy m_bias;
     
     Double_t evaluate() const;
      
   private:

     ClassDef(Hfitter::MggBiasBkgPdf, 1) 

   };
}

#endif
