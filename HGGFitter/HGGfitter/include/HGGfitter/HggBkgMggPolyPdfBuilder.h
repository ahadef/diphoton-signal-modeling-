// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggPolyPdfBuilder
#define ROOT_Hfitter_HggBkgMggPolyPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggPolyPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggPolyPdfBuilder() { }    
    virtual ~HggBkgMggPolyPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggPolyPdfBuilder, 0);
  };
}

#endif
