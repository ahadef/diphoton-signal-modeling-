#ifndef ROOT_Hfitter_HggCouplingsBuilder
#define ROOT_Hfitter_HggCouplingsBuilder

#include "HfitterModels/HftAbsRealBuilder.h"

namespace Hfitter {

  /** @class HggCouplingsBuilder
      @author Nicolas Berger
  */

  class HggCouplingsBuilder : public HftAbsRealBuilder {

   public:

    HggCouplingsBuilder() { }
    virtual ~HggCouplingsBuilder() { }

    void Setup(const char* type, const char* kappa_f_name, const char* kappa_V_name);
    
    RooAbsReal* Real(const TString& name, RooWorkspace& workspace) const;

  private:

    TString m_type, m_kappa_f_name, m_kappa_V_name;
    
    ClassDef(HggCouplingsBuilder, 1);
  };
}

#endif
