// $Id: HggSigMggCosThStarPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggSigMggCosThStarPdfBuilder
#define ROOT_Hfitter_HggSigMggCosThStarPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggBernsteinPdfBuilder.h"

namespace Hfitter {
  
  class HggSigMggCosThStarPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggSigMggCosThStarPdfBuilder() { }
      
    virtual ~HggSigMggCosThStarPdfBuilder() { }

    void Setup(const char* mggName = "mgg", 
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaName = "cbAlpha", const char* cbNName = "cbN",  
               const char* tailPeakName = "mTail", const char* tailSigmaName = "sigmaTail", const char* tailFractionName = "mggRelNorm",
               const char* cThName = "cosThStar", unsigned int cThOrder = 6, const char* cThNameRoot = "sigBernCTh") 
    { m_mggPdfBuilder.Setup(mggName, cbPeakName, cbSigmaName, cbAlphaName, cbNName, tailPeakName, tailSigmaName, tailFractionName); 
      m_cThPdfBuilder.Setup(cThName, cThOrder, cThNameRoot); }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggSigMggPdfBuilder m_mggPdfBuilder;
    HggBernsteinPdfBuilder m_cThPdfBuilder;

    ClassDef(Hfitter::HggSigMggCosThStarPdfBuilder, 0);
  };
}

#endif
