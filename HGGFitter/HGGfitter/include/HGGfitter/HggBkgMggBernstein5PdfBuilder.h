// Author: Elisabeth Petit

// Higgs signal PDF: 5th order Bernstein

#ifndef ROOT_Hfitter_HggBkgMggBernstein5PdfBuilder
#define ROOT_Hfitter_HggBkgMggBernstein5PdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggBernstein5PdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggBernstein5PdfBuilder() { }    
    virtual ~HggBkgMggBernstein5PdfBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* cs1Name = "cs1", const char* cs2Name = "cs2",
               const char* cs3Name = "cs3", const char* cs4Name = "cs4", const char* cs5Name = "cs5",
               double xMin = 0, double xMax = -1)
    { m_depName = depName; m_xMin = xMin; m_xMax = xMax;
      m_cs1Name = cs1Name; m_cs2Name = cs2Name; m_cs3Name = cs3Name; m_cs4Name = cs4Name; m_cs5Name = cs5Name; }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggBernstein5PdfBuilder, 0);

  private:
    
    TString m_depName, m_cs1Name, m_cs2Name, m_cs3Name, m_cs4Name, m_cs5Name;
    double m_xMin, m_xMax;
  };
}

#endif
