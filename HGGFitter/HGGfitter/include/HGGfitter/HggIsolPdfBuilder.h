// $Id: HggIsolPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggIsolPdfBuilder
#define ROOT_Hfitter_HggIsolPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"
#include "TString.h"

namespace Hfitter {
  
  class HggIsolPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggIsolPdfBuilder() { }   
    virtual ~HggIsolPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    void Setup(bool isGam, const TString& isolVar, const TString& suffix) { m_isGam = isGam; m_isolVar = isolVar; SetSuffix(suffix); }
    
  private:
    
    bool m_isGam;
    TString m_isolVar;    
                   
    ClassDef(Hfitter::HggIsolPdfBuilder, 0);
  };
}

#endif
