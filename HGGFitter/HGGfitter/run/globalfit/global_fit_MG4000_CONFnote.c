%!PS-Adobe-2.0
%%Title: globalfit/global_fit_MG4000_CONFnote.c ( A 4)
%%Pages: (atend)
%%Creator: ROOT Version 6.14/04
%%CreationDate: Mon Apr  8 17:34:04 2019
%%Orientation: Landscape
%%DocumentNeededResources: ProcSet (FontSetInit)
%%EndComments
%%BeginProlog
/s {stroke} def /l {lineto} def /m {moveto} def /t {translate} def
/r {rotate} def /rl {roll}  def /R {repeat} def
/d {rlineto} def /rm {rmoveto} def /gr {grestore} def /f {eofill} def
/c {setrgbcolor} def /black {0 setgray} def /sd {setdash} def
/cl {closepath} def /sf {scalefont setfont} def /lw {setlinewidth} def
/box {m dup 0 exch d exch 0 d 0 exch neg d cl} def
/NC{systemdict begin initclip end}def/C{NC box clip newpath}def
/bl {box s} def /bf {gsave box gsave f grestore 1 lw [] 0 sd s grestore} def /Y { 0 exch d} def /X { 0 d} def 
/K {{pop pop 0 moveto} exch kshow} bind def
/ita {/ang 15 def gsave [1 0 ang dup sin exch cos div 1 0 0] concat} def 
/mp {newpath /y exch def /x exch def} def
/side {[w .77 mul w .23 mul] .385 w mul sd w 0 l currentpoint t -144 r} def
/mr {mp x y w2 0 360 arc} def /m24 {mr s} def /m20 {mr f} def
/mb {mp x y w2 add m w2 neg 0 d 0 w neg d w 0 d 0 w d cl} def
/mt {mp x y w2 add m w2 neg w neg d w 0 d cl} def
/w4 {w 4 div} def
/w6 {w 6 div} def
/w8 {w 8 div} def
/m21 {mb f} def /m25 {mb s} def /m22 {mt f} def /m26{mt s} def
/m23 {mp x y w2 sub m w2 w d w neg 0 d cl f} def
/m27 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl s} def
/m28 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl s } def
/m29 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl fill gr} def
/m30 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl s gr} def
/m31 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m32 {mp x y w2 sub m w2 w d w neg 0 d cl s} def
/m33 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl f} def
/m34 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m35 {mp x y w2 add m w2 neg w2 neg d w2 w2 neg d w2 w2 d w2 neg w2 d x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m36 {mb x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m37 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl s} def
/m38 {mp x w4 sub y w2 add m w4 neg w4 neg d 0 w2 neg d w4 w4 neg d w2 0 d w4 w4 d 0 w2 d w4 neg w4 d w2 neg 0 d x y w2 sub m 0 w d x w2 sub y m w 0 d cl s} def
/m39 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl f} def
/m40 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl s} def
/m41 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl f} def
/m42 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl s} def
/m43 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl f} def
/m44 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl s} def
/m45 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl f} def
/m46 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl s} def
/m47 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d w4 neg w4 neg d  w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl f} def
/m48 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d  w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 neg w4 d w4 w4 d cl f} def
/m49 {mp x w2 sub w3 add y w2 sub w3 add m  0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d 0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m2 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m5 {mp x w2 sub y w2 sub m w w d x w2 sub y w2 add m w w neg d s} def
%%IncludeResource: ProcSet (FontSetInit)
%%IncludeResource: font Times-Roman
%%IncludeResource: font Times-Italic
%%IncludeResource: font Times-Bold
%%IncludeResource: font Times-BoldItalic
%%IncludeResource: font Helvetica
%%IncludeResource: font Helvetica-Oblique
%%IncludeResource: font Helvetica-Bold
%%IncludeResource: font Helvetica-BoldOblique
%%IncludeResource: font Courier
%%IncludeResource: font Courier-Oblique
%%IncludeResource: font Courier-Bold
%%IncludeResource: font Courier-BoldOblique
%%IncludeResource: font Symbol
%%IncludeResource: font ZapfDingbats
/reEncode {exch findfont dup length dict begin {1 index /FID eq  {pop pop} {def} ifelse } forall /Encoding exch def currentdict end dup /FontName get exch definefont pop } def [/Times-Bold /Times-Italic /Times-BoldItalic /Helvetica /Helvetica-Oblique
 /Helvetica-Bold /Helvetica-BoldOblique /Courier /Courier-Oblique /Courier-Bold /Courier-BoldOblique /Times-Roman /AvantGarde-Book /AvantGarde-BookOblique /AvantGarde-Demi /AvantGarde-DemiOblique /Bookman-Demi /Bookman-DemiItalic /Bookman-Light
 /Bookman-LightItalic /Helvetica-Narrow /Helvetica-Narrow-Bold /Helvetica-Narrow-BoldOblique /Helvetica-Narrow-Oblique /NewCenturySchlbk-Roman /NewCenturySchlbk-Bold /NewCenturySchlbk-BoldItalic /NewCenturySchlbk-Italic /Palatino-Bold
 /Palatino-BoldItalic /Palatino-Italic /Palatino-Roman ] {ISOLatin1Encoding reEncode } forall/Zone {/iy exch def /ix exch def  ix 1 sub  3144 mul  1 iy sub  2224 mul t} def
%%EndProlog
%%BeginSetup
%%EndSetup
newpath  gsave  90 r 0 -594 t  28 20 t .25 .25 scale  gsave 
%%Page: 1 1
 gsave  gsave 
 1 1 Zone
 gsave  0 0 t black[  ] 0 sd 3 lw 1 1 1 c 2948 2118 0 0 bf black 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s
 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530
 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218
 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364
 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s
 gsave  2948 2118 0 0 C 592.569 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (3600) show NC gr 
 gsave  2948 2118 0 0 C 855.522 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (3700) show NC gr 
 gsave  2948 2118 0 0 C 1122.18 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (3800) show NC gr 
 gsave  2948 2118 0 0 C 1385.13 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (3900) show NC gr 
 gsave  2948 2118 0 0 C 1651.79 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (4000) show NC gr 
 gsave  2948 2118 0 0 C 1914.74 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (4100) show NC gr 
 gsave  2948 2118 0 0 C 2181.4 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (4200) show NC gr 
 gsave  2948 2118 0 0 C 2444.35 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (4300) show NC gr 
 gsave  2948 2118 0 0 C 2711 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (4400) show NC gr 
 gsave  2948 2118 0 0 C 2573.97 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV]) show NC gr 
 gsave  2948 2118 0 0 C 2544.34 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2514.72 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2451.75 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054
 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742
 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430
 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 472 339 m
 1674 Y s 542 339 m -70 X s 507 383 m -35 X s 507 427 m -35 X s 507 472 m -35 X s 542 516 m -70 X s 507 560 m -35 X s 507 604 m -35 X s 507 649 m -35 X s 542 693 m -70 X s 507 737 m -35 X s 507 781 m -35 X s 507 826 m -35 X s 542 870 m -70 X s 507
 914 m -35 X s 507 958 m -35 X s 507 1003 m -35 X s 542 1047 m -70 X s 507 1091 m -35 X s 507 1135 m -35 X s 507 1180 m -35 X s 542 1224 m -70 X s 507 1268 m -35 X s 507 1312 m -35 X s 507 1357 m -35 X s 542 1401 m -70 X s 507 1445 m -35 X s 507 1489
 m -35 X s 507 1533 m -35 X s 542 1578 m -70 X s 507 1622 m -35 X s 507 1666 m -35 X s 507 1710 m -35 X s 542 1755 m -70 X s 507 1799 m -35 X s 507 1843 m -35 X s 507 1887 m -35 X s 542 1932 m -70 X s 542 1932 m -70 X s 507 1976 m -35 X s
 gsave  2948 2118 0 0 C 411.095 311.099 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0) show NC gr 
 gsave  2948 2118 0 0 C 299.988 488.87 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.02) show NC gr 
 gsave  2948 2118 0 0 C 296.285 666.64 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.04) show NC gr 
 gsave  2948 2118 0 0 C 299.988 840.707 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.06) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1018.48 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.08) show NC gr 
 gsave  2948 2118 0 0 C 359.245 1196.25 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.1) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1374.02 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.12) show NC gr 
 gsave  2948 2118 0 0 C 296.285 1551.79 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.14) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1725.86 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.16) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1903.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.18) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1996.22 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (]) show NC gr 
 gsave  2948 2118 0 0 C 218.51 1955.48 t 90 r /Helvetica findfont 55.5534 sf 0 0 m (\2551) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1748.08 t 90 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1718.45 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1688.82 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1303.65 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (1/N dN/dm) show NC gr  2801 339 m 1674 Y s 2731 339 m 70 X s 2766 383 m 35 X s 2766 427 m 35 X s 2766 472 m 35 X s 2731 516 m 70 X s 2766 560 m 35 X s 2766 604 m 35
 X s 2766 649 m 35 X s 2731 693 m 70 X s 2766 737 m 35 X s 2766 781 m 35 X s 2766 826 m 35 X s 2731 870 m 70 X s 2766 914 m 35 X s 2766 958 m 35 X s 2766 1003 m 35 X s 2731 1047 m 70 X s 2766 1091 m 35 X s 2766 1135 m 35 X s 2766 1180 m 35 X s 2731
 1224 m 70 X s 2766 1268 m 35 X s 2766 1312 m 35 X s 2766 1357 m 35 X s 2731 1401 m 70 X s 2766 1445 m 35 X s 2766 1489 m 35 X s 2766 1533 m 35 X s 2731 1578 m 70 X s 2766 1622 m 35 X s 2766 1666 m 35 X s 2766 1710 m 35 X s 2731 1755 m 70 X s 2766
 1799 m 35 X s 2766 1843 m 35 X s 2766 1887 m 35 X s 2731 1932 m 70 X s 2731 1932 m 70 X s 2766 1976 m 35 X s 1694 1310 m 1 Y s 1694 1311 m cl s 1694 1280 m s 1694 1280 m cl s 1718 1545 m 2 Y s 1718 1547 m cl s 1718 1516 m -2 Y s 1718 1514 m cl s
 1741 1593 m 3 Y s 1741 1596 m cl s 1741 1564 m -3 Y s 1741 1561 m cl s 1764 1459 m 1 Y s 1764 1460 m cl s 1764 1429 m -1 Y s 1764 1428 m cl s 1 1 1 c black /w 30 def /w2 {w 2 div} def /w3 {w 3 div} def 483 339 507 339 530 339 553 339 576 339 600 339
 623 339 646 339 670 339 693 339 716 339 740 339 763 339 786 339 809 339 833 339 856 339 879 339 903 339 926 339 20 { m20} R 972 339 996 339 1019 339 1042 339 1066 339 1089 339 1112 339 1135 339 1159 339 1182 340 1205 339 1229 340 1252 339 1275 341
 1298 340 1322 341 1345 343 1368 345 1392 346 1415 350 1438 351 1461 364 1485 367 1508 385 1531 401 1555 431 1578 491 1601 553 1625 679 1648 835 1671 1058 1694 1295 1718 1531 1741 1579 1764 1444 1788 1180 1811 917 1834 681 1857 516 1881 427 1904 384
 1927 364 1951 351 1974 346 1997 343 2020 344 2044 342 2067 340 2090 339 2114 340 2137 340 2160 340 2183 339 2207 339 2230 340 2253 339 2277 339 57 { m20} R 2323 339 2346 339 2370 339 2393 339 2416 339 2440 339 2463 339 2486 339 2510 339 2533 339
 2556 339 2579 339 2603 339 2626 339 2649 339 2673 339 2696 339 2719 339 2742 339 2766 339 2789 339 21 { m20} R 0 0 1 c 9 lw 1 1 1 c black 0 0 1 c 472 339 m cl s 472 339 m 675 X 23 1 d 70 X 24 1 d 23 X 23 1 d 23 1 d 24 2 d 23 2 d 23 2 d 24 4 d 23 6 d
 23 8 d 23 11 d 24 16 d 23 24 d 12 17 d 11 19 d 12 24 d 12 30 d 11 37 d 6 22 d 6 24 d 6 26 d 6 30 d 5 34 d 6 37 d 6 42 d 6 47 d 6 53 d 5 59 d 24 269 d 6 66 d 5 61 d 6 58 d 6 51 d 6 43 d 6 36 d 5 26 d 3 10 d 3 7 d 3 4 d 3 2 d 3 -1 d 3 -3 d 3 -6 d 3 -9
 d 3 -11 d 3 -13 d 5 -34 d 6 -42 d 6 -50 d 6 -56 d 6 -61 d 6 -65 d 11 -135 d 12 -135 d 6 -64 d 5 -62 d 6 -58 d 6 -54 d 6 -49 d 6 -44 d 6 -37 d 5 -32 d 6 -28 d 6 -23 d 6 -20 d 6 -17 d 11 -26 d 12 -20 d 12 -15 d 11 -11 d 12 -8 d 23 -10 d 23 -6 d 24 -3
 d 23 -2 d 23 -1 d 24 -1 d 23 X 23 -1 d 676 X s black 3 lw 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160
 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848
 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536
 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s 472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m
 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s
 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s
 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s
 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 383 m -35 X s 507 427 m -35 X s 507 472 m -35 X s 542 516 m -70 X s 507 560 m -35 X s 507 604 m -35 X s 507 649 m -35
 X s 542 693 m -70 X s 507 737 m -35 X s 507 781 m -35 X s 507 826 m -35 X s 542 870 m -70 X s 507 914 m -35 X s 507 958 m -35 X s 507 1003 m -35 X s 542 1047 m -70 X s 507 1091 m -35 X s 507 1135 m -35 X s 507 1180 m -35 X s 542 1224 m -70 X s 507
 1268 m -35 X s 507 1312 m -35 X s 507 1357 m -35 X s 542 1401 m -70 X s 507 1445 m -35 X s 507 1489 m -35 X s 507 1533 m -35 X s 542 1578 m -70 X s 507 1622 m -35 X s 507 1666 m -35 X s 507 1710 m -35 X s 542 1755 m -70 X s 507 1799 m -35 X s 507
 1843 m -35 X s 507 1887 m -35 X s 542 1932 m -70 X s 542 1932 m -70 X s 507 1976 m -35 X s 2801 339 m 1674 Y s 2731 339 m 70 X s 2766 383 m 35 X s 2766 427 m 35 X s 2766 472 m 35 X s 2731 516 m 70 X s 2766 560 m 35 X s 2766 604 m 35 X s 2766 649 m
 35 X s 2731 693 m 70 X s 2766 737 m 35 X s 2766 781 m 35 X s 2766 826 m 35 X s 2731 870 m 70 X s 2766 914 m 35 X s 2766 958 m 35 X s 2766 1003 m 35 X s 2731 1047 m 70 X s 2766 1091 m 35 X s 2766 1135 m 35 X s 2766 1180 m 35 X s 2731 1224 m 70 X s
 2766 1268 m 35 X s 2766 1312 m 35 X s 2766 1357 m 35 X s 2731 1401 m 70 X s 2766 1445 m 35 X s 2766 1489 m 35 X s 2766 1533 m 35 X s 2731 1578 m 70 X s 2766 1622 m 35 X s 2766 1666 m 35 X s 2766 1710 m 35 X s 2731 1755 m 70 X s 2766 1799 m 35 X s
 2766 1843 m 35 X s 2766 1887 m 35 X s 2731 1932 m 70 X s 2731 1932 m 70 X s 2766 1976 m 35 X s
 gsave  2948 2118 0 0 C 588.866 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (ATLAS) show NC gr 
 gsave  2948 2118 0 0 C 981.443 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (Simulation Preliminary) show NC gr 
 gsave  2948 2118 0 0 C 1251.8 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1211.06 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1129.58 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (�) show NC gr 
 gsave  2948 2118 0 0 C 674.047 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 13 TeV, X) show NC gr 
 gsave  2948 2118 0 0 C 633.308 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (s) show NC gr  6 lw 604 1700 m 11 -48 d s 3 lw 615 1652 m 7 70 d s 622 1722 m 52 X s
 gsave  2948 2118 0 0 C 588.866 1359.21 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (chi2 =6.89 ) show NC gr 
 gsave  2948 2118 0 0 C 688.862 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 4000 GeV) show NC gr 
 gsave  2948 2118 0 0 C 651.826 1485.13 t 0 r /Helvetica findfont 55.5534 sf 0 0 m (X) show NC gr 
 gsave  2948 2118 0 0 C 588.866 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  gr showpage
 gr 
%%Trailer
%%Pages:  1
 gr  gr  gr 
%%EOF
