%!PS-Adobe-2.0
%%Title: globalfit/global_fit_MG200_CONFnote.c ( A 4)
%%Pages: (atend)
%%Creator: ROOT Version 6.14/04
%%CreationDate: Mon Apr  8 17:33:23 2019
%%Orientation: Landscape
%%DocumentNeededResources: ProcSet (FontSetInit)
%%EndComments
%%BeginProlog
/s {stroke} def /l {lineto} def /m {moveto} def /t {translate} def
/r {rotate} def /rl {roll}  def /R {repeat} def
/d {rlineto} def /rm {rmoveto} def /gr {grestore} def /f {eofill} def
/c {setrgbcolor} def /black {0 setgray} def /sd {setdash} def
/cl {closepath} def /sf {scalefont setfont} def /lw {setlinewidth} def
/box {m dup 0 exch d exch 0 d 0 exch neg d cl} def
/NC{systemdict begin initclip end}def/C{NC box clip newpath}def
/bl {box s} def /bf {gsave box gsave f grestore 1 lw [] 0 sd s grestore} def /Y { 0 exch d} def /X { 0 d} def 
/K {{pop pop 0 moveto} exch kshow} bind def
/ita {/ang 15 def gsave [1 0 ang dup sin exch cos div 1 0 0] concat} def 
/mp {newpath /y exch def /x exch def} def
/side {[w .77 mul w .23 mul] .385 w mul sd w 0 l currentpoint t -144 r} def
/mr {mp x y w2 0 360 arc} def /m24 {mr s} def /m20 {mr f} def
/mb {mp x y w2 add m w2 neg 0 d 0 w neg d w 0 d 0 w d cl} def
/mt {mp x y w2 add m w2 neg w neg d w 0 d cl} def
/w4 {w 4 div} def
/w6 {w 6 div} def
/w8 {w 8 div} def
/m21 {mb f} def /m25 {mb s} def /m22 {mt f} def /m26{mt s} def
/m23 {mp x y w2 sub m w2 w d w neg 0 d cl f} def
/m27 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl s} def
/m28 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl s } def
/m29 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl fill gr} def
/m30 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl s gr} def
/m31 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m32 {mp x y w2 sub m w2 w d w neg 0 d cl s} def
/m33 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl f} def
/m34 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m35 {mp x y w2 add m w2 neg w2 neg d w2 w2 neg d w2 w2 d w2 neg w2 d x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m36 {mb x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m37 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl s} def
/m38 {mp x w4 sub y w2 add m w4 neg w4 neg d 0 w2 neg d w4 w4 neg d w2 0 d w4 w4 d 0 w2 d w4 neg w4 d w2 neg 0 d x y w2 sub m 0 w d x w2 sub y m w 0 d cl s} def
/m39 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl f} def
/m40 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl s} def
/m41 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl f} def
/m42 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl s} def
/m43 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl f} def
/m44 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl s} def
/m45 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl f} def
/m46 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl s} def
/m47 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d w4 neg w4 neg d  w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl f} def
/m48 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d  w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 neg w4 d w4 w4 d cl f} def
/m49 {mp x w2 sub w3 add y w2 sub w3 add m  0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d 0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m2 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m5 {mp x w2 sub y w2 sub m w w d x w2 sub y w2 add m w w neg d s} def
%%IncludeResource: ProcSet (FontSetInit)
%%IncludeResource: font Times-Roman
%%IncludeResource: font Times-Italic
%%IncludeResource: font Times-Bold
%%IncludeResource: font Times-BoldItalic
%%IncludeResource: font Helvetica
%%IncludeResource: font Helvetica-Oblique
%%IncludeResource: font Helvetica-Bold
%%IncludeResource: font Helvetica-BoldOblique
%%IncludeResource: font Courier
%%IncludeResource: font Courier-Oblique
%%IncludeResource: font Courier-Bold
%%IncludeResource: font Courier-BoldOblique
%%IncludeResource: font Symbol
%%IncludeResource: font ZapfDingbats
/reEncode {exch findfont dup length dict begin {1 index /FID eq  {pop pop} {def} ifelse } forall /Encoding exch def currentdict end dup /FontName get exch definefont pop } def [/Times-Bold /Times-Italic /Times-BoldItalic /Helvetica /Helvetica-Oblique
 /Helvetica-Bold /Helvetica-BoldOblique /Courier /Courier-Oblique /Courier-Bold /Courier-BoldOblique /Times-Roman /AvantGarde-Book /AvantGarde-BookOblique /AvantGarde-Demi /AvantGarde-DemiOblique /Bookman-Demi /Bookman-DemiItalic /Bookman-Light
 /Bookman-LightItalic /Helvetica-Narrow /Helvetica-Narrow-Bold /Helvetica-Narrow-BoldOblique /Helvetica-Narrow-Oblique /NewCenturySchlbk-Roman /NewCenturySchlbk-Bold /NewCenturySchlbk-BoldItalic /NewCenturySchlbk-Italic /Palatino-Bold
 /Palatino-BoldItalic /Palatino-Italic /Palatino-Roman ] {ISOLatin1Encoding reEncode } forall/Zone {/iy exch def /ix exch def  ix 1 sub  3144 mul  1 iy sub  2224 mul t} def
%%EndProlog
%%BeginSetup
%%EndSetup
newpath  gsave  90 r 0 -594 t  28 20 t .25 .25 scale  gsave 
%%Page: 1 1
 gsave  gsave 
 1 1 Zone
 gsave  0 0 t black[  ] 0 sd 3 lw 1 1 1 c 2948 2118 0 0 bf black 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s
 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530
 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218
 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364
 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s 2801 389 m -50 Y s
 gsave  2948 2118 0 0 C 614.79 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (180) show NC gr 
 gsave  2948 2118 0 0 C 877.743 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (185) show NC gr 
 gsave  2948 2118 0 0 C 1144.4 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (190) show NC gr 
 gsave  2948 2118 0 0 C 1407.35 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (195) show NC gr 
 gsave  2948 2118 0 0 C 1674.01 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (200) show NC gr 
 gsave  2948 2118 0 0 C 1936.96 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (205) show NC gr 
 gsave  2948 2118 0 0 C 2203.62 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (210) show NC gr 
 gsave  2948 2118 0 0 C 2466.57 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (215) show NC gr 
 gsave  2948 2118 0 0 C 2733.23 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (220) show NC gr 
 gsave  2948 2118 0 0 C 2573.97 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV]) show NC gr 
 gsave  2948 2118 0 0 C 2544.34 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2514.72 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2451.75 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054
 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742
 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430
 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 2801 1962 m
 51 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 422 m -35 X s 507 506 m -35 X s 507 590 m -35 X s 542 673 m -70 X s 507 757 m -35 X s 507 840 m -35 X s 507 924 m -35 X s 542 1007 m -70 X s 507 1091 m -35 X s 507 1174 m -35 X s 507 1258 m -35 X s 542
 1342 m -70 X s 507 1425 m -35 X s 507 1509 m -35 X s 507 1592 m -35 X s 542 1676 m -70 X s 507 1759 m -35 X s 507 1843 m -35 X s 507 1926 m -35 X s 542 2010 m -70 X s 542 2010 m -70 X s
 gsave  2948 2118 0 0 C 411.095 311.099 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0) show NC gr 
 gsave  2948 2118 0 0 C 299.988 644.419 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.02) show NC gr 
 gsave  2948 2118 0 0 C 296.285 977.739 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.04) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1314.76 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.06) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1648.08 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.08) show NC gr 
 gsave  2948 2118 0 0 C 359.245 1981.4 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.1) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1996.22 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (]) show NC gr 
 gsave  2948 2118 0 0 C 218.51 1955.48 t 90 r /Helvetica findfont 55.5534 sf 0 0 m (\2551) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1748.08 t 90 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1718.45 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1688.82 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1303.65 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (1/N dN/dm) show NC gr  2801 339 m 1674 Y s 2731 339 m 70 X s 2766 422 m 35 X s 2766 506 m 35 X s 2766 590 m 35 X s 2731 673 m 70 X s 2766 757 m 35 X s 2766 840 m 35
 X s 2766 924 m 35 X s 2731 1007 m 70 X s 2766 1091 m 35 X s 2766 1174 m 35 X s 2766 1258 m 35 X s 2731 1342 m 70 X s 2766 1425 m 35 X s 2766 1509 m 35 X s 2766 1592 m 35 X s 2731 1676 m 70 X s 2766 1759 m 35 X s 2766 1843 m 35 X s 2766 1926 m 35 X s
 2731 2010 m 70 X s 2731 2010 m 70 X s 1578 831 m 1 Y s 1578 832 m cl s 1578 801 m -2 Y s 1578 799 m cl s 1601 947 m 3 Y s 1601 950 m cl s 1601 918 m -4 Y s 1601 914 m cl s 1625 1095 m 5 Y s 1625 1100 m cl s 1625 1065 m -6 Y s 1625 1059 m cl s 1648
 1199 m 8 Y s 1648 1207 m cl s 1648 1170 m -8 Y s 1648 1162 m cl s 1671 1399 m 10 Y s 1671 1409 m cl s 1671 1370 m -10 Y s 1671 1360 m cl s 1694 1474 m 11 Y s 1694 1485 m cl s 1694 1445 m -11 Y s 1694 1434 m cl s 1718 1581 m 11 Y s 1718 1592 m cl s
 1718 1551 m -11 Y s 1718 1540 m cl s 1741 1593 m 12 Y s 1741 1605 m cl s 1741 1564 m -12 Y s 1741 1552 m cl s 1764 1576 m 12 Y s 1764 1588 m cl s 1764 1547 m -12 Y s 1764 1535 m cl s 1788 1555 m 12 Y s 1788 1567 m cl s 1788 1526 m -11 Y s 1788 1515
 m cl s 1811 1442 m 10 Y s 1811 1452 m cl s 1811 1412 m -9 Y s 1811 1403 m cl s 1834 1283 m 8 Y s 1834 1291 m cl s 1834 1254 m -8 Y s 1834 1246 m cl s 1857 1123 m 6 Y s 1857 1129 m cl s 1857 1093 m -6 Y s 1857 1087 m cl s 1881 988 m 4 Y s 1881 992 m
 cl s 1881 958 m -4 Y s 1881 954 m cl s 1904 891 m 3 Y s 1904 894 m cl s 1904 862 m -3 Y s 1904 859 m cl s 1927 771 m s 1927 771 m cl s 1927 741 m s 1927 741 m cl s 1 1 1 c black /w 30 def /w2 {w 2 div} def /w3 {w 3 div} def 507 340 530 339 553 339
 576 339 600 340 623 339 646 339 670 340 693 339 716 341 740 339 763 339 786 339 809 340 833 340 856 341 879 339 903 340 926 339 949 341 972 341 996 340 1019 342 1042 342 1066 344 1089 345 1112 345 1135 342 1159 347 1182 346 1205 350 1229 351 1252
 353 1275 351 1298 355 1322 362 1345 368 1368 372 1392 384 1415 407 1438 433 1461 441 1485 488 1508 544 1531 596 1555 686 1578 816 1601 932 1625 1080 1648 1184 1671 1384 1694 1459 1718 1566 1741 1579 1764 1562 1788 1541 1811 1427 1834 1269 1857 1108
 1881 973 1904 876 1927 756 1951 623 1974 564 1997 496 2020 469 2044 423 2067 406 2090 394 2114 382 2137 364 2160 357 2183 351 2207 350 2230 345 2253 343 2277 342 2300 345 2323 342 2346 342 2370 340 2393 341 2416 340 2440 340 2463 339 2486 340 2510
 340 2533 339 2556 339 2579 339 2603 339 2626 339 2649 339 2673 339 2696 339 2719 339 2742 339 2766 339 2789 339 99 { m20} R 0 0 1 c 9 lw 1 1 1 c black 0 0 1 c 472 339 m cl s 472 339 m 326 X 23 1 d 140 X 23 1 d 23 X 24 1 d 23 X 23 1 d 24 X 23 1 d 23
 1 d 23 2 d 24 1 d 23 3 d 23 2 d 24 3 d 23 5 d 23 5 d 23 6 d 24 8 d 23 11 d 23 13 d 24 18 d 23 22 d 23 29 d 12 18 d 11 21 d 12 23 d 12 27 d 11 31 d 12 35 d 12 41 d 11 47 d 12 55 d 12 62 d 11 67 d 12 69 d 23 144 d 23 141 d 12 66 d 12 61 d 11 54 d 12
 46 d 12 37 d 5 15 d 6 12 d 6 9 d 6 6 d 6 4 d 6 1 d 5 -3 d 6 -5 d 6 -8 d 6 -10 d 6 -14 d 6 -16 d 11 -39 d 12 -49 d 11 -56 d 12 -62 d 12 -67 d 23 -142 d 23 -143 d 12 -69 d 12 -66 d 11 -61 d 12 -57 d 12 -49 d 11 -42 d 12 -36 d 11 -31 d 12 -27 d 12 -23
 d 11 -20 d 12 -17 d 23 -28 d 24 -21 d 23 -15 d 23 -12 d 24 -9 d 23 -7 d 23 -5 d 23 -4 d 24 -3 d 23 -2 d 23 -2 d 24 -1 d 23 -1 d 23 -1 d 23 -1 d 24 X 23 -1 d 93 X 23 -1 d 257 X s black 3 lw 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789
 364 m -25 Y s 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m
 -50 Y s 1530 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m
 -25 Y s 2218 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50
 Y s 630 364 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s 2801 389 m -50 Y s 472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054
 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742
 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430
 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 2801 1962 m
 51 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 422 m -35 X s 507 506 m -35 X s 507 590 m -35 X s 542 673 m -70 X s 507 757 m -35 X s 507 840 m -35 X s 507 924 m -35 X s 542 1007 m -70 X s 507 1091 m -35 X s 507 1174 m -35 X s 507 1258 m -35 X s 542
 1342 m -70 X s 507 1425 m -35 X s 507 1509 m -35 X s 507 1592 m -35 X s 542 1676 m -70 X s 507 1759 m -35 X s 507 1843 m -35 X s 507 1926 m -35 X s 542 2010 m -70 X s 542 2010 m -70 X s 2801 339 m 1674 Y s 2731 339 m 70 X s 2766 422 m 35 X s 2766
 506 m 35 X s 2766 590 m 35 X s 2731 673 m 70 X s 2766 757 m 35 X s 2766 840 m 35 X s 2766 924 m 35 X s 2731 1007 m 70 X s 2766 1091 m 35 X s 2766 1174 m 35 X s 2766 1258 m 35 X s 2731 1342 m 70 X s 2766 1425 m 35 X s 2766 1509 m 35 X s 2766 1592 m
 35 X s 2731 1676 m 70 X s 2766 1759 m 35 X s 2766 1843 m 35 X s 2766 1926 m 35 X s 2731 2010 m 70 X s 2731 2010 m 70 X s
 gsave  2948 2118 0 0 C 588.866 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (ATLAS) show NC gr 
 gsave  2948 2118 0 0 C 981.443 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (Simulation Preliminary) show NC gr 
 gsave  2948 2118 0 0 C 1251.8 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1211.06 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1129.58 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (�) show NC gr 
 gsave  2948 2118 0 0 C 674.047 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 13 TeV, X) show NC gr 
 gsave  2948 2118 0 0 C 633.308 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (s) show NC gr  6 lw 604 1700 m 11 -48 d s 3 lw 615 1652 m 7 70 d s 622 1722 m 52 X s
 gsave  2948 2118 0 0 C 588.866 1359.21 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (chi2 =2.28 ) show NC gr 
 gsave  2948 2118 0 0 C 688.862 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 200 GeV) show NC gr 
 gsave  2948 2118 0 0 C 651.826 1485.13 t 0 r /Helvetica findfont 55.5534 sf 0 0 m (X) show NC gr 
 gsave  2948 2118 0 0 C 588.866 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  gr showpage
 gr 
%%Trailer
%%Pages:  1
 gr  gr  gr 
%%EOF
