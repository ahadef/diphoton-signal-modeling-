%!PS-Adobe-2.0
%%Title: globalfit/global_fit_MG800_CONFnote.c ( A 4)
%%Pages: (atend)
%%Creator: ROOT Version 6.14/04
%%CreationDate: Mon Apr  8 17:33:32 2019
%%Orientation: Landscape
%%DocumentNeededResources: ProcSet (FontSetInit)
%%EndComments
%%BeginProlog
/s {stroke} def /l {lineto} def /m {moveto} def /t {translate} def
/r {rotate} def /rl {roll}  def /R {repeat} def
/d {rlineto} def /rm {rmoveto} def /gr {grestore} def /f {eofill} def
/c {setrgbcolor} def /black {0 setgray} def /sd {setdash} def
/cl {closepath} def /sf {scalefont setfont} def /lw {setlinewidth} def
/box {m dup 0 exch d exch 0 d 0 exch neg d cl} def
/NC{systemdict begin initclip end}def/C{NC box clip newpath}def
/bl {box s} def /bf {gsave box gsave f grestore 1 lw [] 0 sd s grestore} def /Y { 0 exch d} def /X { 0 d} def 
/K {{pop pop 0 moveto} exch kshow} bind def
/ita {/ang 15 def gsave [1 0 ang dup sin exch cos div 1 0 0] concat} def 
/mp {newpath /y exch def /x exch def} def
/side {[w .77 mul w .23 mul] .385 w mul sd w 0 l currentpoint t -144 r} def
/mr {mp x y w2 0 360 arc} def /m24 {mr s} def /m20 {mr f} def
/mb {mp x y w2 add m w2 neg 0 d 0 w neg d w 0 d 0 w d cl} def
/mt {mp x y w2 add m w2 neg w neg d w 0 d cl} def
/w4 {w 4 div} def
/w6 {w 6 div} def
/w8 {w 8 div} def
/m21 {mb f} def /m25 {mb s} def /m22 {mt f} def /m26{mt s} def
/m23 {mp x y w2 sub m w2 w d w neg 0 d cl f} def
/m27 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl s} def
/m28 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl s } def
/m29 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl fill gr} def
/m30 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl s gr} def
/m31 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m32 {mp x y w2 sub m w2 w d w neg 0 d cl s} def
/m33 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl f} def
/m34 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m35 {mp x y w2 add m w2 neg w2 neg d w2 w2 neg d w2 w2 d w2 neg w2 d x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m36 {mb x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m37 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl s} def
/m38 {mp x w4 sub y w2 add m w4 neg w4 neg d 0 w2 neg d w4 w4 neg d w2 0 d w4 w4 d 0 w2 d w4 neg w4 d w2 neg 0 d x y w2 sub m 0 w d x w2 sub y m w 0 d cl s} def
/m39 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl f} def
/m40 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl s} def
/m41 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl f} def
/m42 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl s} def
/m43 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl f} def
/m44 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl s} def
/m45 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl f} def
/m46 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl s} def
/m47 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d w4 neg w4 neg d  w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl f} def
/m48 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d  w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 neg w4 d w4 w4 d cl f} def
/m49 {mp x w2 sub w3 add y w2 sub w3 add m  0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d 0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m2 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m5 {mp x w2 sub y w2 sub m w w d x w2 sub y w2 add m w w neg d s} def
%%IncludeResource: ProcSet (FontSetInit)
%%IncludeResource: font Times-Roman
%%IncludeResource: font Times-Italic
%%IncludeResource: font Times-Bold
%%IncludeResource: font Times-BoldItalic
%%IncludeResource: font Helvetica
%%IncludeResource: font Helvetica-Oblique
%%IncludeResource: font Helvetica-Bold
%%IncludeResource: font Helvetica-BoldOblique
%%IncludeResource: font Courier
%%IncludeResource: font Courier-Oblique
%%IncludeResource: font Courier-Bold
%%IncludeResource: font Courier-BoldOblique
%%IncludeResource: font Symbol
%%IncludeResource: font ZapfDingbats
/reEncode {exch findfont dup length dict begin {1 index /FID eq  {pop pop} {def} ifelse } forall /Encoding exch def currentdict end dup /FontName get exch definefont pop } def [/Times-Bold /Times-Italic /Times-BoldItalic /Helvetica /Helvetica-Oblique
 /Helvetica-Bold /Helvetica-BoldOblique /Courier /Courier-Oblique /Courier-Bold /Courier-BoldOblique /Times-Roman /AvantGarde-Book /AvantGarde-BookOblique /AvantGarde-Demi /AvantGarde-DemiOblique /Bookman-Demi /Bookman-DemiItalic /Bookman-Light
 /Bookman-LightItalic /Helvetica-Narrow /Helvetica-Narrow-Bold /Helvetica-Narrow-BoldOblique /Helvetica-Narrow-Oblique /NewCenturySchlbk-Roman /NewCenturySchlbk-Bold /NewCenturySchlbk-BoldItalic /NewCenturySchlbk-Italic /Palatino-Bold
 /Palatino-BoldItalic /Palatino-Italic /Palatino-Roman ] {ISOLatin1Encoding reEncode } forall/Zone {/iy exch def /ix exch def  ix 1 sub  3144 mul  1 iy sub  2224 mul t} def
%%EndProlog
%%BeginSetup
%%EndSetup
newpath  gsave  90 r 0 -594 t  28 20 t .25 .25 scale  gsave 
%%Page: 1 1
 gsave  gsave 
 1 1 Zone
 gsave  0 0 t black[  ] 0 sd 3 lw 1 1 1 c 2948 2118 0 0 bf black 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 472 339 m 2329 X s 683 389 m -50 Y s 750 364 m -25 Y s 816 364 m -25 Y s
 882 364 m -25 Y s 948 389 m -50 Y s 1014 364 m -25 Y s 1080 364 m -25 Y s 1147 364 m -25 Y s 1213 389 m -50 Y s 1279 364 m -25 Y s 1345 364 m -25 Y s 1411 364 m -25 Y s 1477 389 m -50 Y s 1544 364 m -25 Y s 1610 364 m -25 Y s 1676 364 m -25 Y s 1742
 389 m -50 Y s 1808 364 m -25 Y s 1874 364 m -25 Y s 1941 364 m -25 Y s 2007 389 m -50 Y s 2073 364 m -25 Y s 2139 364 m -25 Y s 2205 364 m -25 Y s 2271 389 m -50 Y s 2337 364 m -25 Y s 2404 364 m -25 Y s 2470 364 m -25 Y s 2536 389 m -50 Y s 2602
 364 m -25 Y s 2668 364 m -25 Y s 2734 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 617 364 m -25 Y s 551 364 m -25 Y s 485 364 m -25 Y s 2801 389 m -50 Y s
 gsave  2948 2118 0 0 C 614.79 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (720) show NC gr 
 gsave  2948 2118 0 0 C 877.743 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (740) show NC gr 
 gsave  2948 2118 0 0 C 1144.4 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (760) show NC gr 
 gsave  2948 2118 0 0 C 1407.35 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (780) show NC gr 
 gsave  2948 2118 0 0 C 1674.01 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (800) show NC gr 
 gsave  2948 2118 0 0 C 1936.96 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (820) show NC gr 
 gsave  2948 2118 0 0 C 2203.62 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (840) show NC gr 
 gsave  2948 2118 0 0 C 2466.57 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (860) show NC gr 
 gsave  2948 2118 0 0 C 2733.23 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (880) show NC gr 
 gsave  2948 2118 0 0 C 2573.97 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV]) show NC gr 
 gsave  2948 2118 0 0 C 2544.34 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2514.72 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2451.75 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  472 2013 m 2329 X s 683 1962 m 51 Y s 750 1987 m 26 Y s 816 1987 m 26 Y s 882 1987 m 26 Y s 948 1962 m 51 Y s 1014 1987 m 26 Y s 1080 1987 m 26 Y s 1147
 1987 m 26 Y s 1213 1962 m 51 Y s 1279 1987 m 26 Y s 1345 1987 m 26 Y s 1411 1987 m 26 Y s 1477 1962 m 51 Y s 1544 1987 m 26 Y s 1610 1987 m 26 Y s 1676 1987 m 26 Y s 1742 1962 m 51 Y s 1808 1987 m 26 Y s 1874 1987 m 26 Y s 1941 1987 m 26 Y s 2007
 1962 m 51 Y s 2073 1987 m 26 Y s 2139 1987 m 26 Y s 2205 1987 m 26 Y s 2271 1962 m 51 Y s 2337 1987 m 26 Y s 2404 1987 m 26 Y s 2470 1987 m 26 Y s 2536 1962 m 51 Y s 2602 1987 m 26 Y s 2668 1987 m 26 Y s 2734 1987 m 26 Y s 2801 1962 m 51 Y s 683
 1962 m 51 Y s 617 1987 m 26 Y s 551 1987 m 26 Y s 485 1987 m 26 Y s 2801 1962 m 51 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 391 m -35 X s 507 443 m -35 X s 507 495 m -35 X s 542 547 m -70 X s 507 599 m -35 X s 507 652 m -35 X s 507 704 m -35 X s
 542 756 m -70 X s 507 808 m -35 X s 507 860 m -35 X s 507 912 m -35 X s 542 964 m -70 X s 507 1016 m -35 X s 507 1068 m -35 X s 507 1121 m -35 X s 542 1173 m -70 X s 507 1225 m -35 X s 507 1277 m -35 X s 507 1329 m -35 X s 542 1381 m -70 X s 507
 1433 m -35 X s 507 1485 m -35 X s 507 1537 m -35 X s 542 1590 m -70 X s 507 1642 m -35 X s 507 1694 m -35 X s 507 1746 m -35 X s 542 1798 m -70 X s 507 1850 m -35 X s 507 1902 m -35 X s 507 1954 m -35 X s 542 2006 m -70 X s 542 2006 m -70 X s
 gsave  2948 2118 0 0 C 411.095 311.099 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0) show NC gr 
 gsave  2948 2118 0 0 C 299.988 518.498 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.02) show NC gr 
 gsave  2948 2118 0 0 C 296.285 729.601 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.04) show NC gr 
 gsave  2948 2118 0 0 C 299.988 937 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.06) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1144.4 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.08) show NC gr 
 gsave  2948 2118 0 0 C 359.245 1351.8 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.1) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1562.9 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.12) show NC gr 
 gsave  2948 2118 0 0 C 296.285 1770.3 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.14) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1977.7 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.16) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1996.22 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (]) show NC gr 
 gsave  2948 2118 0 0 C 218.51 1955.48 t 90 r /Helvetica findfont 55.5534 sf 0 0 m (\2551) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1748.08 t 90 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1718.45 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1688.82 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1303.65 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (1/N dN/dm) show NC gr  2801 339 m 1674 Y s 2731 339 m 70 X s 2766 391 m 35 X s 2766 443 m 35 X s 2766 495 m 35 X s 2731 547 m 70 X s 2766 599 m 35 X s 2766 652 m 35
 X s 2766 704 m 35 X s 2731 756 m 70 X s 2766 808 m 35 X s 2766 860 m 35 X s 2766 912 m 35 X s 2731 964 m 70 X s 2766 1016 m 35 X s 2766 1068 m 35 X s 2766 1121 m 35 X s 2731 1173 m 70 X s 2766 1225 m 35 X s 2766 1277 m 35 X s 2766 1329 m 35 X s 2731
 1381 m 70 X s 2766 1433 m 35 X s 2766 1485 m 35 X s 2766 1537 m 35 X s 2731 1590 m 70 X s 2766 1642 m 35 X s 2766 1694 m 35 X s 2766 1746 m 35 X s 2731 1798 m 70 X s 2766 1850 m 35 X s 2766 1902 m 35 X s 2766 1954 m 35 X s 2731 2006 m 70 X s 2731
 2006 m 70 X s 1694 1350 m 1 Y s 1694 1351 m cl s 1694 1321 m -1 Y s 1694 1320 m cl s 1718 1480 m 2 Y s 1718 1482 m cl s 1718 1451 m -2 Y s 1718 1449 m cl s 1741 1593 m 3 Y s 1741 1596 m cl s 1741 1564 m -3 Y s 1741 1561 m cl s 1764 1528 m 2 Y s 1764
 1530 m cl s 1764 1498 m -2 Y s 1764 1496 m cl s 1788 1364 m 1 Y s 1788 1365 m cl s 1788 1334 m -1 Y s 1788 1333 m cl s 1 1 1 c black /w 30 def /w2 {w 2 div} def /w3 {w 3 div} def 483 339 507 339 530 339 553 339 576 339 600 339 623 339 646 339 670
 339 693 339 716 339 740 339 763 339 786 339 809 339 833 339 856 339 879 339 903 339 926 339 949 339 972 339 996 339 1019 339 1042 340 1066 339 1089 339 1112 339 1135 340 1159 339 1182 339 1205 340 1229 340 1252 341 1275 342 1298 341 1322 343 1345
 344 1368 346 1392 349 1415 352 1438 358 1461 366 1485 379 1508 389 1531 424 1555 456 1578 508 1601 601 1625 724 1648 893 1671 1108 1694 1336 1718 1465 1741 1579 1764 1513 1788 1349 1811 1140 1834 922 1857 707 1881 565 1904 487 1927 421 1951 387 1974
 370 1997 359 2020 351 2044 349 2067 342 2090 342 2114 340 2137 340 2160 341 2183 339 2207 339 2230 339 2253 339 2277 339 2300 339 2323 339 2346 340 2370 339 2393 339 2416 339 2440 339 2463 339 2486 339 2510 339 2533 339 2556 339 2579 339 2603 339
 2626 339 2649 339 2673 339 2696 339 2719 339 2742 339 2766 339 2789 339 100 { m20} R 0 0 1 c 9 lw 1 1 1 c black 0 0 1 c 472 339 m cl s 472 339 m 652 X 23 1 d 70 X 23 1 d 24 X 23 1 d 23 1 d 23 1 d 24 2 d 23 2 d 23 3 d 24 5 d 23 7 d 23 9 d 23 14 d 24
 20 d 11 13 d 12 16 d 12 20 d 11 25 d 12 30 d 12 37 d 5 22 d 6 25 d 6 27 d 6 30 d 6 35 d 6 38 d 5 42 d 6 48 d 12 107 d 11 113 d 12 115 d 12 112 d 6 52 d 5 50 d 6 45 d 6 41 d 6 35 d 6 29 d 5 23 d 6 16 d 6 8 d 6 1 d 6 -6 d 6 -14 d 5 -20 d 6 -27 d 6 -34
 d 6 -39 d 6 -44 d 6 -48 d 5 -52 d 6 -54 d 23 -229 d 12 -111 d 6 -53 d 6 -51 d 6 -47 d 5 -44 d 6 -40 d 6 -34 d 6 -31 d 6 -27 d 6 -24 d 5 -22 d 12 -35 d 12 -28 d 11 -22 d 12 -17 d 12 -14 d 11 -11 d 23 -16 d 24 -10 d 23 -6 d 23 -4 d 24 -3 d 23 -2 d 23
 -1 d 24 X 23 -1 d 23 X 23 -1 d 583 X s black 3 lw 472 339 m 2329 X s 683 389 m -50 Y s 750 364 m -25 Y s 816 364 m -25 Y s 882 364 m -25 Y s 948 389 m -50 Y s 1014 364 m -25 Y s 1080 364 m -25 Y s 1147 364 m -25 Y s 1213 389 m -50 Y s 1279 364 m -25
 Y s 1345 364 m -25 Y s 1411 364 m -25 Y s 1477 389 m -50 Y s 1544 364 m -25 Y s 1610 364 m -25 Y s 1676 364 m -25 Y s 1742 389 m -50 Y s 1808 364 m -25 Y s 1874 364 m -25 Y s 1941 364 m -25 Y s 2007 389 m -50 Y s 2073 364 m -25 Y s 2139 364 m -25 Y
 s 2205 364 m -25 Y s 2271 389 m -50 Y s 2337 364 m -25 Y s 2404 364 m -25 Y s 2470 364 m -25 Y s 2536 389 m -50 Y s 2602 364 m -25 Y s 2668 364 m -25 Y s 2734 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 617 364 m -25 Y s 551 364 m -25 Y s 485
 364 m -25 Y s 2801 389 m -50 Y s 472 2013 m 2329 X s 683 1962 m 51 Y s 750 1987 m 26 Y s 816 1987 m 26 Y s 882 1987 m 26 Y s 948 1962 m 51 Y s 1014 1987 m 26 Y s 1080 1987 m 26 Y s 1147 1987 m 26 Y s 1213 1962 m 51 Y s 1279 1987 m 26 Y s 1345 1987 m
 26 Y s 1411 1987 m 26 Y s 1477 1962 m 51 Y s 1544 1987 m 26 Y s 1610 1987 m 26 Y s 1676 1987 m 26 Y s 1742 1962 m 51 Y s 1808 1987 m 26 Y s 1874 1987 m 26 Y s 1941 1987 m 26 Y s 2007 1962 m 51 Y s 2073 1987 m 26 Y s 2139 1987 m 26 Y s 2205 1987 m 26
 Y s 2271 1962 m 51 Y s 2337 1987 m 26 Y s 2404 1987 m 26 Y s 2470 1987 m 26 Y s 2536 1962 m 51 Y s 2602 1987 m 26 Y s 2668 1987 m 26 Y s 2734 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 617 1987 m 26 Y s 551 1987 m 26 Y s 485 1987 m 26 Y s
 2801 1962 m 51 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 391 m -35 X s 507 443 m -35 X s 507 495 m -35 X s 542 547 m -70 X s 507 599 m -35 X s 507 652 m -35 X s 507 704 m -35 X s 542 756 m -70 X s 507 808 m -35 X s 507 860 m -35 X s 507 912 m -35
 X s 542 964 m -70 X s 507 1016 m -35 X s 507 1068 m -35 X s 507 1121 m -35 X s 542 1173 m -70 X s 507 1225 m -35 X s 507 1277 m -35 X s 507 1329 m -35 X s 542 1381 m -70 X s 507 1433 m -35 X s 507 1485 m -35 X s 507 1537 m -35 X s 542 1590 m -70 X s
 507 1642 m -35 X s 507 1694 m -35 X s 507 1746 m -35 X s 542 1798 m -70 X s 507 1850 m -35 X s 507 1902 m -35 X s 507 1954 m -35 X s 542 2006 m -70 X s 542 2006 m -70 X s 2801 339 m 1674 Y s 2731 339 m 70 X s 2766 391 m 35 X s 2766 443 m 35 X s 2766
 495 m 35 X s 2731 547 m 70 X s 2766 599 m 35 X s 2766 652 m 35 X s 2766 704 m 35 X s 2731 756 m 70 X s 2766 808 m 35 X s 2766 860 m 35 X s 2766 912 m 35 X s 2731 964 m 70 X s 2766 1016 m 35 X s 2766 1068 m 35 X s 2766 1121 m 35 X s 2731 1173 m 70 X
 s 2766 1225 m 35 X s 2766 1277 m 35 X s 2766 1329 m 35 X s 2731 1381 m 70 X s 2766 1433 m 35 X s 2766 1485 m 35 X s 2766 1537 m 35 X s 2731 1590 m 70 X s 2766 1642 m 35 X s 2766 1694 m 35 X s 2766 1746 m 35 X s 2731 1798 m 70 X s 2766 1850 m 35 X s
 2766 1902 m 35 X s 2766 1954 m 35 X s 2731 2006 m 70 X s 2731 2006 m 70 X s
 gsave  2948 2118 0 0 C 588.866 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (ATLAS) show NC gr 
 gsave  2948 2118 0 0 C 981.443 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (Simulation Preliminary) show NC gr 
 gsave  2948 2118 0 0 C 1251.8 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1211.06 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1129.58 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (�) show NC gr 
 gsave  2948 2118 0 0 C 674.047 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 13 TeV, X) show NC gr 
 gsave  2948 2118 0 0 C 633.308 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (s) show NC gr  6 lw 604 1700 m 11 -48 d s 3 lw 615 1652 m 7 70 d s 622 1722 m 52 X s
 gsave  2948 2118 0 0 C 588.866 1359.21 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (chi2 =2.08 ) show NC gr 
 gsave  2948 2118 0 0 C 688.862 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 800 GeV) show NC gr 
 gsave  2948 2118 0 0 C 651.826 1485.13 t 0 r /Helvetica findfont 55.5534 sf 0 0 m (X) show NC gr 
 gsave  2948 2118 0 0 C 588.866 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  gr showpage
 gr 
%%Trailer
%%Pages:  1
 gr  gr  gr 
%%EOF
