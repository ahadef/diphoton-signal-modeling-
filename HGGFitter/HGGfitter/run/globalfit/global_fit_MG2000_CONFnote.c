%!PS-Adobe-2.0
%%Title: globalfit/global_fit_MG2000_CONFnote.c ( A 4)
%%Pages: (atend)
%%Creator: ROOT Version 6.14/04
%%CreationDate: Mon Apr  8 17:33:50 2019
%%Orientation: Landscape
%%DocumentNeededResources: ProcSet (FontSetInit)
%%EndComments
%%BeginProlog
/s {stroke} def /l {lineto} def /m {moveto} def /t {translate} def
/r {rotate} def /rl {roll}  def /R {repeat} def
/d {rlineto} def /rm {rmoveto} def /gr {grestore} def /f {eofill} def
/c {setrgbcolor} def /black {0 setgray} def /sd {setdash} def
/cl {closepath} def /sf {scalefont setfont} def /lw {setlinewidth} def
/box {m dup 0 exch d exch 0 d 0 exch neg d cl} def
/NC{systemdict begin initclip end}def/C{NC box clip newpath}def
/bl {box s} def /bf {gsave box gsave f grestore 1 lw [] 0 sd s grestore} def /Y { 0 exch d} def /X { 0 d} def 
/K {{pop pop 0 moveto} exch kshow} bind def
/ita {/ang 15 def gsave [1 0 ang dup sin exch cos div 1 0 0] concat} def 
/mp {newpath /y exch def /x exch def} def
/side {[w .77 mul w .23 mul] .385 w mul sd w 0 l currentpoint t -144 r} def
/mr {mp x y w2 0 360 arc} def /m24 {mr s} def /m20 {mr f} def
/mb {mp x y w2 add m w2 neg 0 d 0 w neg d w 0 d 0 w d cl} def
/mt {mp x y w2 add m w2 neg w neg d w 0 d cl} def
/w4 {w 4 div} def
/w6 {w 6 div} def
/w8 {w 8 div} def
/m21 {mb f} def /m25 {mb s} def /m22 {mt f} def /m26{mt s} def
/m23 {mp x y w2 sub m w2 w d w neg 0 d cl f} def
/m27 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl s} def
/m28 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl s } def
/m29 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl fill gr} def
/m30 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl s gr} def
/m31 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m32 {mp x y w2 sub m w2 w d w neg 0 d cl s} def
/m33 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl f} def
/m34 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m35 {mp x y w2 add m w2 neg w2 neg d w2 w2 neg d w2 w2 d w2 neg w2 d x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m36 {mb x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m37 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl s} def
/m38 {mp x w4 sub y w2 add m w4 neg w4 neg d 0 w2 neg d w4 w4 neg d w2 0 d w4 w4 d 0 w2 d w4 neg w4 d w2 neg 0 d x y w2 sub m 0 w d x w2 sub y m w 0 d cl s} def
/m39 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl f} def
/m40 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl s} def
/m41 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl f} def
/m42 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl s} def
/m43 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl f} def
/m44 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl s} def
/m45 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl f} def
/m46 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl s} def
/m47 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d w4 neg w4 neg d  w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl f} def
/m48 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d  w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 neg w4 d w4 w4 d cl f} def
/m49 {mp x w2 sub w3 add y w2 sub w3 add m  0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d 0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m2 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m5 {mp x w2 sub y w2 sub m w w d x w2 sub y w2 add m w w neg d s} def
%%IncludeResource: ProcSet (FontSetInit)
%%IncludeResource: font Times-Roman
%%IncludeResource: font Times-Italic
%%IncludeResource: font Times-Bold
%%IncludeResource: font Times-BoldItalic
%%IncludeResource: font Helvetica
%%IncludeResource: font Helvetica-Oblique
%%IncludeResource: font Helvetica-Bold
%%IncludeResource: font Helvetica-BoldOblique
%%IncludeResource: font Courier
%%IncludeResource: font Courier-Oblique
%%IncludeResource: font Courier-Bold
%%IncludeResource: font Courier-BoldOblique
%%IncludeResource: font Symbol
%%IncludeResource: font ZapfDingbats
/reEncode {exch findfont dup length dict begin {1 index /FID eq  {pop pop} {def} ifelse } forall /Encoding exch def currentdict end dup /FontName get exch definefont pop } def [/Times-Bold /Times-Italic /Times-BoldItalic /Helvetica /Helvetica-Oblique
 /Helvetica-Bold /Helvetica-BoldOblique /Courier /Courier-Oblique /Courier-Bold /Courier-BoldOblique /Times-Roman /AvantGarde-Book /AvantGarde-BookOblique /AvantGarde-Demi /AvantGarde-DemiOblique /Bookman-Demi /Bookman-DemiItalic /Bookman-Light
 /Bookman-LightItalic /Helvetica-Narrow /Helvetica-Narrow-Bold /Helvetica-Narrow-BoldOblique /Helvetica-Narrow-Oblique /NewCenturySchlbk-Roman /NewCenturySchlbk-Bold /NewCenturySchlbk-BoldItalic /NewCenturySchlbk-Italic /Palatino-Bold
 /Palatino-BoldItalic /Palatino-Italic /Palatino-Roman ] {ISOLatin1Encoding reEncode } forall/Zone {/iy exch def /ix exch def  ix 1 sub  3144 mul  1 iy sub  2224 mul t} def
%%EndProlog
%%BeginSetup
%%EndSetup
newpath  gsave  90 r 0 -594 t  28 20 t .25 .25 scale  gsave 
%%Page: 1 1
 gsave  gsave 
 1 1 Zone
 gsave  0 0 t black[  ] 0 sd 3 lw 1 1 1 c 2948 2118 0 0 bf black 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s
 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530
 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218
 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364
 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s
 gsave  2948 2118 0 0 C 592.569 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (1800) show NC gr 
 gsave  2948 2118 0 0 C 855.522 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (1850) show NC gr 
 gsave  2948 2118 0 0 C 1122.18 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (1900) show NC gr 
 gsave  2948 2118 0 0 C 1385.13 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (1950) show NC gr 
 gsave  2948 2118 0 0 C 1651.79 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (2000) show NC gr 
 gsave  2948 2118 0 0 C 1914.74 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (2050) show NC gr 
 gsave  2948 2118 0 0 C 2181.4 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (2100) show NC gr 
 gsave  2948 2118 0 0 C 2444.35 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (2150) show NC gr 
 gsave  2948 2118 0 0 C 2711 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (2200) show NC gr 
 gsave  2948 2118 0 0 C 2573.97 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV]) show NC gr 
 gsave  2948 2118 0 0 C 2544.34 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2514.72 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2451.75 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054
 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742
 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430
 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 472 339 m
 1674 Y s 542 339 m -70 X s 507 386 m -35 X s 507 433 m -35 X s 507 481 m -35 X s 542 528 m -70 X s 507 575 m -35 X s 507 622 m -35 X s 507 670 m -35 X s 542 717 m -70 X s 507 764 m -35 X s 507 811 m -35 X s 507 859 m -35 X s 542 906 m -70 X s 507
 953 m -35 X s 507 1000 m -35 X s 507 1048 m -35 X s 542 1095 m -70 X s 507 1142 m -35 X s 507 1190 m -35 X s 507 1237 m -35 X s 542 1284 m -70 X s 507 1331 m -35 X s 507 1379 m -35 X s 507 1426 m -35 X s 542 1473 m -70 X s 507 1520 m -35 X s 507
 1568 m -35 X s 507 1615 m -35 X s 542 1662 m -70 X s 507 1709 m -35 X s 507 1757 m -35 X s 507 1804 m -35 X s 542 1851 m -70 X s 542 1851 m -70 X s 507 1898 m -35 X s 507 1946 m -35 X s 507 1993 m -35 X s
 gsave  2948 2118 0 0 C 411.095 311.099 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0) show NC gr 
 gsave  2948 2118 0 0 C 299.988 499.98 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.02) show NC gr 
 gsave  2948 2118 0 0 C 296.285 688.862 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.04) show NC gr 
 gsave  2948 2118 0 0 C 299.988 877.743 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.06) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1066.62 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.08) show NC gr 
 gsave  2948 2118 0 0 C 359.245 1255.51 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.1) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1444.39 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.12) show NC gr 
 gsave  2948 2118 0 0 C 296.285 1633.27 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.14) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1822.15 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.16) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1996.22 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (]) show NC gr 
 gsave  2948 2118 0 0 C 218.51 1955.48 t 90 r /Helvetica findfont 55.5534 sf 0 0 m (\2551) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1748.08 t 90 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1718.45 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1688.82 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1303.65 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (1/N dN/dm) show NC gr  2801 339 m 1674 Y s 2731 339 m 70 X s 2766 386 m 35 X s 2766 433 m 35 X s 2766 481 m 35 X s 2731 528 m 70 X s 2766 575 m 35 X s 2766 622 m 35
 X s 2766 670 m 35 X s 2731 717 m 70 X s 2766 764 m 35 X s 2766 811 m 35 X s 2766 859 m 35 X s 2731 906 m 70 X s 2766 953 m 35 X s 2766 1000 m 35 X s 2766 1048 m 35 X s 2731 1095 m 70 X s 2766 1142 m 35 X s 2766 1190 m 35 X s 2766 1237 m 35 X s 2731
 1284 m 70 X s 2766 1331 m 35 X s 2766 1379 m 35 X s 2766 1426 m 35 X s 2731 1473 m 70 X s 2766 1520 m 35 X s 2766 1568 m 35 X s 2766 1615 m 35 X s 2731 1662 m 70 X s 2766 1709 m 35 X s 2766 1757 m 35 X s 2766 1804 m 35 X s 2731 1851 m 70 X s 2731
 1851 m 70 X s 2766 1898 m 35 X s 2766 1946 m 35 X s 2766 1993 m 35 X s 1694 1366 m s 1694 1366 m cl s 1694 1336 m -1 Y s 1694 1335 m cl s 1718 1549 m 2 Y s 1718 1551 m cl s 1718 1519 m -1 Y s 1718 1518 m cl s 1741 1593 m 3 Y s 1741 1596 m cl s 1741
 1564 m -2 Y s 1741 1562 m cl s 1764 1516 m 1 Y s 1764 1517 m cl s 1764 1486 m -2 Y s 1764 1484 m cl s 1 1 1 c black /w 30 def /w2 {w 2 div} def /w3 {w 3 div} def 483 339 507 339 530 339 553 339 576 339 600 339 623 339 646 339 670 339 693 339 716 339
 740 339 763 339 786 339 809 339 833 339 856 339 879 339 903 339 926 339 949 339 972 339 996 339 1019 339 1042 339 1066 339 1089 339 1112 340 1135 339 1159 339 1182 340 1205 339 1229 340 1252 340 1275 341 1298 341 1322 341 1345 342 1368 342 1392 346
 1415 351 1438 356 1461 364 1485 372 1508 379 1531 407 1555 435 1578 490 1601 568 1625 685 1648 862 1671 1112 1694 1351 1718 1534 1741 1579 1764 1501 1788 1226 1811 988 1834 734 1857 589 1881 467 1904 414 1927 385 1951 364 1974 356 1997 350 2020 346
 2044 343 2067 341 2090 341 2114 340 2137 340 2160 340 2183 340 2207 339 2230 339 2253 339 2277 339 2300 339 2323 339 2346 339 2370 339 2393 339 2416 339 2440 339 2463 339 2486 339 2510 339 2533 339 2556 339 2579 339 2603 339 2626 339 2649 339 2673
 339 2696 339 2719 339 2742 339 2766 339 2789 339 100 { m20} R 0 0 1 c 9 lw 1 1 1 c black 0 0 1 c 472 339 m cl s 472 339 m 698 X 24 1 d 70 X 23 1 d 23 1 d 23 X 24 2 d 23 1 d 23 3 d 24 3 d 23 5 d 23 8 d 23 11 d 24 16 d 11 11 d 12 13 d 12 16 d 11 21 d
 12 25 d 12 32 d 11 40 d 6 23 d 6 27 d 6 29 d 6 34 d 5 38 d 6 42 d 6 48 d 6 54 d 6 61 d 5 65 d 24 266 d 6 64 d 5 60 d 6 55 d 6 50 d 6 43 d 6 34 d 5 27 d 6 16 d 6 8 d 6 -3 d 6 -12 d 6 -22 d 5 -31 d 6 -39 d 6 -46 d 6 -53 d 6 -58 d 6 -62 d 11 -132 d 12
 -134 d 11 -129 d 6 -60 d 6 -57 d 6 -52 d 6 -48 d 6 -42 d 5 -35 d 6 -31 d 6 -27 d 6 -23 d 6 -20 d 11 -33 d 12 -25 d 12 -18 d 11 -15 d 12 -11 d 23 -14 d 23 -9 d 24 -5 d 23 -3 d 23 -2 d 24 -1 d 23 -1 d 23 X 24 -1 d 652 X s black 3 lw 472 339 m 2329 X s
 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364
 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m
 -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m
 -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s 472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001
 1987 m 26 Y s 1054 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689
 1987 m 26 Y s 1742 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377
 1987 m 26 Y s 2430 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m
 26 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 386 m -35 X s 507 433 m -35 X s 507 481 m -35 X s 542 528 m -70 X s 507 575 m -35 X s 507 622 m -35 X s 507 670 m -35 X s 542 717 m -70 X s 507 764 m -35 X s 507 811 m -35 X s 507 859 m -35 X s 542 906
 m -70 X s 507 953 m -35 X s 507 1000 m -35 X s 507 1048 m -35 X s 542 1095 m -70 X s 507 1142 m -35 X s 507 1190 m -35 X s 507 1237 m -35 X s 542 1284 m -70 X s 507 1331 m -35 X s 507 1379 m -35 X s 507 1426 m -35 X s 542 1473 m -70 X s 507 1520 m
 -35 X s 507 1568 m -35 X s 507 1615 m -35 X s 542 1662 m -70 X s 507 1709 m -35 X s 507 1757 m -35 X s 507 1804 m -35 X s 542 1851 m -70 X s 542 1851 m -70 X s 507 1898 m -35 X s 507 1946 m -35 X s 507 1993 m -35 X s 2801 339 m 1674 Y s 2731 339 m
 70 X s 2766 386 m 35 X s 2766 433 m 35 X s 2766 481 m 35 X s 2731 528 m 70 X s 2766 575 m 35 X s 2766 622 m 35 X s 2766 670 m 35 X s 2731 717 m 70 X s 2766 764 m 35 X s 2766 811 m 35 X s 2766 859 m 35 X s 2731 906 m 70 X s 2766 953 m 35 X s 2766
 1000 m 35 X s 2766 1048 m 35 X s 2731 1095 m 70 X s 2766 1142 m 35 X s 2766 1190 m 35 X s 2766 1237 m 35 X s 2731 1284 m 70 X s 2766 1331 m 35 X s 2766 1379 m 35 X s 2766 1426 m 35 X s 2731 1473 m 70 X s 2766 1520 m 35 X s 2766 1568 m 35 X s 2766
 1615 m 35 X s 2731 1662 m 70 X s 2766 1709 m 35 X s 2766 1757 m 35 X s 2766 1804 m 35 X s 2731 1851 m 70 X s 2731 1851 m 70 X s 2766 1898 m 35 X s 2766 1946 m 35 X s 2766 1993 m 35 X s
 gsave  2948 2118 0 0 C 588.866 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (ATLAS) show NC gr 
 gsave  2948 2118 0 0 C 981.443 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (Simulation Preliminary) show NC gr 
 gsave  2948 2118 0 0 C 1251.8 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1211.06 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1129.58 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (�) show NC gr 
 gsave  2948 2118 0 0 C 674.047 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 13 TeV, X) show NC gr 
 gsave  2948 2118 0 0 C 633.308 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (s) show NC gr  6 lw 604 1700 m 11 -48 d s 3 lw 615 1652 m 7 70 d s 622 1722 m 52 X s
 gsave  2948 2118 0 0 C 588.866 1359.21 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (chi2 =1.51 ) show NC gr 
 gsave  2948 2118 0 0 C 688.862 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 2000 GeV) show NC gr 
 gsave  2948 2118 0 0 C 651.826 1485.13 t 0 r /Helvetica findfont 55.5534 sf 0 0 m (X) show NC gr 
 gsave  2948 2118 0 0 C 588.866 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  gr showpage
 gr 
%%Trailer
%%Pages:  1
 gr  gr  gr 
%%EOF
