%!PS-Adobe-2.0
%%Title: globalfit/global_fit_MG400_CONFnote.c ( A 4)
%%Pages: (atend)
%%Creator: ROOT Version 6.14/04
%%CreationDate: Mon Apr  8 17:33:27 2019
%%Orientation: Landscape
%%DocumentNeededResources: ProcSet (FontSetInit)
%%EndComments
%%BeginProlog
/s {stroke} def /l {lineto} def /m {moveto} def /t {translate} def
/r {rotate} def /rl {roll}  def /R {repeat} def
/d {rlineto} def /rm {rmoveto} def /gr {grestore} def /f {eofill} def
/c {setrgbcolor} def /black {0 setgray} def /sd {setdash} def
/cl {closepath} def /sf {scalefont setfont} def /lw {setlinewidth} def
/box {m dup 0 exch d exch 0 d 0 exch neg d cl} def
/NC{systemdict begin initclip end}def/C{NC box clip newpath}def
/bl {box s} def /bf {gsave box gsave f grestore 1 lw [] 0 sd s grestore} def /Y { 0 exch d} def /X { 0 d} def 
/K {{pop pop 0 moveto} exch kshow} bind def
/ita {/ang 15 def gsave [1 0 ang dup sin exch cos div 1 0 0] concat} def 
/mp {newpath /y exch def /x exch def} def
/side {[w .77 mul w .23 mul] .385 w mul sd w 0 l currentpoint t -144 r} def
/mr {mp x y w2 0 360 arc} def /m24 {mr s} def /m20 {mr f} def
/mb {mp x y w2 add m w2 neg 0 d 0 w neg d w 0 d 0 w d cl} def
/mt {mp x y w2 add m w2 neg w neg d w 0 d cl} def
/w4 {w 4 div} def
/w6 {w 6 div} def
/w8 {w 8 div} def
/m21 {mb f} def /m25 {mb s} def /m22 {mt f} def /m26{mt s} def
/m23 {mp x y w2 sub m w2 w d w neg 0 d cl f} def
/m27 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl s} def
/m28 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl s } def
/m29 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl fill gr} def
/m30 {mp gsave x w2 sub y w2 add w3 sub m currentpoint t 4 {side} repeat cl s gr} def
/m31 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m32 {mp x y w2 sub m w2 w d w neg 0 d cl s} def
/m33 {mp x y w2 add m w3 neg w2 neg d w3 w2 neg d w3 w2 d cl f} def
/m34 {mp x w2 sub y w2 sub w3 add m w3 0 d  0 w3 neg d w3 0 d 0 w3 d w3 0 d  0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m35 {mp x y w2 add m w2 neg w2 neg d w2 w2 neg d w2 w2 d w2 neg w2 d x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m36 {mb x w2 sub y w2 add m w w neg d x w2 sub y w2 sub m w w d s} def
/m37 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl s} def
/m38 {mp x w4 sub y w2 add m w4 neg w4 neg d 0 w2 neg d w4 w4 neg d w2 0 d w4 w4 d 0 w2 d w4 neg w4 d w2 neg 0 d x y w2 sub m 0 w d x w2 sub y m w 0 d cl s} def
/m39 {mp x y m w4 neg w2 d w4 neg w2 neg d w2 0 d  w4 neg w2 neg d w2 0 d w4 neg w2 d w2 0 d w4 neg w2 d w4 neg w2 neg d cl f} def
/m40 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl s} def
/m41 {mp x y m w4 w2 d w4 w4 neg d w2 neg w4 neg d w2 w4 neg d w4 neg w4 neg d w4 neg w2 d w4 neg w2 neg d w4 neg w4 d w2 w4 d w2 neg w4 d w4 w4 d w4 w2 neg d cl f} def
/m42 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl s} def
/m43 {mp x y w2 add m w8 neg w2 -3 4 div mul d w2 -3 4 div mul w8 neg d w2 3 4 div mul w8 neg d w8 w2 -3 4 div mul d w8 w2 3 4 div mul d w2 3 4 div mul w8 d w2 -3 4 div mul w8 d w8 neg w2 3 4 div mul d cl f} def
/m44 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl s} def
/m45 {mp x y m w6 neg w2 d w2 2 3 div mul 0 d w6 neg w2 neg d w2 w6 d 0 w2 -2 3 div mul d w2 neg w6 d w6 w2 neg d w2 -2 3 div mul 0 d w6 w2 d w2 neg w6 neg d 0 w2 2 3 div mul d w2 w6 neg d cl f} def
/m46 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl s} def
/m47 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d w4 neg w4 neg d  w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d cl f} def
/m48 {mp x y w4 add m w4 neg w4 d w4 neg w4 neg d w4 w4 neg d  w4 neg w4 neg d w4 w4 neg d w4 w4 d w4 w4 neg d w4 w4 d w4 neg w4 d w4 w4 d w4 neg w4 d w4 neg w4 neg d  w4 w4 neg d w4 neg w4 neg d w4 neg w4 d w4 w4 d cl f} def
/m49 {mp x w2 sub w3 add y w2 sub w3 add m  0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 d w3 neg 0 d 0 w3 d w3 neg 0 d 0 w3 neg d w3 neg 0 d 0 w3 neg d w3 0 d 0 w3 d w3 0 d 0 w3 neg d w3 neg 0 d cl f } def
/m2 {mp x y w2 sub m 0 w d x w2 sub y m w 0 d s} def
/m5 {mp x w2 sub y w2 sub m w w d x w2 sub y w2 add m w w neg d s} def
%%IncludeResource: ProcSet (FontSetInit)
%%IncludeResource: font Times-Roman
%%IncludeResource: font Times-Italic
%%IncludeResource: font Times-Bold
%%IncludeResource: font Times-BoldItalic
%%IncludeResource: font Helvetica
%%IncludeResource: font Helvetica-Oblique
%%IncludeResource: font Helvetica-Bold
%%IncludeResource: font Helvetica-BoldOblique
%%IncludeResource: font Courier
%%IncludeResource: font Courier-Oblique
%%IncludeResource: font Courier-Bold
%%IncludeResource: font Courier-BoldOblique
%%IncludeResource: font Symbol
%%IncludeResource: font ZapfDingbats
/reEncode {exch findfont dup length dict begin {1 index /FID eq  {pop pop} {def} ifelse } forall /Encoding exch def currentdict end dup /FontName get exch definefont pop } def [/Times-Bold /Times-Italic /Times-BoldItalic /Helvetica /Helvetica-Oblique
 /Helvetica-Bold /Helvetica-BoldOblique /Courier /Courier-Oblique /Courier-Bold /Courier-BoldOblique /Times-Roman /AvantGarde-Book /AvantGarde-BookOblique /AvantGarde-Demi /AvantGarde-DemiOblique /Bookman-Demi /Bookman-DemiItalic /Bookman-Light
 /Bookman-LightItalic /Helvetica-Narrow /Helvetica-Narrow-Bold /Helvetica-Narrow-BoldOblique /Helvetica-Narrow-Oblique /NewCenturySchlbk-Roman /NewCenturySchlbk-Bold /NewCenturySchlbk-BoldItalic /NewCenturySchlbk-Italic /Palatino-Bold
 /Palatino-BoldItalic /Palatino-Italic /Palatino-Roman ] {ISOLatin1Encoding reEncode } forall/Zone {/iy exch def /ix exch def  ix 1 sub  3144 mul  1 iy sub  2224 mul t} def
%%EndProlog
%%BeginSetup
%%EndSetup
newpath  gsave  90 r 0 -594 t  28 20 t .25 .25 scale  gsave 
%%Page: 1 1
 gsave  gsave 
 1 1 Zone
 gsave  0 0 t black[  ] 0 sd 3 lw 1 1 1 c 2948 2118 0 0 bf black 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 1 1 1 c 2329 1674 472 339 bf black 2329 1674 472 339 bl 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s
 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530
 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218
 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364
 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s 2801 389 m -50 Y s
 gsave  2948 2118 0 0 C 614.79 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (360) show NC gr 
 gsave  2948 2118 0 0 C 877.743 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (370) show NC gr 
 gsave  2948 2118 0 0 C 1144.4 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (380) show NC gr 
 gsave  2948 2118 0 0 C 1407.35 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (390) show NC gr 
 gsave  2948 2118 0 0 C 1674.01 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (400) show NC gr 
 gsave  2948 2118 0 0 C 1936.96 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (410) show NC gr 
 gsave  2948 2118 0 0 C 2203.62 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (420) show NC gr 
 gsave  2948 2118 0 0 C 2466.57 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (430) show NC gr 
 gsave  2948 2118 0 0 C 2733.23 259.249 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (440) show NC gr 
 gsave  2948 2118 0 0 C 2573.97 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV]) show NC gr 
 gsave  2948 2118 0 0 C 2544.34 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2514.72 151.846 t 0 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 2451.75 166.66 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054
 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742
 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430
 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 2801 1962 m
 51 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 403 m -35 X s 507 466 m -35 X s 507 530 m -35 X s 542 594 m -70 X s 507 657 m -35 X s 507 721 m -35 X s 507 785 m -35 X s 542 848 m -70 X s 507 912 m -35 X s 507 976 m -35 X s 507 1039 m -35 X s 542
 1103 m -70 X s 507 1167 m -35 X s 507 1230 m -35 X s 507 1294 m -35 X s 542 1357 m -70 X s 507 1421 m -35 X s 507 1485 m -35 X s 507 1548 m -35 X s 542 1612 m -70 X s 507 1676 m -35 X s 507 1739 m -35 X s 507 1803 m -35 X s 542 1867 m -70 X s 542
 1867 m -70 X s 507 1930 m -35 X s 507 1994 m -35 X s
 gsave  2948 2118 0 0 C 411.095 311.099 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0) show NC gr 
 gsave  2948 2118 0 0 C 299.988 566.644 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.02) show NC gr 
 gsave  2948 2118 0 0 C 296.285 822.19 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.04) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1074.03 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.06) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1329.58 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.08) show NC gr 
 gsave  2948 2118 0 0 C 359.245 1585.12 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.1) show NC gr 
 gsave  2948 2118 0 0 C 299.988 1840.67 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (0.12) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1996.22 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (]) show NC gr 
 gsave  2948 2118 0 0 C 218.51 1955.48 t 90 r /Helvetica findfont 55.5534 sf 0 0 m (\2551) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1748.08 t 90 r /Helvetica findfont 81.4783 sf 0 0 m ( [GeV) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1718.45 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 277.767 1688.82 t 90 r /Symbol findfont 55.5534 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 255.545 1303.65 t 90 r /Helvetica findfont 81.4783 sf 0 0 m (1/N dN/dm) show NC gr  2801 339 m 1674 Y s 2731 339 m 70 X s 2766 403 m 35 X s 2766 466 m 35 X s 2766 530 m 35 X s 2731 594 m 70 X s 2766 657 m 35 X s 2766 721 m 35
 X s 2766 785 m 35 X s 2731 848 m 70 X s 2766 912 m 35 X s 2766 976 m 35 X s 2766 1039 m 35 X s 2731 1103 m 70 X s 2766 1167 m 35 X s 2766 1230 m 35 X s 2766 1294 m 35 X s 2731 1357 m 70 X s 2766 1421 m 35 X s 2766 1485 m 35 X s 2766 1548 m 35 X s
 2731 1612 m 70 X s 2766 1676 m 35 X s 2766 1739 m 35 X s 2766 1803 m 35 X s 2731 1867 m 70 X s 2731 1867 m 70 X s 2766 1930 m 35 X s 2766 1994 m 35 X s 1648 1055 m 1 Y s 1648 1056 m cl s 1648 1026 m -1 Y s 1648 1025 m cl s 1671 1280 m 3 Y s 1671
 1283 m cl s 1671 1250 m -2 Y s 1671 1248 m cl s 1694 1409 m 5 Y s 1694 1414 m cl s 1694 1380 m -4 Y s 1694 1376 m cl s 1718 1552 m 5 Y s 1718 1557 m cl s 1718 1522 m -5 Y s 1718 1517 m cl s 1741 1593 m 6 Y s 1741 1599 m cl s 1741 1564 m -6 Y s 1741
 1558 m cl s 1764 1589 m 5 Y s 1764 1594 m cl s 1764 1559 m -6 Y s 1764 1553 m cl s 1788 1453 m 4 Y s 1788 1457 m cl s 1788 1423 m -5 Y s 1788 1418 m cl s 1811 1273 m 3 Y s 1811 1276 m cl s 1811 1243 m -3 Y s 1811 1240 m cl s 1834 1096 m 1 Y s 1834
 1097 m cl s 1834 1066 m -1 Y s 1834 1065 m cl s 1 1 1 c black /w 30 def /w2 {w 2 div} def /w3 {w 3 div} def 483 339 507 339 530 339 553 339 576 339 600 339 623 339 646 339 670 339 693 339 716 339 740 339 763 339 786 339 809 339 833 339 856 339 879
 339 903 339 926 340 949 339 972 340 996 340 1019 340 1042 339 1066 340 1089 339 1112 340 1135 339 1159 340 1182 341 1205 344 1229 343 1252 343 1275 344 1298 347 1322 347 1345 351 1368 353 1392 354 1415 360 1438 372 1461 386 1485 409 1508 426 1531
 459 1555 527 1578 607 1601 716 1625 872 1648 1040 1671 1265 1694 1395 1718 1537 1741 1579 1764 1574 1788 1438 1811 1258 1834 1081 1857 886 1881 758 1904 629 1927 524 1951 453 1974 412 1997 392 2020 369 2044 363 2067 353 2090 346 2114 347 2137 344
 2160 341 2183 342 2207 341 2230 341 2253 340 2277 339 2300 339 2323 339 2346 340 2370 339 2393 339 2416 339 2440 339 2463 339 2486 339 2510 339 2533 339 2556 339 2579 339 2603 339 2626 339 2649 339 2673 339 2696 339 2719 339 2742 339 2766 339 2789
 339 100 { m20} R 0 0 1 c 9 lw 1 1 1 c black 0 0 1 c 472 339 m cl s 472 339 m 559 X 23 1 d 70 X 23 1 d 47 X 23 1 d 23 1 d 24 1 d 23 1 d 23 2 d 23 3 d 24 3 d 23 5 d 23 6 d 24 8 d 23 11 d 23 16 d 23 22 d 12 14 d 12 16 d 11 20 d 12 23 d 12 28 d 11 34 d
 12 41 d 12 48 d 5 28 d 6 31 d 6 34 d 6 38 d 6 41 d 6 44 d 11 92 d 23 194 d 12 96 d 12 90 d 6 43 d 5 39 d 6 36 d 6 32 d 6 29 d 6 24 d 5 20 d 6 14 d 6 10 d 6 4 d 6 -1 d 6 -6 d 5 -11 d 6 -16 d 6 -21 d 6 -25 d 6 -30 d 6 -34 d 5 -37 d 6 -40 d 12 -88 d 11
 -94 d 24 -195 d 11 -94 d 12 -88 d 12 -81 d 6 -36 d 5 -33 d 6 -29 d 6 -27 d 12 -45 d 11 -38 d 12 -31 d 12 -25 d 11 -20 d 12 -17 d 11 -14 d 24 -21 d 23 -15 d 23 -10 d 24 -7 d 23 -5 d 23 -3 d 24 -2 d 23 -2 d 23 -1 d 23 -1 d 24 X 23 -1 d 47 X 23 -1 d
 466 X s black 3 lw 472 339 m 2329 X s 683 389 m -50 Y s 736 364 m -25 Y s 789 364 m -25 Y s 842 364 m -25 Y s 895 364 m -25 Y s 948 389 m -50 Y s 1001 364 m -25 Y s 1054 364 m -25 Y s 1107 364 m -25 Y s 1160 364 m -25 Y s 1213 389 m -50 Y s 1266 364
 m -25 Y s 1319 364 m -25 Y s 1372 364 m -25 Y s 1424 364 m -25 Y s 1477 389 m -50 Y s 1530 364 m -25 Y s 1583 364 m -25 Y s 1636 364 m -25 Y s 1689 364 m -25 Y s 1742 389 m -50 Y s 1795 364 m -25 Y s 1848 364 m -25 Y s 1901 364 m -25 Y s 1954 364 m
 -25 Y s 2007 389 m -50 Y s 2060 364 m -25 Y s 2113 364 m -25 Y s 2165 364 m -25 Y s 2218 364 m -25 Y s 2271 389 m -50 Y s 2324 364 m -25 Y s 2377 364 m -25 Y s 2430 364 m -25 Y s 2483 364 m -25 Y s 2536 389 m -50 Y s 2589 364 m -25 Y s 2642 364 m
 -25 Y s 2695 364 m -25 Y s 2748 364 m -25 Y s 2801 389 m -50 Y s 683 389 m -50 Y s 630 364 m -25 Y s 578 364 m -25 Y s 525 364 m -25 Y s 472 364 m -25 Y s 2801 389 m -50 Y s 472 2013 m 2329 X s 683 1962 m 51 Y s 736 1987 m 26 Y s 789 1987 m 26 Y s
 842 1987 m 26 Y s 895 1987 m 26 Y s 948 1962 m 51 Y s 1001 1987 m 26 Y s 1054 1987 m 26 Y s 1107 1987 m 26 Y s 1160 1987 m 26 Y s 1213 1962 m 51 Y s 1266 1987 m 26 Y s 1319 1987 m 26 Y s 1372 1987 m 26 Y s 1424 1987 m 26 Y s 1477 1962 m 51 Y s 1530
 1987 m 26 Y s 1583 1987 m 26 Y s 1636 1987 m 26 Y s 1689 1987 m 26 Y s 1742 1962 m 51 Y s 1795 1987 m 26 Y s 1848 1987 m 26 Y s 1901 1987 m 26 Y s 1954 1987 m 26 Y s 2007 1962 m 51 Y s 2060 1987 m 26 Y s 2113 1987 m 26 Y s 2165 1987 m 26 Y s 2218
 1987 m 26 Y s 2271 1962 m 51 Y s 2324 1987 m 26 Y s 2377 1987 m 26 Y s 2430 1987 m 26 Y s 2483 1987 m 26 Y s 2536 1962 m 51 Y s 2589 1987 m 26 Y s 2642 1987 m 26 Y s 2695 1987 m 26 Y s 2748 1987 m 26 Y s 2801 1962 m 51 Y s 683 1962 m 51 Y s 630 1987
 m 26 Y s 578 1987 m 26 Y s 525 1987 m 26 Y s 472 1987 m 26 Y s 2801 1962 m 51 Y s 472 339 m 1674 Y s 542 339 m -70 X s 507 403 m -35 X s 507 466 m -35 X s 507 530 m -35 X s 542 594 m -70 X s 507 657 m -35 X s 507 721 m -35 X s 507 785 m -35 X s 542
 848 m -70 X s 507 912 m -35 X s 507 976 m -35 X s 507 1039 m -35 X s 542 1103 m -70 X s 507 1167 m -35 X s 507 1230 m -35 X s 507 1294 m -35 X s 542 1357 m -70 X s 507 1421 m -35 X s 507 1485 m -35 X s 507 1548 m -35 X s 542 1612 m -70 X s 507 1676
 m -35 X s 507 1739 m -35 X s 507 1803 m -35 X s 542 1867 m -70 X s 542 1867 m -70 X s 507 1930 m -35 X s 507 1994 m -35 X s 2801 339 m 1674 Y s 2731 339 m 70 X s 2766 403 m 35 X s 2766 466 m 35 X s 2766 530 m 35 X s 2731 594 m 70 X s 2766 657 m 35 X
 s 2766 721 m 35 X s 2766 785 m 35 X s 2731 848 m 70 X s 2766 912 m 35 X s 2766 976 m 35 X s 2766 1039 m 35 X s 2731 1103 m 70 X s 2766 1167 m 35 X s 2766 1230 m 35 X s 2766 1294 m 35 X s 2731 1357 m 70 X s 2766 1421 m 35 X s 2766 1485 m 35 X s 2766
 1548 m 35 X s 2731 1612 m 70 X s 2766 1676 m 35 X s 2766 1739 m 35 X s 2766 1803 m 35 X s 2731 1867 m 70 X s 2731 1867 m 70 X s 2766 1930 m 35 X s 2766 1994 m 35 X s
 gsave  2948 2118 0 0 C 588.866 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (ATLAS) show NC gr 
 gsave  2948 2118 0 0 C 981.443 1803.63 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (Simulation Preliminary) show NC gr 
 gsave  2948 2118 0 0 C 1251.8 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1211.06 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (g) show NC gr 
 gsave  2948 2118 0 0 C 1129.58 1655.49 t 0 r /Symbol findfont 81.4783 sf 0 0 m (�) show NC gr 
 gsave  2948 2118 0 0 C 674.047 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 13 TeV, X) show NC gr 
 gsave  2948 2118 0 0 C 633.308 1655.49 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (s) show NC gr  6 lw 604 1700 m 11 -48 d s 3 lw 615 1652 m 7 70 d s 622 1722 m 52 X s
 gsave  2948 2118 0 0 C 588.866 1359.21 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (chi2 =2.54 ) show NC gr 
 gsave  2948 2118 0 0 C 688.862 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m ( = 400 GeV) show NC gr 
 gsave  2948 2118 0 0 C 651.826 1485.13 t 0 r /Helvetica findfont 55.5534 sf 0 0 m (X) show NC gr 
 gsave  2948 2118 0 0 C 588.866 1507.35 t 0 r /Helvetica findfont 81.4783 sf 0 0 m (m) show NC gr  gr showpage
 gr 
%%Trailer
%%Pages:  1
 gr  gr  gr 
%%EOF
