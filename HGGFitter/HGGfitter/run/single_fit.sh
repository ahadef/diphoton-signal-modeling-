#!/bin/bash

#If you are using only one datacard for all mass points you can use option "9999"
#root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(9999,2019)'

# I am using different datacards for every mass point that is why I am not using option 9999
# This allows to assign a set of initial values for every mass point. Hence better fit.


root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(200,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(400,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(800,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(1000,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(1200,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(1600,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(2000,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(2400,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(3000,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(4000,2019)'
root -l  -b -q '../Tools/SignalParametrizationRun2/fit_singleMassMC.C(5000,2019)'





