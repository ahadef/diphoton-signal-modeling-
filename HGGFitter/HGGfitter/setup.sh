#! /bin/sh
mkdir -p lib;
cd lib
if [ ! -h libHfitterModels.so ];      then ln -s ../../Hfitter/lib/libHfitterModels.so; fi;
if [ ! -h libHfitterCalculators.so ]; then ln -s ../../Hfitter/lib/libHfitterCalculators.so; fi;
if [ ! -h libHfitterStats.so ];       then ln -s ../../Hfitter/lib/libHfitterStats.so; fi; 
if [ ! -h libTreeReader.so ];       then ln -s ../../Hfitter/lib/libTreeReader.so; fi; 
cd ..
mkdir -p include; 
cd include;
if [ ! -h HGGfitter ];          then ln -s ../HGGfitter; fi;
if [ ! -h HGGReader ];          then ln -s ../HGGReader; fi;
if [ ! -h HfitterModels ];      then ln -s ../../Hfitter/HfitterModels; fi;
if [ ! -h HfitterCalculators ]; then ln -s ../../Hfitter/HfitterCalculators; fi;
if [ ! -h TreeReader ];         then ln -s ../../Hfitter/TreeReader; fi;
cd ../run
##if [ ! -d Tuples ]; then mkdir Tuples;
##if [ ! -d Toys ];   then mkdir Toys;
##if [ ! -d Logs ];   then mkdir Logs;
##if [ ! -d Plots ];  then mkdir Plots;
#if [ ! -h OQMaps ]; then ln -s ../../OQMaps; fi;
cd ..

if [ -e ../Hfitter/setup.sh ]; then source ../Hfitter/setup.sh; fi;
if [ -e ../RootCore/scripts/setup.sh ]; then source ../RootCore/scripts/setup.sh; fi;

export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:$PWD/../Hfitter/lib:$PWD/../GoodRunsLists/StandAlone

# check Root environment setup
if [ ! $ROOTSYS ]; then
  echo "Warning: Root environment (ROOTSYS) not yet defined. Please do so! ";
fi;
