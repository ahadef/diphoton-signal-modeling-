
#include "/home1/grevtsov/personal/Style/atlasstyle/AtlasStyle.C"
#include "/home1/grevtsov/personal/Style/atlasstyle/AtlasUtils.C"

void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);


void draw_p0_2D(){ 
  bool spin2=0; //set draw option for spin-0 vs spin-2

  SetAtlasStyle();
  gStyle->SetPadRightMargin(0.2);
  gStyle->SetPadTopMargin(0.1);
  gStyle->SetPadRightMargin(0.18);
  gStyle->SetLabelSize(0.045,"xyz");
  gStyle->SetTitleOffset(1.2,"y");
  gStyle->SetPalette(55,0);
  gStyle->SetNumberContours(100);
      
  char p0filename[200];
  int nmass, imass, smass, nwidth;
  float iwidth;
  if (spin2==0){
    nmass=1251,imass=2, smass=200;
    //nwidth=21;    iwidth=0.005;
    nwidth=11;    iwidth=0.01;
  }else{
    //nmass=1001,imass=2, smass=500;
    nmass=401,imass=5, smass=500;
    nwidth=15;//6
    iwidth=0.02;
  }

  TH2F *h3;
  if (spin2==0){
    h3 = new TH2F("h3", "h3", nmass,smass,smass+nmass*imass,nwidth,0,(nwidth)*iwidth*100);
    h3->SetTitle("; m_{X} [GeV]; #Gamma_{X}#kern[0.4]{/}#kern[0.1]{m_{X}} [%]; Local significance [#sigma]");
  }else {
    h3 = new TH2F("h3", "h3", nmass,smass,smass+nmass*imass,nwidth,0.01,(nwidth)*iwidth);
    h3->SetTitle("; m_{G*} [GeV]; #it{k}/#bar{M}_{Pl}; Local significance [#sigma]");
  }
  
  for (int i=0; i<nmass; i++){
    for (int j=0; j<nwidth; j++){

      int mass = smass+imass*i;
      float width;
      if (spin2==0) width = 0.+iwidth*j;
      else width= 0.01+iwidth*j;

      sprintf(p0filename,"/lapp_data/atlas/grevtsov/Diphoton/Outputs/DS_L33/S16_v3/output/p0_%g_%i.root", width, mass);
      TFile *f = TFile::Open(p0filename);
      if (f==0){  	continue;}
      else{ 
        double sig=0.01, p0=1;
        HftInterval* result = (HftInterval*)f->Get("result");

        int N = i*nwidth+j;
        p0 = result->Value(0);
 
        sig = ROOT::Math::normal_quantile_c(p0, 1);
        if (sig<=0 ) sig=0.01;
        if (sig>2) cout << "mass: " << mass << " width: " << width << " sig: " << sig << endl;
	
        h3->SetBinContent(i+1,j+1,sig);

        f->Close();
      }
    }
 }

  TCanvas* atlas_rectangular = new TCanvas("atlas_rectangular","Canvas title",0.,0.,800,600);
  h3->SetMaximum(3.5);
  h3->SetMinimum(0);
  h3->GetXaxis()->SetNdivisions(608);
  h3->Draw("colz");

  ATLASLabel(0.04, 0.94, "Internal");
  TLatex latex2; latex2.SetTextSize(0.045); latex2.SetNDC();
  latex2.DrawLatex(0.3, 0.94, "#sqrt{s} = 13 TeV, 2016 33.9 fb^{-1}");
  if(spin2==0) latex2.DrawLatex(0.7, 0.94, "Spin-0 Selection");
  else latex2.DrawLatex(0.65, 0.94, "Spin-2 Selection");

}

void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize)
{
  TLatex l; //l.SetTextAlign(12);                                                                                                                                                                                  
  l.SetTextSize(tsize);
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p;
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
    //p.DrawLatex(x+delx*1.35,y,text);
    p.DrawLatex(x+delx,y,text);
   
  }
}
