{
HftBand* bExp = HftBand::LoadFromFiles("HighMassLimitHiggsNW_2015+2016/Asymptotic/limitExp", 200, 2000, 50);
HftBand* bObs = HftBand::LoadFromFiles("HighMassLimitHiggsNW_2015+2016/Asymptotic/limitObs", 200, 2000, 2);
HftBand* bOb2016 = HftBand::LoadFromFiles("HighMassLimitHiggsNW_2016/Asymptotic/limitObs", 200, 2000, 2);

SetAtlasStyle();
gStyle->SetLabelSize(0.045,"xy");
TCanvas* c1 = new TCanvas("c1", "", 800, 600);
c1->SetLogy();
bExp->SetXLabel("m_{X} [GeV]");
bExp->SetYLabel("95% CL Upper Limit on #sigma_{fid} #times BR [fb]");
TGraphAsymmErrors** gObs = bObs->Draw();
TGraphAsymmErrors** gOb2016 = bOb2016->Draw();
TGraphAsymmErrors** gExp = bExp->Draw(0.2, 500., kBlack, kDashed, 2, "", "graph",150., 2050.);

gOb2016[0]->SetLineWidth(2);
gOb2016[0]->SetLineColor(kBlue);
gOb2016[0]->SetLineStyle(kDashed);

gExp[0]->SetLineWidth(2);
gObs[0]->SetLineWidth(2);
gObs[0]->SetLineStyle(kSolid);
gObs[0]->Draw("SAME");
gOb2016[0]->Draw("SAME");

ATLASLabel(0.6, 0.87);
TLatex latex2; latex2.SetTextSize(0.045); latex2.SetNDC();
latex2.DrawLatex(0.6, 0.81, "#sqrt{s} = 13 TeV, 3.2 + 3.6 fb^{-1}");
latex2.DrawLatex(0.6, 0.75, "NWA (#Gamma_{X} = 4 MeV)");
//latex2.DrawLatex(0.65, 0.75, "#Gamma_{X}#kern[0.4]{/}#kern[0.1]{m_{X}} = 10 %");
latex2.DrawLatex(0.6, 0.69, "Spin-0 Selection");

TLegend legend(0.25, 0.68, 0.6, 0.92, "","NDC"); legend.SetBorderSize(0);legend.SetFillColor(0); legend.SetTextFont(42);legend.SetTextSize(0.045);
legend.SetTextSize(0.04);
legend.AddEntry(gObs[0], "Observed #it{CL_{s}} limit", "L");
legend.AddEntry(gOb2016[0], "Observed (2016 only)", "L");
legend.AddEntry(gExp[0], "Expected #it{CL_{s}} limit", "L");
legend.AddEntry(gExp[1], "Expected #pm 1 #sigma", "F");
legend.AddEntry(gExp[2], "Expected #pm 2 #sigma", "F");
legend.Draw();

c1->Print("Plots/Scalar_limit_NWA.pdf");
c1->SaveAs("Plots/Scalar_limit_NWA.C");
}
