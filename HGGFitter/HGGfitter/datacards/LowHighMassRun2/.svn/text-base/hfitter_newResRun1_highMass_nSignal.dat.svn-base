// Constraints
////////////////////////

constraint dLumiAux   = RooGaussian("dLumiAux",   "dLumi",   "unitSigma")
constraint dEffAux    = RooGaussian("dEffAux",    "dEff",    "unitSigma")
constraint dIsolAux   = RooGaussian("dIsolAux",   "dIsol",   "unitSigma")
constraint dBkgAux    = RooGaussian("dBkgAux",    "dBkg",    "unitSigma")
constraint dCXAux     = RooGaussian("dCXAux",     "dCX",     "unitSigma")
constraint dEScaleAux = RooGaussian("dEScaleAux", "dEScale", "unitSigma")

dLumi   = 0 min=-5 max=+5 title="#theta_{lumi}"
dEff    = 0 min=-5 max=+5 title="#theta_{eff}"
dIsol   = 0 min=-5 max=+5 title="#theta_{Isol}"
dBkg    = 0 min=-5 max=+5 title="#theta_{bkg}" // spurious signal
dCX     = 0 min=-5 max=+5 title="#theta_{CX}"
dEScale = 0 min=-5 max=+5 title="#theta_{ESS}" const=true  // ESS fixed by default

dLumiAux   = 0 const=true min=-5 max=+5 
dEffAux    = 0 const=true min=-5 max=+5 
dIsolAux   = 0 const=true min=-5 max=+5 
dBkgAux    = 0 const=true min=-5 max=+5 
dCXAux     = 0 const=true min=-5 max=+5 
dEScaleAux = 0 const=true min=-5 max=+5 

unitSigma = 1

// Signal normalization & Systematics
//////////////////////////////////////

sigmaEffSignal  = 0.007

formula sigmaIsolSignal = (sigmaIsolSignal0 + sigmaIsolSignal1/(1 + exp(-(mX - sigmaIsolSignalMass)/sigmaIsolSignalWidth)))
sigmaIsolSignal0 = 1.24 C
sigmaIsolSignal1 = 3.8 C
sigmaIsolSignalMass = 602
sigmaIsolSignalWidth = 81

sigmaLumi  = 0.019

formula kLumi       = (exp(sigmaLumi         * dLumi))
formula kEScale     = (exp(sigmaEScale  /100 * dEScale))
formula kEffSignal  = (exp(sigmaEffSignal/100  * dEff))
formula kIsolSignal = (exp(sigmaIsolSignal/100 * dIsol))
formula kCX         = (exp(sigmaCX/100         * dCX))

xs = 0 min=-100 max=10000 title="#sigma#timesBR(#gamma#gamma)" unit=fb
normLumi = 20.3

formula CX = (CX0 - CX1*exp(CX2*mX))
CX0 = -2.75
CX1 = -3.44
CX2 = -0.00000468

sigmaCX = 3.0

formula nSignal = (xs*normLumi*kLumi*CX*kEScale*kIsolSignal*kEffSignal*kCX + sigmaBkg*dBkg)
