// Run 2 LWA 6% Higgs : Signal shape
/////////////////////////////////

component Signal = HggTwoSidedCBPdf("mgg", "cbPeakSignal", "cbSigmaSignal", "cbALoSignal", "cbNLoSignal", "cbAHiSignal", "cbNHiSignal")
for eta3Cat = (BB, BE, EE)
  
constraint dSigAux = RooGaussian("dSigAux",    "dSig",    "unitSigma")
dSig    = 0 min=-5 max=+5 title="#theta_{#sigma}"
dSigAux = 0 const=true min=-5 max=+5 
unitSigma = 1

formula mnX = ((mX - (100))/100)

formula cbSigmaSignal0  = (sCBSignal0   + sCBSignal1*mnX)
formula cbSigmaSignalUp = (sCBSignal0Up + sCBSignal1Up*mnX)
formula cbSigmaSignalDn = (sCBSignal0Dn + sCBSignal1Dn*mnX)
formula deltaMSignal0  = (mCBSignal0 + mCBSignal1*mnX + mCBSignal2*mnX*mnX)
formula deltaMSignalUp = (mCBSignal0Up + mCBSignal1Up*mnX + mCBSignal2Up*mnX*mnX)
formula deltaMSignalDn = (mCBSignal0Dn + mCBSignal1Dn*mnX + mCBSignal2Dn*mnX*mnX)
formula cbALoSignal = (exp(aLoSignal0 + aLoSignal1*mnX))
formula cbNLoSignal = (nLoSignal0)
formula cbAHiSignal = (exp(aHiSignal0 + aHiSignal1*mnX))
formula cbNHiSignal = (nHiSignal0)

formula onePlusSigmaSigUp = (cbSigmaSignalUp/cbSigmaSignal0)
formula onePlusSigmaSigDn = (cbSigmaSignalDn/cbSigmaSignal0)
interpolate kSig along dSig using (1, onePlusSigmaSigDn, onePlusSigmaSigUp, 4)

formula onePlusDeltaMUp = (deltaMSignalUp/deltaMSignal0)
formula onePlusDeltaMDn = (deltaMSignalDn/deltaMSignal0)
interpolate kDeltaM along dEScale using (1, onePlusDeltaMDn, onePlusDeltaMUp, 0)

formula cbSigmaSignal = (cbSigmaSignal0*kSig)
formula deltaMSignal = (deltaMSignal0*kDeltaM)
formula cbPeakSignal  = (mX + deltaMSignal)

split mCBSignal0,   mCBSignal1,   mCBSignal2   along eta3Cat
split mCBSignal0Up, mCBSignal1Up, mCBSignal2Up along eta3Cat
split mCBSignal0Dn, mCBSignal1Dn, mCBSignal2Dn along eta3Cat
split sCBSignal0,   sCBSignal1   along eta3Cat
split sCBSignal0Up, sCBSignal1Up along eta3Cat
split sCBSignal0Dn, sCBSignal1Dn along eta3Cat
split aLoSignal0, aLoSignal1 along eta3Cat
split aHiSignal0, aHiSignal1 along eta3Cat
split nLoSignal0 along eta3Cat
split nHiSignal0 along eta3Cat

///////////////////////////////////////

mCBSignal0_BB = 1.8559
mCBSignal1_BB = -0.503350
mCBSignal2_BB = -0.0411323
nHiSignal0_BB = 3.5000
aLoSignal0_BB = -0.0177381
aLoSignal1_BB = -0.0100876
sCBSignal0_BB = 5.9450
sCBSignal1_BB = 2.0940
nLoSignal0_BB = 2.0900
aHiSignal0_BB = 0.0025771
aHiSignal1_BB = 0.0028346

mCBSignal0Up_BB = 0.75080
mCBSignal1Up_BB = 0.15906
mCBSignal2Up_BB = 0.0014275
mCBSignal0Dn_BB = -0.410333
mCBSignal1Dn_BB = -1.42138
mCBSignal2Dn_BB = 0.0045847

sCBSignal0Up_BB = 3.6496
sCBSignal1Up_BB = 2.9076
sCBSignal0Dn_BB = 3.5146
sCBSignal1Dn_BB = 2.5904

///////////////////////////////////

mCBSignal0_BE = -2.72216
mCBSignal1_BE = -0.148514
mCBSignal2_BE = 0.0038914
nHiSignal0_BE = 3.5000
aLoSignal0_BE = 0.029109
aLoSignal1_BE = -0.0106013
sCBSignal0_BE = 9.1811
sCBSignal1_BE = 1.8329
nLoSignal0_BE = 2.0900
aHiSignal0_BE = 0.033788
aHiSignal1_BE = 0.0087645

mCBSignal0Up_BE = 0.75080
mCBSignal1Up_BE = 0.15906
mCBSignal2Up_BE = 0.0014275
mCBSignal0Dn_BE = -0.410333
mCBSignal1Dn_BE = -1.42138
mCBSignal2Dn_BE = 0.0045847

sCBSignal0Up_BE = 3.6496
sCBSignal1Up_BE = 2.9076
sCBSignal0Dn_BE = 3.5146
sCBSignal1Dn_BE = 2.5904

/////////////////////////////////////

mCBSignal0_EE = -2.79789
mCBSignal1_EE = -0.208941
mCBSignal2_EE = -0.0138012
nHiSignal0_EE = 3.5000
aLoSignal0_EE = 0.045857
aLoSignal1_EE = -0.0190597
sCBSignal0_EE = 9.4036
sCBSignal1_EE = 1.8712
nLoSignal0_EE = 2.0900
aHiSignal0_EE = 0.097777
aHiSignal1_EE = 0.0046738

mCBSignal0Up_EE = 0.75080
mCBSignal1Up_EE = 0.15906
mCBSignal2Up_EE = 0.0014275
mCBSignal0Dn_EE = -0.410333
mCBSignal1Dn_EE = -1.42138
mCBSignal2Dn_EE = 0.0045847

sCBSignal0Up_EE = 3.6496
sCBSignal1Up_EE = 2.9076
sCBSignal0Dn_EE = 3.5146
sCBSignal1Dn_EE = 2.5904
