// Run 2 LWA 6% Higgs : Signal shape
/////////////////////////////////

component Signal = HggTwoSidedCBPdf("mgg", "cbPeakSignal", "cbSigmaSignal", "cbALoSignal", "cbNLoSignal", "cbAHiSignal", "cbNHiSignal")
for eta2ConvCat = (BBUU, NBUU, BBCR, NBCR)
  
constraint dSigAux = RooGaussian("dSigAux",    "dSig",    "unitSigma")
dSig    = 0 min=-5 max=+5 title="#theta_{#sigma}"
dSigAux = 0 const=true min=-5 max=+5 
unitSigma = 1

formula mnX = ((mX - (100))/100)

formula cbSigmaSignal0  = (sCBSignal0   + sCBSignal1*mnX)
formula cbSigmaSignalUp = (sCBSignal0Up + sCBSignal1Up*mnX)
formula cbSigmaSignalDn = (sCBSignal0Dn + sCBSignal1Dn*mnX)
formula deltaMSignal0  = (mCBSignal0 + mCBSignal1*mnX + mCBSignal2*mnX*mnX)
formula deltaMSignalUp = (mCBSignal0Up + mCBSignal1Up*mnX + mCBSignal2Up*mnX*mnX)
formula deltaMSignalDn = (mCBSignal0Dn + mCBSignal1Dn*mnX + mCBSignal2Dn*mnX*mnX)
formula cbALoSignal = (exp(aLoSignal0 + aLoSignal1*mnX))
formula cbNLoSignal = (nLoSignal0)
formula cbAHiSignal = (exp(aHiSignal0 + aHiSignal1*mnX))
formula cbNHiSignal = (nHiSignal0)

formula onePlusSigmaSigUp = (cbSigmaSignalUp/cbSigmaSignal0)
formula onePlusSigmaSigDn = (cbSigmaSignalDn/cbSigmaSignal0)
interpolate kSig along dSig using (1, onePlusSigmaSigDn, onePlusSigmaSigUp, 4)

formula onePlusDeltaMUp = (deltaMSignalUp/deltaMSignal0)
formula onePlusDeltaMDn = (deltaMSignalDn/deltaMSignal0)
interpolate kDeltaM along dEScale using (1, onePlusDeltaMDn, onePlusDeltaMUp, 0)

formula cbSigmaSignal = (cbSigmaSignal0*kSig)
formula deltaMSignal = (deltaMSignal0*kDeltaM)
formula cbPeakSignal  = (mX + deltaMSignal)

split mCBSignal0,   mCBSignal1,   mCBSignal2   along eta2ConvCat
split mCBSignal0Up, mCBSignal1Up, mCBSignal2Up along eta2ConvCat
split mCBSignal0Dn, mCBSignal1Dn, mCBSignal2Dn along eta2ConvCat
split sCBSignal0,   sCBSignal1   along eta2ConvCat
split sCBSignal0Up, sCBSignal1Up along eta2ConvCat
split sCBSignal0Dn, sCBSignal1Dn along eta2ConvCat
split aLoSignal0, aLoSignal1 along eta2ConvCat
split aHiSignal0, aHiSignal1 along eta2ConvCat
split nLoSignal0 along eta2ConvCat
split nHiSignal0 along eta2ConvCat

///////////////////////////////////////

mCBSignal0_BBUU = -1.55217
mCBSignal1_BBUU = -0.146538
mCBSignal2_BBUU = -0.00549988
nHiSignal0_BBUU = 3.5000
aLoSignal0_BBUU = 0.030987
aLoSignal1_BBUU = -0.0214432
sCBSignal0_BBUU = 9.9236
sCBSignal1_BBUU = 1.4178
nLoSignal0_BBUU = 2.0900
aHiSignal0_BBUU = 0.063420
aHiSignal1_BBUU = -0.00735238

mCBSignal0Up_BBUU = 0.75080
mCBSignal1Up_BBUU = 0.15906
mCBSignal2Up_BBUU = 0.0014275
mCBSignal0Dn_BBUU = -0.410333
mCBSignal1Dn_BBUU = -1.42138
mCBSignal2Dn_BBUU = 0.0045847

sCBSignal0Up_BBUU = 3.6496
sCBSignal1Up_BBUU = 2.9076
sCBSignal0Dn_BBUU = 3.5146
sCBSignal1Dn_BBUU = 2.5904

///////////////////////////////////

mCBSignal0_NBUU = 2.8559
mCBSignal1_NBUU = -0.625478
mCBSignal2_NBUU = -0.0624653
nHiSignal0_NBUU = 3.5000
aLoSignal0_NBUU = 0.013549
aLoSignal1_NBUU = -0.0123116
sCBSignal0_NBUU = 8.1911
sCBSignal1_NBUU = 1.7889
nLoSignal0_NBUU = 2.0900
aHiSignal0_NBUU = 0.029799
aHiSignal1_NBUU = 0.0016709

mCBSignal0Up_NBUU = 0.75080
mCBSignal1Up_NBUU = 0.15906
mCBSignal2Up_NBUU = 0.0014275
mCBSignal0Dn_NBUU = -0.410333
mCBSignal1Dn_NBUU = -1.42138
mCBSignal2Dn_NBUU = 0.0045847

sCBSignal0Up_NBUU = 3.6496
sCBSignal1Up_NBUU = 2.9076
sCBSignal0Dn_NBUU = 3.5146
sCBSignal1Dn_NBUU = 2.5904

///////////////////////////////////////

mCBSignal0_BBCR = -1.04193
mCBSignal1_BBCR = -0.196088
mCBSignal2_BBCR = -0.0140069
nHiSignal0_BBCR = 3.5000
aLoSignal0_BBCR = 0.062698
aLoSignal1_BBCR = -0.0167743
sCBSignal0_BBCR = 9.5499
sCBSignal1_BBCR = 1.7695
nLoSignal0_BBCR = 2.0900
aHiSignal0_BBCR = 0.089956
aHiSignal1_BBCR = 0.00065824

mCBSignal0Up_BBCR = 0.75080
mCBSignal1Up_BBCR = 0.15906
mCBSignal2Up_BBCR = 0.0014275
mCBSignal0Dn_BBCR = -0.410333
mCBSignal1Dn_BBCR = -1.42138
mCBSignal2Dn_BBCR = 0.0045847

sCBSignal0Up_BBCR = 3.6496
sCBSignal1Up_BBCR = 2.9076
sCBSignal0Dn_BBCR = 3.5146
sCBSignal1Dn_BBCR = 2.5904

///////////////////////////////////

mCBSignal0_NBCR = -2.88401
mCBSignal1_NBCR = -0.121747
mCBSignal2_NBCR = 0.0075975
nHiSignal0_NBCR = 3.5000
aLoSignal0_NBCR = 0.014761 
aLoSignal1_NBCR = -0.0114431
sCBSignal0_NBCR = 8.9223
sCBSignal1_NBCR = 1.8658
nLoSignal0_NBCR = 2.0900
aHiSignal0_NBCR = 0.018028
aHiSignal1_NBCR = 0.0091528

mCBSignal0Up_NBCR = 0.75080
mCBSignal1Up_NBCR = 0.15906
mCBSignal2Up_NBCR = 0.0014275
mCBSignal0Dn_NBCR = -0.410333
mCBSignal1Dn_NBCR = -1.42138
mCBSignal2Dn_NBCR = 0.0045847

sCBSignal0Up_NBCR = 3.6496
sCBSignal1Up_NBCR = 2.9076
sCBSignal0Dn_NBCR = 3.5146
sCBSignal1Dn_NBCR = 2.5904

