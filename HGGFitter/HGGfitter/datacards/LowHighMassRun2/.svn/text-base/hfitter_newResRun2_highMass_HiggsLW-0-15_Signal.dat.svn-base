// Run 2 LW Higgs (0-25% interpolation) : Signal shape
///////////////////////////////////////////////////////

component Signal = HggTwoSidedCBPdf("mgg", "cbPeakSignal", "cbSigmaSignal", "cbALoSignal", "cbNLoSignal", "cbAHiSignal", "cbNHiSignal")

constraint dSigAux = RooGaussian("dSigAux",    "dSig",    "unitSigma")
dSig    = 0 min=-5 max=+5 title="#theta_{#sigma}"
dSigAux = 0 const=true min=-5 max=+5 
unitSigma = 1

formula mnX = ((mX - (100))/100)
  
formula sCBSignal0 = (         (g12<1)*(sCBSignal0w1*(1 - g12) + sCBSignal0w2*g12) +
                      (g23>=0)*(g23<1)*(sCBSignal0w2*(1 - g23) + sCBSignal0w3*g23) +
                      (g34>=0)*(g34<1)*(sCBSignal0w3*(1 - g34) + sCBSignal0w4*g34) +
                      (g45>=0)*(g45<1)*(sCBSignal0w4*(1 - g45) + sCBSignal0w5*g45) +
                      (g56>=0)*(g56<1)*(sCBSignal0w5*(1 - g56) + sCBSignal0w6*g56) +
                      (g67>=0)*(g67<1)*(sCBSignal0w6*(1 - g67) + sCBSignal0w7*g67) +
                      (g78>=0)*(g78<1)*(sCBSignal0w7*(1 - g78) + sCBSignal0w8*g78) +
                      (g89>=0)*        (sCBSignal0w8*(1 - g89) + sCBSignal0w9*g89))

formula sCBSignal1 = (         (g12<1)*(sCBSignal1w1*(1 - g12) + sCBSignal1w2*g12) +
                      (g23>=0)*(g23<1)*(sCBSignal1w2*(1 - g23) + sCBSignal1w3*g23) +
                      (g34>=0)*(g34<1)*(sCBSignal1w3*(1 - g34) + sCBSignal1w4*g34) +
                      (g45>=0)*(g45<1)*(sCBSignal1w4*(1 - g45) + sCBSignal1w5*g45) +
                      (g56>=0)*(g56<1)*(sCBSignal1w5*(1 - g56) + sCBSignal1w6*g56) +
                      (g67>=0)*(g67<1)*(sCBSignal1w6*(1 - g67) + sCBSignal1w7*g67) +
                      (g78>=0)*(g78<1)*(sCBSignal1w7*(1 - g78) + sCBSignal1w8*g78) +
                      (g89>=0)*        (sCBSignal1w8*(1 - g89) + sCBSignal1w9*g89))

formula sCBSignal0Up = (         (g12<1)*(sCBSignal0Upw1*(1 - g12) + sCBSignal0Upw2*g12) +
                        (g23>=0)*(g23<1)*(sCBSignal0Upw2*(1 - g23) + sCBSignal0Upw3*g23) +
                        (g34>=0)*(g34<1)*(sCBSignal0Upw3*(1 - g34) + sCBSignal0Upw4*g34) +
                        (g45>=0)*(g45<1)*(sCBSignal0Upw4*(1 - g45) + sCBSignal0Upw5*g45) +
                        (g56>=0)*(g56<1)*(sCBSignal0Upw5*(1 - g56) + sCBSignal0Upw6*g56) +
                        (g67>=0)*(g67<1)*(sCBSignal0Upw6*(1 - g67) + sCBSignal0Upw7*g67) +
                        (g78>=0)*(g78<1)*(sCBSignal0Upw7*(1 - g78) + sCBSignal0Upw8*g78) +
                        (g89>=0)*        (sCBSignal0Upw8*(1 - g89) + sCBSignal0Upw9*g89))

formula sCBSignal1Up = (         (g12<1)*(sCBSignal1Upw1*(1 - g12) + sCBSignal1Upw2*g12) +
                        (g23>=0)*(g23<1)*(sCBSignal1Upw2*(1 - g23) + sCBSignal1Upw3*g23) +
                        (g34>=0)*(g34<1)*(sCBSignal1Upw3*(1 - g34) + sCBSignal1Upw4*g34) +
                        (g45>=0)*(g45<1)*(sCBSignal1Upw4*(1 - g45) + sCBSignal1Upw5*g45) +
                        (g56>=0)*(g56<1)*(sCBSignal1Upw5*(1 - g56) + sCBSignal1Upw6*g56) +
                        (g67>=0)*(g67<1)*(sCBSignal1Upw6*(1 - g67) + sCBSignal1Upw7*g67) +
                        (g78>=0)*(g78<1)*(sCBSignal1Upw7*(1 - g78) + sCBSignal1Upw8*g78) +
                        (g89>=0)*        (sCBSignal1Upw8*(1 - g89) + sCBSignal1Upw9*g89))

formula sCBSignal0Dn = (         (g12<1)*(sCBSignal0Dnw1*(1 - g12) + sCBSignal0Dnw2*g12) +
                        (g23>=0)*(g23<1)*(sCBSignal0Dnw2*(1 - g23) + sCBSignal0Dnw3*g23) +
                        (g34>=0)*(g34<1)*(sCBSignal0Dnw3*(1 - g34) + sCBSignal0Dnw4*g34) +
                        (g45>=0)*(g45<1)*(sCBSignal0Dnw4*(1 - g45) + sCBSignal0Dnw5*g45) +
                        (g56>=0)*(g56<1)*(sCBSignal0Dnw5*(1 - g56) + sCBSignal0Dnw6*g56) +
                        (g67>=0)*(g67<1)*(sCBSignal0Dnw6*(1 - g67) + sCBSignal0Dnw7*g67) +
                        (g78>=0)*(g78<1)*(sCBSignal0Dnw7*(1 - g78) + sCBSignal0Dnw8*g78) +
                        (g89>=0)*        (sCBSignal0Dnw8*(1 - g89) + sCBSignal0Dnw9*g89))
  
formula sCBSignal1Dn = (         (g12<1)*(sCBSignal1Dnw1*(1 - g12) + sCBSignal1Dnw2*g12) +
                        (g23>=0)*(g23<1)*(sCBSignal1Dnw2*(1 - g23) + sCBSignal1Dnw3*g23) +
                        (g34>=0)*(g34<1)*(sCBSignal1Dnw3*(1 - g34) + sCBSignal1Dnw4*g34) +
                        (g45>=0)*(g45<1)*(sCBSignal1Dnw4*(1 - g45) + sCBSignal1Dnw5*g45) +
                        (g56>=0)*(g56<1)*(sCBSignal1Dnw5*(1 - g56) + sCBSignal1Dnw6*g56) +
                        (g67>=0)*(g67<1)*(sCBSignal1Dnw6*(1 - g67) + sCBSignal1Dnw7*g67) +
                        (g78>=0)*(g78<1)*(sCBSignal1Dnw7*(1 - g78) + sCBSignal1Dnw8*g78) +
                        (g89>=0)*        (sCBSignal1Dnw8*(1 - g89) + sCBSignal1Dnw9*g89))

formula mCBSignal0 = (         (g12<1)*(mCBSignal0w1*(1 - g12) + mCBSignal0w2*g12) +
                      (g23>=0)*(g23<1)*(mCBSignal0w2*(1 - g23) + mCBSignal0w3*g23) +
                      (g34>=0)*(g34<1)*(mCBSignal0w3*(1 - g34) + mCBSignal0w4*g34) +
                      (g45>=0)*(g45<1)*(mCBSignal0w4*(1 - g45) + mCBSignal0w5*g45) +
                      (g56>=0)*(g56<1)*(mCBSignal0w5*(1 - g56) + mCBSignal0w6*g56) +
                      (g67>=0)*(g67<1)*(mCBSignal0w6*(1 - g67) + mCBSignal0w7*g67) +
                      (g78>=0)*(g78<1)*(mCBSignal0w7*(1 - g78) + mCBSignal0w8*g78) +
                      (g89>=0)*        (mCBSignal0w8*(1 - g89) + mCBSignal0w9*g89))
  
formula mCBSignal1 = (         (g12<1)*(mCBSignal1w1*(1 - g12) + mCBSignal1w2*g12) +
                      (g23>=0)*(g23<1)*(mCBSignal1w2*(1 - g23) + mCBSignal1w3*g23) +
                      (g34>=0)*(g34<1)*(mCBSignal1w3*(1 - g34) + mCBSignal1w4*g34) +
                      (g45>=0)*(g45<1)*(mCBSignal1w4*(1 - g45) + mCBSignal1w5*g45) +
                      (g56>=0)*(g56<1)*(mCBSignal1w5*(1 - g56) + mCBSignal1w6*g56) +
                      (g67>=0)*(g67<1)*(mCBSignal1w6*(1 - g67) + mCBSignal1w7*g67) +
                      (g78>=0)*(g78<1)*(mCBSignal1w7*(1 - g78) + mCBSignal1w8*g78) +
                      (g89>=0)*        (mCBSignal1w8*(1 - g89) + mCBSignal1w9*g89))

formula mCBSignal2 = (         (g12<1)*(mCBSignal2w1*(1 - g12) + mCBSignal2w2*g12) +
                      (g23>=0)*(g23<1)*(mCBSignal2w2*(1 - g23) + mCBSignal2w3*g23) +
                      (g34>=0)*(g34<1)*(mCBSignal2w3*(1 - g34) + mCBSignal2w4*g34) +
                      (g45>=0)*(g45<1)*(mCBSignal2w4*(1 - g45) + mCBSignal2w5*g45) +
                      (g56>=0)*(g56<1)*(mCBSignal2w5*(1 - g56) + mCBSignal2w6*g56) +
                      (g67>=0)*(g67<1)*(mCBSignal2w6*(1 - g67) + mCBSignal2w7*g67) +
                      (g78>=0)*(g78<1)*(mCBSignal2w7*(1 - g78) + mCBSignal2w8*g78) +
                      (g89>=0)*        (mCBSignal2w8*(1 - g89) + mCBSignal2w9*g89))

formula mCBSignal0Up = (         (g12<1)*(mCBSignal0Upw1*(1 - g12) + mCBSignal0Upw2*g12) +
                        (g23>=0)*(g23<1)*(mCBSignal0Upw2*(1 - g23) + mCBSignal0Upw3*g23) +
                        (g34>=0)*(g34<1)*(mCBSignal0Upw3*(1 - g34) + mCBSignal0Upw4*g34) +
                        (g45>=0)*(g45<1)*(mCBSignal0Upw4*(1 - g45) + mCBSignal0Upw5*g45) +
                        (g56>=0)*(g56<1)*(mCBSignal0Upw5*(1 - g56) + mCBSignal0Upw6*g56) +
                        (g67>=0)*(g67<1)*(mCBSignal0Upw6*(1 - g67) + mCBSignal0Upw7*g67) +
                        (g78>=0)*(g78<1)*(mCBSignal0Upw7*(1 - g78) + mCBSignal0Upw8*g78) +
                        (g89>=0)*        (mCBSignal0Upw8*(1 - g89) + mCBSignal0Upw9*g89))
  
formula mCBSignal1Up = (         (g12<1)*(mCBSignal1Upw1*(1 - g12) + mCBSignal1Upw2*g12) +
                        (g23>=0)*(g23<1)*(mCBSignal1Upw2*(1 - g23) + mCBSignal1Upw3*g23) +
                        (g34>=0)*(g34<1)*(mCBSignal1Upw3*(1 - g34) + mCBSignal1Upw4*g34) +
                        (g45>=0)*(g45<1)*(mCBSignal1Upw4*(1 - g45) + mCBSignal1Upw5*g45) +
                        (g56>=0)*(g56<1)*(mCBSignal1Upw5*(1 - g56) + mCBSignal1Upw6*g56) +
                        (g67>=0)*(g67<1)*(mCBSignal1Upw6*(1 - g67) + mCBSignal1Upw7*g67) +
                        (g78>=0)*(g78<1)*(mCBSignal1Upw7*(1 - g78) + mCBSignal1Upw8*g78) +
                        (g89>=0)*        (mCBSignal1Upw8*(1 - g89) + mCBSignal1Upw9*g89))

formula mCBSignal2Up = (         (g12<1)*(mCBSignal2Upw1*(1 - g12) + mCBSignal2Upw2*g12) +
                        (g23>=0)*(g23<1)*(mCBSignal2Upw2*(1 - g23) + mCBSignal2Upw3*g23) +
                        (g34>=0)*(g34<1)*(mCBSignal2Upw3*(1 - g34) + mCBSignal2Upw4*g34) +
                        (g45>=0)*(g45<1)*(mCBSignal2Upw4*(1 - g45) + mCBSignal2Upw5*g45) +
                        (g56>=0)*(g56<1)*(mCBSignal2Upw5*(1 - g56) + mCBSignal2Upw6*g56) +
                        (g67>=0)*(g67<1)*(mCBSignal2Upw6*(1 - g67) + mCBSignal2Upw7*g67) +
                        (g78>=0)*(g78<1)*(mCBSignal2Upw7*(1 - g78) + mCBSignal2Upw8*g78) +
                        (g89>=0)*        (mCBSignal2Upw8*(1 - g89) + mCBSignal2Upw9*g89))

formula mCBSignal0Dn = (         (g12<1)*(mCBSignal0Dnw1*(1 - g12) + mCBSignal0Dnw2*g12) +
                        (g23>=0)*(g23<1)*(mCBSignal0Dnw2*(1 - g23) + mCBSignal0Dnw3*g23) +
                        (g34>=0)*(g34<1)*(mCBSignal0Dnw3*(1 - g34) + mCBSignal0Dnw4*g34) +
                        (g45>=0)*(g45<1)*(mCBSignal0Dnw4*(1 - g45) + mCBSignal0Dnw5*g45) +
                        (g56>=0)*(g56<1)*(mCBSignal0Dnw5*(1 - g56) + mCBSignal0Dnw6*g56) +
                        (g67>=0)*(g67<1)*(mCBSignal0Dnw6*(1 - g67) + mCBSignal0Dnw7*g67) +
                        (g78>=0)*(g78<1)*(mCBSignal0Dnw7*(1 - g78) + mCBSignal0Dnw8*g78) +
                        (g89>=0)*        (mCBSignal0Dnw8*(1 - g89) + mCBSignal0Dnw9*g89))
  
formula mCBSignal1Dn = (         (g12<1)*(mCBSignal1Dnw1*(1 - g12) + mCBSignal1Dnw2*g12) +
                        (g23>=0)*(g23<1)*(mCBSignal1Dnw2*(1 - g23) + mCBSignal1Dnw3*g23) +
                        (g34>=0)*(g34<1)*(mCBSignal1Dnw3*(1 - g34) + mCBSignal1Dnw4*g34) +
                        (g45>=0)*(g45<1)*(mCBSignal1Dnw4*(1 - g45) + mCBSignal1Dnw5*g45) +
                        (g56>=0)*(g56<1)*(mCBSignal1Dnw5*(1 - g56) + mCBSignal1Dnw6*g56) +
                        (g67>=0)*(g67<1)*(mCBSignal1Dnw6*(1 - g67) + mCBSignal1Dnw7*g67) +
                        (g78>=0)*(g78<1)*(mCBSignal1Dnw7*(1 - g78) + mCBSignal1Dnw8*g78) +
                        (g89>=0)*        (mCBSignal1Dnw8*(1 - g89) + mCBSignal1Dnw9*g89))

formula mCBSignal2Dn = (         (g12<1)*(mCBSignal2Dnw1*(1 - g12) + mCBSignal2Dnw2*g12) +
                        (g23>=0)*(g23<1)*(mCBSignal2Dnw2*(1 - g23) + mCBSignal2Dnw3*g23) +
                        (g34>=0)*(g34<1)*(mCBSignal2Dnw3*(1 - g34) + mCBSignal2Dnw4*g34) +
                        (g45>=0)*(g45<1)*(mCBSignal2Dnw4*(1 - g45) + mCBSignal2Dnw5*g45) +
                        (g56>=0)*(g56<1)*(mCBSignal2Dnw5*(1 - g56) + mCBSignal2Dnw6*g56) +
                        (g67>=0)*(g67<1)*(mCBSignal2Dnw6*(1 - g67) + mCBSignal2Dnw7*g67) +
                        (g78>=0)*(g78<1)*(mCBSignal2Dnw7*(1 - g78) + mCBSignal2Dnw8*g78) +
                        (g89>=0)*        (mCBSignal2Dnw8*(1 - g89) + mCBSignal2Dnw9*g89))

formula aLoSignal0 = (         (g12<1)*(aLoSignal0w1*(1 - g12) + aLoSignal0w2*g12) +
                      (g23>=0)*(g23<1)*(aLoSignal0w2*(1 - g23) + aLoSignal0w3*g23) +
                      (g34>=0)*(g34<1)*(aLoSignal0w3*(1 - g34) + aLoSignal0w4*g34) +
                      (g45>=0)*(g45<1)*(aLoSignal0w4*(1 - g45) + aLoSignal0w5*g45) +
                      (g56>=0)*(g56<1)*(aLoSignal0w5*(1 - g56) + aLoSignal0w6*g56) +
                      (g67>=0)*(g67<1)*(aLoSignal0w6*(1 - g67) + aLoSignal0w7*g67) +
                      (g78>=0)*(g78<1)*(aLoSignal0w7*(1 - g78) + aLoSignal0w8*g78) +
                      (g89>=0)*        (aLoSignal0w8*(1 - g89) + aLoSignal0w9*g89))

formula aLoSignal1 = (         (g12<1)*(aLoSignal1w1*(1 - g12) + aLoSignal1w2*g12) +
                      (g23>=0)*(g23<1)*(aLoSignal1w2*(1 - g23) + aLoSignal1w3*g23) +
                      (g34>=0)*(g34<1)*(aLoSignal1w3*(1 - g34) + aLoSignal1w4*g34) +
                      (g45>=0)*(g45<1)*(aLoSignal1w4*(1 - g45) + aLoSignal1w5*g45) +
                      (g56>=0)*(g56<1)*(aLoSignal1w5*(1 - g56) + aLoSignal1w6*g56) +
                      (g67>=0)*(g67<1)*(aLoSignal1w6*(1 - g67) + aLoSignal1w7*g67) +
                      (g78>=0)*(g78<1)*(aLoSignal1w7*(1 - g78) + aLoSignal1w8*g78) +
                      (g89>=0)*        (aLoSignal1w8*(1 - g89) + aLoSignal1w9*g89))

formula nLoSignal0 = (         (g12<1)*(nLoSignal0w1*(1 - g12) + nLoSignal0w2*g12) +
                      (g23>=0)*(g23<1)*(nLoSignal0w2*(1 - g23) + nLoSignal0w3*g23) +
                      (g34>=0)*(g34<1)*(nLoSignal0w3*(1 - g34) + nLoSignal0w4*g34) +
                      (g45>=0)*(g45<1)*(nLoSignal0w4*(1 - g45) + nLoSignal0w5*g45) +
                      (g56>=0)*(g56<1)*(nLoSignal0w5*(1 - g56) + nLoSignal0w6*g56) +
                      (g67>=0)*(g67<1)*(nLoSignal0w6*(1 - g67) + nLoSignal0w7*g67) +
                      (g78>=0)*(g78<1)*(nLoSignal0w7*(1 - g78) + nLoSignal0w8*g78) +
                      (g89>=0)*        (nLoSignal0w8*(1 - g89) + nLoSignal0w9*g89))

formula aHiSignal0 = (         (g12<1)*(aHiSignal0w1*(1 - g12) + aHiSignal0w2*g12) +
                      (g23>=0)*(g23<1)*(aHiSignal0w2*(1 - g23) + aHiSignal0w3*g23) +
                      (g34>=0)*(g34<1)*(aHiSignal0w3*(1 - g34) + aHiSignal0w4*g34) +
                      (g45>=0)*(g45<1)*(aHiSignal0w4*(1 - g45) + aHiSignal0w5*g45) +
                      (g56>=0)*(g56<1)*(aHiSignal0w5*(1 - g56) + aHiSignal0w6*g56) +
                      (g67>=0)*(g67<1)*(aHiSignal0w6*(1 - g67) + aHiSignal0w7*g67) +
                      (g78>=0)*(g78<1)*(aHiSignal0w7*(1 - g78) + aHiSignal0w8*g78) +
                      (g89>=0)*        (aHiSignal0w8*(1 - g89) + aHiSignal0w9*g89))

formula aHiSignal1 = (         (g12<1)*(aHiSignal1w1*(1 - g12) + aHiSignal1w2*g12) +
                      (g23>=0)*(g23<1)*(aHiSignal1w2*(1 - g23) + aHiSignal1w3*g23) +
                      (g34>=0)*(g34<1)*(aHiSignal1w3*(1 - g34) + aHiSignal1w4*g34) +
                      (g45>=0)*(g45<1)*(aHiSignal1w4*(1 - g45) + aHiSignal1w5*g45) +
                      (g56>=0)*(g56<1)*(aHiSignal1w5*(1 - g56) + aHiSignal1w6*g56) +
                      (g67>=0)*(g67<1)*(aHiSignal1w6*(1 - g67) + aHiSignal1w7*g67) +
                      (g78>=0)*(g78<1)*(aHiSignal1w7*(1 - g78) + aHiSignal1w8*g78) +
                      (g89>=0)*        (aHiSignal1w8*(1 - g89) + aHiSignal1w9*g89))

formula nHiSignal0 = (         (g12<1)*(nHiSignal0w1*(1 - g12) + nHiSignal0w2*g12) +
                      (g23>=0)*(g23<1)*(nHiSignal0w2*(1 - g23) + nHiSignal0w3*g23) +
                      (g34>=0)*(g34<1)*(nHiSignal0w3*(1 - g34) + nHiSignal0w4*g34) +
                      (g45>=0)*(g45<1)*(nHiSignal0w4*(1 - g45) + nHiSignal0w5*g45) +
                      (g56>=0)*(g56<1)*(nHiSignal0w5*(1 - g56) + nHiSignal0w6*g56) +
                      (g67>=0)*(g67<1)*(nHiSignal0w6*(1 - g67) + nHiSignal0w7*g67) +
                      (g78>=0)*(g78<1)*(nHiSignal0w7*(1 - g78) + nHiSignal0w8*g78) +
                      (g89>=0)*        (nHiSignal0w8*(1 - g89) + nHiSignal0w9*g89))
  
formula cbSigmaSignal0  = (sCBSignal0   + sCBSignal1*mnX)
formula cbSigmaSignalUp = (sCBSignal0Up + sCBSignal1Up*mnX)
formula cbSigmaSignalDn = (sCBSignal0Dn + sCBSignal1Dn*mnX)
formula deltaMSignal0  = (mCBSignal0 + mCBSignal1*mnX + mCBSignal2*mnX*mnX)
formula deltaMSignalUp = (mCBSignal0Up + mCBSignal1Up*mnX + mCBSignal2Up*mnX*mnX)
formula deltaMSignalDn = (mCBSignal0Dn + mCBSignal1Dn*mnX + mCBSignal2Dn*mnX*mnX)
formula cbALoSignal = (exp(aLoSignal0 + aLoSignal1*mnX))
formula cbNLoSignal = (nLoSignal0)
formula cbAHiSignal = (exp(aHiSignal0 + aHiSignal1*mnX))
formula cbNHiSignal = (nHiSignal0)

formula onePlusSigmaSigUp = (cbSigmaSignalUp/cbSigmaSignal0)
formula onePlusSigmaSigDn = (cbSigmaSignalDn/cbSigmaSignal0)
interpolate kSig along dSig using (1, onePlusSigmaSigDn, onePlusSigmaSigUp, 4)

formula onePlusDeltaMUp = (deltaMSignalUp/deltaMSignal0)
formula onePlusDeltaMDn = (deltaMSignalDn/deltaMSignal0)
interpolate kDeltaM along dEScale using (1, onePlusDeltaMDn, onePlusDeltaMUp, 0)

formula cbSigmaSignal = (cbSigmaSignal0*kSig)
formula deltaMSignal = (deltaMSignal0*kDeltaM)
formula cbPeakSignal  = (mX + deltaMSignal)

mCBSignal0w1 = -0.0103913
mCBSignal1w1 = -0.0433229
mCBSignal2w1 = 0.00038129
nHiSignal0w1 = 3.2724
aLoSignal0w1 = 0.40739
aLoSignal1w1 = -0.00274434
sCBSignal0w1 = 1.4558
sCBSignal1w1 = 0.59971
nLoSignal0w1 = 8.9225
aHiSignal0w1 = 0.79167
aHiSignal1w1 = 0.0036017

mCBSignal0w2 = -0.0246673
mCBSignal1w2 = -0.0599244
mCBSignal2w2 = 0.0011726
nHiSignal0w2 = 2.3621
aLoSignal0w2 = 0.41199
aLoSignal1w2 = -0.0054092
sCBSignal0w2 = 1.5009
sCBSignal1w2 = 0.66467
nLoSignal0w2 = 2.6109
aHiSignal0w2 = 0.68547
aHiSignal1w2 = 0.0012616

mCBSignal0w3 = -0.0397458
mCBSignal1w3 = -0.0815672
mCBSignal2w3 = 0.0010068
nHiSignal0w3 = 2.3792
aLoSignal0w3 = 0.36571
aLoSignal1w3 = -0.0091266
sCBSignal0w3 = 1.6305
sCBSignal1w3 = 0.82312
nLoSignal0w3 = 2.2649
aHiSignal0w3 = 0.48586
aHiSignal1w3 = 0.0016384

mCBSignal0w4 = 0.030048
mCBSignal1w4 = -0.207126
mCBSignal2w4 = 0.0037235
nHiSignal0w4 = 2.651
aLoSignal0w4 = 0.27931
aLoSignal1w4 = -0.0150601
sCBSignal0w4 = 2.0192
sCBSignal1w4 = 1.2594
nLoSignal0w4 = 2.0602
aHiSignal0w4 = 0.24885
aHiSignal1w4 = 0.0036985

mCBSignal0w5 = 0.15576
mCBSignal1w5 = -0.407031
mCBSignal2w5 = 0.0047765
nHiSignal0w5 = 3.0644
aLoSignal0w5 = 0.19242
aLoSignal1w5 = -0.0226045
sCBSignal0w5 = 2.6589
sCBSignal1w5 = 1.9549
nLoSignal0w5 = 2.0541
aHiSignal0w5 = 0.069657
aHiSignal1w5 = 0.0088162

mCBSignal0w6 = 0.19332
mCBSignal1w6 = -0.678561
mCBSignal2w6 = 0.0074624
nHiSignal0w6 = 3.5002
aLoSignal0w6 = 0.17244
aLoSignal1w6 = -0.0309617
sCBSignal0w6 = 3.535
sCBSignal1w6 = 2.6834
nLoSignal0w6 = 2.0909
aHiSignal0w6 = 0.0010001
aHiSignal1w6 = 0.011904

mCBSignal0w7 = 0.47393
mCBSignal1w7 = -0.997845
mCBSignal2w7 = 0.004569
nHiSignal0w7 = 3.6747
aLoSignal0w7 = 0.15275
aLoSignal1w7 = -0.0381311
sCBSignal0w7 = 4.3529
sCBSignal1w7 = 3.4145
nLoSignal0w7 = 2.167
aHiSignal0w7 = 0.001
aHiSignal1w7 = 0.012096

mCBSignal0w8 = 0.54831
mCBSignal1w8 = -1.23526
mCBSignal2w8 = -0.00961617
nHiSignal0w8 = 3.7033
aLoSignal0w8 = 0.14257
aLoSignal1w8 = -0.0462042
sCBSignal0w8 = 5.374
sCBSignal1w8 = 4.0999
nLoSignal0w8 = 2.3022
aHiSignal0w8 = 0.0010001
aHiSignal1w8 = 0.013862

mCBSignal0w9 = 1.187
mCBSignal1w9 = -2.26285
mCBSignal2w9 = -0.0427265
nHiSignal0w9 = 4.8487
aLoSignal0w9 = 0.20137
aLoSignal1w9 = -0.0769225
sCBSignal0w9 = 8.2257
sCBSignal1w9 = 5.7512
nLoSignal0w9 = 2.8538
aHiSignal0w9 = 0.001
aHiSignal1w9 = 0.013762

mCBSignal0Upw1 = 0.33138
mCBSignal1Upw1 = 0.72476
mCBSignal2Upw1 = -0.00286713
mCBSignal0Upw2 = 0.32869
mCBSignal1Upw2 = 0.70183
mCBSignal2Upw2 = -0.00101181
mCBSignal0Upw3 = 0.33427
mCBSignal1Upw3 = 0.69862
mCBSignal2Upw3 = -0.00244623
mCBSignal0Upw4 = 0.36837
mCBSignal1Upw4 = 0.62389
mCBSignal2Upw4 = -0.00154742
mCBSignal0Upw5 = 0.56928
mCBSignal1Upw5 = 0.39778
mCBSignal2Upw5 = 0.0024154
mCBSignal0Upw6 = 0.7508
mCBSignal1Upw6 = 0.15906
mCBSignal2Upw6 = 0.0014275
mCBSignal0Upw7 = 0.94684
mCBSignal1Upw7 = -0.143782
mCBSignal2Upw7 = -0.00291316
mCBSignal0Upw8 = 1.1736
mCBSignal1Upw8 = -0.425805
mCBSignal2Upw8 = -0.015855
mCBSignal0Upw9 = 1.588
mCBSignal1Upw9 = -1.47244
mCBSignal2Upw9 = -0.0391867

mCBSignal0Dnw1 = -0.350126
mCBSignal1Dnw1 = -0.792718
mCBSignal2Dnw1 = 0.0022236
mCBSignal0Dnw2 = -0.399115
mCBSignal1Dnw2 = -0.806019
mCBSignal2Dnw2 = 0.0024349
mCBSignal0Dnw3 = -0.403692
mCBSignal1Dnw3 = -0.86393
mCBSignal2Dnw3 = 0.0041175
mCBSignal0Dnw4 = -0.468324
mCBSignal1Dnw4 = -0.980755
mCBSignal2Dnw4 = 0.0054205
mCBSignal0Dnw5 = -0.390968
mCBSignal1Dnw5 = -1.20789
mCBSignal2Dnw5 = 0.008672
mCBSignal0Dnw6 = -0.410333
mCBSignal1Dnw6 = -1.42138
mCBSignal2Dnw6 = 0.0045847
mCBSignal0Dnw7 = 0.073292
mCBSignal1Dnw7 = -1.87744
mCBSignal2Dnw7 = 0.0093681
mCBSignal0Dnw8 = 0.2166
mCBSignal1Dnw8 = -2.22358
mCBSignal2Dnw8 = 0.0020922
mCBSignal0Dnw9 = 0.20793
mCBSignal1Dnw9 = -2.96092
mCBSignal2Dnw9 = -0.0487403

sCBSignal0Upw1 = 1.5719
sCBSignal1Upw1 = 0.98931
sCBSignal0Upw2 = 1.6393
sCBSignal1Upw2 = 1.0487
sCBSignal0Upw3 = 1.7876
sCBSignal1Upw3 = 1.1934
sCBSignal0Upw4 = 2.1833
sCBSignal1Upw4 = 1.5791
sCBSignal0Upw5 = 2.7779
sCBSignal1Upw5 = 2.2511
sCBSignal0Upw6 = 3.6496
sCBSignal1Upw6 = 2.9076
sCBSignal0Upw7 = 4.3129
sCBSignal1Upw7 = 3.6401
sCBSignal0Upw8 = 5.2902
sCBSignal1Upw8 = 4.2572
sCBSignal0Upw9 = 8.2623
sCBSignal1Upw9 = 5.8423

sCBSignal0Dnw1 = 1.3377
sCBSignal1Dnw1 = 0.31957
sCBSignal0Dnw2 = 1.3829
sCBSignal1Dnw2 = 0.39431
sCBSignal0Dnw3 = 1.4853
sCBSignal1Dnw3 = 0.58505
sCBSignal0Dnw4 = 1.9224
sCBSignal1Dnw4 = 1.0699
sCBSignal0Dnw5 = 2.6359
sCBSignal1Dnw5 = 1.8571
sCBSignal0Dnw6 = 3.5146
sCBSignal1Dnw6 = 2.5904
sCBSignal0Dnw7 = 4.427
sCBSignal1Dnw7 = 3.3119
sCBSignal0Dnw8 = 5.4079
sCBSignal1Dnw8 = 4.0412
sCBSignal0Dnw9 = 8.2828
sCBSignal1Dnw9 = 5.6626
