// Run 2 LWA 6% Higgs : Signal shape
/////////////////////////////////

component Signal = HggTwoSidedCBPdf("mgg", "cbPeakSignal", "cbSigmaSignal", "cbALoSignal", "cbNLoSignal", "cbAHiSignal", "cbNHiSignal")
for eta2pTtCat = (BBLP, BBHP, NBLP, NBHP)
  
constraint dSigAux = RooGaussian("dSigAux",    "dSig",    "unitSigma")
dSig    = 0 min=-5 max=+5 title="#theta_{#sigma}"
dSigAux = 0 const=true min=-5 max=+5 
unitSigma = 1

formula mnX = ((mX - (100))/100)

formula cbSigmaSignal0  = (sCBSignal0   + sCBSignal1*mnX)
formula cbSigmaSignalUp = (sCBSignal0Up + sCBSignal1Up*mnX)
formula cbSigmaSignalDn = (sCBSignal0Dn + sCBSignal1Dn*mnX)
formula deltaMSignal0  = (mCBSignal0 + mCBSignal1*mnX + mCBSignal2*mnX*mnX)
formula deltaMSignalUp = (mCBSignal0Up + mCBSignal1Up*mnX + mCBSignal2Up*mnX*mnX)
formula deltaMSignalDn = (mCBSignal0Dn + mCBSignal1Dn*mnX + mCBSignal2Dn*mnX*mnX)
formula cbALoSignal = (exp(aLoSignal0 + aLoSignal1*mnX))
formula cbNLoSignal = (nLoSignal0)
formula cbAHiSignal = (exp(aHiSignal0 + aHiSignal1*mnX))
formula cbNHiSignal = (nHiSignal0)

formula onePlusSigmaSigUp = (cbSigmaSignalUp/cbSigmaSignal0)
formula onePlusSigmaSigDn = (cbSigmaSignalDn/cbSigmaSignal0)
interpolate kSig along dSig using (1, onePlusSigmaSigDn, onePlusSigmaSigUp, 4)

formula onePlusDeltaMUp = (deltaMSignalUp/deltaMSignal0)
formula onePlusDeltaMDn = (deltaMSignalDn/deltaMSignal0)
interpolate kDeltaM along dEScale using (1, onePlusDeltaMDn, onePlusDeltaMUp, 0)

formula cbSigmaSignal = (cbSigmaSignal0*kSig)
formula deltaMSignal = (deltaMSignal0*kDeltaM)
formula cbPeakSignal  = (mX + deltaMSignal)

split mCBSignal0,   mCBSignal1,   mCBSignal2   along eta2pTtCat
split mCBSignal0Up, mCBSignal1Up, mCBSignal2Up along eta2pTtCat
split mCBSignal0Dn, mCBSignal1Dn, mCBSignal2Dn along eta2pTtCat
split sCBSignal0,   sCBSignal1   along eta2pTtCat
split sCBSignal0Up, sCBSignal1Up along eta2pTtCat
split sCBSignal0Dn, sCBSignal1Dn along eta2pTtCat
split aLoSignal0, aLoSignal1 along eta2pTtCat
split aHiSignal0, aHiSignal1 along eta2pTtCat
split nLoSignal0 along eta2pTtCat
split nHiSignal0 along eta2pTtCat

///////////////////////////////////////

mCBSignal0_BBLP = -0.773863
mCBSignal1_BBLP = -0.223159
mCBSignal2_BBLP = -0.0179371
nHiSignal0_BBLP = 3.5000
aLoSignal0_BBLP = 0.027278
aLoSignal1_BBLP = -0.0219525
sCBSignal0_BBLP = 9.0557
sCBSignal1_BBLP = 1.6112
nLoSignal0_BBLP = 2.0900
aHiSignal0_BBLP = 0.069732
aHiSignal1_BBLP = -0.00332614

mCBSignal0Up_BBLP = 0.75080
mCBSignal1Up_BBLP = 0.15906
mCBSignal2Up_BBLP = 0.0014275
mCBSignal0Dn_BBLP = -0.410333
mCBSignal1Dn_BBLP = -1.42138
mCBSignal2Dn_BBLP = 0.0045847

sCBSignal0Up_BBLP = 3.6496
sCBSignal1Up_BBLP = 2.9076
sCBSignal0Dn_BBLP = 3.5146
sCBSignal1Dn_BBLP = 2.5904

///////////////////////////////////

mCBSignal0_BBHP = -0.618978
mCBSignal1_BBHP = -0.205079
mCBSignal2_BBHP = -0.0156600
nHiSignal0_BBHP = 3.5000
aLoSignal0_BBHP = 0.054769
aLoSignal1_BBHP = -0.0173245
sCBSignal0_BBHP = 9.2863
sCBSignal1_BBHP = 1.6569
nLoSignal0_BBHP = 2.0900
aHiSignal0_BBHP = 0.063569 
aHiSignal1_BBHP = -0.00412201

mCBSignal0Up_BBHP = 0.75080
mCBSignal1Up_BBHP = 0.15906
mCBSignal2Up_BBHP = 0.0014275
mCBSignal0Dn_BBHP = -0.410333
mCBSignal1Dn_BBHP = -1.42138
mCBSignal2Dn_BBHP = 0.0045847

sCBSignal0Up_BBHP = 3.6496
sCBSignal1Up_BBHP = 2.9076
sCBSignal0Dn_BBHP = 3.5146
sCBSignal1Dn_BBHP = 2.5904

///////////////////////////////////////

mCBSignal0_NBLP = -1.15027
mCBSignal1_NBLP = -0.258924
mCBSignal2_NBLP = -0.0212305
nHiSignal0_NBLP = 3.5000
aLoSignal0_NBLP = 0.037951
aLoSignal1_NBLP = -0.0166871
sCBSignal0_NBLP = 10.034
sCBSignal1_NBLP = 1.6637
nLoSignal0_NBLP = 2.0900
aHiSignal0_NBLP = 0.061580
aHiSignal1_NBLP = 0.0043291

mCBSignal0Up_NBLP = 0.75080
mCBSignal1Up_NBLP = 0.15906
mCBSignal2Up_NBLP = 0.0014275
mCBSignal0Dn_NBLP = -0.410333
mCBSignal1Dn_NBLP = -1.42138
mCBSignal2Dn_NBLP = 0.0045847

sCBSignal0Up_NBLP = 3.6496
sCBSignal1Up_NBLP = 2.9076
sCBSignal0Dn_NBLP = 3.5146
sCBSignal1Dn_NBLP = 2.5904

///////////////////////////////////

mCBSignal0_NBHP = -0.989191
mCBSignal1_NBHP = -0.250333
mCBSignal2_NBHP = -0.0212931
nHiSignal0_NBHP = 3.5000
aLoSignal0_NBHP = 0.046284
aLoSignal1_NBHP = -0.0156687
sCBSignal0_NBHP = 9.6347
sCBSignal1_NBHP = 1.5840
nLoSignal0_NBHP = 2.0900
aHiSignal0_NBHP = 0.047192
aHiSignal1_NBHP = -0.00334330

mCBSignal0Up_NBHP = 0.75080
mCBSignal1Up_NBHP = 0.15906
mCBSignal2Up_NBHP = 0.0014275
mCBSignal0Dn_NBHP = -0.410333
mCBSignal1Dn_NBHP = -1.42138
mCBSignal2Dn_NBHP = 0.0045847

sCBSignal0Up_NBHP = 3.6496
sCBSignal1Up_NBHP = 2.9076
sCBSignal0Dn_NBHP = 3.5146
sCBSignal1Dn_NBHP = 2.5904

