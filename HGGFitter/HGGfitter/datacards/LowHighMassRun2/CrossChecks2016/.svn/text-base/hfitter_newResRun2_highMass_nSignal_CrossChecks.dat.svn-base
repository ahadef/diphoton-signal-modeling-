// Constraints
////////////////////////

constraint dLumiAux   = RooGaussian("dLumiAux",   "dLumi",   "unitSigma")
constraint dEffAux    = RooGaussian("dEffAux",    "dEff",    "unitSigma")
constraint dIsolAux   = RooGaussian("dIsolAux",   "dIsol",   "unitSigma")
constraint dBkgAux    = RooGaussian("dBkgAux",    "dBkg",    "unitSigma")
constraint dCXAux     = RooGaussian("dCXAux",     "dCX",     "unitSigma")
constraint dEScaleAux = RooGaussian("dEScaleAux", "dEScale", "unitSigma")

dLumi   = 0 min=-5 max=+5 title="#theta_{lumi}"
dEff    = 0 min=-5 max=+5 title="#theta_{eff}"
dIsol   = 0 min=-5 max=+5 title="#theta_{Isol}"
dBkg    = 0 min=-5 max=+5 title="#theta_{bkg}" // spurious signal
dCX     = 0 min=-5 max=+5 title="#theta_{CX}"
dEScale = 0 min=-5 max=+5 title="#theta_{ESS}" const=true  // ESS fixed by default

dLumiAux   = 0 const=true min=-5 max=+5 
dEffAux    = 0 const=true min=-5 max=+5 
dIsolAux   = 0 const=true min=-5 max=+5 
dBkgAux    = 0 const=true min=-5 max=+5 
dCXAux     = 0 const=true min=-5 max=+5 
dEScaleAux = 0 const=true min=-5 max=+5 

unitSigma = 1

// Signal normalization & Systematics
//////////////////////////////////////

formula CX = (CX0 - CX1*exp(CX2*mX))
CX0 =  0.734
CX1 =  0.153
CX2 = -0.00479
sigmaCX = 2.9

formula sigmaEff  = ((sigmaEff0 + sigmaEff1*mX)*(1 + exp(sigmaEff2*mX)))
sigmaEff0 =  2.19
sigmaEff1 =  0.000174
sigmaEff2 = -0.0195

formula sigmaCalIsol = (exp(sigmaCalIsol0*mX))
sigmaCalIsol0 = -0.00129
formula sigmaTrkIsol = (sigmaTrkIsol0 + sigmaTrkIsol1*TMath::Erf(sigmaTrkIsol2*(mX - sigmaTrkIsol3)))
sigmaTrkIsol0 =  2.50
sigmaTrkIsol1 = -1.49
sigmaTrkIsol2 =  0.0123
sigmaTrkIsol3 =  322
formula sigmaIsol = (sqrt(sigmaCalIsol*sigmaCalIsol + sigmaTrkIsol*sigmaTrkIsol))

sigmaEScale = 0  

normLumi = 3.32
sigmaLumi  = 5.0

formula onePlusSigmaCXUp = (1 + sigmaCX/100)
formula onePlusSigmaCXDn = (1 - sigmaCX/100)
interpolate kCX along dCX using (1, onePlusSigmaCXDn, onePlusSigmaCXUp, 4)

formula onePlusSigmaEffUp = (1 + sigmaEff/100)
formula onePlusSigmaEffDn = (1 - sigmaEff/100)
interpolate kEff along dEff using (1, onePlusSigmaEffDn, onePlusSigmaEffUp, 4)

formula onePlusSigmaIsolUp = (1 + sigmaIsol/100)
formula onePlusSigmaIsolDn = (1 - sigmaIsol/100)
interpolate kIsol along dIsol using (1, onePlusSigmaIsolDn, onePlusSigmaIsolUp, 4)

formula onePlusSigmaLumiUp = (1 + sigmaLumi/100)
formula onePlusSigmaLumiDn = (1 - sigmaLumi/100)
interpolate kLumi along dLumi using (1, onePlusSigmaLumiDn, onePlusSigmaLumiUp, 4)

formula onePlusSigmaEScaleUp = (1 + sigmaEScale/100)
formula onePlusSigmaEScaleDn = (1 - sigmaEScale/100)
interpolate kEScale along dEScale using (1, onePlusSigmaEScaleDn, onePlusSigmaEScaleUp, 4)

xs = 0 min=-100 max=10000 title="#sigma#timesBR(#gamma#gamma)" unit=fb

formula nSignal = (xs*normLumi*kLumi*CX*kEScale*kIsol*kEff*kCX + sigmaBkg*dBkg)
