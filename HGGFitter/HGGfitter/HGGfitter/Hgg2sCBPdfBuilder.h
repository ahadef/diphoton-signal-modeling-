// $Id: HggSigMggPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   

#ifndef ROOT_Hfitter_Hgg2sCBPdfBuilder
#define ROOT_Hfitter_Hgg2sCBPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class Hgg2sCBPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    Hgg2sCBPdfBuilder() : m_pdfType(0) { }
    virtual ~Hgg2sCBPdfBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbNLoName = "cbNLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    unsigned int m_pdfType;
    TString m_depName, m_cbPeakName, m_cbSigmaName, m_cbAlphaLoName, m_cbAlphaHiName, m_cbNLoName, m_cbNHiName;
    
    ClassDef(Hfitter::Hgg2sCBPdfBuilder, 0);
  };
}

#endif
