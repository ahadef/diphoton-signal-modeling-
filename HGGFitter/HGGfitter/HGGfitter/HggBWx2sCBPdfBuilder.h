#ifndef ROOT_Hfitter_HggBWx2sCBPdfBuilder
#define ROOT_Hfitter_HggBWx2sCBPdfBuilder

#include "HGGfitter/Hgg2sCBPdfBuilder.h"

namespace Hfitter {
  
  class HggBWx2sCBPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBWx2sCBPdfBuilder() { }
    virtual ~HggBWx2sCBPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* bwPeakName = "bwPeak", const char* bwGammaName = "Gamma",
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbNLoName = "cbNLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;    

  private:
    
    Hgg2sCBPdfBuilder m_2sCBBuilder;
    TString m_depName, m_bwPeakName, m_bwGammaName;
    
    ClassDef(Hfitter::HggBWx2sCBPdfBuilder, 0);
  };
}

#endif
