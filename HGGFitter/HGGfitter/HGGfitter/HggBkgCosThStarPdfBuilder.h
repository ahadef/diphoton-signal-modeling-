// $Id: HggBkgCosThStarPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgCosThStarPdfBuilder
#define ROOT_Hfitter_HggBkgCosThStarPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgCosThStarPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgCosThStarPdfBuilder() { }   
    virtual ~HggBkgCosThStarPdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;    

    ClassDef(Hfitter::HggBkgCosThStarPdfBuilder, 0);
  };
}

#endif
