// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggGenericBuilder
#define ROOT_Hfitter_HggGenericBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggGenericBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggGenericBuilder() { }
    virtual ~HggGenericBuilder() { }

    void Setup(const char* depName = "", const char* formula = "", 
               const char* c1Name = "", const char* c2Name = "", 
               const char* c3Name = "", const char* c4Name = "", 
               const char* c5Name = "", const char* c6Name = "");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggGenericBuilder, 0);

  private:
    
    TString m_depName, m_formula;
    std::vector<TString> m_cNames;
  };
}

#endif
