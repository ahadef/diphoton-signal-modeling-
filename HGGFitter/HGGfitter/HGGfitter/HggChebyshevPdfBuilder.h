// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggChebyshevPdfBuilder
#define ROOT_Hfitter_HggChebyshevPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggChebyshevPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggChebyshevPdfBuilder() { }
    virtual ~HggChebyshevPdfBuilder() { }

    void Setup(const char* depName, double xMin, double xMax,
               const char* cs1Name = "cs1", const char* cs2Name = "cs2", 
               const char* cs3Name = "", const char* cs4Name = "", 
               const char* cs5Name = "", const char* cs6Name = "", const char* cs7Name = "");

    void Setup(const char* depName,
               const char* cs1Name, const char* cs2Name, 
               const char* cs3Name = "", const char* cs4Name = "", 
               const char* cs5Name = "", const char* cs6Name = "", const char* cs7Name = "");

    void Setup(const char* depName, double xMin, double xMax,
               unsigned int order, const char* csNameRoot = "cs");

    void Setup(const char* depName, unsigned int order, const char* csNameRoot = "cs");
               
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    TString DepName() const { return m_depName; }
    TString CoeffName(unsigned int i) { return m_csNames[i]; }
    double XMin() const { return m_xMin; }
    double XMax() const { return m_xMax; }
    
    ClassDef(Hfitter::HggChebyshevPdfBuilder, 0);

  private:
    
    TString m_depName;
    std::vector<TString> m_csNames;
    double m_xMin, m_xMax;
  };
}

#endif
