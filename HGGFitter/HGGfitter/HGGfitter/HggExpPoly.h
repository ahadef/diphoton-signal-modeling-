/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 *    File: $Id: HggExpPoly.h 28259 2009-04-16 16:21:16Z wouter $
 * Authors:                                                                  *
 *   Kyle Cranmer
 *                                                                           *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/
#ifndef ROOT_Hfitter_HggExpPoly
#define ROOT_Hfitter_HggExpPoly

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooListProxy.h"

class RooRealVar;
class RooArgList ;

namespace Hfitter {

class HggExpPoly : public RooAbsPdf {
public:

  HggExpPoly() ;
  HggExpPoly(const char *name, const char *title, RooAbsReal& _x, const RooArgList& _coefList, double _offset = 0) ;

  HggExpPoly(const HggExpPoly& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new HggExpPoly(*this, newname); }
  inline virtual ~HggExpPoly() { }

  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const ;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

private:

  RooRealProxy _x;
  RooListProxy _coefList ;
  double m_offset;
  
  Double_t evaluate() const;

  ClassDef(HggExpPoly,1) // Bernstein polynomial PDF
};
}
#endif
