// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace, Kerstin Tackmann

// Higgs bkg PDF

#ifndef ROOT_Hfitter_HggBkgMggPTPdfBuilder
#define ROOT_Hfitter_HggBkgMggPTPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "HGGfitter/HggBkgPTPdfBuilder.h"

namespace Hfitter {

  class HggBkgMggPTPdfBuilder : public HftAbsPdfBuilder {

  public:

    HggBkgMggPTPdfBuilder() { }
    virtual ~HggBkgMggPTPdfBuilder() { }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:

    HggBkgMggPdfBuilder m_mggPdfBuilder;
    HggBkgPTPdfBuilder m_pTPdfBuilder;

    ClassDef(Hfitter::HggBkgMggPTPdfBuilder, 0);
  };
}

#endif
