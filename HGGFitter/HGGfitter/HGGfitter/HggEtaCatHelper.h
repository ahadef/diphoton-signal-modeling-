
#ifndef ROOT_Hfitter_HggEtaCatHelper
#define ROOT_Hfitter_HggEtaCatHelper

#include "TTree.h"
#include "TH2D.h"
#include "TString.h"

namespace Hfitter {

  class HggEtaCatHelper {

   public:

    HggEtaCatHelper(TTree* tree, const TString& var1 = "abs(eta1)", const TString& var2 = "abs(eta2)", 
                    int nBins = 24, double etaMin = 0, double etaMax = 2.4, const TString& cut = "",
                    const TString& title1 = "|#eta_{1}|", const TString& title2 = "|#eta_{2}|", double dEta = -1)
      : m_tree(tree), m_var1(var1), m_var2(var2), m_title1(title1), m_title2(title2),  m_cut(cut),
        m_nBins(nBins), m_etaMin(etaMin), m_etaMax(etaMax), m_dEta(dEta) { }
      
    virtual ~HggEtaCatHelper() { }
      
    TH2D* resolutionPlot();
      
    TH2D* occupancy;
    
   private:
     TTree* m_tree;
     TString m_var1, m_var2, m_title1, m_title2, m_cut;
     int m_nBins;
     double m_etaMin, m_etaMax, m_dEta;
  };
}

#endif
