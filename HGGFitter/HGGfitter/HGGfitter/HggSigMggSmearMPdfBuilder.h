#ifndef ROOT_Hfitter_HggSigMggSmearMPdfBuilder
#define ROOT_Hfitter_HggSigMggSmearMPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HGGfitter/HggSigMggPdfBuilder.h"

namespace Hfitter {

  class HggSigMggSmearMPdfBuilder : public HftAbsPdfBuilder {

  public:

    HggSigMggSmearMPdfBuilder() { }
    virtual ~HggSigMggSmearMPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* dep2Name = "dm",
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma",
               const char* cbAlphaName = "cbAlpha", const char* cbNName = "cbN",
               const char* tailPeakName = "mTail", const char* tailSigmaName = "sigmaTail", const char* tailFractionName = "mggRelNorm",
               unsigned int pdfType = 0, const char* cat = "");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:

    const char* m_dep2Name;
    HggSigMggPdfBuilder m_base;
    
    ClassDef(Hfitter::HggSigMggSmearMPdfBuilder, 0);
  };
}

#endif
