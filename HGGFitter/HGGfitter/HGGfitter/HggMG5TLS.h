#ifndef HGGMG5TLS
#define HGGMG5TLS

#include <TFile.h>
#include <TF1.h>
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooTFnPdfBinding.h"
#include <RooGenericPdf.h>
#include "TComplex.h"

class RooRealVar;


class HggMG5TLS : public RooAbsPdf {
public:
  HggMG5TLS() {} ;
  HggMG5TLS(const char *name, const char *title,
	      RooAbsReal& _x, RooAbsReal& _mean, RooAbsReal& _width);
  HggMG5TLS(const HggMG5TLS& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new HggMG5TLS(*this,newname); }
  inline virtual ~HggMG5TLS() { }
  

  //Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  //Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy x ;
  RooRealProxy mean ;
  RooRealProxy width ;
  
  Double_t evaluate() const ;

//   void initGenerator();
//   Int_t generateDependents();

private:

  ClassDef(HggMG5TLS,1) // Breit Wigner PDF
};

#endif
