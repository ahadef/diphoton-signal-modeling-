// $Id: HggBkgMggCosThStarPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggCosThStarPdfBuilder
#define ROOT_Hfitter_HggBkgMggCosThStarPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBernsteinPdfBuilder.h"
#include "HGGfitter/HggBkgCosThStarPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggCosThStarPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggCosThStarPdfBuilder() { }    
    virtual ~HggBkgMggCosThStarPdfBuilder() { }
  
    void Setup(const char* mggName = "mgg",       unsigned int mggOrder = 4, const char* mggNameRoot = "bkgBernMgg",
               const char* cThName = "cosThStar", unsigned int cThOrder = 6, const char* cThNameRoot = "bkgBernCTh") 
    { m_mggPdfBuilder.Setup(mggName, mggOrder, mggNameRoot);
      m_cThPdfBuilder.Setup(cThName, cThOrder, cThNameRoot); }
  
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;    
  
  private:
    
    HggBernsteinPdfBuilder m_mggPdfBuilder;
    HggBernsteinPdfBuilder m_cThPdfBuilder;

    ClassDef(Hfitter::HggBkgMggCosThStarPdfBuilder, 0);
  };
}

#endif
