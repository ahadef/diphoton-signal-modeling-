// $Id: HggSigMggPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   

#ifndef ROOT_Hfitter_HggTwoSidedNovoPdfBuilder
#define ROOT_Hfitter_HggTwoSidedNovoPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggTwoSidedNovoPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggTwoSidedNovoPdfBuilder() : m_pdfType(0) { }
    virtual ~HggTwoSidedNovoPdfBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbAlphaHiName = "cbAlphaHi");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    unsigned int m_pdfType;
    TString m_depName, m_cbPeakName, m_cbSigmaName, m_cbAlphaLoName, m_cbAlphaHiName;
    
    ClassDef(Hfitter::HggTwoSidedNovoPdfBuilder, 0);
  };
}

#endif
