// Author: Bruno Lenzi

// Higgs signal PDF: 4th order Bernstein

#ifndef ROOT_Hfitter_HggBkgMggBernstein4PdfBuilder
#define ROOT_Hfitter_HggBkgMggBernstein4PdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggBernstein4PdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggBernstein4PdfBuilder() { }    
    virtual ~HggBkgMggBernstein4PdfBuilder() { }
    
    void Setup(const char* depName = "mgg", 
               const char* cs1Name = "cs1", const char* cs2Name = "cs2",
               const char* cs3Name = "cs3", const char* cs4Name = "cs4") 
    { m_depName = depName; m_cs1Name = cs1Name; m_cs2Name = cs2Name; m_cs3Name = cs3Name; m_cs4Name = cs4Name; }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggBernstein4PdfBuilder, 0);

  private:
    
    TString m_depName, m_cs1Name, m_cs2Name, m_cs3Name, m_cs4Name;
  };
}

#endif
