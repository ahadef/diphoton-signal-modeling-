// Author: Bruno Lenzi

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggExpPolySmearMPdfBuilder
#define ROOT_Hfitter_HggExpPolySmearMPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HGGfitter/HggExpPolyPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggExpPolySmearMPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggExpPolySmearMPdfBuilder() { }    
    virtual ~HggExpPolySmearMPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* dep2Name = "dm",
               const char* c1Name = "c1", const char* c2Name = "c2", 
               const char* c3Name = "", const char* c4Name = "", 
               const char* c5Name = "", const char* c6Name = "");

    void Setup(const char* depName, const char* dep2Name, double offset,
              const char* c1Name = "c1", const char* c2Name = "c2", 
              const char* c3Name = "", const char* c4Name = "", 
              const char* c5Name = "", const char* c6Name = "");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggExpPolySmearMPdfBuilder, 0);

  private:
    
    TString m_dep2Name;
    HggExpPolyPdfBuilder m_base;
  };
}

#endif
