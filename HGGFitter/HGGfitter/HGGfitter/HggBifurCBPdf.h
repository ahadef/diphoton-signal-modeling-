#ifndef ROOT_Hfitter_HggBifurCBPdf
#define ROOT_Hfitter_HggBifurCBPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

namespace Hfitter {

class HggBifurCBPdf : public RooAbsPdf {
public:
  HggBifurCBPdf() {}
  HggBifurCBPdf(const char *name, const char *title, RooAbsReal& _m,
               RooAbsReal& _m0, RooAbsReal& _sigma,
               RooAbsReal& _alphaLo, RooAbsReal& _sigmaLo,
               RooAbsReal& _alphaHi, RooAbsReal& _nHi);

  HggBifurCBPdf(const HggBifurCBPdf& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new HggBifurCBPdf(*this,newname); }

  inline virtual ~HggBifurCBPdf() { }

  virtual Int_t getAnalyticalIntegral( RooArgSet& allVars,  RooArgSet& analVars, const char* rangeName=0 ) const;
  virtual Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;

  double gaussianIntegral(double tmin, double tmax) const;
  double powerLawIntegral(double tmin, double tmax, double alpha, double n) const;
  
protected:

  RooRealProxy m;
  RooRealProxy m0;
  RooRealProxy sigma;
  RooRealProxy alphaLo;
  RooRealProxy sigmaLo;
  RooRealProxy alphaHi;
  RooRealProxy nHi;

  Double_t evaluate() const;

private:

  ClassDef(Hfitter::HggBifurCBPdf,1) // Crystal Ball lineshape PDF
};

}

#endif
