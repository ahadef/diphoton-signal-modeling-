// $Id: MggBkgPdf.h,v 1.4 2008/03/06 14:52:04 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs background PDF: f(x) = Exp(c*(x-offset)) --> good choice is: Exp(-0.02*(x-90))

#ifndef ROOT_Hfitter_MggBkgPdf
#define ROOT_Hfitter_MggBkgPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

namespace Hfitter {

   class MggBkgPdf : public RooAbsPdf {

   public:
     
     MggBkgPdf() { }
     MggBkgPdf( const MggBkgPdf& other, const char* name = 0 );
     MggBkgPdf( const char* name, const char* title, 
                RooAbsReal& mgg, RooAbsReal& c, RooAbsReal& offset );
     
     inline virtual ~MggBkgPdf() {}
     
     Int_t    getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0 ) const;
     Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;
     
     virtual TObject* clone( const char* newname ) const { return new MggBkgPdf( *this, newname ); }

   protected:
      
     RooRealProxy m_mgg;
     RooRealProxy m_xi;
     RooRealProxy m_offset;
     
     Double_t evaluate() const;
     
   private:

     ClassDef(Hfitter::MggBkgPdf, 1) 

   };
}

#endif
