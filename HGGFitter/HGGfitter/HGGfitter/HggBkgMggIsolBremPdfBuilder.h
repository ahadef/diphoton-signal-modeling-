#ifndef ROOT_Hfitter_HggBkgMggIsolBremPdfBuilder
#define ROOT_Hfitter_HggBkgMggIsolBremPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBkgMggIsolPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggIsolBremPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggIsolBremPdfBuilder();
      
    virtual ~HggBkgMggIsolBremPdfBuilder() { }

    void Setup(bool isGam1, bool isGam2, bool brem1, bool brem2);

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggBkgMggIsolPdfBuilder m_mggIsolPdfBuilder;
    bool m_brem1, m_brem2;

    ClassDef(Hfitter::HggBkgMggIsolBremPdfBuilder, 0);
  };
}

#endif
