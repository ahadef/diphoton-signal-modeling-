// Author: Elisabeth Petit

// background PDF: sum of 2 power functions

#ifndef ROOT_Hfitter_HggBkgMggPower2PdfBuilder
#define ROOT_Hfitter_HggBkgMggPower2PdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggPower2PdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggPower2PdfBuilder() { }    
    virtual ~HggBkgMggPower2PdfBuilder() { }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggPower2PdfBuilder, 0);
  };
}

#endif
