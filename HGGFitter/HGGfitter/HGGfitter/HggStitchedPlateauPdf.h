#ifndef ROOT_Hfitter_HggStitchedPlateauPdf
#define ROOT_Hfitter_HggStitchedPlateauPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;
class RooAbsReal;

namespace Hfitter {

   class HggStitchedPlateauPdf : public RooAbsPdf {

   public:
     
     HggStitchedPlateauPdf() { }
     HggStitchedPlateauPdf(const char* name, const char* title, RooAbsReal& _x, RooAbsReal& _x0, RooAbsReal& _edge, RooAbsReal& _sigma);
     HggStitchedPlateauPdf(const HggStitchedPlateauPdf& other, const char* name = 0);
     
     virtual TObject* clone(const char* newname) const { return new HggStitchedPlateauPdf(*this, newname); }

     virtual ~HggStitchedPlateauPdf() { }
     
     Int_t    getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const;
     Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const;

     double integralFromMinusInf(double x2) const;
     double integral(double x1, double x2) const;

   protected:
      
     Double_t evaluate() const;
     RooRealProxy x, x0, edge, sigma;
     
   private:

     ClassDef(HggStitchedPlateauPdf, 1) 

   };
}

#endif
