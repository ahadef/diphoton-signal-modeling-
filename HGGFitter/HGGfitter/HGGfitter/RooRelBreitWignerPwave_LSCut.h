#ifndef ROO_RELBREITWIGNERPWAVE_LSCut
#define ROO_RELBREITWIGNERPWAVE_LSCut

#include <TFile.h>
#include <TF1.h>
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooTFnPdfBinding.h"
#include <RooGenericPdf.h>
#include "TComplex.h"

class RooRealVar;


class RooRelBreitWignerPwave_LSCut : public RooAbsPdf {
public:
  RooRelBreitWignerPwave_LSCut() {} ;
  RooRelBreitWignerPwave_LSCut(const char *name, const char *title,
	      RooAbsReal& _x, RooAbsReal& _mean, RooAbsReal& _width);
  RooRelBreitWignerPwave_LSCut(const RooRelBreitWignerPwave_LSCut& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new RooRelBreitWignerPwave_LSCut(*this,newname); }
  inline virtual ~RooRelBreitWignerPwave_LSCut() { }
//  static Double_t LSweight(Double_t *x, Double_t *par);
  Double_t LSweight(Double_t _x) const;
  Double_t Derivative1(Double_t _x) const; 
  Double_t Derivative2(Double_t _x) const; 




//  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
//  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy x ;
  RooRealProxy mean ;
  RooRealProxy width ;
  
  Double_t evaluate() const ;

  Double_t Bgg(Double_t _x);

 // TFile *File_BornME;
 // RooAbsPdf *BornMEpdf;
 // RooGenericPdf *lumipdf;

 // RooAbsReal *m_mass;

 // float massmin, massmax;
 // TF1 *FBgg;

//hyp  TF1 TF_LSweight;
//   void initGenerator();
//   Int_t generateDependents();

private:

  ClassDef(RooRelBreitWignerPwave_LSCut,1) // Breit Wigner PDF
};

#endif
