// $Id: HggSigCosThStarPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggSigCosThStarPdfBuilder
#define ROOT_Hfitter_HggSigCosThStarPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggSigCosThStarPdfBuilder : public HftAbsPdfBuilder {
    
   public:
    
    HggSigCosThStarPdfBuilder() : m_pdfType(3) { }
    virtual ~HggSigCosThStarPdfBuilder() { }
    
    void Setup(unsigned int pdfType = 3) { m_pdfType = pdfType; }
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

   private:
     
    unsigned int m_pdfType;

    ClassDef(Hfitter::HggSigCosThStarPdfBuilder, 1);
      
  };
}

#endif
