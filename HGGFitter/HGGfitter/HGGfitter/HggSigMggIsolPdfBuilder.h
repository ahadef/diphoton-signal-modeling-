#ifndef ROOT_Hfitter_HggSigMggIsolPdfBuilder
#define ROOT_Hfitter_HggSigMggIsolPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggIsolPdfBuilder.h"

namespace Hfitter {
  
  class HggSigMggIsolPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggSigMggIsolPdfBuilder();
      
    virtual ~HggSigMggIsolPdfBuilder() { }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    HggSigMggPdfBuilder m_mggPdfBuilder;
    HggIsolPdfBuilder m_isol1PdfBuilder, m_isol2PdfBuilder;

    ClassDef(Hfitter::HggSigMggIsolPdfBuilder, 0);
  };
}

#endif
