// Author: Marine Kuna

#ifndef ROOT_Hfitter_HggBkgMggIsol2DHistoBuilder
#define ROOT_Hfitter_HggBkgMggIsol2DHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HGGfitter/HggBkgMggPdfBuilder.h"

#include "TString.h"

namespace Hfitter {
  
  class HggBkgMggIsol2DHistoBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggIsol2DHistoBuilder();
      
    virtual ~HggBkgMggIsol2DHistoBuilder() { }

    

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const; 

  private:
    
    HggBkgMggPdfBuilder m_mggPdfBuilder;
   
        
    ClassDef(Hfitter::HggBkgMggIsol2DHistoBuilder, 0);
  };
}

#endif
