#ifndef ROOT_Hfitter_HggGravLSPdfBuilder
#define ROOT_Hfitter_HggGravLSPdfBuilder

#include "HGGfitter/Hgg2sCBPdfBuilder.h"
#include "HGGfitter/HggGravitonLineShapePdf.hh" 

namespace Hfitter {
  
  class HggGravLSPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggGravLSPdfBuilder() { }
    virtual ~HggGravLSPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* mGName = "mG", const char* GkMName = "Gamma",
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbNLoName = "cbNLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    Hgg2sCBPdfBuilder m_2sCBBuilder;
    TString m_depName, m_mGName, m_GkMName;
    
    ClassDef(Hfitter::HggGravLSPdfBuilder, 0);
  };
}

#endif
