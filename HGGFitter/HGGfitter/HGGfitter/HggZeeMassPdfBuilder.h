// $Id: HggSigMggPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   

#ifndef ROOT_Hfitter_HggZeeMassPdfBuilder
#define ROOT_Hfitter_HggZeeMassPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggZeeMassPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggZeeMassPdfBuilder() : m_pdfType(0) { }
    virtual ~HggZeeMassPdfBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbNLoName = "cbNLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi",  
               const char* tailPeakName = "gsPeak", const char* tailSigmaName = "gsTail", const char* cbFractionName = "cbFrac");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    unsigned int m_pdfType;
    TString m_depName, m_cbPeakName, m_cbSigmaName, m_cbAlphaLoName, m_cbAlphaHiName, m_cbNLoName, m_cbNHiName;
    TString m_gsPeakName, m_gsSigmaName, m_cbFractionName;
    
    ClassDef(Hfitter::HggZeeMassPdfBuilder, 0);
  };
}

#endif
