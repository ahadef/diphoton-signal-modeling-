#ifndef ROOT_HggGravTLS
#define ROOT_HggGravTLS

#include <math.h>
#include "Math/ProbFuncMathCore.h"
#include "Riostream.h"
#include "RooAbsPdf.h"
#include "RooAbsReal.h"
#include "RooFit.h"
#include "RooMath.h"
#include "RooRealProxy.h"
#include "RooRealVar.h"
#include "TMath.h"

// ==================================================================
// Implemented by Hongtao Yang <Hongtao.Yang@cern.ch> on Feb. 6, 2016
// with theory inputs provided by Jan Stark <stark@in2p3.fr>
// ==================================================================
class RooRealVar;

class HggGravTLS : public RooAbsPdf {
  
 public:
  
  HggGravTLS();
  HggGravTLS(const char *name, const char *title, RooAbsReal& _x,
			  RooAbsReal& _mG, RooAbsReal& _GkM, int _cme=13);
  
  HggGravTLS(const HggGravTLS& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new HggGravTLS(*this,newname); }
  inline virtual ~HggGravTLS() { }

 protected:

  RooRealProxy x ;
  RooRealProxy mG ;
  RooRealProxy GkM ;
  int cme;
  Double_t evaluate() const;
  Double_t bernstein(int degree, double* p, double mgg, double xmin, double xmax) const;
 private:

  ClassDef(HggGravTLS,1); // Crystal Ball lineshape PDF
    
};
  
#endif
