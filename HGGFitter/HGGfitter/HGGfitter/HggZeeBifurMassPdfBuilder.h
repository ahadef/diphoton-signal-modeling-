// $Id: HggSigMggPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   

#ifndef ROOT_Hfitter_HggZeeBifurMassPdfBuilder
#define ROOT_Hfitter_HggZeeBifurMassPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggZeeBifurMassPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggZeeBifurMassPdfBuilder() : m_pdfType(0) { }
    virtual ~HggZeeBifurMassPdfBuilder() { }

    void Setup(const char* depName = "mgg", 
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbSigmaLoName = "cbSigmaLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi",  
               const char* tailPeakName = "gsPeak", const char* tailSigmaName = "gsTail", const char* cbFractionName = "cbFrac");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    unsigned int m_pdfType;
    TString m_depName, m_cbPeakName, m_cbSigmaName, m_cbAlphaLoName, m_cbAlphaHiName, m_cbSigmaLoName, m_cbNHiName;
    TString m_gsPeakName, m_gsSigmaName, m_cbFractionName;
    
    ClassDef(Hfitter::HggZeeBifurMassPdfBuilder, 0);
  };
}

#endif
