#ifndef ROOT_Hfitter_HggMG5LSPdfBuilder
#define ROOT_Hfitter_HggMG5LSPdfBuilder

#include "HGGfitter/Hgg2sCBPdfBuilder.h"
#include "HGGfitter/HggMG5TLS.h"

namespace Hfitter {
  
  class HggMG5LSPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggMG5LSPdfBuilder() { }
    virtual ~HggMG5LSPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* bwPeakName = "bwPeak", const char* bwGammaName = "Gamma",
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbNLoName = "cbNLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    Hgg2sCBPdfBuilder m_2sCBBuilder;
    TString m_depName, m_bwPeakName, m_bwGammaName;
    
    ClassDef(Hfitter::HggMG5LSPdfBuilder, 0);
  };
}

#endif
