// $Id: HggBkgMggSmearMPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggSmearMPdfBuilder
#define ROOT_Hfitter_HggBkgMggSmearMPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HGGfitter/HggBkgMggPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggSmearMPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggSmearMPdfBuilder() { }    
    virtual ~HggBkgMggSmearMPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* dep2Name = "dm", const char* xiName = "xi") { m_dep2Name = dep2Name; m_base.Setup(depName, xiName); }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggSmearMPdfBuilder, 0);
    
  private:
    
    TString m_dep2Name;
    HggBkgMggPdfBuilder m_base;
  };
}

#endif
