#ifndef ROO_RELBREITWIGNERPWAVE
#define ROO_RELBREITWIGNERPWAVE

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

class RooRelBreitWignerPwave : public RooAbsPdf {
public:
  RooRelBreitWignerPwave() {} ;
  RooRelBreitWignerPwave(const char *name, const char *title,
	      RooAbsReal& _x, RooAbsReal& _mean, RooAbsReal& _width);
  RooRelBreitWignerPwave(const RooRelBreitWignerPwave& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new RooRelBreitWignerPwave(*this,newname); }
  inline virtual ~RooRelBreitWignerPwave() { }

//  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
//  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy x ;
  RooRealProxy mean ;
  RooRealProxy width ;
  
  Double_t evaluate() const ;

//   void initGenerator();
//   Int_t generateDependents();

private:

  ClassDef(RooRelBreitWignerPwave,1) // Breit Wigner PDF
};

#endif
