/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 *    File: $Id: HggCBShape.h,v 1.11 2007/07/12 20:30:49 wouter Exp $
 * Authors:                                                                  *
 *   WV, Wouter Verkerke, UC Santa Barbara, verkerke@slac.stanford.edu       *
 *   DK, David Kirkby,    UC Irvine,         dkirkby@uci.edu                 *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/

#ifndef ROOT_Hfitter_HggCBShape
#define ROOT_Hfitter_HggCBShape

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

// These are living here temporarily since they are needed by rootcint and need to be somewhere (see Linkdef)
#include "RooExponential.h"
#include "RooLognormal.h"
#include "HGGfitter/HggLognormal.h"


class RooRealVar;

namespace Hfitter {

class HggCBShape : public RooAbsPdf {
public:
  HggCBShape() {}
  HggCBShape(const char *name, const char *title, RooAbsReal& _m,
	     RooAbsReal& _m0, RooAbsReal& _sigma,
	     RooAbsReal& _alpha, RooAbsReal& _n);

  HggCBShape(const HggCBShape& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new HggCBShape(*this,newname); }

  inline virtual ~HggCBShape() { }

  virtual Int_t getAnalyticalIntegral( RooArgSet& allVars,  RooArgSet& analVars, const char* rangeName=0 ) const;
  virtual Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;

  // Optimized accept/reject generator support
  virtual Int_t getMaxVal(const RooArgSet& vars) const ;
  virtual Double_t maxVal(Int_t code) const ;

protected:

  Double_t ApproxErf(Double_t arg) const ;

  RooRealProxy m;
  RooRealProxy m0;
  RooRealProxy sigma;
  RooRealProxy alpha;
  RooRealProxy n;

  Double_t evaluate() const;

private:

  ClassDef(Hfitter::HggCBShape,1) // Crystal Ball lineshape PDF
};

}

#endif
