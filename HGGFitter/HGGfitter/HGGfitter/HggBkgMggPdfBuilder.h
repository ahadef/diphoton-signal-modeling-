// $Id: HggBkgMggPdfBuilder.h,v 1.1 2006/11/07 01:46:59 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HggBkgMggPdfBuilder
#define ROOT_Hfitter_HggBkgMggPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {
  
  class HggBkgMggPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggBkgMggPdfBuilder() { }    
    virtual ~HggBkgMggPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* xiName = "xi") { m_depName = depName; m_xiName = xiName; }

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HggBkgMggPdfBuilder, 0);
    
  private:
    
    TString m_depName, m_xiName;
    
  };
}

#endif
