// Author: Marine Kuna

// Higgs signal PDF with isolation

#ifndef ROOT_Hfitter_HggBkgMggDiffIsolBernsteinHistoBuilder
#define ROOT_Hfitter_HggBkgMggDiffIsolBernsteinHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggBkgMggDiffIsolBernsteinHistoBuilder : public HftAbsPdfBuilder {
    
  public:
          
    HggBkgMggDiffIsolBernsteinHistoBuilder() { }    
    virtual ~HggBkgMggDiffIsolBernsteinHistoBuilder() { }

        
    void Setup(bool isGam1, bool isGam2, const char* depName, const char* depName1, const char* depName2, 
                double xMin, double xMax,
               const char* cs1ggName = "cs1_gg", const char* cs1gjName = "cs1_gj", const char* cs2ggName = "cs2_gg", const char* cs2gjName = "cs2_gj",
               const char* cs3ggName = "", const char* cs3gjName = "",  const char* cs4ggName = "", const char* cs4gjName = "",
               const char* cs5ggName = "", const char* cs5gjName = "", const char* cs6ggName = "", const char* cs6gjName = "",
	       const char* cs7ggName = "", const char* cs7gjName = "" ) ;
 	       
	       
    void Setup(bool isGam1, bool isGam2, const char* depName, const char* depName1, const char* depName2,
               const char* cs1ggName, const char* cs1gjName, const char* cs2ggName, const char* cs2gjName, 
               const char* cs3ggName = "", const char* cs3gjName = "", const char* cs4ggName = "", const char* cs4gjName = "", 
               const char* cs5ggName = "", const char* cs5gjName = "", const char* cs6ggName = "", const char* cs6gjName = "",
	       const char* cs7ggName = "",const char* cs7gjName = "");
	      

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    TString m_depName, m_depName1, m_depName2;
    std::vector<TString> m_csggNames;
    std::vector<TString> m_csgjNames;
    double m_xMin, m_xMax;
    
    
    bool m_isGam1;
    bool m_isGam2;
    
         
    ClassDef(Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder, 0);
  };
}

#endif
