/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitModels                                                     *
 *    File: $Id: HggBernstein.h 28259 2009-04-16 16:21:16Z wouter $
 * Authors:                                                                  *
 *   Kyle Cranmer
 *                                                                           *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/
#ifndef ROOT_Hfitter_HggBernstein
#define ROOT_Hfitter_HggBernstein

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooListProxy.h"

class RooRealVar;
class RooArgList ;

namespace Hfitter {

class HggBernstein : public RooAbsPdf {
public:

  HggBernstein() ;
  HggBernstein(const char *name, const char *title,
               RooAbsReal& _x, const RooArgList& _coefList, double xMin = 0, double xMax = -1) ;

  HggBernstein(const HggBernstein& other, const char* name = 0);
  virtual TObject* clone(const char* newname) const { return new HggBernstein(*this, newname); }
  inline virtual ~HggBernstein() { }

  Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

private:

  RooRealProxy _x;
  RooListProxy _coefList ;
  double _xMin, _xMax;
  
  Double_t evaluate() const;

  ClassDef(HggBernstein,1) // Bernstein polynomial PDF
};
}
#endif
