#ifndef ROOT_Hfitter_HggPowHegLSPdfBuilder
#define ROOT_Hfitter_HggPowHegLSPdfBuilder

#include "HGGfitter/Hgg2sCBPdfBuilder.h"
#include "HGGfitter/RooRelBreitWignerPwave_LS.h"

namespace Hfitter {
  
  class HggPowHegLSPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HggPowHegLSPdfBuilder() { }
    virtual ~HggPowHegLSPdfBuilder() { }

    void Setup(const char* depName = "mgg", const char* bwPeakName = "bwPeak", const char* bwGammaName = "Gamma",
               const char* cbPeakName = "cbPeak", const char* cbSigmaName = "cbSigma", 
               const char* cbAlphaLoName = "cbAlphaLo", const char* cbNLoName = "cbNLo",  
               const char* cbAlphaHiName = "cbAlphaHi", const char* cbNHiName = "cbNHi");
    
    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    Hgg2sCBPdfBuilder m_2sCBBuilder;
    TString m_depName, m_bwPeakName, m_bwGammaName;
    
    ClassDef(Hfitter::HggPowHegLSPdfBuilder, 0);
  };
}

#endif
