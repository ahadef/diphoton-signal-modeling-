// Author: Marine Kuna

// Higgs signal PDF with isolation

#ifndef ROOT_Hfitter_HggBkgMggIsolExpPolyHistoBuilder
#define ROOT_Hfitter_HggBkgMggIsolExpPolyHistoBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"


#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HggBkgMggIsolExpPolyHistoBuilder : public HftAbsPdfBuilder {
    
  public:
    
      
    HggBkgMggIsolExpPolyHistoBuilder() { }    
    virtual ~HggBkgMggIsolExpPolyHistoBuilder() { }

        
    void Setup(bool isGam1, bool isGam2, const char* depName = "mgg",
               const char* depName1 = "isol1", const char* depName2 = "isol2",
	       const char* c1Name = "c1", const char* c2Name = "c2", 
               const char* c3Name = "", const char* c4Name = "", 
               const char* c5Name = "", const char* c6Name = "");
	       
	       
    void Setup(bool isGam1, bool isGam2, const char* depName, 
              const char* depName1, const char* depName2,
              double offset, const char* c1Name = "c1", const char* c2Name = "c2", 
              const char* c3Name = "", const char* c4Name = "", 
              const char* c5Name = "", const char* c6Name = "");
 

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

  private:
    
    TString m_depName, m_depName1, m_depName2;
    std::vector<TString> m_cNames;
    double m_offset;
   
    bool m_isGam1;
    bool m_isGam2;
         
    ClassDef(Hfitter::HggBkgMggIsolExpPolyHistoBuilder, 0);
  };
}

#endif
