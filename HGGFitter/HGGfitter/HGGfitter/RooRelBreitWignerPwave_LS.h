#ifndef ROO_RELBREITWIGNERPWAVE_LS
#define ROO_RELBREITWIGNERPWAVE_LS

#include <TFile.h>
#include <TF1.h>
#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooTFnPdfBinding.h"
#include <RooGenericPdf.h>
#include "TComplex.h"

class RooRealVar;


class RooRelBreitWignerPwave_LS : public RooAbsPdf {
public:
  RooRelBreitWignerPwave_LS() {} ;
  RooRelBreitWignerPwave_LS(const char *name, const char *title,
	      RooAbsReal& _x, RooAbsReal& _mean, RooAbsReal& _width);
  RooRelBreitWignerPwave_LS(const RooRelBreitWignerPwave_LS& other, const char* name=0) ;
  virtual TObject* clone(const char* newname) const { return new RooRelBreitWignerPwave_LS(*this,newname); }
  inline virtual ~RooRelBreitWignerPwave_LS() { }
  static Double_t LSweight(Double_t *x, Double_t *par);
  

  //Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0) const ;
  //Double_t analyticalIntegral(Int_t code, const char* rangeName=0) const ;

protected:

  RooRealProxy x ;
  RooRealProxy mean ;
  RooRealProxy width ;
  
  Double_t evaluate() const ;

  TF1 TF_LSweight;
//   void initGenerator();
//   Int_t generateDependents();

private:

  ClassDef(RooRelBreitWignerPwave_LS,1) // Breit Wigner PDF
};

#endif
