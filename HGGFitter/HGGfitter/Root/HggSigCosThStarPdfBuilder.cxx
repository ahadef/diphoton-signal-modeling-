// $Id: HggSigCosThStarPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggSigCosThStarPdfBuilder.h"

#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooBifurGauss.h"
#include "RooAddPdf.h"
#include "HfitterModels/HftPeggedPoly.h"

using namespace Hfitter;

RooAbsPdf* HggSigCosThStarPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& cosThStar = *Dependent(dependents, "cosThStar", "", "cos #theta*");

  RooAbsReal& sig_pol1 = Param(workspace, "sig_pol1");
  RooAbsReal& sig_pol2 = Param(workspace, "sig_pol2");
  RooAbsReal& sig_pol3 = Param(workspace, "sig_pol3");
  RooAbsReal& sig_pol4 = Param(workspace, "sig_pol4");
  RooAbsReal& sig_pol5 = Param(workspace, "sig_pol5");
  RooAbsReal& sig_pol6 = Param(workspace, "sig_pol6");
  
  return new RooPolynomial(name, 
                            "Signal CosThStar PDF for H->gamma gamma", 
                            cosThStar,
                            RooArgList( sig_pol1, sig_pol2, sig_pol3, 
                                        sig_pol4, sig_pol5, sig_pol6 ) );
   
//    if (m_pdfType == 1) {
// 
//        // read data card
//        RooAbsReal& sig_pol1 = Param(workspace, "sig_pol1");
//        RooAbsReal& sig_pol2 = Param(workspace, "sig_pol2");
//        RooAbsReal& sig_pol3 = Param(workspace, "sig_pol3");
//        RooAbsReal& sig_pol4 = Param(workspace, "sig_pol4");
//        RooAbsReal& sig_pol5 = Param(workspace, "sig_pol5");
//        RooAbsReal& sig_pol6 = Param(workspace, "sig_pol6");
//        
//        return new RooPolynomial(name, 
//                                  "Signal CosThStar PDF for H->gamma gamma", 
//                                  cosThStar,
//                                  RooArgList( sig_pol1, sig_pol2, sig_pol3, 
//                                              sig_pol4, sig_pol5, sig_pol6 ) );
//    }
//    else if (m_pdfType == 2) {
// 
//       RooAbsReal& sigCosTh0_1       = Param(workspace, "sigCosTh0_1");
//       RooAbsReal& sigCosThSigmaL_1  = Param(workspace, "sigCosThSigmaL_1");
//       RooAbsReal& sigCosThSigmaR_1  = Param(workspace, "sigCosThSigmaR_1");
//       RooAbsReal& sigCosThRelNorm_1 = Param(workspace, "sigCosThRelNorm_1");
// 
//       RooAbsPdf* costhstrPdf_1 = new RooBifurGauss(name + "csthstr_1", 
//                                                     "1st signal cosTheta* PDF for H->gamma gamma", 
//                                                     cosThStar, sigCosTh0_1, 
//                                                     sigCosThSigmaL_1, sigCosThSigmaR_1 );
// 
//       RooAbsReal& sigCosTh0_2       = Param(workspace, "sigCosTh0_2");
//       RooAbsReal& sigCosThSigmaL_2  = Param(workspace, "sigCosThSigmaL_2");
//       RooAbsReal& sigCosThSigmaR_2  = Param(workspace, "sigCosThSigmaR_2");
// 
//       RooAbsPdf* costhstrPdf_2 = new RooBifurGauss(name + "csthstr_2", 
//                                                     "2nd signal cosTheta* PDF for H->gamma gamma", 
//                                                     cosThStar, sigCosTh0_2, 
//                                                     sigCosThSigmaL_2, sigCosThSigmaR_2 );
// 
// //       RooAbsReal& sigCosTh0_3       = Param(workspace,  "sigCosTh0_3",       0.03, 0, 1 );
// //       RooAbsReal& sigCosThSigmaL_3  = Param(workspace,  "sigCosThSigmaL_3",  0.5, 0.001, 200 );
// //       RooAbsReal& sigCosThSigmaR_3  = Param(workspace,  "sigCosThSigmaR_3",  0.5, 0.001, 200 );
// 
// //       RooAbsPdf* costhstrPdf_3 = new RooBifurGauss(name + "csthstr_3", 
// //                                                     "3rd signal cosTheta* PDF for H->gamma gamma", 
// //                                                     cosThStar, sigCosTh0_3, 
// //                                                     sigCosThSigmaL_3, sigCosThSigmaR_3 );
//       
// //       return new RooAddPdf(name + "_costhstar", "Signal cosTheta* PDF for H->gamma gamma", 
// //                             RooArgList( *costhstrPdf_1, *costhstrPdf_2, *costhstrPdf_3 ), 
// //                             RooArgList( sigCosThRelNorm_1, sigCosThRelNorm_2 ) );
//       return new RooAddPdf(name, "Signal cosTheta* PDF for H->gamma gamma", 
//                             RooArgList( *costhstrPdf_1, *costhstrPdf_2 ), 
//                             RooArgList( sigCosThRelNorm_1 ) );
//    }
//    else if (m_pdfType == 3) {
// 
//     RooAbsReal& sigCosThCoef1 = Param(workspace, "sigCosThCoef1");
//     RooAbsReal& sigCosThCoef2 = Param(workspace, "sigCosThCoef2");
//     RooAbsReal& sigCosThCoef3 = Param(workspace, "sigCosThCoef3");
//     RooAbsReal& sigCosThCoef4 = Param(workspace, "sigCosThCoef4");
//     RooAbsReal& sigCosThCoef5 = Param(workspace, "sigCosThCoef5");
//     RooAbsReal& sigCosThCoef6 = Param(workspace, "sigCosThCoef6");
// 
//     RooAbsReal& sigCosThPower = Param(workspace, "sigCosThPower");
//     RooAbsReal& sigCosThPed   = Param(workspace, "sigCosThPed");
// 
//     RooAbsReal& sigCosThMean1  = Param(workspace, "sigCosThMean1", "GeV", "");
//     RooAbsReal& sigCosThSigma1 = Param(workspace, "sigCosThSigma1", "GeV", "", 1);
// 
//     RooAbsReal& sigCosThMean2  = Param(workspace, "sigCosThMean2", "GeV", "");
//     RooAbsReal& sigCosThSigma2 = Param(workspace, "sigCosThSigma2", "GeV", "", 1);
//     
//     RooAbsReal& sigCosThRelNorm1 = Param(workspace, "sigCosThRelNorm1");
//     RooAbsReal& sigCosThRelNorm2 = Param(workspace, "sigCosThRelNorm2");
//     
//     RooAbsPdf* csthPdf1 = new RooGaussian(name + ".Bump1",
//                                           "1st half of sig cosThStar PDF for H->gamma gamma bump",
//                                           cosThStar,
//                                           sigCosThMean1, sigCosThSigma1 );
//     
//     RooAbsPdf* csthPdf2 = new RooGaussian(name + ".Bump2",
//                                           "2st half of sig cosThStar PDF for H->gamma gamma bump",
//                                           cosThStar,
//                                           sigCosThMean2, sigCosThSigma2 );
//     
//     RooAbsPdf* csthpoly = new HftPeggedPoly(name + ".Poly",
//                                             "signal cosThStar polynomial PDF for H->gamma gamma",
//                                             cosThStar,
//                                             RooArgList( sigCosThCoef1, sigCosThCoef2, sigCosThCoef3, sigCosThCoef4, sigCosThCoef5, sigCosThCoef6 ),
//                                                         sigCosThPower, sigCosThPed, 0, 1 );
//     return new RooAddPdf(name, "signal cos theta* PDF for H->gamma gamma",
//                           RooArgList( *csthPdf1, *csthPdf2, *csthpoly ), 
//                           RooArgList( sigCosThRelNorm1, sigCosThRelNorm2 ) );
//    }
//    else {
//       RooAbsReal& sigCosTh0_1       = Param(workspace, "sigCosTh0_1");
//       RooAbsReal& sigCosThSigma_1   = Param(workspace, "sigCosThSigma_1");
//       RooAbsReal& sigCosThRelNorm_1 = Param(workspace, "sigCosThRelNorm_1");
//       RooAbsPdf* costhstrPdf_1 = new RooGaussian(name + "csthstr_1", 
//                                                   "1st signal cosTheta* PDF for H->gamma gamma", 
//                                                   cosThStar, sigCosTh0_1, sigCosThSigma_1 );
// 
//       RooAbsReal& sigCosTh0_2       = Param(workspace, "sigCosTh0_2");
//       RooAbsReal& sigCosThSigma_2   = Param(workspace, "sigCosThSigma_2");
// 
//       RooAbsPdf* costhstrPdf_2 = new RooGaussian(name + "csthstr_2", 
//                                                   "1st signal cosTheta* PDF for H->gamma gamma", 
//                                                   cosThStar, sigCosTh0_2, sigCosThSigma_2 );
// 
//       return new RooAddPdf(name, "Signal cosTheta* PDF for H->gamma gamma", 
//                             RooArgList( *costhstrPdf_1, *costhstrPdf_2 ), 
//                             RooArgList( sigCosThRelNorm_1 ) );
//    }
}
