#include "HGGfitter/HggCouplingsBuilder.h"

#include "HGGfitter/HggCouplings.h"
#include "RooCFunction2Binding.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;

void HggCouplingsBuilder::Setup(const char* type, const char* kappa_f_name, const char* kappa_V_name)
{
  m_type = type;
  m_kappa_f_name = kappa_f_name;
  m_kappa_V_name = kappa_V_name;
  if (m_type != "mu_t" && m_type != "mu_VBF" && m_type != "mu_VH") {
    cout << "ERROR : unsupported coupling type " << m_type << endl;
  }
}


RooAbsReal* HggCouplingsBuilder::Real(const TString& name, RooWorkspace& workspace) const
{
  RooAbsReal& kappa_f = Param(workspace, m_kappa_f_name);
  RooAbsReal& kappa_V = Param(workspace, m_kappa_V_name);

  typedef double (*RealFunc2Type)(double, double);
  RealFunc2Type func = 0;

  if (m_type == "mu_t") func = HggCouplings::mu_t;
  else if (m_type == "mu_VBF") func = HggCouplings::mu_VBF;
  else if (m_type == "mu_VH") func = HggCouplings::mu_VH;
  else {
    cout << "ERROR : unsupported coupling type " << m_type << endl;
    return 0;
  }
  return new RooCFunction2Binding<double, double, double>(name, "", func, kappa_f, kappa_V);
}
