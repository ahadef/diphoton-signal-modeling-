// $Id: HggSigMggIsolBremPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggSigMggIsolBremPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooExponential.h"

using namespace Hfitter;

HggSigMggIsolBremPdfBuilder::HggSigMggIsolBremPdfBuilder()
{ 
}


RooAbsPdf* HggSigMggIsolBremPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  if (!m_brem1 && !m_brem2) return m_mggIsolPdfBuilder.Pdf(name, dependents, workspace);
  
  RooArgSet pdfs;
  pdfs.add(*m_mggIsolPdfBuilder.Pdf(name + "_mggIsol", dependents, workspace));
  
  if(m_brem1) {
    RooRealVar* brem1 = Dependent(dependents, "brem1");
    pdfs.add(*new RooExponential(name + "_brem1", "", *brem1, Param(workspace, "sigSlopeBrem1")));
  }
  
  if(m_brem2) {  
    RooRealVar* brem2 = Dependent(dependents, "brem2");
    pdfs.add(*new RooExponential(name + "_brem2", "", *brem2, Param(workspace, "sigSlopeBrem2")));
  }
  
  return new RooProdPdf(name, "Signal PDF for mgg and isolation", pdfs);
}
