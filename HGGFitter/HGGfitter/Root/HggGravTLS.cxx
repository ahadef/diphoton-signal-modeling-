//////////////////////////////////////////
//  New Graviton theoretical line shape //
// (Lgg + alpha * Lqq ) * m^7 * BW      //
//

// class declaration include file below retrieved from workspace code storage
#include "HGGfitter/HggGravTLS.h"

ClassImp(HggGravTLS)

//_____________________________________________________________________________
HggGravTLS:: HggGravTLS() {
}

//_____________________________________________________________________________
HggGravTLS::HggGravTLS(const char *name, const char *title,
					   RooAbsReal& _x, RooAbsReal& _mG, RooAbsReal& _GkM, int _cme) :
  RooAbsPdf(name, title),
  x("x", "Dependent", this, _x),
  mG("mG", "Mass", this, _mG),
  GkM("GkM", "GkM", this, _GkM),
  cme(_cme)
{
}


//_____________________________________________________________________________
HggGravTLS::HggGravTLS(const HggGravTLS& other, const char* name) :
  RooAbsPdf(other, name),
  x("x", this, other.x),
  mG("mG", this, other.mG),
  GkM("GkM", this, other.GkM),
  cme(other.cme)
{
}

//_____________________________________________________________________________
Double_t HggGravTLS::evaluate() const {
  // pre-defined variables
  Double_t m2Res=mG*mG;
  Double_t sHat=x*x;
  
  if(cme==13){
    Double_t wRes=1.44*mG*GkM*GkM; // Width of the resonance
    Double_t GamMRat=wRes/mG;
    // Breit-Wigner shape
    Double_t weightBW=1 / ((sHat - m2Res)*(sHat - m2Res) + (sHat * GamMRat)*(sHat * GamMRat));
  
    double Lgg = pow(1-pow(x/13000,0.982575/3),11.2968) * pow(x/13000,-2.58083) * (1.89206e-6);
    double Lqq = pow(1-pow(x/13000,1./3),8.0919) * pow(x/13000,-2.26565) * (8.43489e-8);
    double alpha = 3./2;
    return pow(x,7)*(Lgg+alpha*Lqq)*weightBW; 
  }
  else if(cme==8){
    Double_t wRes = 1.43813*mG*GkM*GkM;
    Double_t GamMRat=wRes/mG;
    Double_t weightBW = 1 / ((sHat - m2Res)*(sHat - m2Res) + (sHat * GamMRat)*(sHat * GamMRat));
    // Parameters derived from Bernstein polynomial, in the range from 200 GeV to 3.5 TeV.
    // p0 fixed to 0
    // p1        = 0.0607621    +/-  0.015913  (limited)
    // p2        = 0.327415     +/-  0.080904  (limited)
    // p3        = 0.274664     +/-  0.0737216 (limited)
    // p4        = 0.134802     +/-  0.0385843 (limited)
    // p5        = 0.0690092    +/-  0.0198695 (limited)
    // p6        = 0.039084     +/-  0.00980382        (limited)
    
    double p[7]={0, 0.0607621, 0.327415, 0.274664, 0.134802, 0.0690092, 0.039084};
    Double_t weightPL = 1;
    if(x<200) weightPL=bernstein(6, p, 200, 200, 3500);
    else if(x>3500) weightPL=bernstein(6, p, 3500, 200, 3500);
    else weightPL=bernstein(6, p, x, 200, 3500);
    Double_t w=weightBW*weightPL;          
    return w;
  }
  else{
    std::cerr<<"Uknown center of mass energy "<<cme<<std::endl;
    abort();
  }
}

//_____________________________________________________________________________
Double_t HggGravTLS::bernstein(int degree, double* p, double mgg, double xmin, double xmax) const 
{
  Double_t mggx = (mgg - xmin) / (xmax - xmin); // rescale to [0,1]

  if(degree == 0) {

    return p[0];

  } else if(degree == 1) {

    Double_t a0 = p[0]; // c0
    Double_t a1 = p[1] - a0; // c1 - c0
    return a1 * mggx + a0;

  } else if(degree == 2) {

    Double_t a0 = p[0]; // c0
    Double_t a1 = 2 * (p[1] - a0); // 2 * (c1 - c0)
    Double_t a2 = p[2] - a1 - a0; // c0 - 2 * c1 + c2
    return (a2 * mggx + a1) * mggx + a0;

  } else if(degree > 2) {

    Double_t t = mggx;
    Double_t s = 1 - mggx;

    Double_t result = p[0] * s;    
    for(Int_t i = 1; i < degree; i++) {
      result = (result + t * TMath::Binomial(degree, i) * (p[i])) * s;
      t *= mggx;
    }
    result += t * p[degree]; 

    return result;
  }

  // in case list of arguments passed is empty
  return TMath::SignalingNaN();
}
