#include "HGGfitter/HggBkgMggLaurent4PdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooClassFactory.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggLaurent4PdfBuilder::Pdf(const TString& /*name*/, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, "mgg", "GeV", "m_{#gamma#gamma}");
  
 
  RooAbsReal& f1   = Param(workspace, "f1",  "");//,0 ,1);
  RooAbsReal& f2   = Param(workspace, "f2", "");//,0 ,1);
  RooAbsReal& f3   = Param(workspace, "f3", "");//,0 ,1);
 

  RooAbsPdf* pdf = RooClassFactory::makePdfInstance("GenPdf","f1*pow(mgg,-3) + f2*pow(mgg,-4) + f3*pow(mgg,-5) + (1-f1-f2-f3)*pow(mgg,-6) ",RooArgSet(mgg,f1, f2, f3)) ;

  return pdf;

}
