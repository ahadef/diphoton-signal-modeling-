// $Id: HggBkgMggCosThStarPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggCosThStarPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;


RooAbsPdf* HggBkgMggCosThStarPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{ 
  RooAbsPdf* mggPdf = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);
  RooAbsPdf* cThPdf = m_cThPdfBuilder.Pdf(name + "_cTh", dependents, workspace);

  return new RooProdPdf(name, "Background PDF for mgg and cosThStar", RooArgSet(*mggPdf, *cThPdf));
}
