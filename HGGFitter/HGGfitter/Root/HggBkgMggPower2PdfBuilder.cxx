#include "HGGfitter/HggBkgMggPower2PdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooClassFactory.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggPower2PdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, "mgg", "GeV", "m_{#gamma#gamma}");
  
  RooAbsReal& frac = Param(workspace, "frac");
  RooAbsReal& p1   = Param(workspace, "p1",  "", -20, 0.);
  RooAbsReal& p2   = Param(workspace, "p2", "", -20, 0.);


  RooAbsPdf* pdf1 = RooClassFactory::makePdfInstance("GenPdf1","pow(mgg,p1)",RooArgSet(mgg,p1)) ;
  RooAbsPdf* pdf2 = RooClassFactory::makePdfInstance("GenPdf2","pow(mgg,p2)",RooArgSet(mgg,p2)) ;
  return new RooAddPdf(name, "Background PDF for mgg", RooArgList(*pdf2, *pdf1), RooArgList(frac));



}
