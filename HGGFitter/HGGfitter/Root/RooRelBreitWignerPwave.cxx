#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>
#include "TMath.h"

#include "HGGfitter/RooRelBreitWignerPwave.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
// #include "RooFitTools/RooRandom.h"

using namespace std;

ClassImp(RooRelBreitWignerPwave)


////////////////////////////////////////////////////////////////////////////////

RooRelBreitWignerPwave::RooRelBreitWignerPwave(const char *name, const char *title,
          RooAbsReal& _x, RooAbsReal& _mean,
          RooAbsReal& _width) :
  RooAbsPdf(name,title),
  x("x","Dependent",this,_x),
  mean("mean","Mean",this,_mean),
  width("width","Width",this,_width)
{
}



////////////////////////////////////////////////////////////////////////////////

RooRelBreitWignerPwave::RooRelBreitWignerPwave(const RooRelBreitWignerPwave& other, const char* name) : 
  RooAbsPdf(other,name), x("x",this,other.x), mean("mean",this,other.mean),
  width("width",this,other.width)
{
}



////////////////////////////////////////////////////////////////////////////////

Double_t RooRelBreitWignerPwave::evaluate() const
{
  Double_t k = x*x*width/(mean*TMath::Pi()); 
  return k / ((x*x- mean*mean)*(x*x- mean*mean) + (x*x/mean*width)*(x*x/mean*width));

//  Double_t arg= x - mean;  
//  return 1. / (arg*arg + 0.25*width*width);
}



//////////////////////////////////////////////////////////////////////////////////
//
//Int_t RooRelBreitWignerPwave::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const 
//{
//  if (matchArgs(allVars,analVars,x)) return 1 ;
//  return 0 ;
//}
//
//
//
//////////////////////////////////////////////////////////////////////////////////
//
//Double_t RooRelBreitWignerPwave::analyticalIntegral(Int_t code, const char* rangeName) const 
//{
//  switch(code) {
//  case 1: 
//    {
//      Double_t c = 2./width;
//      return c*(atan(c*(x.max(rangeName)-mean)) - atan(c*(x.min(rangeName)-mean)));
//    }
//  }
//  
//  assert(0) ;
//  return 0 ;
//}

