// $Id: HggExpPowerPdf.cxx,v 1.4 2007/12/13 20:34:23 hoecker Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggExpPowerPdf.h"
#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "TMath.h"

using namespace Hfitter;


HggExpPowerPdf::HggExpPowerPdf(const HggExpPowerPdf& other, const char* name)
   : RooAbsPdf( other, name ),
     m_x    ("x",     this, other.m_x),
     m_xi   ("xi",    this, other.m_xi),
     m_power("power", this, other.m_power),
     m_coeff("coeff", this, other.m_coeff)
{}

HggExpPowerPdf::HggExpPowerPdf(const char* name, const char* title, 
                               RooAbsReal& x, RooAbsReal& xi, RooAbsReal& power, RooAbsReal& coeff)
   : RooAbsPdf( name, title ),
     m_x    ("x",     "Dependent",            this, x),
     m_xi   ("xi",    "Exponential slope",    this, xi),
     m_power("power", "Monomial power",       this, power),
     m_coeff("coeff", "Monomial coefficient", this, coeff)
{}


Double_t HggExpPowerPdf::evaluate() const
{
   return TMath::Exp(m_xi*m_x) + m_coeff*TMath::Power(m_x, m_power);
}


Int_t HggExpPowerPdf::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/ ) const 
{
   if (matchArgs(allVars,analVars,m_x)) return 1;
   return 0;
}


Double_t HggExpPowerPdf::analyticalIntegral( Int_t code, const char* rangeName ) const 
{
   switch(code) {
   case 1: 
     
     double integral = 0;
     if (m_xi != 0) 
       integral += (TMath::Exp(m_xi*m_x.max(rangeName)) - 
                    TMath::Exp(m_xi*m_x.min(rangeName)))/m_xi;
     if (m_power != -1) 
       integral += m_coeff/(m_power + 1)*(TMath::Power(m_x.max(rangeName), m_power + 1) -
                                          TMath::Power(m_x.min(rangeName), m_power + 1));
     else {
       double ratio = m_x.max(rangeName)/m_x.min(rangeName);
       if (ratio > 0) integral += m_coeff*TMath::Log(ratio);
     }
     return integral;
   }
   
  assert(0);
  return 0;
}
