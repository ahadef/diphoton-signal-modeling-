// $Id: HggBkgMggPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "RooExponential.h"
//#include "RooAddition.h"
#include "RooConstVar.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  RooAbsReal& xi  = Param(workspace, m_xiName,  "1/GeV");
  
  //RooAddition* mggShifted = new RooAddition(m_depName + "Shifted", "", RooArgList(mgg, RooFit::RooConst(90)));  
  return new RooExponential(name, "Exponential PDF", mgg, xi);
}
