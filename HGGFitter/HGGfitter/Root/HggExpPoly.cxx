#include "HGGfitter/HggExpPoly.h"

#include "RooFit.h"
#include "Riostream.h"
#include "Riostream.h"
#include <math.h>
#include "TMath.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooArgList.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


//_____________________________________________________________________________
HggExpPoly::HggExpPoly()
{
}


//_____________________________________________________________________________
HggExpPoly::HggExpPoly(const char* name, const char* title, RooAbsReal& x, const RooArgList& coefList, double offset): 
  RooAbsPdf(name, title),
  _x("x", "Dependent", this, x),
  _coefList("coefficients","List of coefficients",this),
  m_offset(offset)
{
  // Constructor
  TIterator* coefIter = coefList.createIterator() ;
  RooAbsArg* coef ;
  while((coef = (RooAbsArg*)coefIter->Next())) {
    if (!dynamic_cast<RooAbsReal*>(coef)) {
      cout << "HggExpPoly::ctor(" << GetName() << ") ERROR: coefficient " << coef->GetName() 
           << " is not of type RooAbsReal" << endl ;
      assert(0) ;
    }
    _coefList.add(*coef) ;
  }
  delete coefIter ;
}


//_____________________________________________________________________________
HggExpPoly::HggExpPoly(const HggExpPoly& other, const char* name) :
  RooAbsPdf(other, name), 
  _x("x", this, other._x), 
  _coefList("coefList",this,other._coefList),
  m_offset(other.m_offset)
{
}


//_____________________________________________________________________________
Double_t HggExpPoly::evaluate() const 
{
  double fact = (m_offset != 0 ? (_x - m_offset)/m_offset : _x);
  double var = fact, val = 0;
  Int_t degree = _coefList.getSize();

  RooFIter iter = _coefList.fwdIterator() ;
  for (int i=0; i < degree; ++i) {
    val += ((RooAbsReal*)iter.next())->getVal() * var;
    var *= fact;
  }
  return TMath::Exp(val);
}


//_____________________________________________________________________________
Int_t HggExpPoly::getAnalyticalIntegral(RooArgSet& /*allVars*/, RooArgSet& /*analVars*/, const char* /*rangeName*/) const 
{
  // integral for n=2 exists but is unstable 
  // if (matchArgs(allVars, analVars, _x) && _coefList.getSize() <= 2) return 1;
  return 0;
}


//_____________________________________________________________________________
Double_t HggExpPoly::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code == 1);
  Double_t xMin = (m_offset != 0 ? (_x.min(rangeName) - m_offset)/m_offset : _x.min(rangeName)); 
  Double_t xMax = (m_offset != 0 ? (_x.max(rangeName) - m_offset)/m_offset : _x.max(rangeName)); 

  if (_coefList.getSize() == 1) {
    // exponential integral
    double slope = ((RooAbsReal*)_coefList.at(0))->getVal();
    if (slope == 0) {
      cout << "ERROR in ggExpPoly::analyticalIntegral : slope of exponential is 0, returning 0." << endl;
      return 0;
    }
    double result = (TMath::Exp(slope*xMax) - TMath::Exp(slope*xMin))/slope;
    if (m_offset != 0) result *= m_offset;
    return result;
  }
  

  if (_coefList.getSize() == 2) {
    // Gaussian integral
    double b = ((RooAbsReal*)_coefList.at(0))->getVal();
    double a = ((RooAbsReal*)_coefList.at(1))->getVal();
    if (a >= 0) {
      cout << "ERROR in ggExpPoly::analyticalIntegral : quadratic coefficient is >= 0, returning 0" << endl;
      return 0;
    }
    double sqrta = TMath::Sqrt(-a);
    double argMin = (2*a*xMin + b)/(2*sqrta);
    double argMax = (2*a*xMax + b)/(2*sqrta);
    cout << "xxx range = " << xMin << " " << xMax << " " << TMath::Sqrt(TMath::Pi())/2/sqrta*TMath::Exp(-b*b/(4*a))*(TMath::Erf(argMin) - TMath::Erf(argMax))*m_offset << endl;
    return TMath::Sqrt(TMath::Pi())/2/sqrta*TMath::Exp(-b*b/(4*a))*(TMath::Erf(argMin) - TMath::Erf(argMax))*m_offset;
  }

  assert(1);
  return 0;
}
