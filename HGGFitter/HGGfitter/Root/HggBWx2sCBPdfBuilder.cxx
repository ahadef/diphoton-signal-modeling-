#include "HGGfitter/HggBWx2sCBPdfBuilder.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftDatacardReader.h"

#include "RooBreitWigner.h"
#include "RooFFTConvPdf.h"
//#include "HGGfitter/HggFFTConvPdf.h"
#include "RooNumConvPdf.h"
#include "RooConstVar.h"
#include "RooGaussian.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

#include "RooGaussian.h"

void HggBWx2sCBPdfBuilder::Setup(const char* depName, const char* bwPeakName, const char* bwGammaName,  
                                 const char* cbPeakName, const char* cbSigmaName, 
                                 const char* cbAlphaLoName, const char* cbNLoName,
                                 const char* cbAlphaHiName, const char* cbNHiName)
{ 
  m_depName = depName;
  m_bwPeakName = bwPeakName;
  m_bwGammaName = bwGammaName;
  m_2sCBBuilder.Setup(depName, cbPeakName, cbSigmaName, cbAlphaLoName, cbNLoName, cbAlphaHiName, cbNHiName);
}


RooAbsPdf* HggBWx2sCBPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");
  // Breit-Wigner
  RooAbsReal& bwPeak  = Param(workspace, m_bwPeakName, "GeV", "", 0);
  RooAbsReal& bwGamma  = Param(workspace, m_bwGammaName, "GeV", "", 1);
  RooBreitWigner *bwPdf = new RooBreitWigner(name + "_BW", "Breit-Wigner lineshape", mgg, bwPeak, bwGamma);
  if (DatacardReader()) {
    RooArgSet* bwObs = bwPdf->getParameters(RooArgSet());
    DatacardReader()->ReadRealVars(*bwObs);
    delete bwObs;
  }
  // Resolution
  RooAbsPdf* resolPdf = m_2sCBBuilder.Pdf(name + "_2sCB", dependents, workspace);
  if (DatacardReader()) {
    RooArgSet* resolObs = resolPdf->getParameters(RooArgSet());
    DatacardReader()->ReadRealVars(*resolObs);
    delete resolObs;
  }
  //HftModel::Print(*bwPdf);
  //cout << "//////////////////// (x) //////////////////" << endl;
  //HftModel::Print(*resolPdf);
  
  // Convolution DSCB*BW
  //cout << mgg.getMin() << " " << mgg.getMax() << endl;
  RooFFTConvPdf* pdf = new RooFFTConvPdf(name, "BW #otimes DSCB", mgg, *bwPdf, *resolPdf);
  //HggFFTConvPdf* pdf = new HggFFTConvPdf(name, "BW #otimes DSCB", mgg, *bwPdf, *resolPdf);
  mgg.setBins(50000, "cache");
  //cout << pdf << endl;
  //cout << pdf->getVal() << endl;
  
//   // HACK
//   RooGaussian* gaussianPdf = new RooGaussian(name + "_gaussian", "", mgg, 
//                                              Param(workspace, "cbPeakSignal"), Param(workspace, "cbSigmaSignal"));
//   return new RooFFTConvPdf(name, "BW #otimes DSCB", mgg, *bwPdf, *gaussianPdf);

  
  return pdf;
}
