// $Id: HggSigMggPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggSigMggPdfBuilder.h"

#include "RooGaussian.h"
#include "HGGfitter/HggCBShape.h"
#include "RooCBShape.h"
#include "RooAddPdf.h"
#include "RooFormulaVar.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void HggSigMggPdfBuilder::Setup(const char* depName, const char* cbPeakName, const char* cbSigmaName, const char* cbAlphaName, const char* cbNName,  
                                const char* tailPeakName, const char* tailSigmaName, const char* tailFractionName, 
                                unsigned int pdfType, const char* cat) 
{ 
  m_depName = depName; 
  m_cbPeakName = cbPeakName; 
  m_cbSigmaName = cbSigmaName; 
  m_cbAlphaName = cbAlphaName; 
  m_cbNName = cbNName;
  m_tailPeakName = tailPeakName; 
  m_tailSigmaName = tailSigmaName; 
  m_tailFractionName = tailFractionName;
  m_pdfType = pdfType; 
  if (TString(cat) != "") SetSuffix("_{" + TString(cat) + "}");  
}


RooAbsPdf* HggSigMggPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  RooAbsPdf* peakPdf = 0;
    
  if (m_pdfType == 0)
    peakPdf = new HggCBShape(name + ".Peak", 
                             "Peak component of mgg Signal PDF", mgg,
                             Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                             Param(workspace, m_cbAlphaName), Param(workspace, m_cbNName));
  else
    peakPdf = new RooCBShape(name + ".Peak", 
                             "Peak component of mgg Signal PDF", mgg,
                             Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                             Param(workspace, m_cbAlphaName), Param(workspace, m_cbNName));

   RooAbsPdf* tailPdf = new RooGaussian(name + ".Tail", 
					"Tail components of mgg Signal PDF", mgg,
                                        Param(workspace, m_tailPeakName, "GeV"), Param(workspace, m_tailSigmaName, "GeV", "" , 1));

   return new RooAddPdf(name, "mgg Signal PDF",
			RooArgList(*peakPdf, *tailPdf), RooArgList(Param(workspace, m_tailFractionName)) );
}
