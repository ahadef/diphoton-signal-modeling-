#include "HGGfitter/HggZeeBifurMassPdfBuilder.h"

#include "RooGaussian.h"
#include "HGGfitter/HggBifurCBPdf.h"
#include "RooAddPdf.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void HggZeeBifurMassPdfBuilder::Setup(const char* depName, const char* cbPeakName, const char* cbSigmaName, 
                                 const char* cbAlphaLoName, const char* cbSigmaLoName,
                                 const char* cbAlphaHiName, const char* cbNHiName,
                                 const char* gsPeakName, const char* gsSigmaName, const char* cbFractionName) 
{ 
  m_depName = depName; 
  m_cbPeakName = cbPeakName; 
  m_cbSigmaName = cbSigmaName; 
  m_cbAlphaLoName = cbAlphaLoName; 
  m_cbAlphaHiName = cbAlphaHiName; 
  m_cbSigmaLoName = cbSigmaLoName;
  m_cbNHiName = cbNHiName;
  m_gsPeakName = gsPeakName; 
  m_gsSigmaName = gsSigmaName; 
  m_cbFractionName = cbFractionName;
}


RooAbsPdf* HggZeeBifurMassPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{ee}");

  RooAbsPdf* cbPdf = new HggBifurCBPdf(name + ".CB", 
                                       "Peak component of mee Signal PDF", mgg,
                                       Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                                       Param(workspace, m_cbAlphaLoName), Param(workspace, m_cbSigmaLoName),
                                       Param(workspace, m_cbAlphaHiName), Param(workspace, m_cbNHiName));

  RooAbsPdf* gsPdf = new RooGaussian(name + ".Gauss", 
                                       "Tail components of mee Signal PDF", mgg,
                                       Param(workspace, m_gsPeakName, "GeV"), Param(workspace, m_gsSigmaName, "GeV", "" , 1));

   return new RooAddPdf(name, "mee PDF",
                        RooArgList(*cbPdf, *gsPdf), RooArgList(Param(workspace, m_cbFractionName)) );
}
