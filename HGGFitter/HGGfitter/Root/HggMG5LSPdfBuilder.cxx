#include "HGGfitter/HggMG5LSPdfBuilder.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftDatacardReader.h"

#include "RooBreitWigner.h"
#include "HGGfitter/HggMG5TLS.h"
#include "RooFFTConvPdf.h"
#include "RooNumConvPdf.h"
#include "RooConstVar.h"


#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

#include "RooGaussian.h"

void HggMG5LSPdfBuilder::Setup(const char* depName, const char* bwPeakName, const char* bwGammaName,  
                                 const char* cbPeakName, const char* cbSigmaName, 
                                 const char* cbAlphaLoName, const char* cbNLoName,
                                 const char* cbAlphaHiName, const char* cbNHiName)
{ 
  m_depName = depName;
  m_bwPeakName = bwPeakName;
  m_bwGammaName = bwGammaName;
  m_2sCBBuilder.Setup(depName, cbPeakName, cbSigmaName, cbAlphaLoName, cbNLoName, cbAlphaHiName, cbNHiName);
}

RooAbsPdf* HggMG5LSPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{

  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");
  // Breit-Wigner
  RooAbsReal& bwPeak  = Param(workspace, m_bwPeakName, "GeV", "", 0);
  RooAbsReal& bwGamma  = Param(workspace, m_bwGammaName, "GeV", "", 1);
  //RooBreitWigner *bwPdf = new RooBreitWigner(name + "_BW", "Breit-Wigner lineshape", mgg, bwPeak, bwGamma);
  HggMG5TLS *bwPdf = new HggMG5TLS(name + "_BW", "Breit-Wigner lineshape", mgg, bwPeak, bwGamma);

  if (DatacardReader()) {
    RooArgSet* bwObs = bwPdf->getParameters(RooArgSet());
    DatacardReader()->ReadRealVars(*bwObs);
    delete bwObs;
  }


  // Resolution
  RooAbsPdf* resolPdf = m_2sCBBuilder.Pdf(name + "_2sCB", dependents, workspace);
  if (DatacardReader()) {
    RooArgSet* resolObs = resolPdf->getParameters(RooArgSet());
    DatacardReader()->ReadRealVars(*resolObs);
    delete resolObs;
  }
    
  // Convolution DSCB*BW
  // cout<< "Convolute with params" <<  endl;
  // cout<< "peak " << bwPeak.getVal() << "; width: " << bwGamma.getVal()<<  endl;

  RooFFTConvPdf* pdf = new RooFFTConvPdf(name, "BW #otimes DSCB", mgg, *bwPdf, *resolPdf);
  mgg.setBins(50000, "cache");
  return pdf;
}
