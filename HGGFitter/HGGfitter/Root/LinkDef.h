#ifdef __CINT__

#include <vector>

#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "RooExponential.h"
#include "RooLognormal.h"

#include "HGGfitter/MggBkgPdf.h"
#include "HGGfitter/MggBiasBkgPdf.h"
#include "HGGfitter/HggCBShape.h"
#include "HGGfitter/HggTwoSidedCBPdf.h"
#include "HGGfitter/HggTwoSidedNovoPdf.h"
#include "HGGfitter/HggNovoCBPdf.h"
#include "HGGfitter/HggBifurCBPdf.h"
#include "HGGfitter/HggLognormal.h"
#include "HGGfitter/HggStitchedPlateauPdf.h"
#include "HGGfitter/HggBernstein.h"
#include "HGGfitter/HggExpPowerPdf.h"
#include "HGGfitter/HggPowerLaw.h"
#include "HGGfitter/HggExpPoly.h"
#include "HGGfitter/RooRelBreitWignerPwave.h"
#include "HGGfitter/RooRelBreitWignerPwave_LS.h"
#include "HGGfitter/RooRelBreitWignerPwave_LSCut.h"
#include "HGGfitter/HggMG5TLS.h"
#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "HGGfitter/HggBkgMggDoubleExpPdfBuilder.h"
#include "HGGfitter/HggBkgMggExpPowerPdfBuilder.h"
#include "HGGfitter/HggBkgMggBiasPdfBuilder.h"
#include "HGGfitter/HggBkgMggPolyPdfBuilder.h"
#include "HGGfitter/HggBernsteinPdfBuilder.h"
#include "HGGfitter/HggChebyshevPdfBuilder.h"
#include "HGGfitter/HggBkgMggBernstein4PdfBuilder.h"
#include "HGGfitter/HggBkgMggBernstein5PdfBuilder.h"
#include "HGGfitter/HggBkgMggLaurent4PdfBuilder.h"
#include "HGGfitter/HggBkgMggPower2PdfBuilder.h"
#include "HGGfitter/HggExpPolyPdfBuilder.h"
#include "HGGfitter/HggPolyPdfBuilder.h"
#include "HGGfitter/HggGenericBuilder.h"
#include "HGGfitter/HggEliLandauExpBuilder.h"
#include "HGGfitter/HggSigCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgCosThStarPdfBuilder.h"
#include "HGGfitter/HggSigPTPdfBuilder.h"
#include "HGGfitter/HggBkgPTPdfBuilder.h"
#include "HGGfitter/HggSigMggCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h"
#include "HGGfitter/HggBkgMggPTKeysPdfBuilder.h"
#include "HGGfitter/HggSigMggPTPdfBuilder.h"
#include "HGGfitter/HggBkgMggPTPdfBuilder.h"
#include "HGGfitter/HggSigMggCosThStarPTPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h"
#include "HGGfitter/HggSigMggSmearMPdfBuilder.h"
#include "HGGfitter/HggBkgMggSmearMPdfBuilder.h"
#include "HGGfitter/HggExpPolySmearMPdfBuilder.h"
#include "HGGfitter/HggBernsteinSmearMPdfBuilder.h"
#include "HGGfitter/HggIsolPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolPdfBuilder.h"
#include "HGGfitter/HggBkgMggIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolBernsteinHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolBernsteinHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolExpPolyHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolExpPolyHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsol2DHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolKeysPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolBremPdfBuilder.h"
#include "HGGfitter/HggBkgMggIsolBremPdfBuilder.h"
#include "HGGfitter/HggZeeMassPdfBuilder.h"
#include "HGGfitter/HggZeeNovoMassPdfBuilder.h"
#include "HGGfitter/HggZeeBifurMassPdfBuilder.h"
#include "HGGfitter/HggTwoSidedNovoPdfBuilder.h"
#include "HGGfitter/Hgg2sCBPdfBuilder.h"
#include "HGGfitter/HggBWx2sCBPdfBuilder.h"
#include "HGGfitter/HggCouplings.h"
#include "HGGfitter/HggCouplingsBuilder.h"
#include "HGGfitter/HggTools.h"
#include "HGGfitter/HggEtaCatHelper.h"
#include "HGGfitter/HggResolHelper.h"
#include "HGGfitter/HggZeeIsolStudy.h"
#include "HGGfitter/HggNewResonanceStudy.h"
#include "HGGfitter/HggNLLVar.h"

#pragma link C++ namespace Hfitter;

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ class Hfitter::MggBkgPdf+; 
#pragma link C++ class Hfitter::MggBiasBkgPdf+; 
#pragma link C++ class Hfitter::HggCBShape+; 
#pragma link C++ class HggTwoSidedCBPdf+; 
#pragma link C++ class Hfitter::HggTwoSidedNovoPdf+;
#pragma link C++ class Hfitter::HggNovoCBPdf+; 
#pragma link C++ class Hfitter::HggBifurCBPdf+; 
#pragma link C++ class Hfitter::HggLognormal+; 
#pragma link C++ class Hfitter::HggModifiedLognormal+; 
#pragma link C++ class Hfitter::HggStitchedPlateauPdf+;
#pragma link C++ class Hfitter::HggBernstein+;
#pragma link C++ class Hfitter::HggExpPowerPdf+;
#pragma link C++ class Hfitter::HggPowerLaw+;
#pragma link C++ class Hfitter::HggExpPoly+;
#pragma link C++ class RooRelBreitWignerPwave+;
#pragma link C++ class RooRelBreitWignerPwave_LS+;
#pragma link C++ class RooRelBreitWignerPwave_LSCut+;
#pragma link C++ class HggMG5TLS+;

#pragma link C++ class Hfitter::HggSigMggPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggDoubleExpPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggExpPowerPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggBiasPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggPolyPdfBuilder+;
#pragma link C++ class Hfitter::HggBernsteinPdfBuilder+;
#pragma link C++ class Hfitter::HggChebyshevPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggBernstein4PdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggBernstein5PdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggLaurent4PdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggPower2PdfBuilder+;
#pragma link C++ class Hfitter::HggExpPolyPdfBuilder+;
#pragma link C++ class Hfitter::HggPolyPdfBuilder+;
#pragma link C++ class Hfitter::HggGenericBuilder+;
#pragma link C++ class Hfitter::HggEliLandauExpBuilder+;

#pragma link C++ class Hfitter::HggSigCosThStarPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgCosThStarPdfBuilder+;
#pragma link C++ class Hfitter::HggSigPTPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgPTPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggCosThStarPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggCosThStarPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggCosThStarKeysPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggPTKeysPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggPTPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggPTPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggCosThStarPTPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggCosThStarPTPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggSmearMPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggSmearMPdfBuilder+;
#pragma link C++ class Hfitter::HggExpPolySmearMPdfBuilder+;
#pragma link C++ class Hfitter::HggBernsteinSmearMPdfBuilder+;
#pragma link C++ class Hfitter::HggIsolPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggIsolPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggIsolHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsolPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsolHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggDiffIsolHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsolBernsteinHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsolExpPolyHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsol2DHistoBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsolKeysPdfBuilder+;
#pragma link C++ class Hfitter::HggSigMggIsolBremPdfBuilder+;
#pragma link C++ class Hfitter::HggBkgMggIsolBremPdfBuilder+;

#pragma link C++ class Hfitter::HggZeeMassPdfBuilder+;
#pragma link C++ class Hfitter::HggZeeNovoMassPdfBuilder+;
#pragma link C++ class Hfitter::HggZeeBifurMassPdfBuilder+;
#pragma link C++ class Hfitter::HggTwoSidedNovoPdfBuilder+;
#pragma link C++ class Hfitter::Hgg2sCBPdfBuilder+;
#pragma link C++ class Hfitter::HggBWx2sCBPdfBuilder+;

#include "HGGfitter/HggPowHegLSPdfBuilder.h"
#pragma link C++ class Hfitter::HggPowHegLSPdfBuilder+;

#include "HGGfitter/HggMG5LSPdfBuilder.h"
#pragma link C++ class Hfitter::HggMG5LSPdfBuilder+;

#pragma link C++ class Hfitter::HggCouplings+;
#pragma link C++ class Hfitter::HggCouplingsBuilder+;

#pragma link C++ class Hfitter::HggTools+; 
#pragma link C++ class Hfitter::HggEtaCatHelper+; 
#pragma link C++ class Hfitter::HggResolHelper+; 
#pragma link C++ class Hfitter::HggZeeIsolStudy+; 
#pragma link C++ class Hfitter::HggNewResonanceStudy+; 

// These need the corresponding headers, which aren't included by RootCore by default (although they are above)
// Work around for now by including them in HggCBShape
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooLognormal, 2>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal, 2>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal, 3>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooExponential, 1>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf, 6>+;

#pragma link C++ class HggNLLVar+;

#include "HGGfitter/HggGravitonLineShapePdf.hh"
#pragma link C++ class HggGravitonLineShapePdf;

#include "HGGfitter/HggGravLSPdfBuilder.h"
#pragma link C++ class Hfitter::HggGravLSPdfBuilder+;

#include "HGGfitter/HggGravTLS.h"
#pragma link C++ class HggGravTLS;
#include "HGGfitter/HggFactGravLSPdfBuilder.h"
#pragma link C++ class Hfitter::HggFactGravLSPdfBuilder+;

#endif
