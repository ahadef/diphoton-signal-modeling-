
#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>

#include "HGGfitter/HggTwoSidedNovoPdf.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooMath.h"
#include "TMath.h"
#include "Math/ProbFuncMathCore.h"

using namespace Hfitter;


//_____________________________________________________________________________
HggTwoSidedNovoPdf::HggTwoSidedNovoPdf(const char *name, const char *title,
           RooAbsReal& _m, RooAbsReal& _m0, RooAbsReal& _sigma,
           RooAbsReal& _alphaLo, RooAbsReal& _alphaHi) :
  RooAbsPdf(name, title),
  m("m", "Dependent", this, _m),
  m0("m0", "M0", this, _m0),
  sigma("sigma", "Sigma", this, _sigma),
  alphaLo("alphaLo", "Low-side Alpha", this, _alphaLo),
  alphaHi("alphaHi", "High-side Alpha", this, _alphaHi)
{
}


//_____________________________________________________________________________
HggTwoSidedNovoPdf::HggTwoSidedNovoPdf(const HggTwoSidedNovoPdf& other, const char* name) :
  RooAbsPdf(other, name), m("m", this, other.m), m0("m0", this, other.m0),
  sigma("sigma", this, other.sigma), 
  alphaLo("alphaLo", this, other.alphaLo), alphaHi("alphaHi", this, other.alphaHi)
{
}


//_____________________________________________________________________________
Double_t HggTwoSidedNovoPdf::evaluate() const {

  Double_t t = (m-m0)/sigma;

  if (t < -alphaLo) 
    return exp(+ alphaLo*(t + alphaLo) - 0.5*alphaLo*alphaLo);
  else if (t > alphaHi)
    return exp(- alphaHi*(t - alphaHi) - 0.5*alphaHi*alphaHi);
  return exp(-0.5*t*t);
}


//_____________________________________________________________________________
Int_t HggTwoSidedNovoPdf::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const
{
  if( matchArgs(allVars,analVars,m) )
    return 1 ;
  
  return 0;
}


//_____________________________________________________________________________
Double_t HggTwoSidedNovoPdf::analyticalIntegral(Int_t code, const char* rangeName) const
{
  assert(code==1);
  double result = 0;
    
  double sig = fabs((Double_t)sigma);
  double tmin = (m.min(rangeName)-m0)/sig;
  double tmax = (m.max(rangeName)-m0)/sig;

  if (tmin < -alphaLo) 
    result += expIntegral(tmin, TMath::Min(tmax, -alphaLo), alphaLo)*exp(0.5*alphaLo*alphaLo);
  if (tmin < alphaHi)
    result += gaussianIntegral(TMath::Max(tmin, -alphaLo), TMath::Min(tmax, alphaHi));
  if (tmax > alphaHi)
    result += expIntegral(TMath::Max(tmin, alphaHi), tmax, -alphaHi)*exp(0.5*alphaHi*alphaHi);

  return sig*result;
}


double HggTwoSidedNovoPdf::expIntegral(double tmin, double tmax, double beta) const
{
  return (exp(beta*tmax) - exp(beta*tmin))/beta;
}

double HggTwoSidedNovoPdf::gaussianIntegral(double tmin, double tmax) const
{
  return sqrt(TMath::TwoPi())*(ROOT::Math::gaussian_cdf(tmax) - ROOT::Math::gaussian_cdf(tmin));
}

