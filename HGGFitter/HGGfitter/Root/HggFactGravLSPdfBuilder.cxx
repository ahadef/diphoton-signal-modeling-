//////////////////////////////////////////
//  New Graviton line shape 
//  Convolution of detector resoluton with factorised graviton theoretical line shape
//


#include "HGGfitter/HggFactGravLSPdfBuilder.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftDatacardReader.h"

#include "RooGaussian.h"
#include "RooBreitWigner.h"
#include "HGGfitter/HggGravTLS.h"
#include "RooFFTConvPdf.h"
#include "RooNumConvPdf.h"
#include "RooConstVar.h"


#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void HggFactGravLSPdfBuilder::Setup(const char* depName, const char* mGName, const char* GkMName,  
                                 const char* cbPeakName, const char* cbSigmaName, 
                                 const char* cbAlphaLoName, const char* cbNLoName,
                                 const char* cbAlphaHiName, const char* cbNHiName)
{ 
  m_depName = depName;
  m_mGName = mGName;
  m_GkMName = GkMName;
  m_2sCBBuilder.Setup(depName, cbPeakName, cbSigmaName, cbAlphaLoName, cbNLoName, cbAlphaHiName, cbNHiName);
}

RooAbsPdf* HggFactGravLSPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{

  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");
  // Breit-Wigner
  RooAbsReal& mG  = Param(workspace, m_mGName, "GeV", "", 0);
  RooAbsReal& GkM  = Param(workspace, m_GkMName, "GeV", "", 1);
  //RooBreitWigner *grPdf = new RooBreitWigner(name + "_GR", "Breit-Wigner lineshape", mgg, mG, GkM);
  //RooRelBreitWignerPwave_LS *grPdf = new RooRelBreitWignerPwave_LS(name + "_GR", "Breit-Wigner lineshape", mgg, mG, GkM);

  HggGravTLS *grPdf = new HggGravTLS(name + "_GR", "Graviton lineshape", mgg, mG, GkM);
  if (DatacardReader()) {
    RooArgSet* grObs = grPdf->getParameters(RooArgSet());
    DatacardReader()->ReadRealVars(*grObs);
    delete grObs;
  }


  // Resolution
  RooAbsPdf* resolPdf = m_2sCBBuilder.Pdf(name + "_2sCB", dependents, workspace);
  if (DatacardReader()) {
    RooArgSet* resolObs = resolPdf->getParameters(RooArgSet());
    DatacardReader()->ReadRealVars(*resolObs);
    delete resolObs;
  }
    
  // Convolution DSCB*GR
  // cout<< "Convolute with params" <<  endl;
  // cout<< "peak " << mG.getVal() << "; width: " << GkM.getVal()<<  endl;

  //mgg.setBins(10000, "cache");
  //RooNumConvPdf* pdf = new RooNumConvPdf(name, "GR #otimes DSCB", mgg, *grPdf, *resolPdf);
  //pdf->setConvolutionWindow(mG,GkM,5);

  RooFFTConvPdf* pdf = new RooFFTConvPdf(name, "GR #otimes DSCB", mgg, *grPdf, *resolPdf);
  mgg.setBins(50000, "cache");
  return pdf;
}
