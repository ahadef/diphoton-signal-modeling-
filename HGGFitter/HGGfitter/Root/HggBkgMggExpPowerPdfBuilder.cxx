// $Id: HggBkgMggExpPowerPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggExpPowerPdfBuilder.h"
#include "HGGfitter/HggExpPowerPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggExpPowerPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  RooAbsReal& xi    = Param(workspace, m_xiName,  "1/GeV");
  RooAbsReal& power = Param(workspace, m_powerName);
  RooAbsReal& coeff = Param(workspace, m_coeffName);

  return new Hfitter::HggExpPowerPdf(name, "Background PDF for mgg", mgg, xi, power, coeff);
}
