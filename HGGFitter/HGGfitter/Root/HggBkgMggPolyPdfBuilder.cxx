#include "HGGfitter/HggBkgMggPolyPdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooChebychev.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggPolyPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
 RooAbsReal& mgg = *Dependent(dependents, "mgg", "GeV", "m_{#gamma#gamma}");

 RooAbsReal& cs1  = Param(workspace, "cs1",  "");
 RooAbsReal& cs2  = Param(workspace, "cs2",  "");

 return new RooChebychev(name, "Background PDF for mgg using 2nd order Chebychev polynomials",
                                      mgg, RooArgList(cs1, cs2));

}
