// Author: Kerstin Tackmann

#include "HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h"

#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooNDKeysPdf.h"
#include "RooHistPdf.h"

#include "TChain.h"
#include "TH2F.h"
#include "TFile.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggCosThStarKeysPdfBuilder::Pdf(const TString& /*name*/, 
						 const RooArgList& dependents, 
						 RooWorkspace& /*workspace*/) const
{
  RooRealVar& mgg = (RooRealVar&)*dependents.find("mgg");
  RooRealVar& cosThStar = (RooRealVar&)*dependents.find("cosThStar");
  RooRealVar weight("weight","weight",0.,1000.);

  RooDataSet bkgDataSet("bkgDataSet","",RooArgSet(mgg,cosThStar,weight),"weight");

  //  TString keysInitInputFile = "test";

  if(m_fName == ""){
    std::cout << "Need to pass name of ntuple for building the 2d bkg keys Pdf or for reading histogram." << std::endl;
    assert(0);
  }else{
    if(m_fMode==0){
      std::cout << "Building the 2d bkg keys Pdf from " << m_fName << std::endl;
    }else{
      std::cout << "Reading the 2d histogram from " << m_fName << std::endl;
    }
  }

  TH2F* ph2;
  RooArgList argList(mgg,cosThStar);

  if(m_fMode==0){//Building the keys Pdf from a tree

    TChain* chain = new TChain("tree");
    chain->Add(m_fName);
    
    double dmgg, dcosThStar, dweight;
    chain->SetBranchAddress("mgg",&dmgg);
    chain->SetBranchAddress("cosThStar",&dcosThStar);
    chain->SetBranchAddress("weight",&dweight);
    
    int nentries = chain->GetEntries();
    //    nentries = 10;
    for(int i=0; i<nentries; i++){
      chain->GetEntry(i);
      mgg.setVal(dmgg);
      cosThStar.setVal(dcosThStar);
      weight.setVal(dweight);
      bkgDataSet.add(RooArgSet(mgg,cosThStar,weight),dweight);
    }
        
    RooNDKeysPdf* keysMggcosThStar = new RooNDKeysPdf("keysMggcosThStar","keysMggcosThStar", argList, bkgDataSet,"avm",1);
    
    Double_t lo[2]= {mgg.getMin(),cosThStar.getMin()};
    Double_t hi[2]= {mgg.getMax(),cosThStar.getMax(0)} ;
    Int_t nBins[2] = { 1000, 1000 };
    
    ph2 = mgg.createHistogram("ctsvsmgg",cosThStar,0,lo,hi,nBins); //"foo",&xlo,&xhi,&nBins);
    ph2->GetXaxis()->SetTitle("mgg");
    ph2->GetYaxis()->SetTitle("cosThStar");
    
    keysMggcosThStar->fillHistogram(ph2,RooArgList(mgg,cosThStar));

    TFile *f = new TFile("keys.root","RECREATE");
    ph2->Write();
    f->Close();

  }else{//reading a 2d histogram for better speed

    TFile *f = new TFile(m_fName,"READ");
    ph2 = (TH2F*)f->Get("ctsvsmgg__mgg_cosThStar");
  }

  RooDataHist *dataHist = new RooDataHist("hist2", "", argList, ph2);
  RooArgSet tmpSet(argList);
  RooHistPdf* histMggcosThStar = new RooHistPdf("histMggcosThStar","histMggcosThStar", tmpSet, *dataHist);
    
//   std::cout << "Generating events from the keys Pdf" << std::endl;
//   keysMggcosThStar->generate(tmpSet,100);
//   std::cout << "Done generating events from the keys Pdf" << std::endl;
  
//   std::cout << "Generating events from the histo Pdf" << std::endl;
//   histMggcosThStar->generate(tmpSet,100);
//   std::cout << "Done generating events from the histo Pdf" << std::endl;

//   assert(0);
  
  return histMggcosThStar;
  //  return keysMggcosThStar;
  
}
