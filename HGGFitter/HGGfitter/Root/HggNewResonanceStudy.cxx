#include "HGGfitter/HggNewResonanceStudy.h"

#include "RooPlot.h"
#include "TAxis.h"
#include "TString.h"
#include "TCanvas.h"
#include "TMath.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TH1D.h"
#include "TChain.h"
#include "RooDataSet.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftModelBuilderV4.h"
#include "HfitterModels/HftData.h"
#include "HfitterModels/HftDataHist.h"
#include "HfitterModels/Options.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "HfitterStats/HftScanLimitCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"
#include "HfitterStats/HftCLsbQTildaCalculator.h"
#include "HfitterStats/HftUncappedPValueCalculator.h"
#include "HfitterStats/HftScanStudy.h"
#include "TreeReader/MultiReader.h"
#include "TreeReader/TreeAbsReader.h"

#include <map>
#include <vector>
#include <algorithm>

using namespace Hfitter;
using namespace TreeReader;

#include <iostream>
using std::cout;
using std::endl;

void HggNewResonanceStudy::FitParameter(HftModel* hgg, const TString& varName, const std::vector<TString>& parNames, const TString& dataFileName, 
                                      const std::vector<double>& masses, const std::vector<double>& ranges, const TString& outputName, 
                                      const TString& treeName)
{
  std::vector<TGraphErrors*> graphs; 
  for (unsigned int k = 0; k < parNames.size(); k++) {
    TGraphErrors* g = new TGraphErrors(masses.size());
    g->SetName("fit_" + parNames[k]);
    graphs.push_back(g);
  }
  RooPlot* plot = hgg->Var(varName)->frame();
  plot->SetName("plot");
  HftData* data = HftData::Open(dataFileName, treeName, *hgg);
  data->DataSet()->plotOn(plot);
  
  int colors[2] = {2,4};
  TFile* fOut = TFile::Open(outputName, "RECREATE");
  
  for (unsigned int i = 0; i < masses.size(); i++) {

    cout << masses[i] << endl;
    TString rangeName = Form("range%d", i);
    hgg->Var(varName)->setRange(rangeName, masses[i] - ranges[i], masses[i] + ranges[i]);
    hgg->Var(varName)->setRange(masses[i] - ranges[i], masses[i] + ranges[i]);

    HftData* data = HftData::Open(dataFileName, treeName, *hgg);
    //hgg->ComponentModel("Background")->FitData(*data->DataSet(), "New");
    hgg->FitData(*data->DataSet(), "New");
    for (unsigned int k = 0; k < parNames.size(); k++) {
      graphs[k]->SetPoint(i, masses[i], hgg->Var(parNames[k])->getVal());
      graphs[k]->SetPointError(i, 0, hgg->Var(parNames[k])->getError());
    }
    //hgg->ComponentModel("Background")->Pdf()->plotOn(plot, RooFit::Range(rangeName), RooFit::Normalization(1, RooAbsReal::Relative), RooFit::LineColor(colors[i%2]));
    //RooPlot* pp = hgg->ComponentModel("Background")->Plot(varName, *data->DataSet());
    hgg->Pdf()->plotOn(plot, RooFit::Range(rangeName), RooFit::Normalization(1, RooAbsReal::Relative), RooFit::LineColor(colors[i%2]));
    RooPlot* pp = hgg->Plot(varName, *data->DataSet());
    pp->SetName(Form("fit%d", i));
    fOut->cd();
    pp->Write();
  }
  fOut->cd();
  for (unsigned int k = 0; k < parNames.size(); k++) graphs[k]->Write();
  plot->Write();
  delete fOut;
}


void HggNewResonanceStudy::FillHighMassExpPoints(std::vector<double>& masses, int range, int version)
{
  if (range == 0) {
    FillHighMassExpPoints(masses, 1, version);
    FillHighMassExpPoints(masses, 2, version);
    FillHighMassExpPoints(masses, 3, version);
  }
  else if (range == 1) {
    masses.push_back(version == 0 ? 110 : 113);
    masses.push_back(115);
    FillRange( 4, 120, 123, masses);
    FillRange(11, 124, 129, masses);
    FillRange(10, 130, 139, masses);
    FillRange( 6, 140, 190, masses);
  }
  else if (range == 2) {
    FillRange(10, 200, 380, masses);
  }  
  else if (range == 3) {
    FillRange(11, 400, 600, masses);
  }
  else if (range == 12) {
    FillRange(5, 200, 280, masses);
  }  
  else if (range == 13) {
    FillRange(5, 300, 380, masses);
  }
  else if (range == 14) {
    FillRange(5, 400, 480, masses);
  }  
  else if (range == 15) {
    FillRange(6, 500, 600, masses);
  }
  else if (range == 16) {
    FillRange(3, 650, 750, masses);
  }
  else if (range == 42) {
    masses.push_back(150);
    masses.push_back(300);
    masses.push_back(600);
  }
}


void HggNewResonanceStudy::FillHighMassObsPoints(std::vector<double>& masses, int range, int version)
{
  if (range == 0) {
    FillHighMassObsPoints(masses, 1, version);
    FillHighMassObsPoints(masses, 2, version);
    FillHighMassObsPoints(masses, 3, version);
  }
  else if (range == 1) {
    FillRange(180, 110, 199.5, masses);
  }
  else if (range == 2) {
    FillRange(200, 200, 399, masses);
  }
  else if (range == 3) {
    FillRange(101, 400, 600, masses);
  }
  else if (range == 12) {
    FillRange(200, 200, 299.5, masses);
  }
  else if (range == 13) {
    FillRange(200, 300, 399.5, masses);
  }
  else if (range == 14) {
    FillRange(200, 400, 499.5, masses);
  }
  else if (range == 15) {
    FillRange(201, 500, 600, masses);
  }
  else if (range == 16) {
    FillRange(180, 601, 780, masses);
  }
  else if (range == 42) {
    masses.push_back(150);
    masses.push_back(300);
    masses.push_back(600);
  }
}


void HggNewResonanceStudy::FillLowMassExpPoints(std::vector<double>& masses, int version)
{
  if (version == 0) {
    FillRange(2, 75, 80, masses);
    FillRange(3, 82, 86, masses);
    FillRange(7, 87, 93, masses);
    FillRange(3, 94, 98, masses);
    FillRange(3, 100, 110, masses);
  }
  else if (version == 1) {
    FillRange(7, 65, 80, masses);
    FillRange(5, 81, 85, masses);
    FillRange(9, 86, 90, masses);
    FillRange(4, 91, 94, masses);
    FillRange(7, 95, 110, masses);
  }
  else if (version == 2) {
    FillRange(21, 65,  85, masses);
    FillRange(9,  86,  90, masses);
    FillRange(20, 91, 110, masses);
  }
  else if (version == 3) {
    FillRange(21, 65,  85, masses);
    FillRange(9,  86,  90, masses);
    FillRange(23, 91, 113, masses);
  }
  else if (version == 42) {
    masses.push_back(75);
  }
  else {
    FillRange(21, 65,  85, masses);
    FillRange(13, 86,  92, masses);
    FillRange(18, 93, 110, masses);
  }
}


void HggNewResonanceStudy::FillLowMassObsPoints(std::vector<double>& masses, int version)
{
  if (version == 42) {
    masses.push_back(75);
  }
  else
    FillRange(91, 65,  110, masses);
}


double HggNewResonanceStudy::GetRange(double mass, HftModel* hgg, int widthHypo)
{
  if (widthHypo == 0) {
    hgg->Var("mX")->setVal(mass);
    //double ratio = 3.5;
    double ratio = 3.5*exp(0.00316*(mass - 110));
    return hgg->Real("cbSigmaSignal")->getVal()*ratio;
  }
  else if (widthHypo == 9) {
    hgg->Var("mX")->setVal(mass);
    double ratio = 3.5;
    if (mass > 200) ratio = 3.5 + (mass - 200)/100*3;
    return hgg->Real("cbSigmaSignal")->getVal()*ratio;
  }
  else if (widthHypo == 8) {
    hgg->Var("mX")->setVal(mass);
    double ratio = 1 + mass/100*2.5;
    return hgg->Real("cbSigmaSignal")->getVal()*ratio;
  }
  else if (widthHypo == 1) {
    double p0 = 2.1644;
    double p1 = 0.0271183;
    return 1.5*(p0 + p1*mass)*3;
  }
  else if (widthHypo == 3) {
    double p0 = 3.6406;
    double p1 = 0.0268491;
    return 1.5*(p0 + p1*mass);
  }
  else if (widthHypo == 5) {
    double p0 = 5.26425;
    double p1 = 0.0264206;
    return 2*(p0 + p1*mass);
  }
  else if (widthHypo == 33) {
    hgg->Var("mX")->setVal(mass);
    double ratio = 0.590+0.0265*mass;
    return hgg->Real("cbSigmaSignal")->getVal()*ratio;
  }
  else if (widthHypo == 44) return (78.74*(TMath::Min(mass,600.)-110)/110 + 20)/2;
  else if (widthHypo == 55) {
    if (mass > 500) return (80*(mass-110)/110 - 130);
    return (78.74*(mass-110)/110 + 20)/2;
  }
  cout << "ERROR: wrong width hypo " << widthHypo << endl;
  return 0;
}


double HggNewResonanceStudy::GetAsimovNBkg(double mass, int widthHypo)
{
  double u = (mass-100)/100;
  
  if (widthHypo == 0) {
    double n0 = 9.21009;
    double n1 = 5.17521e-03;
    double n2 = 5.01659;
//     double n0 = 1.13533e+01;
//     double n1 = 4.12676e-03;
//     double n2 = 3.01812e+00;
    return exp(n0*exp(-n1*mass)+ n2);
  }
  else if (widthHypo == 9) {
    double n0 = 9.86222e+00;
    double n1 = 6.41386e-03;
    double n2 = 5.36171e+00;
    return exp(n0*exp(-n1*mass)+ n2);
  }
  else if (widthHypo == 8) {
    double n0 = 8.79553e+00;
    double n1 = 4.12135e-03;
    double n2 = 4.70635e+00;
    return exp(n0*exp(-n1*mass)+ n2);
  }
  else if (widthHypo == 33) {
    double n0 = 8.60363e+00;
    double n1 = 4.02536e-03;
    double n2 = 4.70105e+00;
    return exp(n0*exp(-n1*mass)+ n2);
  }
  else if (widthHypo == 44) {
    double p0 = 1.10588e+01;
    double p1 = -1.41573e+00;
    double p2 = 8.37990e-02;
    return exp(p0+u*(p1 + u*p2));
  }
  else if (widthHypo == 55) {
    double p0 = 1.10588e+01;
    double p1 = -1.41573e+00;
    double p2 = 8.37990e-02;
  
    return (mass < 500 ? 1 : 1/exp(mass/500))*exp(p0+u*(p1 + u*p2));
  }
  else if (widthHypo == 1) {
    double n0 = 10.8566;
    double n1 = -3.24425;
    double n2 = 0.701339;
    double n3 = -0.0780792;
    double n4 = 0.00160697;
    return exp(n0 + u*(n1 + u*(n2 + u*(n3 + u*n4))));
  }
  else if (widthHypo == 3) {
    double n0 = 11.1969;
    double n1 = -3.72303;
    double n2 = 1.18108;
    double n3 = -0.266938;
    double n4 = 0.0244601;
    return exp(n0 + u*(n1 + u*(n2 + u*(n3 + u*n4))));
  }
  else if (widthHypo == 5) {
    double n0 = 11.7432;
    double n1 = -3.68297;
    double n2 = 0.978109;
    double n3 = -0.165699;
    double n4 = 0.0112879;
    return exp(n0 + u*(n1 + u*(n2 + u*(n3 + u*n4))));
  }
  cout << "ERROR: wrong width hypo " << widthHypo << endl;
  return 0;
}


bool HggNewResonanceStudy::GetAsimovParamsW0(double mX, double& slope)
{
  double p0 = -4.54602e-02;
  double p1 =  3.58365e-02;
  double p2 =  1.01809;
//   double p0 = -4.76206e-02;
//   double p1 =  3.41562e-02;
//   double p2 =  1.45282e+00;
  slope = p0 + p1*(1 - exp(-(mX - 100)/100*p2));

  return true;
}


bool HggNewResonanceStudy::GetAsimovParamsW1(double mX, double& a1, double& a2)
{
  double u = (mX-100)/100;

  double p0 = -3.83566;
  double p1 = -0.0874555;
  double p2 = 0.437484;
  double p3 = -0.0536119;
  a1 = p0 + u*(p1 + u*(p2 + u*p3));

  double q0 = 0.954184;
  double q1 = -0.456915;
  double q2 = 0.059308;
  double q3 = -0.00193461;
  a2 = q0 + u*(q1 + u*(q2 + u*q3));

  return true;
}


bool HggNewResonanceStudy::GetAsimovParamsW3(double mX, double& a1, double& a2)
{
  double u = (mX-100)/100;

  double p0 =  -4.09986;
  double p1 =  0.84186;
  double p2 =  -0.255234;
  double p3 =  0.0904533;
  double p4 =  -0.00870068;
  a1 = p0 + u*(p1 + u*(p2 + u*(p3 + u*p4)));

  double q0 = 1.0331;
  double q1 = -0.744378;
  double q2 = 0.292369;
  double q3 = -0.0616062;
  double q4 = 0.00483517;
  a2 = q0 + u*(q1 + u*(q2 + u*(q3 + u*q4)));

  return true;
}

bool HggNewResonanceStudy::GetAsimovParamsW5(double mX, double& c1, double& c2, double& c3, double& c4)
{
  double u = (mX-100)/100;

  double p0 = 0.656761;
  double p1 = 0.0808199;
  double p2 = -0.0122826;
  double p3 = 0.000321398;
  c1 = p0 + u*(p1 + u*(p2 + u*p3));

  double q0 = 0.484977;
  double q1 = 0.0684939;
  double q2 = 0.0178263;
  double q3 = -0.0120315;
  double q4 = 0.00145036;
  c2 = q0 + u*(q1 + u*(q2 + u*(q3 + u*q4)));

  double r0 = 0.35677;
  double r1 = 0.109996;
  double r2 = -0.016787;
  double r3 = 0.0004494;
  c3 = r0 + u*(r1 + u*(r2 + u*r3));

  double s0 = 0.269396;
  double s1 = 0.116829;
  double s2 = -0.0192674;
  double s3 = -0.00027721;
  double s4 = 0.000203841;
  c4 = s0 + u*(s1 + u*(s2 + u*(s3 + u*s4)));

  return true;
}


double HggNewResonanceStudy::GetNormalization(RooAbsPdf& pdf, RooRealVar& dependent, const TString& range)
{
  RooAbsReal* integral = pdf.createIntegral(dependent, dependent, range);
  double val = integral->getVal();
  delete integral;
  return val;
}


void HggNewResonanceStudy::GetRanges(const std::vector<double>& masses, HftModel* hgg, std::vector<double>& ranges, int widthHypo)
{
  for (unsigned int i = 0; i < masses.size(); i++) {
    ranges.push_back(GetRange(masses[i], hgg, widthHypo));
  }
}


void HggNewResonanceStudy::PrepareModel(HftModel* hgg, double mass, int widthHypo)
{
  if (hgg->Var("xs")) hgg->Var("xs")->setVal(0);
  hgg->Var("mX")->setVal(mass);
  double nBkg = GetAsimovNBkg(mass, widthHypo);
  hgg->Var("nBackground")->setVal(nBkg*(hgg->Var("normLumi") ? hgg->Var("normLumi")->getVal()/20.3 : 1));
  if (widthHypo == 0) {
    double slope;
    GetAsimovParamsW0(mass, slope);
    hgg->Var("xi")->setVal(slope);
    hgg->Var("xi")->setRange(slope - 0.01, -0.005);
  }
  if (widthHypo == 9) {
    double p0 = -4.53169e-02;
    double p1 =  3.48382e-02;
    double p2 =  9.83971e-01;
    double slope = p0 + p1*(1 - exp(-(mass - 100)/100*p2));
    hgg->Var("xi")->setVal(slope);
    hgg->Var("xi")->setRange(slope - 0.01, -0.005);
  }
  if (widthHypo == 8) {
    double p0 = -4.35525e-02;
    double p1 =  3.39371e-02;
    double p2 =  9.55323e-01;
    double slope = p0 + p1*(1 - exp(-(mass - 100)/100*p2));
    hgg->Var("xi")->setVal(slope);
    hgg->Var("xi")->setRange(slope - 0.01, -0.005);
  }
  if (widthHypo == 33) {
    double p0 = -4.52960e-02;
    double p1 =  3.58068e-02;
    double p2 =  1.02338e+00;
    double slope = p0 + p1*(1 - exp(-(mass - 100)/100*p2));
    hgg->Var("xi")->setVal(slope);
    hgg->Var("xi")->setRange(slope - 0.01, -0.005);
  }
  if (widthHypo == 44 || widthHypo == 55) {
     double p0 = -3.93102e+00;
     double p1 = -1.36765e-01;
     double p2 = 6.57817e-01;
     double p3 = -1.81301e-01;
     double p4 = 1.47651e-02;

     double q0 = 1.12973e+00;
     double q1 = -6.94250e-01;
     double q2 = 1.69694e-01;
     double q3 = -1.76746e-02;
     double q4 = 6.02452e-04;

     double u = (mass - 100)/100;
     if (mass > 600) u = 5; // saturate at 600
     double a1 = p0 + u*(p1 + u*(p2 + u*(p3 + u*p4)));
     double a2 = q0 + u*(q1 + u*(q2 + u*(q3 + u*q4)));
     if (mass > 600) {
       double uu = (mass - 100)/100; 
       a1 *= u/uu;
       a2 *= u*u/(uu*uu);
     }
     hgg->Var("a1")->setVal(a1);
     hgg->Var("a2")->setVal(a2);
  }
  if (widthHypo == 1) {
    double a1, a2;
    GetAsimovParamsW1(mass, a1, a2);
    hgg->Var("a1")->setVal(a1);
    hgg->Var("a2")->setVal(a2);
  }
  if (widthHypo == 3) {
    double a1, a2;
    GetAsimovParamsW3(mass, a1, a2);
    hgg->Var("a1")->setVal(a1);
    hgg->Var("a2")->setVal(a2);
  }
  if (widthHypo == 5) {
    double c1, c2, c3, c4;
    GetAsimovParamsW5(mass, c1, c2, c3, c4);
    hgg->Var("c1")->setVal(c1);
    hgg->Var("c2")->setVal(c2);
    hgg->Var("c3")->setVal(c3);
    hgg->Var("c4")->setVal(c4);
  }
  RooRealVar* mgg = hgg->Var("mgg");
  double range = HggNewResonanceStudy::GetRange(mass, hgg, widthHypo);
  //range *= 2;
  double mMin = TMath::Min(mass, 600.) - range;
  double mMax = TMath::Min(mass, 600.) + range;
  //if (mass >= 340 && mass <= 350) { mMin = 315; mMax = 375; }
  bool rescale = false;
  if (mMin < mgg->getMin()) {
    cout << "WARNING: high-mass range low edge should be " << mMin << ", lower than high-mass minimum " << mgg->getMin() << endl;
    mMin = mgg->getMin();
    rescale = true;
  }
  if (mMax > mgg->getMax()) {
    cout << "WARNING: high-mass range high edge should be " << mMax << ", higher than high-mass maximum " << mgg->getMax() << endl;
    mMax = mgg->getMax();
    rescale = true;
  }
  mgg->setRange("fitRange", mMin, mMax);
  if (rescale) {
    RooAbsPdf* pdf = hgg->ComponentModel("Background")->Pdf();
    mgg->setRange("origRange", mass - range, mass + range);
    double fitNorm = GetNormalization(*pdf, *mgg, "fitRange");
    double origNorm = GetNormalization(*pdf, *mgg, "origRange");
    cout << "INFO : due to range limit, applying rescaling on nBackground with factor " << fitNorm/origNorm << endl;
    hgg->Var("nBackground")->setVal(hgg->Var("nBackground")->getVal()*fitNorm/origNorm);
  }
  // Disable Higgs if needed, to avoid PDF==0 issues
  if (hgg->Real("cbPeakHiggs")) {
    double zHiggs = (mass - hgg->Real("cbPeakHiggs")->getVal())/hgg->Real("cbSigmaHiggs")->getVal();
    if (zHiggs > 10 || zHiggs < -10) {
      hgg->Var("cbSigmaHiggsBlowup")->setVal(100);
      hgg->Var("mu")->setVal(0);
    }
    else {
      RooAbsPdf* hPdf = hgg->Workspace()->pdf("Higgs");
      hgg->Var("fracInRangeHiggs")->setVal(GetNormalization(*hPdf, *mgg, "fitRange"));
    }
  }
  double binSize = mgg->getBinWidth(0);
  mgg->setRange(mMin, mMax);
  mgg->setBins((mMax - mMin)/binSize);
}


HftScanStudy* HggNewResonanceStudy::MakeHighMassLimitStudy(const std::vector<double>& massesObs, const std::vector<double>& massesExp, 
                                                           const TString& datacard, const TString& dataFileName,
                                                           const TString& output, const TString& fitOptions, 
                                                           int widthHypo, bool binned, bool smallTree, bool asimovFromData, bool fillSysts)
{
  std::vector<HftModel*> models;
  std::vector<const HftAbsCalculator*> observed, expected;
  std::vector<RooAbsData*> data;

  unsigned int iExp = 0;
  
  for (unsigned int i = 0; i < massesObs.size(); i++) {
    cout << "Processing point " << i << " of " << massesObs.size() << endl;
    HftModel* hgg = HftModelBuilder::Create("hgg", datacard);
    if (!hgg) hgg = HftModelBuilderV4::Create("hgg", datacard);
    models.push_back(hgg);
    PrepareModel(hgg, massesObs[i], widthHypo);

    HftData* dat = HftData::Open(dataFileName, "tree", *models[i]);
    if (!dat) {
      HftDataHist* binDat = HftDataHist::Open(dataFileName, "mass", *models[i]);
      data.push_back((RooAbsData*)binDat->DataHist());
    }
    else
      data.push_back(binned ? (RooAbsData*)dat->DataSet()->binnedClone() : (RooAbsData*)dat->DataSet());

    if (asimovFromData) { // hack for high-mass tests
//       hgg->Var("xs")->setConstant();
//       hgg->Parameters().selectByName("dMC*")->setAttribAll("Constant", 1);
//       hgg->Parameters().Print("V");
//       hgg->FitData(*data[i], fitOptions);
//       hgg->Var("xs")->setConstant(0);
//       hgg->Parameters().selectByName("dMC*")->setAttribAll("Constant", 0);
//       cout << "== Values from Asimov Fit at mX = " << massesObs[i] << endl;
//       hgg->FreeParameters().Print("V");
      HftModel* hgg_simple = HftModelBuilder::Create("hgg_simple", "../datacards/LowHighMassRun2/hfitter_newResRun2_highMass_simple.dat");
      PrepareModel(hgg_simple, massesObs[i], widthHypo);
      hgg_simple->Var("nSignal")->setConstant();
      HftDataHist* binDat = HftDataHist::Open(dataFileName, "mass", *hgg_simple);
      hgg_simple->FitData(*binDat->DataHist(), fitOptions);
      delete binDat;
      cout << "== Values from Asimov Fit at mX = " << massesObs[i] << endl;
      RooArgList freePars = hgg_simple->FreeParameters();
      freePars.Print("V");
      for (int k = 0; k < freePars.getSize(); k++) {
        hgg->Var(freePars.at(k)->GetName())->setVal(((RooRealVar*)freePars.at(k))->getVal());
      }
    }
    if (fillSysts) {
      std::vector<TString> pars;
      pars.push_back("nBackground");
      pars.push_back("a1");
      pars.push_back("a2");
      for (unsigned int k = 0; k < 22; k++) {
        if (!hgg->Var(Form("dMC%d", k))) break;
        TFile* sysFile = TFile::Open(Form("BkgModel/Jan/mcSyst_%d.root", k));
        for (unsigned int j = 0; j < pars.size(); j++) {
          if (!hgg->Var("mc_" + pars[j])) continue;
          TString deltaName = Form("delta%d_%s", k, pars[j].Data());
          TGraph* syst = (TGraph*)sysFile->Get("systUD_" + pars[j]);
          TGraph* statErr = (TGraph*)sysFile->Get("statErr_" + pars[j]);
          TGraphAsymmErrors* val = (TGraphAsymmErrors*)sysFile->Get(pars[j] + "_nom");
          hgg->Var(deltaName)->setVal(HftBand::Interpolate(*syst, massesObs[i]));
          hgg->Var("mc_" + pars[j])->setVal(hgg->Var(pars[j])->getVal());
          double sigma = HftBand::Interpolate(*statErr, massesObs[i]);
          double value = HftBand(*val).Interpolate(massesObs[i]);
          cout << "** " << sigma/value*hgg->Var(pars[j])->getVal() << " " << sigma << " " << value << " " << hgg->Var(pars[j])->getVal() << endl;
          hgg->Var("sigma_" + pars[j])->setVal(sigma/value*hgg->Var(pars[j])->getVal());
          cout << "== Values set from syst files: " << endl;
          hgg->Parameters().selectByName("*_" + pars[j])->Print("V");
          //hgg->Var("a1MC")->setVal(HftBand(*a1_nom).Interpolate(massesObs[i])); // get from Asimov instead
        }
      }
    }
    HftParameterStorage state = hgg->CurrentState();
    HftParameterStorage asimovState = state;
    if (!asimovFromData) SetHighMassExpectedNPs(asimovState);
    state.Print();
    // Bkg in 1.4 sigma window
    double peakBkg = hgg->Var("nBackground")->getVal()/GetRange(massesObs[i], hgg, widthHypo)*hgg->Real("cbSigmaSignal")->getVal()*1.4;
    
    //if (massesObs[i] > 500) peakBkg /= exp(massesObs[i]/1000);
    
    double approxLimit = 1.64*TMath::Sqrt(peakBkg)/hgg->Var("normLumi")->getVal()/hgg->Real("CX")->getVal();
    // approxLimit = 1.64*sqrt(hgg->Var("nBackground")->getVal()/3.58*1.4)/hgg->Var("normLumi")->getVal()/hgg->Real("CX")->getVal(); 
    cout << "Using approxLimit = " << approxLimit << endl;
    std::vector<double> pos;
    //if (approxLimit/5 > 0.1) pos.push_back(0.1);

    if (asimovFromData) {
      pos.push_back(approxLimit/20);
      FillRange(10, approxLimit/10, approxLimit, pos);
      FillRange(4, approxLimit*1.2, approxLimit*1.8, pos);
      FillRange(10, approxLimit, approxLimit*10, pos);
      pos.push_back(approxLimit*20);
    }
    else {
      FillRange( 5, approxLimit/5, approxLimit, pos);
      FillRange(20, 1.10*approxLimit,  3*approxLimit, pos);
      FillRange( 4, 3.25*approxLimit,  4*approxLimit, pos);
      FillRange(18,  4.5*approxLimit,  13*approxLimit, pos);
      FillRange( 6,   14*approxLimit,  19*approxLimit, pos);
      pos.push_back(20*approxLimit);
    }
    HftScanLimitCalculator* obs = new HftScanLimitCalculator( HftCLsCalculator(
      HftCLsbQTildaCalculator(*hgg, state, "xs", fitOptions/*, "" , false, smallTree*/)), "xs", pos);  // need to reimplement smallTree
    observed.push_back(obs);

    if (massesExp.size() > iExp && massesObs[i] == massesExp[iExp]) {
      cout << "Creating expected calculator for mass " << massesExp[iExp] << endl;
      HftAsimovCalculator* exp = new HftAsimovCalculator(
        HftScanLimitCalculator( HftCLsCalculator(
          HftCLsbQTildaCalculator(*hgg, asimovState, "xs", fitOptions, "" , false, smallTree)
        ), "xs", pos),
        asimovState);
      expected.push_back(exp);
      iExp++;
    }
  }

  HftScanStudy* limit = new HftScanStudy(HftScanningCalculator(observed, *models[0]->Var("mX"), massesObs),
                                         HftScanningCalculator(expected, *models[0]->Var("mX"), massesExp),
                                         output, data);
  
  cout << "dEBUG" << endl;
  limit->Observed()->Calculator(8)->Model().Parameters().Print("V");
  return limit;
}


HftScanStudy* HggNewResonanceStudy::MakeHighMassP0Study(const std::vector<double>& massesObs, const std::vector<double>& massesExp, const TString& datacard, const TString& dataFileName,
                                                        const TString& output, const TString& fitOptions, int widthHypo, bool binned, const TString& weightVar)
{
  std::vector<HftModel*> models;
  std::vector<const HftAbsCalculator*> observed, expected;
  std::vector<RooAbsData*> data;

  unsigned int iExp = 0;
  
  for (unsigned int i = 0; i < massesObs.size(); i++) {
    cout << "Processing point " << i << " of " << massesObs.size() << endl;
    HftModel* hgg = HftModelBuilder::Create("hgg", datacard);
    if (!hgg) hgg = HftModelBuilderV4::Create("hgg", datacard);
    PrepareModel(hgg, massesObs[i], widthHypo);

    HftParameterStorage asimovState;
    hgg->SaveState(asimovState);
    SetHighMassExpectedNPs(asimovState);
    asimovState.Save("xs", 10, -1, 0); // 10 fb
    
    HftUncappedPValueCalculator* obs = new HftUncappedPValueCalculator(*hgg, "xs", fitOptions);
    models.push_back(hgg);
    observed.push_back(obs);

    HftData* dat = HftData::Open(dataFileName, "tree", *models[i], "", weightVar);
    data.push_back(binned ? (RooAbsData*)dat->DataSet()->binnedClone() : (RooAbsData*)dat->DataSet());

    if (massesObs[i] == massesExp[iExp]) {
      cout << "Creating expected calculator for mass " << massesExp[iExp] << endl;
      HftAsimovCalculator* exp = new HftAsimovCalculator(
        HftUncappedPValueCalculator(*hgg, "xs", fitOptions), asimovState);
      expected.push_back(exp);
      iExp++;
    }
  }

  HftScanStudy* p0 = new HftScanStudy(HftScanningCalculator(observed, *models[0]->Var("mX"), massesObs),
                                      HftScanningCalculator(expected, *models[0]->Var("mX"), massesExp),
                                      output, data);
  return p0;
}


bool HggNewResonanceStudy::SetHighMassExpectedNPs(HftParameterStorage& state)
{
  state.Save("dSig",        -0.78364, 1, 0); state.Save("dSigAux",      state.RealValue("dSig"),      -1, 1); 
  state.Save("dEScale",      0.57319, 1, 0); state.Save("dEScaleAux",   state.RealValue("dEScale"),   -1, 1);
  state.Save("dMatLow",     0.665211, 1, 0); state.Save("dMatLowAux",   state.RealValue("dMatLow"),   -1, 1);
  state.Save("dMatHigh",    0.101365, 1, 0); state.Save("dMatHighAux",  state.RealValue("dMatHigh"),  -1, 1);
  state.Save("dPSBarrel",   0.167587, 1, 0); state.Save("dPSBarrelAux", state.RealValue("dPSBarrel"), -1, 1);
  state.Save("dPSEndcap",  0.0166154, 1, 0); state.Save("dPSEndcapAux", state.RealValue("dPSEndcap"), -1, 1);
  state.Save("dExtraES",    0.529921, 1, 0); state.Save("dExtraESAux",  state.RealValue("dExtraES"),  -1, 1);
  state.Save("dLumi",      0.0895092, 1, 0); state.Save("dLumiAux",     state.RealValue("dLumi"),     -1, 1);
  state.Save("dIsol",      0.0319328, 1, 0); state.Save("dIsolAux",     state.RealValue("dIsol"),     -1, 1);
  state.Save("dEff",       0.0782978, 1, 0); state.Save("dEffAux",      state.RealValue("dEff"),      -1, 1);
  state.Save("dGGFTh",      0.229192, 1, 0); state.Save("dGGFThAux",    state.RealValue("dGGFTh"),    -1, 1);
  state.Save("dGGPdf",      0.206841, 1, 0); state.Save("dGGPdfAux",    state.RealValue("dGGPdf"),    -1, 1);
  state.Save("dTTHTh",    0.00806052, 1, 0); state.Save("dTTHThAux",    state.RealValue("dTTHTh"),    -1, 1);
  state.Save("dVBFTh",   0.000435887, 1, 0); state.Save("dVBFThAux",    state.RealValue("dVBFTh"),    -1, 1);
  state.Save("dQQPdf",    0.00736642, 1, 0); state.Save("dQQPdfAux",    state.RealValue("dQQPdf"),    -1, 1);
  state.Save("dVHTh",    0.000938606, 1, 0); state.Save("dVHThAux",     state.RealValue("dVHTh"),     -1, 1);
  state.Save("dBR",         0.153929, 1, 0); state.Save("dBRAux",       state.RealValue("dBR"),       -1, 1);
  return true;
}


void HggNewResonanceStudy::MakeSmallDataFileFromPresel(const TString& inputName, const TString& outputName, bool lowMass)
{
  MultiReader input;
  BranchReader<float>* mggI = input.AddFloat("mgg");
  BasicReader<bool>* isPA = input.AddBool("isPassing");
  BasicReader<bool>* isPR = input.AddBool("isPassingRelative");
  BranchReader<int>* cf1 = input.AddInt("ph1_convFlag");
  BranchReader<int>* cf2 = input.AddInt("ph2_convFlag");

  MultiReader output;
  BranchReader<double>* mggO = output.AddDouble("mgg");
  BranchReader<int>* catConvO = output.AddInt("catConv_idx");
  BasicReader<bool>* isPAO = output.AddBool("isPassing");
  BasicReader<bool>* isPRO = output.AddBool("isPassingRelative");

  cout << "Processing file " << inputName << endl;  
  TFile* inputFile = TFile::Open(inputName);
  TTree* inputTree = (TTree*)inputFile->Get("tree");
  input.ReadFrom(inputTree);
  
  TFile* outputFile = TFile::Open(outputName, "RECREATE");
  TTree* outputTree = new TTree("tree", "");
  output.WriteTo(outputTree);

  for (unsigned int k = 0; k < inputTree->GetEntries(); k++) {
    if (k % 10000 == 0) cout << "Processing entry " << k << " of " << inputTree->GetEntries() << endl;
    inputTree->GetEntry(k);
    if (lowMass) {
      if (!isPA->GetValue()) continue;
      if (mggI->GetValue() < 60 || mggI->GetValue() > 120) continue;
    }
    else {
//       if (!isPR->GetValue() && !isPA->GetValue()) continue;
      if (!isPR->GetValue()) continue;
      if (mggI->GetValue() < 100) continue;
    }
    mggO->SetValue(mggI->GetValue());
    catConvO->SetValue((cf1->GetValue() % 10 > 0) + (cf2->GetValue() % 10 > 0));
    isPAO->SetValue(isPA->GetValue());
    isPRO->SetValue(isPR->GetValue());
    outputTree->Fill();
  }
  outputFile->cd();
  outputTree->Write();
  delete outputFile;
}


void HggNewResonanceStudy::MakeSmallDataFileFromNTUPxAOD(const TString& inputPath, const TString& outputName, bool lowMass)
{
  MultiReader input;
  BranchReader<float>* mggI = input.AddFloat("mass");

  MultiReader output;
  BranchReader<double>* mggO = output.AddDouble("mgg");
  BranchReader<int>* catConvO = output.AddInt("catConv_idx");
  BasicReader<bool>* isPAO = output.AddBool("isPassing");
  BasicReader<bool>* isPRO = output.AddBool("isPassingRelative");

  cout << "Processing path " << inputPath << endl;
  TChain* inputTree = new TChain("CollectionTree");
  TreeAbsReader::Add(*inputTree, inputPath);
  input.ReadFrom(inputTree);
  
  TFile* outputFile = TFile::Open(outputName, "RECREATE");
  TTree* outputTree = new TTree("tree", "");
  output.WriteTo(outputTree);

  for (unsigned int k = 0; k < inputTree->GetEntries(); k++) {
    if (k % 10000 == 0) cout << "Processing entry " << k << " of " << inputTree->GetEntries() << endl;
    inputTree->GetEntry(k);
    if (lowMass) {
      cout << "ERROR : low-mass case not implemented yet" << endl;
      break;
    }
    else {
//       if (!isPR->GetValue() && !isPA->GetValue()) continue;
      //if (!isPR->GetValue()) continue;
      if (mggI->GetValue() < 100) continue;
    }
    mggO->SetValue(mggI->GetValue());
    //catConvO->SetValue((cf1->GetValue() % 10 > 0) + (cf2->GetValue() % 10 > 0));
    //isPAO->SetValue(isPA->GetValue());
    //isPRO->SetValue(isPR->GetValue());
    catConvO->SetValue(0);
    isPAO->SetValue(1);
    isPRO->SetValue(1);
    outputTree->Fill();
  }
  outputFile->cd();
  outputTree->Write();
  delete outputFile;
}


HftScanStudy* HggNewResonanceStudy::MakeLowMassLimitStudy(const std::vector<double>& massesObs, const std::vector<double>& massesExp, 
                                                          const TString& datacard, const TString& dataFileName,
                                                          const TString& output, const TString& fitOptions, bool binned, bool /*smallTree*/) // need to reinstate smallTree
{
  HftModel* hgg = HftModelBuilder::Create("hgg", datacard);
  hgg->Var("xs")->setVal(0);
  HftParameterStorage state;
  hgg->SaveState(state);

  HftParameterStorage asimovState = state;
  SetHighMassExpectedNPs(asimovState);

  double approxLimit = 1;
  if (hgg->Real("cbSigmaSignal") && !hgg->Real("nBackground_unconv")) {
    approxLimit = 1.64*sqrt(hgg->Var("nBackground")->getVal()*hgg->Real("cbSigmaSignal")->getVal()*2/(120-70))/hgg->Var("normLumi")->getVal()/hgg->Real("CX")->getVal();
  }
  else if (hgg->Real("cbSigmaSignal_unconv")) {
    approxLimit = 1.64*sqrt(hgg->Var("nBackground_unconv")->getVal()*hgg->Real("cbSigmaSignal_unconv")->getVal()*2/(120-70))/hgg->Var("normLumi")->getVal()/hgg->Real("CX")->getVal();
  }
//   else if (hgg->Real("cbSigmaSignal_oneConv")) {
//     approxLimit = 1.64*sqrt(hgg->Var("nBackground_oneConv")->getVal()*hgg->Real("cbSigmaSignal_oneConv")->getVal()*2/(120-70))/hgg->Var("normLumi")->getVal()/hgg->Real("CX")->getVal();
//   }
  else
    approxLimit = 1.64*sqrt(hgg->Var("nBackground_unconv")->getVal()*hgg->Real("cbSigmaSignal")->getVal()*2/(120-70))/hgg->Var("normLumi")->getVal()/hgg->Real("CX")->getVal();

  cout << "Using approxLimit = " << approxLimit << endl;
  approxLimit *= 2; // temp
  std::vector<double> pos;
  //pos.push_back(approxLimit/20);
  //pos.push_back(approxLimit/10);
  FillRange( 5, approxLimit/5, approxLimit, pos);
  FillRange(12, 1.25*approxLimit,  4*approxLimit, pos);
  FillRange( 6,   5*approxLimit,  10*approxLimit, pos);
  pos.push_back(20*approxLimit);
  
  HftScanLimitCalculator* obs = new HftScanLimitCalculator( HftCLsCalculator(
    HftCLsbQTildaCalculator(*hgg, state, "xs", fitOptions/*, "", false, smallTree*/)), "xs", pos);  // need to reimplement smallTree
  HftAsimovCalculator* exp = new HftAsimovCalculator( HftScanLimitCalculator( HftCLsCalculator(
    HftCLsbQTildaCalculator(*hgg, asimovState, "xs", fitOptions/*, "", false, smallTree)*/)), "xs", pos), asimovState);  // need to reimplement smallTree
  std::vector<RooAbsData*> data;
  HftData* dat = HftData::Open(dataFileName, "tree", *hgg);
  RooAbsData* ds = (binned ? (RooAbsData*)dat->DataSet()->binnedClone() : (RooAbsData*)dat->DataSet());
  for (unsigned int i = 0; i < massesObs.size(); i++) 
    data.push_back(ds);
  
  HftScanStudy* limit = new HftScanStudy(HftScanningCalculator(*obs, "mX", massesObs), 
                                         HftScanningCalculator(*exp, "mX", massesExp), output, data);
  return limit;
}


HftScanStudy* HggNewResonanceStudy::MakeLowMassP0Study(const std::vector<double>& massesObs, const std::vector<double>& massesExp, const TString& datacard, const TString& dataFileName,
                                                       const TString& output, const TString& fitOptions, bool binned)
{
  HftModel* hgg = HftModelBuilder::Create("hgg", datacard);
  HftParameterStorage asimovState;
  hgg->SaveState(asimovState);
  SetLowMassExpectedNPs(asimovState);
  asimovState.Save("xs", 10, -1, 0); // 10 fb

  std::vector<RooAbsData*> data;
  HftData* dat = HftData::Open(dataFileName, "tree", *hgg);
  RooAbsData* ds = (binned ? (RooAbsData*)dat->DataSet()->binnedClone() : (RooAbsData*)dat->DataSet());
  for (unsigned int i = 0; i < massesObs.size(); i++) 
    data.push_back(ds);

  HftUncappedPValueCalculator* obs = new HftUncappedPValueCalculator(*hgg, "xs", fitOptions);
  HftAsimovCalculator* exp = new HftAsimovCalculator(
        HftUncappedPValueCalculator(*hgg, "xs", fitOptions), asimovState);

  HftScanStudy* p0 = new HftScanStudy(HftScanningCalculator(*obs, "mX", massesObs), 
                                      HftScanningCalculator(*exp, "mX", massesExp),
                                      output, data);
  return p0;
}


bool HggNewResonanceStudy:: SetLowMassExpectedNPs(HftParameterStorage& state)
{
  state.Save("dNormStatUU", -0.231245,   1, 0); state.Save("dNormStatUUAux", state.RealValue("dNormStatUU"), -1, 1); 
  state.Save("dNormStatUC", -0.0888173,  1, 0); state.Save("dNormStatUCAux", state.RealValue("dNormStatUC"), -1, 1); 
  state.Save("dNormStatCU", -0.0554651,  1, 0); state.Save("dNormStatCUAux", state.RealValue("dNormStatCU"), -1, 1); 
  state.Save("dNormStatCC",  0.859074,   1, 0); state.Save("dNormStatCCAux", state.RealValue("dNormStatCC"), -1, 1); 
  state.Save("dNormSystDY",  0.952531,   1, 0); state.Save("dNormSystDYAux", state.RealValue("dNormSystDY"), -1, 1); 

  state.Save("dShape1DY",   -0.743017,   1, 0); state.Save("dShape1DYAux",   state.RealValue("dShape1DY"),   -1, 1); 
  state.Save("dShape2DY",   -0.212207,   1, 0); state.Save("dShape2DYAux",   state.RealValue("dShape2DY"),   -1, 1); 

  state.Save("dPeakStatUU", -0.00239364, 1, 0); state.Save("dPeakStatUUAux", state.RealValue("dPeakStatUU"), -1, 1); 
  state.Save("dPeakStatUC",  0.015028,   1, 0); state.Save("dPeakStatUCAux", state.RealValue("dPeakStatUC"), -1, 1); 
  state.Save("dPeakStatCU",  0.0161327,  1, 0); state.Save("dPeakStatCUAux", state.RealValue("dPeakStatCU"), -1, 1); 
  state.Save("dPeakStatCC", -0.00589155, 1, 0); state.Save("dPeakStatCCAux", state.RealValue("dPeakStatCC"), -1, 1); 
  state.Save("dPeakSystDY",  1.65572,    1, 0); state.Save("dPeakSystDYAux", state.RealValue("dPeakSystDY"), -1, 1); 

  return true;
}


unsigned int HggNewResonanceStudy::MergePlots(const TString& highMassName, const TString& lowMassName, unsigned int nRegions)
{
  TString names[4] = { "obs0", "exp0", "exp1", "exp2" };
  
  TFile* f[99];
  TString fileNames[99];
  unsigned int nF = 0;
  if (lowMassName != "")
    fileNames[nF++] = "Results/graphs_" + lowMassName + ".root";
  for (unsigned int i = 0; i < nRegions; i++)
    fileNames[nF++] = "Results/graphs_" + highMassName + Form("Z%d.root", i+1);
  TGraphAsymmErrors* graphs[99][4], * output[4];
  unsigned int n = 4, iRef = 0;
  bool first = true;
  for (unsigned int i = 0; i < nF; i++) {    
    f[i] = TFile::Open(fileNames[i]);
    if (!f[i]) continue;
    for (unsigned int j = 0; j < 4; j++) {
      graphs[i][j] = (TGraphAsymmErrors*)f[i]->Get(names[j]);
      iRef = i;
      if (!graphs[i][j]) { n = j; break; }
      if (first) {
        output[j] = new TGraphAsymmErrors();
        output[j]->SetName(graphs[i][j]->GetName());
      }
      cout << "Adding " << graphs[i][j]->GetN() << " points to graph " << names[j] << " from file " << fileNames[i] << endl;
      for (int k = 0; k < graphs[i][j]->GetN(); k++) {
        int ii = output[j]->GetN();
        TGraphAsymmErrors* g = graphs[i][j];
        output[j]->SetPoint(ii, g->GetX()[k], g->GetY()[k]);
        output[j]->SetPointError(ii, g->GetErrorXlow(k), g->GetErrorXhigh(k), g->GetErrorYlow(k), g->GetErrorYhigh(k));
      }
    }
    first = false;
  }
  TFile* of = TFile::Open("Results/graphs_" + highMassName + "_merge.root", "RECREATE");
  for (unsigned int j = 0; j < n; j++) {
    output[j]->GetXaxis()->SetTitle(graphs[iRef][j]->GetXaxis()->GetTitle());
    output[j]->GetYaxis()->SetTitle(graphs[iRef][j]->GetYaxis()->GetTitle());
    output[j]->SetLineColor(graphs[iRef][j]->GetLineColor());
    output[j]->SetLineStyle(graphs[iRef][j]->GetLineStyle());
    output[j]->SetFillColor(graphs[iRef][j]->GetFillColor());
    output[j]->SetFillStyle(graphs[iRef][j]->GetFillStyle());
    output[j]->Write();
  }
  delete of;
  return n;
}


bool HggNewResonanceStudy::MergeBkgDecompPlots(const TString& highMassName, const TString& lowMassName, double mSwitch)
{
  TFile* fHi = TFile::Open(highMassName);
  TFile* fLo = TFile::Open(lowMassName);

  TString names[4] = { "GG", "GJ", "JG", "JJ" };
  TGraphAsymmErrors* graphs[4];
  TH1D* hists[4];
  
  for (unsigned int i = 0; i < 4; i++) {
    TGraphAsymmErrors* gHi = (TGraphAsymmErrors*)fHi->Get("gW" + names[i] + "TITIStatSyst_DATA_mgg");
    TGraphAsymmErrors* gLo = (TGraphAsymmErrors*)fLo->Get("gW" + names[i] + "TITIStatSyst_DATA_mgg");
    graphs[i] = new TGraphAsymmErrors();
    graphs[i]->SetName("errors_" + names[i]);
    for (int k = 0; k < gLo->GetN(); k++) {
      if (gLo->GetX()[k] > mSwitch) continue;
      int ii = graphs[i]->GetN();
      graphs[i]->SetPoint(ii, gLo->GetX()[k], gLo->GetY()[k]);
      graphs[i]->SetPointError(ii, gLo->GetErrorXlow(k), gLo->GetErrorXhigh(k), gLo->GetErrorYlow(k), gLo->GetErrorYhigh(k));
    }
    for (int k = 0; k < gHi->GetN(); k++) {
      if (gHi->GetX()[k] <= mSwitch) continue;
      int ii = graphs[i]->GetN();
      graphs[i]->SetPoint(ii, gHi->GetX()[k], gHi->GetY()[k]);
      graphs[i]->SetPointError(ii, gHi->GetErrorXlow(k), gHi->GetErrorXhigh(k), gHi->GetErrorYlow(k), gHi->GetErrorYhigh(k));
    }

    
    TH1D* hHi = (TH1D*)fHi->Get("hW" + names[i] + "TITI_DATA_mgg");
    TH1D* hLo = (TH1D*)fLo->Get("hW" + names[i] + "TITI_DATA_mgg");
    double bins[999];
    int nBins = 0;
    
    for (int k = 1; k <= hLo->GetXaxis()->GetNbins(); k++) {
      if (hLo->GetXaxis()->GetBinLowEdge(k) > mSwitch) continue;
      bins[nBins] = hLo->GetXaxis()->GetBinLowEdge(k);
      cout << "Adding " << hLo->GetXaxis()->GetBinLowEdge(k) << endl;
      nBins++;
    }
    for (int k = 1; k <= hHi->GetXaxis()->GetNbins(); k++) {
      if (hHi->GetXaxis()->GetBinLowEdge(k) <= mSwitch) continue;
      bins[nBins] = hHi->GetXaxis()->GetBinLowEdge(k);
      cout << "Adding " << hHi->GetXaxis()->GetBinLowEdge(k) << endl;
      nBins++;
    }
    bins[nBins] = hHi->GetXaxis()->GetBinUpEdge(hHi->GetXaxis()->GetNbins());
    cout << "Adding " << hHi->GetXaxis()->GetBinUpEdge(hHi->GetXaxis()->GetNbins()) << endl;
    hists[i] = new TH1D("hist_" + names[i], "", nBins, bins);
    int n = 0;
    for (int k = 1; k <= hLo->GetXaxis()->GetNbins(); k++) {
      if (hLo->GetXaxis()->GetBinLowEdge(k) > mSwitch) continue;
      n++;
      hists[i]->SetBinContent(n, hLo->GetBinContent(k));
    }
    for (int k = 1; k <= hHi->GetXaxis()->GetNbins(); k++) {
      if (hHi->GetXaxis()->GetBinLowEdge(k) <= mSwitch) continue;
      n++;
      hists[i]->SetBinContent(n, hHi->GetBinContent(k));
    }
  }
  
  TFile* of = TFile::Open("Results/bkg_decomp_merge.root", "RECREATE");
  for (unsigned int i = 0; i < 4; i++) {
    hists[i]->Write();
    graphs[i]->Write();
  }
  delete of;
  return true;
}


bool HggNewResonanceStudy::MakeHighMassNPGraphs(const TString& datacard, double mass, const TString& dataFile, 
                                                const TString& output, bool bkgOnly)
{
  HftModel* hgg = HftModelBuilder::Create("hgg", datacard);
  RooArgList fp = hgg->FreeParameters();
  fp.remove(*hgg->Var("nBackground"));
  fp.remove(*hgg->Var("mgg"));
  fp.remove(*hgg->Var("a1"));
  fp.remove(*hgg->Var("a2"));
  fp.remove(*hgg->Var("xs"));
  HggNewResonanceStudy::PrepareModel(hgg, mass, 44);
  if (bkgOnly) {
    hgg->Var("xs")->setVal(0);
    hgg->Var("xs")->setConstant();
  }
  HftParameterStorage is = hgg->CurrentState();
  HftData* data = HftData::Open(dataFile, "tree", *hgg);
  hgg->FitData(*data->DataSet(), "NewRobustOffsetOffcheck");
  HftParameterStorage bf = hgg->CurrentState();
  double xs0 = hgg->Var("xs")->getVal();

  std::map<TString, double> sensVal, npVal, npErr;
  std::vector< std::pair<double, TString> > sensVect;
  for (int i = 0; i < fp.getSize(); i++) {
    RooRealVar* np = hgg->Var(fp.at(i)->GetName());
    npVal[np->GetName()] = np->getVal();
    npErr[np->GetName()] = np->getError();
  }
  
  for (int i = 0; i < fp.getSize(); i++) {
    RooRealVar* np = hgg->Var(fp.at(i)->GetName());
    if (bkgOnly) {
      sensVal[np->GetName()] = 0;
      sensVect.push_back(std::make_pair<double, TString>(0, np->GetName()));
      continue;
    }
    cout << "==> Testing parameter " << np->GetName() << " (" << i+1 << " of " << fp.getSize() << ")" << endl; 
    hgg->LoadState(is);
    np->setVal(+1);
    np->setConstant();
    hgg->FitData(*data->DataSet(), "NewRobustOffsetOffcheck");
    double xs1 = hgg->Var("xs")->getVal();
    sensVal[np->GetName()] = fabs((xs1 - xs0)/xs0);
    sensVect.push_back(std::make_pair<double, TString>(-fabs((xs1 - xs0)/xs0), np->GetName()));
  }
  
  std::sort(sensVect.begin(), sensVect.end());
  
  TH1D* vals = new TH1D("vals", "", sensVect.size(), 0, sensVect.size());
  TGraphErrors* sens = new TGraphErrors(sensVect.size()); sens->SetName("sens");

  for (unsigned int i = 0; i < sensVect.size(); i++) {
    vals->SetBinContent(i+1, npVal[sensVect[i].second]);
    vals->SetBinError(i+1, npErr[sensVect[i].second]);
    sens->SetPoint(i, vals->GetXaxis()->GetBinCenter(i+1), 0);
    sens->SetPointError(i, 0, sensVal[sensVect[i].second]);
    cout <<  sensVect[i].second << " " << sensVal[sensVect[i].second] << " " << sensVect[i].first << endl;
  }
  for (unsigned int i = 0; i < sensVect.size(); i++) {
    TString label = sensVect[i].second;
    label = "#theta_{" + label(1, label.Length() - 1) + "}";
    sens->GetXaxis()->SetBinLabel(i+1, label);
    vals->GetXaxis()->SetBinLabel(i+1, label);
  }
  sens->SetMaximum(+2);
  sens->SetMinimum(-2);
  sens->SetFillColor(5);
  vals->SetMarkerStyle(20);
  vals->SetMarkerSize(0.8);

  vals->SetMaximum(+2);
  vals->SetMinimum(-2);

  TFile* outFile = TFile::Open(output, "RECREATE");
  sens->Write();
  vals->Write();
  delete outFile;
  
  return true;
}


bool HggNewResonanceStudy::MakeNPGraphs(HftModel& model, RooAbsData& data, const TString& poiName, const TString& output, 
                                        const TString& fitOptions, const TString& exclude)
{
  RooArgList fp = model.FreeParameters();
  TObjArray* vars = exclude.Tokenize(":");
  for (int i = 0; i < vars->GetEntries(); i++) {
    RooRealVar* var = model.Var(vars->At(i)->GetName());
    if (var) fp.remove(*var);
  }
  RooRealVar* poi = model.Var(poiName);
  if (!poi) {
    cout << "ERROR: cannot find parameter of interest " << poiName << endl;
    return false;
  }
  fp.remove(*poi);
  model.Fit(data, fitOptions);
  HftParameterStorage initialState = model.CurrentState();
  //double poiNom = poi->getVal();
  
  std::map<TString, double> sensVal, npVal, npErr;
  std::vector< std::pair<double, TString> > sensVect;
  for (int i = 0; i < fp.getSize(); i++) {
    RooRealVar* np = model.Var(fp.at(i)->GetName());
    npVal[np->GetName()] = np->getVal();
    npErr[np->GetName()] = np->getError();
  }
  
  for (int i = 0; i < fp.getSize(); i++) {
    RooRealVar* np = model.Var(fp.at(i)->GetName());
    cout << "==> Testing parameter " << np->GetName() << " (" << i+1 << " of " << fp.getSize() << ")" << endl; 
    model.LoadState(initialState);
    np->setVal(0);
    np->setConstant();
    model.Fit(data, fitOptions);
    double poi0 = poi->getVal();
    model.LoadState(initialState);
    np->setVal(+1);
    np->setConstant();
    model.Fit(data, fitOptions);
    double poi1 = poi->getVal();
    sensVal[np->GetName()] = fabs((poi1 - poi0)/poi0);
    sensVect.push_back(std::make_pair<double, TString>(-fabs((poi1 - poi0)/poi0), np->GetName()));
  }
  std::sort(sensVect.begin(), sensVect.end());
  TH1D* vals = new TH1D("vals", "", sensVect.size(), 0, sensVect.size());
  TGraphErrors* sens = new TGraphErrors(sensVect.size()); sens->SetName("sens");
  for (unsigned int i = 0; i < sensVect.size(); i++) {
    vals->SetBinContent(i+1, npVal[sensVect[i].second]);
    vals->SetBinError(i+1, npErr[sensVect[i].second]);
    sens->SetPoint(i, vals->GetXaxis()->GetBinCenter(i+1), 0);
    sens->SetPointError(i, 0, sensVal[sensVect[i].second]);
    cout <<  sensVect[i].second << " " << sensVal[sensVect[i].second] << " " << sensVect[i].first << " " 
         << npVal[sensVect[i].second] << " +/- " << npErr[sensVect[i].second] << endl;
  }
  for (unsigned int i = 0; i < sensVect.size(); i++) {
    TString label = sensVect[i].second;
    label = "#theta_{" + label(1, label.Length() - 1) + "}";
    sens->GetXaxis()->SetBinLabel(i+1, label);
    vals->GetXaxis()->SetBinLabel(i+1, label);
  }
  sens->SetMaximum(+2);
  sens->SetMinimum(-2);
  sens->SetFillColor(5);
  vals->SetMarkerStyle(20);
  vals->SetMarkerSize(0.8);

  vals->SetMaximum(+2);
  vals->SetMinimum(-2);

  cout << "Writing " << output << endl;
  TFile* outFile = TFile::Open(output, "RECREATE");
  sens->Write();
  vals->Write();
  delete outFile;
  
  return true;
}


bool HggNewResonanceStudy::MakeLowMassNPGraphs(const TString& datacard, double /*mass*/, const TString& dataFile, 
                                               const TString& output, bool bkgOnly)
{
  HftModel* hgg = HftModelBuilder::Create("hgg", datacard);
  hgg->Var("mgg")->setBins(600);
  RooArgList fp = hgg->FreeParameters();
  fp.remove(*hgg->Var("mgg"));
  fp.remove(*hgg->Var("nBackground_unconv"));
  fp.remove(*hgg->Var("landauSigma_unconv"));
  fp.remove(*hgg->Var("landauFrac_unconv"));
  fp.remove(*hgg->Var("bkgExpSlope_unconv"));
  fp.remove(*hgg->Var("nBackground_oneConv"));
  fp.remove(*hgg->Var("landauSigma_oneConv"));
  fp.remove(*hgg->Var("landauFrac_oneConv"));
  fp.remove(*hgg->Var("bkgExpSlope_oneConv"));
  fp.remove(*hgg->Var("nBackground_twoConv"));
  fp.remove(*hgg->Var("landauSigma_twoConv"));
  fp.remove(*hgg->Var("landauFrac_twoConv"));
  fp.remove(*hgg->Var("bkgExpSlope_twoConv"));
  fp.remove(*hgg->Var("xs"));
  HftParameterStorage is = hgg->CurrentState();
  HftData* data = HftData::Open(dataFile, "tree", *hgg);
  RooDataHist* bc = data->DataSet()->binnedClone();
  double offset = 0;
  hgg->FitData(*bc, "NewRobustOffsetOffcheck", &offset);
  HftParameterStorage bf = hgg->CurrentState();
  double xs0 = hgg->Var("xs")->getVal();

  std::map<TString, double> sensVal, npVal, npErr;
  std::vector< std::pair<double, TString> > sensVect;
  for (int i = 0; i < fp.getSize(); i++) {
    RooRealVar* np = hgg->Var(fp.at(i)->GetName());
    npVal[np->GetName()] = np->getVal();
    npErr[np->GetName()] = np->getError();
  }
  
  for (int i = 0; i < fp.getSize(); i++) {
    RooRealVar* np = hgg->Var(fp.at(i)->GetName());
    if (bkgOnly) {
      sensVal[np->GetName()] = 0;
      sensVect.push_back(std::make_pair<double, TString>(0, np->GetName()));
      continue;
    }
    cout << "==> Testing parameter " << np->GetName() << " (" << i+1 << " of " << fp.getSize() << ")" << endl; 
    hgg->LoadState(is);
    np->setVal(+1);
    np->setConstant();
    hgg->FitData(*bc, "NewRobustOffsetOffcheck", &offset);
    double xs1 = hgg->Var("xs")->getVal();
    sensVal[np->GetName()] = fabs((xs1 - xs0)/xs0);
    sensVect.push_back(std::make_pair<double, TString>(-fabs((xs1 - xs0)/xs0), np->GetName()));
  }
  
  std::sort(sensVect.begin(), sensVect.end());
  
  TH1D* vals = new TH1D("vals", "", sensVect.size(), 0, sensVect.size());
  TGraphErrors* sens = new TGraphErrors(sensVect.size()); sens->SetName("sens");

  for (unsigned int i = 0; i < sensVect.size(); i++) {
    vals->SetBinContent(i+1, npVal[sensVect[i].second]);
    vals->SetBinError(i+1, npErr[sensVect[i].second]);
    sens->SetPoint(i, vals->GetXaxis()->GetBinCenter(i+1), 0);
    sens->SetPointError(i, 0, sensVal[sensVect[i].second]);
  }
  for (unsigned int i = 0; i < sensVect.size(); i++) {
    TString label = sensVect[i].second;
    label = "#theta_{" + label(1, label.Length() - 1) + "}";
    sens->GetXaxis()->SetBinLabel(i+1, label);
    vals->GetXaxis()->SetBinLabel(i+1, label);
  }
  sens->SetMaximum(+1.5);
  sens->SetMinimum(-1.5);
  sens->SetFillColor(5);

  TFile* outFile = TFile::Open(output, "RECREATE");
  sens->Write();
  vals->Write();
  delete outFile;
  
  return true;
}

double HggNewResonanceStudy::ComputeSpuriousSignal(HftModel* hgg, double mX, double range, 
                                                   const TString& dataFile, const TString& dataObj, double lumi, double targetLumi, 
                                                   double step, double weight)
{
  hgg->Var("mX")->setVal(mX);

  RooRealVar* mgg = hgg->Var("mgg");
  mgg->setRange(mX - range, mX + range);
  //mgg->setRange("fitRange", mMin, mMax);

  double relWeight = 1;
  
  HftDataHist* data = HftDataHist::Open(dataFile, dataObj, *hgg, weight > -999 ? weight : targetLumi/lumi); // if fails, could try unbinned HftData
  if (!data) {
    cout << "ERROR : could not open data file " << dataFile << endl;
    return -1;
  }
  if (weight > -999) {
    weight = data->Weight(); // for when weight = -1 and HftDataHist computes the correct weight
    relWeight = weight/(targetLumi/lumi);
  }
  
  hgg->Var("nBackground")->setRange(0, 2*data->DataHist()->sumEntries());
  hgg->Var("nBackground")->setVal(data->DataHist()->sumEntries());

  hgg->Var("nSignal")->setRange(-1E4*relWeight, 1E4*relWeight);

  hgg->FreeParameters().setAttribAll("Constant", 1);
  hgg->Var("nSignal")->setConstant(0);
  hgg->Var("nBackground")->setConstant(0);
  hgg->Var("a1")->setConstant(0);
  hgg->Var("a2")->setConstant(0);

  TGraph* gSig = new TGraph(); gSig->SetName("signal"); gSig->SetTitle("Spurious Signal");
  TGraph* gErr = new TGraph(); gErr->SetName("error");  gErr->SetTitle("Error on signal");
  TGraph* gSSZ = new TGraph(); gSSZ->SetName("ratio");  gSSZ->SetTitle("Spurious signal ratio");
  
  new TCanvas("c1", "");

  hgg->Var("nSignal")->setConstant(1);
  hgg->Var("nSignal")->setVal(0);
  hgg->FitData(*data->DataHist(), "NewRobustOffsetOffcheckW-");
  hgg->Var("nSignal")->setConstant(0);
  HftParameterStorage state = hgg->CurrentState();
  state.Print();
  
  unsigned int i = 0;
  double m = int(mX - 0.9*range);
  while (m < mX + 0.9*range + step) {
    cout << "### Processing point " << i << " at mass " << m << endl;
    hgg->LoadState(state);
    hgg->Var("mX")->setVal(m);
    hgg->FitData(*data->DataHist(), "NewRobustOffsetOffcheckW-");
    double sig = hgg->Var("nSignal")->getVal();
    double err = hgg->Var("nSignal")->getError()*sqrt(relWeight);
    gSig->SetPoint(i, m, sig);
    gErr->SetPoint(i, m, err);
    gSSZ->SetPoint(i, m, sig/err);
    if (m >= mX && m < mX + step)
      hgg->Plot("mgg", *data->DataHist());
    i++;
    m += step;
  }
  
  cout << "Using relWeight = " << relWeight << endl;

  TFile* f = TFile::Open("Results/SpuriousSignal/SS_" + hgg->GetName() + "_" + Form("%g_%g", mX, range) + ".root", "RECREATE");
  gSig->Write();
  gErr->Write();
  gSSZ->Write();
  delete f;
  
  return TMath::MaxElement(1, gSSZ->GetY());
}


double HggNewResonanceStudy::ComputeSpuriousSignalRange(HftModel* hgg, double mX, double rangeMin, double rangeMax, double rangeStep, 
                                                        const TString& dataFile, const TString& dataObj, double lumi, double targetLumi, 
                                                        unsigned int nSteps, double weight)
{
  double range = rangeMin; 
  unsigned int i = 0;
  
  TGraph* gSS = new TGraph();
  gSS->SetName("gSS");
  
  double ssRange = -1;
  
  while (range < rangeMax) {
    cout << "#######################" << endl;
    cout << "### range = " << range << endl;
    cout << "#######################" << endl;
    double step = 2*range/nSteps;
    double ss = HggNewResonanceStudy::ComputeSpuriousSignal(hgg, mX, range, dataFile, dataObj, lumi, targetLumi, step, weight);
    gSS->SetPoint(i, range, ss);
    if (fabs(ss) > 0.2 && ssRange < 0) ssRange = range;
    cout << "For mX = " << mX << ", range = " << range << ", SS = " << ss << endl;
    i++;
    range += rangeStep;
  }

  TFile* f = TFile::Open("Results/SpuriousSignal/SSrange_" + hgg->GetName() + "_" + Form("%g", mX) + ".root", "RECREATE");
  gSS->Write();
  delete f;
  return ssRange;
}


bool HggNewResonanceStudy::MakeHighMassSmearedMCHistos(const TString& inputName, const TString& outputName, unsigned int nBins, double mMin, double mMax)
{
  MultiReader input;
  BranchReader<float>* mass   = input.AddFloat("mass");
  BranchReader<float>* et1    = input.AddFloat("pt_1st");
  BranchReader<float>* et2    = input.AddFloat("pt_2nd");
  BranchReader<float>* eta1   = input.AddFloat("eta_1st");
  BranchReader<float>* eta2   = input.AddFloat("eta_2nd");
  BranchReader<float>* id1    = input.AddFloat("ph_flag_1st");
  BranchReader<float>* id2    = input.AddFloat("ph_flag_2nd");
  BranchReader<float>* isol1  = input.AddFloat("TopoETcone40_1st");
  BranchReader<float>* isol2  = input.AddFloat("TopoETcone40_2nd");
  BranchReader<float>* weight = input.AddFloat("weight");

  TFile* outputFile = TFile::Open(outputName, "RECREATE");
  TH1D* hMass = new TH1D("mass", "mass, weighted", nBins, mMin, mMax);
  TH1D* hMnow = new TH1D("mass_no_weight", "mass, no weight", 2900, 100, 3000);
  
  cout << "Processing file " << inputName << endl;  
  TFile* inputFile = TFile::Open(inputName);
  TTree* inputTree = (TTree*)inputFile->Get("mvatree");
  input.ReadFrom(inputTree);

  for (unsigned int k = 0; k < inputTree->GetEntries(); k++) {
    if (k % 10000 == 0) cout << "Processing entry " << k << " of " << inputTree->GetEntries() << endl;
    inputTree->GetEntry(k);
    if (mass->GetValue() < 100) continue;
    if (et1->GetValue()/mass->GetValue() < 0.4 || et2->GetValue()/mass->GetValue() < 0.3) continue;
    if (fabs(eta1->GetValue()) > 1.37 && fabs(eta1->GetValue()) < 1.52) continue;
    if (fabs(eta2->GetValue()) > 1.37 && fabs(eta2->GetValue()) < 1.52) continue;
    if (fabs(eta1->GetValue()) > 2.37) continue;
    if (fabs(eta2->GetValue()) > 2.37) continue;
    if (id1->GetValue() < 0.5) continue;  // it's a float...
    if (id2->GetValue() < 0.5) continue;  // it's a float...
    if (isol1->GetValue() - 0.007*(et1->GetValue() - 80) > 6) continue; // cannot be looser than 7 GeV anyway, cut already applied in tree
    if (isol2->GetValue() - 0.007*(et2->GetValue() - 80) > 6) continue; // cannot be looser than 7 GeV anyway, cut already applied in tree

    hMass->Fill(mass->GetValue(), weight->GetValue());
    hMnow->Fill(mass->GetValue());
  }
  outputFile->cd();
  hMass->Write();
  hMnow->Write();
  delete outputFile;
  delete inputFile;
  return true;
}


bool HggNewResonanceStudy::MCSyst(HftModel* model, const TString& nomData, const TString& upData, const TString& dnData, const TString& name,
                                  double mMin, double mMax, unsigned int nSteps, const TString& fitOptions,
                                  const std::vector<TString>& vars, const TString& output)
{
  std::vector<TGraphErrors*> gv;
  std::vector<TGraphErrors*> gd;
  std::vector<TGraph*> statErr;
  
  std::vector<TString> suffix(3);
  suffix[0] = "nom";
  suffix[1] = "up";
  suffix[2] = "dn";

  std::vector<TString> dataName(3);
  dataName[0] = nomData;
  dataName[1] = upData;
  dataName[2] = dnData;

  for (unsigned int i = 0; i < vars.size(); i++) {
    TGraph* se = new TGraph(); se->SetName("statErr_" + vars[i]);
    statErr.push_back(se);
    if (!model->Var(vars[i])) {
      cout << "ERROR : variable " << vars[i] << " not found" << endl;
      return false;
    }
    for (unsigned int j = 0; j < 3; j++) {
      TGraphErrors* g = new TGraphErrors(); g->SetName(vars[i] + "_" + suffix[j]);
      gv.push_back(g);
    }
    for (unsigned int j = 1; j < 3; j++) {
      TGraphErrors* g = new TGraphErrors(); g->SetName("delta_" + vars[i] + "_" + suffix[j]);
      gd.push_back(g);
    }
  }
  
  HftParameterStorage state = model->CurrentState();
  double mggMin = model->Var("mgg")->getMin();
  double mggMax = model->Var("mgg")->getMax();
  unsigned int nBins = model->Var("mgg")->getBins();
  
  for (unsigned int k = 0; k < nSteps + 1; k++) {
    double mass = mMin + (mMax - mMin)/nSteps*k;
    cout << "mass = " << mass << endl;
    for (unsigned int j = 0; j < 3; j++) {
      model->LoadState(state);
      model->Var("mgg")->setRange(mggMin, mggMax);
      model->Var("mgg")->setBins(nBins);
      HggNewResonanceStudy::PrepareModel(model, mass, 55);
      HftDataHist* d = HftDataHist::Open(dataName[j], name, *model, -1);
      cout << "---------- Opened data " << dataName[j] << ", mass = " << mass << ", n = " << d->DataHist()->sumEntries() << endl;
      model->FitData(*d->DataHist(), fitOptions);
      for (unsigned int i = 0; i < vars.size(); i++) {
        if (j == 0) {
          statErr[i]->SetPoint(k, mass, model->Var(vars[i])->getError());
        }
        double val = model->Var(vars[i])->getVal();
        double err = model->Var(vars[i])->getError();
        gv[3*i + j]->SetPoint(k, mass, val);
        gv[3*i + j]->SetPointError(k, 0, err);
        if (j > 0) {
          double val0 = gv[3*i]->GetY()[k];
          double err0 = gv[3*i]->GetErrorY(k);
          gd[2*i + j - 1]->SetPoint(k, mass, (val - val0)/val0);
          gd[2*i + j - 1]->SetPointError(k, 0,  sqrt(TMath::Power(err/val, 2) + TMath::Power(err0/val0, 2)));
        }
      }
      delete d;
    }
  }
  
  TFile* f = TFile::Open(output, "RECREATE");
  for (unsigned int i = 0; i < vars.size(); i++) {
    statErr[i]->Write();
    for (unsigned int j = 0; j < 3; j++) gv[3*i + j]->Write();
    for (unsigned int j = 1; j < 3; j++) gd[2*i + j - 1]->Write();
    TGraph* gs = new TGraph(); gs->SetName("syst_" + vars[i]);
    TGraph* gu = new TGraph(); gu->SetName("systUD_" + vars[i]);
    TGraphErrors* gb = new TGraphErrors(); gb->SetName("band_" + vars[i]);
    for (int k = 0; k < gd[2*i]->GetN(); k++) {
      double up = gd[2*i]->GetY()[k];
      double dn = gd[2*i + 1]->GetY()[k];
      double syst = (fabs(up) > fabs(dn) ? up : dn);
      gs->SetPoint(k, gd[2*i]->GetX()[k], syst);
      gu->SetPoint(k, gd[2*i]->GetX()[k], (up - dn)/2);
      gb->SetPoint(k, gd[2*i]->GetX()[k], 0);
      gb->SetPointError(k, 0, syst);
    }
    gs->Write();
    gu->Write();
    gb->Write();
  }
  delete f;
  return true;
}
