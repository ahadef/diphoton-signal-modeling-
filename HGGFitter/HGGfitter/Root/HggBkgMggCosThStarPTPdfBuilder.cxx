// $Id: HggBkgMggCosThStarPTPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggCosThStarPTPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = (RooRealVar&)*dependents.find("mgg");
  RooRealVar& cosThStar = (RooRealVar&)*dependents.find("cosThStar");
  RooRealVar& pT = (RooRealVar&)*dependents.find("pT");
  
  RooAbsPdf* mggPdf = m_mggPdfBuilder.Pdf(name + "_mgg", RooArgList(mgg), workspace);
  RooAbsPdf* cosThStarPdf = m_cosThStarPdfBuilder.Pdf(name + "_cosThStar", RooArgList(cosThStar), workspace);
  RooAbsPdf* pTPdf = m_pTPdfBuilder.Pdf(name + "_pT", RooArgList(pT), workspace);

  return new RooProdPdf(name, "Background PDF for mgg,cosThStar and pT", 
                        RooArgSet(*mggPdf, *cosThStarPdf, *pTPdf));
}
