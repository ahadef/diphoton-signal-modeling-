// $Id: MggBkgPdf.cxx,v 1.4 2007/12/13 20:34:23 hoecker Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/MggBkgPdf.h"
#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "TMath.h"


Hfitter::MggBkgPdf::MggBkgPdf( const MggBkgPdf& other, const char* name )
   : RooAbsPdf( other, name ),
     m_mgg   ( "mgg",    this, other.m_mgg ),
     m_xi    ( "xi",     this, other.m_xi ),
     m_offset( "offset", this, other.m_offset )
{}

Hfitter::MggBkgPdf::MggBkgPdf( const char* name, const char* title, 
                               RooAbsReal& x, RooAbsReal& xi, RooAbsReal& offset )
   : RooAbsPdf( name, title ),
     m_mgg   ( "mgg",    "Dependent", this, x ),
     m_xi    ( "xi",     "Exponent",  this, xi ),
     m_offset( "offset", "offset",    this, offset )
{}

Double_t Hfitter::MggBkgPdf::evaluate() const
{
   return TMath::Exp(m_xi*(m_mgg - m_offset));
}

Int_t Hfitter::MggBkgPdf::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, 
                                                 const char* /*rangeName*/ ) const 
{
   if (matchArgs(allVars,analVars,m_mgg)) return 1;
   return 0;
}

Double_t Hfitter::MggBkgPdf::analyticalIntegral( Int_t code, const char* rangeName ) const 
{
   switch(code) {
   case 1: 
      if (m_xi == 0.0) return 0;
      return (TMath::Exp( m_xi*(m_mgg.max(rangeName) - m_offset) ) - 
              TMath::Exp( m_xi*(m_mgg.min(rangeName) - m_offset) ))/m_xi;
   }
   
  assert(0);
  return 0;
}
