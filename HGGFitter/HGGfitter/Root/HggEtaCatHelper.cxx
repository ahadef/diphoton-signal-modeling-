#include "HGGfitter/HggEtaCatHelper.h"
#include <iostream>
using std::cout;
using std::endl;


using namespace Hfitter;

TH2D* HggEtaCatHelper::resolutionPlot()
{
  TH2D* h = new TH2D("resol", "", m_nBins, m_etaMin, m_etaMax, m_nBins, m_etaMin, m_etaMax);
  h->GetXaxis()->SetTitle(m_title1);
  h->GetYaxis()->SetTitle(m_title2);
  occupancy = new TH2D("occupancy", "", m_nBins, m_etaMin, m_etaMax, m_nBins, m_etaMin, m_etaMax);
  occupancy->GetXaxis()->SetTitle(m_title1);
  occupancy->GetYaxis()->SetTitle(m_title2);
  TH1D* mass = 0; 
  double delta = 0.5*(m_dEta > 0 ? m_dEta : m_etaMax/m_nBins);
  for (int i = 1; i <= m_nBins; i++) {
    double x0 = h->GetXaxis()->GetBinCenter(i);
    cout << "Column " << i << " of " << m_nBins << endl;
    for (int j = 1; j <= m_nBins; j++) {
      double y0 = h->GetYaxis()->GetBinCenter(j);
      TString cut = Form("%s>%.3f&&%s<%.3f&&%s>%.3f&&%s<%.3f", 
                         m_var1.Data(), x0 - delta, m_var1.Data(), x0 + delta, 
                         m_var2.Data(), y0 - delta, m_var2.Data(), y0 + delta);
      if (m_cut) cut == cut + "&&" + m_cut;
      if (mass) delete mass;
      cout << cut << endl;
      mass = new TH1D("mass", "", 100, 110, 130);
      m_tree->Draw("mgg>>mass", cut, "GOFF");
      h->SetBinContent(i, j, mass->GetRMS());
      occupancy->SetBinContent(i,j,mass->GetEntries());
    }
  }
  return h;
}

