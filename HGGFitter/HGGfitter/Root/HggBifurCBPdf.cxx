
#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>

#include "HGGfitter/HggBifurCBPdf.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooMath.h"
#include "TMath.h"
#include "Math/ProbFuncMathCore.h"

using namespace Hfitter;


//_____________________________________________________________________________
HggBifurCBPdf::HggBifurCBPdf(const char *name, const char *title,
           RooAbsReal& _m, RooAbsReal& _m0, RooAbsReal& _sigma,
           RooAbsReal& _alphaLo, RooAbsReal& _sigmaLo,
           RooAbsReal& _alphaHi, RooAbsReal& _nHi) :
  RooAbsPdf(name, title),
  m("m", "Dependent", this, _m),
  m0("m0", "M0", this, _m0),
  sigma("sigma", "Sigma", this, _sigma),
  alphaLo("alphaLo", "Low-side Alpha", this, _alphaLo),
  sigmaLo("sigmaLo", "Low-side Order", this, _sigmaLo),
  alphaHi("alphaHi", "High-side Alpha", this, _alphaHi),
  nHi("nHi", "Hig-side Order", this, _nHi)
{
}


//_____________________________________________________________________________
HggBifurCBPdf::HggBifurCBPdf(const HggBifurCBPdf& other, const char* name) :
  RooAbsPdf(other, name), m("m", this, other.m), m0("m0", this, other.m0),
  sigma("sigma", this, other.sigma), 
  alphaLo("alphaLo", this, other.alphaLo), sigmaLo("sigmaLo", this, other.sigmaLo),
  alphaHi("alphaHi", this, other.alphaHi), nHi("nHi", this, other.nHi)
{
}


//_____________________________________________________________________________
Double_t HggBifurCBPdf::evaluate() const {

  Double_t t = (m-m0)/sigma;

  if (t < -alphaLo) {
    double rho = (sigmaLo*sigmaLo)/(sigma*sigma);
    double x0 = -(1-rho)*alphaLo;
    double y0 = 0.5*x0*alphaLo;
    return exp(-0.5/rho*(t - x0)*(t - x0) + y0);
  }
  else if (t > alphaHi) {
    Double_t a = exp(-0.5*alphaHi*alphaHi);
    Double_t b = nHi/alphaHi - alphaHi; 
    return a/TMath::Power(alphaHi/nHi*(b + t), nHi);
  }
  return exp(-0.5*t*t);
}


//_____________________________________________________________________________
Int_t HggBifurCBPdf::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const
{
  if( matchArgs(allVars,analVars,m) )
    return 1 ;
  
  return 0;
}


//_____________________________________________________________________________
Double_t HggBifurCBPdf::analyticalIntegral(Int_t code, const char* rangeName) const
{
  assert(code==1);
  double result = 0;
    
  double sig = fabs((Double_t)sigma);
  double tmin = (m.min(rangeName)-m0)/sig;
  double tmax = (m.max(rangeName)-m0)/sig;
  
  if (tmin < -alphaLo) {
    double rho = (sigmaLo*sigmaLo)/(sigma*sigma);
    double x0 = -(1-rho)*alphaLo;
    double y0 = 0.5*x0*alphaLo;
    double umin = (tmin - x0)/sqrt(rho);
    double umax = (tmax - x0)/sqrt(rho);
    double uLo  = (-alphaLo - x0)/sqrt(rho);
    result += sqrt(rho)*gaussianIntegral(umin, TMath::Min(umax, uLo))*exp(y0);
  }
  if (tmin < alphaHi && tmax > -alphaLo)
    result += gaussianIntegral(TMath::Max(tmin, -alphaLo), TMath::Min(tmax, alphaHi));
  if (tmax > alphaHi)
    result += powerLawIntegral(-tmax, TMath::Min(-tmin, -alphaHi), alphaHi, nHi);
  return sig*result;
}


double HggBifurCBPdf::gaussianIntegral(double tmin, double tmax) const
{
  if (tmin > tmax) return 0;
  return sqrt(TMath::TwoPi())*(ROOT::Math::gaussian_cdf(tmax) - ROOT::Math::gaussian_cdf(tmin));
}


double HggBifurCBPdf::powerLawIntegral(double tmin, double tmax, double alpha, double n) const
{
  double a = exp(-0.5*alpha*alpha);
  double b = n/alpha - alpha;
  return a/(1 - n)*( (b - tmin)/(TMath::Power(alpha/n*(b - tmin), n)) - (b - tmax)/(TMath::Power(alpha/n*(b - tmax), n)) );
}
