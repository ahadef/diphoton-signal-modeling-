// $Id: HggBkgMggBiasPdfBuilder.cxx,v 1.3 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggBiasPdfBuilder.h"
#include "HGGfitter/MggBiasBkgPdf.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggBiasPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  // retrieve dependent for the Higgs mass
  RooRealVar& mgg = (RooRealVar&)*dependents.find("mgg");
  return new Hfitter::MggBiasBkgPdf(name, "Biased mgg Background PDF", mgg,
				    Param(workspace, "xi"), Param(workspace, "offset"), Param(workspace, "bias"));
}
