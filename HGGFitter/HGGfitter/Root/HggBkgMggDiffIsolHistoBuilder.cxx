// Author: Marine Kuna

#include "HGGfitter/HggBkgMggDiffIsolHistoBuilder.h"

#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "RooExponential.h"
#include "RooConstVar.h"
#include "RooRealConstant.h"
#include "RooAddPdf.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "TFile.h"
#include "TH1F.h"
#include "TH2F.h"

using namespace Hfitter;


RooAbsPdf* HggBkgMggDiffIsolHistoBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{  
  
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  RooMsgService::instance().setSilentMode(true);

  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");
  RooRealVar& isol1 = *Dependent(dependents, m_depName1, "GeV", "isol_{lead}");
  RooRealVar& isol2 = *Dependent(dependents, m_depName2, "GeV", "isol_{sub}");

  //RooArgList isolargList(isol1,isol2);


  //////////////////////////////////////////////// mgg part //////////////////////////////////////////

  RooAbsReal& xi_gg  = Param(workspace, m_xiggName,  "1/GeV");
  RooAbsReal& xi_gj  = Param(workspace, m_xigjName,  "1/GeV");
  
  RooAbsPdf* mggPdf;
  
  if (m_isGam1 && m_isGam2)
  {
  mggPdf = new RooExponential(name + "_MggOnlyPdf", "Exponential PDF", mgg, xi_gg);
  }
  else
  {
  mggPdf = new RooExponential(name + "_MggOnlyPdf", "Exponential PDF", mgg, xi_gj);
  }

  
  /////////////////////////////////////////// isolation part //////////////////////////////////////////
     
     
      TFile *f_ph1[11];
    TFile *f_ph2[11];
    TFile *f_j1[11];
    TFile *f_j2[11];
    TFile *f_jj[11];
   
    TH1F* tempHisto_ph1[11];
    TH1F* tempHisto_ph2[11];
    TH1F* tempHisto_j1[11];
    TH1F* tempHisto_j2[11];
    
    TH2F* tempHisto_jj[11];
  
  //////////////////////////////////////// open leading photon histograms ///////////////////////////////////////
    f_ph1[0] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    tempHisto_ph1[0] = (TH1F*)f_ph1[0]->Get("resulthist");
    tempHisto_ph1[0]->SetDirectory(0);
    f_ph1[0]->Close();
   
    f_ph1[1] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[1] = (TH1F*)f_ph1[1]->Get("resulthist");
    tempHisto_ph1[1]->SetDirectory(0);
    f_ph1[1]->Close();
    
    f_ph1[2] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[2] = (TH1F*)f_ph1[2]->Get("resulthist");
    tempHisto_ph1[2]->SetDirectory(0);
    f_ph1[2]->Close();
   
    f_ph1[3] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Rest_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[3] = (TH1F*)f_ph1[3]->Get("resulthist");
    tempHisto_ph1[3]->SetDirectory(0);
    f_ph1[3]->Close();
   
    f_ph1[4] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Rest_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[4] = (TH1F*)f_ph1[4]->Get("resulthist");
    tempHisto_ph1[4]->SetDirectory(0);
    f_ph1[4]->Close();
   
    f_ph1[5] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[5] = (TH1F*)f_ph1[5]->Get("resulthist");
    tempHisto_ph1[5]->SetDirectory(0);
    f_ph1[5]->Close();
    
    f_ph1[6] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[6] = (TH1F*)f_ph1[6]->Get("resulthist");
    tempHisto_ph1[6]->SetDirectory(0);
    f_ph1[6]->Close();
    
    f_ph1[7] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Medium_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[7] = (TH1F*)f_ph1[7]->Get("resulthist");
    tempHisto_ph1[7]->SetDirectory(0);
    f_ph1[7]->Close();
    
    f_ph1[8] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Medium_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[8] = (TH1F*)f_ph1[8]->Get("resulthist");
    tempHisto_ph1[8]->SetDirectory(0);
    f_ph1[8]->Close();
    
    f_ph1[9] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Bad_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[9] = (TH1F*)f_ph1[9]->Get("resulthist");
    tempHisto_ph1[9]->SetDirectory(0);
    f_ph1[9]->Close();
    
    f_ph1[10] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_IsVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph1[10] = (TH1F*)f_ph1[10]->Get("resulthist");
    tempHisto_ph1[10]->SetDirectory(0);
    f_ph1[10]->Close();


  //////////////////////////////////////// open subleading photon histograms ///////////////////////////////////////
  
    f_ph2[0] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    tempHisto_ph2[0] = (TH1F*)f_ph2[0]->Get("resulthist");
    tempHisto_ph2[0]->SetDirectory(0);
    f_ph2[0]->Close();
   
    f_ph2[1] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[1] = (TH1F*)f_ph2[1]->Get("resulthist");
    tempHisto_ph2[1]->SetDirectory(0);
    f_ph2[1]->Close();
    
    f_ph2[2] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[2] = (TH1F*)f_ph2[2]->Get("resulthist");
    tempHisto_ph2[2]->SetDirectory(0);
    f_ph2[2]->Close();
   
    f_ph2[3] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Rest_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[3] = (TH1F*)f_ph2[3]->Get("resulthist");
    tempHisto_ph2[3]->SetDirectory(0);
    f_ph2[3]->Close();
   
    f_ph2[4] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Rest_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[4] = (TH1F*)f_ph2[4]->Get("resulthist");
    tempHisto_ph2[4]->SetDirectory(0);
    f_ph2[4]->Close();
   
    f_ph2[5] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[5] = (TH1F*)f_ph2[5]->Get("resulthist");
    tempHisto_ph2[5]->SetDirectory(0);
    f_ph2[5]->Close();
    
    f_ph2[6] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[6] = (TH1F*)f_ph2[6]->Get("resulthist");
    tempHisto_ph2[6]->SetDirectory(0);
    f_ph2[6]->Close();
    
    f_ph2[7] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Medium_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[7] = (TH1F*)f_ph2[7]->Get("resulthist");
    tempHisto_ph2[7]->SetDirectory(0);
    f_ph2[7]->Close();
    
    f_ph2[8] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Medium_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[8] = (TH1F*)f_ph2[8]->Get("resulthist");
    tempHisto_ph2[8]->SetDirectory(0);
    f_ph2[8]->Close();
    
    f_ph2[9] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Bad_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[9] = (TH1F*)f_ph2[9]->Get("resulthist");
    tempHisto_ph2[9]->SetDirectory(0);
    f_ph2[9]->Close();
    
    f_ph2[10] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_IsVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto_ph2[10] = (TH1F*)f_ph2[10]->Get("resulthist");
    tempHisto_ph2[10]->SetDirectory(0);
    f_ph2[10]->Close();



  //////////////////////////////////////// open leading jet histograms ///////////////////////////////////////
    f_j1[0] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    tempHisto_j1[0] = (TH1F*)f_j1[0]->Get("bkghist");
    tempHisto_j1[0]->SetDirectory(0);
    f_j1[0]->Close();
   
    f_j1[1] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_NoConv_Good_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[1] = (TH1F*)f_j1[1]->Get("bkghist");
    tempHisto_j1[1]->SetDirectory(0);
    f_j1[1]->Close();
    
    f_j1[2] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_NoConv_Good_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[2] = (TH1F*)f_j1[2]->Get("bkghist");
    tempHisto_j1[2]->SetDirectory(0);
    f_j1[2]->Close();
   
    f_j1[3] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_NoConv_Rest_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[3] = (TH1F*)f_j1[3]->Get("bkghist");
    tempHisto_j1[3]->SetDirectory(0);
    f_j1[3]->Close();
   
    f_j1[4] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_NoConv_Rest_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[4] = (TH1F*)f_j1[4]->Get("bkghist");
    tempHisto_j1[4]->SetDirectory(0);
    f_j1[4]->Close();
   
    f_j1[5] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_Good_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[5] = (TH1F*)f_j1[5]->Get("bkghist");
    tempHisto_j1[5]->SetDirectory(0);
    f_j1[5]->Close();
    
    f_j1[6] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_Good_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[6] = (TH1F*)f_j1[6]->Get("bkghist");
    tempHisto_j1[6]->SetDirectory(0);
    f_j1[6]->Close();
    
    f_j1[7] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_Medium_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[7] = (TH1F*)f_j1[7]->Get("bkghist");
    tempHisto_j1[7]->SetDirectory(0);
    f_j1[7]->Close();
    
    f_j1[8] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_Medium_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[8] = (TH1F*)f_j1[8]->Get("bkghist");
    tempHisto_j1[8]->SetDirectory(0);
    f_j1[8]->Close();
    
    f_j1[9] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_Bad_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[9] = (TH1F*)f_j1[9]->Get("bkghist");
    tempHisto_j1[9]->SetDirectory(0);
    f_j1[9]->Close();
    
    f_j1[10] = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_IsVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j1[10] = (TH1F*)f_j1[10]->Get("bkghist");
    tempHisto_j1[10]->SetDirectory(0);
    f_j1[10]->Close();


  //////////////////////////////////////// open subleading jet histograms ///////////////////////////////////////
  
    f_j2[0] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    tempHisto_j2[0] = (TH1F*)f_j2[0]->Get("bkghist");
    tempHisto_j2[0]->SetDirectory(0);
    f_j2[0]->Close();
   
    f_j2[1] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_NoConv_Good_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[1] = (TH1F*)f_j2[1]->Get("bkghist");
    tempHisto_j2[1]->SetDirectory(0);
    f_j2[1]->Close();
    
    f_j2[2] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_NoConv_Good_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[2] = (TH1F*)f_j2[2]->Get("bkghist");
    tempHisto_j2[2]->SetDirectory(0);
    f_j2[2]->Close();
   
    f_j2[3] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_NoConv_Rest_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[3] = (TH1F*)f_j2[3]->Get("bkghist");
    tempHisto_j2[3]->SetDirectory(0);
    f_j2[3]->Close();
   
    f_j2[4] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_NoConv_Rest_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[4] = (TH1F*)f_j2[4]->Get("bkghist");
    tempHisto_j2[4]->SetDirectory(0);
    f_j2[4]->Close();
   
    f_j2[5] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_Good_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[5] = (TH1F*)f_j2[5]->Get("bkghist");
    tempHisto_j2[5]->SetDirectory(0);
    f_j2[5]->Close();
    
    f_j2[6] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_Good_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[6] = (TH1F*)f_j2[6]->Get("bkghist");
    tempHisto_j2[6]->SetDirectory(0);
    f_j2[6]->Close();
    
    f_j2[7] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_Medium_LowPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[7] = (TH1F*)f_j2[7]->Get("bkghist");
    tempHisto_j2[7]->SetDirectory(0);
    f_j2[7]->Close();
    
    f_j2[8] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_Medium_HighPtt_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[8] = (TH1F*)f_j2[8]->Get("bkghist");
    tempHisto_j2[8]->SetDirectory(0);
    f_j2[8]->Close();
    
    f_j2[9] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_Bad_NoVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[9] = (TH1F*)f_j2[9]->Get("bkghist");
    tempHisto_j2[9]->SetDirectory(0);
    f_j2[9]->Close();
    
    f_j2[10] = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_IsVBF_TopoIsol_Hgg_all_FittingHisto.root","READ");
    tempHisto_j2[10] = (TH1F*)f_j2[10]->Get("bkghist");
    tempHisto_j2[10]->SetDirectory(0);
    f_j2[10]->Close();

  //////////////////////////////////////// open dijet histograms ///////////////////////////////////////

    f_jj[0] = new TFile("jj_2D_pdf_2012.root","READ");
    tempHisto_jj[0] = (TH2F*)f_jj[0]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[0]->SetDirectory(0);
    f_jj[0]->Close();
   
    f_jj[1] = new TFile("jj_2D_pdf_cat1.root","READ");
    tempHisto_jj[1] = (TH2F*)f_jj[1]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[1]->SetDirectory(0);
    f_jj[1]->Close();
    
    f_jj[2] = new TFile("jj_2D_pdf_cat2.root","READ");
    tempHisto_jj[2] = (TH2F*)f_jj[2]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[2]->SetDirectory(0);
    f_jj[2]->Close();
   
    f_jj[3] = new TFile("jj_2D_pdf_cat3.root","READ");
    tempHisto_jj[3] = (TH2F*)f_jj[3]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[3]->SetDirectory(0);
    f_jj[3]->Close();
   
    f_jj[4] = new TFile("jj_2D_pdf_cat4.root","READ");
    tempHisto_jj[4] = (TH2F*)f_jj[4]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[4]->SetDirectory(0);
    f_jj[4]->Close();
   
    f_jj[5] = new TFile("jj_2D_pdf_cat5.root","READ");
    tempHisto_jj[5] = (TH2F*)f_jj[5]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[5]->SetDirectory(0);
    f_jj[5]->Close();
    
    f_jj[6] = new TFile("jj_2D_pdf_cat6.root","READ");
    tempHisto_jj[6] = (TH2F*)f_jj[6]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[6]->SetDirectory(0);
    f_jj[6]->Close();
    
    f_jj[7] = new TFile("jj_2D_pdf_cat7.root","READ");
    tempHisto_jj[7] = (TH2F*)f_jj[7]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[7]->SetDirectory(0);
    f_jj[7]->Close();
    
    f_jj[8] = new TFile("jj_2D_pdf_cat8.root","READ");
    tempHisto_jj[8] = (TH2F*)f_jj[8]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[8]->SetDirectory(0);
    f_jj[8]->Close();
    
    f_jj[9] = new TFile("jj_2D_pdf_cat9.root","READ");
    tempHisto_jj[9] = (TH2F*)f_jj[9]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[9]->SetDirectory(0);
    f_jj[9]->Close();
    
    f_jj[10] = new TFile("jj_2D_pdf_cat10.root","READ");
    tempHisto_jj[10] = (TH2F*)f_jj[10]->Get("htemp__isolation1_isolation2");
    tempHisto_jj[10]->SetDirectory(0);
    f_jj[10]->Close();


  //////////////////////////////////////// RooDataHist for gammas ///////////////////////////////////////

    RooArgList argList1(isol1);
    RooArgList argList2(isol2);
    
    RooDataHist *dataHist_ph1_inclusive = new RooDataHist("hist_ph1_lead_inclusive", "", argList1, tempHisto_ph1[0]);
    RooDataHist *dataHist_ph1_cat1 = new RooDataHist("hist_ph1_lead_cat1", "", argList1, tempHisto_ph1[1]);
    RooDataHist *dataHist_ph1_cat2 = new RooDataHist("hist_ph1_lead_cat2", "", argList1, tempHisto_ph1[2]);
    RooDataHist *dataHist_ph1_cat3 = new RooDataHist("hist_ph1_lead_cat3", "", argList1, tempHisto_ph1[3]);
    RooDataHist *dataHist_ph1_cat4 = new RooDataHist("hist_ph1_lead_cat4", "", argList1, tempHisto_ph1[4]);
    RooDataHist *dataHist_ph1_cat5 = new RooDataHist("hist_ph1_lead_cat5", "", argList1, tempHisto_ph1[5]);
    RooDataHist *dataHist_ph1_cat6 = new RooDataHist("hist_ph1_lead_cat6", "", argList1, tempHisto_ph1[6]);
    RooDataHist *dataHist_ph1_cat7 = new RooDataHist("hist_ph1_lead_cat7", "", argList1, tempHisto_ph1[7]);
    RooDataHist *dataHist_ph1_cat8 = new RooDataHist("hist_ph1_lead_cat8", "", argList1, tempHisto_ph1[8]);
    RooDataHist *dataHist_ph1_cat9 = new RooDataHist("hist_ph1_lead_cat9", "", argList1, tempHisto_ph1[9]);
    RooDataHist *dataHist_ph1_cat10 = new RooDataHist("hist_ph1_lead_cat10", "", argList1, tempHisto_ph1[10]);

    RooDataHist *dataHist_ph2_inclusive = new RooDataHist("hist_ph2_sub_inclusive", "", argList2, tempHisto_ph2[0]);
    RooDataHist *dataHist_ph2_cat1 = new RooDataHist("hist_ph2_sub_cat1", "", argList2, tempHisto_ph2[1]);
    RooDataHist *dataHist_ph2_cat2 = new RooDataHist("hist_ph2_sub_cat2", "", argList2, tempHisto_ph2[2]);
    RooDataHist *dataHist_ph2_cat3 = new RooDataHist("hist_ph2_sub_cat3", "", argList2, tempHisto_ph2[3]);
    RooDataHist *dataHist_ph2_cat4 = new RooDataHist("hist_ph2_sub_cat4", "", argList2, tempHisto_ph2[4]);
    RooDataHist *dataHist_ph2_cat5 = new RooDataHist("hist_ph2_sub_cat5", "", argList2, tempHisto_ph2[5]);
    RooDataHist *dataHist_ph2_cat6 = new RooDataHist("hist_ph2_sub_cat6", "", argList2, tempHisto_ph2[6]);
    RooDataHist *dataHist_ph2_cat7 = new RooDataHist("hist_ph2_sub_cat7", "", argList2, tempHisto_ph2[7]);
    RooDataHist *dataHist_ph2_cat8 = new RooDataHist("hist_ph2_sub_cat8", "", argList2, tempHisto_ph2[8]);
    RooDataHist *dataHist_ph2_cat9 = new RooDataHist("hist_ph2_sub_cat9", "", argList2, tempHisto_ph2[9]);
    RooDataHist *dataHist_ph2_cat10 = new RooDataHist("hist_ph2_sub_cat10", "", argList2, tempHisto_ph2[10]);
  
  //////////////////////////////////////// RooDataHist for jets ///////////////////////////////////////

    RooDataHist *dataHist_j1_inclusive = new RooDataHist("hist_j1_lead_inclusive", "", argList1, tempHisto_j1[0]);
    RooDataHist *dataHist_j1_cat1 = new RooDataHist("hist_j1_lead_cat1", "", argList1, tempHisto_j1[1]);
    RooDataHist *dataHist_j1_cat2 = new RooDataHist("hist_j1_lead_cat2", "", argList1, tempHisto_j1[2]);
    RooDataHist *dataHist_j1_cat3 = new RooDataHist("hist_j1_lead_cat3", "", argList1, tempHisto_j1[3]);
    RooDataHist *dataHist_j1_cat4 = new RooDataHist("hist_j1_lead_cat4", "", argList1, tempHisto_j1[4]);
    RooDataHist *dataHist_j1_cat5 = new RooDataHist("hist_j1_lead_cat5", "", argList1, tempHisto_j1[5]);
    RooDataHist *dataHist_j1_cat6 = new RooDataHist("hist_j1_lead_cat6", "", argList1, tempHisto_j1[6]);
    RooDataHist *dataHist_j1_cat7 = new RooDataHist("hist_j1_lead_cat7", "", argList1, tempHisto_j1[7]);
    RooDataHist *dataHist_j1_cat8 = new RooDataHist("hist_j1_lead_cat8", "", argList1, tempHisto_j1[8]);
    RooDataHist *dataHist_j1_cat9 = new RooDataHist("hist_j1_lead_cat9", "", argList1, tempHisto_j1[9]);
    RooDataHist *dataHist_j1_cat10 = new RooDataHist("hist_j1_lead_cat10", "", argList1, tempHisto_j1[10]);

    RooDataHist *dataHist_j2_inclusive = new RooDataHist("hist_j2_sub_inclusive", "", argList2, tempHisto_j2[0]);
    RooDataHist *dataHist_j2_cat1 = new RooDataHist("hist_j2_sub_cat1", "", argList2, tempHisto_j2[1]);
    RooDataHist *dataHist_j2_cat2 = new RooDataHist("hist_j2_sub_cat2", "", argList2, tempHisto_j2[2]);
    RooDataHist *dataHist_j2_cat3 = new RooDataHist("hist_j2_sub_cat3", "", argList2, tempHisto_j2[3]);
    RooDataHist *dataHist_j2_cat4 = new RooDataHist("hist_j2_sub_cat4", "", argList2, tempHisto_j2[4]);
    RooDataHist *dataHist_j2_cat5 = new RooDataHist("hist_j2_sub_cat5", "", argList2, tempHisto_j2[5]);
    RooDataHist *dataHist_j2_cat6 = new RooDataHist("hist_j2_sub_cat6", "", argList2, tempHisto_j2[6]);
    RooDataHist *dataHist_j2_cat7 = new RooDataHist("hist_j2_sub_cat7", "", argList2, tempHisto_j2[7]);
    RooDataHist *dataHist_j2_cat8 = new RooDataHist("hist_j2_sub_cat8", "", argList2, tempHisto_j2[8]);
    RooDataHist *dataHist_j2_cat9 = new RooDataHist("hist_j2_sub_cat9", "", argList2, tempHisto_j2[9]);
    RooDataHist *dataHist_j2_cat10 = new RooDataHist("hist_j2_sub_cat10", "", argList2, tempHisto_j2[10]);
   
   //////////////////////////////////////// RooDataHist for di-jets ///////////////////////////////////////
     RooArgList isolargList(isol1,isol2);


    RooDataHist *dataHist_jj_inclusive = new RooDataHist("hist_jj_lead_inclusive", "", isolargList, tempHisto_jj[0]);
    RooDataHist *dataHist_jj_cat1 = new RooDataHist("hist_jj_lead_cat1", "", isolargList, tempHisto_jj[1]);
    RooDataHist *dataHist_jj_cat2 = new RooDataHist("hist_jj_lead_cat2", "", isolargList, tempHisto_jj[2]);
    RooDataHist *dataHist_jj_cat3 = new RooDataHist("hist_jj_lead_cat3", "", isolargList, tempHisto_jj[3]);
    RooDataHist *dataHist_jj_cat4 = new RooDataHist("hist_jj_lead_cat4", "", isolargList, tempHisto_jj[4]);
    RooDataHist *dataHist_jj_cat5 = new RooDataHist("hist_jj_lead_cat5", "", isolargList, tempHisto_jj[5]);
    RooDataHist *dataHist_jj_cat6 = new RooDataHist("hist_jj_lead_cat6", "", isolargList, tempHisto_jj[6]);
    RooDataHist *dataHist_jj_cat7 = new RooDataHist("hist_jj_lead_cat7", "", isolargList, tempHisto_jj[7]);
    RooDataHist *dataHist_jj_cat8 = new RooDataHist("hist_jj_lead_cat8", "", isolargList, tempHisto_jj[8]);
    RooDataHist *dataHist_jj_cat9 = new RooDataHist("hist_jj_lead_cat9", "", isolargList, tempHisto_jj[9]);
    RooDataHist *dataHist_jj_cat10 = new RooDataHist("hist_jj_lead_cat10", "", isolargList, tempHisto_jj[10]);
       
  //////////////////////////////////////// RooHistPdfs for gammas ///////////////////////////////////////
    
    RooArgSet tmpSet1(argList1);
    RooArgSet tmpSet2(argList2);

    RooHistPdf *isol1GamPdf_inclusive = new RooHistPdf("isol1GamPdf_inclusive","isol1GamPdf_inclusive", tmpSet1, *dataHist_ph1_inclusive);
    RooHistPdf *isol1GamPdf_cat1 = new RooHistPdf("isol1GamPdf_cat1","isol1GamPdf_cat1", tmpSet1, *dataHist_ph1_cat1);
    RooHistPdf *isol1GamPdf_cat2 = new RooHistPdf("isol1GamPdf_cat2","isol1GamPdf_cat2", tmpSet1, *dataHist_ph1_cat2);
    RooHistPdf *isol1GamPdf_cat3 = new RooHistPdf("isol1GamPdf_cat3","isol1GamPdf_cat3", tmpSet1, *dataHist_ph1_cat3);
    RooHistPdf *isol1GamPdf_cat4 = new RooHistPdf("isol1GamPdf_cat4","isol1GamPdf_cat4", tmpSet1, *dataHist_ph1_cat4);
    RooHistPdf *isol1GamPdf_cat5 = new RooHistPdf("isol1GamPdf_cat5","isol1GamPdf_cat5", tmpSet1, *dataHist_ph1_cat5);
    RooHistPdf *isol1GamPdf_cat6 = new RooHistPdf("isol1GamPdf_cat6","isol1GamPdf_cat6", tmpSet1, *dataHist_ph1_cat6);
    RooHistPdf *isol1GamPdf_cat7 = new RooHistPdf("isol1GamPdf_cat7","isol1GamPdf_cat7", tmpSet1, *dataHist_ph1_cat7);
    RooHistPdf *isol1GamPdf_cat8 = new RooHistPdf("isol1GamPdf_cat8","isol1GamPdf_cat8", tmpSet1, *dataHist_ph1_cat8);
    RooHistPdf *isol1GamPdf_cat9 = new RooHistPdf("isol1GamPdf_cat9","isol1GamPdf_cat9", tmpSet1, *dataHist_ph1_cat9);
    RooHistPdf *isol1GamPdf_cat10 = new RooHistPdf("isol1GamPdf_cat10","isol1GamPdf_cat10", tmpSet1, *dataHist_ph1_cat10);

    RooHistPdf *isol2GamPdf_inclusive = new RooHistPdf("isol2GamPdf_inclusive","isol2GamPdf_inclusive", tmpSet2,*dataHist_ph2_inclusive);
    RooHistPdf *isol2GamPdf_cat1 = new RooHistPdf("isol2GamPdf_cat1","isol2GamPdf_cat1", tmpSet2, *dataHist_ph2_cat1);
    RooHistPdf *isol2GamPdf_cat2 = new RooHistPdf("isol2GamPdf_cat2","isol2GamPdf_cat2", tmpSet2, *dataHist_ph2_cat2);
    RooHistPdf *isol2GamPdf_cat3 = new RooHistPdf("isol2GamPdf_cat3","isol2GamPdf_cat3", tmpSet2, *dataHist_ph2_cat3);
    RooHistPdf *isol2GamPdf_cat4 = new RooHistPdf("isol2GamPdf_cat4","isol2GamPdf_cat4", tmpSet2, *dataHist_ph2_cat4);
    RooHistPdf *isol2GamPdf_cat5 = new RooHistPdf("isol2GamPdf_cat5","isol2GamPdf_cat5", tmpSet2, *dataHist_ph2_cat5);
    RooHistPdf *isol2GamPdf_cat6 = new RooHistPdf("isol2GamPdf_cat6","isol2GamPdf_cat6", tmpSet2, *dataHist_ph2_cat6);
    RooHistPdf *isol2GamPdf_cat7 = new RooHistPdf("isol2GamPdf_cat7","isol2GamPdf_cat7", tmpSet2, *dataHist_ph2_cat7);
    RooHistPdf *isol2GamPdf_cat8 = new RooHistPdf("isol2GamPdf_cat8","isol2GamPdf_cat8", tmpSet2, *dataHist_ph2_cat8);
    RooHistPdf *isol2GamPdf_cat9 = new RooHistPdf("isol2GamPdf_cat9","isol2GamPdf_cat9", tmpSet2, *dataHist_ph2_cat9);
    RooHistPdf *isol2GamPdf_cat10 = new RooHistPdf("isol2GamPdf_cat10","isol2GamPdf_cat10", tmpSet2, *dataHist_ph2_cat10);
 
   //////////////////////////////////////// RooHistPdfs for jets ///////////////////////////////////////
   
    RooHistPdf *isol1JetPdf_inclusive = new RooHistPdf("isol1JetPdf_inclusive","isol1JetPdf_inclusive", tmpSet1, *dataHist_j1_inclusive);
    RooHistPdf *isol1JetPdf_cat1 = new RooHistPdf("isol1JetPdf_cat1","isol1JetPdf_cat1", tmpSet1, *dataHist_j1_cat1);
    RooHistPdf *isol1JetPdf_cat2 = new RooHistPdf("isol1JetPdf_cat2","isol1JetPdf_cat2", tmpSet1, *dataHist_j1_cat2);
    RooHistPdf *isol1JetPdf_cat3 = new RooHistPdf("isol1JetPdf_cat3","isol1JetPdf_cat3", tmpSet1, *dataHist_j1_cat3);
    RooHistPdf *isol1JetPdf_cat4 = new RooHistPdf("isol1JetPdf_cat4","isol1JetPdf_cat4", tmpSet1, *dataHist_j1_cat4);
    RooHistPdf *isol1JetPdf_cat5 = new RooHistPdf("isol1JetPdf_cat5","isol1JetPdf_cat5", tmpSet1, *dataHist_j1_cat5);
    RooHistPdf *isol1JetPdf_cat6 = new RooHistPdf("isol1JetPdf_cat6","isol1JetPdf_cat6", tmpSet1, *dataHist_j1_cat6);
    RooHistPdf *isol1JetPdf_cat7 = new RooHistPdf("isol1JetPdf_cat7","isol1JetPdf_cat7", tmpSet1, *dataHist_j1_cat7);
    RooHistPdf *isol1JetPdf_cat8 = new RooHistPdf("isol1JetPdf_cat8","isol1JetPdf_cat8", tmpSet1, *dataHist_j1_cat8);
    RooHistPdf *isol1JetPdf_cat9 = new RooHistPdf("isol1JetPdf_cat9","isol1JetPdf_cat9", tmpSet1, *dataHist_j1_cat9);
    RooHistPdf *isol1JetPdf_cat10 = new RooHistPdf("isol1JetPdf_cat10","isol1JetPdf_cat10", tmpSet1, *dataHist_j1_cat10);

    RooHistPdf *isol2JetPdf_inclusive = new RooHistPdf("isol2JetPdf_inclusive","isol2JetPdf_inclusive", tmpSet2,*dataHist_j2_inclusive);
    RooHistPdf *isol2JetPdf_cat1 = new RooHistPdf("isol2JetPdf_cat1","isol2JetPdf_cat1", tmpSet2, *dataHist_j2_cat1);
    RooHistPdf *isol2JetPdf_cat2 = new RooHistPdf("isol2JetPdf_cat2","isol2JetPdf_cat2", tmpSet2, *dataHist_j2_cat2);
    RooHistPdf *isol2JetPdf_cat3 = new RooHistPdf("isol2JetPdf_cat3","isol2JetPdf_cat3", tmpSet2, *dataHist_j2_cat3);
    RooHistPdf *isol2JetPdf_cat4 = new RooHistPdf("isol2JetPdf_cat4","isol2JetPdf_cat4", tmpSet2, *dataHist_j2_cat4);
    RooHistPdf *isol2JetPdf_cat5 = new RooHistPdf("isol2JetPdf_cat5","isol2JetPdf_cat5", tmpSet2, *dataHist_j2_cat5);
    RooHistPdf *isol2JetPdf_cat6 = new RooHistPdf("isol2JetPdf_cat6","isol2JetPdf_cat6", tmpSet2, *dataHist_j2_cat6);
    RooHistPdf *isol2JetPdf_cat7 = new RooHistPdf("isol2JetPdf_cat7","isol2JetPdf_cat7", tmpSet2, *dataHist_j2_cat7);
    RooHistPdf *isol2JetPdf_cat8 = new RooHistPdf("isol2JetPdf_cat8","isol2JetPdf_cat8", tmpSet2, *dataHist_j2_cat8);
    RooHistPdf *isol2JetPdf_cat9 = new RooHistPdf("isol2JetPdf_cat9","isol2JetPdf_cat9", tmpSet2, *dataHist_j2_cat9);
    RooHistPdf *isol2JetPdf_cat10 = new RooHistPdf("isol2JetPdf_cat10","isol2JetPdf_cat10", tmpSet2, *dataHist_j2_cat10);

   //////////////////////////////////////// RooHistPdfs for di-jets ///////////////////////////////////////

     RooArgSet tmpSet(isolargList);
   
    RooHistPdf *isolJetJetPdf_inclusive = new RooHistPdf("isolJetJetPdf_inclusive","isolJetJetPdf_inclusive", tmpSet, *dataHist_jj_inclusive);
    RooHistPdf *isolJetJetPdf_cat1 = new RooHistPdf("isolJetJetPdf_cat1","isolJetJetPdf_cat1", tmpSet, *dataHist_jj_cat1);
    RooHistPdf *isolJetJetPdf_cat2 = new RooHistPdf("isolJetJetPdf_cat2","isolJetJetPdf_cat2", tmpSet, *dataHist_jj_cat2);
    RooHistPdf *isolJetJetPdf_cat3 = new RooHistPdf("isolJetJetPdf_cat3","isolJetJetPdf_cat3", tmpSet, *dataHist_jj_cat3);
    RooHistPdf *isolJetJetPdf_cat4 = new RooHistPdf("isolJetJetPdf_cat4","isolJetJetPdf_cat4", tmpSet, *dataHist_jj_cat4);
    RooHistPdf *isolJetJetPdf_cat5 = new RooHistPdf("isolJetJetPdf_cat5","isolJetJetPdf_cat5", tmpSet, *dataHist_jj_cat5);
    RooHistPdf *isolJetJetPdf_cat6 = new RooHistPdf("isolJetJetPdf_cat6","isolJetJetPdf_cat6", tmpSet, *dataHist_jj_cat6);
    RooHistPdf *isolJetJetPdf_cat7 = new RooHistPdf("isolJetJetPdf_cat7","isolJetJetPdf_cat7", tmpSet, *dataHist_jj_cat7);
    RooHistPdf *isolJetJetPdf_cat8 = new RooHistPdf("isolJetJetPdf_cat8","isolJetJetPdf_cat8", tmpSet, *dataHist_jj_cat8);
    RooHistPdf *isolJetJetPdf_cat9 = new RooHistPdf("isolJetJetPdf_cat9","isolJetJetPdf_cat9", tmpSet, *dataHist_jj_cat9);
    RooHistPdf *isolJetJetPdf_cat10 = new RooHistPdf("isolJetJetPdf_cat10","isolJetJetPdf_cat10", tmpSet, *dataHist_jj_cat10);


//////////////////////////////// RooArgList for gammas //////////////////////
    RooArgList hist_ph1PDFList_first(*isol1GamPdf_inclusive,
    			   *isol1GamPdf_cat1,
    			   *isol1GamPdf_cat2,
    			   *isol1GamPdf_cat3,
    			   *isol1GamPdf_cat4,
    			   *isol1GamPdf_cat5
    			   );
			   
    RooArgList hist_ph1PDFList_second(*isol1GamPdf_cat6,
    			   *isol1GamPdf_cat7,
    			   *isol1GamPdf_cat8,
    			   *isol1GamPdf_cat9,
    			   *isol1GamPdf_cat10
    			   );
			   
    RooArgList hist_ph2PDFList_first(*isol2GamPdf_inclusive,
    			   *isol2GamPdf_cat1,
    			   *isol2GamPdf_cat2,
    			   *isol2GamPdf_cat3,
    			   *isol2GamPdf_cat4,
    			   *isol2GamPdf_cat5
    			   );
			   
    RooArgList hist_ph2PDFList_second(*isol2GamPdf_cat6,
    			   *isol2GamPdf_cat7,
    			   *isol2GamPdf_cat8,
    			   *isol2GamPdf_cat9,
    			   *isol2GamPdf_cat10
    			   );

//////////////////////////////// RooArgList for jets //////////////////////
    RooArgList hist_j1PDFList_first(*isol1JetPdf_inclusive,
    			   *isol1JetPdf_cat1,
    			   *isol1JetPdf_cat2,
    			   *isol1JetPdf_cat3,
    			   *isol1JetPdf_cat4,
    			   *isol1JetPdf_cat5
    			   );
			   
    RooArgList hist_j1PDFList_second(*isol1JetPdf_cat6,
    			   *isol1JetPdf_cat7,
    			   *isol1JetPdf_cat8,
    			   *isol1JetPdf_cat9,
    			   *isol1JetPdf_cat10
    			   );
			   
    RooArgList hist_j2PDFList_first(*isol2JetPdf_inclusive,
    			   *isol2JetPdf_cat1,
    			   *isol2JetPdf_cat2,
    			   *isol2JetPdf_cat3,
    			   *isol2JetPdf_cat4,
    			   *isol2JetPdf_cat5
    			   );
			   
    RooArgList hist_j2PDFList_second(*isol2JetPdf_cat6,
    			   *isol2JetPdf_cat7,
    			   *isol2JetPdf_cat8,
    			   *isol2JetPdf_cat9,
    			   *isol2JetPdf_cat10
    			   );

//////////////////////////////// RooArgList for di-jets //////////////////////

    RooArgList hist_jjPDFList_first(*isolJetJetPdf_inclusive,
    			   *isolJetJetPdf_cat1,
    			   *isolJetJetPdf_cat2,
    			   *isolJetJetPdf_cat3,
    			   *isolJetJetPdf_cat4,
    			   *isolJetJetPdf_cat5
    			   );
			   
    RooArgList hist_jjPDFList_second(*isolJetJetPdf_cat6,
    			   *isolJetJetPdf_cat7,
    			   *isolJetJetPdf_cat8,
    			   *isolJetJetPdf_cat9,
    			   *isolJetJetPdf_cat10
    			   );



  //////////////////////////////// coefficients are the same for jets and gams //////////////////////
  
    RooArgList coefList_first(Param(workspace, "histCatCoeff0"),
    			Param(workspace, "histCatCoeff1"),
    			Param(workspace, "histCatCoeff2"),
    			Param(workspace, "histCatCoeff3"),
    			Param(workspace, "histCatCoeff4"),
    			Param(workspace, "histCatCoeff5")
    			);
			
    RooArgList coefList_second(Param(workspace, "histCatCoeff6"),
    			Param(workspace, "histCatCoeff7"),
    			Param(workspace, "histCatCoeff8"),
    			Param(workspace, "histCatCoeff9"),
    			Param(workspace, "histCatCoeff10")
    			);
			
  //////////////////////////////// build the gam pdf ///////////////////////////////////////////


  RooAbsPdf* isol1GamPdf_first = new RooAddPdf(name + "_Gam_Isol1_first", "leading Gam isol1 PDF _first",
                                               hist_ph1PDFList_first, coefList_first );
					       
  RooAbsPdf* isol1GamPdf_second = new RooAddPdf(name + "_Gam_Isol1_second", "leading Gam isol1 PDF _second",
                                               hist_ph1PDFList_second, coefList_second );
					       
  RooAbsPdf* isol1GamPdf = new RooAddPdf(name + "_Gam_Isol1", "leading Gam isol1 PDF",
                                               RooArgList(*isol1GamPdf_first,*isol1GamPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );
	
  RooAbsPdf* isol2GamPdf_first = new RooAddPdf(name + "_Gam_Isol2_first", "leading Gam isol2 PDF _first",
                                               hist_ph2PDFList_first, coefList_first );
					       
  RooAbsPdf* isol2GamPdf_second = new RooAddPdf(name + "_Gam_Isol2_second", "leading Gam isol2 PDF _second",
                                               hist_ph2PDFList_second, coefList_second );
					       
  RooAbsPdf* isol2GamPdf = new RooAddPdf(name + "_Gam_Isol2", "leading isol2 PDF",
                                               RooArgList(*isol2GamPdf_first,*isol2GamPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );

 //////////////////////////////// build the jet pdf ///////////////////////////////////////////


  RooAbsPdf* isol1JetPdf_first = new RooAddPdf(name + "_Jet_Isol1_first", "leading Jet isol1 PDF _first",
                                               hist_j1PDFList_first, coefList_first );
					       
  RooAbsPdf* isol1JetPdf_second = new RooAddPdf(name + "_Jet_Isol1_second", "leading Jet isol1 PDF _second",
                                               hist_j1PDFList_second, coefList_second );
					       
  RooAbsPdf* isol1JetPdf = new RooAddPdf(name + "_Jet_Isol1", "leading Jet isol1 PDF",
                                               RooArgList(*isol1JetPdf_first,*isol1JetPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );
	
  RooAbsPdf* isol2JetPdf_first = new RooAddPdf(name + "_Jet_Isol2_first", "leading Jet isol2 PDF _first",
                                               hist_j2PDFList_first, coefList_first );
					       
  RooAbsPdf* isol2JetPdf_second = new RooAddPdf(name + "_Jet_Isol2_second", "leading Jet isol2 PDF _second",
                                               hist_j2PDFList_second, coefList_second );
					       
  RooAbsPdf* isol2JetPdf = new RooAddPdf(name + "_Jet_Isol2", "leading isol2 PDF",
                                               RooArgList(*isol2JetPdf_first,*isol2JetPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );

 //////////////////////////////// build the di-jet pdf ///////////////////////////////////////////

  RooAbsPdf* isolJetJetPdf_first = new RooAddPdf(name + "_JetJet_Isol_first", "JetJet isol PDF _first",
                                               hist_jjPDFList_first, coefList_first );
					       
  RooAbsPdf* isolJetJetPdf_second = new RooAddPdf(name + "_JetJet_Isol_second", "JetJet isol PDF _second",
                                               hist_jjPDFList_second, coefList_second );
					       
  RooAbsPdf* isolJetJetPdf = new RooAddPdf(name + "_JetJet_Isol", "JetJet isol PDF",
                                               RooArgList(*isolJetJetPdf_first,*isolJetJetPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );


////////////////////////////////// return the right pdf according to the case /////////////////:

  if (m_isGam1 && m_isGam2)
    {
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1GamPdf, *isol2GamPdf));			
    }
  else if (m_isGam1 && !m_isGam2)
    {
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1GamPdf, *isol2JetPdf));			
    }
  else if (!m_isGam1 && m_isGam2)
    {
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1JetPdf, *isol2GamPdf));			
    }
  //else if (!m_isGam1 && !m_isGam2)
  else
    {
   return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isolJetJetPdf));
    }

     
  /*  TH2F* ph;
    TH1F* ph1;
    TH1F* ph2;

    RooDataHist *dataHist;
    RooDataHist *dataHist1;
    RooDataHist *dataHist2;

    RooAbsPdf* isolPdf;
    RooAbsPdf* isol1Pdf;
    RooAbsPdf* isol2Pdf;
    
    TFile *ff = 0;
    TFile *f1 = 0;
    TFile *f2 = 0;


  if (m_isGam1 && m_isGam2)
    {
    
    // leading photon 1D pdf
    f1 = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph1 = (TH1F*)f1->Get("resulthist");
    ph1->SetDirectory(0);
    f1->Close(); 
       
    dataHist1 = new RooDataHist("hist1", "", isol1, ph1);
    isol1Pdf = new RooHistPdf("isol1GamPdf","isol1GamPdf", isol1, *dataHist1);
    
    
    // subleading photon 1D pdf
    f2 = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph2 = (TH1F*)f2->Get("resulthist");
    ph2->SetDirectory(0);
    f2->Close();
    
    dataHist2 = new RooDataHist("hist2", "", isol2, ph2);
    isol2Pdf = new RooHistPdf("isol2GamPdf","isol2GamPdf", isol2, *dataHist2);

    std::cout << " " << std::endl;

    // return product
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1Pdf, *isol2Pdf));			
    }
    
  else if (m_isGam1 && !m_isGam2)
    {
     // leading photon 1D pdf
    f1 = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph1 = (TH1F*)f1->Get("resulthist");
    ph1->SetDirectory(0);
    f1->Close(); 
       
    dataHist1 = new RooDataHist("hist1", "", isol1, ph1);
    isol1Pdf = new RooHistPdf("isol1GamPdf","isol1GamPdf", isol1, *dataHist1);
   
   
     // subleading jet 1D pdf
    f2 = new TFile("bkg_data2012_secondjet_etabin0_range-5.0:20.0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph2 = (TH1F*)f2->Get("bkghist");
    ph2->SetDirectory(0);
    f2->Close();
    
    dataHist2 = new RooDataHist("hist2", "", isol2, ph2);
    isol2Pdf = new RooHistPdf("isol2JetPdf","isol2JetPdf", isol2, *dataHist2);    

    std::cout << " " << std::endl;

   
    // return product
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1Pdf, *isol2Pdf));
    
    }
  else if (!m_isGam1 && m_isGam2)
    {
     // leading jet 1D pdf
    f1 = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph1 = (TH1F*)f1->Get("bkghist");
    ph1->SetDirectory(0);
    f1->Close();
    
    dataHist1 = new RooDataHist("hist1", "", isol1, ph1);
    isol1Pdf = new RooHistPdf("isol1JetPdf","isol1JetPdf", isol1, *dataHist1);
    
    
    // subleading photon 1D pdf
    f2 = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph2 = (TH1F*)f2->Get("resulthist");
    ph2->SetDirectory(0);
    f2->Close();
    
    dataHist2 = new RooDataHist("hist2", "", isol2, ph2);
    isol2Pdf = new RooHistPdf("isol2GamPdf","isol2GamPdf", isol2, *dataHist2);

    std::cout << " " << std::endl;

    // return product
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1Pdf, *isol2Pdf));			
    
    }
  else if (!m_isGam1 && !m_isGam2)
    {
    
    // 2D jet-jet pdf (for iso1/iso2 correlations)
    ff = new TFile("jj_2D_pdf_2012.root","READ");
    ph = (TH2F*)ff->Get("htemp__isolation1_isolation2");
    ph->SetDirectory(0);
    ff->Close();
    
    dataHist = new RooDataHist("hist", "", isolargList, ph);
    RooArgSet tmpSet(isolargList);
    isolPdf = new RooHistPdf("isol2DPdf","isol2DPdf", tmpSet, *dataHist);    
    
    return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isolPdf));
   

    }
   				
*/

			
}
