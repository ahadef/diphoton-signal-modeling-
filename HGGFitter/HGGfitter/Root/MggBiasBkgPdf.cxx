// $Id: MggBiasBkgPdf.cxx,v 1.3 2007/12/13 20:34:22 hoecker Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/MggBiasBkgPdf.h"
#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "TMath.h"


Hfitter::MggBiasBkgPdf::MggBiasBkgPdf( const MggBiasBkgPdf& other, const char* name )
   : RooAbsPdf( other, name ),
     m_mgg   ( "mgg",    this, other.m_mgg ),
     m_xi    ( "xi",     this, other.m_xi ),
     m_offset( "offset", this, other.m_offset ),
     m_bias  ( "bias",   this, other.m_bias )
{}

Hfitter::MggBiasBkgPdf::MggBiasBkgPdf( const char* name, const char* title, 
                                       RooAbsReal& x, RooAbsReal& xi, RooAbsReal& offset, RooAbsReal& bias )
   : RooAbsPdf( name, title ),
     m_mgg   ( "mgg",    "Dependent", this, x ),
     m_xi    ( "xi",     "Exponent",  this, xi ),
     m_offset( "offset", "offset",    this, offset ),
     m_bias  ( "bias",   "bias",      this, bias )
{}

Double_t Hfitter::MggBiasBkgPdf::evaluate() const
{
   return TMath::Exp(-m_xi*m_offset)*( TMath::Exp(m_xi*m_mgg) + 
                                       (m_mgg - m_mgg.min())*(m_mgg - m_mgg.max())*m_bias );
}

Int_t Hfitter::MggBiasBkgPdf::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, 
                                                     const char* /*rangeName*/ ) const 
{
   if (matchArgs(allVars,analVars,m_mgg)) return 1;
   return 0;
}

Double_t Hfitter::MggBiasBkgPdf::analyticalIntegral( Int_t code, const char* rangeName ) const 
{
   Double_t mgg1   = m_mgg.min(rangeName);   
   Double_t mgg2   = m_mgg.max(rangeName);
   Double_t mggMin = m_mgg.min();
   Double_t mggMax = m_mgg.max();
   
   switch(code) {
   case 1: 
      if (m_xi == 0.0) return 0;
      return TMath::Exp(-m_xi*m_offset)*( (TMath::Exp( m_xi*mgg2 ) - TMath::Exp( m_xi*mgg1 ))/m_xi +
                                          m_bias*(mgg2 - mgg1)*( -3.0*mggMax*(mgg1 + mgg2) + 
                                                                 2.0*(mgg1*mgg1 + mgg1*mgg2 + mgg2*mgg2) +
                                                                 mggMin*(6.0*mggMax - 3.0*(mgg1 + mgg2) ) )/6.0 );
   }
   
   assert(0);
   return 0;
}
