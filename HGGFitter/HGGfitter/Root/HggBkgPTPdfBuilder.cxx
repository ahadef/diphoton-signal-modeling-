// $Id: HggBkgPTPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgPTPdfBuilder.h"

#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "HfitterModels/HftPolyExp.h"

using namespace Hfitter;

RooAbsPdf* HggBkgPTPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& pT = *Dependent(dependents, "pT", "GeV", "p_{T#gamma#gamma}");

   RooAbsReal& bkgPtM0    = Param(workspace, "bkgPtM0");
   RooAbsReal& bkgPtPow1  = Param(workspace, "bkgPtPow1");
   RooAbsReal& bkgPtExp1  = Param(workspace, "bkgPtExp1");
   RooAbsReal& bkgPtPow2  = Param(workspace, "bkgPtPow2");
   RooAbsReal& bkgPtExp2  = Param(workspace, "bkgPtExp2");
   RooAbsReal& bkgPtPow3  = Param(workspace, "bkgPtPow3");
   RooAbsReal& bkgPtExp3  = Param(workspace, "bkgPtExp3");
   RooAbsReal& bkgPtTC0   = Param(workspace, "bkgPtTC0");
   RooAbsReal& bkgPtTCS   = Param(workspace, "bkgPtTCS");
   RooAbsReal& bkgPtTail1Norm = Param(workspace, "bkgPtTail1Norm");
   RooAbsReal& bkgPtTail2Norm = Param(workspace, "bkgPtTail2Norm");
   RooAbsReal& bkgPtTailTCNorm = Param(workspace, "bkgPtTailTCNorm");

   RooAbsPdf* ptPdf1 = new HftPolyExp(name + "_pT_1", "1st half bkg p_T PDF for H->gamma gamma", 
                                       pT, bkgPtM0, bkgPtPow1, bkgPtExp1);
   RooAbsPdf* ptPdf2 = new HftPolyExp(name + "_pT_2", "2nd half bkg p_T PDF for H->gamma gamma", 
                                       pT, bkgPtM0, bkgPtPow2, bkgPtExp2);
   RooAbsPdf* ptPdf3 = new HftPolyExp(name + "_pT_3", "3rd half bkg p_T PDF for H->gamma gamma", 
                                       pT, bkgPtM0, bkgPtPow3, bkgPtExp3);
   RooAbsPdf* ptPdfTC = new RooGaussian(name + "_pT_TC", "Tail-Catcher bkg p_T PDF for H->gamma gamma", 
                                       pT, bkgPtTC0, bkgPtTCS);

   return new RooAddPdf(name, "Bkg p_T PDF for H->gamma gamma", 
                        RooArgList(*ptPdf2, *ptPdf3, *ptPdfTC, *ptPdf1), 
                        RooArgList(bkgPtTail1Norm, bkgPtTail2Norm, bkgPtTailTCNorm) );
}
