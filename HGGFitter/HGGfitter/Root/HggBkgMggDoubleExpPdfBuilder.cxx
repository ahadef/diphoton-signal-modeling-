// $Id: HggBkgMggDoubleExpPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggDoubleExpPdfBuilder.h"
#include "RooExponential.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggDoubleExpPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  RooAbsReal& xi1  = Param(workspace, m_xi1Name,  "1/GeV");
  RooAbsReal& xi2  = Param(workspace, m_xi2Name,  "1/GeV");
  RooAbsReal& frac = Param(workspace, m_fracName);

  RooExponential* exp1 = new RooExponential(name + ".exp1", "first exponential component", mgg, xi1);
  RooExponential* exp2 = new RooExponential(name + ".exp2", "second exponential component", mgg, xi2);
  
  return new RooAddPdf(name, "Double exponential PDF", RooArgList(*exp1, *exp2), frac);
}
