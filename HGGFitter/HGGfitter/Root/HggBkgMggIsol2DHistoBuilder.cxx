// Author: Marine Kuna

#include "HGGfitter/HggBkgMggIsol2DHistoBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "TFile.h"
#include "TH2F.h"

using namespace Hfitter;


HggBkgMggIsol2DHistoBuilder::HggBkgMggIsol2DHistoBuilder()
{ 
}


RooAbsPdf* HggBkgMggIsol2DHistoBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf  = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);

  
  RooRealVar& isol1 = (RooRealVar&)*dependents.find("isol1");
  RooRealVar& isol2 = (RooRealVar&)*dependents.find("isol2");
  RooArgList argList(isol1,isol2);

  RooAbsReal& IsolationCategory    = Param(workspace, "IsolationCategory");
  std::cout << "Parameters read for category : " << IsolationCategory.getVal() << std::endl;
   
     
    TH2F* ph;
    
    
    TFile *f1 = new TFile("jj_inclusive.root","READ");

 
    ph = (TH2F*)f1->Get("htemp__isolation1_isolation2");
    ph->SetDirectory(0);
    f1->Close();
    
    RooDataHist *dataHist = new RooDataHist("hist", "", argList, ph);
    RooArgSet tmpSet(argList);
    RooHistPdf* isolPdf = new RooHistPdf("isol2DPdf","isol2DPdf", tmpSet, *dataHist);    
    


  return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isolPdf));
  
			
			
}
