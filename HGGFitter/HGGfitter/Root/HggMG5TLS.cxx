#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>
#include "TMath.h"

#include "HGGfitter/HggMG5TLS.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"

using namespace std;

ClassImp(HggMG5TLS)


HggMG5TLS::HggMG5TLS(const char *name, const char *title,
          RooAbsReal& _x, RooAbsReal& _mean,
          RooAbsReal& _width) :
  RooAbsPdf(name,title),
  x("x","Dependent",this,_x),
  mean("mean","Mean",this,_mean),
  width("width","Width",this,_width)//,

{
}



////////////////////////////////////////////////////////////////////////////////

HggMG5TLS::HggMG5TLS(const HggMG5TLS& other, const char* name) : 
  RooAbsPdf(other,name), x("x",this,other.x), mean("mean",this,other.mean),
  width("width",this,other.width)
{
}


////////////////////////////////////////////////////////////////////////////////

Double_t HggMG5TLS::evaluate() const
{

  double pdfweight = pow(1-pow(x/13000,1./3),11.6566) * pow(x/13000,-2.55713) * (2.09254e-6); //pow((1-pow((x/13000),(1./3))),8.95) * pow((x/13000),-4.1) * (-2.95e-10 + 1.32e-7*(x/13000) -1.7e-7*pow((x/13000), 2));

  return pow(x,7)*pdfweight / ((x*x- mean*mean)*(x*x- mean*mean) + (mean*width)*(mean*width));


}



//////////////////////////////////////////////////////////////////////////////////
//
// Int_t HggMG5TLS::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const 
// {
//  if (matchArgs(allVars,analVars,x)) return 1 ;
//  return 0 ;
//}
//
//
//
//////////////////////////////////////////////////////////////////////////////////
//
// Double_t HggMG5TLS::analyticalIntegral(Int_t code, const char* rangeName) const 
// {
//  switch(code) {
//  case 1: 
//    {
//      Double_t c = 2./width;
//      return c*(atan(c*(x.max(rangeName)-mean)) - atan(c*(x.min(rangeName)-mean)));
//    }
//  }
 
//  assert(0) ;
//  return 0 ;
// }

