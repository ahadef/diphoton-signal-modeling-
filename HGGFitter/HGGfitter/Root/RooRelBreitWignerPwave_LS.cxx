#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>
#include "TMath.h"

#include "HGGfitter/RooRelBreitWignerPwave_LS.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"

using namespace std;

ClassImp(RooRelBreitWignerPwave_LS)

  // Move function LSweight insive evaluete, to avoid segmentation violation
  // probably, I do some mistake in defining it
  // To mention - both ways gives the same result, but when LS weight inside evaluate - no crash!
/*
Double_t RooRelBreitWignerPwave_LS::LSweight(Double_t *x, Double_t *par) 
{
		double pdfweight = pow((1-pow((x[0]/13000),(1./3))),8.95) * pow((x[0]/13000),-4.1) * (-2.95e-10 + 1.32e-7*(x[0]/13000) -1.7e-7*pow((x[0]/13000), 2));	

		//double effweight = 0.290136-0.167657*exp(-0.344971*(x[0]-100.0)/100.0);
		Double_t GF = 1.16637E-5;
		Double_t norm = 1./pow(TMath::Pi(),2) * GF /576./sqrt(2) * 9/4;
		par[0]=norm;
		par[1]=173.;
		Float_t mass = x[0];
	
		Float_t tau = 4.*pow(par[1],2)/pow(mass,2); //par[1] is the top mass
		TComplex ftau(0);
		if (tau >= 1.) {
			Float_t Retau = pow(TMath::ASin(1/sqrt(tau)), 2);
			ftau = TComplex(Retau,0);
		} else {
			Float_t ratio = (1 + sqrt(1 - tau)) / (1- sqrt( 1- tau));
			Double_t Retau = log(ratio);
			Double_t Imtau = -TMath::Pi();
			TComplex acomp = TComplex(Retau,Imtau);
			ftau = -1./4 * acomp * acomp;
		}
		//TComplex fact = 1 + (1-tau)*ftau;
		TComplex fact0 =  ((double)(1-tau)) * ftau;
		TComplex fact = 1. + fact0;
		Double_t Lambda = 0.2;
		Double_t alphas = 12 * TMath::Pi()/log(pow((mass/Lambda),2));
		if (tau >= 1.)
			alphas = alphas/(33 - 2*5);
		else
			alphas = alphas/(33 - 2*6);
		return pdfweight*par[0] * pow(alphas, 2) * pow(mass, 3) * pow(tau, 2) * fact.Rho2();
	return 1;
}
*/
////////////////////////////////////////////////////////////////////////////////

RooRelBreitWignerPwave_LS::RooRelBreitWignerPwave_LS(const char *name, const char *title,
          RooAbsReal& _x, RooAbsReal& _mean,
          RooAbsReal& _width) :
  RooAbsPdf(name,title),
  x("x","Dependent",this,_x),
  mean("mean","Mean",this,_mean),
  width("width","Width",this,_width)//,
  //TF_LSweight(TString(name) + "_TF_LSweight",LSweight, 150, 3000,2)

{
}



////////////////////////////////////////////////////////////////////////////////

RooRelBreitWignerPwave_LS::RooRelBreitWignerPwave_LS(const RooRelBreitWignerPwave_LS& other, const char* name) : 
  RooAbsPdf(other,name), x("x",this,other.x), mean("mean",this,other.mean),
  width("width",this,other.width)//, TF_LSweight(other.TF_LSweight)
{
}


////////////////////////////////////////////////////////////////////////////////

Double_t RooRelBreitWignerPwave_LS::evaluate() const
{
  Double_t k = x*x*width/(mean*TMath::Pi()); 

  // Move function LSweight insive evaluete, to avoid segmentation violation
  // probably, I do some mistake in defining it
  // To mention - both ways gives the same result, but when LS weight inside evaluate - no crash!
  Double_t xval = x;
  double pdfweight = pow((1-pow((x/13000),(1./3))),8.95) * pow((x/13000),-4.1) * (-2.95e-10 + 1.32e-7*(x/13000) -1.7e-7*pow((x/13000), 2));
  double Bggweight = 1;//Bgg(xval);
  //double _x[1];
  double par[2];
  
  Double_t GF = 1.16637E-5;
  Double_t norm = 1./pow(TMath::Pi(),2) * GF /576./sqrt(2) * 9/4;
  par[0]=norm;
  par[1]=173.;
  //_x[0]=xval;
    
  Float_t mass = xval ; //_x[0];
    
  Float_t tau = 4.*pow(par[1],2)/pow(mass,2); //par[1] is the top mass
  TComplex ftau(0);
  if (tau >= 1.) {
    Float_t Retau = pow(TMath::ASin(1/sqrt(tau)), 2);
    ftau = TComplex(Retau,0);
  } else {
    Float_t ratio = (1 + sqrt(1 - tau)) / (1- sqrt( 1- tau));
    Double_t Retau = log(ratio);
    Double_t Imtau = -TMath::Pi();
    TComplex acomp = TComplex(Retau,Imtau);
    ftau = -1./4 * acomp * acomp;
  }
  //TComplex fact = 1 + (1-tau)*ftau;
  TComplex fact0 =  ((double)(1-tau)) * ftau;
  TComplex fact = 1. + fact0;
  Double_t Lambda = 0.2;
  Double_t alphas = 12 * TMath::Pi()/log(pow((mass/Lambda),2));
  if (tau >= 1.)
    alphas = alphas/(33 - 2*5);
  else
    alphas = alphas/(33 - 2*6);
  Bggweight = par[0] * pow(alphas, 2) * pow(mass, 3) * pow(tau, 2) * fact.Rho2();

  return k*pdfweight*Bggweight / ((x*x- mean*mean)*(x*x- mean*mean) + (x*x/mean*width)*(x*x/mean*width));

  //return k* (TF_LSweight.Eval(x)) / ((x*x- mean*mean)*(x*x- mean*mean) + (x*x/mean*width)*(x*x/mean*width));

  //Double_t arg= x - mean;  
  //return 1. / (arg*arg + 0.25*width*width);

}



//////////////////////////////////////////////////////////////////////////////////
//
// Int_t RooRelBreitWignerPwave_LS::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const 
// {
//  if (matchArgs(allVars,analVars,x)) return 1 ;
//  return 0 ;
//}
//
//
//
//////////////////////////////////////////////////////////////////////////////////
//
// Double_t RooRelBreitWignerPwave_LS::analyticalIntegral(Int_t code, const char* rangeName) const 
// {
//  switch(code) {
//  case 1: 
//    {
//      Double_t c = 2./width;
//      return c*(atan(c*(x.max(rangeName)-mean)) - atan(c*(x.min(rangeName)-mean)));
//    }
//  }
 
//  assert(0) ;
//  return 0 ;
// }

