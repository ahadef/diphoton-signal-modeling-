// $Id: HggBkgCosThStarPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgCosThStarPdfBuilder.h"

#include "RooGaussian.h"
#include "RooPolynomial.h"
#include "RooAddPdf.h"
#include "HfitterModels/HftPeggedPoly.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


RooAbsPdf* HggBkgCosThStarPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& cosThStar = *Dependent(dependents, "cosThStar", "", "cos #theta*");

  RooAbsReal& bkgCosThCoef1 = Param(workspace, "bkgCosThCoef1");
  RooAbsReal& bkgCosThCoef2 = Param(workspace, "bkgCosThCoef2");
  RooAbsReal& bkgCosThCoef3 = Param(workspace, "bkgCosThCoef3");
  RooAbsReal& bkgCosThCoef4 = Param(workspace, "bkgCosThCoef4");
  RooAbsReal& bkgCosThCoef5 = Param(workspace, "bkgCosThCoef5");
  RooAbsReal& bkgCosThCoef6 = Param(workspace, "bkgCosThCoef6");

  RooAbsReal& bkgCosThPower = Param(workspace, "bkgCosThPower");
  RooAbsReal& bkgCosThPed   = Param(workspace, "bkgCosThPed");

  RooAbsReal& bkgCosThMean1  = Param(workspace, "bkgCosThMean1");
  RooAbsReal& bkgCosThSigma1 = Param(workspace, "bkgCosThSigma1", "GeV", "", 1);

  RooAbsReal& bkgCosThMean2  = Param(workspace, "bkgCosThMean2");
  RooAbsReal& bkgCosThSigma2 = Param(workspace, "bkgCosThSigma2", "GeV", "", 1);
  
  RooAbsReal& bkgCosThRelNorm1 = Param(workspace, "bkgCosThRelNorm1");
  RooAbsReal& bkgCosThRelNorm2 = Param(workspace, "bkgCosThRelNorm2");
  
  RooAbsPdf* csthPdf1 = new RooGaussian(name + "_bump1",
                                         "1st half of bkg cosThStar PDF for H->gamma gamma bump",
                                         cosThStar,
                                         bkgCosThMean1, bkgCosThSigma1 );
  
  RooAbsPdf* csthPdf2 = new RooGaussian(name + "_bump2",
                                         "2st half of bkg cosThStar PDF for H->gamma gamma bump",
                                         cosThStar,
                                         bkgCosThMean2, bkgCosThSigma2 );
  
  RooAbsPdf* csthpoly = new HftPeggedPoly(name + "_poly",
                                           "Bkg cosThStar PDF for H->gamma gamma",
                                           cosThStar,
                                           RooArgList( bkgCosThCoef1, bkgCosThCoef2, bkgCosThCoef3, bkgCosThCoef4, bkgCosThCoef5, bkgCosThCoef6 ),
                                               bkgCosThPower, bkgCosThPed, 0, 1 );
  
   return new RooAddPdf(name, "Bkg cos theta* PDF for H->gamma gamma",
                        RooArgList( *csthPdf1, *csthPdf2, *csthpoly ), 
                        RooArgList( bkgCosThRelNorm1, bkgCosThRelNorm2 ) );
}
