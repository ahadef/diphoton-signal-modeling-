// $Id: HggBkgMggIsolPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggIsolPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

using namespace Hfitter;


HggBkgMggIsolPdfBuilder::HggBkgMggIsolPdfBuilder()
{ 
}


void HggBkgMggIsolPdfBuilder::Setup(bool isGam1, bool isGam2) 
{
  m_isol1PdfBuilder.Setup(isGam1, "isol1", "Leading");
  m_isol2PdfBuilder.Setup(isGam2, "isol2", "Subleading");
}


RooAbsPdf* HggBkgMggIsolPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf  = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);    
  RooAbsPdf* isol1Pdf = m_isol1PdfBuilder.Pdf(name + "_isol1", dependents, workspace);
  RooAbsPdf* isol2Pdf = m_isol2PdfBuilder.Pdf(name + "_isol2", dependents, workspace);

  return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1Pdf, *isol2Pdf));
}
