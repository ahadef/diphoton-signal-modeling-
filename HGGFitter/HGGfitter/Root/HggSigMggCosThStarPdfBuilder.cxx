// $Id: HggSigMggCosThStarPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggSigMggCosThStarPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

RooAbsPdf* HggSigMggCosThStarPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);
  RooAbsPdf* cThPdf = m_cThPdfBuilder.Pdf(name + "_cosThStar", dependents, workspace);

  return new RooProdPdf(name, "Signal PDF for mgg and cosThStar", RooArgSet(*mggPdf, *cThPdf));
}
