// $Id: HftTools.cxx,v 1.34 2009/01/30 16:25:12 hoecker Exp $   
// Author: Mohamed Aharrouche, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggZeeIsolStudy.h"

#include <iostream>
#include "TH1.h"
#include "TFile.h"
#include "TString.h"
#include "TChain.h"
#include "TSystem.h"
#include "TArrayD.h"
#include "TPRegexp.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooCurve.h"
#include "RooHist.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLatex.h"
#include "TROOT.h"
#include "TLegend.h"
#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftDataHist.h"
#include "TreeReader/MultiReader.h"
#include "TreeReader/TVector3Reader.h"

using std::cout;
using std::endl;

using namespace Hfitter;
using namespace TreeReader;


void HggZeeIsolStudy::makeZeeFromEG(const TString& inputFileNames, const TString& treeName, const TString& outputFileName, bool isMC)
{
  cout << "Processing file(s) " << inputFileNames << endl;  
  TChain* chain = new TChain(treeName, "");
  chain->Add(inputFileNames);
  
  // Setup Input
  //////////////
  
  MultiReader input;
  
  BranchReader<int>* RunNumber = input.AddInt("RunNumber");
  BranchReader<int>* LB = input.AddInt("LB");
  BranchReader<int>* EventNumber = input.AddInt("EventNumber");
  BranchReader<double>* weight = (isMC ? input.AddDouble("weight") : 0);

  BranchReader<float>* dy_el1_isol_track = input.AddFloat("dy_el1_isol_track");
  BranchReader<float>* dy_el2_isol_track = input.AddFloat("dy_el2_isol_track");
  BranchReader<float>* dy_el1_isol_calo  = input.AddFloat("dy_el1_isol_calo");
  BranchReader<float>* dy_el2_isol_calo  = input.AddFloat("dy_el2_isol_calo");

  BranchReader<int>* dy_el1_isTight      = input.AddInt("dy_el1_isTight");
  BranchReader<int>* dy_el2_isTight      = input.AddInt("dy_el2_isTight");

  TVector3Reader<PtEtaPhi> el1_p3("dy_el1_pt", "dy_el1_eta", "dy_el1_phi", &input);
  TVector3Reader<PtEtaPhi> el2_p3("dy_el2_pt", "dy_el2_eta", "dy_el2_phi", &input);

  input.ReadFrom(chain);
  
  // Setup Output
  //////////////

  MultiReader output;
  
  output.AddInt("RunNumber");
  output.AddInt("LB");
  output.AddInt("EventNumber");
  output.AddDouble("weight");

  output.AddFloat("el1_isol_calo");
  output.AddFloat("el2_isol_calo");

  output.AddFloat("el1_isol_track");
  output.AddFloat("el2_isol_track");

  output.AddBool("el1_isTight");
  output.AddBool("el2_isTight");

  output.Add("el1_p4", new BasicReader<TLorentzVector>("el1_p4"));
  output.Add("el2_p4", new BasicReader<TLorentzVector>("el2_p4"));

  output.AddFloat("mee");
  
  TFile* outputFile = TFile::Open(outputFileName, "RECREATE");
  TTree* outputTree = new TTree("tree", "");
  output.WriteTo(outputTree);

  for (unsigned int k = 0; k < chain->GetEntries(); k++) {
    if (k % 10000 == 0) cout << "Processing entry " << k << " of " << chain->GetEntries() << endl;
    chain->GetEntry(k);

    output.Int("RunNumber")->SetValue(RunNumber->GetValue());
    output.Int("LB")->SetValue(LB->GetValue());
    output.Int("EventNumber")->SetValue(EventNumber->GetValue());
    output.Double("weight")->SetValue(isMC ? weight->GetValue() : 1);

    output.Float("el1_isol_calo")->SetValue(dy_el1_isol_calo->GetValue());
    output.Float("el2_isol_calo")->SetValue(dy_el2_isol_calo->GetValue());
    output.Float("el1_isol_track")->SetValue(dy_el1_isol_track->GetValue());
    output.Float("el2_isol_track")->SetValue(dy_el2_isol_track->GetValue());

    output.Bool("el1_isTight")->SetValue(dy_el1_isTight->GetValue());
    output.Bool("el2_isTight")->SetValue(dy_el2_isTight->GetValue());

    TLorentzVector el1_p4(el1_p3(), el1_p3().Mag());
    TLorentzVector el2_p4(el2_p3(), el2_p3().Mag());

    output.FindReader< BasicReader<TLorentzVector> >("el1_p4")->SetValue(el1_p4);
    output.FindReader< BasicReader<TLorentzVector> >("el2_p4")->SetValue(el2_p4);

    output.Float("mee")->SetValue((el1_p4 + el2_p4).M());
    outputTree->Fill();
  }

  outputFile->cd();
  outputTree->Write();
  delete outputFile;
}


void HggZeeIsolStudy::makeZeeHists(const TString& inputFileName, const TString& treeName, const TString& outputFileName, 
                                   unsigned int massBins, double massLo, double massHi,
                                   unsigned int isolBins, double isolLo, double isolHi,
                                   const std::vector<double>& bins, double mMin, double mMax, bool isMC)
{
  cout << "Processing file " << inputFileName << endl;  
  TFile *_file0 = TFile::Open(inputFileName);
  TTree* inputTree = (TTree*)_file0->Get(treeName);
  
  // Setup Input
  //////////////
  
  MultiReader input;
  
  BranchReader<double>* weight = (isMC ? input.AddDouble("weight") : 0);
  BasicReader<bool>* el1_isTight      = input.AddBool("el1_isTight");
  BasicReader<bool>* el2_isTight      = input.AddBool("el2_isTight");

  BranchReader<float>* mee = input.AddFloat("mee");

  BranchReader<float>* el1_isol_track = input.AddFloat("el1_isol_track");
  //BranchReader<float>* el2_isol_track = input.AddFloat("el2_isol_track");
  BranchReader<float>* el1_isol_calo  = input.AddFloat("el1_isol_calo");
  //BranchReader<float>* el2_isol_calo  = input.AddFloat("el2_isol_calo");
  
  input.ReadFrom(inputTree);

  TLorentzVector* el1_p4 = 0, *el2_p4 = 0;
  inputTree->SetBranchAddress("el1_p4", &el1_p4);
  inputTree->SetBranchAddress("el2_p4", &el2_p4);
  
  // Setup Output
  //////////////

  TFile* outFile = TFile::Open(outputFileName, "RECREATE");
  std::vector<TH1D*> isolCalHists, isolTrkHists, isolCalPeakHists, isolCalTailHists, isolTrkPeakHists, isolTrkTailHists, meeHists;

  TH1D* pt1Hist = new TH1D("ptHist", "", 500, 0, 500);
  TH1D* meeHist = new TH1D("meeHist", "", massBins, massLo, massHi);
  TH1D* isolCalHist = new TH1D("isolCalHist", "", isolBins, isolLo, isolHi);
  TH1D* isolTrkHist = new TH1D("isolTrkHist", "", isolBins, isolLo, isolHi);

  for (unsigned int i = 0; i < bins.size(); i++) {
    TString ptMax = (i == bins.size() - 1 ? "inf" : Form("%.0f", bins[i+1])); 
    TH1D* isolCalHist = new TH1D(Form("isolCal%d", i), 
                              Form("Calo Isolation, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              isolBins, isolLo, isolHi);
    TH1D* isolTrkHist = new TH1D(Form("isolTrk%d", i), 
                              Form("Track Isolation, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              isolBins, isolLo, isolHi);
    TH1D* peakCalHist = new TH1D(Form("isolCalPeak%d", i), 
                              Form("Peak region calo isolation, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              isolBins, isolLo, isolHi);
    TH1D* peakTrkHist = new TH1D(Form("isolTrkPeak%d", i), 
                              Form("Peak region track isolation, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              isolBins, isolLo, isolHi);
    TH1D* tailCalHist = new TH1D(Form("isolCalTail%d", i), 
                              Form("Tail region calo isolation, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              isolBins, isolLo, isolHi);
    TH1D* tailTrkHist = new TH1D(Form("isolTrkTail%d", i), 
                              Form("Tail region track isolation, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              isolBins, isolLo, isolHi);
    TH1D* allHist = new TH1D(Form("mee%d", i), Form("mee, pt= %.0f - %s GeV", bins[i], ptMax.Data()),
                              massBins, massLo, massHi);
    isolCalHists.push_back(isolCalHist);
    isolTrkHists.push_back(isolTrkHist);
    isolCalPeakHists.push_back(peakCalHist);
    isolCalTailHists.push_back(tailCalHist);
    isolTrkPeakHists.push_back(peakTrkHist);
    isolTrkTailHists.push_back(tailTrkHist);
    meeHists.push_back(allHist);
  }

  for (unsigned int k = 0; k < inputTree->GetEntries(); k++) {
    if (k % 500000 == 0) cout << "Processing entry " << k << " of " << inputTree->GetEntries() << endl;
    inputTree->GetEntry(k);
    if (mee->GetValue() < massLo) continue;
    if (mee->GetValue() > massHi) continue;
    if (el1_p4->Pt() < 30) continue;
    if (el2_p4->Pt() < 30) continue;
    if (!el1_isTight->GetValue()) continue;
    if (!el2_isTight->GetValue()) continue;
 
    pt1Hist->Fill(el1_p4->Pt(), isMC ? weight->GetValue() : 1);
    meeHist->Fill(mee->GetValue(), isMC ? weight->GetValue() : 1);
    isolCalHist->Fill(el1_isol_calo->GetValue(),  isMC ? weight->GetValue() : 1);
    isolTrkHist->Fill(el1_isol_track->GetValue(), isMC ? weight->GetValue() : 1);

    unsigned int ptBin = 0;
    for (unsigned int i = 0; i < meeHists.size(); i++) {
      if (el1_p4->Pt() < bins[i]) break;
      ptBin = i;
    }
    isolCalHists[ptBin]->Fill(el1_isol_calo->GetValue(),  isMC ? weight->GetValue() : 1);
    isolTrkHists[ptBin]->Fill(el1_isol_track->GetValue(), isMC ? weight->GetValue() : 1);
    if (mee->GetValue() < mMin || mee->GetValue() > mMax) {
      isolCalTailHists[ptBin]->Fill(el1_isol_calo->GetValue(),  isMC ? weight->GetValue() : 1);
      isolTrkTailHists[ptBin]->Fill(el1_isol_track->GetValue(), isMC ? weight->GetValue() : 1);
    }
    else {
      isolCalPeakHists[ptBin]->Fill(el1_isol_calo->GetValue(),  isMC ? weight->GetValue() : 1);
      isolTrkPeakHists[ptBin]->Fill(el1_isol_track->GetValue(), isMC ? weight->GetValue() : 1);
    }
    meeHists[ptBin]->Fill(mee->GetValue(), isMC ? weight->GetValue() : 1);
  }

  outFile->cd();
  pt1Hist->Write();
  meeHist->Write();
  isolCalHist->Write();
  isolTrkHist->Write();
  for (unsigned int i = 0; i < meeHists.size(); i++) {
    isolCalHists[i]->Write();
    isolCalPeakHists[i]->Write();
    isolCalTailHists[i]->Write();
    isolTrkHists[i]->Write();
    isolTrkPeakHists[i]->Write();
    isolTrkTailHists[i]->Write();
    meeHists[i]->Write();
  }
  delete outFile;
}


bool HggZeeIsolStudy::analyzeBin(const TString& datacard, const TString& dtName, const TString& mcName, const TString& suffix, 
                                 double pt, double peakLo, double peakHi, const TString& outputName, double ptSlope)
{
  // Setup fit
  HftModel* zee = HftModelBuilder::Create("zee", datacard);
  HftParameterStorage state;
  zee->SaveState(state);
  HftDataHist* mcHist = HftDataHist::Open(mcName, "mee" + suffix, *zee);
  HftDataHist* dtHist = HftDataHist::Open(dtName, "mee" + suffix, *zee);
  
  // MC fit
  zee->ComponentModel(0)->Fit(*mcHist, "zee.dat");
  RooPlot* plotMC = zee->ComponentModel(0)->Plot("mee", *mcHist->DataHist());
  plotMC->SetName("plotMC");
  plotMC->SetTitle("Z->ee mass distribution, PowHeg+Pythia MC");
  // Data fit
  zee->LoadState(state, false, true);
  zee->FreeParameters().Print("V");
  zee->FitData(*dtHist->DataHist());
  RooPlot* plotDT = zee->Plot("mee", *dtHist->DataHist());
  plotDT->SetName("plotDT");
  plotDT->SetTitle("Z->ee mass distribution, data");

  // Compute coefficients
  RooRealVar* mee = zee->Var("mee");
  RooRealVar* nS = zee->Var("nSignal");
  // Don't use nB in the formula below since it cancels out from the final result and can be ==0
  //RooRealVar* nB = zee->Var("nBackground");
  mee->setRange("peak", peakLo, peakHi);
  double nSP = nS->getVal()*zee->ComponentModel(0)->Pdf()->createIntegral(*mee, *mee, "peak")->getVal();
  double nST = nS->getVal() - nSP;
  double nBP = zee->ComponentModel(1)->Pdf()->createIntegral(*mee, *mee, "peak")->getVal();
  double nBT = 1 - nBP;
  TMatrix nMat(2,2);
  nMat(0,0) = nSP;
  nMat(0,1) = nBP;
  nMat(1,0) = nST;
  nMat(1,1) = nBT;
  nMat.Print();
  nMat.Invert();
  nMat.Print();
  // Load histograms
  TFile* fDT = TFile::Open(dtName);
  TH1D* isolCalPeak = (TH1D*)fDT->Get("isolCalPeak" + suffix);
  isolCalPeak->SetName("isolCalPeak");
  TH1D* isolTrkPeak = (TH1D*)fDT->Get("isolTrkPeak" + suffix);
  isolTrkPeak->SetName("isolTrkPeak");
  TH1D* isolCalTail = (TH1D*)fDT->Get("isolCalTail" + suffix);
  isolCalTail->SetName("isolCalTail");
  TH1D* isolTrkTail = (TH1D*)fDT->Get("isolTrkTail" + suffix);
  isolTrkTail->SetName("isolTrkTail");
  TFile* fMC = TFile::Open(mcName);
  TH1D* isolCalMC = (TH1D*)fMC->Get("isolCalPeak" + suffix);
  isolCalMC->SetName("isolCalMC");
  TH1D* isolTrkMC = (TH1D*)fMC->Get("isolTrkPeak" + suffix);
  isolTrkMC->SetName("isolTrkMC");

  // Compute signal hist
  isolCalPeak->Sumw2();
  isolTrkPeak->Sumw2();
  isolCalTail->Sumw2();
  isolTrkTail->Sumw2();

  TH1D* isolCalSig = (TH1D*)isolCalPeak->Clone();
  isolCalSig->SetName("isolCalSig");
  isolCalSig->SetTitle("Subtracted " + TString(isolCalSig->GetTitle())(10, 99));
  isolCalSig->Reset();
  isolCalSig->Add(isolCalPeak, nMat(0,0));
  isolCalSig->Add(isolCalTail, nMat(0,1));
  isolCalSig->Scale(nSP);

  TH1D* isolTrkSig = (TH1D*)isolTrkPeak->Clone();
  isolTrkSig->SetName("isolTrkSig");
  isolTrkSig->SetTitle("Subtracted " + TString(isolTrkSig->GetTitle())(10, 99));
  isolTrkSig->Reset();
  isolTrkSig->Add(isolTrkPeak, nMat(0,0));
  isolTrkSig->Add(isolTrkTail, nMat(0,1));
  isolTrkSig->Scale(nSP);
  
  //int cutBin = isolSig->FindBin(6.0) - 1;
  int cutBin = isolCalSig->FindBin(6.0 + ptSlope*(pt-60)) - 1;
  
  double intCalMC = isolCalMC->Integral();
  double effCalMC = isolCalMC->Integral(0, cutBin)/intCalMC;
  double errCalMC = sqrt(effCalMC*(1-effCalMC))/sqrt(intCalMC);
  
  double intCalDT = isolCalSig->Integral();
  double effCalDT = isolCalSig->Integral(0, cutBin)/intCalDT;
  double errCalDT = sqrt(effCalDT*(1-effCalDT))/sqrt(intCalDT);
  
  double intCalPK = isolCalPeak->Integral();
  double effCalPK = isolCalPeak->Integral(0, cutBin)/intCalPK;
  double errCalPK = sqrt(effCalPK*(1-effCalPK))/sqrt(intCalPK);

  double intTrkMC = isolTrkMC->Integral();
  double effTrkMC = isolTrkMC->Integral(0, cutBin)/intTrkMC;
  double errTrkMC = sqrt(effTrkMC*(1-effTrkMC))/sqrt(intTrkMC);

  double intTrkDT = isolTrkSig->Integral();
  double effTrkDT = isolTrkSig->Integral(0, cutBin)/intTrkDT;
  double errTrkDT = sqrt(effTrkDT*(1-effTrkDT))/sqrt(intTrkDT);
  
  double intTrkPK = isolTrkPeak->Integral();
  double effTrkPK = isolTrkPeak->Integral(0, cutBin)/intTrkPK;
  double errTrkPK = sqrt(effTrkPK*(1-effTrkPK))/sqrt(intTrkPK);
  
  TH1D* results = new TH1D("results", "", 6, 0, 1);
  results->SetBinContent(1, effCalMC);
  results->SetBinError  (1, errCalMC);
  results->SetBinContent(2, effCalDT);
  results->SetBinError  (2, errCalDT);
  results->SetBinContent(3, effCalPK);
  results->SetBinError  (4, errCalPK);
  results->SetBinContent(4, effTrkMC);
  results->SetBinError  (4, errTrkMC);
  results->SetBinContent(5, effTrkDT);
  results->SetBinError  (5, errTrkDT);
  results->SetBinContent(6, effTrkPK);
  results->SetBinError  (6, errTrkPK);
  
  cout << "cal = " << effCalMC << " +/- " << errCalMC << ", " << effCalDT << " +/- " << errCalDT << endl;
  cout << "trk = " << effTrkMC << " +/- " << errTrkMC << ", " << effTrkDT << " +/- " << errTrkDT << endl;

  // setup output
  TFile* outputFile = TFile::Open(outputName, "RECREATE");
  plotMC->Write();
  plotDT->Write();
  isolCalPeak->Write();
  isolCalTail->Write();
  isolCalSig->Write();  
  isolCalMC->Write();
  isolTrkPeak->Write();
  isolTrkTail->Write();
  isolTrkSig->Write();  
  isolTrkMC->Write();
  results->Write();
  
  delete outputFile;
  return true;
}


bool HggZeeIsolStudy::makePlots(const TString& fileRoot, const std::vector<double>& bins, const TString& outputRoot, 
                                double effMin, bool doTrk, double err1, double err2, double err3, double err4)
{
  double binArray[99];
  for (unsigned int i = 0; i < bins.size(); i++) binArray[i] = bins[i];
  TH1D* effDT = new TH1D("effDT", "", bins.size() - 1, binArray); effDT->SetLineColor(1);
  TH1D* effMC = new TH1D("effMC", "", bins.size() - 1, binArray); effMC->SetLineColor(4);
  TH1D* effPK = new TH1D("effPK", "", bins.size() - 1, binArray); effPK->SetLineColor(2);
  TH1D* effSy = new TH1D("effSy", "", bins.size() - 1, binArray); effSy->SetFillColor(5);

  TString spec = (doTrk ? "Trk" : "Cal");
  int offset = (doTrk ? 4 : 1);
  
  TText txt;
  txt.SetNDC();
  txt.SetTextSize(0.08);

  std::vector<TH1D*> isolHistsData, isolHistsMC;
  
  for (unsigned int i = 0; i < bins.size() - 1; i++) {
    cout << "Processing bin " << i << endl;
    TCanvas* c1 = new TCanvas(TString("canvas_")  + Form("%d", i), Form("Plots for bin %d", i),  800, 800);
    TCanvas* cS = new TCanvas(TString("canvas_S") + Form("%d", i), Form("SigPDF for bin %d", i), 600, 600);
    TCanvas* cF = new TCanvas(TString("canvas_F") + Form("%d", i), Form("TFit for bin %d", i), 600, 600);
    c1->Divide(2,2);
    TFile* f = TFile::Open(fileRoot + Form("%d", i) + ".root");
    TH1D* isolPeak = (TH1D*)f->Get("isol" + spec + "Peak");
    TH1D* isolTail = (TH1D*)f->Get("isol" + spec + "Tail");
    TH1D* isolSig  = (TH1D*)f->Get("isol" + spec + "Sig");
    TH1D* isolMC   = (TH1D*)f->Get("isol" + spec + "MC");
    TH1D* results  = (TH1D*)f->Get("results");
    RooPlot* plotMC = (RooPlot*)f->Get("plotMC");
    RooPlot* plotDT = (RooPlot*)f->Get("plotDT");
    isolSig->SetMinimum(0.5);
    
    c1->cd(1)->SetLogy();
    isolPeak->Rebin(5);
    isolPeak->GetXaxis()->SetTitle("Isolation energy [GeV]");
    isolPeak->GetYaxis()->SetTitle("Nomalized Events");
    isolPeak->Draw();
    txt.DrawText(0.4, 0.7, "Peak Region");
    c1->cd(2)->SetLogy();
    isolTail->Rebin(5);
    isolTail->GetXaxis()->SetTitle("Isolation energy [GeV]");
    isolTail->GetYaxis()->SetTitle("Nomalized Events");
    isolTail->Draw();
    txt.DrawText(0.4, 0.7, "Sideband");
    c1->cd(3)->SetLogy();
    isolSig->Rebin(5);
    isolSig->GetXaxis()->SetTitle("Isolation energy [GeV]");
    isolSig->GetYaxis()->SetTitle("Nomalized Events");
    isolSig->Draw();
    txt.DrawText(0.4, 0.7, "Signal, Data");
    c1->cd(4)->SetLogy();
    isolMC->Rebin(5);
    isolMC->GetXaxis()->SetTitle("Isolation energy [GeV]");
    isolMC->GetYaxis()->SetTitle("Nomalized Events");
    isolMC->Draw();
    txt.DrawText(0.4, 0.7, "Signal, MC");

    cS->cd()->SetLogy();
    plotMC->Draw();
    txt.DrawText(0.3, 0.2, "Signal template");
    cF->cd()->SetLogy();
    plotDT->Draw();
    txt.DrawText(0.6, 0.2, "Data Fit");

    double err = err1;
    if (effSy->GetBinCenter(i + 1) > 350) err = err4;
    else if (effSy->GetBinCenter(i + 1) > 250) err = err3;
    else if (effSy->GetBinCenter(i + 1) > 125) err = err2;
    
    effMC->SetBinContent(i + 1, results->GetBinContent(0 + offset));
    effMC->SetBinError  (i + 1, results->GetBinError  (0 + offset));
    effDT->SetBinContent(i + 1, results->GetBinContent(1 + offset));
    effDT->SetBinError  (i + 1, results->GetBinError  (1 + offset));
    effPK->SetBinContent(i + 1, results->GetBinContent(2 + offset));
    effPK->SetBinError  (i + 1, results->GetBinError  (2 + offset));
    effSy->SetBinContent(i + 1, results->GetBinContent(1 + offset));
    effSy->SetBinError  (i + 1, err);
    c1->Print(outputRoot + spec + Form("_bin%d.gif", i));
    c1->Print(outputRoot + spec + Form("_bin%d.eps", i));
    cS->Print(outputRoot + spec + Form("_sigPDF_bin%d.gif", i));
    cS->Print(outputRoot + spec + Form("_sigPDF_bin%d.eps", i));
    cF->Print(outputRoot + spec + Form("_fit_bin%d.gif", i));
    cF->Print(outputRoot + spec + Form("_fit_bin%d.eps", i));
    
    isolHistsData.push_back(isolSig);
    isolHistsMC.push_back(isolMC);
  }
  TCanvas* cC = new TCanvas("canvas_comp", "Bin comparison", 1200, 800);
  cC->Divide(bins.size() - 1);
  for (unsigned int i = 0; i < bins.size() - 1; i++) {
    cC->cd(i+1)->SetLogy();
    isolHistsMC[i]->SetLineColor(2);
    isolHistsData[i]->SetLineColor(4);
    isolHistsMC[i]->GetXaxis()->SetRangeUser(-5,15);
    isolHistsData[i]->Scale(isolHistsMC[i]->Integral()/isolHistsData[i]->Integral());
    isolHistsMC[i]->Draw("H");
    isolHistsData[i]->Draw("HSAME");
  }
  cC->Print(outputRoot + spec + "_comp.gif");
  cC->Print(outputRoot + spec + "_comp.eps");

  
  TCanvas* c2 = new TCanvas("canvas_eff", "", 800, 600);
  effSy->GetXaxis()->SetTitle("p_{T}^{leading} [GeV]");
  effSy->GetYaxis()->SetTitle("Isolation cut efficiency");
  effSy->SetMaximum(1.02);
  if (err1 > 0) {
    if (effMin > 0) effSy->SetMinimum(effMin);
    effSy->Draw("E3");
    effMC->Draw("HSAME");
  }
  else {
    if (effMin > 0) effMC->SetMinimum(effMin);
    effMC->Draw("H");
  }
  effDT->Draw("HSAME");
  effPK->Draw("HSAME");
  TLegend* legend = new TLegend(0.15, 0.4, 0.6, 0.15, "","NDC"); 
  legend->SetBorderSize(0); legend->SetFillColor(0);
  legend->AddEntry(effMC, "Efficiency in MC");
  legend->AddEntry(effDT, "Efficiency in data");
  legend->AddEntry(effPK, "Efficiency in data, no bkg subtr.");
  if (err1 > 0) legend->AddEntry(effSy, "Systematic error");
  legend->Draw();
  c2->Print(outputRoot + spec + "_eff.gif");
  c2->Print(outputRoot + spec + "_eff.eps");
  return true;
}
