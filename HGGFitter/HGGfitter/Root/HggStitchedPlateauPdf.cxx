#include "HGGfitter/HggStitchedPlateauPdf.h"

#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "RooMath.h"
#include "TMath.h"

#include <Math/SpecFuncMathCore.h>
#include <Math/PdfFuncMathCore.h>
#include <Math/ProbFuncMathCore.h>

using namespace Hfitter;
using std::cout;
using std::endl;


HggStitchedPlateauPdf::HggStitchedPlateauPdf(const char* name, const char *title, RooAbsReal& _x, RooAbsReal& _x0,  
                                             RooAbsReal& _edge,  RooAbsReal& _sigma) :
  RooAbsPdf(name,title),
  x("x", "Observable", this, _x),
  x0("x0", "offset", this, _x0),
  edge("edge", "edge position", this, _edge),
  sigma("sigma", "edge width", this, _sigma)
{
}


HggStitchedPlateauPdf::HggStitchedPlateauPdf(const HggStitchedPlateauPdf& other, const char* name) : 
  RooAbsPdf(other, name), 
  x("x", this, other.x), 
  x0("x0", this, other.x0), 
  edge("edge", this, other.edge),
  sigma("sigma", this, other.sigma)
{
}


Double_t HggStitchedPlateauPdf::evaluate() const
{
  if (sigma <= 0) return 0;
  double xRel = x - x0;
  double arg = 0;
  if (xRel < -edge) arg = (xRel + edge)/sigma;
  if (xRel >  edge) arg = (xRel - edge)/sigma;
  return TMath::Exp(-0.5*TMath::Power(arg, 2));
}


Int_t HggStitchedPlateauPdf::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const 
{
  if (matchArgs(allVars,analVars,x)) return 1 ;
  return 0 ;
}


Double_t HggStitchedPlateauPdf::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code==1) ;
  return integral(x.min(rangeName), x.max(rangeName));
}


double HggStitchedPlateauPdf::integralFromMinusInf(double x2) const
{
  double k = sigma*TMath::Sqrt(2*TMath::Pi());
  double xRel = x2 - x0;
  if (xRel < -edge) return k*ROOT::Math::gaussian_cdf((xRel + edge)/sigma);
  if (xRel <  edge) return 0.5*k + (xRel + edge);
  return 2*edge + k*ROOT::Math::gaussian_cdf((xRel - edge)/sigma);
}


double HggStitchedPlateauPdf::integral(double x1, double x2) const
{
  return integralFromMinusInf(x2) - integralFromMinusInf(x1);
}
