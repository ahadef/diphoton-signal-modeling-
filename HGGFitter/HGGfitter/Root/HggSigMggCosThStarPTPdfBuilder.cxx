// $Id: HggSigMggCosThStarPTPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggSigMggCosThStarPTPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

using namespace Hfitter;

RooAbsPdf* HggSigMggCosThStarPTPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);
  RooAbsPdf* cosThStarPdf = m_cosThStarPdfBuilder.Pdf(name + "_cosThStar", dependents, workspace);
  RooAbsPdf* pTPdf = m_pTPdfBuilder.Pdf(name + "_pT", dependents, workspace);

  return new RooProdPdf(name, "Signal PDF for mgg,cosThStar and pT", 
			RooArgSet(*mggPdf, *cosThStarPdf, *pTPdf));
}
