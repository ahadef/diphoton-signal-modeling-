#include "HGGfitter/HggLognormal.h"

#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "RooMath.h"
#include "TMath.h"

#include <Math/SpecFuncMathCore.h>
#include <Math/PdfFuncMathCore.h>
#include <Math/ProbFuncMathCore.h>

using namespace Hfitter;
using std::cout;
using std::endl;


//_____________________________________________________________________________
HggLognormal::HggLognormal(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _expm, 
                           RooAbsReal& _sigmaDn, RooAbsReal& _sigmaUp, bool useModifiedSigma) :
  RooAbsPdf(name,title),
  x("x", "Observable", this, _x),
  expm("x0", "expm", this, _expm),
  sigmaDn("sigmaDn", "low-side sigma", this, _sigmaDn),
  sigmaUp("sigmaUp", "high-side sigma", this, _sigmaUp),
  m_useModifiedSigma(useModifiedSigma)
{
}


//_____________________________________________________________________________
HggLognormal::HggLognormal(const char *name, const char *title, RooAbsReal& _x, RooAbsReal& _expm, 
                           RooAbsReal& _sigma, bool useModifiedSigma) :
  RooAbsPdf(name,title),
  x("x", "Observable", this, _x),
  expm("x0", "expm", this, _expm),
  sigmaDn("sigmaDn", "low-side sigma", this, _sigma),
  sigmaUp("sigmaUp", "high-side sigma", this, _sigma),
  m_useModifiedSigma(useModifiedSigma)
{
}


//_____________________________________________________________________________
HggLognormal::HggLognormal(const HggLognormal& other, const char* name) : 
  RooAbsPdf(other, name), x("x", this, other.x), expm("expm", this, other.expm), 
  sigmaDn("sigmaDn", this, other.sigmaDn), sigmaUp("sigmaUp", this, other.sigmaUp),
  m_useModifiedSigma(other.m_useModifiedSigma)
{
}

double HggLognormal::Kappa(double sigma)
{
  return TMath::Sqrt(TMath::Log(1 + sigma*sigma));
}


double HggLognormal::Sigma(double kappa)
{
  return TMath::Sqrt(TMath::Exp(kappa*kappa) - 1);
}


double HggLognormal::kappaDn() const
{
  if (!m_useModifiedSigma) return sigmaDn;
  return Kappa(sigmaDn);
}


double HggLognormal::kappaUp() const
{
  if (!m_useModifiedSigma) return sigmaUp;
  return Kappa(sigmaUp);
}


//_____________________________________________________________________________
Double_t HggLognormal::evaluate() const
{
  if (x == 0) return 0;
  double kappa =  x < expm ? kappaDn() : kappaUp();
  return TMath::Exp(-0.5*TMath::Power(TMath::Log(x/expm)/kappa, 2))/x;
}



//_____________________________________________________________________________
Int_t HggLognormal::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const 
{
  if (matchArgs(allVars,analVars,x)) return 1 ;
  return 0 ;
}



//_____________________________________________________________________________
Double_t HggLognormal::analyticalIntegral(Int_t code, const char* rangeName) const 
{
  assert(code==1) ;
  return integral(x.min(rangeName), x.max(rangeName));
}


double HggLognormal::integral(double xMin, double xMax) const
{
  static double k = TMath::Sqrt(2*TMath::Pi());
  double kappa1 = (xMin < expm ? kappaDn() : kappaUp());
  double kappa2 = (xMax < expm ? kappaDn() : kappaUp());

  return k*(+kappa2*(ROOT::Math::gaussian_cdf(TMath::Log(xMax/expm)/kappa2) - 0.5) 
            -kappa1*(ROOT::Math::gaussian_cdf(TMath::Log(xMin/expm)/kappa1) - 0.5));
}



//_____________________________________________________________________________
Int_t HggLognormal::getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t /*staticInitOK*/) const
{
  if (matchArgs(directVars,generateVars,x)) return 1 ;  
  return 0 ;
}



//_____________________________________________________________________________
void HggLognormal::generateEvent(Int_t code)
{
  assert(code==1) ;
  
  if (kappaUp() == 0 || kappaDn() == 0 || expm <= 0) {
    x = 0;
    return;
  }
  
  // this is the ratio of negatives-side integral to total integral, to draw which side to compute 
  double ratio = TMath::Abs(kappaDn())/(TMath::Abs(kappaUp()) + TMath::Abs(kappaDn()));
  double xm = TMath::Log(expm);

  Double_t xgen ;
  while (1) {
    int sign = (RooRandom::randomGenerator()->Uniform() < ratio ? -1 : 1);
    double gaussian = RooRandom::randomGenerator()->Gaus(xm, (sign == -1 ? kappaDn() : kappaUp()));
    xgen = TMath::Exp(xm + sign*TMath::Abs(gaussian - xm)); // make sure we generate on the correct side;
    if (xgen <= x.max() && xgen >= x.min()) {
      x = xgen ;
      break;
    }
  }

  return;
}
