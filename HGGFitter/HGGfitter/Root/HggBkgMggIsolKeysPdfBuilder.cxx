//Author : Marine Kuna

#include "HGGfitter/HggBkgMggIsolKeysPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooNDKeysPdf.h"
#include "RooHistPdf.h"

#include "TChain.h"
#include "TH2F.h"
#include "TH3F.h"
#include "TFile.h"

using namespace Hfitter;


HggBkgMggIsolKeysPdfBuilder::HggBkgMggIsolKeysPdfBuilder()
{ 
}

/*
void HggBkgMggIsolKeysPdfBuilder::Setup(bool isGam1, bool isGam2) 
{
  m_isol1PdfBuilder.Setup(isGam1, "isol1");
  m_isol2PdfBuilder.Setup(isGam2, "isol2");
}
*/

RooAbsPdf* HggBkgMggIsolKeysPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf  = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);    
  /*RooAbsPdf* isol1Pdf = m_isol1PdfBuilder.Pdf(name + "_isol1", dependents, workspace);
  RooAbsPdf* isol2Pdf = m_isol2PdfBuilder.Pdf(name + "_isol2", dependents, workspace);

  return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, *isol1Pdf, *isol2Pdf));*/
			
 // RooRealVar& mgg = (RooRealVar&)*dependents.find("mgg");
  RooRealVar& isol1 = (RooRealVar&)*dependents.find("isol1");
  RooRealVar& isol2 = (RooRealVar&)*dependents.find("isol2");
  //RooRealVar weight("weight","weight",0.,1000.);
  RooRealVar weight("weight","weight",0.,1.);
			

  //RooDataSet bkgDataSet("bkgDataSet","",RooArgSet(mgg,isol1, isol2,weight),"weight");
  RooDataSet bkgDataSet("bkgDataSet","",RooArgSet(isol1, isol2,weight),"weight");
			
  if(m_fName == ""){
    std::cout << "Need to pass name of ntuple for building the 2d bkg keys Pdf or for reading histogram." << std::endl;
    assert(0);
  } else{
    if(m_fMode==0){
      std::cout << "Building the 2d bkg keys Pdf from " << m_fName << std::endl;
    }else{
      std::cout << "Reading the 2d histogram from " << m_fName << std::endl;
    }
  }
  
  TH2F* ph2;
  RooArgList argList(isol1,isol2);
 
   if(m_fMode==0){//Building the keys Pdf from a tree
  TChain* chain = new TChain("tree2011_dijet");
    chain->Add(m_fName);
    
    //double dmgg, disol1, disol2, dweight;    
    double  disol1, disol2, dweight;    
    //chain->SetBranchAddress("mgg_dijet",&dmgg);
    chain->SetBranchAddress("isol1_dijet",&disol1);
    chain->SetBranchAddress("isol2_dijet",&disol2);
    chain->SetBranchAddress("weight_dijet",&dweight);
    
    int nentries = chain->GetEntries();
    //    nentries = 10;
    for(int i=0; i<nentries; i++){
      chain->GetEntry(i);
      //mgg.setVal(dmgg);
      isol1.setVal(disol1);
      isol2.setVal(disol2);
      weight.setVal(dweight);
      //bkgDataSet.add(RooArgSet(mgg,isol1,isol2,weight),dweight);
      bkgDataSet.add(RooArgSet(isol1,isol2,weight),dweight);
    }
        
    //RooNDKeysPdf* keysMggcosThStar = new RooNDKeysPdf("keysMggcosThStar","keysMggcosThStar", argList, bkgDataSet,"avm",1);
    //RooNDKeysPdf* keysMggIsol = new RooNDKeysPdf("keysMggIsol","keysMggIsol", argList, bkgDataSet,"avm",1);
    RooNDKeysPdf* keysIsol = new RooNDKeysPdf("keysIsol","keysIsol", argList, bkgDataSet,"avm",1);
   
    /*Double_t lo[3]= {mgg.getMin(),isol1.getMin(),isol2.getMin()};
    Double_t hi[3]= {mgg.getMax(),isol1.getMax(0),isol2.getMax()} ;
    Int_t nBins[3] = { 100, 100, 100 };*/
   
    Double_t lo[2]= {isol1.getMin(),isol2.getMin()};
    Double_t hi[2]= {isol1.getMax(),isol2.getMax()} ;
    Int_t nBins[2] = {  1000, 1000 };
    
    //ph2 = mgg.createHistogram("ctsvsmgg",cosThStar,0,lo,hi,nBins); //"foo",&xlo,&xhi,&nBins);
    /*ph3 = mgg.createHistogram("ctsvsmgg",isol1,isol2,0,lo,hi,nBins); //"foo",&xlo,&xhi,&nBins);
    ph3->GetXaxis()->SetTitle("mgg");
    ph3->GetYaxis()->SetTitle("isol1");
    ph3->GetZaxis()->SetTitle("isol2");*/
    
    ph2 = isol1.createHistogram("ctsvsmgg",isol2,0,lo,hi,nBins); //"foo",&xlo,&xhi,&nBins);
    ph2->GetXaxis()->SetTitle("isol1");
    ph2->GetYaxis()->SetTitle("isol2");
    
    //keysMggcosThStar->fillHistogram(ph2,RooArgList(mgg,cosThStar));
    //keysMggIsol->fillHistogram(ph3,RooArgList(mgg,isol1,isol2));
    keysIsol->fillHistogram(ph2,RooArgList(isol1,isol2));

    TFile *f = new TFile("keys.root","RECREATE");
    //ph3->Write();
    ph2->Write();
    f->Close();

  }else{//reading a 2d histogram for better speed

    TFile *f = new TFile(m_fName,"READ");
    ph2 = (TH2F*)f->Get("htemp__isolation1_isolation2");
  //  ph3 = (TH3F*)f->Get("ctsvsmgg__mgg_cosThStar");
  }

  //RooDataHist *dataHist = new RooDataHist("hist3", "", argList, ph3);
  RooDataHist *dataHist = new RooDataHist("hist2", "", argList, ph2);
  RooArgSet tmpSet(argList);
  //RooHistPdf* histMggcosThStar = new RooHistPdf("histMggcosThStar","histMggcosThStar", tmpSet, *dataHist);
  //RooHistPdf* histMggIsol = new RooHistPdf("histMggIsol","histMggIsol", tmpSet, *dataHist);
  RooHistPdf* histIsol = new RooHistPdf("histIsol","histIsol", tmpSet, *dataHist);
   
//   std::cout << "Generating events from the keys Pdf" << std::endl;
//   keysMggcosThStar->generate(tmpSet,100);
//   std::cout << "Done generating events from the keys Pdf" << std::endl;
  
//   std::cout << "Generating events from the histo Pdf" << std::endl;
//   histMggcosThStar->generate(tmpSet,100);
//   std::cout << "Done generating events from the histo Pdf" << std::endl;

//   assert(0);
  
  //return histMggIsol;

  return new RooProdPdf(name, "Bkg PDF for mgg and isolation", 
                        RooArgSet(*mggPdf, histIsol));

  
			
}
