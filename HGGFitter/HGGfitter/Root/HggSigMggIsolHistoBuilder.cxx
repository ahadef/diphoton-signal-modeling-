// Author: Marine Kuna

#include "HGGfitter/HggSigMggIsolHistoBuilder.h"

#include "RooGaussian.h"
#include "HGGfitter/HggCBShape.h"
#include "RooCBShape.h"
#include "RooAddPdf.h"
#include "RooFormulaVar.h"

#include "RooRealConstant.h"
#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooHistPdf.h"
#include "TFile.h"
#include "TH1F.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void HggSigMggIsolHistoBuilder::Setup(const char* depName, const char* depName1, const char* depName2,
                                      const char* cbPeakName, const char* cbSigmaName, 
                                      const char* cbAlphaName, const char* cbNName,  
                                      const char* tailPeakName, const char* tailSigmaName, const char* tailFractionName, 
                                      unsigned int pdfType, const char* cat) 
{ 
  m_depName = depName; 
  m_depName1 = depName1; 
  m_depName2 = depName2; 
  m_cbPeakName = cbPeakName; 
  m_cbSigmaName = cbSigmaName; 
  m_cbAlphaName = cbAlphaName; 
  m_cbNName = cbNName;
  m_tailPeakName = tailPeakName; 
  m_tailSigmaName = tailSigmaName; 
  m_tailFractionName = tailFractionName;
  m_pdfType = pdfType; 
  if (TString(cat) != "") {SetSuffix("_{" + TString(cat) + "}"); cout << TString(cat) << endl; }
}


RooAbsPdf* HggSigMggIsolHistoBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
     
  RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  RooMsgService::instance().setSilentMode(true);

     
  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");
  RooRealVar& isol1 = *Dependent(dependents, m_depName1, "GeV", "isol_{lead}");
  RooRealVar& isol2 = *Dependent(dependents, m_depName2, "GeV", "isol_{sub}");
  
 ///////////////////////////////////// build the mgg crystal ball /////////////////////////////
 
  RooAbsPdf* peakPdf = 0;
 
   if (m_pdfType == 0)
    peakPdf = new HggCBShape(name + "MggSignalPdf.Peak", 
                             "Peak component of mgg Signal PDF", mgg,
                             Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                             Param(workspace, m_cbAlphaName), Param(workspace, m_cbNName));
  else
    peakPdf = new RooCBShape(name + "MggSignalPdf.Peak", 
                             "Peak component of mgg Signal PDF", mgg,
                             Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                             Param(workspace, m_cbAlphaName), Param(workspace, m_cbNName));

   RooAbsPdf* tailPdf = new RooGaussian(name + "MggSignalPdf.Tail", 
					"Tail components of mgg Signal PDF", mgg,
                                        Param(workspace, m_tailPeakName, "GeV"), Param(workspace, m_tailSigmaName, "GeV", "" , 1));

   RooAbsPdf* mggPdf = new RooAddPdf(name + "MggSignalPdf", "mgg Signal PDF",
			RooArgList(*peakPdf, *tailPdf), RooArgList(Param(workspace, m_tailFractionName)) );

 
 /////////////////////////////////////////// build isolation part /////////////////////////////////////////////
   
   //RooAbsReal& IsolationCategory = Param(workspace, m_IsolationCategoryName, "") ;
   //std::cout << "Category definition : \t" << IsolationCategory.getVal() << std::endl;

   //RooAbsReal& IsolationCategory = Param(workspace, m_IsolationCategoryName, "") ;
  
  //RooAbsReal& mCBpar0    = Param(workspace, "mCBpar0");
  //std::cout << "mCBpar0 : \t" << mCBpar0.getVal() << std::endl;

///////////////////////////////////////////////////////
    
    //Double_t lo= -5.;
    //Double_t hi= 20.;
    //Int_t nBins =  25;

    //TH1F* ph1 = 0;
    //TH1F* ph2 = 0;
    
    TFile *f1[11];
    TFile *f2[11];
    
    TH1F* tempHisto1[11];
    TH1F* tempHisto2[11];
    
  
    f1[0] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    tempHisto1[0] = (TH1F*)f1[0]->Get("resulthist");
    tempHisto1[0]->SetDirectory(0);
    f1[0]->Close();
   
    f1[1] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[1] = (TH1F*)f1[1]->Get("resulthist");
    tempHisto1[1]->SetDirectory(0);
    f1[1]->Close();
    
    f1[2] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[2] = (TH1F*)f1[2]->Get("resulthist");
    tempHisto1[2]->SetDirectory(0);
    f1[2]->Close();
   
    f1[3] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Rest_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[3] = (TH1F*)f1[3]->Get("resulthist");
    tempHisto1[3]->SetDirectory(0);
    f1[3]->Close();
   
    f1[4] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_NoConv_Rest_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[4] = (TH1F*)f1[4]->Get("resulthist");
    tempHisto1[4]->SetDirectory(0);
    f1[4]->Close();
   
    f1[5] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[5] = (TH1F*)f1[5]->Get("resulthist");
    tempHisto1[5]->SetDirectory(0);
    f1[5]->Close();
    
    f1[6] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[6] = (TH1F*)f1[6]->Get("resulthist");
    tempHisto1[6]->SetDirectory(0);
    f1[6]->Close();
    
    f1[7] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Medium_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[7] = (TH1F*)f1[7]->Get("resulthist");
    tempHisto1[7]->SetDirectory(0);
    f1[7]->Close();
    
    f1[8] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Medium_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[8] = (TH1F*)f1[8]->Get("resulthist");
    tempHisto1[8]->SetDirectory(0);
    f1[8]->Close();
    
    f1[9] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_Bad_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[9] = (TH1F*)f1[9]->Get("resulthist");
    tempHisto1[9]->SetDirectory(0);
    f1[9]->Close();
    
    f1[10] = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_IsVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto1[10] = (TH1F*)f1[10]->Get("resulthist");
    tempHisto1[10]->SetDirectory(0);
    f1[10]->Close();
   
    //if(!tempHisto1[0]) tempHisto1[0] = (TH1F*)f->Get("background_mmc_mass_0jetBin");
    /*for(int i=0;i<7;i++) {
      if(!tempHisto[i])
    	tempHisto[i] = (TH1F*)f->Get("background_mmc_mass");
      //      std::cout<<tempHisto[i]<<std::endl;
    }*/

  
    f2[0] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    tempHisto2[0] = (TH1F*)f2[0]->Get("resulthist");
    tempHisto2[0]->SetDirectory(0);
    f2[0]->Close();
   
    f2[1] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[1] = (TH1F*)f2[1]->Get("resulthist");
    tempHisto2[1]->SetDirectory(0);
    f2[1]->Close();
    
    f2[2] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[2] = (TH1F*)f2[2]->Get("resulthist");
    tempHisto2[2]->SetDirectory(0);
    f2[2]->Close();
   
    f2[3] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Rest_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[3] = (TH1F*)f2[3]->Get("resulthist");
    tempHisto2[3]->SetDirectory(0);
    f2[3]->Close();
   
    f2[4] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_NoConv_Rest_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[4] = (TH1F*)f2[4]->Get("resulthist");
    tempHisto2[4]->SetDirectory(0);
    f2[4]->Close();
   
    f2[5] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Good_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[5] = (TH1F*)f2[5]->Get("resulthist");
    tempHisto2[5]->SetDirectory(0);
    f2[5]->Close();
    
    f2[6] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Good_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[6] = (TH1F*)f2[6]->Get("resulthist");
    tempHisto2[6]->SetDirectory(0);
    f2[6]->Close();
    
    f2[7] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Medium_LowPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[7] = (TH1F*)f2[7]->Get("resulthist");
    tempHisto2[7]->SetDirectory(0);
    f2[7]->Close();
    
    f2[8] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Medium_HighPtt_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[8] = (TH1F*)f2[8]->Get("resulthist");
    tempHisto2[8]->SetDirectory(0);
    f2[8]->Close();
    
    f2[9] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_Bad_NoVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[9] = (TH1F*)f2[9]->Get("resulthist");
    tempHisto2[9]->SetDirectory(0);
    f2[9]->Close();
    
    f2[10] = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_IsVBF_TopoIsol_Hgg_shift0.000_all_FittingHisto.root","READ");
    tempHisto2[10] = (TH1F*)f2[10]->Get("resulthist");
    tempHisto2[10]->SetDirectory(0);
    f2[10]->Close();

    RooArgList argList1(isol1);
    RooArgList argList2(isol2);

    RooDataHist *dataHist1_inclusive = new RooDataHist("hist1_photon_lead_inclusive", "", argList1, tempHisto1[0]);
    RooDataHist *dataHist1_cat1 = new RooDataHist("hist1_photon_lead_cat1", "", argList1, tempHisto1[1]);
    RooDataHist *dataHist1_cat2 = new RooDataHist("hist1_photon_lead_cat2", "", argList1, tempHisto1[2]);
    RooDataHist *dataHist1_cat3 = new RooDataHist("hist1_photon_lead_cat3", "", argList1, tempHisto1[3]);
    RooDataHist *dataHist1_cat4 = new RooDataHist("hist1_photon_lead_cat4", "", argList1, tempHisto1[4]);
    RooDataHist *dataHist1_cat5 = new RooDataHist("hist1_photon_lead_cat5", "", argList1, tempHisto1[5]);
    RooDataHist *dataHist1_cat6 = new RooDataHist("hist1_photon_lead_cat6", "", argList1, tempHisto1[6]);
    RooDataHist *dataHist1_cat7 = new RooDataHist("hist1_photon_lead_cat7", "", argList1, tempHisto1[7]);
    RooDataHist *dataHist1_cat8 = new RooDataHist("hist1_photon_lead_cat8", "", argList1, tempHisto1[8]);
    RooDataHist *dataHist1_cat9 = new RooDataHist("hist1_photon_lead_cat9", "", argList1, tempHisto1[9]);
    RooDataHist *dataHist1_cat10 = new RooDataHist("hist1_photon_lead_cat10", "", argList1, tempHisto1[10]);

    RooDataHist *dataHist2_inclusive = new RooDataHist("hist2_photon_sub_inclusive", "", argList2, tempHisto2[0]);
    RooDataHist *dataHist2_cat1 = new RooDataHist("hist2_photon_sub_cat1", "", argList2, tempHisto2[1]);
    RooDataHist *dataHist2_cat2 = new RooDataHist("hist2_photon_sub_cat2", "", argList2, tempHisto2[2]);
    RooDataHist *dataHist2_cat3 = new RooDataHist("hist2_photon_sub_cat3", "", argList2, tempHisto2[3]);
    RooDataHist *dataHist2_cat4 = new RooDataHist("hist2_photon_sub_cat4", "", argList2, tempHisto2[4]);
    RooDataHist *dataHist2_cat5 = new RooDataHist("hist2_photon_sub_cat5", "", argList2, tempHisto2[5]);
    RooDataHist *dataHist2_cat6 = new RooDataHist("hist2_photon_sub_cat6", "", argList2, tempHisto2[6]);
    RooDataHist *dataHist2_cat7 = new RooDataHist("hist2_photon_sub_cat7", "", argList2, tempHisto2[7]);
    RooDataHist *dataHist2_cat8 = new RooDataHist("hist2_photon_sub_cat8", "", argList2, tempHisto2[8]);
    RooDataHist *dataHist2_cat9 = new RooDataHist("hist2_photon_sub_cat9", "", argList2, tempHisto2[9]);
    RooDataHist *dataHist2_cat10 = new RooDataHist("hist2_photon_sub_cat10", "", argList2, tempHisto2[10]);

    RooArgSet tmpSet1(argList1);
    RooArgSet tmpSet2(argList2);


    RooHistPdf *isol1SigPdf_inclusive = new RooHistPdf("isol1SigPdf_inclusive","isol1SigPdf_inclusive", tmpSet1, *dataHist1_inclusive);
    RooHistPdf *isol1SigPdf_cat1 = new RooHistPdf("isol1SigPdf_cat1","isol1SigPdf_cat1", tmpSet1, *dataHist1_cat1);
    RooHistPdf *isol1SigPdf_cat2 = new RooHistPdf("isol1SigPdf_cat2","isol1SigPdf_cat2", tmpSet1, *dataHist1_cat2);
    RooHistPdf *isol1SigPdf_cat3 = new RooHistPdf("isol1SigPdf_cat3","isol1SigPdf_cat3", tmpSet1, *dataHist1_cat3);
    RooHistPdf *isol1SigPdf_cat4 = new RooHistPdf("isol1SigPdf_cat4","isol1SigPdf_cat4", tmpSet1, *dataHist1_cat4);
    RooHistPdf *isol1SigPdf_cat5 = new RooHistPdf("isol1SigPdf_cat5","isol1SigPdf_cat5", tmpSet1, *dataHist1_cat5);
    RooHistPdf *isol1SigPdf_cat6 = new RooHistPdf("isol1SigPdf_cat6","isol1SigPdf_cat6", tmpSet1, *dataHist1_cat6);
    RooHistPdf *isol1SigPdf_cat7 = new RooHistPdf("isol1SigPdf_cat7","isol1SigPdf_cat7", tmpSet1, *dataHist1_cat7);
    RooHistPdf *isol1SigPdf_cat8 = new RooHistPdf("isol1SigPdf_cat8","isol1SigPdf_cat8", tmpSet1, *dataHist1_cat8);
    RooHistPdf *isol1SigPdf_cat9 = new RooHistPdf("isol1SigPdf_cat9","isol1SigPdf_cat9", tmpSet1, *dataHist1_cat9);
    RooHistPdf *isol1SigPdf_cat10 = new RooHistPdf("isol1SigPdf_cat10","isol1SigPdf_cat10", tmpSet1, *dataHist1_cat10);

    RooHistPdf *isol2SigPdf_inclusive = new RooHistPdf("isol2SigPdf_inclusive","isol2SigPdf_inclusive", tmpSet2,*dataHist2_inclusive);
    RooHistPdf *isol2SigPdf_cat1 = new RooHistPdf("isol2SigPdf_cat1","isol2SigPdf_cat1", tmpSet2, *dataHist2_cat1);
    RooHistPdf *isol2SigPdf_cat2 = new RooHistPdf("isol2SigPdf_cat2","isol2SigPdf_cat2", tmpSet2, *dataHist2_cat2);
    RooHistPdf *isol2SigPdf_cat3 = new RooHistPdf("isol2SigPdf_cat3","isol2SigPdf_cat3", tmpSet2, *dataHist2_cat3);
    RooHistPdf *isol2SigPdf_cat4 = new RooHistPdf("isol2SigPdf_cat4","isol2SigPdf_cat4", tmpSet2, *dataHist2_cat4);
    RooHistPdf *isol2SigPdf_cat5 = new RooHistPdf("isol2SigPdf_cat5","isol2SigPdf_cat5", tmpSet2, *dataHist2_cat5);
    RooHistPdf *isol2SigPdf_cat6 = new RooHistPdf("isol2SigPdf_cat6","isol2SigPdf_cat6", tmpSet2, *dataHist2_cat6);
    RooHistPdf *isol2SigPdf_cat7 = new RooHistPdf("isol2SigPdf_cat7","isol2SigPdf_cat7", tmpSet2, *dataHist2_cat7);
    RooHistPdf *isol2SigPdf_cat8 = new RooHistPdf("isol2SigPdf_cat8","isol2SigPdf_cat8", tmpSet2, *dataHist2_cat8);
    RooHistPdf *isol2SigPdf_cat9 = new RooHistPdf("isol2SigPdf_cat9","isol2SigPdf_cat9", tmpSet2, *dataHist2_cat9);
    RooHistPdf *isol2SigPdf_cat10 = new RooHistPdf("isol2SigPdf_cat10","isol2SigPdf_cat10", tmpSet2, *dataHist2_cat10);
   

    RooArgList hist1PDFList_first(*isol1SigPdf_inclusive,
    			   *isol1SigPdf_cat1,
    			   *isol1SigPdf_cat2,
    			   *isol1SigPdf_cat3,
    			   *isol1SigPdf_cat4,
    			   *isol1SigPdf_cat5
    			   );
			   
    RooArgList hist1PDFList_second(*isol1SigPdf_cat6,
    			   *isol1SigPdf_cat7,
    			   *isol1SigPdf_cat8,
    			   *isol1SigPdf_cat9,
    			   *isol1SigPdf_cat10
    			   );
			   
    RooArgList hist2PDFList_first(*isol2SigPdf_inclusive,
    			   *isol2SigPdf_cat1,
    			   *isol2SigPdf_cat2,
    			   *isol2SigPdf_cat3,
    			   *isol2SigPdf_cat4,
    			   *isol2SigPdf_cat5
    			   );
			   
    RooArgList hist2PDFList_second(*isol2SigPdf_cat6,
    			   *isol2SigPdf_cat7,
    			   *isol2SigPdf_cat8,
    			   *isol2SigPdf_cat9,
    			   *isol2SigPdf_cat10
    			   );
    
    RooArgList coefList_first(Param(workspace, "histCatCoeff0"),
    			Param(workspace, "histCatCoeff1"),
    			Param(workspace, "histCatCoeff2"),
    			Param(workspace, "histCatCoeff3"),
    			Param(workspace, "histCatCoeff4"),
    			Param(workspace, "histCatCoeff5")
    			);
			
    RooArgList coefList_second(Param(workspace, "histCatCoeff6"),
    			Param(workspace, "histCatCoeff7"),
    			Param(workspace, "histCatCoeff8"),
    			Param(workspace, "histCatCoeff9"),
    			Param(workspace, "histCatCoeff10")
    			);


  RooAbsPdf* isol1SigPdf_first = new RooAddPdf(name + "_Sig_Isol1_first", "leading isol1 signal PDF _first",
                                               hist1PDFList_first, coefList_first );
					       
  RooAbsPdf* isol1SigPdf_second = new RooAddPdf(name + "_Sig_Isol1_second", "leading isol1 signal PDF _second",
                                               hist1PDFList_second, coefList_second );
					       
  RooAbsPdf* isol1SigPdf = new RooAddPdf(name + "_Sig_Isol1", "leading isol1 signal PDF",
                                               RooArgList(*isol1SigPdf_first,*isol1SigPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );
	
  RooAbsPdf* isol2SigPdf_first = new RooAddPdf(name + "_Sig_Isol2_first", "leading isol2 signal PDF _first",
                                               hist2PDFList_first, coefList_first );
					       
  RooAbsPdf* isol2SigPdf_second = new RooAddPdf(name + "_Sig_Isol2_second", "leading isol2 signal PDF _second",
                                               hist2PDFList_second, coefList_second );
					       
  RooAbsPdf* isol2SigPdf = new RooAddPdf(name + "_Sig_Isol2", "leading isol2 signal PDF",
                                               RooArgList(*isol2SigPdf_first,*isol2SigPdf_second), 
					       RooArgList(Param(workspace, "histCatCoeffFirst"),
					                  Param(workspace, "histCatCoeffSecond") ) );
					       

  return new RooProdPdf(name, "Signal PDF for mgg and isolation", 
			RooArgSet(*mggPdf, *isol1SigPdf, *isol2SigPdf));


 

 
/*
    TH1F* ph1 = 0;
    TH1F* ph2 = 0;

    TFile *f1 = 0;
    TFile *f2 = 0;
    
    
    //TFile::Open(Form("/tmp/temp_%d.root", gSystem->GetPid()), "RECREATE");
    
  
    // test to separate the histograms according to the category
    // uncoGoodLpt
    if (Param(workspace, "mCBpar0").getVal() > -0.27 && Param(workspace, "mCBpar0").getVal() < -0.26 )
    {
    f1 = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph1 = (TH1F*)f1->Get("resulthist");
    ph1->SetDirectory(0);
    f1->Close();
    std::cout << "my category ! \t" <<  std::endl;
    }
    // uncoRestLpt
    else if (Param(workspace, "mCBpar0").getVal() > -0.39 && Param(workspace, "mCBpar0").getVal() < -0.38 )
    {
    f1 = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph1 = (TH1F*)f1->Get("resulthist");
    ph1->SetDirectory(0);
    f1->Close();
    std::cout << "my category ! \t" <<  std::endl;
    }
    // other categories
    else
    {
    //f1 = new TFile("bkg_data2012_firstjet_etabin0_range-5.0:20.0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    //ph1 = (TH1F*)f1->Get("bkghist");
    //ph1->SetDirectory(0);
    //f1->Close();
    //std::cout << "other categories ! \t" <<  std::endl;
    f1 = new TFile("photon_data2012_firstphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    ph1 = (TH1F*)f1->Get("resulthist");
    ph1->SetDirectory(0);
    f1->Close();
    std::cout << "my category ! \t" <<  std::endl;
    }

	
    f2 = new TFile("photon_data2012_secondphot_range-5.0:20.0_etabin0_reverse4_TopoIsol_Hgg_FittingHisto.root","READ");
    

    ph2 = (TH1F*)f2->Get("resulthist");
    ph2->SetDirectory(0);
    f2->Close();
 
    RooDataHist *dataHist1 = new RooDataHist("hist1", "", isol1, ph1);
    RooDataHist *dataHist2 = new RooDataHist("hist2", "", isol2, ph2);


    RooHistPdf* isol1SigPdf = new RooHistPdf("isol1SigPdf","isol1SigPdf", isol1, *dataHist1);
    RooHistPdf* isol2SigPdf = new RooHistPdf("isol2SigPdf","isol2SigPdf", isol2, *dataHist2);
  

    return new RooProdPdf(name, "Signal PDF for mgg and isolation", 
			RooArgSet(*mggPdf, *isol1SigPdf, *isol2SigPdf));
 */
}
