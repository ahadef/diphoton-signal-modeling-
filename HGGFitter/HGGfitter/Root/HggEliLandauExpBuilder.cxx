// $Id: HggEliLandauExpBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggEliLandauExpBuilder.h"

#include "RooLandau.h"
#include "RooExponential.h"
#include "RooAddPdf.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


void HggEliLandauExpBuilder::Setup(const char* depName, const char* landauMeanName, const char* landauSigmaName, 
                                   const char* expSlope, const char* landauFracName)
{ 
  m_depName = depName; 
  m_landauMeanName = landauMeanName; 
  m_landauSigmaName = landauSigmaName; 
  m_expSlope = expSlope; 
  m_landauFracName = landauFracName;
}


RooAbsPdf* HggEliLandauExpBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = *Dependent(dependents, m_depName);

  RooLandau* landauPdf = new RooLandau(name + ".Landau", "Landau component of Landau+exp PDF", mgg,
                                       Param(workspace, m_landauMeanName), Param(workspace, m_landauSigmaName));
  
  RooExponential* expPdf = new RooExponential(name + ".exp", "exponential component of Landau+exp PDF", mgg,
                                              Param(workspace, m_expSlope));
  
  return new RooAddPdf(name, "Landau+exp PDF",
                       RooArgList(*landauPdf, *expPdf), RooArgList(Param(workspace, m_landauFracName)));
}
