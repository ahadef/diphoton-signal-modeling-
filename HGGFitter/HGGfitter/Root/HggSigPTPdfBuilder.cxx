// $Id: HggSigPTPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggSigPTPdfBuilder.h"

#include "RooGaussian.h"
#include "RooBifurGauss.h"
#include "RooAddPdf.h"
#include "HfitterModels/HftPolyExp.h"

using namespace Hfitter;

RooAbsPdf* HggSigPTPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& pT = *Dependent(dependents, "pT", "GeV", "p_{T#gamma#gamma}");
     // choose PDF
   const Int_t pdfType = 3; 
   
   if (pdfType == 1) {

      RooAbsReal& sigPtM0    = Param(workspace, "sigPtM0");
      RooAbsReal& sigPtPow1  = Param(workspace, "sigPtPow1");
      RooAbsReal& sigPtExp1  = Param(workspace, "sigPtExp1");
      RooAbsReal& sigPtPow2  = Param(workspace, "sigPtPow2");
      RooAbsReal& sigPtExp2  = Param(workspace, "sigPtExp2");
      RooAbsReal& sigPtPow3  = Param(workspace, "sigPtPow3");
      RooAbsReal& sigPtExp3  = Param(workspace, "sigPtExp3");
      RooAbsReal& sigPtRelNorm  = Param(workspace, "sigPtRelNorm");
      RooAbsReal& sigPtTailNorm = Param(workspace, "sigPtTailNorm");
      
      RooAbsPdf* ptPdf1 = new HftPolyExp(name + "_pT_1", "1st half Signal p_T PDF for H->gamma gamma", 
                                          pT, sigPtM0, sigPtPow1, sigPtExp1);
      RooAbsPdf* ptPdf2 = new HftPolyExp(name + "_pT_2", "2nd half Signal p_T PDF for H->gamma gamma", 
                                          pT, sigPtM0, sigPtPow2, sigPtExp2);
      RooAbsPdf* ptPdf3 = new HftPolyExp(name + "_pT_3", "3nd half Signal p_T PDF for H->gamma gamma", 
                                          pT, sigPtM0, sigPtPow3, sigPtExp3);
      
      
      return new RooAddPdf(name + "_pT", "Signal p_T PDF for H->gamma gamma", 
                            RooArgList(*ptPdf1, *ptPdf2, *ptPdf3), RooArgList(sigPtRelNorm,sigPtTailNorm ) );

   }
   else if (pdfType == 2) {

      RooAbsReal& sigPt0_1       = Param(workspace, "sigPt0_1");
      RooAbsReal& sigPtSigma_1   = Param(workspace, "sigPtSigma_1");
      RooAbsReal& sigPtRelNorm_1 = Param(workspace, "sigPtRelNorm_1");

      RooAbsPdf* ptPdf_1 = new RooGaussian(name + "pT_1", 
                                            "1st signal pT PDF for H->gamma gamma", 
                                            pT, sigPt0_1, sigPtSigma_1 );

      RooAbsReal& sigPt0_2       = Param(workspace, "sigPt0_2");
      RooAbsReal& sigPtSigma_2   = Param(workspace, "sigPtSigma_2");
      RooAbsReal& sigPtRelNorm_2 = Param(workspace, "sigPtRelNorm_2");

      RooAbsPdf* ptPdf_2 = new RooGaussian(name + "pT_2", 
                                            "2nd signal pT PDF for H->gamma gamma", 
                                            pT, sigPt0_2, sigPtSigma_2 );

      RooAbsReal& sigPt0_3       = Param(workspace, "sigPt0_3");
      RooAbsReal& sigPtSigma_3   = Param(workspace, "sigPtSigma_3");

      RooAbsPdf* ptPdf_3 = new RooGaussian(name + "pT_3", 
                                            "3RD signal pT PDF for H->gamma gamma", 
                                            pT, sigPt0_3, sigPtSigma_3 );

      
      return new RooAddPdf(name + "_pT", "Signal pT PDF for H->gamma gamma", 
                            RooArgList( *ptPdf_1, *ptPdf_2, *ptPdf_3 ), 
                            RooArgList( sigPtRelNorm_1, sigPtRelNorm_2 ) );
   }
   else {

      RooAbsReal& sigPt0_1       = Param(workspace, "sigPt01");
      RooAbsReal& sigPtSigmaL_1  = Param(workspace, "sigPtSigmaL1");
      RooAbsReal& sigPtSigmaR_1  = Param(workspace, "sigPtSigmaR1");
      RooAbsReal& sigPtRelNorm_1 = Param(workspace, "sigPtRelNorm1");

      RooAbsPdf* ptPdf_1 = new RooBifurGauss(name + "pT_1", 
                                              "1st signal pT PDF for H->gamma gamma", 
                                              pT, sigPt0_1, sigPtSigmaL_1, sigPtSigmaR_1 );

      RooAbsReal& sigPt0_2       = Param(workspace, "sigPt02");
      RooAbsReal& sigPtSigma_2   = Param(workspace, "sigPtSigma2");
      RooAbsReal& sigPtRelNorm_2 = Param(workspace, "sigPtRelNorm2");

      RooAbsPdf* ptPdf_2 = new RooGaussian(name + "pT_2", 
                                            "2nd signal pT PDF for H->gamma gamma", 
                                            pT, sigPt0_2, sigPtSigma_2 );

      RooAbsReal& sigPt0_3       = Param(workspace, "sigPt03");
      RooAbsReal& sigPtSigma_3   = Param(workspace, "sigPtSigma3");

      RooAbsPdf* ptPdf_3 = new RooGaussian(name + "pT_3", 
                                            "3RD signal pT PDF for H->gamma gamma", 
                                            pT, sigPt0_3, sigPtSigma_3 );

      RooAbsReal& sigPt0_4       = Param(workspace, "sigPt04");
      RooAbsReal& sigPtSigmaL_4  = Param(workspace, "sigPtSigmaL4");
      RooAbsReal& sigPtSigmaR_4  = Param(workspace, "sigPtSigmaR4");
      RooAbsReal& sigPtRelNorm_4 = Param(workspace, "sigPtRelNorm4");

      RooAbsPdf* ptPdf_4 = new RooBifurGauss(name + "pT_4", 
                                                    "4th signal pT PDF for H->gamma gamma", 
                                                    pT, sigPt0_4, sigPtSigmaL_4, sigPtSigmaR_4 );

     
      return new RooAddPdf(name, "Signal pT PDF for H->gamma gamma", 
                            RooArgList( *ptPdf_1, *ptPdf_2, *ptPdf_4, *ptPdf_3 ), 
                            RooArgList( sigPtRelNorm_1, sigPtRelNorm_2, sigPtRelNorm_4) );
   }
}
