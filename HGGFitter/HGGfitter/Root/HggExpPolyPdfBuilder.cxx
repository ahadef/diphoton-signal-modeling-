#include "HGGfitter/HggExpPolyPdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooGenericPdf.h"
#include "HGGfitter/HggExpPoly.h"

using namespace Hfitter;


void HggExpPolyPdfBuilder::Setup(const char* depName,
                                 const char* c1Name, const char* c2Name, const char* c3Name, 
                                 const char* c4Name, const char* c5Name, const char* c6Name)
{
  m_depName = depName;
  m_offset = 0;
  if (TString(c1Name) != "") m_cNames.push_back(c1Name); else return;
  if (TString(c2Name) != "") m_cNames.push_back(c2Name); else return;
  if (TString(c3Name) != "") m_cNames.push_back(c3Name); else return;
  if (TString(c4Name) != "") m_cNames.push_back(c4Name); else return;
  if (TString(c5Name) != "") m_cNames.push_back(c5Name); else return;
  if (TString(c6Name) != "") m_cNames.push_back(c6Name); else return;
}


void HggExpPolyPdfBuilder::Setup(const char* depName, double offset,
                                 const char* c1Name, const char* c2Name, const char* c3Name, 
                                 const char* c4Name, const char* c5Name, const char* c6Name)
{
  m_depName = depName;
  m_offset = offset;
  if (TString(c1Name) != "") m_cNames.push_back(c1Name); else return;
  if (TString(c2Name) != "") m_cNames.push_back(c2Name); else return;
  if (TString(c3Name) != "") m_cNames.push_back(c3Name); else return;
  if (TString(c4Name) != "") m_cNames.push_back(c4Name); else return;
  if (TString(c5Name) != "") m_cNames.push_back(c5Name); else return;
  if (TString(c6Name) != "") m_cNames.push_back(c6Name); else return;
}


RooAbsPdf* HggExpPolyPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

//   RooArgList coeffs;
//   for (std::vector<TString>::const_iterator cName = m_cNames.begin(); cName != m_cNames.end(); cName++)
//     coeffs.add(Param(workspace, *cName,  ""));
// 
//     return new HggExpPoly(name, Form("PDF for " + m_depName + " using exp(order-%d polynomials)", int(m_cNames.size())), 
//                         mgg, coeffs, m_offset);

  RooArgList vars;

  unsigned int iVar = 0, expo = 1;
  vars.add(mgg); iVar++;
  TString var = "@0";
  TString formula = "";
  
  if (m_offset != 0) {
    var = "(@0 - @1)/@1";
    workspace.import(*new RooConstVar("expPoly_offset", "offset", m_offset), RooFit::Silence(true));
    vars.add(*workspace.function("expPoly_offset"));
    //vars.add(Param(workspace, "expPoly_offset", "GeV", "offset", m_offset));
    iVar++;
  }
  RooArgList coeffs;
  for (std::vector<TString>::const_iterator cName = m_cNames.begin(); cName != m_cNames.end(); cName++) {
    if (formula != "") formula += "+";
    formula += Form("@%d*pow(%s,%d)", iVar, var.Data(), expo);
    expo++;
    vars.add(Param(workspace, *cName, "")); iVar++;
  }
  formula = "exp(" + formula + ")";

  return new RooGenericPdf(name, Form("PDF for " + m_depName + " using exp(order-%d polynomials)", int(m_cNames.size())), formula, vars);  
}
