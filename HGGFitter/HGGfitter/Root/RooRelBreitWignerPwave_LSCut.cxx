#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>
#include "TMath.h"

#include "HGGfitter/RooRelBreitWignerPwave_LSCut.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
// #include "RooFitTools/RooRandom.h"

using namespace std;

ClassImp(RooRelBreitWignerPwave_LSCut)


//Double_t RooRelBreitWignerPwave_LSCut::LSweight(Double_t *x, Double_t *par) 
Double_t RooRelBreitWignerPwave_LSCut::LSweight(Double_t _x) const
{
  double x[1];
  double par[2];
  x[0] = _x;
  
  //Numertor of BW
  Double_t k = x[0]*x[0]*width/(mean*TMath::Pi());

  //rel BW
  Double_t BWVal = k / ((x[0]*x[0]- mean*mean)*(x[0]*x[0]- mean*mean) + (x[0]*x[0]/mean*width)*(x[0]*x[0]/mean*width));
  
  //Luminosity func
  double pdfweight = pow((1-pow((x[0]/13000),(1./3))),8.95) * pow((x[0]/13000),-4.1) * (-2.95e-10 + 1.32e-7*(x[0]/13000) -1.7e-7*pow((x[0]/13000), 2));	

  //Compute Matrix element - contribution from top quark
  Double_t GF = 1.16637E-5;
  Double_t norm = 1./pow(TMath::Pi(),2) * GF /576./sqrt(2) * 9/4;
  par[0]=norm;
  par[1]=173.;
  Float_t mass = x[0];
  
  // 
  Float_t tau = 4.*pow(par[1],2)/pow(mass,2); //par[1] is the top mass
  TComplex ftau(0);
  if (tau >= 1.) {
    Float_t Retau = pow(TMath::ASin(1/sqrt(tau)), 2);
    ftau = TComplex(Retau,0);
  } else {
    Float_t ratio = (1 + sqrt(1 - tau)) / (1- sqrt( 1- tau));
    Double_t Retau = log(ratio);
    Double_t Imtau = -TMath::Pi();
    TComplex acomp = TComplex(Retau,Imtau);
    ftau = -1./4 * acomp * acomp;
  }
  //TComplex fact = 1 + (1-tau)*ftau;
  TComplex fact0 =  ((double)(1-tau)) * ftau;
  TComplex fact = 1. + fact0;
  Double_t Lambda = 0.2;
  Double_t alphas = 12 * TMath::Pi()/log(pow((mass/Lambda),2));
  if (tau >= 1.)
    alphas = alphas/(33 - 2*5);
  else
    alphas = alphas/(33 - 2*6);
  
  //cout<<" BWVal="<<BWVal<<"  pdfweight="<<pdfweight<<"  par[0]="<<par[0]<<" pow(alphas, 2)="<<pow(alphas, 2)<<"  pow(mass, 3)="<<pow(mass, 3)<<" pow(tau, 2)="<<pow(tau, 2)<<" fact.Rho2()="<<fact.Rho2()<<endl;
  return BWVal*pdfweight*par[0] * pow(alphas, 2) * pow(mass, 3) * pow(tau, 2) * fact.Rho2() *  2.16066e15;
  return 1;
}


Double_t RooRelBreitWignerPwave_LSCut::Derivative1(Double_t _x) const
{
	double xx; 
	double h=0.001;
	xx = _x+h;     double f1 = LSweight(xx);
	xx = _x-h;     double f2 = LSweight(xx);
	xx = _x+h/2;   double g1 = LSweight(xx);
	xx = _x-h/2;   double g2 = LSweight(xx);

	double h2    = 1/(2.*h);
	double d0    = f1 - f2;
	double d2    = g1 - g2;
	double deriv = h2*(8*d2 - d0)/3.;

	return deriv;
}


Double_t RooRelBreitWignerPwave_LSCut::Derivative2(Double_t _x) const
{
	double xx;
	double h=0.001;
	xx = _x+h;     double f1 = LSweight(xx);
	xx = _x;       double f2 = LSweight(xx);
	xx = _x-h;     double f3 = LSweight(xx);

	xx = _x+h/2;   double g1 = LSweight(xx);
	xx = _x-h/2;   double g3 = LSweight(xx);

	double hh    = 1/(h*h);
	double d0    = f3 - 2*f2 + f1;
	double d2    = 4*g3 - 8*f2 +4*g1;
	double deriv = hh*(4*d2 - d0)/3.;
	return deriv;

}



////////////////////////////////////////////////////////////////////////////////

RooRelBreitWignerPwave_LSCut::RooRelBreitWignerPwave_LSCut(const char *name, const char *title,
          RooAbsReal& _x, RooAbsReal& _mean,
          RooAbsReal& _width) :
  RooAbsPdf(name,title),
  x("x","Dependent",this,_x),
  mean("mean","Mean",this,_mean),
  width("width","Width",this,_width)
{
}



////////////////////////////////////////////////////////////////////////////////

RooRelBreitWignerPwave_LSCut::RooRelBreitWignerPwave_LSCut(const RooRelBreitWignerPwave_LSCut& other, const char* name) : 
  RooAbsPdf(other,name), x("x",this,other.x), mean("mean",this,other.mean),
  width("width",this,other.width)
//hyp, TF_LSweight(other.TF_LSweight)
{
}

Double_t RooRelBreitWignerPwave_LSCut::evaluate() const
{
  double xcut = mean-2.*width; //Unify cut-off, 2 Gamma - same selections as used for CX factors 
  double ycut = 0; //LSweight(x) + Derivative1(x)*(x - xcut) + 0.5*Derivative2(x)*(x-xcut)*(x-xcut); 

  if(x<xcut) { 
    ycut = LSweight(xcut)*exp(Derivative1(xcut)/LSweight(xcut)*(x - xcut) );
  }

  if(x<xcut)  {return ycut;}
  else { return  LSweight(x);}
  
  //  if(x<xcut) { return k* ycut / ((x*x- mean*mean)*(x*x- mean*mean) + (x*x/mean*width)*(x*x/mean*width)); }
  //  else  { return k* LSweight(x) / ((x*x- mean*mean)*(x*x- mean*mean) + (x*x/mean*width)*(x*x/mean*width));}  
}



//////////////////////////////////////////////////////////////////////////////////
//
//Int_t RooRelBreitWignerPwave_LSCut::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const 
//{
//  if (matchArgs(allVars,analVars,x)) return 1 ;
//  return 0 ;
//}
//
//
//
//////////////////////////////////////////////////////////////////////////////////
//
//Double_t RooRelBreitWignerPwave_LSCut::analyticalIntegral(Int_t code, const char* rangeName) const 
//{
//  switch(code) {
//  case 1: 
//    {
//      Double_t c = 2./width;
//      return c*(atan(c*(x.max(rangeName)-mean)) - atan(c*(x.min(rangeName)-mean)));
//    }
//  }
//  
//  assert(0) ;
//  return 0 ;
//}

