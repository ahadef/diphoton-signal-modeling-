// $Id: HggBkgMggIsolBremPdfBuilder.cxx,v 1.5 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggIsolBremPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"
#include "RooExponential.h"

using namespace Hfitter;


HggBkgMggIsolBremPdfBuilder::HggBkgMggIsolBremPdfBuilder()
{ 
}


void HggBkgMggIsolBremPdfBuilder::Setup(bool isGam1, bool isGam2, bool brem1, bool brem2) 
{
  m_mggIsolPdfBuilder.Setup(isGam1, isGam2);
  m_brem1 = brem1;
  m_brem2 = brem2;
}


RooAbsPdf* HggBkgMggIsolBremPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  if (!m_brem1 && !m_brem2) return m_mggIsolPdfBuilder.Pdf(name, dependents, workspace);
  
  RooArgSet pdfs;
  pdfs.add(*m_mggIsolPdfBuilder.Pdf(name + "_mggIsol", dependents, workspace));
  
  if(m_brem1) {
    RooRealVar* brem1 = Dependent(dependents, "brem1");
    pdfs.add(*new RooExponential(name + "_brem1", "", *brem1, Param(workspace, "bkgSlopeBrem1")));
  }
  
  if(m_brem2) {  
    RooRealVar* brem2 = Dependent(dependents, "brem2");
    pdfs.add(*new RooExponential(name + "_brem2", "", *brem2, Param(workspace, "bkgSlopeBrem2")));
  }
  return new RooProdPdf(name, "Bkg PDF for mgg and isolation", pdfs);
}
