#include "HGGfitter/HggBkgMggBernstein4PdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooBernstein.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggBernstein4PdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
 RooAbsReal& mgg = *Dependent(dependents, "mgg", "GeV", "m_{#gamma#gamma}");

 RooAbsReal& cs1  = Param(workspace, m_cs1Name, "");
 RooAbsReal& cs2  = Param(workspace, m_cs2Name, "");
 RooAbsReal& cs3  = Param(workspace, m_cs3Name, "");
 RooAbsReal& cs4  = Param(workspace, m_cs4Name, "");

 return new RooBernstein(name, "Background PDF for mgg using 4th order Bernstein polynomials",
                                      mgg, RooArgList(RooFit::RooConst(1), cs1, cs2, cs3, cs4));

}
