#include "HGGfitter/HggTwoSidedNovoPdfBuilder.h"

#include "RooGaussian.h"
#include "HGGfitter/HggTwoSidedNovoPdf.h"
#include "RooAddPdf.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void HggTwoSidedNovoPdfBuilder::Setup(const char* depName, const char* cbPeakName, const char* cbSigmaName, 
                                 const char* cbAlphaLoName, const char* cbAlphaHiName) 
{ 
  m_depName = depName; 
  m_cbPeakName = cbPeakName; 
  m_cbSigmaName = cbSigmaName; 
  m_cbAlphaLoName = cbAlphaLoName; 
  m_cbAlphaHiName = cbAlphaHiName; 
}


RooAbsPdf* HggTwoSidedNovoPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  return new HggTwoSidedNovoPdf(name + ".CB", 
                                "2SN  mgg Signal PDF", mgg,
                                Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                                Param(workspace, m_cbAlphaLoName),
                                Param(workspace, m_cbAlphaHiName));
}
