#include "HGGfitter/HggResolHelper.h"
#include <iostream>
using std::cout;
using std::endl;


using namespace Hfitter;

TH1D* HggResolHelper::resolutionPlot()
{
  TH1D* h = new TH1D("resol", "", m_nBins, m_varMin, m_varMax);
  h->GetXaxis()->SetTitle(m_title);
  occupancy = new TH1D("occupancy", "", m_nBins, m_varMin, m_varMax);
  occupancy->GetXaxis()->SetTitle(m_title);
  TH1D* mass = 0;
  for (int i = 1; i <= m_nBins; i++) {
    double varMin = h->GetXaxis()->GetBinCenter(i) - h->GetXaxis()->GetBinWidth(i)/2;
    double varMax = h->GetXaxis()->GetBinCenter(i) + h->GetXaxis()->GetBinWidth(i)/2;
    cout << "Bin " << i << " of " << m_nBins << endl;
    TString cut = Form("%s>%.3f&&%s<%.3f",
                       m_var.Data(), varMin, m_var.Data(), varMax);
    cut = cut + "&&" + m_cut;
    if (mass) delete mass;
    mass = new TH1D("mass", "", 100, 110, 130);
    m_tree->Draw("mgg>>mass", cut, "GOFF");
    h->SetBinContent(i, mass->GetRMS());
    h->SetBinError(i, mass->GetRMSError());
    occupancy->SetBinContent(i, mass->GetEntries());
  }
  return h;
}

