
#include "RooFit.h"

#include "Riostream.h"
#include "Riostream.h"
#include <math.h>

#include "HGGfitter/HggNovoCBPdf.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooMath.h"
#include "TMath.h"
#include "Math/ProbFuncMathCore.h"

using namespace Hfitter;


//_____________________________________________________________________________
HggNovoCBPdf::HggNovoCBPdf(const char *name, const char *title,
           RooAbsReal& _m, RooAbsReal& _m0, RooAbsReal& _sigma,
           RooAbsReal& _alphaLo, RooAbsReal& _alphaHi, RooAbsReal& _nHi) :
  RooAbsPdf(name, title),
  m("m", "Dependent", this, _m),
  m0("m0", "M0", this, _m0),
  sigma("sigma", "Sigma", this, _sigma),
  alphaLo("alphaLo", "Low-side Alpha", this, _alphaLo),
  alphaHi("alphaHi", "High-side Alpha", this, _alphaHi),
  nHi("nHi", "Hig-side Order", this, _nHi)
{
}


//_____________________________________________________________________________
HggNovoCBPdf::HggNovoCBPdf(const HggNovoCBPdf& other, const char* name) :
  RooAbsPdf(other, name), m("m", this, other.m), m0("m0", this, other.m0),
  sigma("sigma", this, other.sigma), 
  alphaLo("alphaLo", this, other.alphaLo), alphaHi("alphaHi", this, other.alphaHi), nHi("nHi", this, other.nHi)
{
}


//_____________________________________________________________________________
Double_t HggNovoCBPdf::evaluate() const {

  Double_t t = (m-m0)/sigma;

  
  double beta = alphaLo;
  if (t < -alphaLo) return exp(beta*(t + alphaLo) - 0.5*alphaLo*alphaLo);
  else if (t > alphaHi) {
    Double_t a = exp(-0.5*alphaHi*alphaHi);
    Double_t b = nHi/alphaHi - alphaHi; 
    return a/TMath::Power(alphaHi/nHi*(b + t), nHi);
  }
  return exp(-0.5*t*t);
}


//_____________________________________________________________________________
Int_t HggNovoCBPdf::getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/) const
{
  if( matchArgs(allVars,analVars,m) )
    return 1 ;
  
  return 0;
}


//_____________________________________________________________________________
Double_t HggNovoCBPdf::analyticalIntegral(Int_t code, const char* rangeName) const
{
  assert(code==1);
  double result = 0;
    
  double sig = fabs((Double_t)sigma);
  double tmin = (m.min(rangeName)-m0)/sig;
  double tmax = (m.max(rangeName)-m0)/sig;
  
  double beta = alphaLo;
  
  if (tmin < -alphaLo) 
    result += expIntegral(tmin, TMath::Min(tmax, -alphaLo), beta)*exp(alphaLo*(beta - 0.5*alphaLo));
  if (tmin < alphaHi)
    result += gaussianIntegral(TMath::Max(tmin, -alphaLo), TMath::Min(tmax, alphaHi));
  if (tmax > alphaHi)
    result += powerLawIntegral(-tmax, TMath::Min(-tmin, -alphaHi), alphaHi, nHi);

  return sig*result;
}


double HggNovoCBPdf::expIntegral(double tmin, double tmax, double beta) const
{
  return (exp(beta*tmax) - exp(beta*tmin))/beta;
}

double HggNovoCBPdf::gaussianIntegral(double tmin, double tmax) const
{
  return sqrt(TMath::TwoPi())*(ROOT::Math::gaussian_cdf(tmax) - ROOT::Math::gaussian_cdf(tmin));
}


double HggNovoCBPdf::powerLawIntegral(double tmin, double tmax, double alpha, double n) const
{
  double a = exp(-0.5*alpha*alpha);
  double b = n/alpha - alpha;
  return a/(1 - n)*( (b - tmin)/(TMath::Power(alpha/n*(b - tmin), n)) - (b - tmax)/(TMath::Power(alpha/n*(b - tmax), n)) );
}
