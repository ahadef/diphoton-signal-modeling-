#include "HGGfitter/HggGenericBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooGenericPdf.h"
#include "HGGfitter/HggExpPoly.h"

using namespace Hfitter;


void HggGenericBuilder::Setup(const char* depName, const char* formula,
                              const char* c1Name, const char* c2Name, const char* c3Name, 
                              const char* c4Name, const char* c5Name, const char* c6Name)
{
  m_depName = depName;
  m_formula = formula;
  if (TString(c1Name) != "") m_cNames.push_back(c1Name); else return;
  if (TString(c2Name) != "") m_cNames.push_back(c2Name); else return;
  if (TString(c3Name) != "") m_cNames.push_back(c3Name); else return;
  if (TString(c4Name) != "") m_cNames.push_back(c4Name); else return;
  if (TString(c5Name) != "") m_cNames.push_back(c5Name); else return;
  if (TString(c6Name) != "") m_cNames.push_back(c6Name); else return;
}


RooAbsPdf* HggGenericBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& depVar = *Dependent(dependents, m_depName);

  RooArgList vars;
  vars.add(depVar);
  
  for (std::vector<TString>::const_iterator cName = m_cNames.begin(); cName != m_cNames.end(); cName++)
    vars.add(Param(workspace, *cName, ""));

  return new RooGenericPdf(name, Form("Generic PDF for " + m_depName, int(m_cNames.size())), m_formula, vars);  
}
