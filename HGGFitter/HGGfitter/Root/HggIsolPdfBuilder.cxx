#include "HGGfitter/HggIsolPdfBuilder.h"

#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooNovosibirsk.h"
#include "RooConstVar.h"

using namespace Hfitter;

RooAbsPdf* HggIsolPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& isol = *Dependent(dependents, m_isolVar);
  //cout << "using " << m_isGam << endl;
  if (m_isGam) {
    RooAbsReal& isolG1Mean    = Param(workspace, "isolG1Mean");
    RooAbsReal& isolG1Sigma = Param(workspace, "isolG1Sigma", "", "", 1);
    RooAbsReal& isolG2Mean    = Param(workspace, "isolG2Mean");
    RooAbsReal& isolG2Sigma = Param(workspace, "isolG2Sigma", "", "", 1);
    RooAbsReal& isolG2Fraction = Param(workspace, "isolG2Fraction");
    RooAbsReal& isolG3Mean    = Param(workspace, "isolG3Mean");
    RooAbsReal& isolG3Sigma = Param(workspace, "isolG3Sigma", "", "", 1);
    RooAbsReal& isolG3Fraction = Param(workspace, "isolG3Fraction");
    
    RooAbsPdf* GaussianTerm1 = new RooGaussian(name + "_photonIsolG1Term", "Gaussian term 1 PDF for photons", isol, 
                          isolG1Mean, isolG1Sigma);
    RooAbsPdf* GaussianTerm2 = new RooGaussian(name + "_photonIsolG2Term", "Gaussian term 2 PDF for photons", isol, 
                          isolG2Mean, isolG2Sigma);
    RooAbsPdf* GaussianTerm3 = new RooGaussian(name + "_photonIsolG3Term", "Gaussian term 3 PDF for photons", isol, 
                          isolG3Mean, isolG3Sigma);
   
    
    return new RooAddPdf(name, "Isolation PDF for photon", 
                        RooArgList(*GaussianTerm3, *GaussianTerm2, *GaussianTerm1), 
                        RooArgList(isolG3Fraction, isolG2Fraction) );

  
  }
  RooAbsReal& isolNovoMu     = Param(workspace, "isolNovoMu");
  RooAbsReal& isolNovoSigma  = Param(workspace, "isolNovoSigma", "", "", 1);
  RooAbsReal& isolNovoLambda = Param(workspace, "isolNovoLambda");
  RooAbsReal& isolNovoFrac = Param(workspace, "isolNovoFrac");
  
  RooAbsReal& isolGNovoSigma  = Param(workspace, "isolGNovoSigma", "", "", 1);
 
  
  RooAbsPdf* GaussianTermBkg = new RooGaussian(name + "_jetIsolG", "Gaussian term Isolation PDF for jets", isol, 
                          isolNovoMu, isolGNovoSigma);
  RooAbsPdf* NovoTermBkg = new RooNovosibirsk(name + "_jetIsolNovo", "Novo term Isolation PDF for jets", isol,
                          isolNovoMu, isolNovoSigma, isolNovoLambda);
  
  return new RooAddPdf(name + "_jetIsol", "Isolation PDF for jets", 
                       RooArgList (*NovoTermBkg, *GaussianTermBkg),RooArgList( isolNovoFrac));


}
