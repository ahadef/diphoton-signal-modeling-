#include "HGGfitter/Hgg2sCBPdfBuilder.h"

#include "RooGaussian.h"
#include "HGGfitter/HggTwoSidedCBPdf.h"
#include "RooAddPdf.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void Hgg2sCBPdfBuilder::Setup(const char* depName, const char* cbPeakName, const char* cbSigmaName, 
                                 const char* cbAlphaLoName, const char* cbNLoName,
                                 const char* cbAlphaHiName, const char* cbNHiName)
{ 
  m_depName = depName; 
  m_cbPeakName = cbPeakName; 
  m_cbSigmaName = cbSigmaName; 
  m_cbAlphaLoName = cbAlphaLoName; 
  m_cbAlphaHiName = cbAlphaHiName; 
  m_cbNLoName = cbNLoName;
  m_cbNHiName = cbNHiName;
}


RooAbsPdf* Hgg2sCBPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = *Dependent(dependents, m_depName, "GeV", "m_{ee}");

  if (m_cbAlphaLoName == "") {
    return new RooGaussian(name, "Double-sided Crystal Ball PDF", mgg,
                           Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"));
  }
  RooAbsPdf* cbPdf = new HggTwoSidedCBPdf(name, "Double-sided Crystal Ball PDF", mgg,
                                      Param(workspace, m_cbPeakName, "GeV"), Param(workspace, m_cbSigmaName, "GeV"), 
                                      Param(workspace, m_cbAlphaLoName), Param(workspace, m_cbNLoName),
                                      Param(workspace, m_cbAlphaHiName), Param(workspace, m_cbNHiName));
  return cbPdf;
}
