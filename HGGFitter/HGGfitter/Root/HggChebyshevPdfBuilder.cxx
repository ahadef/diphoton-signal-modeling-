#include "HGGfitter/HggChebyshevPdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "RooAddPdf.h"
#include "RooConstVar.h"
#include "RooChebychev.h"

using namespace Hfitter;


void HggChebyshevPdfBuilder::Setup(const char* depName, double xMin, double xMax,
                                   const char* cs1Name, const char* cs2Name, const char* cs3Name, 
                                   const char* cs4Name, const char* cs5Name, const char* cs6Name, const char* cs7Name)
{
  m_depName = depName;
  m_xMin = xMin;
  m_xMax = xMax;
  if (TString(cs1Name) != "") m_csNames.push_back(cs1Name); else return;
  if (TString(cs2Name) != "") m_csNames.push_back(cs2Name); else return;
  if (TString(cs3Name) != "") m_csNames.push_back(cs3Name); else return;
  if (TString(cs4Name) != "") m_csNames.push_back(cs4Name); else return;
  if (TString(cs5Name) != "") m_csNames.push_back(cs5Name); else return;
  if (TString(cs6Name) != "") m_csNames.push_back(cs6Name); else return;
  if (TString(cs7Name) != "") m_csNames.push_back(cs7Name); else return;
}


void HggChebyshevPdfBuilder::Setup(const char* depName,
                                   const char* cs1Name, const char* cs2Name, const char* cs3Name, 
                                   const char* cs4Name, const char* cs5Name, const char* cs6Name, const char* cs7Name)
{
  Setup(depName, 0, -1, cs1Name, cs2Name, cs3Name, cs4Name, cs5Name, cs6Name, cs7Name);
}


void HggChebyshevPdfBuilder::Setup(const char* depName, double xMin, double xMax, unsigned int order, const char* csNameRoot)
{
  m_depName = depName;
  m_xMin = xMin;
  m_xMax = xMax;
  for (unsigned int i = 1; i <= order; i++) m_csNames.push_back(Form("%s%d", csNameRoot, i));
}


void HggChebyshevPdfBuilder::Setup(const char* depName, unsigned int order, const char* csNameRoot)
{
  Setup(depName, 0, -1, order, csNameRoot);
}


RooAbsPdf* HggChebyshevPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  RooArgList coeffs;
  for (std::vector<TString>::const_iterator csName = m_csNames.begin(); csName != m_csNames.end(); csName++)
    coeffs.add(Param(workspace, *csName,  ""));

  // Not implemeted yet
  //   if (m_xMin < m_xMax)
  //     return new HggBernstein(name, Form("Background PDF for mgg using order-%d Bernstein polynomials", int(m_csNames.size())), 
  //                             mgg, coeffs, m_xMin, m_xMax);
  //   else
  //     return new RooBernstein(name, Form("Background PDF for mgg using order-%d Bernstein polynomials", int(m_csNames.size())), mgg, coeffs);

  return new RooChebychev(name, "Chebyshev polynomial PDF", mgg, coeffs);
}
