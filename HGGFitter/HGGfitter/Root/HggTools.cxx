// $Id: HftTools.cxx,v 1.34 2009/01/30 16:25:12 hoecker Exp $   
// Author: Mohamed Aharrouche, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggTools.h"

#include <iostream>
#include "TH1.h"
#include "TFile.h"
#include "TString.h"
#include "TChain.h"
#include "TSystem.h"
#include "TPRegexp.h"
#include "RooRealVar.h"
#include "RooDataSet.h"
#include "RooDataHist.h"
#include "RooPlot.h"
#include "RooCurve.h"
#include "RooHist.h"
#include "TMath.h"
#include "TLorentzVector.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TLine.h"
#include "TLatex.h"
#include "TROOT.h"
#include "TLegend.h"
#include "RooAbsCategory.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftModelBuilder.h"
// #include "TreeReader/MultiReader.h"
// #include "TreeReader/TVector3Reader.h"

using std::cout;
using std::endl;

using namespace Hfitter;
// using namespace TreeReader;


void HggTools::listBranches(TTree* tree, const char* branches)
{
  TPRegexp reg(branches);
  TObjArray* bl = tree->GetListOfBranches();

  for (int i = 0; i < bl->GetEntries(); i++) {
    TString name = bl->At(i)->GetName();
    if (name.Contains(reg)) cout << name << endl;
  }
}


RooDataSet* HggTools::dataSet(const char* fileNames, const HftModel& model, const char* cut,
			               const char* weightVar)
{
  TChain* chain = new TChain("tree");
  chain->Add(fileNames);
  TFile::Open(Form("/tmp/temp_%d.root", gSystem->GetPid()), "RECREATE");

  RooArgList list(model.ObservablesAndCats());

  if (TString(weightVar) != "") {
	RooRealVar* weight = new RooRealVar("weight", "", 1, -1, 1);
	list.add(*weight);	
  }

  return new RooDataSet(Form("%s_dataSet", model.GetName().Data()), "", chain, list, cut, weightVar);
}


RooDataHist* HggTools::dataHist(const char* fileNames, const HftModel& model, const char* cut)
{
  TChain* chain = new TChain("tree");
  chain->Add(fileNames);
  TFile::Open(Form("/tmp/temp_%d.root", gSystem->GetPid()), "RECREATE");
  TTree* tree = chain->CopyTree("");
  RooDataSet* dataSet = new RooDataSet(Form("%s_dataSet", model.GetName().Data()),
                                       "", tree, model.Observables(), cut);
  return new RooDataHist(Form("%s_dataHist", model.GetName().Data()), "",  model.Observables(), *dataSet);
}


RooPlot* HggTools::plotVars( HftModel& model, RooAbsData& data,
                            const char* saveTo)
{
   bool save = (TString(saveTo) != "");
   RooArgList deps = model.Observables();
   gROOT->cd();

   gStyle->SetOptTitle(0);
   
   TCanvas* c  = (TCanvas*)gROOT->FindObject("Hfitter");
   TCanvas* c1 = (TCanvas*)gROOT->FindObject("Hfitter2");

   if (!c) { 
     c = new TCanvas( "Hfitter", "Hfitter", 0, 0, 500*deps.getSize(), 500 ); 
     c->Divide( deps.getSize(), 1 );
   }
   
   if (!c1) c1 = new TCanvas( "Hfitter2", "Hfitter", 0, 0, 500, 500 ); 

   RooPlot* retPlot = 0;
   TFile* outputFile = 0;
   
   if (save) {
     outputFile = TFile::Open(TString(saveTo) + ".root", "RECREATE");
     gROOT->cd();
   }
     
   for (int i = 0; i < deps.getSize(); i++) {
     c->cd( i + 1 );
     cout << deps[i].GetName() << " (" << i+1 << " of " << deps.getSize() << ")" << endl;
     
     RooPlot* plot = model.Plot(deps[i].GetName(), data);
     plot->Draw();
     retPlot = plot;
     
     c1->cd();
     plot->Draw();
     if (save) {
       c1->Print(TString(saveTo) + "_" + deps[i].GetName() + ".eps");
       outputFile->cd();
       plot->Write();
       gROOT->cd();
     }
   }

   c->Update();
   if (save) c->Print(TString(saveTo) + ".eps");
   if (outputFile) outputFile->Close();
   
   return retPlot;
}


TCanvas* HggTools::plotCats(HftModel& model, const TString& var, RooAbsData& data, const TString& catName,
                            HftModel* other, unsigned int nLines, const RooAbsData::ErrorType& errorType,
                            const TString& ranges)
{
  RooAbsCategory* cat = (RooAbsCategory*)model.Workspace()->cat(catName);
  if (!cat) {
    cout << "ERROR : category " << catName << " not defined" << endl;
    return 0;
  }
  
  if (nLines == 0) nLines = (unsigned int)TMath::Sqrt(cat->numTypes());
  unsigned int nCol = cat->numTypes()/nLines;
  if (cat->numTypes() % nLines != 0) nCol++;
  TCanvas* c1 = new TCanvas("cats", var + " in bins of " + catName, 900, 900);
  c1->Divide(nCol, nLines);

  TLatex text;
  text.SetNDC();
  text.SetTextSize(0.05);

  double totalChi2 = 0;
  int totalNDof = 0;
  
  std::vector<RooAbsData*> datas;
  for (int i = 0; i < cat->numTypes(); i++) {
    TString catState = cat->lookupType(i)->GetName();
    TString cut = catName + Form("==%d", i);
    RooAbsData* catData = data.reduce(cut);
    datas.push_back(catData);
    c1->cd(i+1);
    RooPlot* f = model.Plot(var, *catData, Options(PlotErrorsOpt(errorType), ranges != "" ? Options("Range(" + ranges + ")") : Options()), other);
    RooCurve* curve = f->getCurve(var + "_pdf");
    RooHist* hist = f->getHist(var + "_data");
    int ndof = f->GetNbinsX();
    double chi2 = curve->chiSquare(*hist, 0)*ndof;
    cout << "Chi2/n for this category = " << chi2 << "/" << ndof << endl;
    totalChi2 += chi2;
    totalNDof += ndof;
    text.DrawText(0.6, 0.7, catState);
  }
  cout << "Total Chi2/n = " << totalChi2 << "/" << totalNDof << endl;
  return c1;
}


bool HggTools::MakeAnimation(std::vector<Hfitter::HftModel*>& models, const std::vector<TString>& labels, 
                             const TString& parName, unsigned int nBins, double parMin, double parMax,
                             const TString& outputFileName, double yMax)
{
  TLine l;
  TCanvas* c1 = new TCanvas("c1", "", 800, 600);
  double posAmp = (parMax - parMin)/2;
  
  int colors[4] = {1,4,2,6};
  
  gSystem->MakeDirectory("HggTemp");
  for (unsigned int i = 0; i <= 4*nBins; i++) {
    int ii = 0;
    if (i < nBins) ii = i;
    else if (i < 3*nBins) ii = 2*nBins - i;
    else ii = i - 4*nBins;
    double pos = posAmp/nBins*ii;

    TLegend* leg = new TLegend(0.7, 0.5, 0.9, 0.2, "", "NDC");
    leg->SetFillColor(0);
    
    RooPlot* f = models[0]->Var("mgg")->frame();
    f->SetTitle("Variations of " + parName);
    for (unsigned int k = 0; k < models.size(); k++) {
      models[k]->Var(parName)->setRange(parMin, parMax);
      models[k]->Var(parName)->setVal(pos);
      RooDataSet* asimov = models[k]->GenerateAsimov();
      //asimov->plotOn(f);
      models[k]->Pdf()->plotOn(f,  RooFit::LineColor(colors[k % 4]), RooFit::ProjWData(*asimov), RooFit::Normalization(asimov->sumEntries(), RooAbsReal::NumEvent));
      TH1D* h = new TH1D();
      h->SetName(Form("modelHist%d", k));
      h->SetLineColor(colors[k % 4]);
      h->SetLineWidth(2);
      leg->AddEntry(h, labels[k]);
    }
    f->GetYaxis()->SetTitle("");
    if (yMax > 0) f->SetMaximum(yMax);
    f->Draw();
    
    double scaleMin = 0.65, scaleMax = 0.85, scaleY = 0.80, arrowH = 0.02, arrowW = 0.01, tickH = 0.01;
    double scaleCen = 0.5*(scaleMin + scaleMax);
    double scalePos = scaleCen + pos/posAmp*(scaleMax - scaleCen);
    l.DrawLineNDC(scaleMin, scaleY,  scaleMax, scaleY);
    l.DrawLineNDC(scaleMin, scaleY + tickH, scaleMin, scaleY - tickH);
    l.DrawLineNDC(scaleCen, scaleY + tickH, scaleCen, scaleY - tickH);
    l.DrawLineNDC(scaleMax, scaleY + tickH, scaleMax, scaleY - tickH);
    
    l.DrawLineNDC(scalePos, scaleY, scalePos + arrowW, scaleY + arrowH);
    l.DrawLineNDC(scalePos, scaleY, scalePos - arrowW, scaleY + arrowH);
    l.DrawLineNDC(scalePos - arrowW, scaleY + arrowH, scalePos + arrowW, scaleY + arrowH);
    
    TLatex txt;
    txt.DrawTextNDC(scaleMin - 0.01, scaleY - 0.05, Form("%g", parMin));
    txt.DrawTextNDC(scaleMax - 0.01, scaleY - 0.05, Form("%g", parMax));
    txt.DrawTextNDC(scaleMin, scaleY - 0.1, Form("%s", parName.Data()));
    leg->Draw();
    c1->Print(Form("HggTemp/frame%05i.png", i));
  }
  gSystem->Exec(Form("convert -delay 20 -loop 0 HggTemp/frame*.png %s && rm -rf HggTemp", outputFileName.Data()));
  return true;
}
