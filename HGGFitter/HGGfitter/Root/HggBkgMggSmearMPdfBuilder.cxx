// $Id: HggBkgMggSmearMPdfBuilder.cxx,v 1.2 2007/01/23 15:27:34 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggBkgMggSmearMPdfBuilder.h"
#include "RooGaussian.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooFormulaVar.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggSmearMPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf = m_base.Pdf(name + "_mgg", dependents, workspace);

  RooAbsReal& dm = *Dependent(dependents, m_dep2Name, "GeV", "#delta m_{#gamma#gamma}");
  RooAbsReal& smearX0 = Param(workspace, "smearX0", "", "", -5.16);
  RooAbsReal& smearS0 = Param(workspace, "smearS0", "", "",  1.91);
  RooAbsReal& smearX1 = Param(workspace, "smearX1", "", "", -0.626);
  RooAbsReal& smearS1 = Param(workspace, "smearS1", "", "",  1.60);
  RooAbsReal& smearFL = Param(workspace, "smearFL", "", "",  0.284);
  RooAbsReal& smearSG = Param(workspace, "smearSG", "", "",  0.5);

  RooFormulaVar* smearX2 = new RooFormulaVar("smearX2", "", "-26 - @0", RooArgList(smearX0));
  RooFormulaVar* smearX3 = new RooFormulaVar("smearX3", "", "-26 - @0", RooArgList(smearX1));

  RooGaussian* g0 = new RooGaussian(name + "_smearG0", "", dm,  smearX0, smearS0);
  RooGaussian* g1 = new RooGaussian(name + "_smearG1", "", dm,  smearX1, smearS1);
  RooGaussian* g2 = new RooGaussian(name + "_smearG2", "", dm, *smearX2, smearS0);
  RooGaussian* g3 = new RooGaussian(name + "_smearG3", "", dm, *smearX3, smearS1);

  RooAddPdf* add01 = new RooAddPdf(name + "_add01", "", RooArgList(*g0, *g1), smearFL);
  RooAddPdf* add23 = new RooAddPdf(name + "_add23", "", RooArgList(*g2, *g3), smearFL);
  RooAddPdf* smear = new RooAddPdf(name + "_smear", "", RooArgList(*add01, *add23), smearSG);

  return new RooProdPdf(name, "", RooArgList(*mggPdf, *smear));
}
