// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace, Kerstin Tackmann

#include "HGGfitter/HggSigMggPTPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

using namespace Hfitter;

RooAbsPdf* HggSigMggPTPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsPdf* mggPdf = m_mggPdfBuilder.Pdf(name + "_mgg", dependents, workspace);
  RooAbsPdf* pTPdf = m_pTPdfBuilder.Pdf(name + "_pT", dependents, workspace);

  return new RooProdPdf(name, "Signal PDF for mgg and pT", 
			RooArgSet(*mggPdf, *pTPdf));
}
