// $Id: HggPowerLaw.cxx,v 1.4 2007/12/13 20:34:23 hoecker Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HGGfitter/HggPowerLaw.h"
#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "TMath.h"

using namespace Hfitter;


HggPowerLaw::HggPowerLaw(const HggPowerLaw& other, const char* name)
   : RooAbsPdf( other, name ),
     m_x  ("x",        this, other.m_x),
     m_exp("exponent", this, other.m_exp)
{}

HggPowerLaw::HggPowerLaw(const char* name, const char* title, 
                               RooAbsReal& x, RooAbsReal& exponent)
   : RooAbsPdf( name, title ),
     m_x    ("x",        "Dependent",   this, x),
     m_exp  ("exponent", "Exponent",    this, exponent)
{}


Double_t HggPowerLaw::evaluate() const
{
   return TMath::Power(m_x, m_exp);
}


Int_t HggPowerLaw::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* /*rangeName*/ ) const 
{
   if (matchArgs(allVars,analVars,m_x)) return 1;
   return 0;
}


Double_t HggPowerLaw::analyticalIntegral( Int_t code, const char* rangeName ) const 
{
   switch(code) {
   case 1: 

     double integral =
     (TMath::Power(m_x.max(rangeName), m_exp + 1) -
     TMath::Power(m_x.min(rangeName), m_exp + 1))/(m_exp + 1);

     return integral;
   }
   
  assert(0);
  return 0;
}
