#include "HGGfitter/HggCouplings.h"

#include "TMath.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


double HggCouplings::mu_t(double kappa_f, double /*kappa_V*/)
{
  return kappa_f;
}


double HggCouplings::mu_VBF(double /*kappa_f*/, double kappa_V)
{
  return kappa_V;
}


double HggCouplings::mu_VH(double /*kappa_f*/, double kappa_V)
{
  return kappa_V;
}

