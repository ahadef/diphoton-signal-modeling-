// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace, Kerstin Tackmann

#include "HGGfitter/HggBkgMggPTPdfBuilder.h"

#include "RooProdPdf.h"
#include "RooArgList.h"
#include "RooArgSet.h"
#include "RooRealVar.h"

using namespace Hfitter;

RooAbsPdf* HggBkgMggPTPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooRealVar& mgg = (RooRealVar&)*dependents.find("mgg");
  RooRealVar& pT = (RooRealVar&)*dependents.find("pT");
  
  RooAbsPdf* mggPdf = m_mggPdfBuilder.Pdf(name + "_mgg", RooArgList(mgg), workspace);
  RooAbsPdf* pTPdf = m_pTPdfBuilder.Pdf(name + "_pT", RooArgList(pT), workspace);

  return new RooProdPdf(name, "Background PDF for mgg and pT", 
                        RooArgSet(*mggPdf, *pTPdf));
}
