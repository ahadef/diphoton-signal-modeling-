#include "AtlasStyle.C"
#include "AtlasUtils.C"
#include <stdio.h>

double fitf(double *x, double *par);

void ATLASLabel(Double_t x,Double_t y,const char* text="",Color_t color=1, Double_t tsize=0.05);

Double_t getIQ(TH1F* h);

void Plot_Systematics(int N_points=0, double y_min=0, double y_max=0, double ratio_min=0, double ratio_max=0, const Double_t * Mass=0, const Double_t * Rms=0, const Double_t * Error_Rms=0, const Double_t * Rms_up=0, const Double_t * Error_Rms_up=0, const Double_t * Rms_down=0, const Double_t * Error_Rms_down=0, const Double_t * Var=0, const Double_t * Var_up=0, const Double_t * Var_down=0, const Double_t * Error_Var_up=0, const Double_t * Error_Var_down=0, const char * legend_up ="", const char * legend_down="", const char * Ytitle="", const char * output_name = "");

void test_sys_resolution(int N_points=8){

  SetAtlasStyle();

  Double_t Mass[]={200,400,800,1200,2000,2400,3000,4000};
  Double_t Rms[N_points], 	Rms_up[N_points], 	Rms_down[N_points];
  Double_t Var[N_points], 	Var_up[N_points], 	Var_down[N_points];
  Double_t Error_Rms[N_points], Error_Rms_up[N_points], Error_Rms_down[N_points];
  Double_t Error_Var[N_points], Error_Var_up[N_points], Error_Var_down[N_points];
  Double_t Mean[N_points], 	Mean_up[N_points], 	Mean_down[N_points];
  Double_t Var_Mean[N_points], 	Var_Mean_up[N_points], 	Var_Mean_down[N_points];
  Double_t Error_Mean[N_points], Error_Mean_up[N_points], Error_Mean_down[N_points];
  Double_t Error_Var_Mean[N_points], Error_Var_Mean_up[N_points], Error_Var_Mean_down[N_points];
  char buffer[500];

  //for(int i=0;i<3; i++){
  for(int i=0;i<N_points; i++){


    snprintf(buffer,500,"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024b/mc16a/PhotonSys/mc16a.aMCnloPy8_X0toyy_%.f_NW.MxAODPhotonSys.e5107_s3126_r9364_p3705.h024b.root",Mass[i]);
    if(Mass[i]==4000 || Mass[i]==3000)
      snprintf(buffer,500,"/eos/atlas/atlascerngroupdisk/phys-higgs/HSG1/MxAOD/h024b/mc16a/PhotonSys/mc16a.aMCnloPy8_X0toyy_%.f_NW.MxAODPhotonSys.e6917_s3126_r9364_p3705.h024b.root",Mass[i]);

    TFile * file_data = TFile::Open(buffer);
    TTree *tree= (TTree*)file_data->Get("CollectionTree");
    int N_tot = tree->GetEntries();
    cout<<" N_tot= "<<N_tot<<endl;

    char nominal_mass_range_min[100], nominal_mass_range_max[100];
    char up_mass_range_min[100],      up_mass_range_max[100];
    char down_mass_range_min[100],    down_mass_range_max[100];

    snprintf(nominal_mass_range_min, 100, "HGamEventInfoAuxDyn.m_yy>%.f",Mass[i]*1000/1.17);
    snprintf(nominal_mass_range_max, 100, "HGamEventInfoAuxDyn.m_yy<%.f",Mass[i]*1000*1.15);
    //resolution
    snprintf(up_mass_range_min, 100, "HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.m_yy>%.f",Mass[i]*1000/1.17);
    snprintf(up_mass_range_max, 100, "HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.m_yy<%.f",Mass[i]*1000*1.15);
    snprintf(down_mass_range_min, 100, "HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.m_yy>%.f",Mass[i]*1000/1.17);
    snprintf(down_mass_range_max, 100, "HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.m_yy<%.f",Mass[i]*1000*1.15);
    //scale
    //snprintf(up_mass_range_min, 100, "HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.m_yy>%.f",Mass[i]*1000/1.17);
    //snprintf(up_mass_range_max, 100, "HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.m_yy<%.f",Mass[i]*1000*1.15);
    //snprintf(down_mass_range_min, 100, "HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.m_yy>%.f",Mass[i]*1000/1.17);
    //snprintf(down_mass_range_max, 100, "HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.m_yy<%.f",Mass[i]*1000*1.15);

    TCut cut_nominal_mass_min =  nominal_mass_range_min;
    TCut cut_nominal_mass_max =  nominal_mass_range_max;
    TCut cut_up_mass_min      =  up_mass_range_min;
    TCut cut_up_mass_max      =  up_mass_range_max;
    TCut cut_down_mass_min    =  down_mass_range_min;
    TCut cut_down_mass_max    =  down_mass_range_max;


    tree->Draw("HGamEventInfoAuxDyn.m_yy>>h_mgg","HGamEventInfoAuxDyn.isPassedHighMyy" && cut_nominal_mass_min && cut_nominal_mass_max);
    //resolution
    tree->Draw("HGamEventInfo_EG_RESOLUTION_ALL__1upAuxDyn.m_yy>>h_mgg_sys_up","HGamEventInfoAuxDyn.isPassedHighMyy" && cut_up_mass_min && cut_up_mass_max);
    tree->Draw("HGamEventInfo_EG_RESOLUTION_ALL__1downAuxDyn.m_yy>>h_mgg_sys_down","HGamEventInfoAuxDyn.isPassedHighMyy" && cut_down_mass_min && cut_down_mass_max);
    //scale
    //tree->Draw("HGamEventInfo_EG_SCALE_ALL__1upAuxDyn.m_yy>>h_mgg_sys_up","HGamEventInfoAuxDyn.isPassedHighMyy" && cut_up_mass_min && cut_up_mass_max);
    //tree->Draw("HGamEventInfo_EG_SCALE_ALL__1downAuxDyn.m_yy>>h_mgg_sys_down","HGamEventInfoAuxDyn.isPassedHighMyy" && cut_down_mass_min && cut_down_mass_max);

    TH1F * h_mgg          = (TH1F*)gDirectory->Get("h_mgg");
    TH1F * h_mgg_sys_up   = (TH1F*)gDirectory->Get("h_mgg_sys_up");
    TH1F * h_mgg_sys_down = (TH1F*)gDirectory->Get("h_mgg_sys_down");

    //normalise hist to the unit
    Double_t norm = 1;
    Double_t scale_mgg = norm/(h_mgg->Integral());
    Double_t scale_mgg_sys_up = norm/(h_mgg_sys_up->Integral());
    Double_t scale_mgg_sys_down = norm/(h_mgg_sys_down->Integral());

    h_mgg->Scale(scale_mgg);
    h_mgg_sys_up->Scale(scale_mgg_sys_up);
    h_mgg_sys_down->Scale(scale_mgg_sys_down);

    TCanvas *c1 = new TCanvas("c", "canvas", 800, 800);
    // Upper plot will be in pad1
    TPad *pad1 = new TPad("pad1", "pad1", 0, 0.3, 1, 1.0);
    pad1->SetBottomMargin(0); // Upper and lower plot are joined
    pad1->Draw();             // Draw the upper pad: pad1
    pad1->cd();               // pad1 becomes the current pad




    h_mgg_sys_down->SetLineColor(kBlue);
    h_mgg_sys_down->SetMarkerColor(kBlue);
    h_mgg_sys_down->Draw("hist");

   h_mgg_sys_up->SetLineColor(kRed);
    h_mgg_sys_up->SetMarkerColor(kRed);
    h_mgg_sys_up->Draw("hist same");

    h_mgg->SetMarkerStyle(2);
    h_mgg->Draw("hist same");


    //add legend
    auto legend = new TLegend(0.2,0.7,0.4,0.9);
    legend->AddEntry(h_mgg,"nominal","p");
    legend->AddEntry(h_mgg_sys_up,"resolution up","p");
    legend->AddEntry(h_mgg_sys_down,"resolution down","p");
    legend->SetBorderSize(0);
    legend->Draw();

    // get mean of histograms
    Double_t mean_mgg             = h_mgg->GetMean();
    Double_t mean_mgg_sys_up      = h_mgg_sys_up->GetMean();
    Double_t mean_mgg_sys_down    = h_mgg_sys_down->GetMean();

    // get RMS of histograms
    Double_t rms_mgg           	  = h_mgg->GetRMS();
    Double_t rms_mgg_sys_up    	  = h_mgg_sys_up->GetRMS();
    Double_t rms_mgg_sys_down  	  = h_mgg_sys_down->GetRMS();

    // get 68% IQ of histogrmas

    Double_t iq_mgg           	  = getIQ(h_mgg);
    Double_t iq_mgg_sys_up         = getIQ(h_mgg_sys_up);
    Double_t iq_mgg_sys_down       = getIQ(h_mgg_sys_down);

    // get standar errors 
    Double_t error_mean_mgg_sys      = h_mgg->GetMeanError();
    Double_t error_mean_mgg_sys_up   = h_mgg_sys_up->GetMeanError();
    Double_t error_mean_mgg_sys_down = h_mgg_sys_down->GetMeanError();

    // get rms errors 
    Double_t error_rms_mgg_sys      = h_mgg->GetRMSError();
    Double_t error_rms_mgg_sys_up   = h_mgg_sys_up->GetRMSError();
    Double_t error_rms_mgg_sys_down = h_mgg_sys_down->GetRMSError();

    // get mean variations
    Double_t mean_variation_up      	  = 100*abs(mean_mgg_sys_up-mean_mgg)/mean_mgg;
    Double_t mean_variation_down    	  = 100*abs(mean_mgg_sys_down-mean_mgg)/mean_mgg;
    Double_t error_variation_mean_up      = mean_variation_up*error_mean_mgg_sys/mean_mgg;
    Double_t error_variation_mean_down    = mean_variation_down*error_mean_mgg_sys/mean_mgg;

    // get rms variations
    Double_t variation_up      	    = 100*abs(rms_mgg_sys_up-rms_mgg)/rms_mgg;
    Double_t variation_down    	    = 100*abs(rms_mgg_sys_down-rms_mgg)/rms_mgg;
    Double_t error_variation_up     = variation_up*error_rms_mgg_sys/rms_mgg;
    Double_t error_variation_down   = variation_down*error_rms_mgg_sys/rms_mgg;

    // get iq variations
    Double_t iq_variation_up           = 100*abs(iq_mgg_sys_up-iq_mgg)/iq_mgg;
    Double_t iq_variation_down         = 100*abs(iq_mgg_sys_down-iq_mgg)/iq_mgg;
    Double_t error_iq_variation_up     = iq_variation_up*error_rms_mgg_sys/iq_mgg;
    Double_t error_iq_variation_down   = iq_variation_down*error_rms_mgg_sys/iq_mgg;

    cout << "mean_mgg = "            << mean_mgg            << ";     rms_mgg = "          << rms_mgg          << ";     68% iq_mgg = "          << iq_mgg        <<endl;
    cout << "mean_mgg_sys_up = "     << mean_mgg_sys_up     << ";     rms_mgg_sys_up = "   << rms_mgg_sys_up   << ";     68% iq_mgg_sys_up = "            << iq_mgg_sys_up  <<endl;
    cout << "mean_mgg_sys_down = "   << mean_mgg_sys_down   << ";     rms_mgg_sys_down = " << rms_mgg_sys_down << ";     68% iq_mgg_sys_down = "          << iq_mgg_sys_down <<endl;
/*  
    // fill vector of RMS vs mass
    Rms[i]	    = rms_mgg/1000;
    Rms_up[i]	    = rms_mgg_sys_up/1000;
    Rms_down[i]	    = rms_mgg_sys_down/1000;
    Error_Rms[i]      = error_rms_mgg_sys/1000;
    Error_Rms_up[i]   = error_rms_mgg_sys_up/1000;
    Error_Rms_down[i] = error_rms_mgg_sys_down/1000;
    Var[i]	    = 0;
    Var_up[i]  	    = (rms_mgg_sys_up   - rms_mgg)/rms_mgg;
    Var_down[i]	    = (rms_mgg_sys_down - rms_mgg)/rms_mgg;
    Error_Var_up[i]   = error_variation_up/1000;
    Error_Var_down[i] = error_variation_down/1000;
*/
    // fill vector of RMS vs mass
    Rms[i]	    = iq_mgg/1000;
    Rms_up[i]	    = iq_mgg_sys_up/1000;
    Rms_down[i]	    = iq_mgg_sys_down/1000;
    Error_Rms[i]      = error_rms_mgg_sys/1000;
    Error_Rms_up[i]   = error_rms_mgg_sys_up/1000;
    Error_Rms_down[i] = error_rms_mgg_sys_down/1000;
    Var[i]	    = 0;
    Var_up[i]  	    = (iq_mgg_sys_up   - iq_mgg)/iq_mgg;
    Var_down[i]	    = (iq_mgg_sys_down - iq_mgg)/iq_mgg;
    Error_Var_up[i]   = error_variation_up/1000;
    Error_Var_down[i] = error_variation_down/1000;

    // fill vector of mean vs mass
    Mean[i]	    = (mean_mgg/1000) - Mass[i];
    Mean_up[i]	    = (mean_mgg_sys_up/1000) - Mass[i];
    Mean_down[i]	    = (mean_mgg_sys_down/1000) - Mass[i];
    Error_Mean[i]	    = error_mean_mgg_sys/1000;
    Error_Mean_up[i]   = error_mean_mgg_sys_up/1000;
    Error_Mean_down[i] = error_mean_mgg_sys_down/1000;
    Var_Mean[i]	    = 0;
    Var_Mean_up[i]  	    = (mean_mgg_sys_up   - mean_mgg)/mean_mgg;
    Var_Mean_down[i]	    = (mean_mgg_sys_down - mean_mgg)/mean_mgg;
    Error_Var_Mean_up[i]   = error_variation_mean_up/1000;
    Error_Var_Mean_down[i] = error_variation_mean_down/1000;

    //show rms in the plot
    char rms1[100], rms2[100], rms3[100], sys_up[100], sys_down[100], mass_point[100];
    char iq1[100], iq2[100], iq3[100], iq_sys_up[100], iq_sys_down[100];
    snprintf(rms1,     100, "rms nominal = %.f\n",rms_mgg);
    snprintf(rms2,     100, "rms up      = %.f\n",rms_mgg_sys_up);
    snprintf(rms3,     100, "rms down    = %.f\n",rms_mgg_sys_down);
    snprintf(sys_up,   100, "sys up      = %.2f \% \n",variation_up);
    snprintf(sys_down, 100, "sys down    = %.2f \% \n",variation_down);
    snprintf(iq1,     100, "68 % interquartile nominal = %.f\n",iq_mgg);
    snprintf(iq2,     100, "68% interquartile up      = %.f\n",iq_mgg_sys_up);
    snprintf(iq3,     100, "68% interquartile down    = %.f\n",iq_mgg_sys_down);
    snprintf(iq_sys_up,   100, "68% iq sys up      = %.2f \% \n",iq_variation_up);
    snprintf(iq_sys_down, 100, "68% iq sys down    = %.2f \% \n",iq_variation_down);
    snprintf(mass_point,     100, "mass point:  %.f GeV\n",Mass[i]);

    TLatex latex;
    latex.SetTextSize(0.045);
    latex.SetTextAlign(13);  //align at top
    latex.DrawLatexNDC(.6,.9,mass_point);
    latex.DrawLatexNDC(.6,.8,rms1);
    latex.SetTextColor(kRed);
    latex.DrawLatexNDC(.6,.7,rms2);
    latex.SetTextColor(kBlue);
    latex.DrawLatexNDC(.6,.6,rms3);
    latex.SetTextColor(kBlack);
    latex.DrawLatexNDC(.6,.5,sys_up);
    latex.DrawLatexNDC(.6,.4,sys_down);

    latex.DrawLatexNDC(.6,.8,iq1);
    latex.SetTextColor(kRed);
    latex.DrawLatexNDC(.6,.7,iq2);
    latex.SetTextColor(kBlue);
    latex.DrawLatexNDC(.6,.6,iq3);
    latex.SetTextColor(kBlack);
    latex.DrawLatexNDC(.6,.5,iq_sys_up);
    latex.DrawLatexNDC(.6,.4,iq_sys_down);

    // lower plot will be in pad
    c1->cd();          // Go back to the main canvas before defining pad2
    TPad *pad2 = new TPad("pad2", "pad2", 0, 0.08, 1, 0.3);
    pad2->SetTopMargin(0);
    pad2->SetBottomMargin(0.);
    pad2->SetGridx(); // vertical grid
    pad2->Draw();
    pad2->cd();       // pad2 becomes the current pad
    // Define the ratio plot
    TH1F *h_ratio = (TH1F*)h_mgg->Clone("h_ratio");
    h_ratio->SetLineColor(kRed);
    h_ratio->SetMarkerColor(kRed);

    h_ratio->SetMinimum(0.1);  // Define Y ..
    h_ratio->SetMaximum(6.0); // .. range
    h_ratio->Sumw2();
    h_ratio->SetStats(0);      // No statistics on lower plot
    h_ratio->Divide(h_mgg_sys_up);
    h_ratio->SetMarkerStyle(21);
    h_ratio->GetXaxis()->SetTitle("m_{X} [GeV]");
    h_ratio->GetXaxis()->SetTitleSize(0.1);
    h_ratio->GetXaxis()->SetLabelSize(0.1);


    TH1F *h_ratio_down = (TH1F*)h_mgg->Clone("h_ratio_down");
    h_ratio_down->SetLineColor(kBlue);
    h_ratio_down->SetMarkerColor(kBlue);

    h_ratio_down->SetMinimum(0.1);  // Define Y ..
    h_ratio_down->SetMaximum(6.0); // .. range
    h_ratio_down->Sumw2();
    h_ratio_down->SetStats(0);      // No statistics on lower plot
    h_ratio_down->Divide(h_mgg_sys_down);
    h_ratio_down->SetMarkerStyle(21);
    h_ratio_down->GetXaxis()->SetTitle("m_{X} [GeV]");
    h_ratio_down->GetXaxis()->SetTitle("var/nominal");
    h_ratio_down->GetXaxis()->SetTitleSize(0.1);
    h_ratio_down->GetXaxis()->SetLabelSize(0.1);
    h_ratio_down->Draw("hist");       // Draw the ratio plot
    h_ratio->Draw("hist same");       // Draw the ratio plot

    char output_name[50];
    snprintf(output_name, 50, "sys_%.fGeV.pdf", Mass[i]);
    snprintf(output_name, 50, "sys_%.fGeV.png", Mass[i]);
    c1->SaveAs(output_name);
  }

  // plot rms vs mass
  char output_sys_name[50];
  //snprintf(output_sys_name, 50, "rms-vs-mx.pdf");     //rms
  snprintf(output_sys_name, 50, "iq-vs-mx.pdf");	//iq
  snprintf(output_sys_name, 50, "iq-vs-mx.png");	//iq
  Plot_Systematics(N_points, -0.999, 44.990, -0.49, 0.59, Mass, Rms, Error_Rms, Rms_up, Error_Rms_up, Rms_down, Error_Rms_down, Var, Var_up, Var_down, Error_Var_up, Error_Var_down, "resolution up", "resolution down", "IQ^{68%} [GeV]", output_sys_name);

  // plot mean vs mass
  for(int i=0; i<N_points; i++) {
    cout<<"mean["<<i<<"]= "<<Mean[i]<<" +/- "<<Error_Mean[i]<<endl;
    cout<<"mean up["<<i<<"]= "<<Mean_up[i]<<" +/- "<<Error_Mean_up[i]<<endl;
    cout<<"var_mean up["<<i<<"]= "<<Var_Mean_up[i]<<" +/- "<<Error_Var_Mean_up[i]<<endl;
    cout<<"mean down["<<i<<"]= "<<Mean_down[i]<<" +/- "<<Error_Mean_down[i]<<endl;
    cout<<"var_mean down["<<i<<"]= "<<Var_Mean_down[i]<<" +/- "<<Error_Var_Mean_down[i]<<endl;
  }

  //snprintf(output_sys_name, 50, "mean-vs-mx.pdf");		//mean
  //Plot_Systematics(N_points, -41, 41, -0.01, 0.01, Mass, Mean, Error_Mean, Mean_up, Error_Mean_up, Mean_down, Error_Mean_down, Var_Mean, Var_Mean_up, Var_Mean_down, Error_Var_Mean_up, Error_Var_Mean_down, "scale up", "scale down", "Mean - m_{X} [GeV]", output_sys_name);

}

//====================//
double fitf(double *x,double *par) {

  double fitval = par[0] - par[1]*TMath::Exp(-par[2]*x[0]);

  return fitval;
}

//====================//
void ATLASLabel(Double_t x,Double_t y,const char* text,Color_t color, Double_t tsize) 
{
  TLatex l; //l.SetTextAlign(12); 
  l.SetTextSize(tsize); 
  l.SetNDC();
  l.SetTextFont(72);
  l.SetTextColor(color);

  double delx = 0.115*696*gPad->GetWh()/(472*gPad->GetWw());

  l.DrawLatex(x,y,"ATLAS");
  if (text) {
    TLatex p; 
    p.SetNDC();
    p.SetTextFont(42);
    p.SetTextColor(color);
    p.SetTextSize(tsize);
    p.DrawLatex(x+delx*.6,y,text);
  }
}

//=======================//
void Plot_Systematics(int N_points, double y_min, double y_max, double ratio_min, double ratio_max, const Double_t * Mass, const Double_t * Rms, const Double_t * Error_Rms, const Double_t * Rms_up, const Double_t * Error_Rms_up, const Double_t * Rms_down, const Double_t * Error_Rms_down, const Double_t * Var, const Double_t * Var_up, const Double_t * Var_down, const Double_t * Error_Var_up, const Double_t * Error_Var_down, const char * legend_up, const char * legend_down, const char * Ytitle, const char * output_name){
  TCanvas *c2 = new TCanvas("c", "canvas", 800, 800);
  // Upper plot will be in pad1
  TPad *pad3 = new TPad("pad3", "pad3", 0, 0.4, 1, 1.0);
  pad3->SetBottomMargin(0); // Upper and lower plot are joined
  //pad1->SetGridx();         // Vertical grid
  pad3->Draw();             // Draw the upper pad: pad1
  pad3->cd();               // pad1 becomes the current pad

  TGraphErrors* gr_rms = new TGraphErrors(N_points,Mass,Rms,0,Error_Rms);
  gr_rms->GetXaxis()->SetTitle("m_{X} [GeV]");
  gr_rms->GetYaxis()->SetTitle(Ytitle);
  gr_rms->GetYaxis()->SetRangeUser(y_min, y_max);
  gr_rms->Draw("A*");
  gr_rms->Fit("pol1");

  TGraphErrors* gr_rms_up = new TGraphErrors(N_points,Mass,Rms_up,0,Error_Rms_up);
  gr_rms_up->SetLineColor(4);
  gr_rms_up->SetMarkerColor(4);
  gr_rms_up->SetMarkerSize(1.);
  gr_rms_up->SetMarkerStyle(21);
  gr_rms_up->Draw("same p");
  gr_rms_up->Fit("pol1");
  gr_rms_up->GetFunction("pol1")->SetLineColor(4);

  TGraphErrors* gr_rms_down = new TGraphErrors(N_points,Mass,Rms_down,0,Error_Rms_down);
  gr_rms_down->SetLineColor(2);
  gr_rms_down->SetMarkerColor(2);
  gr_rms_down->SetMarkerSize(1.);
  gr_rms_down->SetMarkerStyle(20);
  gr_rms_down->Draw("same p");
  gr_rms_down->Fit("pol1");
  gr_rms_down->GetFunction("pol1")->SetLineColor(2);

  //add legend
  //auto legend = new TLegend(0.6,0.1,0.9,0.3);		//rms
  auto legend = new TLegend(0.6,0.05,0.9,0.25);		//68% IQ
  //auto legend = new TLegend(0.2,0.05,0.5,0.25);   //mean
  legend->AddEntry(gr_rms,"nominal","pl");
  legend->AddEntry(gr_rms_up,legend_up,"pl");
  legend->AddEntry(gr_rms_down,legend_down,"pl");
  legend->SetBorderSize(0);
  legend->Draw();

  ATLASLabel(0.2,0.85,"Simulation Internal",1,0.05);

  // lower plot will be in pad
  c2->cd();          // Go back to the main canvas before defining pad2
  TPad *pad4 = new TPad("pad4", "pad4", 0, 0.08, 1, 0.4);
  pad4->SetTopMargin(0);
  pad4->SetBottomMargin(0.4);
  pad4->SetGridx(); // vertical grid
  pad4->SetGridy(); // horizontal grid
  pad4->Draw();
  pad4->cd();       // pad2 becomes the current pad

  TGraphErrors* gr_var = new TGraphErrors(N_points,Mass,Var,0,0);
  gr_var->GetXaxis()->SetTitle("m_{X} [GeV]");
  gr_var->GetXaxis()->SetTitleSize(0.1);
  gr_var->GetXaxis()->SetLabelSize(0.1);
  gr_var->GetYaxis()->SetTitle("Variation");
  gr_var->GetYaxis()->SetTitleSize(0.1);
  gr_var->GetYaxis()->SetTitleOffset(0.6);
  gr_var->GetYaxis()->SetLabelSize(0.09);
  gr_var->GetYaxis()->SetRangeUser(ratio_min, ratio_max);
  gr_var->GetYaxis()->SetNdivisions(20208);
  gr_var->Draw("AC*");

  cout<<"var_up: fit to exp:"<<endl;
  TGraphErrors* gr_var_up = new TGraphErrors(N_points,Mass,Var_up,0,Error_Var_up);
  gr_var_up->SetLineColor(4);
  gr_var_up->SetMarkerColor(4);
  gr_var_up->SetMarkerSize(1.);
  gr_var_up->SetMarkerStyle(21);
  gr_var_up->Draw("same p");
  //gr_var_up->Fit("expo");
  TF1  *fit = new TF1("function",fitf,200,4000,3);
  fit->SetParameters(0.4, 0.4, 0.002);			//rms , iq
  //fit->SetParameters(0.1, 0.4, 0.002); 		//mean
  gr_var_up->Fit("function");

  cout<<"var_down: fit to exp:"<<endl;
  TGraphErrors* gr_var_down = new TGraphErrors(N_points,Mass,Var_down,0,Error_Var_down);
  gr_var_down->SetLineColor(2);
  gr_var_down->SetMarkerColor(2);
  gr_var_down->SetMarkerSize(1.);
  gr_var_down->SetMarkerStyle(20);
  gr_var_down->Draw("same p");
  fit->SetParameters(-0.2, 0.4, 0.002);
  gr_var_down->Fit("function");


  c2->SaveAs(output_name);
}

Double_t getIQ(TH1F* h)
{

  const int NQ = 3;
  Double_t xq[ NQ ] = { 0.16, 0.84, 1};  // position where to compute the quantiles in [0,1]
  Double_t yq[ NQ ];  // array to contain the quantiles
  for (Int_t i=0;i<NQ;i++) {
    yq[i] = -999;
  }
  
  h->GetQuantiles(NQ,yq,xq);
  for (Int_t i=0;i<NQ;i++) {
    cout << i << " " << yq[i] << endl;
  }
  cout << " --- " << endl;
  for (Int_t i=0;i<NQ;i++) {
    if (i==0)
      cout << i << " " << h->Integral( 1, h->FindBin(yq[i])) << endl;
    else
      cout << i << " " << h->Integral( h->FindBin(yq[i-1]), h->FindBin(yq[i]) ) << endl;
  }
  Double_t IQ = (yq[1]-yq[0])/2;
  cout << " range containing ~68% of signal " << yq[0] << "--" << yq[1] << endl;
  cout << " (integral) " << h->Integral( h->FindBin(yq[0]), h->FindBin(yq[1]) ) << endl;
  cout << "68% interquartile: ("<<  yq[1]<<"-"<<yq[0]<<")/2 = "<< IQ<<endl;
  cout << "RMS = "<<h->GetRMS()<<endl;
/*  
  TCanvas c1;
  h->GetXaxis()->SetRangeUser(960,1040);
  cout << "StdDev = "<<h->GetStdDev()<<endl;  
  h->DrawCopy();

  TLine *l1[ NQ ] ;
  TLatex *t = new TLatex();
  t->SetNDC();
  t->SetTextFont(43);    
  t->SetTextSize(26);

  for (Int_t i=0;i<NQ;i++) {
    l1[i] = new TLine( yq[i], 0, yq[i], h->GetMaximum());
    l1[i]->SetLineStyle(2);
    l1[i]->SetLineColor(kRed);
    l1[i]->Draw();

    float iqe = (i==0) ? xq[i] : xq[i]-xq[i-1];
    t->DrawLatex( 0.22+i*0.25, 0.5, Form("%g",iqe));
  }
  
  c1.Print("iq.pdf");
*/
  return (IQ);
  
}


