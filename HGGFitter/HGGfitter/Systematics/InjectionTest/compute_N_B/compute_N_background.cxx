void compute_N_background(float mx = 200)
{

  TFile *f = TFile::Open("template_yyOnly_fixStitch.root");
  TH1D *h = (TH1D*) f->Get("h");
  
  //float mx = 200;
  float s = 2.5;
  s = 1.7 + 0.564 * ((mx-100)/100);

  float Ndata = 433655; //don't change;
  //int i1 = h->FindBin( mx-2*s );
  //int i2 = h->FindBin( mx+2*s );

  int i1 = h->FindBin(mx*0.75);
  int i2 = h->FindBin(mx*1.5);

  float b = h->Integral( i1, i2 )/h->Integral()*Ndata;

  cout << mx << "GeV:	 min = "<< mx*0.75 << " ;	 max = "<< mx*1.5 << endl;
  cout << mx << "GeV:	 sigma = "<< s << ";	 B = "<< b << endl;

}
