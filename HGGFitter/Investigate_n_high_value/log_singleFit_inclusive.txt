
[1mRooFit v3.60 -- Developed by Wouter Verkerke and David Kirkby[0m 
                Copyright (C) 2000-2013 NIKHEF, University of California & Stanford University
                All rights reserved, please read http://roofit.sourceforge.net/license.txt

INFO : Hfitter libraries successfully loaded.
INFO : TreeReader library successfully loaded.
INFO : HGGfitter library successfully loaded.

Processing ../Tools/SignalParametrizationRun2/fit_singleMassMC.C(9999,165504,"inclusive")...

Applying ATLAS style settings...

check list of availiable mass points
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
../Tools/SignalParametrizationRun2/MC165504/MGMC15.txt
11
 point: 60
-------- mH: 60; up: 66; mHlow: 54;  n_bins= 24;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG60.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG60.dat 
[#1] INFO:Eval -- RooAbsReal::attachToTree(mgg) TTree Float_t branch mgg will be converted to double precision
[#1] INFO:Eval -- RooAbsReal::attachToTree(weightN) TTree Float_t branch weightN will be converted to double precision
[#1] INFO:Eval -- RooTreeDataStore::loadValues(tree_dataSet) Ignored 20 out of range events
  1) RooRealVar::       dmX = 0.114778 +/- 0.0308826
  2) RooRealVar::   cbSigma = 1.15775 +/- 0.0455474
  3) RooRealVar:: cbAlphaLo = 1.99028 +/- 0.407158
  4) RooRealVar::     cbNLo = 3.77915 +/- 3.41784
  5) RooRealVar:: cbAlphaHi = 1.59625 +/- 0.206592
  6) RooRealVar::     cbNHi = 14972.9 +/- 300199
  7) RooRealVar::   nSignal = 4049.66 +/- 81.8682
######   var: 60, min: -1e+30 - max 1e+30
######   var: 0.114778, min: -1 - max 1
######   var: 1.15775, min: 0 - max 40
######   var: 1.99028, min: 0.5 - max 6
######   var: 3.77915, min: 0 - max 50000
######   var: 1.59625, min: 0.5 - max 10
######   var: 14972.9, min: 0 - max 1e+06
 point: 70
-------- mH: 70; up: 77; mHlow: 63;  n_bins= 28;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG70.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG70.dat 
  1) RooRealVar::       dmX = 0.0852089 +/- 0.0287855
  2) RooRealVar::   cbSigma = 1.18894 +/- 0.0341532
  3) RooRealVar:: cbAlphaLo = 1.50161 +/- 0.138347
  4) RooRealVar::     cbNLo = 6.61558 +/- 3.96249
  5) RooRealVar:: cbAlphaHi = 1.26699 +/- 0.0851507
  6) RooRealVar::     cbNHi = 8844.86 +/- 3.83541
  7) RooRealVar::   nSignal = 5890.13 +/- 100.359
######   var: 70, min: -1e+30 - max 1e+30
######   var: 0.0852089, min: -1 - max 1
######   var: 1.18894, min: 0 - max 40
######   var: 1.50161, min: 0.5 - max 6
######   var: 6.61558, min: 0 - max 50000
######   var: 1.26699, min: 0.5 - max 10
######   var: 8844.86, min: 0 - max 1e+06
 point: 80
-------- mH: 80; up: 88; mHlow: 72;  n_bins= 32;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG80.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG80.dat 
  1) RooRealVar::       dmX = 0.0994924 +/- 0.0495634
  2) RooRealVar::   cbSigma = 1.37527 +/- 0.0738335
  3) RooRealVar:: cbAlphaLo = 1.69482 +/- 0.368647
  4) RooRealVar::     cbNLo = 6.57853 +/- 8.361
  5) RooRealVar:: cbAlphaHi = 1.64726 +/- 0.524994
  6) RooRealVar::     cbNHi = 25.8949 +/- 80.93
  7) RooRealVar::   nSignal = 7159.33 +/- 151.99
######   var: 80, min: -1e+30 - max 1e+30
######   var: 0.0994924, min: -1 - max 1
######   var: 1.37527, min: 0 - max 40
######   var: 1.69482, min: 0.5 - max 6
######   var: 6.57853, min: 0 - max 50000
######   var: 1.64726, min: 0.5 - max 10
######   var: 25.8949, min: 0 - max 1e+06
 point: 90
-------- mH: 90; up: 99; mHlow: 81;  n_bins= 36;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG90.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG90.dat 
  1) RooRealVar::       dmX = -0.0284659 +/- 0.135395
  2) RooRealVar::   cbSigma = 1.35776 +/- 0.268418
  3) RooRealVar:: cbAlphaLo = 1.2168 +/- 0.387793
  4) RooRealVar::     cbNLo = 14299.7 +/- 3.53415e+06
  5) RooRealVar:: cbAlphaHi = 1.22986 +/- 0.476885
  6) RooRealVar::     cbNHi = 4079.36 +/- 163072
  7) RooRealVar::   nSignal = 482.757 +/- 29.5762
######   var: 90, min: -1e+30 - max 1e+30
######   var: -0.0284659, min: -1 - max 1
######   var: 1.35776, min: 0 - max 40
######   var: 1.2168, min: 0.5 - max 6
######   var: 14299.7, min: 0 - max 50000
######   var: 1.22986, min: 0.5 - max 10
######   var: 4079.36, min: 0 - max 1e+06
 point: 100
-------- mH: 100; up: 110; mHlow: 90;  n_bins= 40;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG100.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG100.dat 
  1) RooRealVar::       dmX = 0.0973495 +/- 0.0849679
  2) RooRealVar::   cbSigma = 1.55018 +/- 0.0768225
  3) RooRealVar:: cbAlphaLo = 2.22833 +/- 0.389153
  4) RooRealVar::     cbNLo = 3.89363 +/- 1.71892
  5) RooRealVar:: cbAlphaHi = 1.28082 +/- 0.169711
  6) RooRealVar::     cbNHi = 5993.08 +/- 126211
  7) RooRealVar::   nSignal = 1227.74 +/- 49.1594
######   var: 100, min: -1e+30 - max 1e+30
######   var: 0.0973495, min: -1 - max 1
######   var: 1.55018, min: 0 - max 40
######   var: 2.22833, min: 0.5 - max 6
######   var: 3.89363, min: 0 - max 50000
######   var: 1.28082, min: 0.5 - max 10
######   var: 5993.08, min: 0 - max 1e+06
 point: 110
-------- mH: 110; up: 121; mHlow: 99;  n_bins= 44;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG110.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG110.dat 
  1) RooRealVar::       dmX = 0.0845518 +/- 0.0580813
  2) RooRealVar::   cbSigma = 1.59295 +/- 0.062493
  3) RooRealVar:: cbAlphaLo = 1.75001 +/- 0.282379
  4) RooRealVar::     cbNLo = 6.17177 +/- 4.72827
  5) RooRealVar:: cbAlphaHi = 1.46692 +/- 0.251321
  6) RooRealVar::     cbNHi = 65.7786 +/- 419.13
  7) RooRealVar::   nSignal = 2049.58 +/- 57.6139
######   var: 110, min: -1e+30 - max 1e+30
######   var: 0.0845518, min: -1 - max 1
######   var: 1.59295, min: 0 - max 40
######   var: 1.75001, min: 0.5 - max 6
######   var: 6.17177, min: 0 - max 50000
######   var: 1.46692, min: 0.5 - max 10
######   var: 65.7786, min: 0 - max 1e+06
 point: 120
-------- mH: 120; up: 132; mHlow: 108;  n_bins= 48;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG120.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG120.dat 
  1) RooRealVar::       dmX = -0.047371 +/- 0.0545294
  2) RooRealVar::   cbSigma = 1.50765 +/- 0.100892
  3) RooRealVar:: cbAlphaLo = 1.57931 +/- 0.302377
  4) RooRealVar::     cbNLo = 3.33104 +/- 1.53873
  5) RooRealVar:: cbAlphaHi = 1.13001 +/- 0.113629
  6) RooRealVar::     cbNHi = 29212 +/- 858206
  7) RooRealVar::   nSignal = 2716.79 +/- 66.7187
######   var: 120, min: -1e+30 - max 1e+30
######   var: -0.047371, min: -1 - max 1
######   var: 1.50765, min: 0 - max 40
######   var: 1.57931, min: 0.5 - max 6
######   var: 3.33104, min: 0 - max 50000
######   var: 1.13001, min: 0.5 - max 10
######   var: 29212, min: 0 - max 1e+06
 point: 140
-------- mH: 140; up: 154; mHlow: 126;  n_bins= 56;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG140.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG140.dat 
  1) RooRealVar::       dmX = 0.0478335 +/- 0.0497447
  2) RooRealVar::   cbSigma = 1.79883 +/- 0.0533585
  3) RooRealVar:: cbAlphaLo = 1.36018 +/- 0.096323
  4) RooRealVar::     cbNLo = 3096.62 +/- 113940
  5) RooRealVar:: cbAlphaHi = 1.45653 +/- 0.101439
  6) RooRealVar::     cbNHi = 1917.05 +/- 5539.85
  7) RooRealVar::   nSignal = 3496.68 +/- 75.8998
######   var: 140, min: -1e+30 - max 1e+30
######   var: 0.0478335, min: -1 - max 1
######   var: 1.79883, min: 0 - max 40
######   var: 1.36018, min: 0.5 - max 6
######   var: 3096.62, min: 0 - max 50000
######   var: 1.45653, min: 0.5 - max 10
######   var: 1917.05, min: 0 - max 1e+06
 point: 160
-------- mH: 160; up: 176; mHlow: 144;  n_bins= 64;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG160.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG160.dat 
  1) RooRealVar::       dmX = 0.036035 +/- 0.0485747
  2) RooRealVar::   cbSigma = 2.06484 +/- 0.0537233
  3) RooRealVar:: cbAlphaLo = 1.78774 +/- 0.181063
  4) RooRealVar::     cbNLo = 4.38292 +/- 1.9529
  5) RooRealVar:: cbAlphaHi = 1.76907 +/- 0.304209
  6) RooRealVar::     cbNHi = 162.648 +/- 2302.96
  7) RooRealVar::   nSignal = 4437.02 +/- 86.1864
######   var: 160, min: -1e+30 - max 1e+30
######   var: 0.036035, min: -1 - max 1
######   var: 2.06484, min: 0 - max 40
######   var: 1.78774, min: 0.5 - max 6
######   var: 4.38292, min: 0 - max 50000
######   var: 1.76907, min: 0.5 - max 10
######   var: 162.648, min: 0 - max 1e+06
 point: 180
-------- mH: 180; up: 198; mHlow: 162;  n_bins= 72;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG180.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG180.dat 
  1) RooRealVar::       dmX = 0.113749 +/- 0.0462137
  2) RooRealVar::   cbSigma = 2.15944 +/- 0.0445154
  3) RooRealVar:: cbAlphaLo = 1.65958 +/- 0.124181
  4) RooRealVar::     cbNLo = 4.47681 +/- 1.19792
  5) RooRealVar:: cbAlphaHi = 1.65675 +/- 0.130621
  6) RooRealVar::     cbNHi = 14.4683 +/- 11.6747
  7) RooRealVar::   nSignal = 5030.16 +/- 90.0178
######   var: 180, min: -1e+30 - max 1e+30
######   var: 0.113749, min: -1 - max 1
######   var: 2.15944, min: 0 - max 40
######   var: 1.65958, min: 0.5 - max 6
######   var: 4.47681, min: 0 - max 50000
######   var: 1.65675, min: 0.5 - max 10
######   var: 14.4683, min: 0 - max 1e+06
 point: 200
-------- mH: 200; up: 220; mHlow: 180;  n_bins= 80;   weights to use: weightN
 =======================     !!!!!!!!!!!!!!!!!!!!    ==================  
                      File       ../Tools/SignalParametrizationRun2/dummyMCinputSampleFolder/MC165504/HGGfitter_format_input/mc16a_13TeV_MG200.root 
                      dc_I       ../datacards/LowHighMassRun2/SP/MC165504/single_input.dat 
                      dc_O       ../datacards/LowHighMassRun2/SP/MC165504/output_inclusive/single_MG200.dat 
  1) RooRealVar::       dmX = 0.100813 +/- 0.0539706
  2) RooRealVar::   cbSigma = 2.24497 +/- 0.0702249
  3) RooRealVar:: cbAlphaLo = 1.38724 +/- 0.0784318
  4) RooRealVar::     cbNLo = 11.0029 +/- 2.70048
  5) RooRealVar:: cbAlphaHi = 1.47623 +/- 0.263985
  6) RooRealVar::     cbNHi = 72.1308 +/- 508.01
  7) RooRealVar::   nSignal = 5533.33 +/- 96.1436
######   var: 200, min: -1e+30 - max 1e+30
######   var: 0.100813, min: -1 - max 1
######   var: 2.24497, min: 0 - max 40
######   var: 1.38724, min: 0.5 - max 6
######   var: 11.0029, min: 0 - max 50000
######   var: 1.47623, min: 0.5 - max 10
######   var: 72.1308, min: 0 - max 1e+06
