#ifndef ROOT_Hfitter_HftPLRCalculator
#define ROOT_Hfitter_HftPLRCalculator

#include "HfitterStats/HftAbsHypoTestCalculator.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterStats/HftMLCalculator.h"
#include "HfitterStats/HftExpHypoTestCalc.h"

#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanPoint.h"

#include "RooFitResult.h"
#include "RooAbsData.h"
#include "TMath.h"


namespace Hfitter {

  /** @class HftPLRCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsCalculator to compute discovery significances.
  */
 
   
  class HftPLRCalculator : public HftAbsHypoTestCalculator {

   public:

    HftPLRCalculator(HftModel& model, const TString& pois, const Options& fitOptions = "", 
                     const HftParameterStorage& expectedState = HftParameterStorage(),
                     const HftParameterStorage& initialStateFree = HftParameterStorage(),
                     const HftParameterStorage& initialStateHypo = HftParameterStorage(),
                     const RooArgList& paramsToStore = RooArgList(),
                     const TString& name = "", bool resultIsCL = false, bool freeFirst = false);

    HftPLRCalculator(HftModel& model, const HftScanPoint& hypo, const Options& fitOptions = "", 
                     const HftParameterStorage& expectedState = HftParameterStorage(),
                     const HftParameterStorage& initialStateFree = HftParameterStorage(),
                     const HftParameterStorage& initialStateHypo = HftParameterStorage(),
                     const RooArgList& paramsToStore = RooArgList(), 
                     const TString& name = "", bool resultIsCL = false, bool freeFirst = false);

    HftPLRCalculator(HftModel& model, const HftScanPoint& testHypo, const HftScanPoint& compHypo, const Options& fitOptions = "",
                     const HftParameterStorage& expectedState = HftParameterStorage(),
                     const HftParameterStorage& initialStateFree = HftParameterStorage(),
                     const HftParameterStorage& initialStateHypo = HftParameterStorage(),
                     const RooArgList& paramsToStore = RooArgList(), 
                     const TString& name = "", bool resultIsCL = false, bool freeFirst = false);

    HftPLRCalculator(const HftPLRCalculator& other, const TString& name = "", const HftScanPoint& hypo = HftScanPoint(),
                     const HftAbsCalculator* cloner = 0);

    void Init(const Options& fitOptions, const HftParameterStorage& initialStateFree, const HftParameterStorage& initialStateHypo,
              const RooArgList& paramsToStore);

    HftParameterStorage DefaultExpectedState(); 

    virtual ~HftPLRCalculator();    
    void DeleteExpCalc();
        
    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* cloner = 0) const 
    { return new HftPLRCalculator(*this, name, hypo, cloner); }

    bool ComputeFromAsymptotics(double dll, double& result) const;

    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& stat) const { return GetPLR(stat); }
    bool GetPLR(double& plr) const;
    
    TString StatisticName()  const { return "-2.log(LR)"; }
    TString StatisticTitle() const { return "-2 Log-likelihood-ratio"; }

    virtual bool GetStatisticBand(int i, double& qBand) const;
    bool GetResultBand(int i, double& rBand) const;

    HftExpHypoTestCalc* ExpCalc() const { return m_expCalc; }
    bool GetExpPLR(double& q) const { return (ExpCalc() && ExpCalc()->GetExpPLR(q)) || (!ExpCalc() && GetPLR(q)); }
    bool GetSigma(double q, double& sig) const;
    // Can compute sigma from either PLR or from statistic (which gets changed for q, qtilda, etc.) but I think PLR is more appropriate
    bool GetExpSigma(double& sig) const { double q; return GetExpPLR(q) && GetSigma(q, sig); }
    
    bool MakeNullHypoBranches(HftTree& tree);
    bool MakeAltHypoBranches(HftTree& tree);
    bool FillNullHypoBranches(HftTree& tree);
    bool FillAltHypoBranches(HftTree& tree);
    
    bool LoadAltHypoInputs(RooAbsData& data);  // free
    bool LoadNullHypoInputs(RooAbsData& data); // hypo
    
    bool LoadAltHypoInputs(HftTree& tree);
    bool LoadNullHypoInputs(HftTree& tree);
    
    bool LoadAltHypoInputs(const HftAbsCalculator& other);
    bool LoadNullHypoInputs(const HftAbsCalculator& other);
    
    HftMLCalculator* FreeCalc() const { return m_freeCalc; }
    HftMLCalculator* HypoCalc() const { return m_hypoCalc; }

    HftMLCalculator* AltHypoCalc() const { return m_freeCalc; }
    HftMLCalculator* NullHypoCalc() const { return m_hypoCalc; }

    void SetChainFitStates(bool cfs) { m_chainFitStates = cfs; }
    bool ChainFitStates() const { return m_chainFitStates; }
    
    bool HasSeparateCompHypo() const { return m_compHypo.Vars().size() != 0; }
    const HftScanPoint& ComputationFree() const { return HasSeparateCompHypo() ? m_compFree : AltHypo(); }
    const HftScanPoint& ComputationHypo() const { return HasSeparateCompHypo() ? m_compHypo : NullHypo(); }
    void SetCompHypo(const HftScanPoint& compHypo);
    
    double FreeFitValue(unsigned int i) const { return FreeCalc()->FitValue(*HypoVar(i)); }
    double HypoFitValue(unsigned int i) const { return HypoCalc()->FitValue(*HypoVar(i)); }
    double CompHypoValue(unsigned int i) const { return ComputationHypo().State().Value(*HypoVar(i)); }
    double CompFreeValue(unsigned int i) const { return ComputationFree().State().Value(*HypoVar(i)); }
    double ExpValue(unsigned int i) const;

    // Shortcut to get significance directly from statistic 
    bool ComputeSignificance(double stat, double& z) const;

    TString Str(const TString& prefix = "", const TString& options = "") const;
    TString PLRStr(const TString& prefix = "", const TString& options = "") const;

    double HypoValue(unsigned int i) const { return NullHypoValue(i); }

    static double Sqr (double x) { return x*x; }
    static double SignedSqr (double x) { return x > 0 ? x*x : -x*x; }
    static double SignedSqrt(double x) { return x > 0 ? TMath::Sqrt(x) : -TMath::Sqrt(-x); }
    static double SqrtAbs(double x) { return x > 0 ? TMath::Sqrt(x) : TMath::Sqrt(-x); }
    static double SqrtPos(double x) { return x > 0 ? TMath::Sqrt(x) : 0; }

    static bool GetExpSigma(const HftAbsHypoTestCalculator& calc, double& sigma);
    static bool GetCompHypo(const HftAbsHypoTestCalculator& calc, HftScanPoint& hypo);
    static bool SetCompHypo(HftAbsHypoTestCalculator& calc, const HftScanPoint& hypo);

    void SetVerbosity(int verbosity) { m_freeCalc->SetVerbosity(verbosity); m_hypoCalc->SetVerbosity(verbosity); }
    int Verbosity() const { return FreeCalc()->Verbosity(); }

  protected:

    HftScanPoint m_compHypo, m_compFree;
    HftMLCalculator* m_freeCalc, *m_hypoCalc;
    HftExpHypoTestCalc* m_expCalc;
    HftParameterStorage m_fitInitialState;
    bool m_chainFitStates;
  };
}

#endif
