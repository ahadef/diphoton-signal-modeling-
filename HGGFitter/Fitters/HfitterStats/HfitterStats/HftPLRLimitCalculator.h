#ifndef ROOT_Hfitter_HftPLRLimitCalculator
#define ROOT_Hfitter_HftPLRLimitCalculator

#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterModels/HftModel.h"

namespace Hfitter {

  class HftPLRCalculator;
  
  /** @class HftPLRLimitCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsCLsbCalculator to compute exclusion limits using the 'q' estimator
  */
   
  class HftPLRLimitCalculator : public HftPLRCalculator
  {
   public:
     
    HftPLRLimitCalculator(HftModel& model, const TString& hypoVars, const Options& fitOptions = Options(), 
                            const HftParameterStorage& expectedState = HftParameterStorage(), 
                            const TString& name = "", bool freeFirst = false)
      : HftPLRCalculator(model, hypoVars, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                         RooArgList(), name, true, freeFirst) { }

    HftPLRLimitCalculator(HftModel& model, const HftScanPoint& hypo, const Options& fitOptions = Options(),
                          const HftParameterStorage& expectedState = HftParameterStorage(), const TString& name = "",
                          bool freeFirst = false)
      : HftPLRCalculator(model, hypo, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                         RooArgList(), name, true, freeFirst) { }

    HftPLRLimitCalculator(const HftPLRLimitCalculator& other, const TString& name = "", const HftScanPoint& hypo = HftScanPoint(), 
                          const HftAbsCalculator* cloner = 0) : HftPLRCalculator(other, name, hypo, cloner) { }

    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* cloner = 0) const 
    { return new HftPLRLimitCalculator(*this, name, hypo, cloner); }
             
    virtual ~HftPLRLimitCalculator() { }
        
    TString Str(const TString& prefix = "", const TString& options = "") const;    
  };
}

#endif
