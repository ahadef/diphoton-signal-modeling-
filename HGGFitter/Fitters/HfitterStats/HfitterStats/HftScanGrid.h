#ifndef ROOT_Hfitter_HftScanGrid
#define ROOT_Hfitter_HftScanGrid

#include "HfitterStats/HftScanPoint.h"
#include "TString.h"
#include "RooRealVar.h"
#include <map>
#include <vector>

class TH1D;
class TH2D;

namespace Hfitter {

  /** @class HftScanGrid
      @author Nicolas Berger

    A class containing information about a multidimensional scan
  */

  class HftScanGrid {

   public:

    //! default constructor 
    HftScanGrid() { }
    
    virtual ~HftScanGrid() { }

    //! add a point along a grid variable. If the variable is not defined yet in the grid, a new dimension is added
    //! @param var : the variable, @param val : the value at which to place the point.
    void Add(const RooRealVar& var, double val);
    
    //! add a range of equidistant points along a grid variable.
    //! @param var : the variable, @param nPoints : number of points to add
    //! @param minVal : the value at which to place the first point. @param maxVal : the value at which to place the last point.
    void Add(const RooRealVar& var, unsigned int nPoints, double minVal, double maxVal);

    void Add(const RooRealVar& var, const std::vector<double>& vals);
    
    //! returns the number of points
    unsigned int NPoints() const { return m_points.size(); }
    
    //! returns the point at the given index. @param i : point index.
    const HftScanPoint* Point(unsigned int i) const;
    
    //! returns the number of points defined along a specified variable. @param var : the variable name.
    unsigned int NPoints(const TString& var) const;
    
    unsigned int NVariables() const { return m_varPos.size(); }
    TString Variable(unsigned int i) const;
    
    const std::vector<double>& Positions(unsigned int i) const { return m_pos[i]; }
    
    TH1D* Empty1DHistogram(const TString& name = "h1") const;
    TH2D* Empty2DHistogram(const TString& name = "h2") const;

    static bool MakeBinEdges(const std::vector<double>& binCenters, std::vector<double>& binEdges);
    
    void DrawMarkers(int style = 2, int color = 1, double size = 2) const;
    
    void SetPoint(unsigned int i, const HftScanPoint point) { m_points[i] = point; }
    
   protected:

     std::map<TString, unsigned int> m_varPos; //!< mapping of variable name -> variable index
     std::vector<unsigned int> m_nPoints;      //!< number of points defined for each variable
     std::vector<Hfitter::HftScanPoint> m_points;       //!< vector of scan points
     
     std::vector< std::vector<double> > m_pos;
     
  };
}

#endif
