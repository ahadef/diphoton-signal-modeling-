#ifndef ROOT_Hfitter_HftLRCalculator
#define ROOT_Hfitter_HftLRCalculator

#include "HfitterStats/HftAbsCalculator.h"

#include "TString.h"
#include <vector>

class RooRealVar;
class TH1D;

namespace Hfitter {

  /** @class HftLRCalculator
      @author Nicolas Berger

  */  
  
  class HftLRCalculator : public HftAbsCalculator {
    
   public:
     
    HftLRCalculator(HftModel& model, const HftParameterStorage& sigState, const HftParameterStorage& bkgState, const TString& name = "")
      : HftAbsCalculator(model, name), m_sigState(sigState), m_bkgState(bkgState) { }
      
    HftLRCalculator(const HftLRCalculator& other, const TString& name = "")
      : HftAbsCalculator(other, name), m_sigState(other.m_sigState), m_bkgState(other.m_bkgState) { }

    virtual ~HftLRCalculator() { }

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftLRCalculator(*this, name); }
            
    HftParameterStorage SigState() const { return m_sigState; }
    HftParameterStorage BkgState() const { return m_bkgState; }
            
    const std::vector<double>& SigNLL() const { return m_sigNLL; }
    const std::vector<double>& BkgNLL() const { return m_bkgNLL; }
    const std::vector<double>& Weight() const { return m_weight; }
    
    TH1D* LRHistogram(const TString& name, unsigned int nBins = 100, double llrMin = 0, double llrMax = 1,  int which = 0);
    
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const;
    
    TString ResultName() const { return "LR"; }
    TString ResultTitle() const { return "Likelihood ratio"; }

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    bool SaveToFile(const TString& fileName, const TString& treeName = "tree");
    bool MakeBranches(HftTree& tree);      

    using HftAbsCalculator::FillBranches;
    bool FillBranches(HftTree& /*tree*/) { return true; } // never used
    bool FillBranches(HftTree& tree, unsigned int i);
    
    TString Str(const TString& prefix = "", const TString& options = "") const;

   protected:
    
    HftParameterStorage m_sigState, m_bkgState;
    std::vector<double> m_sigNLL, m_bkgNLL, m_weight;
  };
}

#endif
