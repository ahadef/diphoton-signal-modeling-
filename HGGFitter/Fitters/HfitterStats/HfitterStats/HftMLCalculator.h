#ifndef ROOT_Hfitter_HftMLCalculator
#define ROOT_Hfitter_HftMLCalculator

#include "HfitterStats/HftAbsCalculator.h"

#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/Options.h"
#include "HfitterStats/HftTree.h"

#include "RooFitResult.h"
#include "RooAbsData.h"


namespace Hfitter {

  /** @class HftMLCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsCalculator to compute discovery significances.
  */
 
   
  class HftMLCalculator : public HftAbsCalculator {

   public:

    HftMLCalculator(HftModel& model, const Options& fitOptions = "", const HftParameterStorage& initialState = HftParameterStorage(),
                    const RooArgList& paramsToStore = RooArgList(), const TString& name = "");

    HftMLCalculator(const HftMLCalculator& other, const TString& name = "");

    virtual ~HftMLCalculator();

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftMLCalculator(*this, name); }

    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { result = m_nll; return true; }
    TString ResultName() const { return "nll"; }
    TString ResultTitle() const { return "-log #cal{L}"; }

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    HftParameterStorage InitialState(unsigned int i = 0) const { return m_initialStates[i]; }
    HftParameterStorage FitState() const { return m_fitState; }

    RooFitResult* FitResult() const { return m_fitResult; }    
    double FitValue(const RooRealVar& var) const { return m_fitState.Value(var); }
    double FitError(const RooRealVar& var) const { return m_fitState.Error(var); }
    
    Options FitOptions() const { return m_fitOptions; }
    void SetFitOptions(const Options& fitOptions) { m_fitOptions = fitOptions; }

    RooArgList ParamsToStore() const { return m_paramsToStore; }
    
    TString Str(const TString& prefix = "", const TString& options = "") const;

    void SetInitialState(const HftParameterStorage& state) { m_initialStates[0] = state; }
    void AddInitialState(const HftParameterStorage& state) { m_initialStates.push_back(state); }
    int Verbosity() const { return m_fitOptions.Verbosity(); }
    void SetVerbosity(int verbosity);

  protected:
    std::vector<HftParameterStorage> m_initialStates;
    HftParameterStorage m_fitState;
    Options m_fitOptions;
    RooArgList m_paramsToStore;

    RooFitResult* m_fitResult;
    double m_nll;
    int m_status;
  };
}

#endif
