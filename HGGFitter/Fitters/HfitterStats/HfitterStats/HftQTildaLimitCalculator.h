#ifndef ROOT_Hfitter_HftQTildaLimitCalculator
#define ROOT_Hfitter_HftQTildaLimitCalculator

#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterModels/HftModel.h"


namespace Hfitter {

  /** @class HftQTildaLimitCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsCLsbCalculator to compute exclusion limits using the 'q' estimator
  */
   
  class HftQTildaLimitCalculator : public HftPLRCalculator
  {
   public:
     
    HftQTildaLimitCalculator(HftModel& model, const TString& hypoVar, const Options& fitOptions = Options(), 
                            const HftParameterStorage& expectedState = HftParameterStorage(), 
                            const TString& name = "", bool freeFirst = false);
    
    HftQTildaLimitCalculator(HftModel& model, const HftScanPoint& hypo, const Options& fitOptions = Options(),
                             const HftParameterStorage& expectedState = HftParameterStorage(), 
                             const TString& name = "", bool freeFirst = false);

    HftQTildaLimitCalculator(const HftQTildaLimitCalculator& other, const TString& name = "", const HftScanPoint& hypo = HftScanPoint(),
                             const HftAbsCalculator* cloner = 0);

    // backward compatibility 
    HftQTildaLimitCalculator(HftModel& model, const HftParameterStorage& asimovState,
                            const TString& signalIntensityVar, const Options& fitOptions = Options(), 
                            const TString& name = "", bool fakeAsimovFit = false, bool smallTree = false);
      
    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* cloner = 0)
    const { return new HftQTildaLimitCalculator(*this, name, hypo, cloner); }
             
    virtual ~HftQTildaLimitCalculator() { }

    //! Define CL variables. @param tree : the tree to add branches to, @param blockName : the common prefix for the branches.
    bool MakeAltHypoBranches(HftTree& tree);
       
    //! Fill CL variables. @param tree : the tree to fill, @param data : the dataset to study,
    //! @param blockName : the block name for the fitting point.
    bool FillAltHypoBranches(HftTree& tree);
    
    using HftAbsHypoTestCalculator::LoadAltHypoInputs;
    bool LoadAltHypoInputs(RooAbsData& data);
    bool LoadAltHypoInputs(HftTree& tree);
    bool LoadAltHypoInputs(const HftAbsCalculator& other);

    //! Calculate the exclusion confidence level.
    bool ComputeFromAsymptotics(double qTilda, double& cl) const;
    
    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& qTilda) const;
    bool GetStatisticBand(int i, double& qBand) const;

    TString StatisticName()  const { return "qTilda"; }
    TString StatisticTitle() const { return "#tilde{q}"; }
    TString ResultTitle() const { return "#tilde{CL}_{S+B}"; }

    HftMLCalculator* ZeroCalc() const { return m_zeroCalc; }

    TString Str(const TString& prefix = "", const TString& options = "") const;

    void SetVerbosity(int verbosity) { HftPLRCalculator::SetVerbosity(verbosity); m_zeroCalc->SetVerbosity(verbosity); }

   protected:
   
    HftMLCalculator* m_zeroCalc;
  };
}

#endif
