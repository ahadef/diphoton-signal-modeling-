#ifndef ROOT_Hfitter_HftOneSidedPValueCalculator
#define ROOT_Hfitter_HftOneSidedPValueCalculator

#include "HfitterStats/HftPLRPValueCalculator.h"


namespace Hfitter {

  /** @class HftOneSidedPValueCalculator
      @author Andreas Hoecker
      @author Nicolas Berger

    An implementation of @c HftAbsCalculator to compute discovery significances.
  */
 
  
  class HftOneSidedPValueCalculator : public HftPLRPValueCalculator {

   public:

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting.
    //! @param bkgOnly : true if background-only toys are to be generated. @param signalComponent : name of the signal component of the model.
    HftOneSidedPValueCalculator(HftModel& model, const TString& hypoVarName, 
                            const Options& fitOptions = Options(), const TString& name = "")
     : HftPLRPValueCalculator(model, hypoVarName, fitOptions, name) { }
    
    HftOneSidedPValueCalculator(HftModel& model, const TString& hypoVarName, double hypoValue,
                            const Options& fitOptions = Options(), const TString& name = "")
     : HftPLRPValueCalculator(model, hypoVarName, hypoValue, fitOptions, name) { }

     HftOneSidedPValueCalculator(const HftOneSidedPValueCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0)
      : HftPLRPValueCalculator(other, name, cloner) { }

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftOneSidedPValueCalculator(*this, name, cloner); }
            
    virtual ~HftOneSidedPValueCalculator() { }
    
    //! Calculate the value of the statistic with current inputs
    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& q0) const;
    bool GetStatisticBand(int i, double& tBand) const;

    //! Calculate the confidence level from the statistic, using either asymptotic formulas or sampling
    bool ComputeFromAsymptotics(double q0, double& p0) const;

    //! Not used for results, since we get significance directly from the statistic, Z = sqrt(q).
    double SignificanceFromPValue(double p0) const;

    TString StatisticName()  const { return "q0"; }
    TString StatisticTitle() const { return "q_{0}"; }
    
    TString Str(const TString& prefix = "", const TString& options = "") const;
  };
}

#endif
