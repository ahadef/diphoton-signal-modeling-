#ifndef ROOT_Hfitter_HftDiscreteTestCalculator
#define ROOT_Hfitter_HftDiscreteTestCalculator

#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftBand.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/Options.h"
#include "TString.h"
#include "TMatrixDSym.h"

namespace Hfitter {

  /** @class HftDiscreteTestCalculator
      @author Nicolas Berger
      Temporaray class to test dicrete profiling (to be added to the model in a clean implementation)

  */
  
  class HftMultiHypoTestCalculator;
  
  class HftDiscreteTestCalculator : public HftAbsHypoTestCalculator
  {
   public:

    HftDiscreteTestCalculator(HftModel& model, std::vector<Hfitter::HftPLRCalculator*> calcs, const TString& name = "");

    virtual ~HftDiscreteTestCalculator();

    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* cloner = 0) const;
    
    //! before toy loop: define model variables. @param model : the model to use for fitting, @param genPoint : the generation point.
    bool MakeAltHypoBranches(HftTree& tree);

    //! before toy loop: define fit point variables. @param tree : the tree where to create the branches,
    //! @param blockName : the branch name prefix.
    bool MakeNullHypoBranches(HftTree& tree);

    //! Fill branches for each toy iteration. @param tree : the tree where to fill the branches, @param data : the (toy) dataset to operate on,
    //! @param blockName : the branch name prefix. 
    bool FillAltHypoBranches(HftTree& tree);
    
    //! Fill branches at each scan point. @param tree : the tree where to fill the branches, @param data : the (toy) dataset to operate on,
    //! @param blockName : the branch name prefix. 
    bool FillNullHypoBranches(HftTree& tree);
 
    bool LoadAltHypoInputs(RooAbsData& data);
    bool LoadNullHypoInputs(RooAbsData& data);
    
    bool LoadAltHypoInputs(HftTree& tree);
    bool LoadNullHypoInputs(HftTree& tree);

    using HftAbsHypoTestCalculator::LoadNullHypoInputs;    
    bool LoadAltHypoInputs(const HftAbsCalculator& other);
    bool LoadNullHypoInputs(const HftAbsCalculator& other);

    unsigned int NCalculators() const { return m_calculators.size(); }
    HftPLRCalculator* Calculator(unsigned int i = 0) const { return m_calculators[i]; }

    HftPLRCalculator* NullHypoCalculator() const;
    HftPLRCalculator* AltHypoCalculator() const;
    
    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& q) const;
    
    TString StatisticName()  const { return m_calculators[0]->StatisticName(); }
    TString StatisticTitle() const { return m_calculators[0]->StatisticTitle(); }
    TString ResultTitle() const { return m_calculators[0]->ResultTitle(); } 
    
    TString Str(const TString& prefix = "", const TString& options = "") const;

    bool ComputeFromAsymptotics(double /*stat*/, double& /*result*/) const { return false; }
    
    int WhichResult() const { return m_whichResult; }
    void SetWhichResult(int wr) { m_whichResult = wr; }
    
    static bool SetWhichResult(HftMultiHypoTestCalculator& mhc, int wr); 
    
  protected:
    
    int m_whichResult;
    std::vector<Hfitter::HftPLRCalculator*> m_calculators;

  };
}

#endif
