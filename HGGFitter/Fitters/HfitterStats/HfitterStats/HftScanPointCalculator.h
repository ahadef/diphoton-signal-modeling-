#ifndef ROOT_Hfitter_HftScanPointCalculator
#define ROOT_Hfitter_HftScanPointCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftScanPoint.h"


namespace Hfitter {
  
  class HftScanPointCalculator : public HftAbsCalculator {

   public:

    //! constructor from a single model. This model is used for both generation and fitting. @param studyName : a name for this study. @param genModel : the model used for generation and fitting
    HftScanPointCalculator(const HftAbsCalculator& calculator, const HftScanPoint& point, const TString& name = "", bool updateState = true,
                           const HftAbsCalculator* cloner = 0);
    HftScanPointCalculator(const HftScanPointCalculator& calculator, const TString& name = "", const HftAbsCalculator* cloner = 0);

    virtual ~HftScanPointCalculator() { delete m_calculator; }

    virtual HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftScanPointCalculator(*this, name, cloner); }

    HftAbsCalculator* Calculator() const { return m_calculator; }
    HftScanPoint Point() const { return m_point; }

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree) { return Calculator()->LoadInputs(tree); }
    bool LoadInputs(const HftAbsCalculator& other);

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);

    //! Calculate the result with current inputs
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { return Calculator()->GetResult(result); }

    bool LoadAsimov();
    bool RunToys(unsigned int nToys, const TString& fileName, const TString& treeName = "tree", unsigned int saveInterval = 20);

    TString ResultName()  const { return Calculator()->ResultName(); }
    TString ResultTitle() const { return Calculator()->ResultTitle(); }

   protected:

    HftAbsCalculator* m_calculator;
    HftScanPoint m_point;
  };
}

#endif
