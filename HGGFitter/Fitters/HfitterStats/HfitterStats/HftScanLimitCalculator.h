#ifndef ROOT_Hfitter_HftScanLimitCalculator
#define ROOT_Hfitter_HftScanLimitCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftMultiHypoTestCalculator.h"


namespace Hfitter {

  class HftScanningCalculator;

  /** @class HftScanLimitCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsToyStudy to scan model parameters. Each call to HftAbsToyStudy::Execute is replaced by a loop performing an action for
    each point of the defined scanning grid (distinct from the generation grid used in HftAbsToyStudy). Two new virtual methods, both named ExecuteScan,
    are implemented. The first is executed once before the loop; the second, which takes the fit point as argument, is executed once for every iteration.
    The Initialize step is similarly modified.
  */

  class HftScanLimitCalculator : public HftAbsCalculator {

   public:
    
    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftScanLimitCalculator(const HftAbsHypoTestCalculator& calc, const TString& hypoVar, unsigned int nPoints = 1, double minHypo = 0, double maxHypo = 0, 
                           const TString& name = ""); 

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftScanLimitCalculator(const HftAbsHypoTestCalculator& calc, const TString& hypoVar, const std::vector<double>& points, 
                           const TString& name = ""); 

    HftScanLimitCalculator(const HftScanLimitCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftScanLimitCalculator(*this, name, cloner); }

    virtual ~HftScanLimitCalculator() { delete m_calculator; }
    
    //! for backward compat. 
    HftScanLimitCalculator(const HftAbsHypoTestCalculator& calc, unsigned int nPoints, double minHypo, double maxHypo, 
                           const TString& hypoVar = "", const TString& name = "", double cl = 0.95, int order = 2); 


    
    HftMultiHypoTestCalculator* Calculator() const { return m_calculator; }
    double SolvingCL() const { return m_cl; }
    int InterpolationOrder() const { return m_order; }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(HftInterval& signal) const;
    bool GetResult(double& signal) const { HftInterval erResult(0); if (!GetResult(erResult)) return false; signal = erResult.Value(); return true; }
    
    TString ResultName()  const { return Calculator()->Var()->GetName(); }
    TString ResultTitle() const { return Calculator()->Var()->GetTitle(); }
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree) { return Calculator()->LoadInputs(tree); }
    bool LoadInputs(const HftAbsCalculator& other) { return Calculator()->LoadInputs(other); }
    
    bool MakeBranches(HftTree& tree);

    using HftAbsCalculator::FillBranches;
    bool FillBranches(HftTree& tree);

    bool GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                              const TString& treeName = "tree", unsigned int saveInterval = 20) const
    { return Calculator()->GenerateSamplingData(state, nToys, fileRoot, treeName, saveInterval); }

    bool GenerateHypoSamplingData(unsigned int i, const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                  const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const
    { return Calculator()->GenerateHypoSamplingData(i, state, nToys, fileRoot, treeName, binned, saveInterval); }
    
    bool GenerateHypoSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                  const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const
    { return Calculator()->GenerateHypoSamplingData(state, nToys, fileRoot, treeName, binned, saveInterval); }

    bool SetSamplingData(const TString& fileRoot, const TString& treeName = "tree", const TString& nameInTree = "") 
    { return Calculator()->SetSamplingData(fileRoot, treeName, nameInTree); }

    bool UpdateState(const HftParameterStorage& toMerge) { return Calculator()->UpdateState(toMerge); }

    TString Str(const TString& prefix = "", const TString& options = "") const;
    
    bool DrawHypos(const TString& hypoPar, double xMin, double xMax) const;
    static bool DrawHypos(const HftScanningCalculator& scanCalc, const TString& hypoPar);
    
    void SetSolvingCL(double cl) { m_cl = cl; }
    void SetInterpolationOrder(int order) { m_order = order; }
    
    void SetVerbosity(int v) { m_calculator->SetVerbosity(v); }
    int Verbosity() const { return m_calculator->Verbosity(); }

  private:
    
    HftMultiHypoTestCalculator* m_calculator;
    double m_cl;
    int m_order;
  };
}

#endif
