#include "TString.h"
#include <vector>
#include <map>

#include "HfitterStats/HftInterval.h"
#include "HfitterStats/HftBand.h"

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanPoint.h"
#include "HfitterStats/HftScanGrid.h"

#include "HfitterStats/HftAbsReprocStudy.h"
#include "HfitterStats/HftScanPointCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"

#include "HfitterStats/HftMultiCalculator.h"
#include "HfitterStats/HftMaxFindingCalculator.h"

#include "HfitterStats/HftValueCalculator.h"
#include "HfitterStats/HftFitValueCalculator.h"
#include "HfitterStats/HftLRCalculator.h"
#include "HfitterStats/HftDataCountingCalculator.h"

#include "HfitterStats/HftAbsStateCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftToysCalculator.h"

template class std::vector<Hfitter::HftInterval>;
