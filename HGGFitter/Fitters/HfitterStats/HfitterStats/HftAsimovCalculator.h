#ifndef ROOT_Hfitter_HftAsimovCalculator
#define ROOT_Hfitter_HftAsimovCalculator

#include "HfitterStats/HftAbsStateCalculator.h"
#include "RooRealVar.h"
#include "TString.h"
#include <vector>

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  /** @class HftAsimovCalculator
      @author Nicolas Berger

  */  
  
  class HftAsimovCalculator : public HftAbsStateCalculator {
   public:
     
    HftAsimovCalculator(const HftAbsCalculator& calculator, const HftParameterStorage& state, const TString& name = "");
    HftAsimovCalculator(HftModel& genModel, const HftAbsCalculator& calculator, 
                        const HftParameterStorage& genState, const HftParameterStorage& fitState, const TString& name = "");
    HftAsimovCalculator(const HftAsimovCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);
    
    virtual ~HftAsimovCalculator();
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const { return new HftAsimovCalculator(*this, name, cloner); }

    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { return Calculator()->GetResult(result); }
    bool GetResult(HftInterval& result) const { return Calculator()->GetResult(result); }

    HftAbsCalculator* Calculator() const { return m_calculator; }
    const HftParameterStorage& FitState() const { return m_fitState; }
    
    bool LoadAsimov();
    bool LoadAsimov(const TString& fileName, const TString& treeName = "tree");

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data)  { return Calculator()->LoadInputs(data); }
    bool LoadInputs(HftTree& tree)  { return Calculator()->LoadInputs(tree); }
    bool LoadInputs(const HftAbsCalculator& other) { return Calculator()->LoadInputs(other); }

    double AsimovResult() { if (!LoadAsimov()) return 0; return Result(); }
    
    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree) { return Calculator()->MakeBranches(tree); }
    bool FillBranches(HftTree& tree) { return Calculator()->FillBranches(tree); }
    
    TString ResultName()  const { return Calculator()->ResultName(); }
    TString ResultTitle() const { return Calculator()->ResultTitle(); }
    TString Str(const TString& prefix = "", const TString& options = "") const;
        
    bool UpdateState(const HftParameterStorage& toMerge) { return HftAbsStateCalculator::UpdateState(toMerge) && Calculator()->UpdateState(toMerge); }

    void SetVerbosity(int verbosity) { m_calculator->SetVerbosity(verbosity); }
    int Verbosity() const { return Calculator()->Verbosity(); }

   private:

     HftAbsCalculator* m_calculator;
     HftParameterStorage m_fitState;
  };
}

#endif
