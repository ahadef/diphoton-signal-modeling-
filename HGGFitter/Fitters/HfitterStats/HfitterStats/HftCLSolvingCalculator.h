#ifndef ROOT_Hfitter_HftCLSolvingCalculator
#define ROOT_Hfitter_HftCLSolvingCalculator

#include "HfitterStats/HftScanLimitCalculator.h"

namespace Hfitter {
  typedef HftScanLimitCalculator HftCLSolvingCalculator;
}

#endif
