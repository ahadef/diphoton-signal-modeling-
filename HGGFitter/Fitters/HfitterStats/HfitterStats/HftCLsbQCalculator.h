#ifndef ROOT_Hfitter_HftCLsbQCalculator
#define ROOT_Hfitter_HftCLsbQCalculator

#include "HfitterStats/HftQLimitCalculator.h"

namespace Hfitter {
  typedef HftQLimitCalculator HftCLsbQCalculator;
}

#endif
