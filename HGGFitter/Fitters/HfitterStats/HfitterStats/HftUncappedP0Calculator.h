#ifndef ROOT_Hfitter_HftUncappedP0Calculator
#define ROOT_Hfitter_HftUncappedP0Calculator

#include "HfitterStats/HftUncappedPValueCalculator.h"

namespace Hfitter {
  typedef HftUncappedPValueCalculator HftUncappedP0Calculator;
}

#endif
