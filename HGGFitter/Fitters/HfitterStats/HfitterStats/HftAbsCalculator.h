#ifndef ROOT_Hfitter_HftAbsCalculator
#define ROOT_Hfitter_HftAbsCalculator

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftInterval.h"
#include "TString.h"
#include "TTree.h"
#include <cfloat>
#include <map>

class RooDataSet;
class RooAbsData;

namespace Hfitter {
  
  class HftTree;
  
  class HftAbsCalculator {

   public:

    //! constructor from a single model. This model is used for both generation and fitting. @param studyName : a name for this study. @param genModel : the model used for generation and fitting
    HftAbsCalculator(HftModel& model, const TString& name = "")   
      : m_model(&model), m_name(name != "" ? name : model.GetName()) { RegisterCalc(GetName(), *this); }

      HftAbsCalculator(const HftAbsCalculator& other, const TString& name = "")   
      : m_model(&other.Model()), m_name(name != "" ? name : other.GetName()) { RegisterCalc(GetName(), *this); }

    virtual ~HftAbsCalculator();
  
    virtual HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const = 0;

    HftModel& Model() const { return *m_model; }
    TString GetName() const { return m_name; }
    virtual void SetName(const TString& name) { DeregisterCalc(*this); m_name = name; RegisterCalc(GetName(), *this); }

    //! Calculate the result with current inputs
    virtual bool GetResult(double& result) const = 0;
    virtual TString ResultName() const = 0;
    virtual TString ResultTitle() const = 0;
    virtual TString ResultUnit() const { return ""; }

    virtual bool LoadInputs(RooAbsData& data) = 0;
    virtual bool LoadInputs(HftTree& tree) = 0;
    virtual bool LoadInputs(const HftAbsCalculator& other) = 0;
    virtual bool LoadInputs(RooAbsData& data, const TString& fileName, const TString& treeName = "tree");

    virtual bool MakeBranches(HftTree& tree) = 0;
    virtual bool FillBranches(HftTree& tree) = 0;
    virtual bool FillBranches(HftTree& tree, RooAbsData& data) { return LoadInputs(data) && FillBranches(tree); }

    virtual bool LoadFromFile(const TString& fileName, const TString& treeName = "tree");
    virtual bool SaveToFile(const TString& fileName, const TString& treeName = "tree");

    //! Calculate the result with current inputs, including errors
    virtual bool GetResult(HftInterval& result) const { double dr; bool status = GetResult(dr); result = HftInterval(dr); return status; }

    //! Calculate the result with current inputs, including errors
    virtual bool GetResult(int i, double& result) const { HftInterval ri = HftInterval::Make(abs(i)); if (!GetResult(ri)) return false; result = ri.Value(i); return true; }

    //! Calculate the result from a dataset
    virtual bool GetResult(RooAbsData& data, double& result) { return LoadInputs(data) && GetResult(result); }

    //! Calculate the result from a dataset
    virtual bool GetResult(RooAbsData& data, HftInterval& result) { return LoadInputs(data) && GetResult(result); }

    //! Calculate the value of the statistic with current inputs
    virtual bool GetResult(HftTree& tree, double& result) { return LoadInputs(tree) && GetResult(result); }

        //! Calculate the value of the statistic with current inputs
    virtual bool GetResult(HftTree& tree, HftInterval& result) { return LoadInputs(tree) && GetResult(result); }

    //! Calculate the confidence level of a dataset
    virtual double Result() const { double result; return GetResult(result) ? result : -DBL_MAX; }

    virtual HftInterval Result(unsigned int nErrors) const { HftInterval result = HftInterval::Make(nErrors); return GetResult(result) ? result : HftInterval(); }

    virtual HftInterval ResultInterval(unsigned int nErrors = 1) const { HftInterval result = HftInterval::Make(nErrors); return GetResult(result) ? result : HftInterval(); }

    //! Calculate the confidence level of a dataset
    virtual double Result(RooAbsData& data) { double result; return GetResult(data, result) ? result : -DBL_MAX; }

    //! recalculate the confidence level of a toy entry
    virtual double Result(HftTree& tree) { double result; return GetResult(tree, result) ? result : -DBL_MAX; }

    virtual int Verbosity() const { return 0; }
    virtual void SetVerbosity(int verbosity);

    virtual void Print(const TString& options = "") const;
    virtual TString Str(const TString& prefix = "", const TString& /*options*/ = "") const { return BaseStr(prefix, "HftAbsCalculator"); }
    TString BaseStr(const TString& prefix = "", const TString& type = "") const;

    virtual bool UpdateState(const HftParameterStorage& /*toMerge*/) { return true; }

    static bool RegisterCalc(const TString& name, HftAbsCalculator& calc);
    static bool DeregisterCalc(HftAbsCalculator& calc);
    static HftAbsCalculator* GetCalc(const TString& name, bool verbose = false);
    static void PrintCalcs(const TString& options = "");

    static TString MergeNames(const TString& name1, const TString& name2);
    TString AppendToName(const TString& suffix) const { return MergeNames(GetName(), suffix); }
    
   protected:

    mutable HftModel* m_model; //!< the model
    TString m_name;
    static std::map<TString, HftAbsCalculator*>* m_calculatorRegistry;
  };  
}

#endif
