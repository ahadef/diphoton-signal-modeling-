#ifndef ROOT_Hfitter_HftCLsbQTildaCalculator
#define ROOT_Hfitter_HftCLsbQTildaCalculator

#include "HfitterStats/HftQTildaLimitCalculator.h"

namespace Hfitter {
  typedef HftQTildaLimitCalculator HftCLsbQTildaCalculator;
}

#endif
