#ifndef ROOT_Hfitter_HftIterLimitCalculator
#define ROOT_Hfitter_HftIterLimitCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftAbsHypoTestCalculator.h"
#include "HfitterModels/HftParameterStorage.h"


namespace Hfitter {

  class HftScanningCalculator;

  /** @class HftIterLimitCalculator
      @author Bruno Lenzi
      @author Nicolas Berger

    An implementation of @c HftAbsToyStudy to scan model parameters. Each call to HftAbsToyStudy::Execute is replaced by a loop performing an action for
    each point of the defined scanning grid (distinct from the generation grid used in HftAbsToyStudy). Two new virtual methods, both named ExecuteScan,
    are implemented. The first is executed once before the loop; the second, which takes the fit point as argument, is executed once for every iteration.
    The Initialize step is similarly modified.
  */

  class HftIterLimitCalculator : public HftAbsCalculator {

   public:

    //! Constructor 
    HftIterLimitCalculator(const HftAbsHypoTestCalculator& calculator, unsigned int nBands = 0, double cl = 0.95,
                           double tolerance = 1E-4, const TString& name = ""); 
    HftIterLimitCalculator(const HftIterLimitCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const 
    { return new HftIterLimitCalculator(*this, name, cloner); }
    
    virtual ~HftIterLimitCalculator();

    HftAbsHypoTestCalculator* Calculator(int i) const { return m_calculators[i + NBands()]; }
    void ReplaceCalculator(int i, HftAbsHypoTestCalculator* newCalc) { delete m_calculators[i + NBands()]; m_calculators[i + NBands()] = newCalc; }
    
    RooRealVar* Var() const { return Calculator(0)->HypoVar(0); }

    double Target() const { return m_target; }
    void SetTarget(double t) { m_target = t;}
    
    double Tolerance() const { return m_tolerance; }
    void SetTolerance(double t) { m_tolerance = t;}

    unsigned int NIterMax() const { return m_nIterMax; }
    void SetNIterMax(unsigned int n) { m_nIterMax = n; }

    unsigned int NCompIterMax() const { return m_nCompIterMax; }
    void SetNCompIterMax(unsigned int n) { m_nCompIterMax = n; }

    bool ResultIsCL() const { return m_resultIsCL; }
    void SetResultIsCL(bool ricl = true) { m_resultIsCL = ricl; }

    int NBands() const { return int(m_calculators.size())/2; }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(HftInterval& hypo) const;
    bool GetResult(double& hypo) const;
    
    TString ResultName()  const { return Var()->GetName(); }
    TString ResultTitle() const { return Var()->GetTitle(); }
        
    bool MakeBranches(HftTree& tree);

    using HftAbsCalculator::FillBranches;
    bool FillBranches(HftTree& tree);

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    bool SolveForHypo(unsigned int i, RooAbsData& data);
    bool SolveForTestHypo(unsigned int i);
    bool ShiftTestHypo(unsigned i, double currentResult);
    double NextHypo(double hypo, double sigma, double current, double target) const;
    bool ProduceToys(unsigned int i);

    TString CalculatorName(int i) { return AppendToName(Form("band%+d", i)); }
    
    TString Str(const TString& prefix = "", const TString& options = "") const;

    void SetToyState(const HftParameterStorage& state) { m_toysState = state; }
    void SetNToys(unsigned int nToys) { m_nToys = nToys; }
    void SetToysSaveInterval(unsigned int save) { m_toysSaveInterval = save; }
    void SetBinnedToys(bool binned) { m_binnedToys = binned; }

    int Verbosity() const { return Calculator(0)->Verbosity(); }
    void SetVerbosity(int verbosity);

  private:
    
    std::vector<HftAbsHypoTestCalculator*> m_calculators;
    double m_target, m_tolerance;
    unsigned int m_nIterMax, m_nCompIterMax;
    bool m_resultIsCL;
    HftParameterStorage m_toysState;
    unsigned int m_nToys, m_toysSaveInterval, m_binnedToys;
  };
}

#endif
