#ifndef ROOT_Hfitter_HftExpHypoTestCalc
#define ROOT_Hfitter_HftExpHypoTestCalc

#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftAbsHypoTestCalculator.h"

namespace Hfitter {

  /** @class HftExpHypoTestCalc
      @author Nicolas Berger

  */

  class HftExpHypoTestCalc : public HftAbsHypoTestCalculator
  {
   public:

    HftExpHypoTestCalc(const HftAbsHypoTestCalculator& calc, const HftParameterStorage& expectedState, const TString& name = "");
    HftExpHypoTestCalc(const HftExpHypoTestCalc& other, const TString& name = "", const HftScanPoint& hypo = HftScanPoint());

    virtual ~HftExpHypoTestCalc() { if (m_calc) delete m_calc; }

    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* /*cloner*/ = 0) const 
    { return new HftExpHypoTestCalc(*this, name, hypo); }

    bool MakeAltHypoBranches(HftTree& tree)  { return HypoTestCalc()->MakeAltHypoBranches(tree); }
    bool MakeNullHypoBranches(HftTree& tree)    { return HypoTestCalc()->MakeNullHypoBranches(tree); }
    bool FillAltHypoBranches(HftTree& tree)  { return HypoTestCalc()->FillAltHypoBranches(tree); }
    bool FillNullHypoBranches(HftTree& tree)    { return HypoTestCalc()->FillNullHypoBranches(tree); }

    bool LoadFromAsimov();
    bool LoadAltHypoInputs(RooAbsData& data);
    bool LoadNullHypoInputs(RooAbsData& data);
    
    bool LoadAltHypoInputs(HftTree& tree)    { return HypoTestCalc()->LoadAltHypoInputs(tree); }
    bool LoadNullHypoInputs(HftTree& tree)      { return HypoTestCalc()->LoadNullHypoInputs(tree); }
    
    bool LoadAltHypoInputs(const HftAbsCalculator& other);
    bool LoadNullHypoInputs(const HftAbsCalculator& other);

    bool GetExpStat(double& stat) const { return HypoTestCalc()->GetStatistic(stat); }
    double ExpStat() const { double stat = 0; return (GetExpStat(stat) ? stat : 0); }
    bool GetExpPLR(double& stat) const;

    void MakeCalc() const; // const because mutable m_calc
    
    HftAsimovCalculator* Calculator() { if (!m_calc) MakeCalc(); return m_calc; }
    HftAsimovCalculator* Calculator() const { if (!m_calc) MakeCalc(); return m_calc; }
    HftAbsHypoTestCalculator* HypoTestCalc() const { return (HftAbsHypoTestCalculator*)Calculator()->Calculator(); }
    HftParameterStorage ExpectedState() const { return m_expectedState; }
    
    bool UpdateState(const HftParameterStorage& toMerge) { return Calculator()->UpdateState(toMerge); }

    // Mainly for technical purposes
    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& q) const { return HypoTestCalc()->GetStatistic(q); }
    TString StatisticName()  const { return HypoTestCalc()->StatisticName(); }
    TString StatisticTitle() const { return HypoTestCalc()->StatisticTitle(); }
    TString ResultTitle()    const { return HypoTestCalc()->ResultTitle(); }
    bool ComputeFromAsymptotics(double q, double& cl) const { return HypoTestCalc()->ComputeFromAsymptotics(q, cl); }

    TString Str(const TString& prefix = "", const TString& options = "") const;

  protected:
        
    const HftAbsHypoTestCalculator* m_templateCalc;
    HftParameterStorage m_expectedState;
    
    mutable HftAsimovCalculator* m_calc;
  };
}

#endif
