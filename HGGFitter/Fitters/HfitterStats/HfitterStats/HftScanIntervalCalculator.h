#ifndef ROOT_Hfitter_HftScanIntervalCalculator
#define ROOT_Hfitter_HftScanIntervalCalculator

#include "HfitterModels/Options.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftMultiHypoTestCalculator.h"


namespace Hfitter {

  class HftScanningCalculator;

  /** @class HftScanIntervalCalculator
      @author Nicolas Berger

  */

  class HftScanIntervalCalculator : public HftAbsCalculator {

   public:

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftScanIntervalCalculator(const HftMultiHypoTestCalculator& calculator); 
    
    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftScanIntervalCalculator(const HftAbsHypoTestCalculator& calc, const TString& varName, unsigned int nPoints, double minVal, double maxVal, 
                             const TString& name = ""); 

    HftScanIntervalCalculator(const HftAbsHypoTestCalculator& calc, const TString& varName, const std::vector<double>& points,
                             const TString& name = ""); 

    HftScanIntervalCalculator(const HftAbsHypoTestCalculator& calc, unsigned int nPoints, double minVal, double maxVal, 
                             const TString& name = ""); 

    HftScanIntervalCalculator(HftModel& model, const TString& varName, unsigned int nPoints, double minVal, double maxVal, 
                              const Options& fitOptions = "", const TString& name = ""); 

    HftScanIntervalCalculator(HftModel& model, const TString& varName, const std::vector<double>& points,
                              const Options& fitOptions = "", const TString& name = ""); 

    HftScanIntervalCalculator(const HftScanIntervalCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftScanIntervalCalculator(*this, name, cloner); }
    
    virtual ~HftScanIntervalCalculator() { delete m_calculator; }

    HftMultiHypoTestCalculator* Calculator() const { return m_calculator; }
    double ContourCL() const { return m_cl; }
    int InterpolationOrder() const { return m_order; }
    
    void AddVar(const TString& varName, unsigned int nPoints, double minVal, double maxVal);
    unsigned int NVars() const { return Calculator()->NVars(); }
    RooRealVar* Var(unsigned int k = 0) { return Calculator()->Var(k); }
    
    void SetCL(double cl) { m_cl = cl; }
    void SetZ(double z);
    void SetInterpolationOrder(int order) { m_order = order; }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(HftInterval& interval) const;
    bool GetResult(double& value) const { HftInterval erResult(0); if (!GetResult(erResult)) return false; value = erResult.Value(); return true; }
    
    TString ResultName()  const { return Calculator()->Var()->GetName(); }
    TString ResultTitle() const { return Calculator()->Var()->GetTitle(); }
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data) { return Calculator()->LoadInputs(data); }
    bool LoadInputs(HftTree& tree)   { return Calculator()->LoadInputs(tree); }
    bool LoadInputs(const HftAbsCalculator& other) { return Calculator()->LoadInputs(other); }
    
    bool MakeBranches(HftTree& tree);

    using HftAbsCalculator::FillBranches;
    bool FillBranches(HftTree& tree);

    bool GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                              const TString& treeName = "tree", unsigned int saveInterval = 20) const
    { return Calculator()->GenerateSamplingData(state, nToys, fileRoot, treeName, saveInterval); }

    bool GenerateHypoSamplingData(unsigned int i, const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                  const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const
    { return Calculator()->GenerateHypoSamplingData(i, state, nToys, fileRoot, treeName, binned, saveInterval); }
    
    bool GenerateHypoSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                  const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const
    { return Calculator()->GenerateHypoSamplingData(state, nToys, fileRoot, treeName, binned, saveInterval); }

    bool SetSamplingData(const TString& fileRoot, const TString& treeName = "tree", const TString& nameInTree = "") 
    { return Calculator()->SetSamplingData(fileRoot, treeName, nameInTree); }

    bool UpdateState(const HftParameterStorage& toMerge) { return Calculator()->UpdateState(toMerge); }
    
    HftBand CLCurve() { return Calculator()->CLCurve(); }
    HftBand StatisticCurve() { return Calculator()->StatisticCurve(); }
    TH2D* StatisticHistogram2D(const TString& name = "cl") { return Calculator()->StatisticHistogram2D(name); }
    TH2D* CLHistogram2D(const TString& name = "stat") { return Calculator()->CLHistogram2D(name); }

    void DrawInterval();
    void DrawCLHistogram2D(double cl2 = -1);

    TString Str(const TString& prefix = "", const TString& options = "") const;
    
  private:
    
    HftMultiHypoTestCalculator* m_calculator;
    double m_cl;
    int m_order;
  };
}

#endif
