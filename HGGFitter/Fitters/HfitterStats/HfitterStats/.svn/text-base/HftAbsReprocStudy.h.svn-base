#ifndef ROOT_Hfitter_HftAbsReprocStudy
#define ROOT_Hfitter_HftAbsReprocStudy

#include "HfitterModels/HftModel.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftTree.h"
#include "TString.h"
#include <vector>

namespace Hfitter {

  /** @class HftAbsReprocStudy
      @author Nicolas Berger
  */
 
  
  class HftAbsReprocStudy {

   public:

    //! constructor from a single model. This model is used for both generation and fitting. @param studyName : a name for this study. @param genModel : the model used for generation and fitting
    HftAbsReprocStudy(const TString& studyName, const TString& inputTreeName, const TString& inputFileName);
    virtual ~HftAbsReprocStudy();

    //! add a fitting model
    bool Add(HftAbsCalculator* inputCalculator, HftAbsCalculator* outputCalculator);
    
    //! return the name of this study
    const TString& StudyName()  const { return m_studyName; }
  
    HftAbsCalculator* InputCalculator(unsigned int i) const { return m_inputCalculators[i]; } //!< returns the i'th fit model. @param i : model index
    HftAbsCalculator* OutputCalculator(unsigned int i) const { return m_outputCalculators[i]; } //!< returns the i'th fit model. @param i : model index
    unsigned int NCalculators() const { return m_inputCalculators.size(); } //!< returns the number of fit models.
    
    bool Run(const TString& output = "", unsigned int nEntries = 0); //!< run the study
    
    //! actions to be perfomed once at the beginning of the study
    virtual bool Initialize() { return true; }

    //! actions to be performed once per fitting model at the beginning of the study. @param model : a fitting model
    virtual bool Initialize(HftAbsCalculator& /*calculator*/) { return true; }
    
    //! actions to be performed for each generated toy, once per fitting model. 
    //! @param model : the current fitting model. @param dataSet : the dataset to fit, @param genPoint : the generation point. 
    virtual bool Execute(HftAbsCalculator& /*calculator*/) { return true; }
    
    //! actions to be performed once per fitting model at the end of the study. @param model : the fitting model
    virtual bool Finalize(HftAbsCalculator& /*calculator*/) { return true; }

    //! actions to be performed once at the end of the study.
    virtual bool Finalize() { return true; }
      
    //! accessor for the study tree.
    HftTree* InputTree() { return m_inputTree; }

    //! accessor for the study tree.
    HftTree* OutputTree() { return m_outputTree; }

   protected:
          

    HftTree* m_inputTree; //!< the study TTree.
    HftTree* m_outputTree; //!< the study TTree.
    
    TString m_studyName;  //!< the name of the study.
    TString m_inputTreeName;  //!< the name of the study.
    TString m_inputFileName;  //!< the name of the study.
        
    std::vector<Hfitter::HftAbsCalculator*> m_inputCalculators; //!< the models used for fitting.      
    std::vector<Hfitter::HftAbsCalculator*> m_outputCalculators; //!< the models used for fitting.      
   };
}

#endif
