#ifndef ROOT_Hfitter_HftScanStudy
#define ROOT_Hfitter_HftScanStudy

#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftScanningCalculator.h"

#include "TString.h"
#include <vector>

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  /** @class HftScanStudy
      @author Nicolas Berger

  */  
  
  class HftScanStudy {
    
   public:
     
    HftScanStudy(const HftScanningCalculator& calculator, const TString& fileRoot, 
                 RooAbsData& data, const HftParameterStorage& asimovState, unsigned int sparsifyExp = 1);

    HftScanStudy(const HftScanningCalculator& observed, const std::vector<double>& expected, const TString& fileRoot, 
                 const std::vector<RooAbsData*>& data, const HftParameterStorage& asimovState);

    HftScanStudy(const HftScanningCalculator& observed, const HftScanningCalculator& expected, const TString& fileRoot, 
                 const std::vector<RooAbsData*>& data);

    virtual ~HftScanStudy();
        
    HftScanningCalculator* Expected() { return m_expected; }
    HftScanningCalculator* Observed() { return m_observed; }
        
    TString FileRoot() const { return m_fileRoot; }
    RooAbsData* Data(unsigned int i) const { return (i < m_data.size() ? m_data[i] : 0); } 
    HftParameterStorage AsimovState() const { return m_asimovState; }
    
    bool LoadObserved(unsigned int first = 0, unsigned int last = 0, bool makeIfMissing = true); 
    bool LoadExpected(unsigned int first = 0, unsigned int last = 0, bool makeIfMissing = true); 
    bool Load(unsigned int first = 0, unsigned int last = 0, bool makeIfMissing = false);
    
    void DrawLimit(double yMin = 0, double yMax = 10, double xMin = 0, double xMax = -1, const TString& yTitle = "Limit", 
                const TString& expLegend = "Expected limit", const TString& obsLegend = "Observed limit", 
                const TString& label = "", double lumi = -1,
                double lumiX = 0.57, double lumiY = 0.80, double labelX = 0.57, double labelY = 0.88);
    void DrawP0(double yMin = 1E-5, double yMax = 10, double xMin = 0, double xMax = -1, const TString& yTitle = "Local p_{0}", 
                const TString& expLegend = "Expected p_{0}", const TString& obsLegend = "Observed p_{0}", 
                const TString& label = "", double lumi = -1, unsigned int nSigmas = 0,
                double lumiX = 0.57, double lumiY = 0.80, double labelX = 0.57, double labelY = 0.88);
    void PrintTable(int nErrors = 0, unsigned int nDigits = 3);
        
    static bool Register(const TString& name, HftScanStudy& study);
    static HftScanStudy* Get(const TString& name = "");
 
   private:
    
     static bool FillSparse(const HftScanningCalculator& observed, unsigned int sparsify, std::vector<double>& expected);
     HftScanningCalculator* MakeExpected(const HftScanningCalculator& observed, const std::vector<double>& expected);
     
    HftScanningCalculator* m_expected, * m_observed;
    std::vector<unsigned int> m_isLoaded;
    TString m_fileRoot;
    std::vector<RooAbsData*> m_data;
    HftParameterStorage m_asimovState;
 
    static std::map<TString, HftScanStudy*>* m_registry;
   };
}

#endif
