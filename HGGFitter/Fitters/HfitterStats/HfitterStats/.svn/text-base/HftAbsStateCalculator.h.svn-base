#ifndef ROOT_Hfitter_HftAbsStateCalculator
#define ROOT_Hfitter_HftAbsStateCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterModels/HftParameterStorage.h"

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  /** @class HftAbsStateCalculator
      @author Nicolas Berger

  */  
  
  class HftAbsStateCalculator : public HftAbsCalculator {
   public:
     
    HftAbsStateCalculator(HftModel& model, const HftParameterStorage& state, const TString& name = "")
      : HftAbsCalculator(model, name), m_state(state) { }
      
    HftAbsStateCalculator(const HftAbsCalculator& other, const HftParameterStorage& state, const TString& name = "") 
      : HftAbsCalculator(other, name), m_state(state) { }

    HftAbsStateCalculator(const HftAbsStateCalculator& other, const TString& name = "") 
      : HftAbsCalculator(other, name), m_state(other.State()) { }
    
    virtual ~HftAbsStateCalculator() { }
    
    const HftParameterStorage& State() const { return m_state; }
    
    bool UpdateState(const HftParameterStorage& toMerge) { m_state.Add(toMerge, true); return true; }
    
   private:

     HftParameterStorage m_state;
  };
}

#endif
