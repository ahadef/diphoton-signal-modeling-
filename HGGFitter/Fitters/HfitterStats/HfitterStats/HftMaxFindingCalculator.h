#ifndef ROOT_Hfitter_HftMaxFindingCalculator
#define ROOT_Hfitter_HftMaxFindingCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"

#include "TString.h"
#include <vector>

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  /** @class HftMaxFindingCalculator
      @author Nicolas Berger

  */  
  
  class HftMaxFindingCalculator : public HftAbsCalculator {
    
   public:
     
    HftMaxFindingCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, unsigned int nPoints, 
                            double minVal, double maxVal, bool minimize = false, const TString& name = "");
    HftMaxFindingCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, 
                            const std::vector<double>& pos, bool minimize = false, const TString& name = "");
    HftMaxFindingCalculator(const std::vector<const Hfitter::HftAbsCalculator*>& calculators, RooRealVar& scanVar, 
                            const std::vector<double>& pos, bool minimize = false, const TString& name = "");
    HftMaxFindingCalculator(const HftMaxFindingCalculator& other, const TString& name = "");

    virtual ~HftMaxFindingCalculator();

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftMaxFindingCalculator(*this, name); }
    
    HftScanningCalculator* Calculator() const { return m_scanningCalculator; }
    HftModel& Model() const { return Calculator()->Model(); }
    
    int MaxIndex() const { return (m_maxIndex < 0 ? -1 : int(m_maxIndex + 0.5)); }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const;
    bool GetResult(HftInterval& result) const;
    
    TString ResultName() const { return Calculator()->ResultName(); }
    TString ResultTitle() const { return Calculator()->ResultTitle(); }
    TString ResultUnit() const { return Calculator()->ResultUnit(); }

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);          
    bool FillBranches(HftTree& tree);

    bool IsMinFinding() const { return m_minimize; }
    
    TString Str(const TString& prefix = "", const TString& options = "") const;

   protected:
    
    HftScanningCalculator* m_scanningCalculator;
    bool m_minimize;
    double m_maxIndex;
  };
}

#endif
