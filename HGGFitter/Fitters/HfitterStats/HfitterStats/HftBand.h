#ifndef ROOT_Hfitter_HftBand
#define ROOT_Hfitter_HftBand

#include "HfitterStats/HftInterval.h"
#include "TGraphAsymmErrors.h"
#include "TGraph.h"
#include "TString.h"

#include <vector>

namespace Hfitter {

  /** @class HftBand
      @author Nicolas Berger

      Class storing an 1D error band, i.e. a vector of (value, errorRange)
  */  
  
  class HftBand {
   public:
    
    HftBand(unsigned int nBands = 0, const TString& title = "", const TString& xLabel = "", const TString& yLabel = "");

    //! constructor. Takes two vectors of identical size for the X and Y information of the points.  @param pos : x positions of the points. @param ers : y position and error bands of the points.
    HftBand(const std::vector<double>& pos, const std::vector<Hfitter::HftInterval>& ers,
                 const TString& title = "", const TString& xLabel = "", const TString& yLabel = "");

    HftBand(const TGraphAsymmErrors& graph);
    
    //! returns the number of points in the band
    unsigned int NPoints() const { return m_pos.size(); }

    //! returns the i'the position. @param i : position index
    double Position(unsigned int i) const { return i < NPoints() ? m_pos[i] : -999; }

    //! returns the i'the error range. @param i : position index
    HftInterval ErrorRange(unsigned int i) const { return i < NPoints() ? m_ers[i] : HftInterval(); }

    bool Add(double pos, const HftInterval& range);
    
    //! returns the number of error bands.
    int NBands() const { return m_nBands; }
    int NErrors() const { return NBands(); } // for backward-compatilibity

    //! draw the error range as a set of graphs. @param nameRoot : prefix for the graph names. 
    //! @param xLabel : x-axis label, @param yLabel : y-axis label, @param yMin : lower limit of y-axis range, @param yMax : upper limit of y-axis range.
    TGraphAsymmErrors** Draw(double yMin = 0, double yMax = -1,  Color_t lineColor = kBlack, Style_t lineStyle = kDashed, Width_t lineWidth = 2, 
                             const TString& gOpts = "", const TString& nameRoot = "graph",
                             double xMin = 0, double xMax = -1, const TString& yTitle = "");
    TGraph** DrawLine(double yMin = 0, double yMax = -1, Color_t lineColor = kBlack, Style_t lineStyle = kDashed, 
                      const TString& gOpts = "L", Width_t lineWidth = 2, const TString& nameRoot = "graph", 
                      double xMin = 0, double xMax = -1, const TString& yTitle = "");

    bool Positions(std::vector<double>& pos) const { pos = m_pos; return true; }

    //! static function to extract the n'th values from the stored error ranges and returns them as a vector<double>. 
    //! @param n : the index of the value to extract (see @c HftInterval::Value for the indexing scheme). 
    //! @param vals : the vector returning the extracted values.
    bool Values(int n, std::vector<double>& vals) const;

    bool ValueErrors(int n, std::vector<double>& errs) const;

    TGraphAsymmErrors* Graph(unsigned int i = 0, const TString& name = "") const;
    TGraphAsymmErrors* ErrorGraph(int i = 0, const TString& name = "") const;

    //! Returns a horizontal section of the error band centered around a particular Y value. This is assuming that the graph of the error band is 
    //! monotonically increasing. @param val : target Y value .
    bool FindPosition(double val, HftInterval& pos, unsigned int order = 2, double xMin = -DBL_MAX, double xMax = DBL_MAX) const;
    HftInterval FindPosition(double val, unsigned int order = 2, double xMin = -DBL_MAX, double xMax = DBL_MAX) const
    { HftInterval pos; if (!FindPosition(val, pos, order, xMin, xMax)) return HftInterval(); return pos; } 

    static double FindPosition(TGraph& graph, double val, int order = 2, double xMin = -DBL_MAX, double xMax = DBL_MAX);
    
    static bool FindPosition(double y, double& x, const std::vector<double>& xs, const std::vector<double>& ys, unsigned int order, double xMin, double xMax);
    static bool FindPositionLinear(double y, double& x, double x1, double x2, double y1, double y2);
    static bool FindPositionQuadratic(double y, double& xn, double& xp, double x1, double x2, double x3, double y1, double y2, double y3);

    //HftInterval Interpolate(double pos) const;

    bool Interpolate(double pos, HftInterval& val, unsigned int order = 2) const;
    HftInterval Interpolate(double pos, unsigned int order = 2) const { HftInterval val; if (!Interpolate(pos, val, order)) return HftInterval(); return val; }

    static bool Interpolate(double x, double& y, const std::vector<double>& xs, const std::vector<double>& ys, unsigned int order = 2);
    static bool InterpolateLinear(double x, double& y, double x1, double x2, double y1, double y2);
    static bool InterpolateQuadratic(double x, double& y, double x1, double x2, double x3, double y1, double y2, double y3);

    static double Interpolate(TGraph& graph, double pos, int order = 2);

    bool FindMinimum(double& pos, HftInterval& val, double xMin = -DBL_MAX, double xMax = DBL_MAX) const;
    static bool FindMinimum(double& x, const std::vector<double>& xs, const std::vector<double>& ys, double xMin, double xMax);
    static bool FindMinimum(double& x, double& y, double x1, double x2, double x3, double y1, double y2, double y3);

    static bool Intersections(const TGraphAsymmErrors& g1, const TGraphAsymmErrors& g2, std::vector<double>& xs, std::vector<double>& ys);
    static bool Max(const TGraphAsymmErrors& g1, const TGraphAsymmErrors& g2, TGraphAsymmErrors& gm);

    HftBand Resample(const std::vector<double>& pos) const;
    HftBand Resample(unsigned int n, double minPos = 0, double maxPos = -1) const;

    TString Title()  const { return m_title; }
    TString XLabel() const { return m_xLabel; }
    TString YLabel() const { return m_yLabel; }
    
    void SetXLabel(const TString& label) { m_xLabel = label; }
    void SetYLabel(const TString& label) { m_yLabel = label; }
    
    double YMin() const { return m_yMin; }    
    double YMax() const { return m_yMax; }

    void SetYMin(double yMin) { m_yMin = yMin; }
    void SetYMax(double yMax) { m_yMax = yMax; }

    TString Str(const TString& options = "") const; 
    
    //! dump the values to std::cout
    void Print(const TString& options = "") const;

    bool Difference(const HftBand& other, HftBand& diff, bool relative = false) const;
    
    HftInterval Max() const;
    HftInterval Min() const;
    
    int MaxPosition() const;
    int MinPosition() const;
    
    static void SetColor(unsigned int i, unsigned int c) { m_colors[i] = c; }

    bool ShiftY(double dy, HftBand& shifted) const;

    static HftBand* LoadFromFiles(const TString& fileRoot, double minVal, double maxVal, double step, const TString& result_name = "result");
    static TGraph* Difference(const TGraph& g1, const TGraph& g2, bool relative = false, const TString& name = "diff");
    static TGraphAsymmErrors* Difference(const TGraphAsymmErrors& g1, const TGraph& g2, bool relative = false, const TString& name = "diff");
    
  private:
     
    std::vector<double> m_pos; //!< vector storing the positions.
    std::vector<Hfitter::HftInterval> m_ers; //!< vector storing the error ranges.
    int m_nBands;
    TString m_title, m_xLabel, m_yLabel;
    double m_yMin, m_yMax;
    
    static unsigned int m_colors[5];
  };
}

#endif
