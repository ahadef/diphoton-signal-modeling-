#ifndef ROOT_Hfitter_HftAbsHypoTestCalculator
#define ROOT_Hfitter_HftAbsHypoTestCalculator

#include "HfitterStats/HftAbsStatCalculator.h"

#include "HfitterStats/HftScanPoint.h"


namespace Hfitter {

  class HftExpHypoTestCalc;
  
  /** @class HftAbsHypoTestCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsToyStudy to scan model parameters. Each call to HftAbsToyStudy::Execute is replaced by a loop performing an action for
    each point of the defined scanning grid (distinct from the generation grid used in HftAbsToyStudy). Two new virtual methods, both named ExecuteScan,
    are implemented. The first is executed once before the loop; the second, which takes the fit point as argument, is executed once for every iteration.
    The Initialize step is similarly modified.
  */
 
    
  class HftAbsHypoTestCalculator : public HftAbsStatCalculator {

   public:

    //! Constructor for the case of single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftAbsHypoTestCalculator(HftModel& model, const HftScanPoint& nullHypo, const HftScanPoint& altHypo, bool resultIsCL = false,
                             const TString& name = "", bool altHypoFirst = false);
    
    // Common special case:  null hypo = hypoVars fixed at current values, alt hypo = hypoVars free
    HftAbsHypoTestCalculator(HftModel& model, const TString& hypoVars, bool resultIsCL = false, const TString& name = "",
                             bool altHypoFirst = false);

    // Another formulation : null provided, alt is the same with fixed variables released
    HftAbsHypoTestCalculator(HftModel& model, const HftScanPoint& hypo, bool resultIsCL = false, const TString& name = "",
                             bool altHypoFirst = false);
    
    HftAbsHypoTestCalculator(const HftAbsHypoTestCalculator& other, const TString& name, const HftScanPoint& hypo);

    void InitHypoVars();

    virtual ~HftAbsHypoTestCalculator() { }

    using HftAbsCalculator::GetResult;
    bool GetResult(HftInterval& result) const;

    virtual bool GetResultBand(int /*i*/, double& result) const { return GetResult(result); }

    virtual HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name = "", const HftAbsCalculator* cloner = 0) const = 0;
    virtual HftAbsHypoTestCalculator* CloneHypo(const HftAbsCalculator* cloner = 0) const { return CloneHypo(NullHypo(), GetName(), cloner); }
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const { return CloneHypo(NullHypo(), name, cloner); }

    const HftScanPoint& NullHypo() const { return m_nullHypo; }
    const HftScanPoint& AltHypo() const { return m_altHypo; }

    unsigned int NHypoVars() const { return m_hypoVars.size(); }
    RooRealVar* HypoVar(unsigned int i) const { return m_hypoVars[i]; }
    
    double NullHypoValue(unsigned int i) const { return NullHypo().State().Value(*HypoVar(i)); }
    double AltHypoValue(unsigned int i) const { return AltHypo().State().Value(*HypoVar(i)); }

    virtual bool MakeNullHypoBranches(HftTree& tree) = 0;
    virtual bool MakeAltHypoBranches (HftTree& tree) = 0;
    
    virtual bool FillNullHypoBranches(HftTree& tree) = 0;
    virtual bool FillAltHypoBranches (HftTree& tree) = 0;

    virtual bool LoadNullHypoInputs(RooAbsData& data) = 0;
    virtual bool LoadAltHypoInputs(RooAbsData& data) = 0;

    virtual bool LoadNullHypoInputs(HftTree& tree) = 0;
    virtual bool LoadAltHypoInputs(HftTree& tree) = 0;

    virtual bool LoadNullHypoInputs(const HftAbsCalculator& other) = 0;
    virtual bool LoadAltHypoInputs(const HftAbsCalculator& other) = 0;
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(const HftAbsHypoTestCalculator& other, RooAbsData& data);
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    bool DrawHypos(const TString& hypoPar, double xMin, double xMax) const;

    //! implementation of AbsHypoCalc hypo: the null is *the* hypo
    const HftScanPoint& Hypo() const { return m_nullHypo; }

    //! implementation of the virtual function from @c HftAbsCalculator
    bool MakeBranches(HftTree& tree);

    using HftAbsCalculator::FillBranches;
    //! implementation of the virtual function from @c HftAbsCalculator
    bool FillBranches(HftTree& tree);

    bool AltHypoFirst() const { return m_altHypoFirst; }
    
    virtual bool GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileName, 
                                      const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const;

  protected:

    HftScanPoint m_nullHypo, m_altHypo;
    std::vector<RooRealVar*> m_hypoVars;

    bool m_altHypoFirst;
  };
}

#endif
