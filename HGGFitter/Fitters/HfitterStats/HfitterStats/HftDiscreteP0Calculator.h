#ifndef ROOT_Hfitter_HftDiscreteP0Calculator
#define ROOT_Hfitter_HftDiscreteP0Calculator

#include "HfitterStats/HftAbsP0Calculator.h"

#include "HfitterModels/HftParameterStorage.h"
#include "HfitterStats/HftTree.h"
#include "RooAbsData.h"


namespace Hfitter {

  /** @class HftDiscreteP0Calculator
      @author Andreas Hoecker
      @author Nicolas Berger

    An implementation of @c HftAbsCalculator to compute discovery significances.
  */
 
   
  class HftDiscreteP0Calculator : public HftAbsP0Calculator {

   public:

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting.
    //! @param bkgOnly : true if background-only toys are to be generated. @param signalComponent : name of the signal component of the model.
    HftDiscreteP0Calculator(HftModel& nullModel, const HftParameterStorage& nullHypo, 
                            HftModel& altModel, const HftParameterStorage& altHypo,
                            const TString& fitOptions = "", const TString& name = "", bool resultIsSignif = false)
      : HftAbsP0Calculator(nullModel, name, resultIsSignif), m_altModel(&altModel), m_nullHypo(nullHypo), m_altHypo(altHypo), m_fitOptions(fitOptions) { }

    HftDiscreteP0Calculator(const HftDiscreteP0Calculator& other, const TString& name = "")
     : HftAbsP0Calculator(other, name), m_altModel(other.m_altModel), m_nullHypo(other.m_nullHypo), 
       m_altHypo(other.m_altHypo), m_fitOptions(other.m_fitOptions) { }
    
    virtual ~HftDiscreteP0Calculator() { }

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftDiscreteP0Calculator(*this, name); }
    
    HftModel& AltModel() const { return *m_altModel; }
    HftParameterStorage NullHypo() const { return m_nullHypo; }
    HftParameterStorage AltHypo() const { return m_altHypo; }

    //! before toy loop: define study variables. @param model : the model to use for fitting, @param genPoint : the generation point. 
    bool MakeBranches(HftTree& tree);
        
    using HftAbsCalculator::FillBranches;
    //! in toy loop: for each model, perform a free fit and a fit bkg-only fit, and fill relevant variables. 
    //! @param model : the model to use for fitting. @param dataSet : the dataset to fit to,@param genPoint : the generation point.      
    bool FillBranches(HftTree& tree);

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    double NullNLL() const { return m_nullNLL; }
    double AltNLL() const { return m_altNLL; }

    double NullStatus() const { return m_nullStatus; }    
    double AltStatus() const { return m_altStatus; }    
    
    TString FitOptions() const { return m_fitOptions; }
    void SetFitOptions(const TString& fitOptions) { m_fitOptions = fitOptions; }
    
    HftParameterStorage NullState() const { return m_nullState; }
    HftParameterStorage AltState() const { return m_altState; }

    using HftAbsStatCalculator::GetStatistic;
    //! Calculate the value of the statistic with current inputs
    bool GetStatistic(double& lambda) const;
 
    //! Calculate the confidence level from the statistic, using either asymptotic formulas or sampling
    bool ComputeP0FromAsymptotics(double /*q0*/, double& p0) const { p0 = 1; return true; }

    double SignificanceFromP0(double p0) const;

    TString StatisticName()  const { return "lambda"; }
    TString StatisticTitle() const { return "#lambda"; }

  protected:

    HftModel* m_altModel;
    HftParameterStorage m_nullHypo, m_altHypo;
    
    HftParameterStorage m_nullState;
    mutable double m_nullStatus;
    mutable double m_nullNLL;            //!< cached value for fitted NLL.
    
    HftParameterStorage m_altState;
    mutable double m_altStatus;
    mutable double m_altNLL;        //!< cached value for NLL of hypo fit

    TString m_fitOptions;
  };
}

#endif
