#ifndef ROOT_Hfitter_HftInterval
#define ROOT_Hfitter_HftInterval

#include <vector>
#include "TString.h"
#include "RooRealVar.h"

class TH1;

namespace Hfitter {

  /** @class HftInterval
      @author Nicolas Berger

      Class storing a one point of an error band. It contains a central value along with an arbitrary number of error ranges, each range consisting of a positive and a negative error.
  */  
  
  class HftInterval {
   public:
     
    HftInterval() { }
         
    //! constructor from a list of values: @param vals : the central and error values. the vector should be of odd size 2n + 1, with the central value at index n, the +/- 1 sigma errors respectively at indices  n+1 and n-1, and the +/- n sigma errors at indices 0 and 2n.
    HftInterval(const std::vector<double>& vals, const std::vector<double>& errs = std::vector<double>());
    
    HftInterval(double value) : m_vals(1), m_errs(1) { m_vals[0] = value; }    
    HftInterval(double value, double error) : m_vals(3), m_errs(3) { m_vals[1] = value; m_vals[0] = m_vals[1] - error; m_vals[2] = m_vals[1] + error; }
    HftInterval(double value, double nerr, double perr);
    HftInterval(double value, double n2, double n1, double p1, double p2);
    
    HftInterval(const RooRealVar& var) : m_vals(3), m_errs(3) { m_vals[1] = var.getVal(); m_vals[0] = m_vals[1]+var.getErrorLo(); m_vals[2] = m_vals[1]+var.getErrorHi(); }
    
    static HftInterval Make(unsigned int n) { return HftInterval(std::vector<double>(2*n  + 1)); }
    
    //! retrieve the n'th value. Returns true if the index is valid, false otherwise. @param n : the index of the requested value. the central value has index 0, the +/- 1 sigma errors indices +1 and -1 respectively, etc. @param val : the variable through which the requested value is returned
    bool Value(int n, double& val) const;

    bool ValueError(int n, double& val) const;

    //! returns the value at index n. @param n : the index of the value (see @c HftInterval::Value for the indexing scheme). 
    double Value(int n = 0) const { double v; Value(n, v); return v; }

    //! returns the error at index n. @param n : the index of the error (see @c HftInterval::Value for the indexing scheme). 
    double Error(int n = 1) const { return Value(n) - Value(); }

    double PosError(unsigned int n = 1) const { return Value(+int(n)) - Value(); }
    double NegError(unsigned int n = 1) const { return Value(-int(n)) - Value(); }
    
    double ValueError(int n = 0) const { double v; ValueError(n, v); return v; }

    //! set value at index n. @param n : the index of the value to set (see @c HftInterval::Value for the indexing scheme). @param val : the value to be set.
    bool SetValue(int n, double val);

    bool SetValueError(int n, double err);

    bool SetError(int n, double err);
    
    //! returns the number of error ranges.
    int NBands() const { int s = m_vals.size(); if (s == 0) return -1; return (s - 1)/2; }
    int NErrors() const { return NBands(); } // for backward compatibility
    
    //! overloading of (), returning the central value.
    double operator()() const { return Value(); }
    
    //! automatic conversion to double, returning the central value.
    operator double() const { return Value(); }
    
    bool CopyValues(const HftInterval& other);
    
    //! returns the median and +/- 1...n sigma quantiles of the specified dataset. 
    //! @param vals : a vector containing the data. @param n : the number of quantiles to extract.
    static bool MedianWithErrors(const std::vector<double>& vals, HftInterval& median, bool verbose = true);
    static HftInterval MedianWithErrors(const std::vector<double>& vals, int n);

    //! returns the median and +/- 1...n sigma quantiles of the specified histogram.
    //! @param hist : a histogram containing the data. @param n : the number of quantiles to extract.
    static HftInterval MedianWithErrors(const TH1& hist, int n);

    static double QuantileValue(double quantile, const std::vector<double>& sortedVals);
        
    TString Str(const TString& options = "") const; 
    
    //! dump the values to std::cout
    void Print(const TString& options = "") const;
    
    bool Difference(const HftInterval& other, HftInterval& diff, bool relative = false) const;

    HftInterval Shift(double dy) const;
    
    HftInterval Flip() const;
    
   private:
     
    std::vector<double> m_vals, m_errs; //!< vector storing the values.
  };
}

#endif
