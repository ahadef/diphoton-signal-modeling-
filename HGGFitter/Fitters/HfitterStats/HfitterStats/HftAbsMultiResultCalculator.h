#ifndef ROOT_Hfitter_HftAbsMultiResultCalculator
#define ROOT_Hfitter_HftAbsMultiResultCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "TString.h"

namespace Hfitter {

  /** @class HftAbsMultiResultCalculator
      @author Nicolas Berger

  */  
  
  class HftAbsMultiResultCalculator : public HftAbsCalculator {
   public:
     
    HftAbsMultiResultCalculator(HftModel& model, const TString& name = "") 
    : HftAbsCalculator(model, name) { }

    HftAbsMultiResultCalculator(const HftAbsMultiResultCalculator& other, const TString& name = "") 
    : HftAbsCalculator(other, name) { }

    virtual ~HftAbsMultiResultCalculator() { }
    
    virtual unsigned int NResults() const = 0;
    virtual unsigned int CurrentIndex() const = 0;
    
    virtual bool SetCurrent(unsigned int index) = 0;
    virtual bool SetCurrent(const TString& name) = 0;
  };
}

#endif
