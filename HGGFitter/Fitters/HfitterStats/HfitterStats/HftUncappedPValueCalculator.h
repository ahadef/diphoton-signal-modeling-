#ifndef ROOT_Hfitter_HftUncappedPValueCalculator
#define ROOT_Hfitter_HftUncappedPValueCalculator

#include "HfitterStats/HftPLRPValueCalculator.h"


namespace Hfitter {

  /** @class HftUncappedPValueCalculator
      @author Andreas Hoecker
      @author Nicolas Berger

    An implementation of @c HftAbsCalculator to compute discovery significances.
  */
 
   
  class HftUncappedPValueCalculator : public HftPLRPValueCalculator {

   public:

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting.
    //! @param bkgOnly : true if background-only toys are to be generated. @param signalComponent : name of the signal component of the model.
    HftUncappedPValueCalculator(HftModel& model, const TString& hypoVarName, 
                                const Options& fitOptions = Options(), const TString& name = "")
     : HftPLRPValueCalculator(model, hypoVarName, fitOptions, name) { }
    
    HftUncappedPValueCalculator(HftModel& model, const TString& hypoVarName, double hypoValue,
                                const Options& fitOptions = Options(), const TString& name = "")
     : HftPLRPValueCalculator(model, hypoVarName, hypoValue, fitOptions, name) { }

    HftUncappedPValueCalculator(const HftUncappedPValueCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0)
      : HftPLRPValueCalculator(other, name, cloner) { }

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const 
    { return new HftUncappedPValueCalculator(*this, name, cloner); }
            
    virtual ~HftUncappedPValueCalculator() { }
    
    using HftAbsStatCalculator::GetStatistic;
    //! Calculate the value of the statistic with current inputs
    bool GetStatistic(double& q0) const;
    bool GetStatisticBand(int i, double& tBand) const;
 
    //! Calculate the confidence level from the statistic, using either asymptotic formulas or sampling
    bool ComputeFromAsymptotics(double q0, double& p0) const;

    // Shortcut to get significance directly from statistic, updated to handle negative q0
    bool ComputeSignificance(double stat, double& z) const;

    //! Not used for asymptotic results, since we get significance directly from the statistic, Z = sqrt(q).
    double SignificanceFromPValue(double p0) const;

    TString StatisticName()  const { return "tUncap0"; }
    TString StatisticTitle() const { return "#t^{uncap}_{0}"; }
    
    TString Str(const TString& prefix = "", const TString& options = "") const;
  };
}

#endif
