#ifndef ROOT_Hfitter_HftDataCountingCalculator
#define ROOT_Hfitter_HftDataCountingCalculator

#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "RooCategory.h"

class RooCategory;

namespace Hfitter {

  
  /** @class HftDataCountingCalculator
      @author Nicolas Berger

  */  
  
  class HftDataCountingCalculator : public HftAbsMultiResultCalculator {
   public:
     
    HftDataCountingCalculator(HftModel& model, const TString& catName = "", const TString& name = "");
    HftDataCountingCalculator(const HftDataCountingCalculator& other, const TString& name = "");

    virtual ~HftDataCountingCalculator() { }
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const {  return new HftDataCountingCalculator(*this, name); }
    
    unsigned int NResults() const { return m_cat ? m_cat->numTypes() : 0; }
    const RooCategory* Cat() const { return m_cat; }
    
    unsigned int Counts(unsigned int k) const { return m_counts[k]; }
    
    unsigned int CurrentIndex() const { return m_index; }
    TString CurrentLabel() const { return m_cat ? m_cat->lookupType(m_index)->GetName() : ""; }
    bool SetCurrent(unsigned int k) { m_index = k; return true; }
    bool SetCurrent(const TString& name);
    
    TString Label(unsigned int index) const;
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);

    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { result = Counts(CurrentIndex()); return true; }
    
    TString ResultName() const { return "nEvents_" + CurrentLabel(); }
    TString ResultTitle() const { return "Number of events in category " + CurrentLabel(); }
    TString Str(const TString& prefix = "", const TString& options = "") const;

    
   private:
     
     unsigned int m_index;
     RooCategory* m_cat;
     std::vector<unsigned int> m_counts;
     
  };
}

#endif
