#ifndef ROOT_Hfitter_HftCLsCalculator
#define ROOT_Hfitter_HftCLsCalculator

#include "HfitterStats/HftPLRCalculator.h"


namespace Hfitter {

  /** @class HftCLsCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsHypoTestCalculator to compute expected exclusion limits using the CLs method.
  */
   
  class HftMultiHypoTestCalculator;
  class HftScanLimitCalculator;
  
  class HftCLsCalculator : public HftAbsHypoTestCalculator
  {
   public:

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    //! @param nMuHypos : number of signal hypothesis scan points. @param muMin : lower bound of the signal hypothesis scan. 
    //! @param muMax : upper bound of the signal hypothesis scan. @param signalIntensityVar : name of the signal intensity variable to use
    //! @param signalComponent : name of the signal component of the model.    
    HftCLsCalculator(const HftPLRCalculator& clSBCalculator, const TString& name = "");
    
    HftCLsCalculator(const HftCLsCalculator& other, const TString& name = "", const HftScanPoint& hypo = HftScanPoint(), 
                     const HftAbsCalculator* cloner = 0);
    
    virtual ~HftCLsCalculator() { delete m_clSBCalculator; delete m_clBCalculator; }

    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name = "", const HftAbsCalculator* cloner = 0) const 
    { return new HftCLsCalculator(*this, name, hypo, cloner); }

    //! before toy loop: define model variables. @param model : the model to use for fitting, @param genPoint : the generation point.
    bool MakeAltHypoBranches(HftTree& tree);

    //! before toy loop: define fit point variables. @param tree : the tree where to create the branches,
    //! @param blockName : the branch name prefix.
    bool MakeNullHypoBranches(HftTree& tree);

    //! Fill branches for each toy iteration. @param tree : the tree where to fill the branches, @param data : the (toy) dataset to operate on,
    //! @param blockName : the branch name prefix. 
    bool FillAltHypoBranches(HftTree& tree);
    
    //! Fill branches at each scan point. @param tree : the tree where to fill the branches, @param data : the (toy) dataset to operate on,
    //! @param blockName : the branch name prefix. 
    bool FillNullHypoBranches(HftTree& tree);

    bool LoadAltHypoInputs(RooAbsData& data);
    bool LoadNullHypoInputs(RooAbsData& data);
    
    bool LoadAltHypoInputs(HftTree& tree);
    bool LoadNullHypoInputs(HftTree& tree);
    
    bool LoadAltHypoInputs(const HftAbsCalculator& other);
    bool LoadNullHypoInputs(const HftAbsCalculator& other);

    const HftPLRCalculator* CLsbCalculator() const { return m_clSBCalculator; }
    const HftPLRCalculator* CLbCalculator()  const { return m_clBCalculator; }

    int Verbosity() const { return CLsbCalculator()->Verbosity(); }
    
    //! Calculate the exclusion confidence level.
    bool GetResultBand(int i, double& rBand) const;

    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& q) const;

    bool ComputeFromAsymptotics(double q, double& cl) const;
    bool UseSampling() const { return CLsbCalculator()->UseSampling() && !IgnoreSamplingData(); }
    bool ComputeFromSampling(double q, double& cl) const;

    TString StatisticName()  const { return CLsbCalculator()->StatisticName(); }
    TString StatisticTitle() const { return CLsbCalculator()->StatisticTitle(); }
    TString ResultTitle() const { return "CL_{S}"; }    

    static TString CLsbFileName(const TString& fileName);
    static TString CLbFileName(const TString& fileName);

    bool GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileName, 
                              const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const;
    
    using HftAbsStatCalculator::SetSamplingData;
    bool SetSamplingData(const TString& fileName, const TString& treeName = "tree"); // for CLs+b and CLb
    bool SetSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree); // for CLs+b and CLb

    bool SetCLsbSamplingData(const TString& fileName, const TString& treeName = "tree") { return SetCLsbSamplingData(fileName, treeName, CLsbCalculator()->GetName()); } // for CLs+b
    bool SetCLbSamplingData(const TString& fileName, const TString& treeName = "tree") { return SetCLbSamplingData(fileName, treeName, CLbCalculator()->GetName()); } // for CLb

    bool SetCLsbSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree); // for CLs+b
    bool SetCLbSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree); // for CLb

    static bool SetCLsbSamplingData(HftMultiHypoTestCalculator& multiHypo, const TString& fileName, 
                                   const TString& treeName = "tree", const TString& nameInTree = "");
    static bool SetCLbSamplingData(HftMultiHypoTestCalculator& multiHypo, const TString& fileName, 
                                   const TString& treeName = "tree", const TString& nameInTree = "");

    static bool SetCLsbSamplingData(HftScanLimitCalculator& solver, const TString& fileName, 
                                   const TString& treeName = "tree", const TString& nameInTree = "");
    static bool SetCLbSamplingData(HftScanLimitCalculator& solver, const TString& fileName, 
                                   const TString& treeName = "tree", const TString& nameInTree = "");
    
    bool UpdateState(const HftParameterStorage& toMerge) { return m_clSBCalculator->UpdateState(toMerge) && m_clBCalculator->UpdateState(toMerge); }

    void SetCompHypo(const HftScanPoint& compHypo);
    
    static bool GetExpSigma(const HftAbsHypoTestCalculator& calc, double& sigma);
    static bool GetCompHypo(const HftAbsHypoTestCalculator& calc, HftScanPoint& hypo);
    static bool SetCompHypo(HftAbsHypoTestCalculator& calc, const HftScanPoint& hypo);

    void SetName(const TString& name);
    TString Str(const TString& prefix = "", const TString& options = "") const;
    
    void SetVerbosity(int verbosity) { m_clSBCalculator->SetVerbosity(verbosity); m_clBCalculator->SetVerbosity(verbosity); }
        
  protected:
    
    HftPLRCalculator* m_clSBCalculator, * m_clBCalculator;
  };
}

#endif
