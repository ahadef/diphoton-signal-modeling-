#ifndef ROOT_Hfitter_HftPLRPValueCalculator
#define ROOT_Hfitter_HftPLRPValueCalculator

#include "HfitterStats/HftPLRCalculator.h"


namespace Hfitter {

  /** @class HftPLRPValueCalculator
      @author Andreas Hoecker
      @author Nicolas Berger

    An implementation of @c HftAbsCalculator to compute discovery significances.
  */
 
   
  class HftPLRPValueCalculator : public HftPLRCalculator {

   public:

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting.
    //! @param bkgOnly : true if background-only toys are to be generated. @param signalComponent : name of the signal component of the model.
    HftPLRPValueCalculator(HftModel& model, const TString& hypoVarName, 
                              const Options& fitOptions = Options(), const TString& name = "");

    HftPLRPValueCalculator(HftModel& model, const TString& hypoVarName, double hypoVal,
                              const Options& fitOptions = Options(), const TString& name = "");
    
    HftPLRPValueCalculator(const HftPLRPValueCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0)
      : HftPLRCalculator(other, name, HftScanPoint(), cloner) { }
      
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftPLRPValueCalculator(*this, name, cloner); }

    virtual ~HftPLRPValueCalculator() { }
                
    TString Str(const TString& prefix = "", const TString& options = "") const;
  };
}

#endif
