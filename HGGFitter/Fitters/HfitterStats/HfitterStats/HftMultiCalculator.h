#ifndef ROOT_Hfitter_HftMultiCalculator
#define ROOT_Hfitter_HftMultiCalculator

#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "TString.h"

namespace Hfitter {

  /** @class HftMultiCalculator
      @author Nicolas Berger

  */  
  
  class HftMultiCalculator : public HftAbsMultiResultCalculator {
   public:
     
    HftMultiCalculator(const std::vector<Hfitter::HftAbsCalculator*>& calculators, const TString& name = "") 
      : HftAbsMultiResultCalculator(calculators[0]->Model(), name) { Init(calculators); }

    HftMultiCalculator(HftAbsCalculator& calc1, HftAbsCalculator& calc2, unsigned int current = 0, const TString& name = "");
    HftMultiCalculator(HftAbsCalculator& calc1, HftAbsCalculator& calc2, HftAbsCalculator& calc3, unsigned int current = 0, const TString& name = "");

    HftMultiCalculator(const HftMultiCalculator& other, const TString& name = "");

    virtual ~HftMultiCalculator();
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftMultiCalculator(*this, name); }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { return Calculator()->GetResult(result); }
    bool GetResult(HftInterval& result) const { return Calculator()->GetResult(result); }
    
    unsigned int NResults() const { return m_calculators.size(); }
    unsigned int CurrentIndex() const { return m_current; }
    
    HftAbsCalculator* Calculator(unsigned int i) const { return m_calculators[i]; }
    HftAbsCalculator* Calculator() const { return Calculator(m_current); }
    
    bool SetCurrent(unsigned int index) { m_current = index; return true; }
    bool SetCurrent(const TString& name);
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);
    
    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);          
    bool FillBranches(HftTree& tree);    

    TString ResultName() const  { return Calculator()->ResultName(); } 
    TString ResultTitle() const  { return Calculator()->ResultTitle(); } 
    TString ResultUnit() const { return Calculator()->ResultUnit(); } 

   private:
     
    void Init(const std::vector<Hfitter::HftAbsCalculator*>& calculators);
     
    std::vector<Hfitter::HftAbsCalculator*> m_calculators;
    unsigned int m_current; 
  };
}

#endif
