#ifndef ROOT_Hfitter_HftValueCalculator
#define ROOT_Hfitter_HftValueCalculator

#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "RooRealVar.h"
#include "TString.h"
#include <vector>

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  class HftScanningCalculator;
  
  /** @class HftValueCalculator
      @author Nicolas Berger

  */  
  
  class HftValueCalculator : public HftAbsMultiResultCalculator {
   public:
     
    HftValueCalculator(HftModel& model, const TString& varNames, const TString& name = "");
    HftValueCalculator(HftModel& model, const std::vector<TString>& varNames, const TString& name = "");
    HftValueCalculator(const HftValueCalculator& other, const TString& name = "");

    virtual ~HftValueCalculator();
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const {  return new HftValueCalculator(*this, name); }
    
    unsigned int NResults() const { return m_vars.size(); }
    RooRealVar* Var(unsigned int k) const { return (k >= m_vars.size() ? 0 : m_vars[k]); }

    bool MakeVars(const std::vector<TString>& varNames);
    
    unsigned int CurrentIndex() const { return m_index; }
    bool SetCurrent(unsigned int k) { m_index = k; return true; }
    bool SetCurrent(const TString& name);
    RooRealVar* CurrentVar() const { return Var(m_index); }
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);
    
    using HftAbsCalculator::GetResult;
    bool GetResult(HftInterval& result) const;
    bool GetResult(double& result) const { HftInterval range; bool stat = GetResult(range); result = range.Value(); return stat; }
    
    TString ResultName() const { return (CurrentVar() ? CurrentVar()->GetName() : ""); }
    TString ResultTitle() const { return (CurrentVar() ? CurrentVar()->GetTitle() : ""); }
    TString ResultUnit() const { return (CurrentVar() ? CurrentVar()->getUnit() : ""); }
   
    static bool SetCurrent(HftScanningCalculator& scan, unsigned int k);
    TString Str(const TString& prefix = "", const TString& options = "") const;

   private:
    
    std::vector<RooRealVar*> m_vars;
    unsigned int m_index;
  };
}

#endif
