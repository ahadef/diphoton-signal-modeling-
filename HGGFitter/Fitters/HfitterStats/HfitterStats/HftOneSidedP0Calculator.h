#ifndef ROOT_Hfitter_HftOneSidedP0Calculator
#define ROOT_Hfitter_HftOneSidedP0Calculator

#include "HfitterStats/HftOneSidedPValueCalculator.h"

namespace Hfitter {
  typedef HftOneSidedPValueCalculator HftOneSidedP0Calculator;
}

#endif
