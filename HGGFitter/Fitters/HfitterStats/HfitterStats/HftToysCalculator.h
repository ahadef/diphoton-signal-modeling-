#ifndef ROOT_Hfitter_HftToysCalculator
#define ROOT_Hfitter_HftToysCalculator

#include "HfitterStats/HftAbsStateCalculator.h"
#include "RooRealVar.h"
#include "TString.h"
#include <vector>

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  /** @class HftToysCalculator
      @author Nicolas Berger

  */  
  
  class HftToysCalculator : public HftAbsStateCalculator {
   public:
     
    HftToysCalculator(const HftAbsCalculator& calculator, const HftParameterStorage& state, unsigned int nErrorsMax = 2, 
                      bool generateBinned = false, const TString& name = "");
    HftToysCalculator(HftModel& genModel, const HftParameterStorage& genState, const std::vector<Hfitter::HftAbsCalculator*>& calculators, 
		      const std::vector<Hfitter::HftParameterStorage>& fitStates, unsigned int nErrorsMax = 2, bool generateBinned = false, const TString& name = "");
    HftToysCalculator(HftModel& genModel, const HftParameterStorage& genState, const HftAbsCalculator& calculator, 
                      const HftParameterStorage& fitState = HftParameterStorage(), unsigned int nErrorsMax = 2, bool generateBinned = false, const TString& name = "");
    HftToysCalculator(const HftToysCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);
    
    virtual ~HftToysCalculator();
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftToysCalculator(*this, name); }
    void Init(const std::vector<const Hfitter::HftAbsCalculator*>& calculators, 
              const std::vector<Hfitter::HftParameterStorage>& fitStates, unsigned int nErrorsMax, const HftAbsCalculator* cloner = 0);

    unsigned int NErrorsMax() const { return m_results[0].NErrors(); }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { HftInterval range(0); if (!GetResult(range)) return false; result = range.Value(0); return true; }
    bool GetResult(HftInterval& result) const { return result.CopyValues(m_results[m_current]); }

    unsigned int NCalculators() const { return m_calculators.size(); } 
    HftAbsCalculator* Calculator(unsigned int i) const { return m_calculators[i]; }

    unsigned int CurrentIndex() const { return m_current; }
    HftAbsCalculator* CurrentCalculator() const { return m_calculators[m_current]; }

    void SetCurrentIndex(unsigned int index) { m_current = index; }
    
    RooAbsData* GenerateToy();

    bool RunToys   (unsigned int nToys, const TString& fileName, const TString& treeName = "tree", unsigned int saveInterval = 20);
    bool LoadFromFile(const TString& fileName, const TString& treeName = "tree");

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);
    bool LoadToys(unsigned int nToys, const TString& fileName, const TString& treeName = "tree", unsigned int saveInterval = 20);

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);
    
    TString ResultName()  const { return CurrentCalculator()->ResultName(); }
    TString ResultTitle() const { return CurrentCalculator()->ResultTitle(); }
   
    HftInterval StoredResult(unsigned int i) const { return m_results[i]; }
    double Correlation(unsigned int i, unsigned int j) { return m_correlations[CorrelationIndex(i, j)]; }
    
    unsigned int CorrelationIndex(unsigned int i, unsigned int j) const;
    
    unsigned int NCorrelations() { return (NCalculators()*(NCalculators() + 1))/2; }
    TString Str(const TString& prefix, const TString& options = "") const;
  
    void SetBinned(bool binned = true) { m_generateBinned = binned; }
    bool GenerateBinned() const { return m_generateBinned; }
    void SetGhosts(const TString& var, double x1, double x2, double step, double w = 1E-5) 
    { m_ghostVar = var; m_minGhost = x1; m_maxGhost = x2; m_ghostStep = step; m_ghostWeight = w; }
    
    void SetDoCorrelations(bool doCorr = true) { m_doCorrelations = doCorr; }
    bool DoCorrelations() const { return m_doCorrelations; }

    bool UpdateState(const HftParameterStorage& toMerge);

    void AddExtraVar(RooAbsArg& var) { m_extraVariables.add(var); }
    RooArgList ExtraVariables() const { return m_extraVariables; }
    
    void SetPeekFileName(const TString& name) { m_peekFileName = name; }
    void SetSaveToys(const TString& name) { m_saveToys = name; }
    
  private:

     std::vector<Hfitter::HftAbsCalculator*> m_calculators;
     std::vector<Hfitter::HftParameterStorage> m_fitStates;
     bool m_generateBinned;
     TString m_ghostVar;
     double m_minGhost, m_maxGhost, m_ghostStep, m_ghostWeight;
     std::vector<Hfitter::HftInterval> m_results;
     std::vector<double> m_correlations;
     unsigned int m_current;
     bool m_doCorrelations;
    
     TString m_peekFileName, m_saveToys;
     RooArgList m_extraVariables;
  };
}

#endif
