#ifndef ROOT_Hfitter_HftScanPoint
#define ROOT_Hfitter_HftScanPoint

#include "HfitterModels/HftParameterStorage.h"

namespace Hfitter {

  /** @class HftScanPoint
      @author Nicolas Berger

    A class containing information about a point in a multidimensional grid.
  */

  class HftScanPoint {

   public:
     
    //! default constructor
    HftScanPoint() { }
    HftScanPoint(const HftParameterStorage& state, const std::vector<TString>& vars, 
                 const std::vector<unsigned int>& indices = std::vector<unsigned int>());
    HftScanPoint(const RooRealVar& var);
    HftScanPoint(const TString& varName, double val, double err = 0, bool constness = false);
    HftScanPoint(const std::vector<RooRealVar*>& vars);

    HftScanPoint(const HftScanPoint& point, const HftParameterStorage& newState) 
     : m_state(newState), m_vars(point.Vars()), m_indices(point.Indices()) { }
    
    virtual ~HftScanPoint() { }
    
    //! returns a @c HftParameterStorage object containing the values of the grid variables at this point.
    const HftParameterStorage& State() const { return m_state; }
    HftParameterStorage& State() { return m_state; }
    
    //! returns a vector containing the names of the grid variables.
    const std::vector<TString>& Vars() const { return m_vars; }
    
    //! returns a vector containing the indices of this point along the grid variables.
    const std::vector<unsigned int>& Indices() const { return m_indices; }

    //! returns a label for the point (encoding the grid variable and index information)
    TString Label() const;
    
    //! dump the point information
    void Print() const;
    
    
    bool UpdateValues(const HftParameterStorage& newState) { return m_state.CopyValues(newState); }
    
    friend class HftScanGrid;
    
   protected:

     HftParameterStorage m_state;         //!< the values of the grid variables at this point.
     std::vector<TString> m_vars;         //!< the names of the grid variables.
     std::vector<unsigned int> m_indices; //!< the indices of this point along the grid variables.
  };
}

#endif
