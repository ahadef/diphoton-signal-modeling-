#ifndef ROOT_Hfitter_HftTree
#define ROOT_Hfitter_HftTree

#ifndef __GCCXML__
#include <map>
// #include <tr1/unordered_map>
#endif

#include <vector>

#include "TTree.h"
#include "TString.h"

#include "RooRealVar.h"
#include "RooArgList.h"
#include "HfitterModels/HftParameterStorage.h"

namespace Hfitter {

#ifndef __GCCXML__
  //typedef std::tr1::unordered_map<std::string, double> MapType;
  typedef std::map<std::string, double> MapType;
#endif
  
  /** @class HftTree
      @author Nicolas Berger

    A class containing a TTree, with simple function to add an fill variables.
  */
 
  
  class HftTree {

    public:

      //! constructor. @param treeName : name of the study (will be name of the tree). @param fileName : name of the file where to store the tree.
      HftTree(const TString& treeName, const TString& fileName, bool isNew = true, unsigned int saveInterval = 20);
            
      //! open existing file
      static HftTree* Open(const TString& treeName, const TString& fileName, unsigned int saveInterval = 20);
      
      virtual ~HftTree();

      //! helper function to create a normalized storage name for a variable. This storage name should distinguish variables with identical names
      //! belonging to different models or different blocks. ROOT-unfriendly special characters are also suprressed. 
      //! @param name : the original variable name. 
      //! @param blockName : the name of the storage block in which the variable will be stored.
      static TString VarName(const TString& name, const TString& blockName = "");
            
      //! add a variable to the study tree. @param name : the name of the variable to be stored. 
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also stores the initial value and error of the variable 
      bool AddVar(const TString& name, const TString& blockName = "", bool verbose = false, bool ignoreIfAbsent = false, bool asymErrors = false);
      
      //! add a list of variables to the study tree. Only the floating variables will be used. 
      //! @param vars : the list of variables to be stored.
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also stores the initial value and error of the variable 
      //! @param skipConstant : if true, only non-constant parameters will be added. 
      bool AddVars(const RooArgList& vars, const TString& blockName = "", bool verbose = false, bool skipConstant = true);            

      //! add a list of variables to the study tree. Only the floating variables will be used. 
      //! @param vars : the list of variables to be stored.
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also stores the initial value and error of the variable 
      //! @param skipConstant : if true, only non-constant parameters will be added. 
      bool AddVars(const HftParameterStorage& state, const TString& blockName = "", bool verbose = false, bool skipConstant = true);            
      
      //! fills a tree study variable with the current value of its RooRealVar.
      //! @param var : the variable from which to take the value. 
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also fills the initial value and error of the variable 
      bool FillVar(const RooRealVar& var, const TString& blockName = "", bool verbose = false, bool asymErrors = false);
      
      //! fills a list tree study variables with the current value of their RooRealVars. 
      //! @param vars : the list of variables from which to take the values.
      //! @param blockName : the name of the storage block in which the variable will be stored.
      //! @param verbose : if true, also fills the initial value and error of the variable 
      bool FillVars(const RooArgList& vars, const TString& blockName = "", bool verbose = false, bool skipConstant = true);

      //! fills a list tree study variables with the current value of their RooRealVars. 
      //! @param vars : the list of variables from which to take the values.
      //! @param blockName : the name of the storage block in which the variable will be stored.
      //! @param verbose : if true, also fills the initial value and error of the variable 
      bool FillVars(const HftParameterStorage& state, const TString& blockName = "", bool verbose = false, bool skipConstant = true);

      //! fills a TTree study variable with a specified value.
      //! @param name : the name of the variable to be set.
      //! @param val : the value to set it to.
      //! @param blockName : the name of the storage block in which the variable will be stored.
      bool FillVar(const TString& name, double val, const TString& blockName = "", bool ignoreIfAbsent = false);

      //! fills a tree study variable with the current value of its RooRealVar.
      //! @param var : the variable from which to take the value.
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also fills the initial value and error of the variable 
      bool LoadVar(RooRealVar& var, const TString& blockName = "", bool verbose = false, bool ignoreIfAbsent = false);
      
      //! fills a list tree study variables with the current value of their RooRealVars. 
      //! @param vars : the list of variables from which to take the values. 
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also fills the initial value and error of the variable 
      bool LoadVars(const RooArgList& vars, const TString& blockName = "", bool verbose = false, bool ignoreIfAbsent = false);

      //! fills a list tree study variables with the current value of their RooRealVars. 
      //! @param vars : the list of variables from which to take the values. 
      //! @param blockName : the name of the storage block in which the variable will be stored. 
      //! @param verbose : if true, also fills the initial value and error of the variable 
      bool LoadVars(HftParameterStorage& state, const TString& blockName = "", bool verbose = false, bool ignoreIfAbsent = false);

      //! fills a TTree study variable with a specified value. 
      //! @param name : the name of the variable to be set. 
      //! @param val : the value to set it to.
      //! @param blockName : the name of the storage block in which the variable will be stored.
      bool LoadVal(const TString& name, double& val, const TString& blockName = "", bool ignoreIfAbsent = false);
      bool LoadValErr(const TString& name, double& val, double& err, const TString& blockName = "", bool ignoreIfAbsent = false);
      
      //! return the TFile object for the output.
      TFile* File() const { return m_file; }
      
      //! returns the output TTree.
      TTree* Tree() const { return m_tree; }
      
      operator TTree*() { return Tree(); }
      
      long long GetEntries() const { return m_tree ? m_tree->GetEntries() : -1; }
      bool HasBranch(const TString& varName) const { return m_tree->GetBranch(varName); }
      
      //! fill the tree with the currently stored values.
      void Fill();

      void GetEntry(long long i);
           
      void SetSaveInterval(unsigned int saveInterval) { m_saveInterval = saveInterval; }
 
      bool IsNew() const { return m_isNew; }
      
      bool Write();
 
      void Print() const;

     private:
      
      TFile* m_file; //!< the file in which to store the study TTree.
      TTree* m_tree; //!< the study TTree.
      bool m_isNew;
      unsigned int m_saveInterval;

#ifndef __GCCXML__
      MapType m_vars; //!< the variables in the study: maps name -> value.
#endif
  };
}

#endif
