#include "TString.h"
#include <vector>
#include <map>

#include "HfitterStats/HftLikelihoodRatioCalculator.h"
#include "HfitterStats/HftProfileLikelihoodCalculator.h"

#include "HfitterStats/HftAbsStatCalculator.h"
#include "HfitterStats/HftAbsCLCalculator.h"
#include "HfitterStats/HftAbsP0Calculator.h"

#include "HfitterStats/HftAbsHypoCalculator.h"
#include "HfitterStats/HftMultiHypoCLCalculator.h"
#include "HfitterStats/HftCLSolvingCalculator.h"
#include "HfitterStats/HftIterLimitCalculator.h"

#include "HfitterStats/HftAbsNLLP0Calculator.h"
#include "HfitterStats/HftOneSidedPValueCalculator.h"
#include "HfitterStats/HftTwoSidedP0Calculator.h"
#include "HfitterStats/HftUncappedPValueCalculator.h"
#include "HfitterStats/HftDiscreteP0Calculator.h"

#include "HfitterStats/HftAbsCLsbCalculator.h"
#include "HfitterStats/HftCLsbQBasicCalculator.h"
#include "HfitterStats/HftCLsbQCalculator.h"
#include "HfitterStats/HftCLsbQTildaCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"

#include "HfitterStats/HftDiscreteCLCalculator.h"

#include "HfitterStats/HftExclReprocStudy.h"

#include "HfitterStats/HftScanStudy.h"

template class std::vector<Hfitter::HftMultiHypoCLCalculator*>;

