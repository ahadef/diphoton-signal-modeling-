#ifndef ROOT_Hfitter_HftStatTools
#define ROOT_Hfitter_HftStatTools

#include "TString.h"
#include "RooWorkspace.h"

#include "HfitterModels/HftModel.h"

namespace Hfitter {
  
  /** @class HftTools
      @author Andreas Hoecker
      @author Nicolas Berger
  */  

  class HftAbsHypoTestCalculator;
  
  enum ULType { QLimit, QTildaLimit, CLsQLimit, CLsQTildaLimit };
  enum PValueType { TwoSidedPValue, OneSidedPValue, UncappedPValue };
  
  class HftStatTools {
   public:
    static TString Test() { return "test"; }
    
    static void ComputeInterval(HftModel& model, const TString& variable, RooAbsData& data, const TString& outputFile, 
                                double sigmas = 1);
    static void ComputeInterval(const TString& datacard, const TString& variable, const TString& filename, const TString& outputFile, 
                                double sigmas = 1, const TString& treeName = "tree");
    static void ComputeInterval(RooWorkspace& workspace, const TString& variable, const TString& dataName, const TString& outputFile, 
                                double sigmas = 1);
    
    static void ComputeUpperLimit(HftModel& model, const TString& variable, RooAbsData& data, const TString& outputFile, 
                                  ULType type = CLsQTildaLimit, double cl = 0.95);
    static void ComputeUpperLimit(const TString& datacard, const TString& variable, const TString& filename, const TString& outputFile,
                                  ULType type = CLsQTildaLimit, double cl = 0.95, const TString& treeName = "tree");
    static void ComputeUpperLimit(RooWorkspace& workspace, const TString& variable, const TString& dataName, const TString& outputFile,
                                  ULType type = CLsQTildaLimit, double cl = 0.95);
    
    static void ComputePValue(HftModel& model, const TString& variable, RooAbsData& data, const TString& outputFile, 
                              PValueType type = UncappedPValue);
    static void ComputePValue(const TString& datacard, const TString& variable, const TString& filename, const TString& outputFile, 
                              PValueType type = UncappedPValue, const TString& treeName = "tree");
    static void ComputePValue(RooWorkspace& workspace, const TString& variable, const TString& dataname, const TString& outputFile, 
                              PValueType type = UncappedPValue);
    
    static void ComputeInterval(HftModel& model, const TString& variable, const std::vector<double>& points, RooAbsData& data,
                                const TString& outputFile, double sigmas = 1);
    static void ComputeInterval(const TString& datacard, const TString& variable, const std::vector<double>& points, const TString& filename,
                                const TString& outputFile, double sigmas = 1, const TString& treeName = "tree");
    static void ComputeInterval(RooWorkspace& workspace, const TString& variable, const std::vector<double>& points, const TString& dataName,
                                const TString& outputFile, double sigmas = 1);
    
    static void ComputeUpperLimit(HftModel& model, const TString& variable, const std::vector<double>& points, RooAbsData& data, 
                                  const TString& outputFile, ULType type = CLsQTildaLimit, double cl = 0.95);
    static void ComputeUpperLimit(const TString& datacard, const TString& variable, const std::vector<double>& points, const TString& filename,
                                  const TString& outputFile, ULType type = CLsQTildaLimit, double cl = 0.95, const TString& treeName = "tree");
    static void ComputeUpperLimit(RooWorkspace& workspace, const TString& variable, const std::vector<double>& points, const TString& dataName,
                                  const TString& outputFile, ULType type = CLsQTildaLimit, double cl = 0.95);
    
    static void ComputePValue(HftModel& model, const TString& variable, const std::vector<double>& points, RooAbsData& data,
                              const TString& outputFile, PValueType type = UncappedPValue);
    static void ComputePValue(const TString& datacard, const TString& variable, const std::vector<double>& points, const TString& filename,
                              const TString& outputFile, PValueType type = UncappedPValue, const TString& treeName = "tree");
    static void ComputePValue(RooWorkspace& workspace, const TString& variable, const std::vector<double>& points, const TString& dataName,
                              const TString& outputFile, PValueType type = UncappedPValue);  
    
    static HftAbsHypoTestCalculator* LimitCalc(HftModel& model, const TString& variable, ULType type);
    static HftAbsHypoTestCalculator* PValueCalc(HftModel& model, const TString& variable, PValueType type);
  };
}  
#endif
