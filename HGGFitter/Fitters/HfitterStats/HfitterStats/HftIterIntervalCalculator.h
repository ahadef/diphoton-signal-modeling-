#ifndef ROOT_Hfitter_HftIterIntervalCalculator
#define ROOT_Hfitter_HftIterIntervalCalculator

#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"

#include "HfitterModels/Options.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftMultiHypoTestCalculator.h"


namespace Hfitter {

  class HftScanningCalculator;

  /** @class HftIterIntervalCalculator
      @author Nicolas Berger

  */

  class HftIterIntervalCalculator : public HftAbsCalculator {

   public:
    HftIterIntervalCalculator(const HftAbsHypoTestCalculator& calculator, double z = 1, double tolerance = 1E-4, 
                              const TString& name = ""); 
    HftIterIntervalCalculator(HftModel& model, const TString& varName, const Options& fitOptions = "", 
                              double z = 1, double tolerance = 1E-4, const TString& name = ""); 
    HftIterIntervalCalculator(const HftIterIntervalCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const 
    { return new HftIterIntervalCalculator(*this, name, cloner); }
    
    virtual ~HftIterIntervalCalculator();

    HftAbsHypoTestCalculator* PlusCalculator() const { return m_plusCalc; }
    HftAbsHypoTestCalculator* MinusCalculator() const { return m_minusCalc; }
    void ReplaceCalculator(HftAbsHypoTestCalculator*& targetCalc, HftAbsHypoTestCalculator* newCalc) { delete targetCalc; targetCalc = newCalc; }
    
    RooRealVar* Var() const { return PlusCalculator()->HypoVar(0); }

    double IntervalZ() const { return m_z; }
    double IntervalCL() const { return 2* ROOT::Math::normal_cdf(m_z) - 1; }
    void SetZ(double z) { m_z = z;}
    void SetCL(double cl) { m_z = ROOT::Math::normal_quantile((1 + cl)/2, 1); }
        
    double Tolerance() const { return m_tolerance; }
    void SetTolerance(double t) { m_tolerance = t;}

    unsigned int NIterMax() const { return m_nIterMax; }
    void SetNIterMax(unsigned int n) { m_nIterMax = n; }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(HftInterval& hypo) const;
    bool GetResult(double& hypo) const;
    
    TString ResultName()  const { return Var()->GetName(); }
    TString ResultTitle() const { return Var()->GetTitle(); }
        
    bool MakeBranches(HftTree& tree);

    using HftAbsCalculator::FillBranches;
    bool FillBranches(HftTree& tree);

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    bool SolveForHypo(int direction, RooAbsData& data);
    bool FindNewTestHypo(int direction, double& hypoVal);
    HftAbsHypoTestCalculator* NewHypoCalc(const HftAbsHypoTestCalculator& calc, double newHypoVal, bool keepCompHypo = false) const;
    double NextHypo(double hypo, double sigma, double current, double target, int direction) const;
    
    TString CalculatorName(int i) { return AppendToName(i > 0 ? "plus" : "minus"); }
    
    TString Str(const TString& prefix = "", const TString& options = "") const;
    
    double AsympFunc(double* x, double* p) const;
    
  private:
    
    HftAbsHypoTestCalculator* m_plusCalc, *m_minusCalc;
    double m_z, m_tolerance;
    unsigned int m_nIterMax;
  };
}

#endif
