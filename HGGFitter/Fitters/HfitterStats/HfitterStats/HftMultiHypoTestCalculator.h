#ifndef ROOT_Hfitter_HftMultiHypoTestCalculator
#define ROOT_Hfitter_HftMultiHypoTestCalculator

#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftAbsStatCalculator.h"
#include "HfitterStats/HftAbsHypoTestCalculator.h"


namespace Hfitter {

  /** @class HftMultiHypoTestCalculator
      @author Nicolas Berger

    An implementation of @c HftAbsToyStudy to scan model parameters. Each call to HftAbsToyStudy::Execute is replaced by a loop performing an action for
    each point of the defined scanning grid (distinct from the generation grid used in HftAbsToyStudy). Two new virtual methods, both named ExecuteScan,
    are implemented. The first is executed once before the loop; the second, which takes the fit point as argument, is executed once for every iteration.
    The Initialize step is similarly modified.
  */
     
  class HftMultiHypoTestCalculator : public HftAbsStatCalculator {

   public:

    HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& name = ""); 

    //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& hypoVar, 
                               unsigned int nPoints, double minHypo, double maxHypo, const TString& name = ""); 

        //! Constructor for the case of a single model. @param studyName : a name for the study. @param genModel : the model used for toy generation and fitting. 
    HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& hypoVar, const std::vector<double>& points, 
                               const TString& name = ""); 

    HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& var1,  unsigned int nPoints1, double minHypo1, double maxHypo1, 
                               const TString& var2,  unsigned int nPoints2, double minHypo2, double maxHypo2, const TString& name = "");
    
    HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const std::vector<TString>& vars,
                               const std::vector< std::vector<double> >& pos, const TString& name = "");

    HftMultiHypoTestCalculator(const HftMultiHypoTestCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftMultiHypoTestCalculator(*this, name, cloner); }
    
    virtual ~HftMultiHypoTestCalculator();
    
    //! add a point to the fit scanning grid. 
    //! @param varName : name of the variable along which to add the point
    //! @param val : variable value giving the point position
    void AddScanPoint(const TString& varName, double val);

    //! add a set of equidistant points to the fit scanning grid. 
    //! @param varName : name of the variable along which to add the points
    //! @param nPoints : number of points to add, @param minVal : position of the first point, @param maxVal : position of the last point,
    void AddScanRange(const TString& varName, unsigned int nPoints, double minVal, double maxVal);

    void AddScanRange(const TString& varName, const std::vector<double>& points);

    TString HypoName(unsigned int i) const;
    void UpdateCalculators();
    
    const HftScanGrid& ScanGrid() const { return m_scanGrid; }

    unsigned int NPoints() const { return m_scanGrid.NPoints(); }
    HftAbsHypoTestCalculator* HypoCalculator(unsigned int i) const { return i < NPoints() ? m_hypoCalcs[i] : 0; }
    HftAbsHypoTestCalculator* TemplateCalculator() const { return m_templateCalc; }

    unsigned int NVars() const { return TemplateCalculator()->Hypo().Vars().size(); }
    RooRealVar* Var(unsigned int k = 0) const { return TemplateCalculator()->Model().Var(TemplateCalculator()->Hypo().Vars()[k]); }

    using HftAbsStatCalculator::SetSamplingData;
    bool SetSamplingData(unsigned int i, const TString& fileName, const TString& treeName = "tree", const TString& nameInTree = "");
    bool SetSamplingData(const TString& fileRoot, const TString& treeName = "tree", const TString& nameInTree = "");
    bool GenerateHypoSamplingData(unsigned int i, const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                  const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const;
    bool GenerateHypoSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                  const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const;

    TString SamplingDataFileName(const TString& fileRoot, unsigned int i) const;
    
    bool GetResult(int point, double& result) const { return point < 0 ?  m_templateCalc->GetResult(result) : m_hypoCalcs[point]->GetResult(result);  }
    bool GetResult(double& result) const { return GetResult(m_currentPoint, result);  }

    using HftAbsCalculator::GetResult;
    bool GetResult(int point, HftInterval& result) const { return point < 0 ?  m_templateCalc->GetResult(result) : m_hypoCalcs[point]->GetResult(result); }
    bool GetResult(HftInterval& result) const { return GetResult(m_currentPoint, result); }
    
    void SetCurrentPoint(unsigned int i) { if (i < NPoints()) m_currentPoint = i; }

    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& stat) const { return m_currentPoint < 0 ?  m_templateCalc->GetStatistic(stat) : m_hypoCalcs[m_currentPoint]->GetStatistic(stat); }
  
    TString StatisticName()  const { return m_templateCalc->StatisticName(); }
    TString StatisticTitle() const { return m_templateCalc->StatisticTitle(); }
    TString ResultTitle()    const { return m_templateCalc->ResultTitle(); }
    
    bool GetCurve(HftBand& band, bool pvalue = false, bool stat = false, bool verbose = false) const;
    HftBand CLCurve(unsigned int n = 0) const;
    HftBand PValueCurve(unsigned int n = 0) const;
    HftBand StatisticCurve() const;

    HftBand Curve(const TString& varName, unsigned int n = 1, bool verbose = false) const;
    
    bool FillHist(TH1D& h1, bool qNotCL = false);
    TH1D* Histogram1D(const TString& name = "cl1D", bool qNotCL = false);

    bool FillHist(TH2D& h2, bool qNotCL = false);
    TH2D* CLHistogram2D(const TString& name = "cl");
    TH2D* StatisticHistogram2D(const TString& name = "stat");

    using HftAbsCalculator::MakeBranches;
    //! implementation of the virtual function from @c HftAbsCalculator
    bool MakeBranches(HftTree& tree);
    bool MakeBranches(HftTree& tree, unsigned int first, unsigned int last);

    using HftAbsCalculator::FillBranches;
    //! implementation of the virtual function from @c HftAbsCalculator
    bool FillBranches(HftTree& tree);

    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    bool LoadInputs(RooAbsData& data, unsigned int first, unsigned int last);
    bool LoadInputs(HftTree& tree, unsigned int first, unsigned int last);

    using HftAbsCalculator::LoadFromFile;
    bool LoadFromFile(const TString& fileName, unsigned int first, unsigned int last, const TString& treeName = "tree");

    bool UpdateState(const HftParameterStorage& toMerge);

    TString Str(const TString& prefix = "", const TString& options = "") const;

    // These aren't normally called, due to GetResult itself being overridden
    bool ComputeFromAsymptotics(double stat, double& cl) const { return m_currentPoint < 0 ?  m_templateCalc->ComputeFromAsymptotics(stat, cl) : m_hypoCalcs[m_currentPoint]->ComputeFromAsymptotics(stat, cl); }
    bool ComputeFromSampling(double stat, double& cl) const { return m_currentPoint < 0 ?  m_templateCalc->ComputeFromSampling(stat, cl) : m_hypoCalcs[m_currentPoint]->ComputeFromSampling(stat, cl); }

    bool UseCommonAltHypo() const { return m_useCommonAltHypo; }
    void SetUseCommonAltHypo(bool cah = true) { m_useCommonAltHypo = cah; }
    
    void SetVerbosity(int v);
    int Verbosity() const { return ScanGrid().NPoints() == 0 ? 0 : HypoCalculator(0)->Verbosity(); }
    
   protected:

    HftAbsHypoTestCalculator* m_templateCalc;
    std::vector<Hfitter::HftAbsHypoTestCalculator*> m_hypoCalcs;
    HftScanGrid m_scanGrid;  //!< scan points
    bool m_useCommonAltHypo;
    int m_currentPoint;
  };
}

#endif
