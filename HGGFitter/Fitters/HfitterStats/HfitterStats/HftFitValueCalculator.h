#ifndef ROOT_Hfitter_HftFitValueCalculator
#define ROOT_Hfitter_HftFitValueCalculator

#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "RooRealVar.h"
#include "TString.h"
#include <vector>
#include "HfitterModels/Options.h"

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  class HftScanningCalculator;
  
  /** @class HftFitValueCalculator
      @author Nicolas Berger

  */  
  
  class HftFitValueCalculator : public HftAbsMultiResultCalculator {
   public:
     
    HftFitValueCalculator(HftModel& model, const TString& fitVarNames = "", const Options& fitOptions = Options(),
                          bool includeErrors = true, bool resultIsError = false, bool asymErrors = true, const TString& name = "");
    HftFitValueCalculator(HftModel& model, const std::vector<TString>& fitVarNames, const Options& fitOptions = Options(),
                          bool includeErrors = true, bool resultIsError = false, bool asymErrors = true, const TString& name = "");
    HftFitValueCalculator(const HftFitValueCalculator& other, const TString& name = "");

    virtual ~HftFitValueCalculator();
    
    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* /*cloner*/ = 0) const { return new HftFitValueCalculator(*this, name); }

    static bool ParseNames(const TString& namesString, std::vector<TString>& names);
    
    unsigned int NResults() const { return m_fitVars.size() + m_reals.size(); }
    RooRealVar* FitVar(unsigned int k) const { return (k >= m_fitVars.size() ? 0 : m_fitVars[k]); }
    RooRealVar* Real(unsigned int k) const { return (k >= NResults() ? 0 : (k >= m_fitVars.size() ? m_reals[k - m_fitVars.size()] : m_fitVars[k])); }
    const Options& FitOptions() const { return m_opts; }

    bool MakeVars(const std::vector<TString>& fitVarNames);
    
    unsigned int CurrentIndex() const { return m_index; }
    bool SetCurrent(unsigned int k) { m_index = k; return true; }
    bool SetCurrent(const TString& name);
    RooRealVar* CurrentVar() const { return FitVar(m_index); }
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data);
    bool LoadInputs(HftTree& tree);
    bool LoadInputs(const HftAbsCalculator& other);

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);
    bool GetResult(HftInterval& result) const;
    
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { HftInterval range; bool stat = GetResult(range); result = range.Value(); return stat; }
    
    double Status() const { return m_status; }
    double MinNLL() const { return m_minNLL; }
    
    TString ResultName() const { return (CurrentVar() ? CurrentVar()->GetName() : ""); }
    TString ResultTitle() const { return (CurrentVar() ? CurrentVar()->GetTitle() : ""); }
    TString ResultUnit() const { return (CurrentVar() ? CurrentVar()->getUnit() : ""); }
   
    static bool SetCurrent(HftScanningCalculator& scan, unsigned int k);
    TString Str(const TString& prefix = "", const TString& options = "") const;

    void SetResultIsError(bool rie) { m_resultIsError = rie; }
    bool ResultIsError() const { return m_resultIsError; }
    
    int Verbosity() const { return m_opts.Verbosity(); }
    void SetVerbosity(int verbosity);
    
   private:
    
    std::vector<RooRealVar*> m_fitVars;
    std::vector<RooRealVar*> m_reals;
    bool m_includeErrors, m_resultIsError, m_asymErrors;
    
    unsigned int m_index;
    Options m_opts;
    double m_status, m_minNLL;
  };
}

#endif
