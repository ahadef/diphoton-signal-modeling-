#ifndef ROOT_Hfitter_HftQLimitCalculator
#define ROOT_Hfitter_HftQLimitCalculator

#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterModels/HftModel.h"

namespace Hfitter {

  /** @class HftQLimitCalculator
      @author Nicolas Berger

    An modification of @c HftPLRCalculator to compute exclusion limits using the 'q' estimator
  */
   
  class HftQLimitCalculator : public HftPLRCalculator
  {
   public:

    HftQLimitCalculator(HftModel& model, const TString& hypoVars, const Options& fitOptions = Options(), 
                       const HftParameterStorage& expectedState = HftParameterStorage(), 
                       const TString& name = "", bool freeFirst = false)
      : HftPLRCalculator(model, hypoVars, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                         RooArgList(), name, true, freeFirst) { }

    HftQLimitCalculator(HftModel& model, const HftScanPoint& hypo, const Options& fitOptions = Options(),
                        const HftParameterStorage& expectedState = HftParameterStorage(), 
                        const TString& name = "", bool freeFirst = false)
      : HftPLRCalculator(model, hypo, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                         RooArgList(), name, true, freeFirst) { }

    HftQLimitCalculator(const HftQLimitCalculator& other, const TString& name = "", const HftScanPoint& hypo = HftScanPoint(),
                        const HftAbsCalculator* cloner = 0) : HftPLRCalculator(other, name, hypo, cloner) { }

    using HftAbsHypoTestCalculator::CloneHypo;
    HftAbsHypoTestCalculator* CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* cloner = 0) const 
    { return new HftQLimitCalculator(*this, name, hypo, cloner); }
            
    virtual ~HftQLimitCalculator() { }
       
    using HftAbsStatCalculator::GetStatistic;
    bool GetStatistic(double& q) const;

    TString StatisticName()  const { return "q"; }
    TString StatisticTitle() const { return "q"; }
    TString ResultTitle() const { return "CL_{S+B}"; }

    //! Calculate the exclusion confidence level.
    bool ComputeFromAsymptotics(double q, double& cl) const;
    
    bool GetStatisticBand(int i, double& qBand) const;
    
    TString Str(const TString& prefix = "", const TString& options = "") const;
  };
}

#endif
