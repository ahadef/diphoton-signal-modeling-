#ifndef ROOT_Hfitter_HftAbsStatCalculator
#define ROOT_Hfitter_HftAbsStatCalculator

#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftInterval.h"

#include "TMath.h"

namespace Hfitter {
  
  class HftAbsStatCalculator : public HftAbsCalculator {

   public:

    //! constructor from a single model. This model is used for both generation and fitting. @param studyName : a name for this study. @param genModel : the model used for generation and fitting
    HftAbsStatCalculator(HftModel& model, const TString& name = "", bool resultIsCL = false)
      : HftAbsCalculator(model, name), m_resultIsCL(resultIsCL), m_resultIsStat(false), m_resultIsSignif(false), m_ignoreSamplingData(false) { }

    HftAbsStatCalculator(const HftAbsStatCalculator& other, const TString& name = "");

    virtual ~HftAbsStatCalculator() { }
    
    //! Calculate the confidence level from the statistic, using either asymptotic formulas or sampling
    using HftAbsCalculator::GetResult;
    virtual bool GetResult(double& result) const;

    TString ResultName() const;
    TString ResultTitle() const;
    
    //! Calculate the confidence level from the statistic, using either asymptotic formulas or sampling
    virtual bool ComputeResult(double stat, double& result) const { return (UseSampling() ? ComputeFromSampling(stat, result) : ComputeFromAsymptotics(stat, result)); }

    //! Calculate the confidence level from the statistic, using either asymptotic formulas or sampling
    virtual bool ComputeFromAsymptotics(double stat, double& result) const = 0;

    virtual bool ComputeFromSampling(double stat, double& result) const;

    //! Calculate the statistic
    virtual bool GetStatistic(double& stat) const = 0;

    //! Calculate the confidence level of a dataset
    virtual double Statistic() const { double stat; return GetStatistic(stat) ? stat : -DBL_MAX; }

    // convenience function to get the expected statistic
    static double Statistic(const HftAsimovCalculator& calculator);

    //! Calculate the confidence level of a dataset
    virtual double GetStatistic(RooAbsData& data, double& stat) { return (LoadInputs(data) && GetStatistic(stat)); }

    //! Calculate the confidence level of a dataset
    virtual double Statistic(RooAbsData& data) { double stat; return (GetStatistic(data, stat) ? stat : -DBL_MAX); }

    virtual TString StatisticName() const = 0;
    virtual TString StatisticTitle() const = 0;

    virtual bool SetSamplingData(const TString& fileName, const TString& treeName = "tree") { return SetSamplingData(fileName, treeName, GetName()); }
    virtual bool SetSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree);
    virtual bool UseSampling() const { return m_samplingData.size() > 0 && !IgnoreSamplingData(); }

    virtual bool GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileName, 
                                      const TString& treeName = "tree", bool binned = false, unsigned int saveInterval = 20) const;
    
    const std::vector<double>& SamplingData() const { return m_samplingData; }

    double SamplingError();

    bool ComputeSignificance(double& z) const { double stat; return (GetStatistic(stat) && ComputeSignificance(stat, z)); }

    //! Compute significance from statistic. This version implements the stat->p0->Z pedestrian version, can be overridden if there is a stat->Z shortcut
    virtual bool ComputeSignificance(double stat, double& z) const { double p0; if (!ComputeResult(stat, p0)) return false; z = SignificanceFromPValue(ResultIsCL() ? 1 - p0 : p0); return true; }
    
    //! Compute Z from p0, if ComputeSignificance above was not overridden
    virtual double SignificanceFromPValue(double p0) const { return TMath::Sqrt(TMath::ChisquareQuantile(1 - p0, 1)); }
   
    double Significance() const { double z; if (!ComputeSignificance(z)) return -1; return z; }
    double AsymptoticResult(double stat) { double result; return ComputeFromAsymptotics(stat, result) ? result : 0; }
    bool ResultIsCL() const { return m_resultIsCL; }
    bool ResultIsStatistic() const { return m_resultIsStat; }
    bool ResultIsSignificance() const { return m_resultIsSignif; }
    
    void SetResultIsCL(bool ricl = true) { m_resultIsCL = ricl;  m_resultIsStat = false; m_resultIsSignif = false; }
    void SetResultIsPValue(bool ripv = true) { m_resultIsCL = !ripv;  m_resultIsStat = false; m_resultIsSignif = false; }
    void SetResultIsStatistic(bool ris = true) { m_resultIsStat = ris; m_resultIsSignif = false; }
    void SetResultIsSignificance(bool ris = true) { m_resultIsSignif = ris; m_resultIsCL = false; m_resultIsStat = false; }
    
    TString StatBaseStr(const TString& prefix = "", const TString& type = "") const;

    static double Significance(const HftAsimovCalculator& calculator);
    static bool SetResultIsSignificance(HftAbsCalculator& calculator, bool isSignif = true);
    
    bool IgnoreSamplingData() const { return m_ignoreSamplingData; }
    void SetIgnoreSamplingData(bool isd = true) { m_ignoreSamplingData = isd; }
    
  protected:

    bool m_resultIsCL, m_resultIsStat, m_resultIsSignif;
    std::vector<double> m_samplingData;
    bool m_ignoreSamplingData;
  };
}

#endif
