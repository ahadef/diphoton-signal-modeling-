#ifndef ROOT_Hfitter_HftScanningCalculator
#define ROOT_Hfitter_HftScanningCalculator

#include "HfitterModels/HftParameterStorage.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftScanPointCalculator.h"
#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftToysCalculator.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftTree.h"

#include "TString.h"
#include <vector>

class RooRealVar;
class TGraphAsymmErrors;

namespace Hfitter {

  /** @class HftScanningCalculator
      @author Nicolas Berger

  */  
  
  class HftScanningCalculator : public HftAbsCalculator {
    
   public:
     
    HftScanningCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, unsigned int nPoints, 
                          double minVal, double maxVal, const TString& name = "", bool updateState = true);

    HftScanningCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, 
                          const std::vector<double>& pos, const TString& name = "", bool updateState = true);

    HftScanningCalculator(const std::vector<const Hfitter::HftAbsCalculator*>& calculators, RooRealVar& scanVar, 
                          const std::vector<double>& pos, const TString& name = "", bool updateState = false); // default to false since each calc has been defined separately

    HftScanningCalculator(const HftScanningCalculator& other, const TString& name = "", const HftAbsCalculator* cloner = 0);

    virtual ~HftScanningCalculator();

    HftAbsCalculator* Clone(const TString& name = "", const HftAbsCalculator* cloner = 0) const
    { return new HftScanningCalculator(*this, name, cloner); }
    
    unsigned NPoints() const { return m_pos.size(); }
    double Position(unsigned int i) const { return m_pos[i]; }
    TString FileName(const TString& fileRoot, unsigned int i); 
    
    RooRealVar* ScanVar() const { return m_scanVar; }

    bool DoStateUpdate() const { return m_updateState; }

    HftScanPointCalculator* ScanPointCalculator(unsigned int i = 0) const { return m_calculators[i]; }
    HftAbsCalculator* Calculator(unsigned int i = 0) const { return m_calculators[i]->Calculator(); }
    HftModel& Model() const { return Calculator()->Model(); }
    
    using HftAbsCalculator::LoadInputs;
    bool LoadInputs(RooAbsData& data) { return LoadInputs(data, 0); }
    bool LoadInputs(RooAbsData& data, unsigned int first, unsigned int last = 0);
    bool LoadInputs(HftTree& /*tree*/) { return true; }
    bool LoadInputs(const HftAbsCalculator& other);

    bool LoadInputs(RooAbsData& data, const TString& fileRoot, unsigned int first = 0, unsigned int last = 0, const TString& treeName = "tree");
    bool LoadAsimov(const TString& fileRoot, unsigned int first, unsigned int last = 0, const TString& treeName = "tree");

    bool LoadFromFile(const TString& fileRoot, const TString& treeName = "tree") { return LoadFromFile(fileRoot, 0, 0, treeName); }
    bool LoadFromFile(const TString& fileRoot, unsigned int first, unsigned int last = 0, const TString& treeName = "tree");

    bool SaveToFile(const TString& fileRoot, const TString& treeName = "tree") { return SaveToFile(fileRoot, 0, 0, treeName); }
    bool SaveToFile(const TString& fileRoot, unsigned int first, unsigned int last = 0, const TString& treeName = "tree");

    using HftAbsCalculator::FillBranches;
    bool MakeBranches(HftTree& tree);
    bool FillBranches(HftTree& tree);
        
    const HftScanGrid& Grid() const { return m_grid; }

    bool GetCurve(HftBand& band) const;
    HftBand Curve(unsigned int n = 0) const { HftBand band(n); return GetCurve(band) ? band : HftBand(); }
              
    TGraphAsymmErrors** Draw(double yMin = 0, double yMax = -1, const TString& gOpts = "", const TString& nameRoot = "graph") const;
    
    HftAbsMultiResultCalculator* MultiCalc(unsigned int i = 0) const { return dynamic_cast<Hfitter::HftAbsMultiResultCalculator*>(Calculator(i)); }

    void SetCurrentPoint(unsigned int current) { if (current < m_pos.size()) m_current = current; }
    unsigned int CurrentPoint() const { return m_current; }
    
    using HftAbsCalculator::GetResult;
    bool GetResult(double& result) const { return Calculator(m_current)->GetResult(result); }
    TString ResultName()  const { return Calculator(m_current)->ResultName(); }
    TString ResultTitle() const { return Calculator(m_current)->ResultTitle(); }

    bool LoadAsimov(unsigned int first = 0, unsigned int last = 0);
    bool RunToys(unsigned int nToys, const TString& fileRoot, unsigned int first = 0, unsigned int last = 0,
                 const TString& treeName = "tree", unsigned int saveInterval = 20);
    
    HftAsimovCalculator* AsimovCalculator(unsigned int i) const { return dynamic_cast<Hfitter::HftAsimovCalculator*>(Calculator(i)); }
    HftToysCalculator*     ToysCalculator(unsigned int i) const { return dynamic_cast<Hfitter::HftToysCalculator*>(Calculator(i)); }

    TString Str(const TString& prefix = "", const TString& options = "") const;

    static HftScanningCalculator* GetScanCalc(const TString& name);

    bool DrawPositions(double yMin, double yMax) const;
    
    int Verbosity() const { return NPoints() == 0 ? 0 : Calculator(0)->Verbosity(); }

    void SetIndexOffset(unsigned int i) { m_indexOffset = i; }
    
   protected:

    bool Init(const std::vector<const Hfitter::HftAbsCalculator*>& calculators, const HftAbsCalculator* cloner = 0);
    
    std::vector<Hfitter::HftScanPointCalculator*> m_calculators;
    RooRealVar* m_scanVar;
    HftScanGrid m_grid;
    std::vector<double> m_pos;
    unsigned int m_current, m_indexOffset;
    bool m_updateState;
  };
}

#endif
