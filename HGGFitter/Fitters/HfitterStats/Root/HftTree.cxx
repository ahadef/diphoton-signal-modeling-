#include "HfitterStats/HftTree.h"

#include "HfitterModels/HftData.h" // for makedirs
#include "TFile.h"
#include "HfitterModels/HftModel.h"
#include "RooRealVar.h"
#include "TSystem.h"
#include <map>

#include "TStopwatch.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftTree::HftTree(const TString& treeName, const TString& fileName, bool isNew, unsigned int saveInterval)
  : m_isNew(isNew), m_saveInterval(saveInterval)
{
  if (m_isNew) {
    TString expandedFN = HftData::PrepareDirs(fileName);
    m_file = TFile::Open(expandedFN, "RECREATE");
    m_tree = new TTree(treeName, TString("Study tree for study ") + treeName);
  }
  else {  
    TString fileNameE(gSystem->ExpandPathName(fileName));
    m_file = (gSystem->AccessPathName(fileNameE) ? 0 : TFile::Open(fileNameE));
    m_tree = (m_file && m_file->IsOpen() ? (TTree*)m_file->Get(treeName) : 0);
  }
}


HftTree::~HftTree()
{
  if (m_file) {
    if (m_isNew) {
      Write();
      delete m_tree;
    }
    delete m_file;
  }
}


HftTree* HftTree::Open(const TString& treeName, const TString& fileName, unsigned int saveInterval)
{
  HftTree* hst = new HftTree(treeName, fileName, false, saveInterval);
  if (!hst->Tree()) { delete hst; hst = 0; }
  return hst;
}


TString HftTree::VarName(const TString& name, const TString& blockName)
{ 
  TString varName = "";
  if (blockName != "") varName += blockName + "_";
  varName += name;
  varName.ReplaceAll(";", "_");
  return varName;
}


bool HftTree::AddVar(const TString& name, const TString& blockName, bool verbose, bool ignoreIfAbsent, bool asymErrors)
{ 
  if (verbose) {
    if (!AddVar(name,          blockName, false, ignoreIfAbsent, asymErrors)) return false;
    if (!AddVar(name + "_err", blockName, false, ignoreIfAbsent, asymErrors)) return false;
    if (asymErrors) {
      if (!AddVar(name + "_errNeg", blockName, false, ignoreIfAbsent, asymErrors)) return false;
      if (!AddVar(name + "_errPos", blockName, false, ignoreIfAbsent, asymErrors)) return false;
    }
    return true;
  }
  TString varName = VarName(name, blockName); 
  if (m_vars.find(varName.Data()) != m_vars.end()) {
    cout << "Study variable " << varName << " already exists, cannot add again" << endl;
    return false;
  }

  if (!m_isNew && !HasBranch(varName)) {
    if (ignoreIfAbsent) return true;
    cout << "ERROR : cannot connect to non-existent branch " << varName << " in tree " << m_tree->GetName() << ". Known vars:" << endl;
    for (MapType::const_iterator v = m_vars.begin(); v != m_vars.end(); v++) cout << v->first << endl;
    return false;
  }

  m_vars[varName.Data()] = 0;
  
  if (m_isNew) {
    m_tree->Branch(varName, &m_vars[varName.Data()], varName + "/D");  // will crash if the variable names are too long...
    //cout << "Added branch " << varName << endl;
  }
  else {
    m_tree->SetBranchAddress(varName, &m_vars[varName.Data()]);
    //cout << "Connected branch " << varName << endl;    
  }
//   timer.Stop();
//   cout << "AddVar timing : so far " << timer.RealTime() << " realtime sec, in " << calls 
//        << " calls, avg = " << timer.RealTime()/calls << ", map size = " << m_vars.size() << endl;
  return true;
}


bool HftTree::FillVar(const RooRealVar& var, const TString& blockName, bool verbose, bool asymErrors)
{
  if (verbose) {
    if (!FillVar(TString(var.GetName()), var.getVal(), blockName)) return false;
    if (!FillVar(TString(var.GetName()) + "_err", var.getError(), blockName)) return false;
    if (asymErrors) {
      if (!FillVar(TString(var.GetName()) + "_errNeg", var.getErrorLo(), blockName)) return false;
      if (!FillVar(TString(var.GetName()) + "_errPos", var.getErrorHi(), blockName)) return false;
    }
    return true;
  }

  return FillVar(var.GetName(), var.getVal(), blockName);
}


bool HftTree::FillVar(const TString& name, double val, const TString& blockName, bool ignoreIfAbsent)
{
  TString varName = VarName(name, blockName);

  if (m_vars.find(varName.Data()) == m_vars.end()) {
    if (ignoreIfAbsent) return true;
    cout << "Study variable " << varName << " not found, cannot fill. Known vars:" << endl;
    for (MapType::const_iterator v = m_vars.begin(); v != m_vars.end(); v++) cout << v->first << endl;
    return false;
  }
  //cout << "--> filling " << varName << " -> " << val << endl;
  m_vars[varName.Data()] = val;
  return true;
}


bool HftTree::LoadVar(RooRealVar& var, const TString& blockName, bool verbose, bool ignoreIfAbsent)
{
  if (verbose) {
    double val = 0, err = 0;
    if (!LoadValErr(var.GetName(), val, err, blockName)) return ignoreIfAbsent;
    var.setVal(val);
    var.setError(err);
    return true;
  }
  double val;
  if (!LoadVal(var.GetName(), val, blockName)) return ignoreIfAbsent;
  var.setVal(val);
  return true;
}


bool HftTree::LoadVal(const TString& name, double& val, const TString& blockName, bool ignoreIfAbsent)
{
  TString varName = VarName(name, blockName);

  if (m_vars.find(varName.Data()) == m_vars.end()) {
    //if (ignoreIfAbsent) cout << "ignoring " << varName << endl;
    if (ignoreIfAbsent) return false;
    cout << "ERROR : Study variable " << varName << " not found, cannot get value." << endl;
    cout << "Defined variables are ";
    for (MapType::const_iterator var = m_vars.begin(); var != m_vars.end(); var++)
      cout << var->first << " ";
    cout << endl;
    return false;
  }
  val = m_vars[varName.Data()];
  //cout << "--> getting " << varName << " -> " << val << endl;
  return true;
}


bool HftTree::LoadValErr(const TString& name, double& val, double& err, const TString& blockName, bool ignoreIfAbsent)
{
  bool statusF = LoadVal(name, val, blockName, ignoreIfAbsent);
  if (!statusF) return ignoreIfAbsent;
  bool statusE = LoadVal(name + "_err", err, blockName, ignoreIfAbsent);
  if (!statusE) return ignoreIfAbsent;
  return true;
}


bool HftTree::AddVars(const RooArgList& vars, const TString& blockName, bool verbose, bool skipConstant)
{
  for (int i = 0; i < vars.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(vars.at(i));
    if (!var) continue;
    if (var->isConstant() && skipConstant) continue;
    if (!AddVar(var->GetName(), blockName, verbose, skipConstant)) return false;
  }   
  return true;
}


bool HftTree::AddVars(const HftParameterStorage& state, const TString& blockName, bool verbose, bool skipConstant)
{
  return AddVars(state.StoredVars(), blockName, verbose, skipConstant);
}


bool HftTree::FillVars(const RooArgList& vars, const TString& blockName, bool verbose, bool skipConstant)
{
  for (int i = 0; i < vars.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(vars.at(i));
    if (!var) continue;
    if (var->isConstant() && skipConstant) continue;
    if (!FillVar(*var, blockName, verbose)) return false;
  }
  return true;
}


bool HftTree::FillVars(const HftParameterStorage& state, const TString& blockName, bool verbose, bool skipConstant)
{
  RooArgList vars = state.StoredVars();
  for (int i = 0; i < vars.getSize(); i++) {
    //cout << "filling from state : " << vars.at(i)->GetName() << endl;
    RooRealVar* var = dynamic_cast<RooRealVar*>(vars.at(i));
    if (!var) continue;
    if (var->isConstant() && skipConstant) continue;
    if (!FillVar(var->GetName(), state.Value(*var), blockName, verbose)) return false;
  }
  return true;
}


bool HftTree::LoadVars(const RooArgList& vars, const TString& blockName, bool verbose, bool ignoreIfAbsent)
{
  for (int i = 0; i < vars.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(vars.at(i));
    if (!var) continue;
    if (!LoadVar(*var, blockName, verbose, ignoreIfAbsent)) return false;
  }
  return true;
}


bool HftTree::LoadVars(HftParameterStorage& state, const TString& blockName, bool verbose, bool ignoreIfAbsent)
{
  RooArgList vars = state.StoredVars();
  for (int i = 0; i < vars.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(vars.at(i));
    if (!var) continue;
    double val = 0, err = 0;
    if (verbose) {
      if (!LoadValErr(var->GetName(), val, err, blockName, ignoreIfAbsent)) return ignoreIfAbsent;      
      state.Save(var->GetName(), val, err);
      return true;
    }
    if (!LoadVal(var->GetName(), val, blockName, ignoreIfAbsent)) return ignoreIfAbsent;
    state.Save(var->GetName(), val);
  }
  return true;
}

void HftTree::Fill()
{
  m_tree->Fill();
  if (m_saveInterval > 0 && m_tree->GetEntries() % m_saveInterval == 0) m_tree->AutoSave();
}


void HftTree::GetEntry(long long i)
{
  m_tree->GetEntry(i);
}


bool HftTree::Write()
{
  if (!m_file) return false;
  m_file->cd();
  m_tree->Write();
  return true;
}


void HftTree::Print() const
{
  if (!Tree()) {
    cout << "HftTree: NULL" << endl;
    return;
  }
  TString s = "";
  s += TString("HftTree: ") + Tree()->GetName();
  for (MapType::const_iterator var = m_vars.begin(); var != m_vars.end(); var++) {
    s += TString("\n   ") + Form("%-30s = %g", var->first.c_str(), var->second);
  }
  cout << s << endl;
}
