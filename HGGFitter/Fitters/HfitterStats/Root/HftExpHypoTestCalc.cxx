#include "HfitterStats/HftExpHypoTestCalc.h"
#include "HfitterStats/HftPLRCalculator.h"

#include "TMath.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftExpHypoTestCalc::HftExpHypoTestCalc(const HftAbsHypoTestCalculator& calc, const HftParameterStorage& expectedState, const TString& name)
  : HftAbsHypoTestCalculator(calc, name, calc.Hypo()), m_templateCalc(&calc), m_expectedState(expectedState), m_calc(0)
{
}


HftExpHypoTestCalc::HftExpHypoTestCalc(const HftExpHypoTestCalc& other, const TString& name, const HftScanPoint& hypo)
  : HftAbsHypoTestCalculator(other, name, hypo), m_templateCalc(other.m_templateCalc), m_expectedState(other.m_expectedState), m_calc(0)
{
}


bool HftExpHypoTestCalc::GetExpPLR(double& stat) const
{ 
  HftPLRCalculator* plrCalc = dynamic_cast<HftPLRCalculator*>(HypoTestCalc());
  if (!plrCalc) return false;
  return plrCalc->GetPLR(stat);
}

void HftExpHypoTestCalc::MakeCalc() const
{
  // Here need a mechanism to make sure the cloning is not recursive:
  // HftExpHypoTestCalc -> new Asimov -> new templateCalc -> new HftExpHypoTestCalc etc.
  // So HftPLRCalculator breaks the loop by not performing the new HftExpHypoTestCalc if called from Asimov.
  m_calc = new HftAsimovCalculator(*m_templateCalc, m_expectedState, AppendToName("forQA"));
  m_calc->SetName(AppendToName("forQA_Asimov"));
}


bool HftExpHypoTestCalc::LoadFromAsimov()
{
  if (HypoTestCalc()->Verbosity() >= 2) {
    cout << "INFO: Computing Expected Statistic for Hypothesis Test in " << GetName() << ", expected state = " << endl;
    m_expectedState.Print();
  }
  HftParameterStorage state = Model().CurrentState();
  if (!Calculator()->LoadAsimov()) return false;
  double expStat = 0;
  if (!GetExpStat(expStat)) return false;
  Model().LoadState(state);
  if (HypoTestCalc()->Verbosity() >= 1) {
    cout << "INFO: Success, expected statistic = " << expStat << endl;
  }
  return true;
}


bool HftExpHypoTestCalc::LoadAltHypoInputs(RooAbsData& /*data*/)
{
  // The loading is done in one shot, need to make sure it is early enough: in "AltHypo" if AltHypoFirst, in Hypo if not
  return AltHypoFirst() ? LoadFromAsimov() : true;
}


bool HftExpHypoTestCalc::LoadNullHypoInputs(RooAbsData& /*data*/)
{
  return !AltHypoFirst() ? LoadFromAsimov() : true;
}


bool HftExpHypoTestCalc::LoadAltHypoInputs(const HftAbsCalculator& other)
{
  const HftExpHypoTestCalc* otherEHTCC = dynamic_cast<const HftExpHypoTestCalc*>(&other);
  if (!otherEHTCC) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftExpHypoTestCalc" << endl;
    return false;
  }
  if (!HypoTestCalc()->LoadAltHypoInputs(*otherEHTCC->HypoTestCalc())) return false;
  return true;
}


bool HftExpHypoTestCalc::LoadNullHypoInputs(const HftAbsCalculator& other)
{
  const HftExpHypoTestCalc* otherEHTCC = dynamic_cast<const HftExpHypoTestCalc*>(&other);
  if (!otherEHTCC) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftExpHypoTestCalc" << endl;
    return false;
  }
  if (!HypoTestCalc()->LoadNullHypoInputs(*otherEHTCC->HypoTestCalc())) return false;
  return true;
}


TString HftExpHypoTestCalc::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftExpHypoTestCalc");
  if (options.Index("V") >= 0) s += "\n" + Calculator()->Str(prefix + "  ", options);
  return s;
}
