#include "HfitterStats/HftScanningCalculator.h"

#include "HfitterModels/HftAbsParameters.h"

#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"
#include "TLine.h"
#include "RooFitResult.h"
#include "RooDataSet.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftScanningCalculator::HftScanningCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, unsigned int nPoints, 
                                             double minVal, double maxVal, const TString& name, bool updateState)
  : HftAbsCalculator(calculator, name), m_current(0), m_indexOffset(0), m_updateState(updateState)
{   
  std::vector<const HftAbsCalculator*> calculators;
  for (unsigned int i = 0; i < nPoints; i++) calculators.push_back(&calculator);
  m_scanVar = calculator.Model().Var(scanVarName);
  if (!m_scanVar) {
    cout << "ERROR : scan variable " << scanVarName << " not found in model" << endl;
    return;
  }
  m_grid.Add(*m_scanVar, nPoints, minVal, maxVal);
  for (unsigned int i = 0; i < Grid().NPoints(); i++)
    m_pos.push_back(Grid().Point(i)->State().Value(*ScanVar()));
  Init(calculators);
}


HftScanningCalculator::HftScanningCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, 
                                             const std::vector<double>& pos, const TString& name, bool updateState) 
  : HftAbsCalculator(calculator, name), m_current(0), m_indexOffset(0), m_updateState(updateState)
{ 
  std::vector<const HftAbsCalculator*> calculators;
  for (unsigned int i = 0; i < pos.size(); i++) calculators.push_back(&calculator);
  m_scanVar = calculator.Model().Var(scanVarName);
  if (!m_scanVar) {
    cout << "ERROR : scan variable " << scanVarName << " not found in model" << endl;
    return;
  }
  m_grid.Add(*m_scanVar, pos);
  for (unsigned int i = 0; i < Grid().NPoints(); i++)
    m_pos.push_back(Grid().Point(i)->State().Value(*ScanVar()));
  Init(calculators);
}


HftScanningCalculator::HftScanningCalculator(const std::vector<const HftAbsCalculator*>& calculators, RooRealVar& scanVar, 
                                             const std::vector<double>& pos, const TString& name, bool updateState)
  : HftAbsCalculator(*calculators[0], name), m_scanVar(&scanVar), m_current(0), m_indexOffset(0), m_updateState(updateState)
{ 
  m_grid.Add(*m_scanVar, pos);
  for (unsigned int i = 0; i < Grid().NPoints(); i++)
    m_pos.push_back(Grid().Point(i)->State().Value(*ScanVar()));
  Init(calculators);
}

HftScanningCalculator::HftScanningCalculator(const HftScanningCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsCalculator(other, name), m_scanVar(other.ScanVar()), m_grid(other.Grid()), m_pos(other.m_pos), m_current(0), m_indexOffset(0), m_updateState(other.m_updateState)
{
  std::vector<const HftAbsCalculator*> calculators;
  for (unsigned int i = 0; i < other.m_calculators.size(); i++) calculators.push_back(other.Calculator(i));
  Init(calculators, cloner);
}


HftScanningCalculator::~HftScanningCalculator() 
{ 
  for (unsigned int i = 0; i < NPoints(); i++) delete m_calculators[i]; 
}


bool HftScanningCalculator::Init(const std::vector<const HftAbsCalculator*>& calculators, const HftAbsCalculator* cloner)
{
  if (calculators.size() != Grid().NPoints()) {
    cout << "ERROR : number of calculators provided (" << calculators.size() 
         << ") does not match number of points (" << Grid().NPoints() << ")" << endl;
    return false;
  }
  TString format = "";
  if (GetName().Index("*") >= 0) {
    format = GetName();
    format.ReplaceAll("*", "%d");
    SetName(GetName().ReplaceAll("*", ""));
  }
  for (unsigned int i = 0; i < Grid().NPoints(); i++) {
    TString name = (format == "" ? "" : Form(format, i));
    HftScanPointCalculator* calculator = new HftScanPointCalculator(*calculators[i], *Grid().Point(i), name, m_updateState, cloner);
    m_calculators.push_back(calculator);
  }
  return true;
}


TString HftScanningCalculator::FileName(const TString& fileRoot, unsigned int i)
{
  return fileRoot + Form("_%d.root", i + m_indexOffset);
}


bool HftScanningCalculator::LoadInputs(const HftAbsCalculator& other)
{
  const HftScanningCalculator* otherSC = dynamic_cast<const HftScanningCalculator*>(&other);
  if (!otherSC) {
    cout << "ERROR : can only copy from another HftScanningCalculator" << endl;
    return false;
  }
  if (otherSC->NPoints() != NPoints()) {
    cout << "ERROR : can only copy from another HftScanningCalculator of same size, here copying from size " 
         << otherSC->NPoints() << " to " << NPoints() << endl;
    return false;
  }
  for (unsigned int i = 0; i < NPoints(); i++)
    if (!ScanPointCalculator(i)->LoadInputs(*otherSC->ScanPointCalculator(i))) return false;    
  return true;
}


bool HftScanningCalculator::LoadFromFile(const TString& fileRoot, unsigned int first, unsigned int last, const TString& treeName)
{
  if (last == 0 || last > NPoints()) last = NPoints();
  for (unsigned int i = first; i < last; i++) {
    if (Verbosity() > 0)
      cout << "Loading from file " << FileName(fileRoot, i) << endl;
    if (!ScanPointCalculator(i)->LoadFromFile(FileName(fileRoot, i), treeName)) return false;
  }
  return true; 
}


bool HftScanningCalculator::SaveToFile(const TString& fileRoot, unsigned int first, unsigned int last, const TString& treeName)
{
  if (last == 0 || last > NPoints()) last = NPoints();
  for (unsigned int i = first; i < last; i++) {
    if (Verbosity() > 0)
      cout << "Saving to file " << FileName(fileRoot, i) << endl;
    if (!ScanPointCalculator(i)->SaveToFile(FileName(fileRoot, i), treeName)) return false;    
  }
  return true; 
}


bool HftScanningCalculator::LoadInputs(RooAbsData& data, unsigned int first, unsigned int last)
{
  if (!ScanVar()) return false;
  if (last == 0 || last > NPoints()) last = NPoints();
  HftParameterStorage state;
  if (!Model().SaveState(state)) return false;
  for (unsigned int i = first; i < last; i++) {
    if (Verbosity() > 0)
      cout << "===> Scan point " << i << " of " << NPoints() << " : " << m_pos[i] << " for variable " << ScanVar()->GetName() << endl;    
    if (!Model().LoadState(state)) return false;
    if (!ScanPointCalculator(i)->LoadInputs(data)) return false;    
  }
  return true;
}


bool HftScanningCalculator::LoadInputs(RooAbsData& data, const TString& fileRoot, unsigned int first, unsigned int last, const TString& treeName)
{
  if (LoadFromFile(fileRoot, first, last, treeName)) return true;
  return (LoadInputs(data, first, last) && SaveToFile(fileRoot, first, last, treeName));
}


bool HftScanningCalculator::LoadAsimov(const TString& fileRoot, unsigned int first, unsigned int last, const TString& treeName)
{
  if (LoadFromFile(fileRoot, first, last, treeName)) return true;
  return (LoadAsimov(first, last) && SaveToFile(fileRoot, first, last, treeName));
}


bool HftScanningCalculator::GetCurve(HftBand& band) const
{
  for (unsigned int i = 0; i < NPoints(); i++) {
    HftInterval result = HftInterval::Make(band.NErrors());
    if (!Calculator(i)->GetResult(result)) return false;
    band.Add(Position(i), result);
    if (Verbosity() > 1)
      cout << "point " << i << " of " << NPoints() << ", pos = " << Position(i) 
      << " : var = " << result.Str() << endl;
  }  
  band.SetXLabel(HftAbsParameters::TitleAndUnit(*ScanVar()));
  band.SetYLabel(HftAbsParameters::TitleAndUnit(ResultTitle(), ResultUnit()));
  band.SetYMin(1E-3);
  band.SetYMax(1);
  return true;
}


TGraphAsymmErrors** HftScanningCalculator::Draw(double yMin, double yMax, const TString& gOpts, const TString& nameRoot) const
{
  HftBand curve = Curve();
  TGraphAsymmErrors** graphs = curve.Draw(yMin, yMax, 1, 1, 1, gOpts, nameRoot);
  TLegend* legend = new TLegend(0.2, 0.9, 0.55, 0.65, "", "NDC");
  legend->SetFillStyle(0);
  legend->SetLineColor(0);
  legend->SetShadowColor(0);
  legend->SetBorderSize(0);
  legend->AddEntry(graphs[0], ResultTitle(), "L");
  legend->SetTextSizePixels(20);
  legend->Draw();
  return graphs;
}


bool HftScanningCalculator::LoadAsimov(unsigned int first, unsigned int last)
{
  if (last <= first) last = NPoints();
  HftParameterStorage state;
  if (!Model().SaveState(state)) return false;
  for (unsigned int i = first; i < last; i++) {
    if (Verbosity() > 0)
      cout << endl << "==> [" << ScanVar()->GetName() << " Scan] : Determining Asimov values for point " << i << " of " << NPoints() << endl << endl;
    if (!Model().LoadState(state)) return false;
    if (!ScanPointCalculator(i)->LoadAsimov()) return false;
  }
  return true;
}


bool HftScanningCalculator::RunToys(unsigned int nToys, const TString& fileRoot, unsigned int first, unsigned int last,
                                    const TString& treeName, unsigned int saveInterval)
{
  if (last <= first) last = NPoints();
  for (unsigned int i = first; i < last; i++) 
    if (!ScanPointCalculator(i)->RunToys(nToys, FileName(fileRoot, i), treeName, saveInterval)) return false;
  return true;
}


TString HftScanningCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftScanningCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + ")";
  for (unsigned int i = 0; i < NPoints(); i++)
    s += "\n" + Calculator(i)->Str(prefix + Form("  %s = %6.2f", ScanVar()->GetName(), Position(i)) + " : ", options);
  return s;
}


HftScanningCalculator* HftScanningCalculator::GetScanCalc(const TString& name)
{
  return dynamic_cast<HftScanningCalculator*>(HftAbsCalculator::GetCalc(name));
}


bool HftScanningCalculator::MakeBranches(HftTree& tree)
{
  // Note: this is to write all subcalculators to the same tree, which is not the standard procedure (See SaveToFile)
  for (unsigned int i = 0; i < NPoints(); i++) 
    if (!Calculator(i)->MakeBranches(tree)) return false;
  return true;
}

bool HftScanningCalculator::FillBranches(HftTree& tree)
{
  // Note: this is to write all subcalculators to the same tree, which is not the standard procedure (See SaveToFile)
  for (unsigned int i = 0; i < NPoints(); i++) 
    if (!Calculator(i)->FillBranches(tree)) return false;
  return true;
}

bool HftScanningCalculator::DrawPositions(double yMin, double yMax) const
{
  TLine l;
  l.SetLineStyle(kDashed);
  for (unsigned int i = 0; i < NPoints(); i++)
    l.DrawLine(Position(i), yMin, Position(i), yMax);
  return true;
}
