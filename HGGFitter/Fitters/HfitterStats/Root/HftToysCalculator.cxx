#include "HfitterStats/HftToysCalculator.h"

#include "HfitterModels/HftData.h"
#include "RooDataSet.h"
#include "RooDataHist.h"


#include "TMath.h"
#include "TSystem.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftToysCalculator::HftToysCalculator(const HftAbsCalculator& calculator, const HftParameterStorage& state, 
                                     unsigned int nErrorsMax, bool generateBinned, const TString& name)
  : HftAbsStateCalculator(calculator, state, name), m_generateBinned(generateBinned), 
    m_ghostVar(""), m_minGhost(0), m_maxGhost(0), m_ghostStep(0), m_ghostWeight(0),
    m_current(0), m_doCorrelations(false), m_saveToys("\n")
{
  std::vector<const HftAbsCalculator*> calculators;
  std::vector<HftParameterStorage> fitStates(1);
  calculators.push_back(&calculator);
  Init(calculators, fitStates, nErrorsMax);
}

HftToysCalculator::HftToysCalculator(HftModel& genModel, const HftParameterStorage& genState, 
                                     const std::vector<HftAbsCalculator*>& calculators, const std::vector<HftParameterStorage>& fitStates, 
                                     unsigned int nErrorsMax, bool generateBinned, const TString& name)
  : HftAbsStateCalculator(genModel, genState, name), m_generateBinned(generateBinned),
    m_ghostVar(""), m_minGhost(0), m_maxGhost(0), m_ghostStep(0), m_ghostWeight(0),
    m_current(0), m_doCorrelations(false), m_saveToys("\n")
{
  std::vector<const HftAbsCalculator*> calcs;
  calcs.insert(calcs.begin(), calculators.begin(), calculators.end());
  Init(calcs, fitStates, nErrorsMax);
}


HftToysCalculator::HftToysCalculator(HftModel& genModel, const HftParameterStorage& genState, const HftAbsCalculator& calculator, 
                                     const HftParameterStorage& fitState, unsigned int nErrorsMax, bool generateBinned, const TString& name)
  : HftAbsStateCalculator(genModel, genState, name), m_generateBinned(generateBinned),
    m_ghostVar(""), m_minGhost(0), m_maxGhost(0), m_ghostStep(0), m_ghostWeight(0),
    m_current(0), m_doCorrelations(false), m_saveToys("\n")
{
  std::vector<const HftAbsCalculator*> calcs;
  std::vector<HftParameterStorage> fitStates;
  calcs.push_back(&calculator);
  fitStates.push_back(fitState);
  Init(calcs, fitStates, nErrorsMax);
}


HftToysCalculator::HftToysCalculator(const HftToysCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsStateCalculator(other, name), m_generateBinned(other.m_generateBinned), 
    m_ghostVar(other.m_ghostVar), m_minGhost(other.m_minGhost), m_maxGhost(other.m_maxGhost), m_ghostStep(other.m_ghostStep),
    m_ghostWeight(other.m_ghostWeight), m_current(0), m_doCorrelations(false), m_saveToys("\n"),
    m_extraVariables(other.m_extraVariables)
{
  std::vector<const HftAbsCalculator*> calcs;
  calcs.insert(calcs.begin(), other.m_calculators.begin(), other.m_calculators.end());
  Init(calcs, other.m_fitStates, other.NErrorsMax(), cloner);
}

    
HftToysCalculator::~HftToysCalculator()
{
  for (unsigned int i = 0; i < m_calculators.size(); i++) delete m_calculators[i];
}


void HftToysCalculator::Init(const std::vector<const HftAbsCalculator*>& calculators, 
                             const std::vector<HftParameterStorage>& fitStates, unsigned int nErrorsMax, const HftAbsCalculator* cloner)
{
  for (unsigned int i = 0; i < calculators.size(); i++) {
    m_calculators.push_back(calculators[i]->Clone("", cloner ? cloner : this));
    m_fitStates.push_back(fitStates.size() == 0 ? HftParameterStorage() : fitStates[i]);
    m_results.push_back(HftInterval::Make(nErrorsMax));
  }
  for (unsigned int i = 0; i < NCorrelations(); i++) m_correlations.push_back(0);
}


RooAbsData* HftToysCalculator::GenerateToy()
{
  Model().LoadState(State());
  if (Calculator(0)->Verbosity() > 1) {
    cout << "=== HftToysCalculator(" << GetName() << ") : generating toy for parameter values " << endl;
    Model().CurrentState().Print();
  }
  RooAbsData* toy = (GenerateBinned() ? (RooAbsData*)Model().GenerateBinned() : (RooAbsData*)Model().GenerateEvents());
  if (m_extraVariables.getSize() > 0) {
    if (GenerateBinned()) {
      cout << "WARNING: will not add extra variables ";
      m_extraVariables.writeToStream(cout, true);
      cout << " since binned generation was requested" << endl;
    }
    else {
      ((RooDataSet*)toy)->addColumns(m_extraVariables);
      cout << "INFO: adding extra variables to toy : ";
      m_extraVariables.Print();
   }
  }
  if (m_ghostVar != "") {
    // This seems to be needed, to make a weighted dataset where weighted events can then be inserted.
    if (!GenerateBinned()) {
      RooArgSet vars = *toy->get();
      RooRealVar* weightVar = new RooRealVar("weight", "", 1);
      vars.add(*weightVar);
      RooAbsData* weightedToy = new RooDataSet(toy->GetName(), toy->GetTitle(), (RooDataSet*)toy, vars, "", "weight");
      delete weightVar;
      delete toy;
      toy = weightedToy;
    }
    double sumW = toy->sumEntries();
    HftData::AddGhosts(*toy, Model(), m_ghostVar, m_minGhost, m_maxGhost, m_ghostStep, m_ghostWeight);
    cout << "INFO: added ghosts to toy, sumW=" << sumW << " -> " << toy->sumEntries() << endl;
  }
  // Unconditional ensemble
  for (unsigned int i = 0; i < NCalculators(); i++)
    Calculator(i)->Model().AuxObservables() = Model().AuxObservables();
  if (Calculator(0)->Verbosity() > 1) cout << "generated " << toy->numEntries() << " entries (sum = " << toy->sumEntries() << ")" << endl;
  return toy;
}


bool HftToysCalculator::RunToys(unsigned int nToys, const TString& fileName, const TString& treeName, unsigned int saveInterval)
{
  if (!gSystem->AccessPathName(fileName)) {
    cout << "INFO: file " << fileName << " already exists, will not generate toys." << endl;
    return true;
  }
  HftParameterStorage state;
  Model().SaveState(state);

  HftTree* tree = new HftTree(treeName, fileName, true, saveInterval);
  if (!MakeBranches(*tree)) return false;

  for (unsigned int k = 0; k < nToys; k++) {
    if (k % saveInterval == 0 || Calculator(0)->Verbosity() > 1)
      cout << "Processing toy iteration " << k << " of " << nToys << endl;
    Model().LoadState(state);
    RooAbsData* toy = GenerateToy();
    toy->SetName(Form("toy_%d", k));
    if (Calculator(0)->Verbosity() >= 3) HftData::PrintData(*toy);

//     TFile* f = TFile::Open(Form("temp_%d.root", k), "RECREATE");
//     toy->Write();
//     delete f;
    if (!toy) return false;
    bool result = LoadInputs(*toy);
    if (Calculator(0)->Verbosity() >= 3) Calculator(0)->Print("RVV");
    delete toy;

    if (!result) {
      cout << "ERROR : could not process toy dataset, aborting processing at iteration " << k << endl;
      return false;
    }
    if (!FillBranches(*tree)) {
      cout << "ERROR : could not fill output ntuple, aborting processing at iteration " << k << endl;
      return false;
    }
    tree->Fill();
  }
  delete tree;
  return true;
}


bool HftToysCalculator::LoadFromFile(const TString& fileName, const TString& treeName)
{
  HftTree* tree = HftTree::Open(treeName, fileName);
  return tree && MakeBranches(*tree) && LoadInputs(*tree);
}


bool HftToysCalculator::LoadInputs(RooAbsData& data)
{
  std::vector<HftParameterStorage> currentStates(NCalculators());
// TBFixed ? not the desired behavior if we want to further process the fit result from the first calculator
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->Model().SaveState(currentStates[i])) return false;
  for (unsigned int i = 0; i < NCalculators(); i++) {
    Calculator(i)->Model().LoadState(currentStates[i]);
    Calculator(i)->Model().LoadState(m_fitStates[i]);
    RooAbsData* thisData = 0;
// ** commented out since this loses the weight information, which is bad for ghosts...
//     // Make a new dataset from this particular calculator to enforce the bounds on the dependents, since these may be narrower
//     // for the fit as for the generation. Works only for unbinned case.
//     if (!m_generateBinned) {
//       thisData = new RooDataSet(data.GetName() + TString("_recast"), "", (RooDataSet*)&data, Calculator(i)->Model().ObservablesAndCats(), "", m_ghostVar == "" ? "" : "weight");
//       cout << "INFO: reducing data from " << data.numEntries() << " to " << thisData->numEntries()
//            << " events to enforce bounds for calculator " << i << endl;
//     }
//     else
      thisData = &data;
    if (m_saveToys != "\n") {
      TString fileName = m_saveToys + thisData->GetName() + Form("_%d.root", i);
      cout << "INFO: saving toy to " << fileName << endl;
      Model().ExportWorkspace(fileName, "modelWS", "", thisData);
    }
    if (!Calculator(i)->LoadInputs(*thisData)) {
      cout << "ERROR in calculator " << i << ", aborting computation" << endl;  
      return false;
    }
 //   if (!m_generateBinned) delete thisData;
  }
  return true;
}


bool HftToysCalculator::LoadInputs(HftTree& input)
{
  std::vector< std::vector<double> > results;
  for (unsigned int i = 0; i < NCalculators(); i++) results.push_back(std::vector<double>(input.GetEntries()));

  HftTree* peekTree = 0;
  if (m_peekFileName != "") {
    peekTree = new HftTree("tree", m_peekFileName, true);
    for (unsigned int i = 0; i < NCalculators(); i++) {
      peekTree->AddVar(Calculator(i)->ResultName(), AppendToName(MergeNames("toys", Calculator(i)->GetName())));
      Calculator(i)->MakeBranches(*peekTree);
    }
  }
  
  for (unsigned int k = 0; k < input.GetEntries(); k++) {
    if (k % 100 == 0)  cout << "Loading toy entry " << k << endl;
    input.GetEntry(k);
    for (unsigned int i = 0; i < NCalculators(); i++) {
      double result;
      if (!Calculator(i)->LoadInputs(input) || !Calculator(i)->GetResult(result)) return false;
      if (Calculator(i)->Verbosity() >= 3) Calculator(i)->Print("RVV");
      
      //cout << "entry " << k << " : " << result << endl;
      results[i][k] = result;
      if (peekTree) {
        peekTree->FillVar(Calculator(i)->ResultName(), result, AppendToName(MergeNames("toys", Calculator(i)->GetName())));
        Calculator(i)->FillBranches(*peekTree);
      }
    }
    if (peekTree) peekTree->Fill();
  }
  
  if (peekTree) delete peekTree;
  
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!HftInterval::MedianWithErrors(results[i], m_results[i]), false) return false;
  if (NErrorsMax() == 0) return true;
  if (!m_doCorrelations) return true;
  
  std::vector<double> mean(NCalculators()), variance(NCalculators());
  for (unsigned int i = 0; i < NCalculators(); i++) {    
    for (unsigned int k = 0; k < results[i].size(); k++) { 
      mean[i] += results[i][k];
      variance[i] += results[i][k]*results[i][k];
    }
    mean[i] /= results[i].size();
    variance[i] /= results[i].size();
    variance[i] -= mean[i]*mean[i];
    for (unsigned int j = 0; j <= i; j++) {
      double sum12 = 0;
      for (unsigned int k = 0; k < results[i].size(); k++) sum12 += results[i][k]*results[j][k];
      sum12 /= results[i].size();
      m_correlations[CorrelationIndex(i, j)] = (sum12 - mean[i]*mean[j])/TMath::Sqrt(variance[i]*variance[j]);
    }
  }
  return true;
}


bool HftToysCalculator::LoadInputs(const HftAbsCalculator& other)
{
  const HftToysCalculator* otherToys = dynamic_cast<const HftToysCalculator*>(&other);
  if (!otherToys) {
    cout << "ERROR: cannot load content from calculator not deriving from HftToysCalculator." << endl;
    return false;
  }
  if (otherToys->NCalculators() != NCalculators()) {
    cout << "ERROR: cannot load content from HftToysCalculator of size (" << otherToys->NCalculators()
         << ") different from ours (" << NCalculators() << ")." << endl;
    return false;
  }
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadInputs(*otherToys->Calculator(i))) return false;
  m_results = otherToys->m_results;
  m_correlations = otherToys->m_correlations;
  return true;
}


bool HftToysCalculator::LoadToys(unsigned int nToys, const TString& fileName, const TString& treeName, unsigned int saveInterval)
{
  return RunToys(nToys, fileName, treeName, saveInterval) && LoadFromFile(fileName, treeName);
}


bool HftToysCalculator::MakeBranches(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++) {
    if (!Calculator(i)->MakeBranches(tree)) return false;
    if (!tree.AddVar(Calculator(i)->ResultName(), GetName() + "_" + Calculator(i)->GetName())) return false;
  }
  return true; 
}


bool HftToysCalculator::FillBranches(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++) {
    if (!Calculator(i)->FillBranches(tree)) return false;
    double result;
    if (!Calculator(i)->GetResult(result)) return false;
    if (!tree.FillVar(Calculator(i)->ResultName(), result, GetName() + "_" + Calculator(i)->GetName())) return false;
  }
  return true; 
}


unsigned int HftToysCalculator::CorrelationIndex(unsigned int i, unsigned int j) const
{
  unsigned int a = (i >= j ? i : j);
  unsigned int b = (i >= j ? j : i);
  return (a*(a + 1))/2 + b;
}


TString HftToysCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = "HftToysCalculator: " + GetName();
  for (unsigned int i = 0; i < NCalculators(); i++) 
    s += TString("\n") + Calculator(i)->Str("   " + prefix, options);
  return s;

}


bool HftToysCalculator::UpdateState(const HftParameterStorage& toMerge) 
{ 
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->UpdateState(toMerge)) return false;
  return HftAbsStateCalculator::UpdateState(toMerge);
}
