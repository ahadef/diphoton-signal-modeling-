#include "HfitterStats/HftDataCountingCalculator.h"

#include "RooDataSet.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftDataCountingCalculator::HftDataCountingCalculator(HftModel& model, const TString& catName, const TString& name)
  : HftAbsMultiResultCalculator(model, name), m_index(0)
{ 
  m_cat = model.Cat(catName);
  if (!m_cat) cout << "WARNING: category " << catName << " not found in model" << endl;
  m_counts.resize(NResults());
}


HftDataCountingCalculator::HftDataCountingCalculator(const HftDataCountingCalculator& other, const TString& name)
  : HftAbsMultiResultCalculator(other, name), m_index(0), m_cat(other.m_cat)
{
  m_counts.resize(NResults());
}


bool HftDataCountingCalculator::SetCurrent(const TString& name)
{
  if (!m_cat) return false;
  const RooCatType* tt = m_cat->lookupType(name);
  if (!tt) return false;
  m_index = (unsigned int)tt->getVal();
  return true;
}


TString HftDataCountingCalculator::Label(unsigned int index) const
{
  if (!m_cat) return "";
  const RooCatType* tt = m_cat->lookupType(index);
  if (!tt) return "";
  return tt->GetName();
}


bool HftDataCountingCalculator::LoadInputs(RooAbsData& data)
{
  if (!m_cat) return false;
  m_counts.clear();
  for (unsigned int k = 0; k < NResults(); k++)
    m_counts.push_back(data.sumEntries(Form("%s==%d", m_cat->GetName(), k)));
  return true;
}


bool HftDataCountingCalculator::LoadInputs(HftTree& input)
{
  for (unsigned int k = 0; k < NResults(); k++) {
    double count = 0;
    if (!input.LoadVal("nEvents_" + Label(k), count, GetName())) return false;
    m_counts[k] = (unsigned int)(count + 0.5);
  }
  return true;
}


bool HftDataCountingCalculator::LoadInputs(const HftAbsCalculator& other)
{  
  const HftDataCountingCalculator* mvOther = dynamic_cast<const HftDataCountingCalculator*>(&other);
  if (!mvOther) {
    cout << "ERROR : cannot copy from a calculator of type other than HftDataCountingCalculator." << endl;
    return false;
  }
  if (mvOther->Cat() != Cat()) {
    cout << "ERROR : cannot copy from HftDataCountingCalculator with different category as ours." << endl;
    return false;
  }
  m_counts.clear();
  for (unsigned int k = 0; k < NResults(); k++) m_counts.push_back(mvOther->Counts(k));
  return true;
}


bool HftDataCountingCalculator::MakeBranches(HftTree& tree)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!tree.AddVar("nEvents_" + Label(k), GetName())) return false;
  return true;
}


bool HftDataCountingCalculator::FillBranches(HftTree& tree) 
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!tree.FillVar("nEvents_" + Label(k), m_counts[k], GetName())) return false;
  return true;
}


TString HftDataCountingCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftDataCountingCalculator: " + GetName();
  for (unsigned int i = 0; i < NResults(); i++) {
    s += TString("\n") + prefix + "   " + Label(i);
    if (options.Index("V") >= 0) s += Form("%d", Counts(i));
  }
  return s;
}
