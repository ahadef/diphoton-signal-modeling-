#include "HfitterStats/HftMultiHypoTestCalculator.h"

#include "HfitterStats/HftTree.h"
#include "RooRealVar.h"

#include "TFile.h"
#include "TTree.h"
#include "TH2D.h"

#include "HfitterModels/HftAbsParameters.h"
#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftFitValueCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;



HftMultiHypoTestCalculator::HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& name)
  : HftAbsStatCalculator(calc.Model(), name),
    m_templateCalc(calc.CloneHypo()), m_useCommonAltHypo(false), m_currentPoint(-1)
{ 
}


HftMultiHypoTestCalculator::HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& hypoVar, 
                                                       unsigned int nPoints, double minHypo, double maxHypo, const TString& name)
  : HftAbsStatCalculator(calc.Model(), name),
    m_templateCalc(calc.CloneHypo()), m_useCommonAltHypo(false), m_currentPoint(-1)
{ 
  TString var = (hypoVar != "" ? hypoVar : calc.Hypo().Vars()[0]);
  AddScanRange(var, nPoints, minHypo, maxHypo);
}


HftMultiHypoTestCalculator::HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& hypoVar, 
                                                       const std::vector<double>& points, const TString& name)
  : HftAbsStatCalculator(calc.Model(), name),
    m_templateCalc(calc.CloneHypo()), m_useCommonAltHypo(false), m_currentPoint(-1)
{ 
  TString var = (hypoVar != "" ? hypoVar : calc.Hypo().Vars()[0]);
  AddScanRange(var, points);
}


HftMultiHypoTestCalculator::HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const TString& var1,  unsigned int nPoints1, 
                                                   double minHypo1, double maxHypo1, const TString& var2,  unsigned int nPoints2, 
                                                   double minHypo2, double maxHypo2, const TString& name)
  : HftAbsStatCalculator(calc.Model(), name),
    m_templateCalc(calc.CloneHypo()), m_useCommonAltHypo(false), m_currentPoint(-1)
{ 
  AddScanRange(var1, nPoints1, minHypo1, maxHypo1);
  AddScanRange(var2, nPoints2, minHypo2, maxHypo2);
}


HftMultiHypoTestCalculator::HftMultiHypoTestCalculator(const HftAbsHypoTestCalculator& calc, const std::vector<TString>& vars,
                                                   const std::vector< std::vector<double> >& pos, const TString& name)
  : HftAbsStatCalculator(calc.Model(), name),
    m_templateCalc(calc.CloneHypo()), m_useCommonAltHypo(false), m_currentPoint(-1)
{ 
  if (vars.size() != pos.size()) {
    cout << "ERROR: cannot construct HftMultiHypoTestCalculator since vector of variables and positions are of different sizes." << endl;
    return;
  }
  for (unsigned int i = 0; i < vars.size(); i++) AddScanRange(vars[i], pos[i]);
}


HftMultiHypoTestCalculator::HftMultiHypoTestCalculator(const HftMultiHypoTestCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsStatCalculator(other, name), 
    m_templateCalc(other.m_templateCalc->CloneHypo(cloner)), m_scanGrid(other.ScanGrid()), m_useCommonAltHypo(other.m_useCommonAltHypo),
    m_currentPoint(other.m_currentPoint)
{
  for (std::vector<HftAbsHypoTestCalculator*>::const_iterator calc = other.m_hypoCalcs.begin();
       calc != other.m_hypoCalcs.end(); calc++)
     m_hypoCalcs.push_back((*calc)->CloneHypo(cloner));
}


HftMultiHypoTestCalculator::~HftMultiHypoTestCalculator()
{
  delete m_templateCalc;
  for (unsigned int i = 0; i < m_hypoCalcs.size(); i++) delete m_hypoCalcs[i]; 
}


void HftMultiHypoTestCalculator::AddScanPoint(const TString& varName, double val)
{
  RooRealVar var(varName, "", 0);
  var.setConstant(true);
  m_scanGrid.Add(var, val);
  UpdateCalculators();
}


void HftMultiHypoTestCalculator::AddScanRange(const TString& varName, unsigned int nPoints, double minVal, double maxVal)
{
  RooRealVar var(varName, "", 0);
  var.setConstant(true);
  m_scanGrid.Add(var, nPoints, minVal, maxVal);
  UpdateCalculators();
}


void HftMultiHypoTestCalculator::AddScanRange(const TString& varName, const std::vector<double>& points)
{
  RooRealVar var(varName, "", 0);
  var.setConstant(true);
  m_scanGrid.Add(var, points);
  UpdateCalculators();
}


TString HftMultiHypoTestCalculator::HypoName(unsigned int i) const
{
  return HftAbsParameters::Merge(GetName(), ScanGrid().Point(i)->Label());
}


void HftMultiHypoTestCalculator::UpdateCalculators()
{
  for (unsigned int i = m_hypoCalcs.size(); i < ScanGrid().NPoints(); i++)
    m_hypoCalcs.push_back(m_templateCalc->CloneHypo(*ScanGrid().Point(i), HypoName(i)));
}


bool HftMultiHypoTestCalculator::MakeBranches(HftTree& tree)
{
  return MakeBranches(tree, 0, ScanGrid().NPoints());
}

 
bool HftMultiHypoTestCalculator::MakeBranches(HftTree& tree, unsigned int first, unsigned int last)
{
  if (last > ScanGrid().NPoints()) last = ScanGrid().NPoints();
  //if (!m_templateCalc->MakeGlobalBranches(tree)) return false;
  //cout << "Making branches for " << last-first << " points." << endl;
  for (unsigned int i = first; i < last; i++) if (!m_hypoCalcs[i]->MakeBranches(tree)) return false;
  return true;
}
 

bool HftMultiHypoTestCalculator::FillBranches(HftTree& tree)
{ 
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) if (!m_hypoCalcs[i]->FillBranches(tree)) return false;
  return true;
}


bool HftMultiHypoTestCalculator::LoadInputs(RooAbsData& data)
{
  return LoadInputs(data, 0, ScanGrid().NPoints());
}


bool HftMultiHypoTestCalculator::LoadInputs(RooAbsData& data, unsigned int first, unsigned int last)
{
  if (last > ScanGrid().NPoints()) last = ScanGrid().NPoints();
  HftParameterStorage commonState;
  if (!Model().SaveState(commonState)) { // make sure we use the same state each time
    cout << "ERROR : could not save calculator initial state for scan" << endl;
    return false;
  }
  for (unsigned int i = first; i < last; i++) {
    if (HypoCalculator(0)->Verbosity() >= 1) {
      cout << "==> Processing point " << i << " of " << ScanGrid().NPoints() << ", state : " << endl;
      ScanGrid().Point(i)->State().Print();
    }
    if (!Model().LoadState(commonState)) { // make sure we use the same state each time
      cout << "ERROR : could not load calculator initial state at scan point " << i << endl;
      return false;
    }
    bool status = (!m_useCommonAltHypo || i == 0) ? m_hypoCalcs[i]->LoadInputs(data) : m_hypoCalcs[i]->LoadInputs(*m_hypoCalcs[0], data);
    if (!status) return false;
  }
  return true;
}


bool HftMultiHypoTestCalculator::LoadInputs(HftTree& tree)
{
  if (NPoints() == 0 && NVars() == 1) {
    unsigned int nMax = 1000;  // Support maximum of 1000 hypos...
    AddScanRange(Var(0)->GetName(), nMax, 0, 1);
    std::vector<double> hypos;
    for (unsigned int i = 0; i < nMax; i++) {
      //cout << "Looking for branch " << HypoName(i) << " : " << (tree.HasBranch(HypoName(i)) ? "yes" : "no") << endl;
      if (!tree.HasBranch(HypoName(i))) break;
      double val;
      tree.AddVar(Var(0)->GetName(), MergeNames(HypoName(i), "nullHypo"));
      tree.LoadVal(Var(0)->GetName(), val, MergeNames(HypoName(i), "nullHypo"));
      hypos.push_back(val);
    }
    cout << "INFO : automatically loaded " << hypos.size() << " hypotheses from input TTree" << endl;

    m_scanGrid = HftScanGrid();
    AddScanRange(Var(0)->GetName(), hypos);
    UpdateCalculators();
  }
  return LoadInputs(tree, 0, ScanGrid().NPoints());
}


bool HftMultiHypoTestCalculator::LoadInputs(HftTree& tree, unsigned int first, unsigned int last)
{
  if (last > ScanGrid().NPoints()) last = ScanGrid().NPoints();
  for (unsigned int i = first; i < last; i++) {
    if (!m_hypoCalcs[i]->LoadInputs(tree)) return false;
    m_scanGrid.SetPoint(i, m_hypoCalcs[i]->NullHypo());
  }
  return true;
}


bool HftMultiHypoTestCalculator::LoadFromFile(const TString& fileName, unsigned int first, unsigned int last, const TString& treeName)
{
  HftTree* tree = HftTree::Open(treeName, fileName);
  if (!tree || !MakeBranches(*tree, first, last)) return false;
  tree->GetEntry(0);
  if (!LoadInputs(*tree, first, last)) return false;
  delete tree;
  return true;
}


bool HftMultiHypoTestCalculator::LoadInputs(const HftAbsCalculator& other)
{
  //cout << "njpb : HftMultiHypoTestCalculator::LoadInputs" << endl;
  const HftMultiHypoTestCalculator* otherScanCalc = dynamic_cast<const HftMultiHypoTestCalculator*>(&other);
  if (!otherScanCalc) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftMultiHypoTestCalculator" << endl;
    return false;
  }
  if (!m_templateCalc->LoadInputs(*otherScanCalc->TemplateCalculator())) return false;
  if (NPoints() != otherScanCalc->NPoints()) {
    m_scanGrid = otherScanCalc->m_scanGrid;
    UpdateCalculators();
  }
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) {
    //cout << "==> Copying data for point " << i << " of " << ScanGrid().NPoints() << endl;
    if (!m_hypoCalcs[i]->LoadInputs(*otherScanCalc->HypoCalculator(i))) return false;
  }
  return true;  
}


bool HftMultiHypoTestCalculator::GetCurve(HftBand& band, bool pvalue, bool stat, bool verbose) const
{
  verbose |= HypoCalculator(0)->Verbosity() > 2;
  if (ScanGrid().NVariables() != 1) {
    cout << "ERROR : can only handle 1D distributions of CL for curve output" << endl;
    return false;
  }
  TString scanVarName = ScanGrid().Variable(0);
  RooRealVar* scanVar = Model().Var(scanVarName);
  if (!scanVar) {
    cout << "ERROR : scan variable " << scanVarName << " not defined in model." << endl;
    return false;
  }
//   HftParameterStorage save;
//   Model().SaveState(save);
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) {
    HypoCalculator(i)->SetResultIsCL();
    if (pvalue) HypoCalculator(i)->SetResultIsPValue();
    if (stat) HypoCalculator(i)->SetResultIsStatistic();
    if (verbose) cout << "--> Point " << i <<  " of " << NPoints() << " (bands = " << band.NErrors() << "): ";
    HftInterval clRange = HftInterval::Make(band.NErrors());
    if (!GetResult(i, clRange)) return false;
    if (verbose) {
      cout << "pos = " << ScanGrid().Point(i)->State().Value(*scanVar) << ", CL = "; clRange.Print();
    }
    band.Add(ScanGrid().Point(i)->State().Value(*scanVar), clRange);
  }
  //  Model().LoadState(save);
  band.SetXLabel(HftAbsParameters::TitleAndUnit(*scanVar));
  band.SetYLabel(HftAbsParameters::TitleAndUnit(stat ? StatisticTitle() : ResultTitle(), 
                                                stat ? "" : ResultUnit()));
  return true;
}


HftBand HftMultiHypoTestCalculator::Curve(const TString& varName, unsigned int nErrors, bool verbose) const
{
  HftBand band(nErrors);
  if (ScanGrid().NVariables() != 1) {
    cout << "ERROR : can only handle 1D distributions of CL for curve output" << endl;
    return false;
  }
  TString scanVarName = ScanGrid().Variable(0);
  RooRealVar* scanVar = Model().Var(scanVarName);
  if (!scanVar) {
    cout << "ERROR : scan variable " << scanVarName << " not defined in model." << endl;
    return false;
  }
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) {
    if (verbose) cout << "--> Point " << i <<  " of " << NPoints() << " : ";
    RooRealVar* var = HypoCalculator(i)->Model().Var(varName);
    if (!var) {
      cout << "ERROR : variable " << varName << " not defined in model at point " << i << endl;
      return HftBand();
    }
    HftInterval range = HftInterval::Make(1);
    range.SetValue(0, var->getVal());
    if (nErrors > 0) range.SetError(1, var->getError());
    if (verbose) cout << "pos = " << ScanGrid().Point(i)->State().Value(*scanVar) << ", val = " << range.Value() << endl;
    band.Add(ScanGrid().Point(i)->State().Value(*scanVar), range);
  }
  return band;
}


HftBand HftMultiHypoTestCalculator::CLCurve(unsigned int n) const
{
  HftBand band(n);
  if (!GetCurve(band)) return HftBand();
  return band;
}


HftBand HftMultiHypoTestCalculator::PValueCurve(unsigned int n) const
{
  HftBand band(n);
  if (!GetCurve(band, true)) return HftBand();
  return band;
}


HftBand HftMultiHypoTestCalculator::StatisticCurve() const
{
  HftBand band;
  if (!GetCurve(band, false, true)) return HftBand();
  return band;
}


bool HftMultiHypoTestCalculator::FillHist(TH1D& h1, bool qNotCL)
{
  if (ScanGrid().NVariables() != 1) {
    cout << "ERROR : can only handle 1D distributions of CL for TH1D output" << endl;
    return false;
  }
  TString scanVarName1 = ScanGrid().Variable(0);
  RooRealVar* scanVar1 = Model().Var(scanVarName1);
  if (!scanVar1) {
    cout << "ERROR : scan variable " << scanVarName1 << " not defined in model." << endl;
    return false;
  }
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) {
    cout << "--> Point " << i <<  " of " << NPoints() << " : ";
    SetCurrentPoint(i);
    double result;
    if (qNotCL) {
      if (!HypoCalculator(i)->GetStatistic(result)) return false;
    }
    else if (!GetResult(result)) return false;
    cout << "pos = " << ScanGrid().Point(i)->State().Value(*scanVar1) << " : "
         << (qNotCL ? "q" : "CL") << " = " << result << endl;
    h1.Fill(ScanGrid().Point(i)->State().Value(*scanVar1), result);
  }
  return true;
}


TH1D* HftMultiHypoTestCalculator::Histogram1D(const TString& name, bool qNotCL)
{
  TH1D* h1 = ScanGrid().Empty1DHistogram(name);
  if (!FillHist(*h1, qNotCL)) return 0;
  return h1;
}


bool HftMultiHypoTestCalculator::FillHist(TH2D& h2, bool qNotCL)
{
  if (ScanGrid().NVariables() != 2) {
    cout << "ERROR : can only handle 2D distributions of CL for TH2D output" << endl;
    return false;
  }
  TString scanVarName1 = ScanGrid().Variable(0);
  RooRealVar* scanVar1 = Model().Var(scanVarName1);
  if (!scanVar1) {
    cout << "ERROR : scan variable " << scanVarName1 << " not defined in model." << endl;
    return false;
  }
  TString scanVarName2 = ScanGrid().Variable(1);
  RooRealVar* scanVar2 = Model().Var(scanVarName2);
  if (!scanVar2) {
    cout << "ERROR : scan variable " << scanVarName2 << " not defined in model." << endl;
    return false;
  }
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) {
    cout << "--> Point " << i <<  " of " << NPoints() << " : ";
    SetCurrentPoint(i);
    double result;
    if (qNotCL) {
      if (!HypoCalculator(i)->GetStatistic(result)) return false;
    }
    else if (!GetResult(result)) return false;
    cout << "pos = " << ScanGrid().Point(i)->State().Value(*scanVar1) << ", " << ScanGrid().Point(i)->State().Value(*scanVar2) << " : "
         << (qNotCL ? "q" : "CL") << " = " << result << endl;
    h2.Fill(ScanGrid().Point(i)->State().Value(*scanVar1), ScanGrid().Point(i)->State().Value(*scanVar2), result);
  }
  return true;
}


TH2D* HftMultiHypoTestCalculator::CLHistogram2D(const TString& name)
{
  TH2D* h2 = ScanGrid().Empty2DHistogram(name);
  if (!FillHist(*h2)) return 0;
  return h2;
}


TH2D* HftMultiHypoTestCalculator::StatisticHistogram2D(const TString& name)
{
  TH2D* h2 = ScanGrid().Empty2DHistogram(name);
  if (!FillHist(*h2, true)) return 0;
  return h2;
}


bool HftMultiHypoTestCalculator::SetSamplingData(unsigned int i, const TString& fileName, const TString& treeName, const TString& nameInTree) 
{ 
  if (i >= NPoints()) return false;
  cout << "HftMultiHypoTestCalculator : associating point " << i << " with file " << fileName << ", tree '" << treeName << "'." << endl;
  return HypoCalculator(i)->SetSamplingData(fileName, treeName, nameInTree == "" ? HypoCalculator(i)->GetName() : nameInTree);
}


bool HftMultiHypoTestCalculator::SetSamplingData(const TString& fileRoot, const TString& treeName, const TString& nameInTree)
{
  for (unsigned int i = 0; i < NPoints(); i++)
    if (!SetSamplingData(i, SamplingDataFileName(fileRoot, i), treeName, nameInTree)) return false;
  return true;
}


bool HftMultiHypoTestCalculator::GenerateHypoSamplingData(unsigned int i, const HftParameterStorage& state, unsigned int nToys, 
                                                        const TString& fileRoot, const TString& treeName, bool binned,
                                                        unsigned int saveInterval) const
{
  if (HypoCalculator(0)->Verbosity() >= 1) {
    cout << Form("INFO : HftMultiHypoTestCalculator: generating sampling data for point %d using %d toys, saving to %s -- gen hypo below:", 
                 i, nToys, SamplingDataFileName(fileRoot, i).Data()) << endl;
    HypoCalculator(i)->Hypo().Print();
  }
  return HypoCalculator(i)->GenerateSamplingData(state, nToys, SamplingDataFileName(fileRoot, i), treeName, binned, saveInterval);
}


bool HftMultiHypoTestCalculator::GenerateHypoSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileRoot, 
                                                        const TString& treeName, bool binned, unsigned int saveInterval) const
{
  HftParameterStorage currentState = Model().CurrentState();
  for (unsigned int i = 0; i < NPoints(); i++) {
    Model().LoadState(currentState);
    if (!GenerateHypoSamplingData(i, state, nToys, fileRoot, treeName, binned, saveInterval)) return false;
  }
  Model().LoadState(currentState);
  return true;
}

TString HftMultiHypoTestCalculator::SamplingDataFileName(const TString& fileRoot, unsigned int i) const
{
  return fileRoot + Form("_%d", i) + ".root";
}


bool HftMultiHypoTestCalculator::UpdateState(const HftParameterStorage& toMerge) 
{ 
  for (unsigned int i = 0; i < NPoints(); i++)
    if (!HypoCalculator(i)->UpdateState(toMerge)) return false;
  return TemplateCalculator()->UpdateState(toMerge);
}

TString HftMultiHypoTestCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = BaseStr(prefix, "HftMultiHypoTestCalculator");
  s += "\n" + TemplateCalculator()->Str(prefix + "  template : ", options);
  RooRealVar* scanVar = Var(0);
  if (scanVar) {
    for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) {
      s += "\n" + HypoCalculator(i)->Str(prefix + Form("  %s = %6.2f", scanVar->GetName(), ScanGrid().Point(i)->State().Value(*scanVar)) + " : ", options);
    }
  }
  return s;
}


void HftMultiHypoTestCalculator::SetVerbosity(int v)
{
  for (unsigned int i = 0; i < ScanGrid().NPoints(); i++) HypoCalculator(i)->SetVerbosity(v);
}
