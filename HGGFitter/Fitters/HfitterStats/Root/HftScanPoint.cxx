#include "HfitterStats/HftScanPoint.h"

#include "HfitterModels/HftAbsParameters.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftScanPoint::HftScanPoint(const HftParameterStorage& state, const std::vector<TString>& vars, 
                           const std::vector<unsigned int>& indices)
  : m_state(state), m_vars(vars), m_indices(indices)
{
  if (m_indices.size() == 0) m_indices.resize(m_vars.size());
}


HftScanPoint::HftScanPoint(const RooRealVar& var)
{
  m_state.Save(var);
  m_vars.push_back(var.GetName());
  m_indices.push_back(0);
}


HftScanPoint::HftScanPoint(const TString& varName, double val, double err, bool constness)
{
  m_state.Save(varName, val, err, constness);
  m_vars.push_back(varName);
  m_indices.push_back(0);
}


HftScanPoint::HftScanPoint(const std::vector<RooRealVar*>& vars)
{
  for (std::vector<RooRealVar*>::const_iterator var = vars.begin(); var != vars.end(); var++) {
    m_state.Save(**var);
    m_vars.push_back((*var)->GetName());
    m_indices.push_back(0);
  }
}


TString HftScanPoint::Label() const
{
  TString label = "";
  for (unsigned int k = 0; k < m_indices.size(); k++) {
    label = HftAbsParameters::Merge(label, Form("%s%d", m_vars[k].Data(), m_indices[k]));
  }
  return label;
}


void HftScanPoint::Print() const
{
  for (unsigned int k = 0; k < m_indices.size(); k++) {
    if (k > 0) cout << ", ";
    cout << m_vars[k] << "(idx = " << m_indices[k] << ") = " << State().Value(RooRealVar(m_vars[k], "", 0));
  }
  cout << endl;
}
