#include "HfitterStats/HftPLRLimitCalculator.h"

using namespace Hfitter;


TString HftPLRLimitCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftPLRLimitCalculator");
  s += "\n" + HftPLRCalculator::Str(prefix + "  ", options);
  return s;
}
