#include "HfitterStats/HftPLRPValueCalculator.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"


using namespace Hfitter;


HftPLRPValueCalculator::HftPLRPValueCalculator(HftModel& model, const TString& hypoVarName, 
                                               const Options& fitOptions, const TString& name)
: HftPLRCalculator(model, HftScanPoint(hypoVarName, 0), fitOptions, HftParameterStorage(), 
                   HftParameterStorage(), HftParameterStorage(), RooArgList(), name)
{
}


HftPLRPValueCalculator::HftPLRPValueCalculator(HftModel& model, const TString& hypoVarName, double hypoVal,
                                                 const Options& fitOptions, const TString& name)
: HftPLRCalculator(model, HftScanPoint(hypoVarName, hypoVal), fitOptions, HftParameterStorage(), 
                   HftParameterStorage(), HftParameterStorage(), RooArgList(), name)
{
}


TString HftPLRPValueCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftPLRPValueCalculator");
  s += PLRStr(prefix + "  ", options);
  if (options.Index("V") >= 0)
    s += "\n" + prefix + " Z = " + Form("%g", Significance())  + "\n";
  return s;
}
