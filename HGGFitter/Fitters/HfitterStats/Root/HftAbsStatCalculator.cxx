#include "HfitterStats/HftAbsStatCalculator.h"

#include "TSystem.h"

#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftToysCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

HftAbsStatCalculator::HftAbsStatCalculator(const HftAbsStatCalculator& other, const TString& name)
  : HftAbsCalculator(other, name), 
    m_resultIsCL(other.m_resultIsCL), m_resultIsStat(other.m_resultIsStat), m_resultIsSignif(other.m_resultIsSignif),
    m_samplingData(other.m_samplingData), m_ignoreSamplingData(false)
{
}


bool HftAbsStatCalculator::GetResult(double& result) const
{
  if (m_resultIsStat) return GetStatistic(result);
  if (m_resultIsSignif) return ComputeSignificance(result);
  double stat;
  if (!GetStatistic(stat) || !ComputeResult(stat, result)) return false;
  return true;
}
  


TString HftAbsStatCalculator::ResultName() const
{ 
  if (m_resultIsStat) return StatisticName();
  if (m_resultIsSignif) return "Z";
  if (m_resultIsCL) return "cl";
  return "p0";
}
  
TString HftAbsStatCalculator::ResultTitle() const
{ 
  if (m_resultIsStat) return StatisticTitle();
  if (m_resultIsSignif) return "Significance";
  if (m_resultIsCL) return "Confidence Level";
  return "p-value";
}


bool HftAbsStatCalculator::SetSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree)
{
  HftTree* tree = HftTree::Open(treeName, fileName);
  if (!tree) {
    cout << "ERROR : cannot open tree '" << treeName << "' in file " << fileName << ", cannot set sampling data." << endl;
    return false;
  }
  if (!tree->AddVar(StatisticName(), nameInTree)) {
    cout << "ERROR : cannot find statistic variable " << StatisticName() << " in tree, cannot set sampling data." << endl;
    delete tree;
    return false;
  }

  // We only need one branch
  tree->Tree()->SetBranchStatus("*", 0);
  tree->Tree()->SetBranchStatus(tree->VarName(StatisticName(), nameInTree), 1);

  unsigned int nentries = tree->GetEntries(); /// this is expensive -- just do it once!
  cout << "Calculator " << GetName() << " : reading " << nentries << " entries of sampling data" << " from " << fileName << endl;
  m_samplingData.resize(nentries);
  double stat = 0;
  for (unsigned int i = 0; i < nentries; i++) { 
    tree->GetEntry(i);
    if (!tree->LoadVal(StatisticName(), stat, nameInTree)) {
      cout << "ERROR reading sampling data" << endl;
      delete tree;
      return false;
    }
    m_samplingData[i] = stat;
  }
  delete tree;
  return true;
}


bool HftAbsStatCalculator::GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileName, 
                                                const TString& treeName, bool binned, unsigned int saveInterval) const
{
  HftToysCalculator* toyCalc = new HftToysCalculator(*this, state, 2, binned);
  if (!gSystem->AccessPathName(fileName)) {
    cout << "INFO: GenerateSamplingData: will reuse already-generated toys in " << fileName << endl;
    return true;
  }
  bool result = toyCalc->RunToys(nToys, fileName, treeName, saveInterval);
  delete toyCalc;
  return result;
}


bool HftAbsStatCalculator::ComputeFromSampling(double stat, double& result) const
{
  if (m_samplingData.size() == 0) {
    cout << Form("ERROR : sampling data has not been set for calculator '%s', cannot compute result using the sampling method.", GetName().Data()) << endl;
    return false;
  }
  double nAbove = 0;
  for (std::vector<double>::const_iterator s = m_samplingData.begin(); s != m_samplingData.end(); s++)
    if (*s >= stat) nAbove++;
  result = (!m_resultIsCL ? nAbove/m_samplingData.size() : (m_samplingData.size() - nAbove)/m_samplingData.size());
//   for (std::vector<double>::const_iterator s = m_samplingData.begin(); s != m_samplingData.end(); s++)
//     cout << *s << " : " << (*s >= stat ? "above" : "below") << endl;
   //cout << "   * njpb : computing sampling for " << GetName() << " : " << stat << " -> " << result << ", " << nAbove << "/" << m_samplingData.size() << endl;
  return true;
}


double HftAbsStatCalculator::SamplingError()
{
  if (m_samplingData.size() == 0) {
    cout << "ERROR : sampling data has not been set, cannot compute sampling error." << endl;
    return false;
  }
  double stat;
  if (!GetStatistic(stat)) return -1;
  double nAbove = 0;
  for (std::vector<double>::const_iterator s = m_samplingData.begin(); s != m_samplingData.end(); s++)
    if (*s >= stat) nAbove++;
  double frac = nAbove/m_samplingData.size();
  return sqrt(frac*(1 - frac)/m_samplingData.size());
}


double HftAbsStatCalculator::Statistic(const HftAsimovCalculator& calculator)
{
  HftAbsStatCalculator* stat = dynamic_cast<HftAbsStatCalculator*>(calculator.Calculator());
  if (!stat) return 0;
  return stat->Statistic();
}

TString HftAbsStatCalculator::StatBaseStr(const TString& prefix, const TString& typeStr) const
{ 
  TString str = prefix + typeStr + ": " + GetName() 
  + " (" + ResultName() + " = " + Form("%g", Result()) 
  + ", " + StatisticName() + " = " + Form("%g", Statistic()) + ")";
  if (m_samplingData.size() > 0) str += "\n" + prefix + Form("  using sampling data with %lu entries", m_samplingData.size());
  return str;
}


double HftAbsStatCalculator::Significance(const HftAsimovCalculator& calculator)
{
  HftAbsStatCalculator* statCalc = dynamic_cast<HftAbsStatCalculator*>(calculator.Calculator());
  if (!statCalc) return 0;
  return statCalc->Significance();
}


bool HftAbsStatCalculator::SetResultIsSignificance(HftAbsCalculator& calculator, bool isSignif)
{
  HftAbsStatCalculator* statCalc = dynamic_cast<HftAbsStatCalculator*>(&calculator);
  if (statCalc) {
    statCalc->SetResultIsSignificance(isSignif);
    return true;
  }
  HftAsimovCalculator* asimov = dynamic_cast<HftAsimovCalculator*>(&calculator);
  if (asimov) return SetResultIsSignificance(*asimov->Calculator(), isSignif);
  
  HftToysCalculator* toys = dynamic_cast<HftToysCalculator*>(&calculator);
  if (toys)  {
    for (unsigned int i = 0; i < toys->NCalculators(); i++)
      if (!SetResultIsSignificance(*toys->Calculator(i), isSignif)) return false;
    return true;
  }

  HftScanningCalculator* scan = dynamic_cast<HftScanningCalculator*>(&calculator);
  if (scan) {
    for (unsigned int i = 0; i < scan->NPoints(); i++)
      if (!SetResultIsSignificance(*scan->Calculator(i), isSignif)) return false;
    return true;
  }
  return false;
}
