#include "HfitterStats/HftScanLimitCalculator.h"

#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "RooRealVar.h"

#include "TFile.h"
#include "TTree.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftScanLimitCalculator::HftScanLimitCalculator(const HftAbsHypoTestCalculator& calculator, const TString& hypoVar,
                                               unsigned int nPoints, double minHypo, double maxHypo, const TString& name)
  : HftAbsCalculator(calculator), 
    m_calculator(new HftMultiHypoTestCalculator(calculator, hypoVar, nPoints, minHypo, maxHypo, name)),
    m_cl(0.95), m_order(2)
{ 
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanLimitCalculator::HftScanLimitCalculator(const HftAbsHypoTestCalculator& calculator, const TString& hypoVar, const std::vector<double>& points, 
                                               const TString& name)
  : HftAbsCalculator(calculator), 
    m_calculator(new HftMultiHypoTestCalculator(calculator, hypoVar, points, name)),
    m_cl(0.95), m_order(2)
{ 
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanLimitCalculator::HftScanLimitCalculator(const HftScanLimitCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsCalculator(other, name), m_calculator((HftMultiHypoTestCalculator*)other.Calculator()->Clone(name, cloner)), 
  m_cl(other.SolvingCL()), m_order(other.m_order)
{
}


HftScanLimitCalculator::HftScanLimitCalculator(const HftAbsHypoTestCalculator& calculator, unsigned int nPoints, double minHypo, double maxHypo, 
                                             const TString& hypoVar, const TString& name, double cl, int order)
  : HftAbsCalculator(calculator), 
    m_calculator(new HftMultiHypoTestCalculator(calculator, hypoVar, nPoints, minHypo, maxHypo, name)),
    m_cl(cl), m_order(order)
{ 
  // TBD check for duplicate hypos, otherwise trouble later
}


bool HftScanLimitCalculator::GetResult(HftInterval& signal) const
{
  HftBand clBand(signal.NErrors());
  if (!Calculator()->GetCurve(clBand)) return false;
  if (!clBand.FindPosition(SolvingCL(), signal, m_order)) return false;
  signal = signal.Flip(); // high-side hypo error correspond to the lower-side error of the individual CL computations, so flip the range
  return true;
}


bool HftScanLimitCalculator::MakeBranches(HftTree& tree)
{
  if (!tree.AddVar(ResultName(), GetName())) return false;
  return Calculator()->MakeBranches(tree);
}


bool HftScanLimitCalculator::FillBranches(HftTree& tree)
{
  double result;
  if (!GetResult(result)) return false;
  if (!tree.FillVar(ResultName(), result, GetName())) return false;
  return Calculator()->FillBranches(tree);
}


bool HftScanLimitCalculator::LoadInputs(RooAbsData& data)
{ 
  return Calculator()->LoadInputs(data);
}


TString HftScanLimitCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftScanLimitCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + " = " + Form("%g", Result()) + ")";
  if (options.Index("R") >= 0)
    s += "\n" + Calculator()->Str(prefix + "   ", options);
  return s;
}


bool HftScanLimitCalculator::DrawHypos(const TString& hypoPar, double xMin, double xMax) const
{
  for (unsigned int i = 0; i < Calculator()->NPoints(); i++) 
    if (!Calculator()->HypoCalculator(i)->DrawHypos(hypoPar, xMin, xMax)) return false;
  return true;
}


bool HftScanLimitCalculator::DrawHypos(const HftScanningCalculator& scanCalc, const TString& hypoPar)
{
  for (unsigned int i = 0; i < scanCalc.NPoints(); i++) {
    const HftScanLimitCalculator* solver = dynamic_cast<const HftScanLimitCalculator*>(scanCalc.Calculator(i));
    if (!solver) {
      cout << "ERROR: at point " << i << ", calculator is not of type HftScanLimitCalculator" << endl;
      return false;
    }
    double pos  = scanCalc.Position(i);
    double prev = scanCalc.Position(i == 0 ? i : i - 1);
    double next = scanCalc.Position(i == scanCalc.NPoints() - 1 ? i : i + 1);
    if (!solver->DrawHypos(hypoPar, (pos + prev)/2, (pos + next)/2)) return false;
  }
  return true;
}
