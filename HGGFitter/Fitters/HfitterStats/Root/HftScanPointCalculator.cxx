#include "HfitterStats/HftScanPointCalculator.h"

#include "HfitterStats/HftAbsStateCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftToysCalculator.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

HftScanPointCalculator::HftScanPointCalculator(const HftAbsCalculator& calculator, const HftScanPoint& point, const TString& name, bool updateState,
                                               const HftAbsCalculator* cloner)
  : HftAbsCalculator(calculator, name), m_calculator(calculator.Clone(name, cloner)), m_point(point) 
{
  if (updateState) m_calculator->UpdateState(point.State());
}


HftScanPointCalculator::HftScanPointCalculator(const HftScanPointCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsCalculator(other, name), m_calculator(other.Calculator()->Clone(name, cloner)), m_point(other.Point()) 
{
}


bool HftScanPointCalculator::LoadInputs(RooAbsData& data) 
{ 
  Model().LoadState(Point().State());
  return Calculator()->LoadInputs(data); 
}


bool HftScanPointCalculator::LoadInputs(const HftAbsCalculator& other)
{ 
  const HftScanPointCalculator* otherSPC = dynamic_cast<const HftScanPointCalculator*>(&other);
  if (!otherSPC) {
    cout << "ERROR : cannot copy from calculator not deriving from HftScanPointCalculator" << endl;
    return false;
  }
  m_point = otherSPC->Point();
  return Calculator()->LoadInputs(other); 
}


bool HftScanPointCalculator::MakeBranches(HftTree& tree)
{ 
  return tree.AddVars(Point().State(), GetName()) && Calculator()->MakeBranches(tree); 
}


bool HftScanPointCalculator::FillBranches(HftTree& tree)
{ 
  return tree.FillVars(Point().State(), GetName()) && Calculator()->FillBranches(tree);
}


bool HftScanPointCalculator::LoadAsimov()
{
  HftAsimovCalculator* asimovCalc = dynamic_cast<HftAsimovCalculator*>(Calculator());
  if (!asimovCalc) return false;
  
  Model().LoadState(Point().State());
  // Point().State().Print();
  
  return asimovCalc->LoadAsimov();
}


bool HftScanPointCalculator::RunToys(unsigned int nToys, const TString& fileName, const TString& treeName, unsigned int saveInterval)
{
  HftToysCalculator* toysCalc = dynamic_cast<HftToysCalculator*>(Calculator());
  if (!toysCalc) return false;
  
  Model().LoadState(Point().State());
  return toysCalc->RunToys(nToys, fileName, treeName, saveInterval);
}
