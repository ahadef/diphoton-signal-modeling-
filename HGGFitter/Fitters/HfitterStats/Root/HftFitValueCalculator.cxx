#include "HfitterStats/HftFitValueCalculator.h"

#include "HfitterStats/HftScanningCalculator.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"
#include "RooFitResult.h"
#include "RooDataSet.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftFitValueCalculator::HftFitValueCalculator(HftModel& model, const TString& fitVarNames, const Options& opts, 
                                             bool includeErrors, bool resultIsError, bool asymErrors, const TString& name)
  : HftAbsMultiResultCalculator(model, name), m_includeErrors(includeErrors), m_resultIsError(resultIsError), m_asymErrors(asymErrors),
    m_index(0), m_opts(opts), m_status(0), m_minNLL(0)
{
  std::vector<TString> fitVarVect; ParseNames(fitVarNames, fitVarVect);
  if (!MakeVars(fitVarVect)) return;
}


HftFitValueCalculator::HftFitValueCalculator(HftModel& model, const std::vector<TString>& fitVarNames, const Options& opts, 
                                             bool includeErrors, bool resultIsError, bool asymErrors, const TString& name)
  : HftAbsMultiResultCalculator(model, name), m_includeErrors(includeErrors), m_resultIsError(resultIsError), m_asymErrors(asymErrors),
    m_index(0), m_opts(opts), m_status(0), m_minNLL(0)
{ 
  if (!MakeVars(fitVarNames)) return;
}


HftFitValueCalculator::HftFitValueCalculator(const HftFitValueCalculator& other, const TString& name)
  : HftAbsMultiResultCalculator(other, name), m_includeErrors(other.m_includeErrors), m_resultIsError(other.m_resultIsError),
    m_asymErrors(other.m_asymErrors), m_index(0), m_opts(other.FitOptions()), m_status(0), m_minNLL(0)
{
  std::vector<TString> fitVarNames;
  for (unsigned int k = 0; k < other.NResults(); k++)
    fitVarNames.push_back(other.Real(k)->GetName());
  if (!MakeVars(fitVarNames)) return;
}


bool HftFitValueCalculator::MakeVars(const std::vector<TString>& fitVarNames)
{
  std::vector<TString> fvn;
  if (fitVarNames.size() == 0) {
    RooArgList freeVars = Model().FreeParameters();
    for (int i = 0; i < freeVars.getSize(); i++) fvn.push_back(freeVars.at(i)->GetName());
  }
  else
    fvn = fitVarNames;
  for (std::vector<TString>::const_iterator fitVarName = fvn.begin(); fitVarName != fvn.end(); fitVarName++) {
    RooAbsReal* real = Model().Real(*fitVarName);
    RooRealVar* fitVar = Model().Var(*fitVarName);
    if (!real) {
      cout << "ERROR : fitting variable " << *fitVarName << " not found in model" << endl;
      m_fitVars.clear();
      m_reals.clear();
      return false;
    }
    if (fitVar)
      m_fitVars.push_back((RooRealVar*)fitVar->clone(fitVar->GetName()));
    else
      m_reals.push_back(new RooRealVar(real->GetName(), real->GetTitle(), 0));
  }
  return true;
}


HftFitValueCalculator::~HftFitValueCalculator()
{
  for (unsigned int k = 0; k < NResults(); k++) delete Real(k);  
}


bool HftFitValueCalculator::ParseNames(const TString& namesString, std::vector<TString>& names)
{
  TObjArray* namesArray = namesString.Tokenize(",");
  if (!namesArray) return false;

  // backward compat
  if (namesArray->GetEntries() == 1 && namesString.Index(":") >= 0) {
    delete namesArray;
    namesArray = namesString.Tokenize(":");
    if (!namesArray) return false;
  }  

  for (int i = 0; i < namesArray->GetEntries(); i++) names.push_back(namesArray->At(i)->GetName());
  delete namesArray;
  return true;
}

    
bool HftFitValueCalculator::SetCurrent(const TString& name)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (Real(k)->GetName() == name) {
      SetCurrent(k);
      return true;
    }
  return false;
}


bool HftFitValueCalculator::LoadInputs(RooAbsData& data)
{
  int status = 0;
  RooFitResult* result = Model().Fit(data, FitOptions(), &m_minNLL, &status);
  m_status = status;
  for (unsigned int k = 0; k < NResults(); k++) {
    Real(k)->setVal(Model().Real(Real(k)->GetName())->getVal());
    Real(k)->setError(0);
  }
  for (unsigned int k = 0; k < m_fitVars.size(); k++) {
    FitVar(k)->setVal(Model().Var(FitVar(k)->GetName())->getVal());
    FitVar(k)->setError(Model().Var(FitVar(k)->GetName())->getError());
    if (Verbosity() > 0) FitVar(k)->Print();  
  }
  delete result;
  return true;
}


bool HftFitValueCalculator::LoadInputs(HftTree& input)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!input.LoadVar(*Real(k), GetName(), m_includeErrors)) return false;
  input.LoadVal("status", m_status, GetName(), true); // status is not crucial, so ignore it for now 
  input.LoadVal("minNLL", m_minNLL, GetName(), true); // status is not crucial, so ignore it for now
  return true;
}


bool HftFitValueCalculator::LoadInputs(const HftAbsCalculator& other)
{  
  const HftFitValueCalculator* mvOther = dynamic_cast<const HftFitValueCalculator*>(&other);
  if (!mvOther) {
    cout << "ERROR : cannot copy from a calculator of type other than HftFitValueCalculator." << endl;
    return false;
  }
  if (mvOther->NResults() != NResults()) {
    cout << "ERROR : cannot copy from HftFitValueCalculator with a number of variables ("
    << mvOther->NResults() << ") different from ours " << NResults() << ")." << endl;
    return false;
  }
  for (unsigned int k = 0; k < m_fitVars.size(); k++) {
    FitVar(k)->setVal(mvOther->FitVar(k)->getVal());
    FitVar(k)->setError(mvOther->FitVar(k)->getError());
  }
  m_status = mvOther->Status();
  m_minNLL = mvOther->MinNLL();
  return true;
}


bool HftFitValueCalculator::MakeBranches(HftTree& tree)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!tree.AddVar(Real(k)->GetName(), GetName(), m_includeErrors, false, m_asymErrors)) return false;
  tree.AddVar("status", GetName(), false, true); // status is not crucial, so ignore it for now 
  tree.AddVar("minNLL", GetName(), false, true); // status is not crucial, so ignore it for now 
  return true;
}


bool HftFitValueCalculator::FillBranches(HftTree& tree) 
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!tree.FillVar(*Real(k), GetName(), m_includeErrors, m_asymErrors)) return false;
  if (!tree.FillVar("status", m_status, GetName())) return false;
  if (!tree.FillVar("minNLL", m_minNLL, GetName())) return false;
  return true;
}


bool HftFitValueCalculator::GetResult(HftInterval& result) const
{
  if (!CurrentVar()) return false;
  if (result.NErrors() > 1) {
    cout << "WARNING : cannot compute +/- " << result.NErrors() << " sigma errors for fit value" << endl;
    return false;
  }
  if (m_resultIsError) {
    result = HftInterval(CurrentVar()->getError());
    return true;
  }
  if (result.NErrors() == 0) 
    result = HftInterval(CurrentVar()->getVal());
  else 
    result = HftInterval(*CurrentVar());
  return true;
}


bool HftFitValueCalculator::SetCurrent(HftScanningCalculator& scan, unsigned int k)
{
  for (unsigned int i = 0; i < scan.NPoints(); i++) {
    HftFitValueCalculator* fvCalc = dynamic_cast<HftFitValueCalculator*>(scan.Calculator(i));
    if (!fvCalc) return false;
    fvCalc->SetCurrent(k);
  }
  return true;
}


TString HftFitValueCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftFitValueCalculator: " + GetName();
  for (unsigned int i = 0; i < NResults(); i++) {
    s += TString("\n") + prefix + "   " + Real(i)->GetName() + TString(" : ") + Real(i)->GetTitle();
    if (options.Index("V") >= 0) s += Form(" = %f +/- %f", Real(i)->getVal(), Real(i)->getError());
  }
  return s;
}


void HftFitValueCalculator::SetVerbosity(int verbosity)
{
  if (m_opts.Find("Verbose")) m_opts.Remove("Verbose");
  if (verbosity > 0) m_opts.Add(StrArgOpt("Verbose", Form("%d", verbosity)));
}
