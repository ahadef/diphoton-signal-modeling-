#include "HfitterStats/HftMLCalculator.h"

#include "HfitterModels/HftModel.h"

#include "RooFitResult.h"
#include "TMath.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftMLCalculator::HftMLCalculator(HftModel& model, const Options& fitOptions, const HftParameterStorage& initialState, 
                                 const RooArgList& paramsToStore, const TString& name)
  : HftAbsCalculator(model, name), m_fitOptions(fitOptions), m_paramsToStore(paramsToStore), 
    m_fitResult(0), m_nll(0), m_status(0)
{
  m_initialStates.push_back(initialState);
  if (m_paramsToStore.getSize() == 0)
    m_paramsToStore.add(model.FreeParameters());
  else {
    for (int i = 0; i < m_paramsToStore.getSize(); i++) 
      if (!model.Var(m_paramsToStore.at(i)->GetName())) m_paramsToStore.remove(*m_paramsToStore.at(i));
  }
}


HftMLCalculator::HftMLCalculator(const HftMLCalculator& other, const TString& name)
  : HftAbsCalculator(other, name), m_initialStates(other.m_initialStates),
    m_fitOptions(other.FitOptions()), m_paramsToStore(other.m_paramsToStore),
    m_fitResult(0), m_nll(0), m_status(0)
{
}


HftMLCalculator::~HftMLCalculator()
{
  if (m_fitResult) delete m_fitResult;
}


bool HftMLCalculator::MakeBranches(HftTree& tree)
{
  if (!tree.AddVar(ResultName(), GetName())) return false;
  if (!tree.AddVar("status", GetName())) return false;
  for (int i = 0; i < m_paramsToStore.getSize(); i++) 
    if (!tree.AddVar(m_paramsToStore.at(i)->GetName(), GetName())) return false;
  return true;
}


bool HftMLCalculator::FillBranches(HftTree& tree)
{
  double nll;
  if (!GetResult(nll)) return false;
  if (!tree.FillVar(ResultName(), nll, GetName())) return false;
  if (!tree.FillVar("status", m_status, GetName())) return false;
  for (int i = 0; i < m_paramsToStore.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(m_paramsToStore.at(i));
    if (!tree.FillVar(var->GetName(), FitValue(*var), GetName())) return false;
  }
  return true;
}


bool HftMLCalculator::LoadInputs(HftTree& tree)
{
  if (!tree.LoadVal(ResultName(), m_nll, GetName())) return false;
  //cout << "Loaded result " << GetName() << " = " << m_nll << endl;
  for (int i = 0; i < m_paramsToStore.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(m_paramsToStore.at(i));
    if (!tree.LoadVar(*var, GetName())) return false;
    //cout << "Loaded " << var->GetName() << " = " << var->getVal() << endl;
    m_fitState.Save(var->GetName(), var->getVal(), FitError(*var), m_fitState.Const(*var));
  }
  return true;
}


bool HftMLCalculator::LoadInputs(RooAbsData& data)
{  
  HftParameterStorage savedState = Model().CurrentState();
  if (m_fitResult) { delete m_fitResult; m_fitResult = 0; }
  
  std::vector<HftParameterStorage> fitStates;
  std::vector<RooFitResult*> fitResults;
  std::vector<double> nlls;
  std::vector<int> statuses;
  unsigned int minState = 0;
  double minNll = DBL_MAX;
  for (auto istate : m_initialStates) {
    Model().LoadState(istate);
    if (Verbosity() >= 3) {
      cout << "=== HftMLCalculator(" << GetName() << ") : fit initial state (" << nlls.size() + 1 
           << " of " << m_initialStates.size() << ") is" << endl;
      Model().CurrentState().Print();
    }
    else if (Verbosity() >= 2) {
      cout << "=== HftMLCalculator(" << GetName() << ") : trying fit initial state " << nlls.size() + 1 
           << " of " << m_initialStates.size() << endl;
    }
    double nll;
    int status;
    fitResults.push_back(Model().Fit(data, m_fitOptions, &nll, &status));
    nlls.push_back(nll);
    statuses.push_back(status);
    if (nll < minNll) { minNll = nll; minState = nlls.size() - 1; }
    if (m_status < 0) {
      cout << "ERROR: error maximizing likelihood in calculator " << GetName() << endl;
    }
    if (Verbosity() >= 2) {
      cout << "=== HftMLCalculator(" << GetName() << Form(") : NLL = %.5g, status = %d, fit final state is ", nll, status) << endl;
      Model().CurrentState().Print();
    }
    fitStates.push_back(Model().CurrentState());
  }
  if (Verbosity() >= 2) cout << "=== HftMLCalculator(" << GetName() << ") : using result from initial state " << minState + 1 << endl;
  m_nll = nlls[minState];
  m_status = statuses[minState];
  m_fitState = fitStates[minState];
  m_fitResult = fitResults[minState];
  Model().LoadState(savedState);
  return true;
}


bool HftMLCalculator::LoadInputs(const HftAbsCalculator& other)
{
  const HftMLCalculator* otherML = dynamic_cast<const HftMLCalculator*>(&other);
  if (!otherML) {
    cout << "ERROR : cannot load input from calculator which doesn't derive from HftMLCalculator" << endl;
    return false;
  }
  m_nll = otherML->m_nll;
  m_status = otherML->m_status;
  for (int i = 0; i < m_paramsToStore.getSize(); i++) {
    RooRealVar* var = (RooRealVar*)m_paramsToStore.at(i);
    m_fitState.Save(var->GetName(), otherML->m_fitState.Value(*var), otherML->m_fitState.Error(*var), otherML->m_fitState.Const(*var));
  }
  return true;
}


TString HftMLCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = BaseStr(prefix, "HftMLCalculator");
  if (options.Index("V") >= 0) {
    for (int i = 0; i < m_paramsToStore.getSize(); i++)
      s += "\n" + prefix + Form("  %s = %g +/- %g %s", m_paramsToStore.at(i)->GetName(),
                                  m_fitState.Value(*(RooRealVar*)m_paramsToStore.at(i)),
                                  m_fitState.Error(*(RooRealVar*)m_paramsToStore.at(i)),
                                  m_fitState.Const(*(RooRealVar*)m_paramsToStore.at(i)) ? "(const)" : "");
  }
  return s;
}


void HftMLCalculator::SetVerbosity(int verbosity)
{
  if (m_fitOptions.Find("Verbose")) m_fitOptions.Remove("Verbose");
  if (verbosity > 0) m_fitOptions.Add(StrArgOpt("Verbose", Form("%d", verbosity)));
}
