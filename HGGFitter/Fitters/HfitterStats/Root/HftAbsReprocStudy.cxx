#include "HfitterStats/HftAbsReprocStudy.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftAbsReprocStudy::HftAbsReprocStudy(const TString& studyName, const TString& inputTreeName, 
                                     const TString& inputFileName)
   : m_studyName(studyName), m_inputTreeName(inputTreeName), m_inputFileName(inputFileName)
{
}


HftAbsReprocStudy::~HftAbsReprocStudy()
{
  for (std::vector<HftAbsCalculator*>::iterator calculator = m_inputCalculators.begin();
       calculator != m_inputCalculators.end(); calculator++)
    if (*calculator) delete *calculator;
  for (std::vector<HftAbsCalculator*>::iterator calculator = m_outputCalculators.begin();
       calculator != m_outputCalculators.end(); calculator++)
    if (*calculator) delete *calculator;
}


bool HftAbsReprocStudy::Add(HftAbsCalculator* inputCalculator, HftAbsCalculator* outputCalculator)
{
  if (!inputCalculator || !outputCalculator) return false;
  m_inputCalculators.push_back(inputCalculator);
  m_outputCalculators.push_back(outputCalculator);
  return true;
}


bool HftAbsReprocStudy::Run(const TString& output, unsigned int nEntries)
{  
  m_inputTree = HftTree::Open(m_inputTreeName, m_inputFileName);
  if (!m_inputTree) {
    cout << "ERROR : cannot open tree " << m_inputTreeName << " in file " << m_inputFileName << ", exiting." << endl;
    return false;
  }
  m_outputTree = new HftTree(StudyName(), (output != "" ? output : StudyName()) + ".root");
  cout << "Initialize..." << endl;
  if (!Initialize()) return false;

  std::vector<HftParameterStorage> inputSaveStates(NCalculators()), outputSaveStates(NCalculators());
  std::vector<HftParameterStorage> inputInitStates(NCalculators()), outputInitStates(NCalculators());
  
  for (unsigned int iCalc = 0; iCalc < NCalculators(); iCalc++) {
    InputCalculator(iCalc)->Model().SaveState(inputSaveStates[iCalc]);
    OutputCalculator(iCalc)->Model().SaveState(outputSaveStates[iCalc]);
  }
  
  for (unsigned int iCalc = 0; iCalc < NCalculators(); iCalc++) {
    if (!Initialize(*InputCalculator(iCalc))) {
      cout << "Error initializing input calculator " << InputCalculator(iCalc)->GetName() << ", stopping." << endl;
      return false; 
    }
    if (!InputCalculator(iCalc)->MakeBranches(*InputTree())) return false;
    InputCalculator(iCalc)->Model().SaveState(inputInitStates[iCalc]);
    
    if (!Initialize(*OutputCalculator(iCalc))) {
      cout << "Error initializing output calculator " << OutputCalculator(iCalc)->GetName() << ", stopping." << endl;
      return false; 
    }
    if (!OutputCalculator(iCalc)->MakeBranches(*OutputTree())) return false;
    OutputCalculator(iCalc)->Model().SaveState(outputInitStates[iCalc]);
  }

  if (nEntries == 0 || nEntries > m_inputTree->Tree()->GetEntries()) nEntries = m_inputTree->Tree()->GetEntries();

  for (unsigned int i = 0; i < nEntries; i++) {
        
    cout << endl;
    cout << "################################################################" << endl;
    cout << "####        Reprocessing toy " << i + 1 << " of " << nEntries << "              ####" << endl;
    cout << "################################################################" << endl;
    cout << endl;
        
    InputTree()->GetEntry(i);
    
    for (unsigned int iCalc = 0; iCalc < NCalculators(); iCalc++)  {    

      cout << "reprocessing study : model " << (iCalc + 1) << " of " << NCalculators() << endl;
      cout << "-------------------------------------------" << endl;
      
      HftAbsCalculator& inputCalculator = *InputCalculator(iCalc);
      inputCalculator.Model().LoadState(inputInitStates[iCalc]); // make sure we use the same state each time
      if (!inputCalculator.LoadInputs(*InputTree())) return false;
      if (!Execute(inputCalculator)) return false;
      
      HftAbsCalculator& outputCalculator = *OutputCalculator(iCalc);
      outputCalculator.Model().LoadState(outputInitStates[iCalc]); // make sure we use the same state each time
      if (!outputCalculator.LoadInputs(inputCalculator)) return false;
      if (!outputCalculator.FillBranches(*OutputTree())) return false;
    }
    m_outputTree->Fill();
  }
  cout << "Finalize..." << endl;
  
  for (unsigned int iCalc = 0; iCalc < NCalculators(); iCalc++) {
    if (!Finalize(*OutputCalculator(iCalc))) {
      cout << "Error finalizing calculator " << OutputCalculator(iCalc)->GetName() << ", stopping" << endl;
      return false;
    }
    InputCalculator(iCalc)->Model().LoadState(inputSaveStates[iCalc]);
    OutputCalculator(iCalc)->Model().LoadState(outputSaveStates[iCalc]);
  }

  if (!Finalize()) return false;
  cout << "Cleaning up..." << endl;
  delete m_inputTree;
  delete m_outputTree;
  cout << "Done!" << endl;
  return true;
}
