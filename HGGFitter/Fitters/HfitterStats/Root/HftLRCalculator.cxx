#include "HfitterStats/HftLRCalculator.h"

#include "RooAbsPdf.h"
#include "RooSimultaneous.h"
#include "RooNLLVar.h"
#include "TMath.h"
#include "TH1D.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


bool HftLRCalculator::GetResult(double& /*result*/) const
{ 
  //result = 1/(1 + TMath::Exp(m_bkgNLL - m_sigNLL));
  return true;
}


bool HftLRCalculator::LoadInputs(RooAbsData& data)
{
  m_sigNLL.clear();
  m_bkgNLL.clear();
  m_weight.clear();
  RooAbsPdf* pdf = Model().Pdf();
  RooArgSet obs = Model().Observables();
  RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(pdf);
  if (simPdf) obs.add(simPdf->indexCat());
  Model().LoadState(SigState());
  for (int i = 0; i < data.numEntries(); i++) {
    obs = *data.get(i);
    m_sigNLL.push_back(pdf->getLogVal(&obs));
    //obs.Print("V");
    //cout << data.GetName() << " " << m_sigNLL.back() << endl;
  }
  Model().LoadState(BkgState());
  for (int i = 0; i < data.numEntries(); i++) {
    obs = *data.get(i);
    m_bkgNLL.push_back(pdf->getLogVal(&obs));
    m_weight.push_back(data.weight());
  }
  delete pdf;
  return true;
}


TH1D* HftLRCalculator::LRHistogram(const TString& name, unsigned int nBins, double llrMin, double llrMax, int which)
{
  TH1D* h = new TH1D(name, "", nBins, llrMin, llrMax);
  for (unsigned int i = 0; i < m_sigNLL.size(); i++) {
    if (which == 0 && m_bkgNLL[i] == 0)
      h->Fill(-1, m_weight[i]);
    else {
      if (which == 0) 
        h->Fill(1/(1 + TMath::Exp(-m_sigNLL[i] + m_bkgNLL[i])), m_weight[i]);
      else if (which == 1)
        h->Fill(m_sigNLL[i], m_weight[i]);        
      else if (which == 2)
        h->Fill(m_bkgNLL[i], m_weight[i]);        
      else if (which == 3)
        h->Fill(m_sigNLL[i] - m_bkgNLL[i], m_weight[i]);        
      else 
        h->Fill(1, m_weight[i]);   
    }
  }
  return h;
}


bool HftLRCalculator::LoadInputs(const HftAbsCalculator& other)
{
  const HftLRCalculator* otherLRC = dynamic_cast<const HftLRCalculator*>(&other);
  if (!otherLRC) {
    cout << "ERROR: can only load from another HftLRCalculator" << endl;
    return false;
  }
  m_sigNLL = otherLRC->m_sigNLL;
  m_bkgNLL = otherLRC->m_bkgNLL;
  m_weight = otherLRC->m_weight;
  return true;
}


bool HftLRCalculator::LoadInputs(HftTree& /*tree*/)
{
  //if (!tree.GetVar("sigNLL", m_sigNLL, GetName(), true)) return false;
  //if (!tree.GetVar("bkgNLL", m_bkgNLL, GetName(), true)) return false;
  return true;
}


bool HftLRCalculator::SaveToFile(const TString& fileName, const TString& treeName)
{
  HftTree* tree = new HftTree(treeName, fileName, true, 10000);
  if (!MakeBranches(*tree)) return false;
  for (unsigned int i = 0; i < m_sigNLL.size(); i++) {
    if (!FillBranches(*tree, i)) return false;    
    tree->Fill();
  }
  delete tree;
  return true;

}


bool HftLRCalculator::MakeBranches(HftTree& tree)
{
  if (!tree.AddVar("sigNLL", GetName())) return false;
  if (!tree.AddVar("bkgNLL", GetName())) return false;
  if (!tree.AddVar("LLR", GetName())) return false;
  if (!tree.AddVar("weight", GetName())) return false;
  return true;
}


bool HftLRCalculator::FillBranches(HftTree& tree, unsigned int i)
{
  if (!tree.FillVar("sigNLL", m_sigNLL[i], GetName())) return false;
  if (!tree.FillVar("bkgNLL", m_bkgNLL[i], GetName())) return false;
  if (!tree.FillVar("LLR", 1/(1 + TMath::Exp(-m_sigNLL[i] + m_bkgNLL[i])), GetName())) return false;
  if (!tree.FillVar("weight", m_weight[i], GetName())) return false;
  return true;
}


TString HftLRCalculator::Str(const TString& prefix, const TString& /*options*/) const
{
  TString s = prefix + "HftLRCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + ")";
  return s;
}
