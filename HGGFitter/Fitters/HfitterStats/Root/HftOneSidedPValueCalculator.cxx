#include "HfitterStats/HftOneSidedPValueCalculator.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"

#include <iostream>
using std::cout;
using std::endl;


using namespace Hfitter;


bool HftOneSidedPValueCalculator::GetStatistic(double& t0) const
{
  if (!HftPLRCalculator::GetStatistic(t0)) return false;
  if (FreeFitValue(0) < NullHypoValue(0)) t0 = 0;
  return true;
}
 
 
bool HftOneSidedPValueCalculator::GetStatisticBand(int i, double& tBand) const
{ 
  if (!HftPLRCalculator::GetStatisticBand(i, tBand)) return false;
  double sigma = FreeCalc()->FitError(*HypoVar(0));
  if (FreeFitValue(0) + i*sigma < NullHypoValue(0)) tBand = 0;
  return true;
}


bool HftOneSidedPValueCalculator::ComputeFromAsymptotics(double t0, double& p0) const
{
  if (t0 < 0) t0 = 0;
  p0 = ROOT::Math::normal_cdf_c(SqrtPos(t0)); // don't respect ResultIsCL
  return true;
}


double HftOneSidedPValueCalculator::SignificanceFromPValue(double p0) const
{ 
  return ROOT::Math::normal_quantile_c(p0, 1);
}


TString HftOneSidedPValueCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftOneSidedPValueCalculator");
  s += PLRStr("  " + prefix, options);
  if (options.Index("V") >= 0)
    s += "\n" + prefix + " Z = " + Form("%g", Significance())  + "\n";
  return s;
}
