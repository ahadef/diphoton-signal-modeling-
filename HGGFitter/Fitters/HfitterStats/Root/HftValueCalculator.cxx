#include "HfitterStats/HftValueCalculator.h"

#include "HfitterStats/HftFitValueCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"
#include "RooFitResult.h"
#include "RooDataSet.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftValueCalculator::HftValueCalculator(HftModel& model, const TString& varNames, const TString& name)
  : HftAbsMultiResultCalculator(model, name), m_index(0)
{ 
  std::vector<TString> varVect; HftFitValueCalculator::ParseNames(varNames, varVect);
  if (!MakeVars(varVect)) return;
}


HftValueCalculator::HftValueCalculator(HftModel& model, const std::vector<TString>& varNames, const TString& name)
  : HftAbsMultiResultCalculator(model, name), m_index(0)
{ 
  if (!MakeVars(varNames)) return;
}


HftValueCalculator::HftValueCalculator(const HftValueCalculator& other, const TString& name)
  : HftAbsMultiResultCalculator(other, name), m_index(0)
{
  std::vector<TString> varNames;
  for (unsigned int k = 0; k < other.NResults(); k++)
    varNames.push_back(other.Var(k)->GetName());
  if (!MakeVars(varNames)) return;
}


bool HftValueCalculator::MakeVars(const std::vector<TString>& varNames)
{
  if (varNames.size() == 0) {
    cout << "ERROR : empty list of variables specified" << endl;
    return false;
  }
  for (std::vector<TString>::const_iterator varName = varNames.begin(); varName != varNames.end(); varName++) {
    RooRealVar* var = new RooRealVar(*varName, "", 0);
    m_vars.push_back(var);
  }
  return true;
}


HftValueCalculator::~HftValueCalculator()
{
  for (unsigned int k = 0; k < NResults(); k++) delete Var(k);  
}


bool HftValueCalculator::SetCurrent(const TString& name)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (Var(k)->GetName() == name) {
      SetCurrent(k);
      return true;
    }
  return false;
}


bool HftValueCalculator::LoadInputs(RooAbsData& /*data*/)
{
  for (unsigned int k = 0; k < NResults(); k++) {
    RooRealVar* rv = Model().Var(Var(k)->GetName());
    if (rv) {
      Var(k)->setVal(rv->getVal());
      Var(k)->setError(rv->getError());
      continue;
    }
    RooAbsReal* rl = Model().Real(Var(k)->GetName());
    if (rl) {
      Var(k)->setVal(rl->getVal());
      Var(k)->setError(0);
      continue;
    }
    cout << "ERROR : cannot find real value " << Var(k)->GetName() << " in tree" << endl;
    return false;
  }
  return true;
}


bool HftValueCalculator::LoadInputs(HftTree& input)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!input.LoadVar(*Var(k), GetName(), true, true)) return false;
  return true;
}


bool HftValueCalculator::LoadInputs(const HftAbsCalculator& other)
{  
  const HftValueCalculator* mvOther = dynamic_cast<const HftValueCalculator*>(&other);
  if (!mvOther) {
    cout << "ERROR : cannot copy from a calculator of type other than HftValueCalculator." << endl;
    return false;
  }
  if (mvOther->NResults() != NResults()) {
    cout << "ERROR : cannot copy from HftValueCalculator with a number of variables ("
    << mvOther->NResults() << ") different from ours " << NResults() << ")." << endl;
    return false;
  }
  for (unsigned int k = 0; k < NResults(); k++) {
    Var(k)->setVal(mvOther->Var(k)->getVal());
    Var(k)->setError(mvOther->Var(k)->getError());
  }
  return true;
}


bool HftValueCalculator::MakeBranches(HftTree& tree)
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!tree.AddVar(Var(k)->GetName(), GetName(), true, true)) return false;
  return true;
}


bool HftValueCalculator::FillBranches(HftTree& tree) 
{
  for (unsigned int k = 0; k < NResults(); k++)
    if (!tree.FillVar(*Var(k), GetName(), true)) return false;
  return true;
}


bool HftValueCalculator::GetResult(HftInterval& result) const
{
  if (!CurrentVar()) return false;
  if (result.NErrors() > 1) {
    cout << "WARNING : cannot compute +/- " << result.NErrors() << " sigma errors for fit value" << endl;
    return false;
  }
  if (result.NErrors() == 0) 
    result = HftInterval(CurrentVar()->getVal());
  else 
    result = HftInterval(*CurrentVar());
  return true;
}


bool HftValueCalculator::SetCurrent(HftScanningCalculator& scan, unsigned int k)
{
  for (unsigned int i = 0; i < scan.NPoints(); i++) {
    HftValueCalculator* fvCalc = dynamic_cast<HftValueCalculator*>(scan.Calculator(i));
    if (!fvCalc) return false;
    fvCalc->SetCurrent(k);
  }
  return true;
}


TString HftValueCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftValueCalculator: " + GetName();
  for (unsigned int i = 0; i < NResults(); i++) {
    s += TString("\n") + prefix + "   " + m_vars[i]->GetName() + TString(" : ") + m_vars[i]->GetTitle();
    if (options.Index("V") >= 0) s += Form(" = %f +/- %f", m_vars[i]->getVal(), m_vars[i]->getError());
  }
  return s;
}
