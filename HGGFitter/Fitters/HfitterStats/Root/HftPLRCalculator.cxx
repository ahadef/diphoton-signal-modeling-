#include "HfitterStats/HftPLRCalculator.h"

#include "HfitterModels/HftModel.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftToysCalculator.h"
#include "HfitterStats/HftExpHypoTestCalc.h"

#include "RooFitResult.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftPLRCalculator::HftPLRCalculator(HftModel& model, const TString& pois, const Options& fitOptions, const HftParameterStorage& expectedState, 
                                   const HftParameterStorage& initialStateFree, const HftParameterStorage& initialStateHypo,
                                   const RooArgList& paramsToStore, const TString& name, bool resultIsCL, bool freeFirst)
 : HftAbsHypoTestCalculator(model, pois, resultIsCL, name, freeFirst), m_chainFitStates(true)
{
  Init(fitOptions, initialStateFree, initialStateHypo, paramsToStore);
  m_expCalc = new HftExpHypoTestCalc(*this, expectedState.NVars() ? expectedState : DefaultExpectedState(), AppendToName("expStat"));
}


HftPLRCalculator::HftPLRCalculator(HftModel& model, const HftScanPoint& hypo, const Options& fitOptions,  
                                   const HftParameterStorage& expectedState,
                                   const HftParameterStorage& initialStateFree, const HftParameterStorage& initialStateHypo,
                                   const RooArgList& paramsToStore, const TString& name, bool resultIsCL, bool freeFirst)
 : HftAbsHypoTestCalculator(model, hypo, resultIsCL, name, freeFirst), m_chainFitStates(true)
{
  Init(fitOptions, initialStateFree, initialStateHypo, paramsToStore);
  m_expCalc = new HftExpHypoTestCalc(*this, expectedState.NVars() ? expectedState : DefaultExpectedState(), AppendToName("expStat"));
}


HftPLRCalculator::HftPLRCalculator(HftModel& model, const HftScanPoint& testHypo, const HftScanPoint& compHypo,
                                   const Options& fitOptions, const HftParameterStorage& expectedState, 
                                   const HftParameterStorage& initialStateFree, const HftParameterStorage& initialStateHypo,
                                   const RooArgList& paramsToStore, const TString& name, bool resultIsCL, bool freeFirst)
 : HftAbsHypoTestCalculator(model, testHypo, resultIsCL, name, freeFirst), m_chainFitStates(true)
{
  Init(fitOptions, initialStateFree, initialStateHypo, paramsToStore);
  SetCompHypo(compHypo);
  m_expCalc = new HftExpHypoTestCalc(*this, expectedState.NVars() ? expectedState : DefaultExpectedState(), AppendToName("expStat"));
}


HftPLRCalculator::HftPLRCalculator(const HftPLRCalculator& other, const TString& name, const HftScanPoint& hypo, const HftAbsCalculator* cloner)
  : HftAbsHypoTestCalculator(other, name, hypo), m_compHypo(other.m_compHypo), m_compFree(other.m_compFree),
    m_chainFitStates(other.m_chainFitStates)
{
  m_freeCalc = new HftMLCalculator(*other.FreeCalc(), AppendToName("freeFit"));
  m_hypoCalc = new HftMLCalculator(*other.HypoCalc(), AppendToName("hypoFit"));
  // have to do the following in case compHypo and compFree are not defined, and hypo changed
  HftParameterStorage newFree = m_freeCalc->InitialState();
  newFree.Add(ComputationFree().State(), true);
  m_freeCalc->SetInitialState(newFree); 
  HftParameterStorage newHypo = m_hypoCalc->InitialState();
  newHypo.Add(ComputationHypo().State(), true);
  m_hypoCalc->SetInitialState(newHypo);
  m_expCalc = 0;

  // If we are being used within an Asimov calculator, no separate computation of expected state: use our own instead (see below)
  const HftAsimovCalculator* asimovCalc = dynamic_cast<const HftAsimovCalculator*>(cloner);
  // here could also set up the free ML calculator to use the Asimov state instead of a fit
  if (asimovCalc) return;
  // For toys, expected statistic should be irrelevant (TBC?)
  const HftToysCalculator* toysCalc = dynamic_cast<const HftToysCalculator*>(cloner);
  if (toysCalc) return;
  
  if (other.ExpCalc()) m_expCalc = new HftExpHypoTestCalc(*this, other.ExpCalc()->ExpectedState(), AppendToName("expStat"));
}


void HftPLRCalculator::Init(const Options& fitOptions, const HftParameterStorage& initialStateFree, 
                            const HftParameterStorage& initialStateHypo, const RooArgList& paramsToStore)
{
  RooArgList pts = paramsToStore;
  if (pts.getSize() == 0) pts.add(Model().FreeParameters());
  for (unsigned int i = 0; i < NHypoVars(); i++) {
    if (pts.find(*HypoVar(i))) continue;
    pts.add(*HypoVar(i));
  }
  HftParameterStorage newFree = initialStateFree;
  newFree.Add(ComputationFree().State(), true); // make sure the initial state of the free fit contains the free condition
  HftParameterStorage newHypo = initialStateHypo;
  newHypo.Add(ComputationHypo().State(), true); // make sure the initial state of the hypo fit contains the hypo condition
  m_freeCalc = new HftMLCalculator(Model(), fitOptions, newFree, pts, AppendToName("freeFit"));
  m_hypoCalc = new HftMLCalculator(Model(), fitOptions, newHypo, pts, AppendToName("hypoFit"));
}


HftParameterStorage HftPLRCalculator::DefaultExpectedState()
{
  HftParameterStorage state = Model().CurrentState();
  for (unsigned int i = 0; i < NHypoVars(); i++) state.Save(HypoVar(i)->GetName(), 0);
  return state;
}


HftPLRCalculator::~HftPLRCalculator()
{
  delete m_freeCalc;
  delete m_hypoCalc;
  DeleteExpCalc();
}


void HftPLRCalculator::DeleteExpCalc()
{ 
  if (m_expCalc) { delete m_expCalc; m_expCalc = 0; } 
}


void HftPLRCalculator::SetCompHypo(const HftScanPoint& compHypo)
{ 
  m_compFree = HftScanPoint(compHypo.State().AllFixed(false), compHypo.Vars());
  HftParameterStorage newFreeState = m_freeCalc->InitialState();
  newFreeState.Add(m_compFree.State(), true);
  m_freeCalc->SetInitialState(newFreeState);

  m_compHypo = HftScanPoint(compHypo.State().AllFixed(true), compHypo.Vars());
  HftParameterStorage newHypoState = m_hypoCalc->InitialState();
  newHypoState.Add(m_compHypo.State(), true);
  m_hypoCalc->SetInitialState(newHypoState);

  if (m_expCalc) {
    HftPLRCalculator* plr = dynamic_cast<HftPLRCalculator*>(m_expCalc->HypoTestCalc());
    plr->m_compFree = m_compFree;
    plr->m_compHypo = m_compHypo;
    plr->m_freeCalc->SetInitialState(newFreeState);
    plr->m_hypoCalc->SetInitialState(newHypoState);
  }
}

    
bool HftPLRCalculator::GetPLR(double& plr) const
{
  double freeNLL = 0, hypoNLL = 0;
  if (!m_freeCalc->GetResult(freeNLL) || !m_hypoCalc->GetResult(hypoNLL)) return false;
  plr = 2*(hypoNLL - freeNLL);
  return true;
}


bool HftPLRCalculator::GetStatisticBand(int i, double& qBand) const
{ 
  // Recover the correct sign for sqrt(q) : q = [(mu_hypo - mu_hat)/sigma]^2 and we want the argument of the []^2
  double q0 = 0;
  if (!GetStatistic(q0)) return false;
  qBand = Sqr(SqrtAbs(q0)*(FreeFitValue(0) < CompHypoValue(0) ? 1 : -1) - i);
  return true;
}


bool HftPLRCalculator::GetResultBand(int i, double& rBand) const
{
  double qBand;
  if (!GetStatisticBand(i, qBand)) return false;
  if (ResultIsStatistic()) {
    rBand = qBand;
    return true;
  }
  if (!ComputeFromAsymptotics(qBand, rBand)) return false;
  return true;
}


bool HftPLRCalculator::GetSigma(double q, double& sig) const 
{ 
  if (q == 0) {
    cout << "ERROR in HftPLRCalculator(" << GetName() << ") :  cannot compute uncertainty from q = 0" << endl;
    sig = 0; 
    return false;
  }
  sig = TMath::Abs(CompHypoValue(0)/SqrtAbs(q));
  return true; 
}


double HftPLRCalculator::ExpValue(unsigned int i) const
{ 
  if (ExpCalc()) return ExpCalc()->ExpectedState().Value(*HypoVar(i));
  return FreeFitValue(i); // Asimov case: use free fit value, should be the same
}


bool HftPLRCalculator::ComputeFromAsymptotics(double q, double& result) const
{
  if (!HasSeparateCompHypo()) {
    double arg = SqrtAbs(q);
    result = 2*ROOT::Math::normal_cdf(-arg);
  }
  else {
    double sigma;
    if (!GetExpSigma(sigma)) {
      cout << "ERROR : cannot get expected uncertainty, cannot compute asymptotics for mu_hypo != mu'" << endl;
      result = 0;
      return false;
    }
    double argP = SqrtAbs(q) + (CompHypoValue(0) - HypoValue(0))/sigma;
    double argM = SqrtAbs(q) - (CompHypoValue(0) - HypoValue(0))/sigma;
    result = ROOT::Math::normal_cdf(-argP) + ROOT::Math::normal_cdf(-argM);
  }
  if (ResultIsCL()) result = 1 - result;
  return true;
}


bool HftPLRCalculator::MakeAltHypoBranches(HftTree& tree)
{
  if (!m_freeCalc->MakeBranches(tree)) return false;
  if (m_expCalc && !m_expCalc->MakeAltHypoBranches(tree)) return false;
  return true;
}


bool HftPLRCalculator::MakeNullHypoBranches(HftTree& tree)
{
  if (!m_hypoCalc->MakeBranches(tree)) return false;
  if (m_expCalc && !m_expCalc->MakeNullHypoBranches(tree)) return false;
  if (!tree.AddVar("dll", GetName())) return false;
  return true;
}


bool HftPLRCalculator::FillAltHypoBranches(HftTree& tree)
{
  if (!m_freeCalc->FillBranches(tree)) return false;
  if (m_expCalc && !m_expCalc->FillAltHypoBranches(tree)) return false;
  return true;
}


bool HftPLRCalculator::FillNullHypoBranches(HftTree& tree)
{ 
  if (!m_hypoCalc->FillBranches(tree)) return false;
  if (m_expCalc && !m_expCalc->FillNullHypoBranches(tree)) return false;
  double dll = 0;
  if (!GetStatistic(dll)) return false;
  if (!tree.FillVar("dll", dll, GetName())) return false;
  return true;
}


bool HftPLRCalculator::LoadAltHypoInputs(RooAbsData& data)
{
  if (m_altHypoFirst) 
    m_fitInitialState = Model().CurrentState(); // We're first : store the initial state of the PLR
  else if (m_chainFitStates) 
    Model().LoadState(NullHypoCalc()->FitState());
  Model().LoadState(ComputationFree().State());
  if (AltHypoCalc()->Verbosity() >= 1)
    cout << endl << "=== PLR Calculator(" << GetName() << ") : doing free fit" << endl;
//   if (Model().Var("dSig")) {
//     Model().Var("dSig")->setRange(1,5);
//     Model().Var("dSig")->setVal(1);
//     Model().Var("dSig")->setConstant(0);
//   }
  if (!m_freeCalc->LoadInputs(data)) return false;
  if (!m_altHypo.UpdateValues(m_freeCalc->FitState())) {
    cout << "ERROR in HftPLRCalculator : not all hypo values could be copied" << endl;
    return false;
  }
  if (m_expCalc) {
    if (AltHypoCalc()->Verbosity() >= 2)
      cout << endl << "=== PLR Calculator(" << GetName() << ") : computing altHypo part of expected statistic" << endl;
    if (!m_expCalc->LoadAltHypoInputs(data)) return false;
  }
  return true;
}


bool HftPLRCalculator::LoadNullHypoInputs(RooAbsData& data)
{
  if (!m_altHypoFirst) 
    m_fitInitialState = Model().CurrentState(); // We're first : store the initial state of the PLR
  else if (m_chainFitStates) 
    Model().LoadState(AltHypoCalc()->FitState());
  Model().LoadState(ComputationHypo().State());
  if (NullHypoCalc()->Verbosity() >= 1)
    cout << endl << "=== PLR Calculator(" << GetName() << ") : doing hypo fit" << endl;
//   if (Model().Var("dSig")) {
//     Model().Var("dSig")->setRange(-1,1);
//     Model().Var("dSig")->setVal(0);
//     Model().Var("dSig")->setConstant(1);
//   }
  if (!m_hypoCalc->LoadInputs(data)) return false;
  if (!m_nullHypo.UpdateValues(m_hypoCalc->FitState())) {
    cout << "ERROR in HftPLRCalculator : not all hypo values could be copied" << endl;
    return false;
  }
  
  if (m_expCalc) {
    if (NullHypoCalc()->Verbosity() >= 2)
      cout << endl << "=== PLR Calculator(" << GetName() << ") : computing nullHypo part of expected statistic" << endl;
    if (!m_expCalc->LoadNullHypoInputs(data)) return false;
  }
  return true;
}


bool HftPLRCalculator::LoadAltHypoInputs(HftTree& tree)
{
  if (!m_freeCalc->LoadInputs(tree)) return false;
  if (!m_altHypo.UpdateValues(m_freeCalc->FitState())) {
    cout << "ERROR in HftPLRCalculator : not all hypo values could be copied" << endl;
    return false;
  }
  if (m_expCalc && !m_expCalc->LoadAltHypoInputs(tree)) return false;
  return true;
}


bool HftPLRCalculator::LoadNullHypoInputs(HftTree& tree)
{
  if (!m_hypoCalc->LoadInputs(tree)) return false;
  if (!m_nullHypo.UpdateValues(m_hypoCalc->FitState())) {
    cout << "ERROR in HftPLRCalculator : not all hypo values could be copied" << endl;
    return false;
  }
  if (m_expCalc && !m_expCalc->LoadNullHypoInputs(tree)) return false;
  return true;
}


bool HftPLRCalculator::LoadAltHypoInputs(const HftAbsCalculator& other)
{
  const HftPLRCalculator* otherPLR = dynamic_cast<const HftPLRCalculator*>(&other);
  if (!otherPLR) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftPLRCalculator" << endl;
    return false;
  }
  if (!m_freeCalc->LoadInputs(*otherPLR->m_freeCalc)) return false;
  if (m_expCalc && !m_expCalc->LoadAltHypoInputs(*otherPLR->m_expCalc)) return false;
  return true;
}


bool HftPLRCalculator::LoadNullHypoInputs(const HftAbsCalculator& other)
{
  const HftPLRCalculator* otherPLR = dynamic_cast<const HftPLRCalculator*>(&other);
  if (!otherPLR) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftPLRCalculator" << endl;
    return false;
  }
  if (!m_hypoCalc->LoadInputs(*otherPLR->m_hypoCalc)) return false;
  if (m_expCalc && !m_expCalc->LoadNullHypoInputs(*otherPLR->m_expCalc)) return false;
  return true;
}


bool HftPLRCalculator::ComputeSignificance(double stat, double& z) const
{
  // only for asymptotic case...
  if (UseSampling()) return HftAbsHypoTestCalculator::ComputeSignificance(stat, z);
  // use shortcut
  z = SqrtPos(stat);
  return true;
}


bool HftPLRCalculator::GetExpSigma(const HftAbsHypoTestCalculator& calc, double& sigma)
{
  const HftPLRCalculator* plrCalc = dynamic_cast<const HftPLRCalculator*>(&calc);
  if (!plrCalc) return false;
  return plrCalc->GetExpSigma(sigma);
}


bool HftPLRCalculator::GetCompHypo(const HftAbsHypoTestCalculator& calc, HftScanPoint& hypo)
{
  const HftPLRCalculator* plrCalc = dynamic_cast<const HftPLRCalculator*>(&calc);
  if (!plrCalc) return false;
  hypo = plrCalc->ComputationHypo();
  return true;
}


bool HftPLRCalculator::SetCompHypo(HftAbsHypoTestCalculator& calc, const HftScanPoint& hypo)
{
  HftPLRCalculator* plrCalc = dynamic_cast<HftPLRCalculator*>(&calc);
  if (!plrCalc) return false;
  plrCalc->SetCompHypo(hypo);
  return true;
}


TString HftPLRCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftPLRCalculator");
  s += PLRStr(prefix + "  ", options);
  return s;
}


TString HftPLRCalculator::PLRStr(const TString& prefix, const TString& options) const
{
  TString s;
  if (options.Index("V") >= 0) 
    s += "\n" + prefix + "free NLL = " + Form("%.5f", FreeCalc()->Result())
    + ", hypo NLL = " + Form("%.5f", HypoCalc()->Result())
    + ", -2 log(delta_L) = " + Form("%.5f", -2*(FreeCalc()->Result() - HypoCalc()->Result()));
  if (options.Index("VV") >= 0) {
    for (unsigned int i = 0; i < NHypoVars(); i++) {
      s += "\n" + prefix + Form("Testing %s = %g (fitted value = %g, exp value = %g", HypoVar(i)->GetName(), 
                         HypoValue(i), FreeFitValue(i), ExpValue(i));
      s += HasSeparateCompHypo() ? Form(", computation using %g)", CompHypoValue(i)) : ")";
    }
    if (!UseSampling()) s += "\n" + prefix + "expected statistic " + (ExpCalc() ? Form("= %g", ExpCalc()->ExpStat()) : "not defined");
  }
  else if (options.Index("V") >= 0) {
    s += "\n" + prefix + "testing ";
    for (unsigned int i = 0; i < NHypoVars(); i++) {
      if (i > 0) s += " ,";
      s += Form("%s = %g", HypoVar(i)->GetName(), HypoValue(i));
    }
    if (!UseSampling()) s += TString(", ") + "expected statistic " + (ExpCalc() ? Form("= %g", ExpCalc()->ExpStat()) : "not defined");
  }
  if (options.Index("R") >= 0) {
    s += "\n" + FreeCalc()->Str(prefix, options);
    s += "\n" + HypoCalc()->Str(prefix, options);
    if (m_expCalc) s += "\n" + ExpCalc()->Str(prefix, options);
  }
  return s;
}  
