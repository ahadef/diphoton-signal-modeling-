#include "HfitterStats/HftQTildaLimitCalculator.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftBand.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "TLine.h"
#include "RooFitResult.h"
#include "RooNLLVar.h"
#include "RooWorkspace.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftQTildaLimitCalculator::HftQTildaLimitCalculator(HftModel& model, const TString& hypoVar, const Options& fitOptions, 
                                                   const HftParameterStorage& expectedState, const TString& name, bool freeFirst)
  : HftPLRCalculator(model, hypoVar, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                     RooArgList(), name, true, freeFirst)
{
  m_zeroCalc = new HftMLCalculator(model, fitOptions, HftParameterStorage(hypoVar, 0, -1, true), 
                                   FreeCalc()->ParamsToStore(), AppendToName("zero"));
}


HftQTildaLimitCalculator::HftQTildaLimitCalculator(HftModel& model, const HftScanPoint& hypo, const Options& fitOptions, 
                                                   const HftParameterStorage& expectedState, const TString& name, bool freeFirst)
  : HftPLRCalculator(model, hypo, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                     RooArgList(), name, true, freeFirst) 
{
  if (hypo.Vars().size() != 1) {
    cout << "ERROR : HftQTildaLimitCalculator expects a hypothesis defined by only one variable, got " << hypo.Vars().size() << endl;
    return;
  }
  TString hypoVar = hypo.Vars()[0];
  m_zeroCalc = new HftMLCalculator(model, fitOptions, HftParameterStorage(hypoVar, 0, -1, true), 
                                   FreeCalc()->ParamsToStore(), AppendToName("zero"));
}


HftQTildaLimitCalculator::HftQTildaLimitCalculator(const HftQTildaLimitCalculator& other, const TString& name, const HftScanPoint& hypo,
                                                   const HftAbsCalculator* cloner)
  : HftPLRCalculator(other, name, hypo, cloner)
{
  m_zeroCalc = new HftMLCalculator(*other.m_zeroCalc, AppendToName("zero"));
}

// backward compatibility 
HftQTildaLimitCalculator::HftQTildaLimitCalculator(HftModel& model, const HftParameterStorage& expectedState,
                                                   const TString& signalIntensityVar, const Options& fitOptions, 
                                                   const TString& name, bool /*fakeAsimovFit*/, bool /*smallTree*/)
: HftPLRCalculator(model, signalIntensityVar, fitOptions, expectedState, HftParameterStorage(), HftParameterStorage(), 
                   RooArgList(), name, true, false)
{
  m_zeroCalc = new HftMLCalculator(model, fitOptions, HftParameterStorage(signalIntensityVar, 0, -1, true), 
                                   FreeCalc()->ParamsToStore(), AppendToName("zero"));
}


bool HftQTildaLimitCalculator::MakeAltHypoBranches(HftTree& tree)
{
  if (!HftPLRCalculator::MakeAltHypoBranches(tree)) return false;
  if (!m_zeroCalc->MakeBranches(tree)) return false;
  return true;
}

  
bool HftQTildaLimitCalculator::FillAltHypoBranches(HftTree& tree)
{ 
  if (!HftPLRCalculator::FillAltHypoBranches(tree)) return false;
  if (!m_zeroCalc->FillBranches(tree)) return false;
  return true;
}


bool HftQTildaLimitCalculator::LoadAltHypoInputs(RooAbsData& data) 
{ 
  if (!HftPLRCalculator::LoadAltHypoInputs(data)) return false;
  if (ZeroCalc()->Verbosity() >= 1)
    cout << endl << "=== HftQTildaLimitCalculator(" << GetName() << ") : doing zero fit" << endl;
  if (!m_zeroCalc->LoadInputs(data)) return false;
  return true;
}


bool HftQTildaLimitCalculator::LoadAltHypoInputs(HftTree& tree)
{ 
  if (!HftPLRCalculator::LoadAltHypoInputs(tree)) return false;
  if (!m_zeroCalc->LoadInputs(tree)) return false;
  return true;
}


bool HftQTildaLimitCalculator::LoadAltHypoInputs(const HftAbsCalculator& other)
{
  if (!HftPLRCalculator::LoadAltHypoInputs(other)) return false;
  const HftQTildaLimitCalculator* otherQTilda = dynamic_cast<const HftQTildaLimitCalculator*>(&other);
  if (!otherQTilda) {
    cout << "ERROR : cannot load global inputs from non-HftQTildaLimitCalculator!" << endl;
    return false;
  }
  m_zeroCalc->LoadInputs(*otherQTilda->m_zeroCalc);
  return true;
}



bool HftQTildaLimitCalculator::GetStatistic(double& qTilda) const
{
  if (CompHypoValue(0) == 0) { // In this case, qTilda = 0 always. Set it to 0 explicitely to avoid FP issues.
    qTilda = 0;
    cout << "WARNING in HftQTildaLimitCalculator(" + GetName() + ") : hypo value used in computation is 0, returning q_tilda = 0" << endl;
    return true; 
  }
  if (FreeFitValue(0) < 0) {
    double zeroNll, hypoNll;
    if (!m_zeroCalc->GetResult(zeroNll) || !m_hypoCalc->GetResult(hypoNll)) return false;
    qTilda = -2*(zeroNll - hypoNll);
  }
  else if (FreeFitValue(0) <= CompHypoValue(0)) {
    double freeNll, hypoNll;
    if (!m_freeCalc->GetResult(freeNll) || !m_hypoCalc->GetResult(hypoNll)) return false;
    qTilda = -2*(freeNll - hypoNll);
  }
  else
    qTilda = 0;
  return true;
}


bool HftQTildaLimitCalculator::GetStatisticBand(int i, double& qBand) const
{ 
  double sigma = 0;
  if (!HftPLRCalculator::GetExpSigma(sigma)) return false;

  if (FreeFitValue(0) + i*sigma < 0) {
    if (FreeFitValue(0) < 0) { // the nominal case is in same region : just modify existing q (in PLRCalc)
      if (!HftPLRCalculator::GetStatistic(qBand)) return false;
    }
    else { // nominal case was different: use asymptotic formula from scratch
      qBand = Sqr(CompHypoValue(0)/sigma) - 2*CompHypoValue(0)*FreeFitValue(0)/Sqr(sigma);
    }
    // in both cases, apply the difference : linear in this case
    qBand -= 2*i*CompHypoValue(0)/sigma; 
  }
  else if (FreeFitValue(0) + i*sigma <= CompHypoValue(0)) {
    if (FreeFitValue(0) > 0 && FreeFitValue(0) <= CompHypoValue(0)) { // same region for nominal case : use diff from nominal (in PLRCalc)
      if (!HftPLRCalculator::GetStatisticBand(i, qBand)) return false;
    }
    else { // nominal case was another case : use asymptotic formula from scratch
      qBand = Sqr((CompHypoValue(0) - FreeFitValue(0) - i*sigma)/sigma);
    }
  }
  else
    qBand = 0;
  return true;
}


bool HftQTildaLimitCalculator::ComputeFromAsymptotics(double qTilda, double& cl) const
{
//  cout << "njpb: computing from asymptotics! " << GetName() << " " << this << endl;
  if (CompHypoValue(0) == 0) { // In this case, qTilda = 0 always.
    //cout << "njpb: null comp value, returning 1/0" << endl;
    cl = (ResultIsCL() ? 0 : 1);
    return true; 
  }

  double sigma;
  if (!GetExpSigma(sigma)) {
    cl = (ResultIsCL() ? 0 : 1);
    cout << "WARNING: HftQTildaLimitCalculator::ComputeFromAsymptotics cannot get expected uncertainty for " << GetName() << ", returning " << cl << endl;
    return true;
  }
  
  double lambda = SignedSqr(CompHypoValue(0)/sigma);
  //cout << "Using lambda = " << lambda << (m_useHardcodedLambdas ? "  (hardcoded)" : "") << endl;
  
  double arg = 0;
    
  // The simple case
  if (!HasSeparateCompHypo()) {
    if (qTilda <= lambda) {
      arg = SignedSqrt(qTilda);
      //cout << "njpb: simple low : " << arg << endl;
    }
    else {
      arg = (qTilda + lambda)/(2*SignedSqrt(lambda));
      //cout << "njpb: simple high : " << arg << " " << qTilda << " " << lambda << endl;
    }
  }
  else {
    if (qTilda <= lambda) {
      double l2 = (CompHypoValue(0) - HypoValue(0))/sigma;
      arg = SignedSqrt(qTilda) - l2;
      //cout << "njpb: complex low : " << arg << " " << SignedSqrt(qTilda) << " " << l2 << " " << sigma << endl;
    }
    else {
      double lambdaNC = SignedSqr(HypoValue(0)/sigma);
      arg = (qTilda - lambda + 2*SignedSqrt(lambda*lambdaNC))/(2*SignedSqrt(lambda));
      //cout << "njpb: complex high : " << arg << " " << SignedSqrt(qTilda) << " " << (CompHypoValue(0) - HypoValue(0))/sigma << " " << sigma << endl;
    }
  }
  
  cl = ResultIsCL() ? 
//   ROOT::Math::chisquared_cdf(arg*arg, NHypoVars())/2 + 0.5 :
//   ROOT::Math::chisquared_cdf_c(arg*arg, NHypoVars())/2;
//   njpb: comment for now: these asymptotics apply only to the 1D case
  ROOT::Math::normal_cdf(arg) :
  ROOT::Math::normal_cdf(-arg);
  return true;
}


TString HftQTildaLimitCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftQTildaLimitCalculator");
  if (options.Index("V") >= 0) {
    s += "\n" + prefix + "  zero NLL = " + Form("%.5f", ZeroCalc()->Result())
      + TString(", -2 log(delta_L_zero) = ") + Form("%.5f", -2*(ZeroCalc()->Result() - HypoCalc()->Result()));
  }
  s += PLRStr(prefix + "  ", options);
  if (options.Index("R") >= 0)
    s += "\n" + ZeroCalc()->Str(prefix + "  ", options);
  return s;
}
