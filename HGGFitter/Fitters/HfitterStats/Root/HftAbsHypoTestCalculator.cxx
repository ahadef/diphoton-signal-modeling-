#include "HfitterStats/HftAbsHypoTestCalculator.h"

#include "HfitterStats/HftTree.h"
#include "RooRealVar.h"

#include "TLine.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftAbsHypoTestCalculator::HftAbsHypoTestCalculator(HftModel& model, const HftScanPoint& nullHypo, const HftScanPoint& altHypo, 
                                                   bool resultIsCL, const TString& name, bool altHypoFirst)
  : HftAbsStatCalculator(model, name, resultIsCL), m_nullHypo(nullHypo), m_altHypo(altHypo), m_altHypoFirst(altHypoFirst)
{
  if (NullHypo().Vars().size() != AltHypo().Vars().size()) {
    cout << "ERROR: null and alternate hypotheses must be defined using the same variables" << endl;
    cout << "ERROR: here null uses " << NullHypo().Vars().size() << " variables while alt uses " << AltHypo().Vars().size() << endl;
    return;
  }
  
  std::vector<TString>::const_iterator altName = AltHypo().Vars().begin();
  for (std::vector<TString>::const_iterator name = NullHypo().Vars().begin(); name != NullHypo().Vars().end(); name++, altName++) {
    if (*name != *altName) {
      cout << "ERROR: null and alternate hypotheses must be defined using the same variables" << endl;
      cout << "ERROR: here " << *name << " (null) != " << *altName << " (alt)" << endl;
      return;
    }
  }
  InitHypoVars();
}


HftAbsHypoTestCalculator::HftAbsHypoTestCalculator(HftModel& model, const HftScanPoint& hypo, bool resultIsCL, const TString& name,
                                                   bool altHypoFirst)
  : HftAbsStatCalculator(model, name, resultIsCL), m_altHypoFirst(altHypoFirst)
{
  m_nullHypo = HftScanPoint(hypo.State().AllFixed(true),  hypo.Vars());
  m_altHypo  = HftScanPoint(hypo.State().AllFixed(false), hypo.Vars());
  InitHypoVars();
}


HftAbsHypoTestCalculator::HftAbsHypoTestCalculator(HftModel& model, const TString& hypoVars, bool resultIsCL, const TString& name,
                                                   bool altHypoFirst)
  : HftAbsStatCalculator(model, name, resultIsCL), m_altHypoFirst(altHypoFirst)
{
  TObjArray* names = hypoVars.Tokenize(":");
  for (int i = 0; i < names->GetEntries(); i++) {
    RooRealVar* var = model.Var(names->At(i)->GetName());
    if (!var)  {
      cout << "ERROR : hypo var '" << names->At(i)->GetName() << "' not found in model!" << endl;
      return;
    }
    m_hypoVars.push_back(var);
  }
  delete names;
  HftScanPoint hypo = HftScanPoint(m_hypoVars);
  m_nullHypo = HftScanPoint(hypo.State().AllFixed(true),  hypo.Vars());
  m_altHypo  = HftScanPoint(hypo.State().AllFixed(false), hypo.Vars());
}


HftAbsHypoTestCalculator::HftAbsHypoTestCalculator(const HftAbsHypoTestCalculator& other, const TString& name, const HftScanPoint& hypo)
  : HftAbsStatCalculator(other, name), m_nullHypo(hypo.Vars().size() ? hypo : other.NullHypo()), 
    m_altHypo(other.m_altHypo), m_altHypoFirst(other.m_altHypoFirst)
{ 
  InitHypoVars(); 
}


void HftAbsHypoTestCalculator::InitHypoVars()
{
  for (std::vector<TString>::const_iterator name = NullHypo().Vars().begin(); name != NullHypo().Vars().end(); name++) {
    RooRealVar* var = Model().Var(*name);
    if (!var)  {
      cout << "ERROR : hypothesis variable " << *name << " not found in model!" << endl;
      return;
    }
    m_hypoVars.push_back(var);
  }
}


bool HftAbsHypoTestCalculator::GetResult(HftInterval& result) const
{
  for (int i = -result.NErrors(); i <= (int)result.NErrors(); i++) {
    double rBand = 0;
    if (i == 0 && !GetResult(rBand)) return false;
    if (i != 0 && !GetResultBand(i, rBand)) return false;
    result.SetValue(i, rBand);
  }
  // return an interval in the "right" direction, with positive +1 sigma error.
  if (result.NBands() > 0 && result.Error(1) < 0) result = result.Flip();
  return true;
}


bool HftAbsHypoTestCalculator::MakeBranches(HftTree& tree)
{
  if (!tree.AddVars(NullHypo().State(), AppendToName("nullHypo"), false, false)) return false;
  if (!MakeNullHypoBranches(tree)) return false;
  if (!tree.AddVars(AltHypo().State(), AppendToName("altHypo"), false, false)) return false;
  if (!MakeAltHypoBranches(tree)) return false;
  if (!tree.AddVar(StatisticName(), GetName())) return false;
  return true;
}

  
bool HftAbsHypoTestCalculator::FillBranches(HftTree& tree)
{ 
  if (!tree.FillVars(NullHypo().State(), AppendToName("nullHypo"), false, false)) return false;
  if (!FillNullHypoBranches(tree)) return false;
  if (!tree.FillVars(AltHypo().State(), AppendToName("altHypo"), false, false)) return false;
  if (!FillAltHypoBranches(tree)) return false;
  if (!tree.FillVar(StatisticName(), Statistic(), GetName())) return false;
  return true;
}


bool HftAbsHypoTestCalculator::LoadInputs(RooAbsData& data)
{
  if (m_altHypoFirst) {
    if (!LoadAltHypoInputs(data))return false;
    if (!LoadNullHypoInputs(data)) return false;
  }
  else {
    if (!LoadNullHypoInputs(data)) return false;
    if (!LoadAltHypoInputs(data)) return false; 
  }
  return true;
}


bool HftAbsHypoTestCalculator::LoadInputs(const HftAbsCalculator& other)
{ 
  if (!LoadNullHypoInputs(other)) return false;
  if (!LoadAltHypoInputs(other)) return false;
  return true;
}


bool HftAbsHypoTestCalculator::LoadInputs(const HftAbsHypoTestCalculator& other, RooAbsData& data)
{
  if (m_altHypoFirst) {
    if (!LoadAltHypoInputs(data))return false;
    if (!LoadNullHypoInputs(other)) return false;
  }
  else {
    if (!LoadNullHypoInputs(other)) return false;
    if (!LoadAltHypoInputs(data)) return false; 
  }
  return true;
}


bool HftAbsHypoTestCalculator::LoadInputs(HftTree& tree)
{
  if (!tree.LoadVars(m_nullHypo.State(), AppendToName("nullHypo"), false, false)) return false;
  if (!LoadNullHypoInputs(tree)) return false;
  if (!tree.LoadVars(m_altHypo.State(), AppendToName("altHypo"), false, false)) return false;
  if (!LoadAltHypoInputs(tree)) return false;
 return true;
}


bool HftAbsHypoTestCalculator::DrawHypos(const TString& hypoPar, double xMin, double xMax) const
{
  TLine l;
  double hypo = Hypo().State().RealValue(hypoPar);
  l.DrawLine(xMin, hypo, xMax, hypo);
  return true;
}


bool HftAbsHypoTestCalculator::GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, 
                                                    const TString& fileName, const TString& treeName, bool binned,
                                                    unsigned int saveInterval) const
{
  HftParameterStorage hypoState = state;
  hypoState.Add(Hypo().State(), true);
  return HftAbsStatCalculator::GenerateSamplingData(hypoState, nToys, fileName, treeName, binned, saveInterval);
}
