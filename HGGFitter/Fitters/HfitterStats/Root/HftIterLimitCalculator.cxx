#include "HfitterStats/HftIterLimitCalculator.h"

#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanningCalculator.h"

#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"
#include "HfitterStats/HftToysCalculator.h"

#include "RooRealVar.h"

#include "TFile.h"
#include "TTree.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftIterLimitCalculator::HftIterLimitCalculator(const HftAbsHypoTestCalculator& calculator, unsigned int nBands, 
                                               double target, double tolerance, const TString& name)
  : HftAbsCalculator(calculator, name), m_target(target), m_tolerance(tolerance), 
    m_nIterMax(20), m_nCompIterMax(100), m_resultIsCL(true), m_nToys(0), m_toysSaveInterval(20), m_binnedToys(false)
{
  for (int i = -(int)nBands; i <= (int)nBands; i++) 
    m_calculators.push_back((HftAbsHypoTestCalculator*)calculator.Clone(CalculatorName(i)));
}


HftIterLimitCalculator::HftIterLimitCalculator(const HftIterLimitCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsCalculator(other, name), m_target(other.Target()), m_tolerance(other.Tolerance()), 
  m_nIterMax(other.m_nIterMax), m_nCompIterMax(other.m_nCompIterMax), m_resultIsCL(other.m_resultIsCL),
  m_toysState(other.m_toysState), m_nToys(other.m_nToys), m_toysSaveInterval(other.m_toysSaveInterval), m_binnedToys(other.m_binnedToys)
{
  for (int i = -other.NBands(); i <= other.NBands(); i++) 
    m_calculators.push_back((HftAbsHypoTestCalculator*)other.Calculator(i)->Clone(CalculatorName(i), cloner));
}


HftIterLimitCalculator::~HftIterLimitCalculator()
{
  for (int i = 0; i < 2*NBands() + 1; i++) delete m_calculators[i];
}


bool HftIterLimitCalculator::GetResult(HftInterval& hypo) const
{
  if (hypo.NBands() > NBands()) {
    cout << "WARNING : " << hypo.NBands() << " bands requested, when only " << NBands() 
    << " were computed. Only setting the ones we have." << endl;
  }
  for (int i = -NBands(); i <= NBands(); i++) hypo.SetValue(i, Calculator(i)->Hypo().State().Value(*Var()));
  hypo = hypo.Flip(); // as usual, when solving CL->pos we get the interval backwards
  return true;
}


bool HftIterLimitCalculator::GetResult(double& hypo) const
{
  hypo = Calculator(NBands())->Hypo().State().Value(*Var());
  return true;
}


bool HftIterLimitCalculator::LoadInputs(RooAbsData& data)
{
  if (m_target >= 1) {
    cout << "ERROR : cannot set limit for confidence level " << m_target << " which is >1 !" << endl;
    return false;
  }
  if (!Calculator(0)->LoadInputs(data)) return false;
  for (int i = -NBands(); i <= NBands(); i++) if (i != 0 && !Calculator(i)->LoadInputs(*Calculator(0))) return false;

  for (int i = 0; abs(i) <= NBands(); i += (i == 0 ? 1 : i > 0 ? -2*i : -2*i + 1)) {
    //cout << "*** Computing " << i << endl;
    Calculator(i)->SetResultIsCL(ResultIsCL());
    if (i > 0) {
      int iprev = (abs(i) - 1)*(i > 0 ? 1 : -1);
      if (!HftPLRCalculator::SetCompHypo(*Calculator(i), Calculator(iprev)->Hypo()) && 
          !HftCLsCalculator::SetCompHypo(*Calculator(i), Calculator(iprev)->Hypo())) return false;
      Calculator(i)->LoadInputs(*Calculator(iprev));
    }
    if (Calculator(i)->Verbosity() > 1) 
      cout << "INFO : solving for CL = " << Target() << " at interval position " << i 
            << ", starting hypothesis = " << Calculator(i)->Hypo().State().Value(*Var()) << endl;
    if (!SolveForHypo(i, data)) return false;
    if (Calculator(i)->Verbosity() > 2) 
      cout << "INFO : found CL = " << Calculator(i)->Result() << " at interval position " << i 
      << " for hypo = " << Calculator(i)->Hypo().State().Value(*Var()) << endl;
  }
  return true;
}


bool HftIterLimitCalculator::SolveForHypo(unsigned int i, RooAbsData& data)
{
  unsigned int nIter = 0;
  double currentResult = 0;
  if (m_nToys > 0 && !ProduceToys(i)) return false;
  if (!Calculator(i)->GetResult(i, currentResult)) return false;
  while (nIter < m_nIterMax) {
    // Shift the tested hypo until the right CL is reached
    if (!SolveForTestHypo(i)) return false;
    if (Calculator(i)->Verbosity() >= 1) {
      cout << "=== Iterative limit Calculator(" << GetName() << ") : will recompute with hypo = ";
      Calculator(i)->Hypo().Print();
    }
    // Now shift the computation hypo to match the tested one
    if (!HftPLRCalculator::SetCompHypo(*Calculator(i), Calculator(i)->Hypo()) && 
        !HftCLsCalculator::SetCompHypo(*Calculator(i), Calculator(i)->Hypo())) return false;
    //cout << "njpb : hypo OK, reloading data " << nIter << endl;
    if (!Calculator(i)->LoadInputs(data)) return false;
    if (m_nToys > 0 && !ProduceToys(i)) return false;
    if (!Calculator(i)->GetResult(i, currentResult)) return false;
    nIter++;
    if (TMath::Abs(currentResult - m_target) < m_tolerance) {
      if (Calculator(i)->Verbosity() >= 1) 
        cout << "=== Iterative limit Calculator(" << GetName() << ") : after " << nIter 
        << " iterations, computed solution CL = " << currentResult 
        << " (target = " << Target() << ", tolerance = " << Tolerance() << "), success!" << endl;
      return true;
    }
    else if (Calculator(i)->Verbosity() >= 1) 
      cout << "=== Iterative limit Calculator(" << GetName() << ") : after " << nIter 
      << " iterations, obtained CL = " << currentResult 
        << " (target = " << Target() << ", tolerance = " << Tolerance() << "), continuing iterations" << endl;

  }
  cout << "ERROR: limit solving did not converge after " << m_nIterMax << " iterations." << endl;
  return false;
}


bool HftIterLimitCalculator::SolveForTestHypo(unsigned int i)
{
  unsigned int nIter = 0;
  double currentResult = 0, toysResult0 = 0, asymptResult0 = 0;
  if (!Calculator(i)->GetResult(i, toysResult0)) return false;
  if (Calculator(i)->UseSampling()) {
    Calculator(i)->SetIgnoreSamplingData();
    if (!Calculator(i)->GetResult(i, asymptResult0)) return false;
    //cout << "SolveForTestHypo:: using sampling " << Calculator(i)->UseSampling() << " " << Calculator(i)->IgnoreSamplingData() << " " << toysResult0 << " " << asymptResult0 << endl;
  }
  else 
    asymptResult0 = toysResult0;
  currentResult = toysResult0;
  while (nIter < m_nCompIterMax) {
    if (!ShiftTestHypo(i, currentResult)) return false;
    Calculator(i)->SetIgnoreSamplingData();
    if (!Calculator(i)->GetResult(i, currentResult)) return false;
    //cout << "Shifting result due to sampling, from " << currentResult << " to " << currentResult + (toysResult0 - asymptResult0) << endl;
    currentResult += (toysResult0 - asymptResult0);
    nIter++;
    if (TMath::Abs(currentResult - m_target) < m_tolerance) {
      if (Calculator(i)->Verbosity() > 0) 
        cout << "=== Iterative limit Calculator(" << GetName() << ") : after " << nIter << " shift(s), computed solution CL = " << currentResult 
        << " (target = " << Target() << ", tolerance = " << Tolerance() << ")" << endl;
      return true;
    }
  }
  cout << "ERROR: limit-solving did not converge after " << m_nCompIterMax << " shifts." << endl;
  return false;
}


bool HftIterLimitCalculator::ShiftTestHypo(unsigned int i, double currentResult)
{
  double sigma;
  if (!HftPLRCalculator::GetExpSigma(*Calculator(i), sigma) && !HftCLsCalculator::GetExpSigma(*Calculator(i), sigma)) return false;
  double hypoVal = Calculator(i)->Hypo().State().Value(*Var());
  double newHypoVal = NextHypo(hypoVal, sigma, currentResult, m_target);
  if (Calculator(i)->Verbosity() > 1) 
    cout << "=== Iterative limit Calculator(" << GetName() << ") : current CL = " 
         << currentResult << ", target = " << m_target << " : move hypothesis from " 
         << hypoVal << " to " << newHypoVal << " (sigma = " << sigma << ")" << endl;
  HftScanPoint newHypo = Calculator(i)->Hypo();
  newHypo.UpdateValues(HftParameterStorage(Var()->GetName(), newHypoVal));
  HftAbsHypoTestCalculator* nextCalc = (HftAbsHypoTestCalculator*)Calculator(i)->CloneHypo(newHypo, CalculatorName(i));
  HftScanPoint compHypo;
  if (!HftPLRCalculator::GetCompHypo(*Calculator(i), compHypo) && !HftCLsCalculator::GetCompHypo(*Calculator(i), compHypo)) return false;
  if (!HftPLRCalculator::SetCompHypo(*nextCalc,      compHypo) && !HftCLsCalculator::SetCompHypo(*nextCalc,      compHypo)) return false;
  if (!nextCalc->LoadInputs(*Calculator(i))) return false;
  ReplaceCalculator(i, nextCalc);
  return true;  
}


double HftIterLimitCalculator::NextHypo(double hypo, double sigma, double current, double target) const
{
  if (current >= 1) {
    current = 0.99; // in case the current result is completely off
    cout << "WARNING: computed CL = 1 at hypothesis " << hypo << ", calculation seems invalid" << endl;
  }
//   current = (1 + current)/2;
//   target = (1 + target)/2;
  // Add option to compute with CLs-based formula instead, for when this doesn't converge... Maybe an option "OptimizeForCLs" which would decide which version to start first, and in case of problem we could anyway fall back in the other one
  return hypo + sigma*(ROOT::Math::normal_quantile(target, 1) - ROOT::Math::normal_quantile(current, 1));
}


bool HftIterLimitCalculator::ProduceToys(unsigned int i)
{
  TString fileName = Form("toys_%s_%g.root", Calculator(i)->GetName().Data(), Calculator(i)->Hypo().State().Value(*Var()));
  HftCLsCalculator* cls = dynamic_cast<HftCLsCalculator*>(Calculator(i));
  if (cls) {
    if (!cls->GenerateSamplingData(m_toysState, m_nToys, fileName, "tree", m_binnedToys, m_toysSaveInterval)) return false;
    if (!cls->SetSamplingData(fileName, "tree")) return false;
    return true;
  }
  
  HftParameterStorage state = m_toysState;
  if (state.NVars() == 0) {
    HftPLRCalculator* plr = dynamic_cast<HftPLRCalculator*>(Calculator(i));
    if (plr) 
      state = plr->HypoCalc()->FitState();
    else {
      cout << "ERROR: HftIterLimitCalculator cannot produce toys, no state specified nor found." << endl;
      return false;
    }
  }
  cout << "Generating with:" << endl;
  state.Print();
  if (!Calculator(i)->GenerateSamplingData(state, m_nToys, fileName)) return false;
  if (!Calculator(i)->SetSamplingData(fileName)) return false;
  return true;
}


bool HftIterLimitCalculator::LoadInputs(HftTree& tree)
{
  for (int i = -NBands(); i <= NBands(); i++) 
    if (!Calculator(i)->LoadInputs(tree)) return false;
  return true;
}


bool HftIterLimitCalculator::LoadInputs(const HftAbsCalculator& other)
{
  for (int i = -NBands(); i <= NBands(); i++) 
    if (!Calculator(i)->LoadInputs(other)) return false;
  return false;
}

 
bool HftIterLimitCalculator::MakeBranches(HftTree& tree)
{
  for (int i = -NBands(); i <= NBands(); i++) 
    if (!Calculator(i)->MakeBranches(tree)) return false;
  return true;
}


bool HftIterLimitCalculator::FillBranches(HftTree& tree)
{
  for (int i = -NBands(); i <= NBands(); i++) 
    if (!Calculator(i)->FillBranches(tree)) return false;
  return true;
}


TString HftIterLimitCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftIterLimitCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + " = " + Form("%g", Result()) + ")";
  if (options.Index("R") >= 0) {
    for (int i = -NBands(); i <= NBands(); i++) 
      s += "\n" + Calculator(i)->Str(prefix + "   ", options);
  }
  return s;
}


void HftIterLimitCalculator::SetVerbosity(int verbosity)
{
  for (int i = -NBands(); i <= NBands(); i++) Calculator(i)->SetVerbosity(verbosity);
}
