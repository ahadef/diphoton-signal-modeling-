#include "HfitterStats/HftIterIntervalCalculator.h"

#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanningCalculator.h"

#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"

#include "RooRealVar.h"

#include "TFile.h"
#include "TTree.h"

#include "TF1.h"
#include "TCanvas.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftIterIntervalCalculator::HftIterIntervalCalculator(const HftAbsHypoTestCalculator& calculator, double z, double tolerance, 
                                                     const TString& name)
  : HftAbsCalculator(calculator, name), m_z(z), m_tolerance(tolerance), m_nIterMax(10)
{
  m_plusCalc = (HftAbsHypoTestCalculator*)calculator.Clone(CalculatorName(+1));
  m_minusCalc = (HftAbsHypoTestCalculator*)calculator.Clone(CalculatorName(-1));
}


HftIterIntervalCalculator::HftIterIntervalCalculator(HftModel& model, const TString& varName, const Options& fitOptions,
                                                     double z, double tolerance, const TString& name)
  : HftAbsCalculator(model, name), m_z(z), m_tolerance(tolerance), m_nIterMax(10)
{
  HftPLRCalculator* calculator = new HftPLRCalculator(model, varName, fitOptions);
  m_plusCalc = (HftAbsHypoTestCalculator*)calculator->Clone(CalculatorName(+1));
  m_minusCalc = (HftAbsHypoTestCalculator*)calculator->Clone(CalculatorName(-1));
  delete calculator;
}


HftIterIntervalCalculator::HftIterIntervalCalculator(const HftIterIntervalCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsCalculator(other, name), m_z(other.IntervalZ()), m_tolerance(other.Tolerance()), 
  m_nIterMax(other.m_nIterMax)
{
  m_plusCalc = (HftAbsHypoTestCalculator*)other.PlusCalculator()->Clone(CalculatorName(+1), cloner);
  m_minusCalc = (HftAbsHypoTestCalculator*)other.MinusCalculator()->Clone(CalculatorName(-1), cloner);
}


HftIterIntervalCalculator::~HftIterIntervalCalculator()
{
  delete m_plusCalc;
  delete m_minusCalc;
}


bool HftIterIntervalCalculator::GetResult(HftInterval& hypo) const
{
  if (hypo.NBands() > 1) {
    cout << "WARNING : " << hypo.NBands() << " bands requested, when only 1 is computed. Only setting the first one." << endl;
  }
  hypo.SetValue( 0,  PlusCalculator()->AltHypoValue(0));
  hypo.SetValue(+1,  PlusCalculator()->NullHypoValue(0));
  hypo.SetValue(-1, MinusCalculator()->NullHypoValue(0));
  return true;
}


bool HftIterIntervalCalculator::GetResult(double& hypo) const
{
  hypo = PlusCalculator()->AltHypoValue(0);
  return true;
}


bool HftIterIntervalCalculator::LoadInputs(RooAbsData& data)
{
  PlusCalculator()->SetResultIsCL(true);
  MinusCalculator()->SetResultIsCL(true);

  // Just to find best-fit value
  double sigma;
  if (!PlusCalculator()->LoadInputs(data)) return false;
  if (!HftPLRCalculator::GetExpSigma(*PlusCalculator(), sigma)) return false;

  if (PlusCalculator()->Verbosity() > 0)
    cout << "INFO : found best-fit value at " << PlusCalculator()->AltHypoValue(0) << ", shifting hypotheses to " 
    << PlusCalculator()->AltHypoValue(0) + sigma << " and " << PlusCalculator()->AltHypoValue(0) - sigma << endl;
  
  HftAbsHypoTestCalculator* newPlusCalc = NewHypoCalc(*m_plusCalc, PlusCalculator()->AltHypoValue(0) + sigma, false);
  HftAbsHypoTestCalculator* newMinusCalc = NewHypoCalc(*m_minusCalc, PlusCalculator()->AltHypoValue(0) - sigma, false);
  newPlusCalc->SetResultIsSignificance();
  newMinusCalc->SetResultIsSignificance();
  
  if (!newPlusCalc->LoadInputs(data)) return false;
  if (!newMinusCalc->LoadInputs(data)) return false;

  ReplaceCalculator(m_minusCalc, newMinusCalc);
  ReplaceCalculator(m_plusCalc, newPlusCalc);

  if (PlusCalculator()->Verbosity() > 1)
    cout << "INFO : solving for Z = " << IntervalZ() << " on plus side" << endl;
  if (!SolveForHypo(+1, data)) return false;
  if (PlusCalculator()->Verbosity() > 0)
    cout << "INFO : found Z = " << PlusCalculator()->Result() << " on positive side for hypo = " << PlusCalculator()->NullHypoValue(0) << endl;

  if (MinusCalculator()->Verbosity() > 1)
    cout << "INFO : solving for Z = " << IntervalZ() << " on minus side" << endl;
  if (!SolveForHypo(-1, data)) return false;
  if (MinusCalculator()->Verbosity() > 0)
    cout << "INFO : found Z = " << MinusCalculator()->Result() << " on negative side for hypo = " << MinusCalculator()->NullHypoValue(0) << endl;
  return true;
}


bool HftIterIntervalCalculator::SolveForHypo(int direction, RooAbsData& data)
{
  unsigned int nIter = 0;
  while (nIter < m_nIterMax) {
    double newHypoVal = 0;
    if (!FindNewTestHypo(direction, newHypoVal)) return false;
    HftAbsHypoTestCalculator* nextCalc = NewHypoCalc(direction > 0 ? *PlusCalculator() : *MinusCalculator(), newHypoVal, false);
    if (!nextCalc->LoadInputs(data)) return false;
    if (direction > 0) ReplaceCalculator(m_plusCalc, nextCalc); else ReplaceCalculator(m_minusCalc, nextCalc);
    double currentResult = 0;
    if (!nextCalc->GetResult(currentResult)) return false;
    nIter++;
    if (TMath::Abs(currentResult - m_z) < m_tolerance) {
      if (nextCalc->Verbosity() > 1)
        cout << "INFO : after " << nIter << " computation iterations, computed solution Z = " << currentResult 
        << " (z = " << IntervalZ() << ", tolerance = " << Tolerance() << ")" << endl;
      return true;
    }
    else {
      if (nextCalc->Verbosity() > 1)
        cout << "INFO : after " << nIter << " computation iterations, we have so far Z = " << currentResult 
        << " (z = " << IntervalZ() << ", tolerance = " << Tolerance() << ")" << endl;
    }
  }
  cout << "WARNING: solving did not converge after " << m_nIterMax << " iteration." << endl;
  return false;
}


bool HftIterIntervalCalculator::FindNewTestHypo(int direction, double& newHypoVal)
{
  HftAbsHypoTestCalculator& calc = direction > 0 ? *PlusCalculator() : *MinusCalculator();
  double statistic = 0, sigma = 0;
  if (!calc.GetStatistic(statistic)) return false;
  if (!HftPLRCalculator::GetExpSigma(calc, sigma)) return false;
  double hypoVal = calc.NullHypoValue(0);
  newHypoVal = NextHypo(hypoVal, sigma, statistic, m_z, direction);
  if (calc.Verbosity() > 1) 
    cout << "INFO : for statistic = " << statistic << ", Z = " << calc.Result() << ", z = " << m_z<< " : move hypothesis from " 
    << hypoVal << " to " << newHypoVal << " (sigma = " << sigma << ")" << endl;
  return true;  
}


HftAbsHypoTestCalculator* HftIterIntervalCalculator::NewHypoCalc(const HftAbsHypoTestCalculator& calc, double newHypoVal, bool keepCompHypo) const
{
  HftScanPoint newHypo = calc.Hypo();
  newHypo.UpdateValues(HftParameterStorage(Var()->GetName(), newHypoVal));
  HftAbsHypoTestCalculator* nextCalc = (HftAbsHypoTestCalculator*)calc.CloneHypo(newHypo);
  if (!keepCompHypo) return nextCalc;
  HftScanPoint compHypo;
  if (!HftPLRCalculator::GetCompHypo(calc, compHypo)) return 0;
  if (!HftPLRCalculator::SetCompHypo(*nextCalc, compHypo)) return 0;
  if (!nextCalc->LoadInputs(calc)) return 0;
  return nextCalc;
}


double HftIterIntervalCalculator::NextHypo(double hypo, double sigma, double statistic, double z, int direction) const
{
  // use sqrt(q)_z = Z_z = (mu_z - mu_hat)/sigma, and sqrt(q_current) = (mu_current - muhat)/sigma
  // so mu_z = mu_current + (Z_z - sqrt(q_current))*sigma
  // this is all for + side. For - side, same with -Z_z
  if (PlusCalculator()->Verbosity() > 1) 
    cout << "INFO: hypo, stat,z,dir = " << hypo << "," << statistic << "," << z << " -("
    << (direction > 0 ? "+" : "-") << ")-> " << hypo + direction*(z - TMath::Sqrt(TMath::Abs(statistic)))*sigma << endl;
  return hypo + direction*(z - TMath::Sqrt(TMath::Abs(statistic)))*sigma;  
}


double HftIterIntervalCalculator::AsympFunc(double* x, double* p) const
{ 
  HftAbsHypoTestCalculator* nextCalc = NewHypoCalc(*PlusCalculator(), x[0], true);
  if (!nextCalc->LoadInputs(*PlusCalculator())) return false;
  double result = nextCalc->AsymptoticResult(p[0]);
  delete nextCalc;
  return result;
}



bool HftIterIntervalCalculator::LoadInputs(HftTree& tree)
{
  if (!PlusCalculator()->LoadInputs(tree)) return false;
  if (!MinusCalculator()->LoadInputs(tree)) return false;
  return true;
}


bool HftIterIntervalCalculator::LoadInputs(const HftAbsCalculator& other)
{
  if (!PlusCalculator()->LoadInputs(other)) return false;
  if (!MinusCalculator()->LoadInputs(other)) return false;
  return false;
}

 
bool HftIterIntervalCalculator::MakeBranches(HftTree& tree)
{
  if (!PlusCalculator()->MakeBranches(tree)) return false;
  if (!MinusCalculator()->MakeBranches(tree)) return false;
  return true;
}


bool HftIterIntervalCalculator::FillBranches(HftTree& tree)
{
  if (!PlusCalculator()->FillBranches(tree)) return false;
  if (!MinusCalculator()->FillBranches(tree)) return false;
  return true;
}


TString HftIterIntervalCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftIterIntervalCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + " = " + Form("%g", Result()) + ")";
  if (options.Index("R") >= 0) {
    s += "\n" + PlusCalculator()->Str(prefix + "   ", options);
    s += "\n" + MinusCalculator()->Str(prefix + "   ", options);
  }
  return s;
}
