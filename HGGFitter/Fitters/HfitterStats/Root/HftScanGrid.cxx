#include "HfitterStats/HftScanGrid.h"
#include "TH2D.h"
#include "TMarker.h"

#include <iostream>
using std::cout;
using std::endl;


using namespace Hfitter;

unsigned int HftScanGrid::NPoints(const TString& var) const
{
  std::map<TString, unsigned int>::const_iterator entry = m_varPos.find(var);
  if (entry == m_varPos.end()) return 0;
  return m_nPoints[entry->second];
}


void HftScanGrid::Add(const RooRealVar& var, double val)
{
  std::map<TString, unsigned int>::iterator varPos = m_varPos.find(var.GetName());
  if (varPos != m_varPos.end()) {
    unsigned int nPoints = m_nPoints[varPos->second];
    for (unsigned int i = 0; i < NPoints(); i++) {
      const HftScanPoint* point = Point(i);
      if (point->Indices()[varPos->second] != nPoints - 1) continue;
      HftScanPoint newPoint(*point);
      newPoint.m_indices[varPos->second] = nPoints;
      newPoint.m_state.Save(var.GetName(), val, var.isConstant(), -1);
      m_points.push_back(newPoint);
    }
    m_nPoints[varPos->second]++;
    m_pos[varPos->second].push_back(val);
  }
  else {
    m_varPos[var.GetName()] = m_nPoints.size();
    m_nPoints.push_back(1);
    std::vector<double> pos;
    pos.push_back(val);
    m_pos.push_back(pos);
    if (NPoints() == 0) m_points.push_back(HftScanPoint()); // if starting from scratch, add single point with no dimensions, to which we add the new one
    for (unsigned int i = 0; i < NPoints(); i++) {      
      HftScanPoint& point = m_points[i];
      point.m_indices.push_back(0);
      point.m_vars.push_back(var.GetName());
      point.m_state.Save(var.GetName(), val, var.isConstant(), -1);
    }
  }
}


void HftScanGrid::Add(const RooRealVar& var, unsigned int nPoints, double minVal, double maxVal)
{
  if (nPoints == 0) return;
  if (nPoints == 1) { Add(var, minVal); return; }
  for (unsigned int i = 0; i < nPoints; i++) 
    Add(var, minVal + (maxVal - minVal)/(nPoints - 1)*i);
}


void HftScanGrid::Add(const RooRealVar& var, const std::vector<double>& vals)
{
  for (std::vector<double>::const_iterator val = vals.begin(); val != vals.end(); val++)
    Add(var, *val);
}


const HftScanPoint* HftScanGrid::Point(unsigned int i) const
{
  if (i >= NPoints()) return 0;
  return &m_points[i];
}


TString HftScanGrid::Variable(unsigned int i) const
{
  for (std::map<TString, unsigned int>::const_iterator var = m_varPos.begin();
       var != m_varPos.end(); var++) {
    if (var->second == i) return var->first;
  }
  return "";
}


TH1D* HftScanGrid::Empty1DHistogram(const TString& name) const
{
  if (NVariables() != 1) {
    cout << "ERROR : Empty1DHistogram() only valid for 1 variables in the grid" << endl;
    return 0;
  }

  std::vector<double> edges1;
  if (!MakeBinEdges(m_pos[0], edges1)) return 0;
  TH1D* h1 = new TH1D(name, "", m_pos[0].size(), &edges1[0]);
  h1->GetXaxis()->SetTitle(Variable(0));
  return h1;
}


TH2D* HftScanGrid::Empty2DHistogram(const TString& name) const
{
  if (NVariables() != 2) {
    cout << "ERROR : Empty2DHistogram() only valid for 2 variables in the grid" << endl;
    return 0;
  }

  std::vector<double> edges1, edges2;
  if (!MakeBinEdges(m_pos[0], edges1)) return 0;
  if (!MakeBinEdges(m_pos[1], edges2)) return 0;
  TH2D* h2 = new TH2D(name, "", m_pos[0].size(), &edges1[0], m_pos[1].size(), &edges2[0]);
  h2->GetXaxis()->SetTitle(Variable(0));
  h2->GetYaxis()->SetTitle(Variable(1));
  return h2;
}


bool HftScanGrid::MakeBinEdges(const std::vector<double>& binCenters, std::vector<double>& binEdges)
{
  unsigned int minIndex = 0;
  double minDelta = -1;
  for (unsigned int i = 1; i < binCenters.size(); i++) {
    double delta = binCenters[i] - binCenters[i - 1];
    if (delta < 0) {
      cout << "ERROR : cannot make bin edges, centers are not sorted!" << endl;
      return false;
    }
    if (delta < minDelta || minDelta < 0) {
      minIndex = i;
      minDelta = delta;
    }
  }
  binEdges = std::vector<double>(binCenters.size() + 1);
  binEdges[minIndex] = (binCenters[minIndex] + binCenters[minIndex - 1])/2;
  for (unsigned int i = minIndex + 1; i <= binCenters.size(); i++) {
    binEdges[i] = binEdges[i - 1] + 2*(binCenters[i - 1] - binEdges[i - 1]);
    if (i < binCenters.size() && binEdges[i] >= binCenters[i]) {
      cout << "ERROR : bin size inconsistency, aborting bin edge computation" << endl;
      return false;
    }
  }
  for (unsigned int i = minIndex; i > 0; i--) {
    binEdges[i - 1] = binEdges[i] + 2*(binCenters[i - 1] - binEdges[i]);  
    if (i > 1 && binEdges[i - 1] <= binCenters[i - 2]) {
      cout << "ERROR : bin size inconsistency, aborting bin edge computation" << endl;
      return false;
    }
  }
  return true;
}


void HftScanGrid::DrawMarkers(int style, int color, double size) const
{
  if (NVariables() != 2) {
    cout << "ERROR : DrawMarkers() only valid for 2 variables in the grid" << endl;
    return;
  }
  TMarker marker;
  marker.SetMarkerStyle(style);
  marker.SetMarkerColor(color);
  marker.SetMarkerSize(size);
  for (unsigned int i = 0; i < NPoints(); i++) marker.DrawMarker(m_pos[0][Point(i)->Indices()[0]], m_pos[1][Point(i)->Indices()[1]]); 
}

