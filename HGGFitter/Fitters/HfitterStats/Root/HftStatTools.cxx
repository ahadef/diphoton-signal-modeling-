#include "HfitterStats/HftStatTools.h"

#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftData.h"
#include "RooDataSet.h"
#include "RooWorkspace.h"

#include "HfitterStats/HftAbsHypoTestCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"

#include "HfitterStats/HftIterIntervalCalculator.h"

#include "HfitterStats/HftQLimitCalculator.h"
#include "HfitterStats/HftQTildaLimitCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"
#include "HfitterStats/HftIterLimitCalculator.h"

#include "HfitterStats/HftPLRPValueCalculator.h"
#include "HfitterStats/HftOneSidedPValueCalculator.h"
#include "HfitterStats/HftUncappedPValueCalculator.h"

#include "TH1D.h"
#include "TH2D.h"
#include "TGraphAsymmErrors.h"
#include "TString.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


void HftStatTools::ComputeInterval(HftModel& model, const TString& variable, RooAbsData& data, const TString& outputFile, double sigmas)
{
  HftIterIntervalCalculator intervalCalc(model, variable, "", sigmas);
  intervalCalc.LoadInputs(data);
  intervalCalc.SaveToFile(outputFile); // HACK: should save useful data instead...
}


void HftStatTools::ComputeInterval(const TString& datacard, const TString& variable, const TString& fileName, const TString& outputFile, double sigmas,
                              const TString& treeName)
{
  HftModel* model = HftModelBuilder::Create("interval", datacard);
  HftData* dataSet = HftData::Open(fileName, treeName, *model); // also try histograms ?
  ComputeInterval(*model, variable, *dataSet, outputFile, sigmas);
  delete dataSet;
}


void HftStatTools::ComputeInterval(RooWorkspace& workspace, const TString& variable, const TString& dataName, const TString& outputFile, double sigmas)
{
  HftModel* model = HftModelBuilder::CreateFromWS(workspace);
  RooAbsData* data = workspace.data(dataName);
  ComputeInterval(*model, variable, *data, outputFile, sigmas);
}


void HftStatTools::ComputeUpperLimit(HftModel& model, const TString& variable, RooAbsData& data, const TString& outputFile, 
                                ULType type, double cl)
{ 
  HftAbsHypoTestCalculator* baseCalc = LimitCalc(model, variable, type);
  HftIterLimitCalculator limitCalc(*baseCalc, 0, cl);
  delete baseCalc;
  limitCalc.LoadInputs(data);
  limitCalc.SaveToFile(outputFile); // HACK: should save useful data instead...
}


void HftStatTools::ComputeUpperLimit(const TString& datacard, const TString& variable, const TString& fileName, const TString& outputFile, 
                                ULType type, double cl, const TString& treeName)
{
  HftModel* model = HftModelBuilder::Create("limit", datacard);
  HftData* dataSet = HftData::Open(fileName, treeName, *model); // also try histograms ?
  ComputeUpperLimit(*model, variable, *dataSet, outputFile, type, cl);
  delete dataSet;
}


void HftStatTools::ComputeUpperLimit(RooWorkspace& workspace, const TString& variable, const TString& dataName, const TString& outputFile, 
                                ULType type, double cl)
{
  HftModel* model = HftModelBuilder::CreateFromWS(workspace);
  RooAbsData* data = workspace.data(dataName);
  ComputeUpperLimit(*model, variable, *data, outputFile, type, cl);
}


void HftStatTools::ComputePValue(HftModel& model, const TString& variable, RooAbsData& data, const TString& outputFile, 
                            PValueType type)
{
  HftAbsHypoTestCalculator* pValueCalc = PValueCalc(model, variable, type);
  pValueCalc->LoadInputs(data);
  pValueCalc->SaveToFile(outputFile); // HACK: should save useful data instead...
  delete pValueCalc;
}


void HftStatTools::ComputePValue(const TString& datacard, const TString& variable, const TString& fileName, const TString& outputFile, 
                            PValueType type, const TString& treeName)
{
  HftModel* model = HftModelBuilder::Create("pvalue", datacard);
  HftData* dataSet = HftData::Open(fileName, treeName, *model); // also try histograms ?
  ComputePValue(*model, variable, *dataSet, outputFile, type);
  delete dataSet;
}


void HftStatTools::ComputePValue(RooWorkspace& workspace, const TString& variable, const TString& dataName, const TString& outputFile, 
                            PValueType type)
{
  HftModel* model = HftModelBuilder::CreateFromWS(workspace);
  RooAbsData* data = workspace.data(dataName);
  ComputePValue(*model, variable, *data, outputFile, type);
}


void HftStatTools::ComputeInterval(HftModel& model, const TString& variable, const std::vector<double>& points, RooAbsData& data, 
                              const TString& outputFile, double sigmas)
{
  HftScanningCalculator intervalCalc(HftIterIntervalCalculator(model, variable, "", sigmas), variable, points);
  intervalCalc.LoadInputs(data);
  intervalCalc.SaveToFile(outputFile); // HACK: should save useful data instead...
}


void HftStatTools::ComputeInterval(const TString& datacard, const TString& variable, const std::vector<double>& points, const TString& fileName,
                              const TString& outputFile, double sigmas, const TString& treeName)
{
  HftModel* model = HftModelBuilder::Create("interval", datacard);
  HftData* dataSet = HftData::Open(fileName, treeName, *model); // also try histograms ?
  ComputeInterval(*model, variable, points, *dataSet, outputFile, sigmas);
  delete dataSet;
}


void HftStatTools::ComputeInterval(RooWorkspace& workspace, const TString& variable, const std::vector<double>& points, const TString& dataName, 
                              const TString& outputFile, double sigmas)
{
  HftModel* model = HftModelBuilder::CreateFromWS(workspace);
  RooAbsData* data = workspace.data(dataName);
  ComputeInterval(*model, variable, points, *data, outputFile, sigmas);
}


void HftStatTools::ComputeUpperLimit(HftModel& model, const TString& variable, const std::vector<double>& points, RooAbsData& data, 
                                const TString& outputFile, ULType type, double cl)
{
  HftAbsHypoTestCalculator* baseCalc = LimitCalc(model, variable, type);
  HftScanningCalculator limitCalc(HftIterLimitCalculator(*baseCalc, 0, cl), variable, points);
  delete baseCalc;
  limitCalc.LoadInputs(data);
  limitCalc.SaveToFile(outputFile); // HACK: should save useful data instead...
}


void HftStatTools::ComputeUpperLimit(const TString& datacard, const TString& variable, const std::vector<double>& points, const TString& fileName, 
                                const TString& outputFile, ULType type, double cl, const TString& treeName)
{
  HftModel* model = HftModelBuilder::Create("limit", datacard);
  HftData* dataSet = HftData::Open(fileName, treeName, *model); // also try histograms ?
  ComputeUpperLimit(*model, variable, points, *dataSet, outputFile, type, cl);
  delete dataSet;
}


void HftStatTools::ComputeUpperLimit(RooWorkspace& workspace, const TString& variable, const std::vector<double>& points, const TString& dataName,
                                const TString& outputFile, ULType type, double cl)
{
  HftModel* model = HftModelBuilder::CreateFromWS(workspace);
  RooAbsData* data = workspace.data(dataName);
  ComputeUpperLimit(*model, variable, points, *data, outputFile, type, cl);
}


void HftStatTools::ComputePValue(HftModel& model, const TString& variable, const std::vector<double>& points, RooAbsData& data, 
                            const TString& outputFile, PValueType type)
{
  HftAbsHypoTestCalculator* baseCalc = PValueCalc(model, variable, type);
  HftScanningCalculator pValueCalc(*baseCalc, variable, points);
  delete baseCalc;
  pValueCalc.LoadInputs(data);
  pValueCalc.SaveToFile(outputFile); // HACK: should save useful data instead...
}


void HftStatTools::ComputePValue(const TString& datacard, const TString& variable, const std::vector<double>& points, const TString& fileName, 
                            const TString& outputFile, PValueType type, const TString& treeName)
{
  HftModel* model = HftModelBuilder::Create("pvalue", datacard);
  HftData* dataSet = HftData::Open(fileName, treeName, *model); // also try histograms ?
  ComputePValue(*model, variable, points, *dataSet->DataSet(), outputFile, type);
  delete dataSet;
}


void HftStatTools::ComputePValue(RooWorkspace& workspace, const TString& variable, const std::vector<double>& points, const TString& dataName,
                            const TString& outputFile, PValueType type)
{
  HftModel* model = HftModelBuilder::CreateFromWS(workspace);
  RooAbsData* data = workspace.data(dataName);
  ComputePValue(*model, variable, points, *data, outputFile, type);
}


HftAbsHypoTestCalculator* HftStatTools::LimitCalc(HftModel& model, const TString& variable, ULType type)
{
  switch (type) {
    case QLimit :         return new HftQLimitCalculator(model, variable);
    case QTildaLimit :    return new HftQTildaLimitCalculator(model, variable);
    case CLsQLimit :      return new HftCLsCalculator(HftQLimitCalculator(model, variable));
    case CLsQTildaLimit : return new HftCLsCalculator(HftQTildaLimitCalculator(model, variable));
  }
  return 0;
}


HftAbsHypoTestCalculator* HftStatTools::PValueCalc(HftModel& model, const TString& variable, PValueType type)
{
  switch (type) {
    case TwoSidedPValue : return new HftPLRPValueCalculator(model, variable);
    case OneSidedPValue : return new HftOneSidedPValueCalculator(model, variable);
    case UncappedPValue : return new HftUncappedPValueCalculator(model, variable);
  }
  return 0;
}
