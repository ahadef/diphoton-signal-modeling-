#include "HfitterStats/HftCLsCalculator.h"

#include "Math/ProbFuncMathCore.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterStats/HftTree.h"

#include "HfitterStats/HftCLsbQCalculator.h"
#include "HfitterStats/HftCLsbQTildaCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftToysCalculator.h"

#include "HfitterStats/HftMultiHypoTestCalculator.h"
#include "HfitterStats/HftScanLimitCalculator.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftCLsCalculator::HftCLsCalculator(const HftPLRCalculator& clSBCalculator, const TString& name)
  : HftAbsHypoTestCalculator(clSBCalculator, name, clSBCalculator.Hypo())
{ 
  HftParameterStorage zeroState = Hypo().State();
  zeroState.Save(Hypo().Vars()[0], 0, 0, true);
  HftScanPoint zeroHypo(Hypo(), zeroState);
  m_clSBCalculator = (HftPLRCalculator*)clSBCalculator.CloneHypo(Hypo(), HftAbsParameters::Merge(GetName(), "CLsb"));
  m_clBCalculator  = (HftPLRCalculator*)clSBCalculator.CloneHypo(zeroHypo, HftAbsParameters::Merge(GetName(), "CLb"));
  m_clBCalculator->SetCompHypo(Hypo());
}


HftCLsCalculator::HftCLsCalculator(const HftCLsCalculator& other, const TString& name, const HftScanPoint& hypo, const HftAbsCalculator* cloner)
  : HftAbsHypoTestCalculator(other, name, hypo),
    m_clSBCalculator((HftPLRCalculator*)other.CLsbCalculator()->CloneHypo(hypo, HftAbsParameters::Merge(GetName(), "CLsb"), cloner)),
    m_clBCalculator((HftPLRCalculator*)other.CLbCalculator()->CloneHypo(other.CLbCalculator()->Hypo(), HftAbsParameters::Merge(GetName(), "CLb"), cloner))

{
  m_clBCalculator->SetCompHypo(hypo);
}


void HftCLsCalculator::SetName(const TString& name)
{
  HftAbsCalculator::SetName(name);
  m_clSBCalculator->SetName(HftAbsParameters::Merge(GetName(), "CLsb"));
  m_clBCalculator->SetName(HftAbsParameters::Merge(GetName(), "CLb"));  
}


bool HftCLsCalculator::GetStatistic(double& q) const
{ 
  return m_clSBCalculator->GetStatistic(q); 
}


bool HftCLsCalculator::ComputeFromAsymptotics(double q, double& cl) const
{  
  // We want to compute 1 - (1 - clSB)/(1 - clB) [CL] or (1 - clSB)/(1 - clB) [p0], in both cases need p=1-CL for CLs+b and CLb
  m_clSBCalculator->SetResultIsCL(false);
  m_clBCalculator->SetResultIsCL(false);
  
  double pSB, pB;
  if (!m_clBCalculator->ComputeFromAsymptotics(q, pB) ||
      !m_clSBCalculator->ComputeFromAsymptotics(q, pSB)) return false;
  if (pB == 0) {
    cout << "ERROR : CLb = 1 for muHypo = " << m_clBCalculator->HypoValue(0) << ", returning -1" << endl;
    cl = -1;
    return true;
  }
  double pS = pSB/pB;
  cl = ResultIsCL() ? 1 - pS : pS;
  //cout << "CLs = " << clSB << " / " <<  clB << " = " << cl << endl;
  return true;
}


bool HftCLsCalculator::ComputeFromSampling(double q, double& cl) const
{
  // We want to compute 1 - (1 - clSB)/(1 - clB) [CL] or (1 - clSB)/(1 - clB) [p0], in both cases need p=1-CL for CLs+b and CLb
  m_clSBCalculator->SetResultIsCL(false);
  m_clBCalculator->SetResultIsCL(false);

  double pSB, pB;
  if (!m_clBCalculator->ComputeFromSampling(q, pB) ||
      !m_clSBCalculator->ComputeFromSampling(q, pSB)) return false;
  if (pB == 0) {
    cout << "ERROR : Sampling CLb = 1 for muHypo = " << m_clBCalculator->HypoValue(0) << ", returning -1" << endl;
    cl = -1;
    return true;
  }
  double pS = pSB/pB;
  cl = ResultIsCL() ? 1 - pS : pS;
  return true;
}


bool HftCLsCalculator::MakeAltHypoBranches(HftTree& tree)
{
  return m_clSBCalculator->MakeAltHypoBranches(tree) && m_clBCalculator->MakeAltHypoBranches(tree);
}


bool HftCLsCalculator::MakeNullHypoBranches(HftTree& tree)
{
  if (!tree.AddVar("clSB", GetName())) return false;
  if (!tree.AddVar("clB", GetName())) return false;
  if (!tree.AddVar("cl", GetName())) return false;
  return m_clSBCalculator->MakeNullHypoBranches(tree) && m_clBCalculator->MakeNullHypoBranches(tree);
}


bool HftCLsCalculator::FillAltHypoBranches(HftTree& tree)
{
  return m_clSBCalculator->FillAltHypoBranches(tree) && m_clBCalculator->FillAltHypoBranches(tree);
}


bool HftCLsCalculator::FillNullHypoBranches(HftTree& tree)
{ 
  double cl; 
  if (!m_clSBCalculator->GetResult(cl)) return false;
  if (!tree.FillVar("clSB", cl, GetName())) return false;
  if (!m_clBCalculator->GetResult(cl)) return false;
  if (!tree.FillVar("clB", cl, GetName())) return false;
  if (!GetResult(cl)) return false;
  if (!tree.FillVar("cl", cl, GetName())) return false;
  return m_clSBCalculator->FillNullHypoBranches(tree) && m_clBCalculator->FillNullHypoBranches(tree);
}


bool HftCLsCalculator::LoadAltHypoInputs(RooAbsData& data)
{
  return m_clSBCalculator->LoadAltHypoInputs(data) && 
         m_clBCalculator->LoadAltHypoInputs(*m_clSBCalculator);
}


bool HftCLsCalculator::LoadNullHypoInputs(RooAbsData& data)
{
  return m_clSBCalculator->LoadNullHypoInputs(data) && 
         m_clBCalculator->LoadNullHypoInputs(*m_clSBCalculator);
}


bool HftCLsCalculator::LoadAltHypoInputs(HftTree& tree)
{
  return m_clSBCalculator->LoadAltHypoInputs(tree) && m_clBCalculator->LoadAltHypoInputs(*m_clSBCalculator);
}


bool HftCLsCalculator::LoadNullHypoInputs(HftTree& tree)
{
  return m_clSBCalculator->LoadNullHypoInputs(tree) && m_clBCalculator->LoadNullHypoInputs(*m_clSBCalculator);
}


bool HftCLsCalculator::LoadAltHypoInputs(const HftAbsCalculator& other)
{
  //cout << "njpb : HftCLsCalculator::LoadAltHypoInputs" << endl;
  const HftCLsCalculator* otherCLs = dynamic_cast<const HftCLsCalculator*>(&other);
  if (otherCLs) return m_clSBCalculator->LoadAltHypoInputs(*otherCLs->CLsbCalculator()) && 
    m_clBCalculator->LoadAltHypoInputs(*otherCLs->CLbCalculator());

  cout << "ERROR : cannot load from a calculator that does not derive from HftCLsCalculator" << endl;
  return false;
}


bool HftCLsCalculator::LoadNullHypoInputs(const HftAbsCalculator& other)
{
  //cout << "njpb : HftCLsCalculator::LoadHypoInputs" << endl;
  const HftCLsCalculator* otherCLs = dynamic_cast<const HftCLsCalculator*>(&other);
  if (otherCLs) return m_clSBCalculator->LoadNullHypoInputs(*otherCLs->CLsbCalculator()) && 
    m_clBCalculator->LoadNullHypoInputs(*otherCLs->CLbCalculator());

  cout << "ERROR : cannot load from a calculator that does not derive from HftCLsCalculator" << endl;
  return false;
}


bool HftCLsCalculator::GetResultBand(int i, double& rBand) const
{
  if (ResultIsCL()) i = -i;
  m_clSBCalculator->SetResultIsCL(false);
  m_clBCalculator->SetResultIsCL(false);
  // The idea: we have (Freq. Limit. recommendations, 1007.1727: mu_{+/-N} = sigma(mu_{+/-N})*(Phi^-1(1-alpha*Phi(+/-N)) +/- N) for the +/- edge of the N sigma band
  // In our notation 1-alpha -> CL. We want to solve for mu_{+/-N} but can't do it easily since sigma depends on it, So instead we compute CL's at fixed
  // mu's and solve by scanning over mu and intersecting the CL bands with CL=0.95. So we have CL= Phi((mu -/+ N)/sigma).
  double rBandSB = 0, rBandB = 0;
  if (!m_clSBCalculator->GetResultBand(i, rBandSB) || !m_clBCalculator->GetResultBand(i, rBandB)) {
    rBand = 0;
    return false;
  }
  rBand = rBandSB/rBandB;
  if (ResultIsCL()) rBand = 1 - rBand;
  return true;
}


TString HftCLsCalculator::CLsbFileName(const TString& fileName) 
{
  TString fileRoot = fileName;
  if (fileRoot.Length() >= 5 && fileRoot(fileRoot.Length() - 5, 5) == ".root") fileRoot = fileRoot(0, fileRoot.Length() - 5);
  return fileRoot + "_CLsb.root";
}


TString HftCLsCalculator::CLbFileName(const TString& fileName) 
{
  TString fileRoot = fileName;
  if (fileRoot.Length() >= 5 && fileRoot(fileRoot.Length() - 5, 5) == ".root") fileRoot = fileRoot(0, fileRoot.Length() - 5);
  return fileRoot + "_CLb.root";
}


bool HftCLsCalculator::GenerateSamplingData(const HftParameterStorage& state, unsigned int nToys, const TString& fileName, 
                                            const TString& treeName, bool binned, unsigned int saveInterval) const
{
  HftParameterStorage clsbState = state;
  clsbState.Add(CLsbCalculator()->NullHypo().State(), true);
  if (Verbosity() > 1) {
    cout << "INFO : Generating CLsb toys with state:" << endl;
    clsbState.Print();
  }
  if (!CLsbCalculator()->GenerateSamplingData(clsbState, nToys, CLsbFileName(fileName), treeName, binned, saveInterval)) return false;
  HftParameterStorage clbState = state;
  clbState.Add(CLbCalculator()->NullHypo().State(), true);
  if (Verbosity() > 1) {
    cout << "Generating CLb toys with:" << endl;
    clbState.Print();
  }
  if (!CLbCalculator()->GenerateSamplingData(clbState,nToys, CLbFileName(fileName), treeName, binned, saveInterval)) return false;
  return true;
}


bool HftCLsCalculator::SetSamplingData(const TString& fileName, const TString& treeName)
{
  if (!SetCLsbSamplingData(CLsbFileName(fileName), treeName, CLsbCalculator()->GetName())) return false;
  if (!SetCLbSamplingData ( CLbFileName(fileName), treeName,  CLbCalculator()->GetName())) return false;
  return true;
}


bool HftCLsCalculator::SetSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree)
{
  if (!SetCLsbSamplingData(CLsbFileName(fileName), treeName, HftAbsParameters::Merge(nameInTree, "CLsb"))) return false;
  if (!SetCLbSamplingData(CLbFileName (fileName), treeName, HftAbsParameters::Merge(nameInTree, "CLb"))) return false;
  return true;
}


bool HftCLsCalculator::SetCLsbSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree)
{
  if (Verbosity() > 0)
    cout << "INFO : Loading CLs+b sampling data from file " << fileName << ", tree " << treeName << ", nameInTree " << nameInTree << endl;
  return m_clSBCalculator->SetSamplingData(fileName, treeName, nameInTree);
}


bool HftCLsCalculator::SetCLbSamplingData(const TString& fileName, const TString& treeName, const TString& nameInTree)
{
  if (Verbosity() > 0)
    cout << "INFO : Loading CLb sampling data from file " << fileName << ", tree " << treeName << ", nameInTree " << nameInTree << endl;
  return m_clBCalculator->SetSamplingData(fileName, treeName, nameInTree);
}


bool HftCLsCalculator::SetCLbSamplingData(HftMultiHypoTestCalculator& multiHypo, const TString& fileName, 
                                          const TString& treeName, const TString& nameInTree)
{ 
  for (unsigned int i = 0; i < multiHypo.NPoints(); i++) {
    HftCLsCalculator* cls = dynamic_cast<HftCLsCalculator*>(multiHypo.HypoCalculator(i));
    if (!cls) {
      cout << "ERROR : cannot set CLb sampling data at point " << i << ", calculator is not CLs" << endl;
      return false;
    }
    cout << "Setting CLb sampling data for point " << i << " from file " << fileName << ", tree '" << treeName << "'." << endl;
    if (!cls->SetCLbSamplingData(fileName, treeName, nameInTree)) return false;
  }
  return true;
}


bool HftCLsCalculator::SetCLsbSamplingData(HftMultiHypoTestCalculator& multiHypo, const TString& fileName, 
                                           const TString& treeName, const TString& nameInTree)
{ 
  for (unsigned int i = 0; i < multiHypo.NPoints(); i++) {
    HftCLsCalculator* cls = dynamic_cast<HftCLsCalculator*>(multiHypo.HypoCalculator(i));
    if (!cls) {
      cout << "ERROR : cannot set CLs+b sampling data at point " << i << ", calculator is not CLs" << endl;
      return false;
    }
    cout << "Setting CLs+b sampling data for point " << i << " from file " << fileName << ", tree '" << treeName << "'." << endl;
    if (!cls->SetCLsbSamplingData(fileName, treeName, nameInTree)) return false;
  }
  return true;
}


bool HftCLsCalculator::SetCLbSamplingData(HftScanLimitCalculator& solver, const TString& fileName, 
                                          const TString& treeName, const TString& nameInTree)
{
  return SetCLbSamplingData(*solver.Calculator(), fileName, treeName, nameInTree);
}


bool HftCLsCalculator::SetCLsbSamplingData(HftScanLimitCalculator& solver, const TString& fileName, 
                                           const TString& treeName, const TString& nameInTree)
{
  return SetCLsbSamplingData(*solver.Calculator(), fileName, treeName, nameInTree);
}


TString HftCLsCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = BaseStr(prefix, "HftCLsCalculator");
  if (options.Index("V") >= 0) {
    s += "\n" + prefix + "  " + Form("psb = %g", m_clSBCalculator->Result());
    s += "\n" + prefix + "  " + Form("pb  = %g", m_clBCalculator->Result());
  }
  if (options.Index("R") >= 0) {
    s += "\n" + m_clSBCalculator->Str(prefix + "  CLsb : ", options);
    s += "\n" + m_clBCalculator->Str(prefix + "  CLb  : ", options);
  }
  return s;
}


void HftCLsCalculator::SetCompHypo(const HftScanPoint& compHypo)
{
   m_clSBCalculator->SetCompHypo(compHypo);
   m_clBCalculator->SetCompHypo(compHypo);
}


bool HftCLsCalculator::GetExpSigma(const HftAbsHypoTestCalculator& calc, double& sigma)
{
  const HftCLsCalculator* clsCalc = dynamic_cast<const HftCLsCalculator*>(&calc);
  if (!clsCalc) return false;
  return clsCalc->CLsbCalculator()->GetExpSigma(sigma);
}


bool HftCLsCalculator::GetCompHypo(const HftAbsHypoTestCalculator& calc, HftScanPoint& hypo)
{
  const HftCLsCalculator* clsCalc = dynamic_cast<const HftCLsCalculator*>(&calc);
  if (!clsCalc) return false;
  hypo = clsCalc->CLsbCalculator()->ComputationHypo();
  return true;
}


bool HftCLsCalculator::SetCompHypo(HftAbsHypoTestCalculator& calc, const HftScanPoint& hypo)
{
  HftCLsCalculator* clsCalc = dynamic_cast<HftCLsCalculator*>(&calc);
  if (!clsCalc) return false;
  clsCalc->SetCompHypo(hypo);
  return true;
}
