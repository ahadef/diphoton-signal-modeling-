#include "HfitterStats/HftMultiCalculator.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftMultiCalculator::HftMultiCalculator(HftAbsCalculator& calc1, HftAbsCalculator& calc2, 
                                       unsigned int current, const TString& name)
  : HftAbsMultiResultCalculator(calc1.Model(), name), m_current(current)
{
  std::vector<HftAbsCalculator*> calcs;
  calcs.push_back(&calc1);
  calcs.push_back(&calc2);
  Init(calcs);
}


HftMultiCalculator::HftMultiCalculator(HftAbsCalculator& calc1, HftAbsCalculator& calc2, HftAbsCalculator& calc3, 
                                       unsigned int current, const TString& name)
  : HftAbsMultiResultCalculator(calc1.Model(), name), m_current(current)
{
  std::vector<HftAbsCalculator*> calcs;
  calcs.push_back(&calc1);
  calcs.push_back(&calc2);
  calcs.push_back(&calc3);
  Init(calcs); 
}


HftMultiCalculator::HftMultiCalculator(const HftMultiCalculator& other, const TString& name)
  : HftAbsMultiResultCalculator(other, name)
{
  Init(other.m_calculators);
}


HftMultiCalculator::~HftMultiCalculator() 
{ 
  for (std::vector<HftAbsCalculator*>::iterator calc = m_calculators.begin(); calc != m_calculators.end(); calc++)
    delete *calc;
}


bool HftMultiCalculator::SetCurrent(const TString& name)
{
  for (unsigned int i = 0; i < NResults(); i++) 
    if (Calculator(i)->GetName() == name) {
      SetCurrent(i);
      return true;
    }
  return false;
}


void HftMultiCalculator::Init(const std::vector<HftAbsCalculator*>& calculators)
{
  for (unsigned int i = 0; i < calculators.size(); i++) 
    m_calculators.push_back(calculators[i]->Clone(AppendToName("calc") + Form("%d", i)));
}


bool HftMultiCalculator::LoadInputs(RooAbsData& data)
{
  for (std::vector<HftAbsCalculator*>::iterator calc = m_calculators.begin(); calc != m_calculators.end(); calc++)
    if (!(*calc)->LoadInputs(data)) return false;
  return true;
}


bool HftMultiCalculator::LoadInputs(HftTree& tree)
{
  for (std::vector<HftAbsCalculator*>::iterator calc = m_calculators.begin(); calc != m_calculators.end(); calc++)
    if (!(*calc)->LoadInputs(tree)) return false;
  return true;
}


bool HftMultiCalculator::LoadInputs(const HftAbsCalculator& other)
{
  const HftMultiCalculator* otherMC = dynamic_cast<const HftMultiCalculator*>(&other);
  if (!otherMC) {
    cout << "ERROR : can only copy from another HftMultiCalculator" << endl;
    return false;
  }
  for (unsigned int i = 0; i < NResults(); i++) 
    if (!Calculator(i)->LoadInputs(*otherMC->Calculator(i))) return false;
  return true;
}


bool HftMultiCalculator::MakeBranches(HftTree& tree)
{
  for (std::vector<HftAbsCalculator*>::iterator calc = m_calculators.begin(); calc != m_calculators.end(); calc++)
    if (!(*calc)->MakeBranches(tree)) return false;
  return true;
}


bool HftMultiCalculator::FillBranches(HftTree& tree)
{
  for (std::vector<HftAbsCalculator*>::iterator calc = m_calculators.begin(); calc != m_calculators.end(); calc++)
    if (!(*calc)->FillBranches(tree)) return false;
  return true;
}

