#include "HfitterStats/HftBand.h"

#include "TMath.h"
#include "TFile.h"
#include "TAxis.h"
#include "TPad.h"

#include "TVector.h"
#include "TMatrix.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

unsigned int HftBand::m_colors[5] = { 1, 3, 5, 2, 3 };


HftBand::HftBand(unsigned int nBands, const TString& title, const TString& xLabel, const TString& yLabel)
  : m_nBands(nBands), m_title(title), m_xLabel(xLabel), m_yLabel(yLabel), m_yMin(-DBL_MAX), m_yMax(-DBL_MAX)
{
}


HftBand::HftBand(const std::vector<double>& pos, const std::vector<HftInterval>& ers, 
                           const TString& title, const TString& xLabel, const TString& yLabel)
  : m_nBands(0), m_title(title), m_xLabel(xLabel), m_yLabel(yLabel), m_yMin(-DBL_MAX), m_yMax(-DBL_MAX)
{
  if (pos.size() == 0) return;
  if (pos.size() != ers.size()) {
    cout << "ERROR : trying to initialize HftBand with vectors of different length." << endl;
    return;
  }
  
  bool first = true;
  for (std::vector<HftInterval>::const_iterator er = ers.begin(); er != ers.end(); er++) {
    if (first) {
      m_nBands = er->NBands();
      first = false;
    }
    if (er->NBands () != NBands()) {
      cout << "ERROR : trying to initialize HftBand with HftInterval objects of different sizes." << endl;
      return;
    }
  }
  m_pos = pos;
  m_ers = ers;
}


HftBand::HftBand(const TGraphAsymmErrors& graph)
  : m_nBands(0), m_title(graph.GetTitle()), m_xLabel(graph.GetXaxis()->GetTitle()), m_yLabel(graph.GetYaxis()->GetTitle())
{
  for (int i = 0; i < graph.GetN(); i++) {
    HftInterval err(0);
    err.SetValue(0, graph.GetY()[i]);
    m_pos.push_back(graph.GetX()[i]);
    m_ers.push_back(err);
  }
}


bool HftBand::Add(double pos, const HftInterval& range)
{
  if (range.NBands() != NBands()) {
    cout << "ERROR : trying to add HftInterval with size " << range.NBands() 
         << " to HftBand with size " << NBands() << "." << endl;
    return false;
  }
  m_pos.push_back(pos);
  m_ers.push_back(range);
  return true;
}



TGraphAsymmErrors* HftBand::Graph(unsigned int i, const TString& name) const
{
  std::vector<double> centralValues, posValues, negValues, posErrors, negErrors;
  if (!Values(0, centralValues)) return 0;
  if (!Values(+i, posValues)) return 0;
  if (!Values(-i, negValues)) return 0;
  const unsigned int arraySize = 10000;
  if (NPoints() > arraySize) {
    cout << "WARNING: truncating error band to " << arraySize << " points in graph conversion." << endl;
  }
  double x[arraySize], y[arraySize], nx[arraySize], px[arraySize], ny[arraySize], py[arraySize];
  double gMin = DBL_MAX, gMax = -DBL_MAX;
  for (unsigned int k = 0; k < NPoints(); k++) { 
    if (k >= arraySize) continue;
    x[k] = Position(k);
    nx[k] = 0;
    px[k] = 0;
    y[k] = centralValues[k];
    ny[k] = centralValues[k] - negValues[k];
    py[k] = posValues[k] - centralValues[k];
    if (posValues[k] > gMax) gMax = posValues[k];
    if (posValues[k] < gMin) gMin = posValues[k];
  }
  TGraphAsymmErrors* graph = new TGraphAsymmErrors(TMath::Min(NPoints(), arraySize), x, y, nx, px, ny, py);
  graph->SetName(name);
  graph->SetTitle("");
  graph->SetMinimum(m_yMin > -DBL_MAX/2 ? m_yMin : gMin - (gMax - gMin)/10);
  graph->SetMaximum(m_yMax > -DBL_MAX/2 ? m_yMax : gMax + (gMax - gMin)/10);
  graph->SetTitle(m_title);
  graph->GetXaxis()->SetTitle(m_xLabel);
  graph->GetYaxis()->SetTitle(m_yLabel);
  graph->GetYaxis()->SetTitleOffset(1.5);
  return graph;
}


TGraphAsymmErrors* HftBand::ErrorGraph(int i, const TString& name) const
{
  std::vector<double> values, errors;
  if (!Values(i, values)) return 0;
  if (!ValueErrors(i, errors)) return 0;
  const unsigned int arraySize = 10000;
  if (NPoints() > arraySize) {
    cout << "WARNING: truncating error band to " << arraySize << " points in error graph conversion." << endl;
  }
  double x[arraySize], nx[arraySize], px[arraySize],
         e[arraySize], ne[arraySize], pe[arraySize];
  for (unsigned int k = 0; k < NPoints(); k++) { 
    if (k >= arraySize) continue;
    x[k] = Position(k);
    nx[k] = 0;
    px[k] = 0;
    e[k]  = values[k];
    ne[k] = errors[k];
    pe[k] = errors[k];
  }
  TGraphAsymmErrors* graph = new TGraphAsymmErrors(TMath::Min(NPoints(), arraySize), x, e, nx, px, ne, pe); 
  graph->SetName(name);
  return graph;
}


TGraphAsymmErrors** HftBand::Draw(double yMin, double yMax, Color_t lineColor, Style_t lineStyle, Width_t lineWidth, 
                                       const TString& gOpts, const TString& nameRoot,
                                       double xMin, double xMax, const TString& yTitle) 
{
  bool doErrors = (gOpts.Index("ERRORS") >= 0);
  TString otherOpts = gOpts;
  otherOpts.ReplaceAll("ERRORS", "");

  if (NPoints() == 0) return 0;
  
  TGraphAsymmErrors** graphs = new TGraphAsymmErrors*[NBands() + 1];
  std::vector<TGraphAsymmErrors*> errorGraphs;
  
  for (int i = NBands(); i >= 0; i--) {
    TString name = Form("%s_%d", nameRoot.Data(), i);
    TGraphAsymmErrors* graph = Graph(i, name);   
    if (yMin < yMax) {
      //graph->GetYaxis()->SetRangeUser(yMin, yMax);
      graph->SetMinimum(yMin);
      graph->SetMaximum(yMax);
    }
    if (xMin < xMax) graph->GetXaxis()->SetRangeUser(xMin, xMax);
    if (yTitle != "") graph->GetYaxis()->SetTitle(yTitle);
    graph->SetFillColor(m_colors[i]);
    if (i == 0) {
      graph->SetLineColor(lineColor);
      graph->SetLineStyle(lineStyle);
      graph->SetLineWidth(lineWidth);
    }
    graphs[i] = graph;
        
    //cout << " --> " << i << " " << graph->GetMaximum() << " " << "L3" + TString(i == NBands() ? "A" : "SAME") << endl;
    graph->Draw("3L" + TString(i == NBands() ? "A" : "SAME") + otherOpts);
    if (doErrors) {
      TGraphAsymmErrors* enGraph = ErrorGraph(-i, name + "_EN");
      TGraphAsymmErrors* epGraph = ErrorGraph(+i, name + "_EP");
      enGraph->SetFillStyle(3002);
      epGraph->SetFillStyle(3002);
      errorGraphs.push_back(enGraph);
      errorGraphs.push_back(epGraph);
    }
  }
  for (unsigned int i = 0; i < errorGraphs.size(); i++) errorGraphs[i]->Draw("3SAME" + otherOpts);
  gPad->RedrawAxis(); // Axes might have been obscured by shaded areas
  return graphs;
}


TGraph** HftBand::DrawLine(double yMin, double yMax, Color_t lineColor, Style_t lineStyle, 
                                const TString& gOpts, Width_t lineWidth, const TString& nameRoot, 
                                double xMin, double xMax, const TString& yTitle)
{
  TString otherOpts = gOpts;
  if (NPoints() == 0) return 0;
  
  TGraph** graphs = new TGraph*[2*NBands() + 1];
  std::vector<TGraph*> errorGraphs;
  
  for (int i = NBands(); i >= -NBands(); i--) {
    TString name = Form("%s_%s%d", nameRoot.Data(), (i > 0 ? "p" : (i == 0 ? "" : "n")), abs(i));
    TGraphAsymmErrors* asymGraph = Graph(abs(i), name + "asym");
    TGraph* graph = new TGraph(asymGraph->GetN());
    for (int k = 0; k < asymGraph->GetN(); k++) 
      graph->SetPoint(k, asymGraph->GetX()[k], asymGraph->GetY()[k] + 
                      (i == 0 ? 0 : (i > 0 ? asymGraph->GetErrorYhigh(k) : -asymGraph->GetErrorYlow(k))));
    graph->SetName(name);
    graph->SetTitle("");
    graph->SetLineColor(lineColor);
    graph->SetLineStyle(lineStyle);
    graph->SetLineWidth(lineWidth);
    graph->GetXaxis()->SetTitle(asymGraph->GetXaxis()->GetTitle());
    graph->GetYaxis()->SetTitle(asymGraph->GetYaxis()->GetTitle());
    graph->GetYaxis()->SetTitleOffset(asymGraph->GetYaxis()->GetTitleOffset());
    if (xMin < xMax) graph->GetXaxis()->SetRangeUser(xMin, xMax);
    if (yTitle != "") graph->GetYaxis()->SetTitle(yTitle);
    delete asymGraph;

    if (yMin < yMax) {
      graph->SetMinimum(yMin);
      graph->SetMaximum(yMax);
    }
    graphs[NBands() + i] = graph;
        
    //cout << " -================> " << i << " " << graph->GetMaximum() << " " << "L3" + TString(i == NBands() ? "A" : "SAME") << endl;
    TString axisOpt = ((i == NBands() && otherOpts.Index("SAME") < 0) ? "A" : "SAME");
    graph->Draw(axisOpt + otherOpts);
  }
  //for (unsigned int i = 0; i < errorGraphs.size(); i++) errorGraphs[i]->Draw(mainOpt + "SAME" + otherOpts);
  gPad->RedrawAxis(); // Axes might have been obscured by shaded areas
  return graphs;
}


bool HftBand::Values(int n,  std::vector<double>& vals) const
{
  vals.clear();
  for (std::vector<HftInterval>::const_iterator er = m_ers.begin(); er != m_ers.end(); er++) {
    double v;
    if (!er->Value(n, v)) { vals.clear(); return false; }
    vals.push_back(v);
  }
  return true;
}


bool HftBand::ValueErrors(int n,  std::vector<double>& errs) const
{
  errs.clear();
  for (std::vector<HftInterval>::const_iterator er = m_ers.begin(); er != m_ers.end(); er++) {
    double v;
    if (!er->ValueError(n, v)) { errs.clear(); return false; }
    errs.push_back(v);
  }
  return true;
}


bool HftBand::FindPosition(double val, HftInterval& pos, unsigned int order, double xMin, double xMax) const
{
  if (NPoints() == 0) return false;
  std::vector<double> positions, errors;

  for (int i = -NBands(); i <= NBands(); i++) {
    std::vector<double> ys, yn, yp;
    if (!Values(i, ys)) return false;
    if (!ValueErrors(i, yn)) return false;
    if (!ValueErrors(i, yp)) return false;
    for (unsigned int j = 0; j < ys.size(); j++) {
      yn[j] = ys[j] - yn[j];
      yp[j] = ys[j] + yp[j];
    }
    double ps, pn, pp;
    if (!FindPosition(val, ps, m_pos, ys, order, xMin, xMax)) ps = (i >= 0 ? 999 : -999);
    if (!FindPosition(val, pn, m_pos, yn, order, xMin, xMax)) pn  = -999;
    if (!FindPosition(val, pp, m_pos, yp, order, xMin, xMax)) pp  =  999;
    positions.push_back(ps);
    errors.push_back(TMath::Abs(pp - pn)/2);
  }
  pos = HftInterval(positions, errors);
  return true;
}


bool HftBand::FindPosition(double y, double& x, const std::vector<double>& xs, const std::vector<double>& ys, unsigned int order, double xMin, double xMax)
{ 
  if (xs.size() != ys.size()) return false;
  if (xs.size() < 2) {
    cout << "ERROR: we have only " << xs.size() << " points, cannot interpolate." << endl;
    return false;
  }
  if (xs.size() == 2 && order == 2) {
    cout << "WARNING: we have only " << xs.size() << " points, using linear interpolation instead of quadratic." << endl;
    order = 1;
  }
  //cout << "njpb: interpolating y = " << y << "using the following values:" << endl;
  //for (unsigned int i = 0; i < xs.size(); i++) cout << "(" << xs[i] << "," << ys[i] << ")" << endl;
  for (unsigned k = 1; k < xs.size(); k++) {
    if (order == 1) {
      if ((ys[k] - y)*(ys[k - 1] - y) > 0) continue;
      if (!FindPositionLinear(y, x, xs[k - 1], xs[k], ys[k - 1], ys[k])) return false;
      if (!(xMin <= x && x <= xMax)) continue;
      return true;
    }
    if (order == 2) {
      // use k - 1, k, k+1 ...
      unsigned int index = k;
      // ... or k - 2, k - 1, k, if 1) k+1 is out of range or 2) the "center" is close to k - 1
      if (k > 1 && (k == xs.size() - 1 || TMath::Abs(ys[k] - y) > TMath::Abs(ys[k - 1] - y))) 
        index = k - 1;
      if ((ys[index - 1] - y)*(ys[index] - y) > 0 && (ys[index] - y)*(ys[index + 1] - y) > 0 && k < xs.size() - 1) {
        //cout << "k = " << k << " : not in the right interval" << endl;
        continue;
      }
      double xn, xp;
      if (!FindPositionQuadratic(y, xn, xp, xs[index - 1], xs[index], xs[index + 1], ys[index - 1], ys[index], ys[index + 1])) {
        //cout << "k = " << k << " : could not interpolate" << endl;
        return false;
      }
      //cout << "interpolating at " << k << " " << xs[index] << " " << ys[index - 1] << " " << ys[index] << " " << ys[index + 1] << " " << y << " -> " << xn << " "<< xp << endl;
      bool xnOK = (xMin <= xn && xn <= xMax && xs[index - 1] <= xn && xn <= xs[index + 1]);
      bool xpOK = (xMin <= xp && xp <= xMax && xs[index - 1] <= xp && xp <= xs[index + 1]);

      if (k == xs.size() - 1) return TMath::Min(xp, xn);
      if (!xpOK && !xnOK) continue;
      if ( xpOK && !xnOK) { x = xp; return true; }
      if (!xpOK &&  xnOK) { x = xn; return true; }
      else if (fabs(xp - xs[index]) < fabs(xn - xs[index]))
        x = xp;
      else
        x = xn;
      //cout << "returning closest : " << x << endl;
      return true;
    }
  }
  cout << "ERROR: could not interpolate y = " << y << "using the following values:" << endl;
  for (unsigned int i = 0; i < xs.size(); i++) cout << "(" << xs[i] << "," << ys[i] << ")" << endl;
  return false;
}


double HftBand::FindPosition(TGraph& graph, double val, int order, double xMin, double xMax)
{
  TGraphAsymmErrors* gae = new TGraphAsymmErrors(graph.GetN(), graph.GetX(), graph.GetY());
  HftBand* band = new HftBand(*gae);
  double pos = band->FindPosition(val, order, xMin, xMax);
  delete gae;
  delete band;
  return pos;
}


bool HftBand::FindPositionLinear(double y, double& x, double x1, double x2, double y1, double y2)
{
  if (y2 - y1 == 0) return false;
  x = x1 + (x2 - x1)/(y2 - y1)*(y - y1);
  return true;
}


bool HftBand::FindPositionQuadratic(double y, double& xn, double& xp, double x1, double x2, double x3, double y1, double y2, double y3)
{
  TVector xVector(3), yVector(3);
  xVector[0] = x1; yVector[0] = y1; 
  xVector[1] = x2; yVector[1] = y2;
  xVector[2] = x3; yVector[2] = y3;
  if (x1 == x2 || x2 == x3 || x1 == x3) {
    cout << "ERROR: error band has multiple points at the same X positions (at least 2 of " << x1 << ", " << x2 << ", " << x3 << "), cannot interpolate" << endl;
    return false;
  }
  
  TMatrix vdmMatrix(3,3);
  for (unsigned int i = 0; i < 3; i++)
    for (unsigned int j = 0; j < 3; j++)
      vdmMatrix(i, j) = TMath::Power(xVector[i], 2 - (int)j);
      if (vdmMatrix.Determinant() == 0) {
        cout << "ERROR: quadratic interpolation matrix is singular, cannot interpolate quadratically." << endl;
        return false;
      }
  vdmMatrix.Invert();
  TVector aVector = vdmMatrix*yVector;
  double a = aVector[0], b = aVector[1], c = aVector[2] - y;
  double discriminant = b*b - 4*a*c;
  if (discriminant < 0) {
    cout << "ERROR: quadratic interpolation discriminant is negative, cannot interpolate quadratically." << endl;
    return false;
  }
  xn = (-b - TMath::Sqrt(discriminant))/(2*a);
  xp = (-b + TMath::Sqrt(discriminant))/(2*a);
  return true;
}


bool HftBand::Interpolate(double pos, HftInterval& val, unsigned int order) const
{
  if (NPoints() == 0) return false;
  std::vector<double> vals, errs;

  for (int i = -NBands(); i <= NBands(); i++) {
    std::vector<double> ys, yn, yp;
    if (!Values(i, ys)) return false;
    if (!ValueErrors(i, yn)) return false;
    if (!ValueErrors(i, yp)) return false;
    for (unsigned int j = 0; j < ys.size(); j++) {
      yn[j] = ys[j] - yn[j];
      yp[j] = ys[j] + yp[j];
    }
    double vl, vn, vp;
    if (!Interpolate(pos, vl, m_pos, ys, order)) vl = (i >= 0 ? 999 : -999);
    if (!Interpolate(pos, vn, m_pos, yn, order)) vn  = -999;
    if (!Interpolate(pos, vp, m_pos, yp, order)) vp  =  999;
    vals.push_back(vl);
    errs.push_back(TMath::Abs(vp - vn)/2);
  }
  val = HftInterval(vals, errs);
  return true;
}


bool HftBand::Interpolate(double x, double& y, const std::vector<double>& xs, const std::vector<double>& ys, unsigned int order)
{
  if (xs.size() != ys.size()) return false;
  if (xs.size() < 2) {
    cout << "ERROR: we have only " << xs.size() << " points, cannot interpolate." << endl;
    return false;
  }
  if (xs.size() == 2 && order == 2) {
    cout << "WARNING: we have only " << xs.size() << " points, using linear interpolation instead of quadratic." << endl;
    order = 1;
  }
  for (unsigned k = 1; k < xs.size(); k++) {
    if ((xs[k] - x)*(xs[k - 1] - x) > 0) continue;
    if (order == 1) return InterpolateLinear(x, y, xs[k - 1], xs[k], ys[k - 1], ys[k]);
    if (order == 2) {
      // use k - 1, k, k+1 ...
      unsigned int index = k;
      // ... or k - 2, k - 1, k, if 1) k+1 is out of range or 2) the "center" is close to k - 1
      if (k > 1 && (k == xs.size() - 1 || TMath::Abs(ys[k] - y) > TMath::Abs(ys[k - 1] - y)))
        index = k - 1;
      return InterpolateQuadratic(x, y, xs[index - 1], xs[index], xs[index + 1], ys[index - 1], ys[index], ys[index + 1]);
    }
    return false;
  }
  return false;
}


bool HftBand::InterpolateLinear(double x, double& y, double x1, double x2, double y1, double y2)
{
  if (x2 - x1 == 0) return false;
  y = y1 + (y2 - y1)/(x2 - x1)*(x - x1);
  return true;
}


bool HftBand::InterpolateQuadratic(double x, double& y, double x1, double x2, double x3, double y1, double y2, double y3)
{
  TVector xVector(3), yVector(3);
  xVector[0] = x1 - x2; yVector[0] = y1 - y2;
  xVector[1] = 0;       yVector[1] = 0;
  xVector[2] = x3 - x2; yVector[2] = y3 - y2;
  x -= x2;

  TMatrix vdmMatrix(3,3);
  for (unsigned int i = 0; i < 3; i++)
    for (unsigned int j = 0; j < 3; j++)
      vdmMatrix(i, j) = TMath::Power(xVector[i], 2 - (int)j);
  if (vdmMatrix.Determinant() == 0) {
    cout << "ERROR: quadratic interpolation matrix is singular, cannot interpolate quadratically." << endl;
    return false;
  }
  vdmMatrix.Invert();
  TVector aVector = vdmMatrix*yVector;
  double a = aVector[0], b = aVector[1], c = aVector[2];
  y = (a*x + b)*x + c + y2;

  return true;
}


double HftBand::Interpolate(TGraph& graph, double pos, int order)
{
  TGraphAsymmErrors* gae = new TGraphAsymmErrors(graph.GetN(), graph.GetX(), graph.GetY());
  HftBand* band = new HftBand(*gae);
  double val = band->Interpolate(pos, order);
  delete gae;
  delete band;
  return val;
}


bool HftBand::FindMinimum(double& pos, HftInterval& val, double xMin, double xMax) const
{
  if (NPoints() == 0) return false;
  std::vector<double> positions, errors;

  std::vector<double> ys;
  if (!Values(0, ys)) return false;
  if (!FindMinimum(pos, m_pos, ys, xMin, xMax)) return false;
  return Interpolate(pos, val, 2);
}


bool HftBand::FindMinimum(double& pos, const std::vector<double>& xs, const std::vector<double>& ys, double xMin, double xMax)
{
  if (xs.size() != ys.size()) return false;
  if (xs.size() < 3) {
    cout << "ERROR: we have only " << xs.size() << " points, cannot find minimum." << endl;
    return false;
  }

  double x = 0, y = 0, minX = 0, minY = DBL_MAX;
  
  for (unsigned k = 1; k < xs.size(); k++) {
    // use k - 1, k, k+1 ...
    unsigned int index = k;
    // ... or k - 2, k - 1, k, if 1) k+1 is out of range or 2) the "center" is close to k - 1
    if (k > 1 && (k == xs.size() - 1 || TMath::Abs(ys[k] - y) > TMath::Abs(ys[k - 1] - y)))
      index = k - 1;
    if (!FindMinimum(x, y, xs[index - 1], xs[index], xs[index + 1], ys[index - 1], ys[index], ys[index + 1])) return false;
    //cout << k << " " << xs[index - 1] << " " <<  xs[index] << " " << xs[index + 1] << " => " << x << endl;
    if (!(xs[index - 1]  < x && x < xs[index + 1])) continue; // invalid minimum, outside interval
    if (!(xMin < x && x < xMax)) continue;
    if (y < minY) {
      minX = x;
      minY = y;
    }
  }
  if (minY > DBL_MAX/2) return false;
  pos = minX;
  return true;
}


bool HftBand::FindMinimum(double& x, double& y, double x1, double x2, double x3, double y1, double y2, double y3)
{
  TVector xVector(3), yVector(3);
  xVector[0] = x1 - x2; yVector[0] = y1 - y2;  // offset by x2, y2 
  xVector[1] = 0;       yVector[1] = 0;
  xVector[2] = x3 - x2; yVector[2] = y3 - y2;

  TMatrix vdmMatrix(3,3);
  for (unsigned int i = 0; i < 3; i++)
    for (unsigned int j = 0; j < 3; j++)
      vdmMatrix(i, j) = TMath::Power(xVector[i], 2 - (int)j);
  if (vdmMatrix.Determinant() == 0) {
    cout << "ERROR: quadratic interpolation matrix is singular, cannot interpolate quadratically." << endl;
    return false;
  }
  // xVector.Print(); yVector.Print(); vdmMatrix.Print();
  vdmMatrix.Invert();
  TVector aVector = vdmMatrix*yVector;

  // aVector.Print();
  double a = aVector[0], b = aVector[1], c = aVector[2];
  if (a == 0) {
    cout << "ERROR: null quadratic term, cannot find minimum" << endl;
    return false;
  }
  x = -b/(2*a);
  y = (a*x + b)*x + c;
  
//  cout << "===" << endl;
//  cout << x2 << " " << Form("%.3f", y2) << endl;
//  cout << xVector[0] << " " << (a*xVector[0] + b)*xVector[0] + c << " " << yVector[0] << endl;
//  cout << xVector[1] << " " << (a*xVector[1] + b)*xVector[1] + c << " " << yVector[1] << endl;
//  cout << xVector[2] << " " << (a*xVector[2] + b)*xVector[2] + c << " " << yVector[2] << endl;
//  cout << x << " " << y << endl;

  x += x2;
  y += y2;
//  cout << x << " " << Form("%.3f", y) << endl;
  return true;
}

// bool HftBand::Interpolate(const TGraphAsymmErrors& graph, double pos, double& val, double& erM, double& erP) const
// {
//   if (graph.GetN() == 0) return HftInterval();
//   std::vector<double> positions, errors;
//   double tol = 1E-5;
// 
//   for (int i = 0; i < graph.GetN() - 1; i++) {
//     if (!(graph.GetX()[i] - tol < pos && pos < graph.GetX()[i + 1] + tol)) continue;
//     if (graph.GetX()[i] == graph.GetX()[i + 1]) {
//       cout << "ERROR : error band has two points at identical position! exiting..." << endl;
//       return false;
//     }
//     double u = (pos - graph.GetX()[i])/(graph.GetX()[i + 1] - graph.GetX()[i]);
//     val = graph.GetY()[i] + u*(graph.GetY()[i + 1] - graph.GetY()[i]);
//     erM = graph.GetErrorYlow(i)  + u*(graph.GetErrorYlow(i + 1)  - graph.GetErrorYlow(i));
//     erP = graph.GetErrorYhigh(i) + u*(graph.GetErrorYhigh(i + 1) - graph.GetErrorYhigh(i));
//     return true;
//   }
//   return false;
// }


// HftInterval HftBand::Interpolate(double pos) const
// {
//   if (NPoints() == 0) return HftInterval();
//   std::vector<double> positions, errors;
//   double tol = 1E-5;
//   
//   for (unsigned int i = 0; i < NPoints() - 1; i++) {    
//     if (!(Position(i) - tol < pos && pos < Position(i + 1) + tol)) continue;
//     if (Position(i) == Position(i + 1)) {
//       cout << "ERROR : error band has two points at identical position! exiting..." << endl;
//       return HftInterval();
//     }
//     double u = (pos - Position(i))/(Position(i + 1) - Position(i));
//     std::vector<double> values, errors;
//     for (int k = -NBands(); k <= NBands(); k++) {
//       values.push_back(ErrorRange(i).Value(k) + u*(ErrorRange(i + 1).Value(k) - ErrorRange(i).Value(k)));
//       errors.push_back(ErrorRange(i).ValueError(k) + u*(ErrorRange(i + 1).ValueError(k) - ErrorRange(i).ValueError(k)));
//     }
//     return HftInterval(values, errors);
//   }
//   return HftInterval();
// }


HftBand HftBand::Resample(const std::vector<double>& pos) const
{
  std::vector<HftInterval> ers;
  for (unsigned int i = 0; i < pos.size(); i++) ers.push_back(Interpolate(pos[i]));
  return HftBand(pos, ers, m_title, m_xLabel, m_yLabel);
}

                                                    
HftBand HftBand::Resample(unsigned int n, double minPos, double maxPos) const
{
  if (n == 0 || m_pos.size() == 0) return HftBand(*this);
  std::vector<double> pos;
  if (minPos > maxPos) {
    minPos = m_pos[0];
    maxPos = m_pos[m_pos.size() - 1];
  }
  for (unsigned int i = 0; i < n; i++)
    pos.push_back(minPos + i*(maxPos - minPos)/(n - 1));

  return Resample(pos);
}


bool HftBand::Intersections(const TGraphAsymmErrors& g1, const TGraphAsymmErrors& g2, std::vector<double>& xs, std::vector<double>& ys)
{
  if (g1.GetN() != g2.GetN()) {
    cout << "ERROR : cannot intersect graphs with different number of points " << g1.GetN() << " and " << g2.GetN() << endl;
    return false;
  }
  for (int i = 0; i < g1.GetN() - 1; i++) {
    if (g1.GetX()[i] != g2.GetX()[i]) {
      cout << "ERROR : cannot intersect graphs with different x positions " << g1.GetX()[i] << " and " << g2.GetX()[i] << " at point " << i << endl;
      return false;
    }
    double xL = g1.GetX()[i];
    double xR = g1.GetX()[i + 1];
    
    double y1L = g1.GetY()[i];
    double y1R = g1.GetY()[i + 1];
    double y2L = g2.GetY()[i];
    double y2R = g2.GetY()[i + 1];
    
    double beta = y2L - y1L;
    double alpha = (y1R - y1L) - (y2R - y2L);

    if (alpha == 0) continue;
    double u = beta/alpha;
    if (u < 0 || u >= 1) continue;
    xs.push_back(xL + (xR  - xL)*u);
    ys.push_back(y1L + (y1R  - y1L)*u);
  }
  return true;
}




bool HftBand::Max(const TGraphAsymmErrors& g1, const TGraphAsymmErrors& g2, TGraphAsymmErrors& gm)
{
  std::vector<double> xs, ys;
  if (g1.GetN() == 0) return true;
  if (!Intersections(g1, g2, xs, ys)) return false;
  
  std::vector<double> x, y;
  
  //for (unsigned int i = 0; i < xs.size(); i++) cout << "Intersect = " << xs[i] << " " << ys[i] << endl;

  const TGraphAsymmErrors* maxGraph = (g1.GetY()[0] > g2.GetY()[0] ? &g1 : &g2);
  x.push_back(g1.GetX()[0]);
  y.push_back(maxGraph->GetY()[0]);
  unsigned int currentXS = 0;
  for (int i = 1; i < g1.GetN(); i++) {
    while (currentXS < xs.size() && xs[currentXS] < g1.GetX()[i]) {
      x.push_back(xs[currentXS]);
      y.push_back(ys[currentXS]);
      currentXS++;
      maxGraph = (maxGraph == &g1 ? &g2 : &g1); 
    }
    x.push_back(g1.GetX()[i]);
    y.push_back(maxGraph->GetY()[i]);
  }
  
  gm.Set(x.size());
  for (unsigned int i = 0; i < x.size(); i++) gm.SetPoint(i, x[i], y[i]);
  return true;
}

TString HftBand::Str(const TString& options) const
{
  TString s;
  for (unsigned int i = 0; i < NPoints(); i++) {
    s += Form("pos = %.3f : ", Position(i));
    s += ErrorRange(i).Str(options);
    s += "\n";
  }

  return s;
}


void HftBand::Print(const TString& options) const 
{ 
  cout << Str(options) << endl; 
}


bool HftBand::Difference(const HftBand& other, HftBand& diff, bool relative) const
{
  if (NPoints() != other.NPoints()) return false;
  diff = HftBand(NBands(), Title(), XLabel(), YLabel());

  for (unsigned int i = 0; i < NPoints(); i++) {
    HftInterval rangeDiff;
    if (!ErrorRange(i).Difference(other.ErrorRange(i), rangeDiff, relative)) return false;
    diff.Add(Position(i), rangeDiff);
  }
  return true;
}


HftInterval HftBand::Max() const
{
  HftInterval max(-DBL_MAX);
  for (unsigned int i = 0; i < NPoints(); i++) 
    if (ErrorRange(i).Value() > max.Value()) max = ErrorRange(i);
  return max;
}


HftInterval HftBand::Min() const
{
  HftInterval min(DBL_MAX);
  for (unsigned int i = 0; i < NPoints(); i++) 
    if (ErrorRange(i).Value() < min.Value()) min = ErrorRange(i);
  return min;
}


int HftBand::MaxPosition() const
{
  HftInterval max(-DBL_MAX);
  int pos = -1;
  for (unsigned int i = 0; i < NPoints(); i++) 
    if (ErrorRange(i).Value() > max.Value()) { max = ErrorRange(i); pos = i; }
  return pos;
}


int HftBand::MinPosition() const
{
  HftInterval min(DBL_MAX);
  int pos = -1;
  for (unsigned int i = 0; i < NPoints(); i++) 
    if (ErrorRange(i).Value() < min.Value()) { min = ErrorRange(i); pos = i; }
  return pos;
}


bool HftBand::ShiftY(double dy, HftBand& shifted) const
{
  shifted = HftBand(NBands(), Title(), XLabel(), YLabel());
  for (unsigned int i = 0; i < NPoints(); i++) shifted.Add(Position(i), ErrorRange(i).Shift(dy));
  return true;
}


HftBand* HftBand::LoadFromFiles(const TString& fileRoot, double minVal, double maxVal, double step, const TString& result_name)
{
  std::vector<double> pos;
  std::vector<HftInterval> results;
  unsigned int n = int((maxVal - minVal)/step) + 1;
  for (unsigned int i = 0; i < n; i++) {
    double val = minVal + i*step;
    TString fileName = Form("%s_%g.root", fileRoot.Data(), val);
    TFile* f = TFile::Open(fileName);
    if (!f || !f->IsOpen()) {
      cout << "WARNING : could not open file " << fileName << endl;
      continue;
    }
    cout << "opened " << f->GetName() << endl;
    HftInterval* interval = (HftInterval*)f->Get(result_name);
    if (!interval) {
      cout << "WARNING : could not find interval with name '" << result_name << "' in file " << fileName << endl;
      delete f;
      continue;
    }
    pos.push_back(val);
    results.push_back(*interval);
    cout << "INFO : got result from " << f->GetName() << endl;
    delete f;
  }
  return new HftBand(pos, results);
}


TGraph* HftBand::Difference(const TGraph& g1, const TGraph& g2, bool relative, const TString& name)
{
  TGraph* diff = new TGraph(g1.GetN());
  for (int i = 0; i < g1.GetN(); i++) {
    double y1 = g1.GetY()[i];
    double y2 = g2.Eval(g1.GetX()[i]);
    double delta = y1 - y2;
    if (relative && y2 != 0) delta /= y2;
    diff->SetPoint(i,  g1.GetX()[i], delta);
  }
  diff->SetName(name);
  diff->SetTitle("");
  diff->GetXaxis()->SetTitle(g1.GetXaxis()->GetTitle());
  diff->GetYaxis()->SetTitle(g1.GetYaxis()->GetTitle());
  diff->SetLineColor(g1.GetLineColor());
  diff->SetLineWidth(g1.GetLineWidth());
  diff->SetLineStyle(g1.GetLineStyle());
  return diff;
}


TGraphAsymmErrors* HftBand::Difference(const TGraphAsymmErrors& g1, const TGraph& g2, bool relative, const TString& name)
{
  TGraphAsymmErrors* diff = new TGraphAsymmErrors(g1.GetN());
  for (int i = 0; i < g1.GetN(); i++) {
    double y1 = g1.GetY()[i];
    double y2 = g2.Eval(g1.GetX()[i]);
    double delta = y1 - y2;
    double scale = (relative && y2 != 0 ? 1/y2 : 1);
    diff->SetPoint(i,  g1.GetX()[i], delta*scale);
    diff->SetPointError(i, g1.GetErrorXlow(i)*scale, g1.GetErrorXhigh(i)*scale, g1.GetErrorYlow(i)*scale, g1.GetErrorYhigh(i)*scale);
  }
  diff->SetName(name);
  diff->SetTitle("");
  diff->GetXaxis()->SetTitle(g1.GetXaxis()->GetTitle());
  diff->GetYaxis()->SetTitle(g1.GetYaxis()->GetTitle());
  diff->SetLineColor(g1.GetLineColor());
  diff->SetLineWidth(g1.GetLineWidth());
  diff->SetLineStyle(g1.GetLineStyle());
  return diff;
}
