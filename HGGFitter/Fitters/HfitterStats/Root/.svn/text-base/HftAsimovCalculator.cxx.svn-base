#include "HfitterStats/HftAsimovCalculator.h"

#include "RooDataSet.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftAsimovCalculator::HftAsimovCalculator(const HftAbsCalculator& calculator, const HftParameterStorage& state, const TString& name)
  : HftAbsStateCalculator(calculator, state, name), m_calculator(calculator.Clone(name, this))
{
}


HftAsimovCalculator::HftAsimovCalculator(HftModel& genModel, const HftAbsCalculator& calculator, 
                                         const HftParameterStorage& genState, const HftParameterStorage& fitState, const TString& name)
  : HftAbsStateCalculator(genModel, genState, name), m_calculator(calculator.Clone(name, this)), m_fitState(fitState)
{
}


HftAsimovCalculator::HftAsimovCalculator(const HftAsimovCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsStateCalculator(other, name), m_calculator(other.Calculator()->Clone(name, cloner ? cloner : this)), m_fitState(other.m_fitState)
{
}

    
HftAsimovCalculator::~HftAsimovCalculator()
{
  delete m_calculator;
}


bool HftAsimovCalculator::LoadAsimov()
{
  HftParameterStorage currentState;
  Calculator()->Model().SaveState(currentState);
  Model().LoadState(State());
  RooDataSet* asimov = Model().GenerateAsimov();
  Calculator()->Model().LoadState(currentState);
  Calculator()->Model().LoadState(m_fitState);
  //cout << "Fit to Asimov with parameters: " << endl;
  //Model().Parameters().Print("v");
  bool result = LoadInputs(*asimov);
  delete asimov;
  return result;
}

bool HftAsimovCalculator::LoadAsimov(const TString& fileName, const TString& treeName)
{
  if (LoadFromFile(fileName, treeName)) return true;
  return (LoadAsimov() && SaveToFile(fileName, treeName));
}


TString HftAsimovCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = BaseStr(prefix, "HftAsimovCalculator");
  if (options.Index("V") >= 0) s += "\n" + Calculator()->Str(prefix + "  ", options);
  return s;
}
