#include "HfitterStats/HftUncappedPValueCalculator.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "Math/QuantFuncMathCore.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


bool HftUncappedPValueCalculator::GetStatistic(double& t0) const
{
  if (!HftPLRCalculator::GetStatistic(t0)) return false;
  t0 *= (FreeFitValue(0) > NullHypoValue(0) ? 1 : -1);
  return true;
}
 
 
bool HftUncappedPValueCalculator::GetStatisticBand(int i, double& tBand) const
{ 
  if (!HftPLRCalculator::GetStatisticBand(i, tBand)) return false;
  double sigma = FreeCalc()->FitError(*HypoVar(0));
  tBand *= (FreeFitValue(0)  + i*sigma > NullHypoValue(0) ? 1 : -1);
  return true;
}


bool HftUncappedPValueCalculator::ComputeFromAsymptotics(double t0, double& p0) const
{
  p0 = ROOT::Math::normal_cdf_c(SignedSqrt(t0));
  return true;
}


double HftUncappedPValueCalculator::SignificanceFromPValue(double p0) const
{ 
  return ROOT::Math::normal_quantile_c(p0, 1); 
}


bool HftUncappedPValueCalculator::ComputeSignificance(double stat, double& z) const
{
  // only for asymptotic case...
  if (UseSampling()) return HftAbsHypoTestCalculator::ComputeSignificance(stat, z);
  // use shortcut
  z = SignedSqrt(stat);
  return true;
}


TString HftUncappedPValueCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftUncappedPValueCalculator");
  s += PLRStr("  " + prefix, options);
  if (options.Index("V") >= 0)
    s += "\n" + prefix + " Z = " + Form("%g", Significance())  + "\n";
  return s;
}
