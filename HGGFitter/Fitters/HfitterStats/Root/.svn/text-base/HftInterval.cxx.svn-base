#include "HfitterStats/HftInterval.h"

#include "TMath.h"
#include "TH1.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

HftInterval::HftInterval(double value, double nerr, double perr) 
  : m_vals(3), m_errs(3) 
{ 
  m_vals[0] = value - nerr; 
  m_vals[1] = value; 
  m_vals[2] = value + perr; 
}


HftInterval::HftInterval(double value, double n2, double n1, double p1, double p2)
  : m_vals(5), m_errs(5) 
{
  m_vals[0] = value - n2; 
  m_vals[1] = value - n1; 
  m_vals[2] = value; 
  m_vals[3] = value + p1; 
  m_vals[4] = value + p2; 
}

HftInterval::HftInterval(const std::vector<double>& vals, const std::vector<double>& errs)
{
  if (vals.size() % 2 == 0) {
    cout << "ERROR: cannot build an HftInterval from a vector of even size (" << vals.size() << ")" << endl;
    return;
  }
  if (errs.size() == 0) 
    m_errs = std::vector<double>(vals.size());
  else {
    if (errs.size() != vals.size()) {
      cout << "ERROR : cannot create HftInterval, the numbers of values and errors provided were different." << endl;
      return;
    }
    m_errs = errs;  
  }
  m_vals = vals;
}
    
    
bool HftInterval::Value(int n, double& val) const
{
  if (TMath::Abs(n) > NErrors()) return false;
  val = m_vals[n + NErrors()];
  return true;
}


bool HftInterval::ValueError(int n, double& err) const
{
  if (TMath::Abs(n) > NErrors()) return false;
  err = m_errs[n + NErrors()];
  return true;
}


bool HftInterval::SetValue(int n, double val)
{
  if (TMath::Abs(n) > NErrors()) return false;
  m_vals[n + NErrors()] = val;
  return true;
}

bool HftInterval::SetError(int n, double err)
{
  return SetValue(n, Value(0) + err) && SetValue(-n, Value(0) - err);
}


bool HftInterval::SetValueError(int n, double err)
{
  if (TMath::Abs(n) > NErrors()) return false;
  m_errs[n + NErrors()] = err;
  return true;
}


bool HftInterval::CopyValues(const HftInterval& other)
{
  if (other.NErrors() < NErrors()) {
    cout << "ERROR: cannot copy values from HftInterval with smaller size (" << other.NErrors() << " < " << NErrors() << ")" << endl;
    return false;
  }
  for (int i = -NErrors(); i <= NErrors(); i++) {
    SetValue(i, other.Value(i));
    SetValueError(i, other.ValueError(i));
  }
  return true;
}


TString HftInterval::Str(const TString& options) const
{
  if (m_vals.size() == 0)
    return "Invalid values";
 
  TString s = "";
  
  TString opts = options;
  opts.ToUpper();
  
  TString floatFormat = (opts.Index("p") ? "%.6f" : "%.3f");
  
  if (opts.Index("v") >= 0) {
    s += Form("Central value = " + floatFormat, Value(0));
    if (m_errs.size() == m_vals.size()) s += Form(" +/- " + floatFormat, ValueError(0));
    s += "\n";
    for (int i = 1; i <= NErrors(); i++) {
      s += Form("+ %d sigma(s) value = " + floatFormat + " ", i, Value(+i));
      if (m_errs.size() == m_vals.size()) s += Form(" +/- " + floatFormat, ValueError(+i));
      s += "\n";
      s += Form("- %d sigma(s) value = " + floatFormat + " ", i, Value(-i));
      if (m_errs.size() == m_vals.size()) s += Form(" +/- " + floatFormat, ValueError(-i));
      if (i < NErrors()) s += "\n";
    }
    return s;
  }
  
  s += Form(floatFormat, Value(0));
  if (NErrors() > 0) {
    s += " +";
    if (NErrors() > 1) s += " (";
    for (int i = 1; i <= NErrors(); i++) { s += Form(" " + floatFormat, +Error(+i)); if (i < NErrors()) s += ","; }
    if (NErrors() > 1) s += " )";
    s += " -";
    if (NErrors() > 1) s += " (";
    for (int i = 1; i <= NErrors(); i++) { s += Form(" " + floatFormat, -Error(-i)); if (i < NErrors()) s += ","; }
    if (NErrors() > 1) s += ")";
  }
  
  return s;
}


void HftInterval::Print(const TString& options) const 
{ 
  cout << Str(options) << endl; 
}


HftInterval HftInterval::MedianWithErrors(const std::vector<double>& vals, int n)
{
  HftInterval median = HftInterval::Make(n);
  if (!MedianWithErrors(vals, median)) return HftInterval();
  return median;
}


bool HftInterval::MedianWithErrors(const std::vector<double>& vals, HftInterval& median, bool verbose)
{
  if (vals.size() == 0) return false;

  std::vector<double> sortedVals = vals;
  sort(sortedVals.begin(), sortedVals.end());
  unsigned int s = sortedVals.size();
    
  //cout << "extracting " << median.NErrors() << "-median of ";
  //for (unsigned int i = 0; i < s; i++) cout << "(" << i << ") "  << sortedVals[i] << " ";
  //cout << endl;
  double medianValue = QuantileValue(0.5, sortedVals);
  for (int i = -median.NErrors(); i <= median.NErrors(); i++) {
    if (vals.size() == 1 && i != 0) {
      median.SetValue(i, 0);
      median.SetValueError(i, 0);
      continue;      
    }
    double prob = TMath::Prob(i*i, 1);
    double quantile = (i <= 0 ? prob/2 : 1 - prob/2);
    double qv = (i == 0 ? medianValue : QuantileValue(quantile, sortedVals));
    median.SetValue(i, qv);
    double deltaQ = sqrt(quantile*(1 - quantile)/s);
    double qvLow  = QuantileValue(quantile - deltaQ,  sortedVals);
    double qvHigh = QuantileValue(quantile + deltaQ, sortedVals);
    double qvErr = TMath::Abs(qvLow-qvHigh)/2;
    if (verbose) {
      if (TMath::Abs(qvErr/medianValue) > 1)
        cout << "ERROR: " << i << "-sigma band calculation has a " <<  int(TMath::Abs(qvErr/medianValue)*100) 
             << " % uncertainty for so few (" << s << ") values!" << std::endl;
      else if (TMath::Abs(qvErr/medianValue) > 0.1)
        cout << "WARNING: " << i << "-sigma band calculation has a " <<  int(TMath::Abs(qvErr/medianValue)*100) 
             << " % uncertainty for so few (" << s << ") values!" << std::endl;
    }
    median.SetValueError(i, qvErr);
    //cout << "Median + " << i << " sigmas = " << qv << endl;
  }
  return median;
}


double HftInterval::QuantileValue(double quantile, const std::vector<double>& sortedVals)
{
  unsigned int s = sortedVals.size();
  double pos = s*quantile - 0.5; // -0.5 since coordinates [i, i+1[ correspond to slot i in the vector.
  unsigned int i = (unsigned int)(pos);
  double delta = pos - i;
  if (i >= sortedVals.size()) return sortedVals.back();
  return (1 - delta)*sortedVals[i] + delta*sortedVals[i+1];
}


HftInterval HftInterval::MedianWithErrors(const TH1& hist, int n)
{  
  HftInterval medians = HftInterval::Make(n);
  double integral = hist.Integral();
  for (int i = -n; i <= n; i++) {
    double prob = TMath::Prob(i*i, 1);
    double quantile = (i > 0 ? 1 - prob/2 : prob/2);
    if (integral*quantile < 10)   
      cout << "ERROR : " << i << " sigma calculation does not make sense for so few (" << integral << ") histogram values!" << std::endl;
    else if (integral*quantile < 1000) 
      cout << "WARNING " << i << " sigma calculation is not optimal for so few (" << integral << ") histogram values!" << std::endl;
    double partialIntegral = 0;
    double pos = hist.GetXaxis()->GetXmax();
    for (int k = 1; k <= hist.GetNbinsX(); k++) {
      partialIntegral += hist.GetBinContent(k);
      if (partialIntegral >= integral*quantile) {
        //cout << "quantile = " << quantile << " : interval around " << hist.GetBinCenter(k) << endl;
        pos = hist.GetBinLowEdge(k) + (integral*quantile - (partialIntegral - hist.GetBinContent(k)))/hist.GetBinContent(k)*hist.GetBinWidth(k);
        break;
      }
    }
    medians.SetValue(i, pos);
    //cout << "Median + " << i << " sigmas = " << pos << endl;
  }
  return medians;
}


bool HftInterval::Difference(const HftInterval& other, HftInterval& diff, bool relative) const
{
  if (NErrors() != other.NErrors()) return false;
  double denom = (relative ? other.Value(0) : 1);
  if (denom == 0 && relative) return false;
  diff = HftInterval::Make(NErrors());
  diff.SetValue(0, (Value(0) - other.Value(0))/denom);
  for (int i = -NErrors(); i <= NErrors(); i++) {
    if (i == 0) continue;
    diff.SetValue(i, diff.Value(0) + (i > 0 ? +1 : -1)*TMath::Sqrt(TMath::Power(Error(i), 2) + TMath::Power(other.Error(i), 2))/denom);
    diff.SetValueError(i, TMath::Sqrt(TMath::Power(ValueError(i), 2) + TMath::Power(other.ValueError(i), 2))/denom);
  }
  return true;
}


HftInterval HftInterval::Shift(double dy) const
{
  HftInterval shifted = HftInterval::Make(NErrors());
  for (int i = -NErrors(); i <= NErrors(); i++) {
    shifted.SetValue(i, Value(i) + dy);
    shifted.SetValueError(i, ValueError(i));
  }
  return shifted;
}


HftInterval HftInterval::Flip() const
{
  HftInterval flipped = HftInterval::Make(NErrors());
  for (int i = -NErrors(); i <= NErrors(); i++) {
    flipped.SetValue(i, Value(-i));
    flipped.SetValueError(i, ValueError(-i));
  }
  return flipped;
}
