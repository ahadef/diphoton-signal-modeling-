#include "HfitterStats/HftScanIntervalCalculator.h"

#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "HfitterStats/HftPLRCalculator.h"

#include "RooRealVar.h"
#include "Math/ProbFuncMathCore.h"

#include "TFile.h"
#include "TTree.h"
#include "TH2D.h"

#include "TStyle.h"
#include "TLine.h"
#include "TMarker.h"
#include "TGraphAsymmErrors.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftScanIntervalCalculator::HftScanIntervalCalculator(const HftMultiHypoTestCalculator& calculator)
  : HftAbsCalculator(calculator), m_calculator((HftMultiHypoTestCalculator*)calculator.Clone()), m_order(2)
{
  SetZ(1);
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanIntervalCalculator::HftScanIntervalCalculator(const HftAbsHypoTestCalculator& calculator, const TString& varName, 
                                                   unsigned int nPoints, double minVal, double maxVal, const TString& name)
  : HftAbsCalculator(calculator), 
    m_calculator(new HftMultiHypoTestCalculator(calculator, varName, nPoints, minVal, maxVal, name)),
    m_order(2)
{ 
  SetZ(1);
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanIntervalCalculator::HftScanIntervalCalculator(const HftAbsHypoTestCalculator& calculator, const TString& varName, 
                                                     const std::vector<double>& points, const TString& name)
  : HftAbsCalculator(calculator), 
    m_calculator(new HftMultiHypoTestCalculator(calculator, varName, points, name)),
    m_order(2)
{ 
  SetZ(1);
  // TBD check for duplicate hypos, otherwise trouble later
}



HftScanIntervalCalculator::HftScanIntervalCalculator(const HftAbsHypoTestCalculator& calculator, unsigned int nPoints,
                                                     double minVal, double maxVal, const TString& name)
  : HftAbsCalculator(calculator), 
    m_calculator(new HftMultiHypoTestCalculator(calculator, calculator.HypoVar(0)->GetName(), nPoints, minVal, maxVal, name)),
    m_order(2)
{ 
  SetZ(1);
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanIntervalCalculator::HftScanIntervalCalculator(HftModel& model, const TString& varName, 
                                                   unsigned int nPoints, double minVal, double maxVal, 
                                                   const Options& fitOptions, const TString& name)
  : HftAbsCalculator(model), 
    m_order(2)
{ 
  HftPLRCalculator* calc = new HftPLRCalculator(model, varName, fitOptions);
  m_calculator = new HftMultiHypoTestCalculator(*calc, varName, nPoints, minVal, maxVal, name);
  delete calc;
  SetZ(1);
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanIntervalCalculator::HftScanIntervalCalculator(HftModel& model, const TString& varName, const std::vector<double>& points, 
                                                     const Options& fitOptions, const TString& name)
  : HftAbsCalculator(model), 
    m_order(2)
{ 
  HftPLRCalculator* calc = new HftPLRCalculator(model, varName, fitOptions);
  m_calculator = new HftMultiHypoTestCalculator(*calc, varName, points, name);
  delete calc;
  SetZ(1);
  // TBD check for duplicate hypos, otherwise trouble later
}


HftScanIntervalCalculator::HftScanIntervalCalculator(const HftScanIntervalCalculator& other, const TString& name, const HftAbsCalculator* cloner)
  : HftAbsCalculator(other, name), m_calculator((HftMultiHypoTestCalculator*)other.Calculator()->Clone(name, cloner)), 
  m_cl(other.ContourCL()), m_order(other.m_order)
{
}


void HftScanIntervalCalculator::AddVar(const TString& varName, unsigned int nPoints, double minVal, double maxVal)
{
  Calculator()->AddScanRange(varName, nPoints, minVal, maxVal);
  SetZ(1);
}
    

void HftScanIntervalCalculator::SetZ(double z)
{
  m_cl = ROOT::Math::chisquared_cdf(z, NVars());
}


bool HftScanIntervalCalculator::GetResult(HftInterval& interval) const
{
  if (NVars() == 0) {
    cout << "WARNING in HftScanIntervalCalculator : no variables defined for itnerval computation" << endl;
    interval =  HftInterval();
    return true;
  }
  if (NVars() > 1) {
    cout << "ERROR in HftScanIntervalCalculator : interval computation not supported for >1 variables" << endl;
    interval =  HftInterval();
    return false;    
  }
  HftBand clBand;
  if (!Calculator()->GetCurve(clBand)) return false;

  int minPos = clBand.MinPosition();
  if (minPos < 0) {
    cout << "ERROR in HftScanIntervalCalculator : cannot find CL minimum" << endl;
    return false;
  }
  // take best fit from calculator closest to the minimum; Won't matter for common alt hypo computation (default)
  // but might make a small difference otherwise
  double cv = Calculator()->HypoCalculator(minPos)->AltHypoValue(0);
  HftInterval lo, up;
  if (!clBand.FindPosition(ContourCL(), lo, m_order, -DBL_MAX, cv)) {
    cout << "ERROR in HftScanIntervalCalculator : cannot locate lower end of interval" << endl;
    return false;
  }
  if (!clBand.FindPosition(ContourCL(), up, m_order, cv, +DBL_MAX)) {
    cout << "ERROR in HftScanIntervalCalculator : cannot locate upper end of interval" << endl;
    return false;
  }
  interval = HftInterval(cv, cv - lo.Value(), up.Value() - cv);
  return true;
}


bool HftScanIntervalCalculator::MakeBranches(HftTree& tree)
{
  if (!tree.AddVar(ResultName(), GetName())) return false;
  return Calculator()->MakeBranches(tree);
}


bool HftScanIntervalCalculator::FillBranches(HftTree& tree)
{
  double result;
  if (!GetResult(result)) return false;
  if (!tree.FillVar(ResultName(), result, GetName())) return false;
  return Calculator()->FillBranches(tree);
}


void HftScanIntervalCalculator::DrawInterval()
{
  if (NVars() != 1 || Calculator()->ScanGrid().NPoints() == 0) return;
  HftBand clCurve = CLCurve();
  HftInterval interval = Result();
  CLCurve().Graph()->Draw("CAP");
  double xMin = Calculator()->ScanGrid().Positions(0)[0];
  double xMax = Calculator()->ScanGrid().Positions(0).back();
  TLine l;
  l.SetLineStyle(kDashed);
  l.DrawLine(xMin, ContourCL(), xMax, ContourCL());
  l.SetLineStyle(kSolid);
  l.SetLineWidth(3);
  l.DrawLine(interval.Value(-1), ContourCL(), interval.Value(+1), ContourCL());
  TMarker m;
  m.SetMarkerSize(0.1);
  m.DrawMarker(interval.Value(), ContourCL());
}


void HftScanIntervalCalculator::DrawCLHistogram2D(double cl2)
{
  double cl = ContourCL();
  TH2D* clHistCont = CLHistogram2D("clHistCont");
  if (cl2 > 0) {
    double cls[2] = { cl, cl2 };
    int styles[2] = {  1,   2 };
    clHistCont->SetContour(2, cls);
    gStyle->SetPalette(2, styles);
  }
  else {
    double cls[1] = { cl };
    int styles[1] = {  1 };
    clHistCont->SetContour(1, cls);
    gStyle->SetPalette(1, styles);

  }
  clHistCont->Draw("CONT2");
}


TString HftScanIntervalCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftScanIntervalCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + " = " + Form("%g", Result()) + ")";
  s += "\n" + Calculator()->Str(prefix + "   ", options);
  return s;
}
