#include "HfitterStats/HftMaxFindingCalculator.h"

#include "RooFitResult.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftMaxFindingCalculator::HftMaxFindingCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, unsigned int nPoints, 
                                                 double minVal, double maxVal, bool minimize, const TString& name)
  : HftAbsCalculator(calculator, name), m_minimize(minimize), m_maxIndex(-999)
{   
  m_scanningCalculator = new HftScanningCalculator(calculator, scanVarName, nPoints, minVal, maxVal, AppendToName("scan"));
}


HftMaxFindingCalculator::HftMaxFindingCalculator(const HftAbsCalculator& calculator, const TString& scanVarName, 
                                                 const std::vector<double>& pos, bool minimize, const TString& name) 
  : HftAbsCalculator(calculator, name), m_minimize(minimize), m_maxIndex(-999)
{ 
  m_scanningCalculator = new HftScanningCalculator(calculator, scanVarName, pos, AppendToName("scan"));
}


HftMaxFindingCalculator::HftMaxFindingCalculator(const std::vector<const HftAbsCalculator*>& calculators, RooRealVar& scanVar, 
                                                 const std::vector<double>& pos, bool minimize, const TString& name)
  : HftAbsCalculator(*calculators[0], name), m_minimize(minimize), m_maxIndex(-999)
{
  m_scanningCalculator = new HftScanningCalculator(calculators, scanVar, pos, AppendToName("scan"));
}

HftMaxFindingCalculator::HftMaxFindingCalculator(const HftMaxFindingCalculator& other, const TString& name)
  : HftAbsCalculator(other, name), m_minimize(other.m_minimize), m_maxIndex(-999)
{
  m_scanningCalculator = new HftScanningCalculator(*other.Calculator(), AppendToName("scan"));
}


HftMaxFindingCalculator::~HftMaxFindingCalculator() 
{ 
  delete m_scanningCalculator; 
}


bool HftMaxFindingCalculator::GetResult(double& result) const
{ 
  if (MaxIndex() < 0) return false; 
  return Calculator()->Calculator(MaxIndex())->GetResult(result); 
}


bool HftMaxFindingCalculator::GetResult(HftInterval& result) const
{ 
  if (MaxIndex() < 0) return false; 
  return Calculator()->Calculator(MaxIndex())->GetResult(result); 
}


bool HftMaxFindingCalculator::LoadInputs(RooAbsData& data)
{
  if (!Calculator()->LoadInputs(data)) return false;
  double maxResult = -DBL_MAX;
  for (unsigned int i = 0; i < Calculator()->NPoints(); i++) {
    double result = 0;
    if (!Calculator()->Calculator(i)->GetResult(result)) return false;
    if (m_maxIndex < 0 || (m_minimize ^ (result > maxResult))) {
      m_maxIndex = i;
      maxResult = result;
    }
  }
  cout << "--> Max result = " << maxResult << ", found at index = " << MaxIndex() << endl;
  return true;
}


bool HftMaxFindingCalculator::LoadInputs(const HftAbsCalculator& other)
{
  const HftMaxFindingCalculator* otherMFC = dynamic_cast<const HftMaxFindingCalculator*>(&other);
  if (!otherMFC) {
    cout << "ERROR : can only copy from another HftMaxFindingCalculator" << endl;
    return false;
  }
  m_maxIndex = otherMFC->m_maxIndex;
  return Calculator()->LoadInputs(*otherMFC->Calculator());
}


bool HftMaxFindingCalculator::LoadInputs(HftTree& tree)
{
  if (!tree.LoadVal("maxIndex", m_maxIndex, GetName(), true)) return false;
  return Calculator()->LoadInputs(tree);
}


bool HftMaxFindingCalculator::MakeBranches(HftTree& tree)
{
  if (!tree.AddVar("maxIndex", GetName())) return false;
  return Calculator()->MakeBranches(tree);
}


bool HftMaxFindingCalculator::FillBranches(HftTree& tree)
{
  if (!tree.FillVar("maxIndex", m_maxIndex, GetName())) return false;
  return Calculator()->FillBranches(tree); 
}


TString HftMaxFindingCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = prefix + "HftMaxFindingCalculator: " + GetName() + " (" + ResultName()  + " : " + ResultTitle() + ")";
  s += "\n" + Calculator()->Str(prefix + "   ", options);
  return s;
}
