#include "HfitterStats/HftScanStudy.h"

#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftExpHypoTestCalc.h"

#include "TGraphErrors.h"
#include "TGraphAsymmErrors.h"
#include "TLegend.h"
#include "TAxis.h"
#include "TLine.h"
#include "TLatex.h"
#include "TMath.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


std::map<TString, HftScanStudy*>* HftScanStudy::m_registry = new std::map<TString, HftScanStudy*>();


HftScanStudy::HftScanStudy(const HftScanningCalculator& calculator, const TString& fileRoot, 
                           RooAbsData& data, const HftParameterStorage& asimovState, unsigned int sparsifyExp)
  : m_fileRoot(fileRoot), m_asimovState(asimovState)
{
  m_observed = (HftScanningCalculator*)calculator.Clone(calculator.AppendToName("obs"));
  for (unsigned int i = 0; i < calculator.NPoints(); i++) m_data.push_back(&data);
  std::vector<double> expected;
  FillSparse(calculator, sparsifyExp, expected);
  m_expected = MakeExpected(calculator, expected);
  Register(calculator.GetName(), *this);
}


HftScanStudy::HftScanStudy(const HftScanningCalculator& observed, const std::vector<double>& expected,
                           const TString& fileRoot, const std::vector<RooAbsData*>& data, const HftParameterStorage& asimovState)
  : m_fileRoot(fileRoot), m_asimovState(asimovState)
{
  m_observed = (HftScanningCalculator*)observed.Clone(observed.AppendToName("obs"));
  if (data.size() != observed.NPoints()) {
    cout << "ERROR: trying to initialize HftScanStudy with array of datasets of size " << data.size() << ", when we have " 
         << observed.NPoints() << " points in the scan of the observed  quantity" << endl;
    return;
  }
  for (unsigned int i = 0; i < observed.NPoints(); i++) m_data.push_back(data[i]);
  m_expected = MakeExpected(observed, expected);
  Register(observed.GetName(), *this);
}


HftScanStudy::HftScanStudy(const HftScanningCalculator& observed, const HftScanningCalculator& expected,
                           const TString& fileRoot, const std::vector<RooAbsData*>& data)
  : m_isLoaded(observed.NPoints()), m_fileRoot(fileRoot)
{
  m_observed = (HftScanningCalculator*)observed.Clone(observed.AppendToName("obs"));
  m_expected = (HftScanningCalculator*)expected.Clone(observed.AppendToName("exp"));
  if (data.size() != observed.NPoints()) {
    cout << "ERROR: trying to initialize HftScanStudy with array of datasets of size " << data.size() << ", when we have " 
         << observed.NPoints() << " points in the scan of the observed  quantity" << endl;
    return;
  }
  for (unsigned int i = 0; i < observed.NPoints(); i++) m_data.push_back(data[i]);
}


HftScanStudy::~HftScanStudy()
{
  delete m_expected;
  delete m_observed;
}


bool HftScanStudy::FillSparse(const HftScanningCalculator& observed, unsigned int sparsify, std::vector<double>& expected)
{
  for (unsigned int i = 0; i < observed.NPoints(); i++) {
    if (i % sparsify != 0  && i != observed.NPoints() - 1) continue; // always take first and last points
    expected.push_back(observed.Position(i));
  }
  return true;
}


HftScanningCalculator* HftScanStudy::MakeExpected(const HftScanningCalculator& observed, const std::vector<double>& expected)
{
  std::vector<const HftAbsCalculator*> calculators;
  std::vector<double> pos;
  unsigned int iExp = 0;
  for (unsigned int i = 0; i < observed.NPoints(); i++) {
    m_isLoaded.push_back(0);
    if (observed.Position(i) != expected[iExp]) continue;
    cout << "Making expected calculator at position " << observed.Position(i) << endl;
    HftPLRCalculator* clsb = dynamic_cast<HftPLRCalculator*>(observed.Calculator(i));
    if (clsb) cout << "INFO: HftScanStudy using Asimov state from Hypo calculator, overriding the one provided in constructor" << endl;
    calculators.push_back(new HftAsimovCalculator(*observed.Calculator(i), clsb && clsb->ExpCalc() ? clsb->ExpCalc()->ExpectedState() : m_asimovState, observed.Calculator(i)->AppendToName("exp")));
    pos.push_back(observed.Position(i));
    iExp++;
  }
  return new HftScanningCalculator(calculators, *observed.ScanVar(), pos, observed.AppendToName("exp"),
                                   observed.DoStateUpdate());
}

bool HftScanStudy::LoadObserved(unsigned int first, unsigned int last, bool makeIfMissing)
{
  if (last == 0) last = Observed()->NPoints();
  HftParameterStorage state;
  if (!m_observed->Model().SaveState(state)) return false;
  for (unsigned i = first; i < last; i++) {
    if ((m_isLoaded[i] & 1) != 0) continue;
    if (!m_observed->Model().LoadState(state)) return false;
    if (makeIfMissing) {
      if (!m_observed->LoadInputs(*m_data[i], m_fileRoot + "_obs", i, i + 1)) return false;
    }
    else {
      if (!m_observed->LoadFromFile(m_fileRoot + "_obs", i, i + 1)) {
        cout << "ERROR: observed data is missing at point " << i << " (and possibly later points as well), use LoadObserved() to generate it" << endl;
        return false;
      }
    }
    m_isLoaded[i] |= 1;
  }
  if (!m_observed->Model().SaveState(state)) return false;
  return true;
}


bool HftScanStudy::LoadExpected(unsigned int first, unsigned int last, bool makeIfMissing)
{
  if (last == 0) last = Expected()->NPoints();
  HftParameterStorage state;
  if (!m_expected->Model().SaveState(state)) return false;
  for (unsigned i = first; i < last; i++) {
    if ((m_isLoaded[i] & 2) != 0) continue;
    if (!m_expected->Model().LoadState(state)) return false;
    if (makeIfMissing) {
      if (!m_expected->LoadAsimov(m_fileRoot + "_exp", i, i + 1)) return false;
    }
    else {
      if (!m_expected->LoadFromFile(m_fileRoot + "_exp", i, i + 1)) {
        cout << "ERROR: expected data is missing at point " << i << " (and possibly later points as well), use LoadExpected() to generate it" << endl;
        return false;
      }
    }
    m_isLoaded[i] |= 2;
  }
  if (!m_expected->Model().LoadState(state)) return false;
  return true;
}


bool HftScanStudy::Load(unsigned int first, unsigned int last, bool makeIfMissing) 
{ 
  return (LoadObserved(first, last, makeIfMissing) && LoadExpected(first, last, makeIfMissing)); 
}


void HftScanStudy::DrawLimit(double yMin, double yMax, double xMin, double xMax, const TString& yTitle, 
                             const TString& expLegend, const TString& obsLegend, const TString& label, 
                             double lumi, double lumiX, double lumiY, double labelX, double labelY)
{
  if (!Load()) return;
  HftBand expected = Expected()->Curve(2);
  HftBand observed = Observed()->Curve(0);
  
  TGraphAsymmErrors** gExp = expected.Draw(yMin, yMax, 2, 2);
  TGraphAsymmErrors* gObs = observed.Graph(0);
  
  if (xMin < xMax) gExp[0]->GetXaxis()->SetRangeUser(xMin, xMax);
  gExp[0]->GetYaxis()->SetTitle(yTitle);
  gExp[0]->GetYaxis()->SetTitleOffset(1);

  gObs->SetLineWidth(3);
  gObs->Draw("L");  
  
  if (expLegend != "" || obsLegend != "") {
    TLegend* legend = new TLegend(0.2, 0.9, 0.55, 0.65, "", "NDC");
    legend->SetFillStyle(0);
    legend->SetLineColor(0);
    legend->SetShadowColor(0);
    legend->SetBorderSize(0);
    legend->AddEntry(gObs, obsLegend, "L");
    
    legend->AddEntry(gExp[0], expLegend, "L");
    legend->AddEntry(gExp[1], "#pm 1 #sigma", "F");
    legend->AddEntry(gExp[2], "#pm 2 #sigma", "F");
    legend->Draw();
  }

  TLine line; 
  line.SetLineStyle(kDashDotted); line.SetLineWidth(3); line.SetLineColor(2);
  TLatex latex2; latex2.SetTextSize(0.045); latex2.SetNDC();

  line.DrawLine(110, 1, 150, 1);
  if (label != "") latex2.DrawLatex(labelX, labelY, label);
  if (lumi > 0) latex2.DrawLatex(lumiX, lumiY, Form("#int L = %.1f fb^{-1}", lumi));
  
  
}    


void HftScanStudy::DrawP0(double yMin, double yMax, double xMin, double xMax, const TString& yTitle, 
                          const TString& expLegend, const TString& obsLegend, const TString& label, 
                          double lumi, unsigned int nSigmas, double lumiX, double lumiY, double labelX, double labelY)
{
  if (!Load()) return;
  HftBand expected = Expected()->Curve(0);
  HftBand observed = Observed()->Curve(0);
  

  TGraph** gExp = Expected()->Curve().DrawLine(yMin, yMax, 1, 2, "", 3, "expected");
  TGraph** gObs = Observed()->Curve().DrawLine(yMin, yMax, 1, 1, "", 3, "observed");

  if (xMin < xMax) gObs[0]->GetXaxis()->SetRangeUser(xMin, xMax);
  gObs[0]->GetYaxis()->SetTitle(yTitle);
  gObs[0]->GetYaxis()->SetTitleOffset(1);
  gObs[0]->Draw("AL");
  gExp[0]->Draw("LSAME");

  if (expLegend != "" || obsLegend != "") {
    TLegend* legend = new TLegend(0.18, 0.93, 0.55, 0.75, "","NDC");
    legend->SetBorderSize(0);
    legend->SetFillColor(0);
    legend->SetTextSize(0.035);
    legend->AddEntry(gObs[0], obsLegend, "l");
    legend->AddEntry(gExp[0], expLegend, "l");
    legend->Draw();
  }
  TLine line; 
  line.SetLineStyle(kDashDotted); line.SetLineWidth(3); line.SetLineColor(2);
  TLatex latex; latex.SetTextSize(0.04);

  for (unsigned int i = 1; i <= nSigmas; i++) {
    double prob = TMath::Prob(i*i,1)/2;
    line.DrawLine(110, prob, 150, prob);
    latex.DrawLatex(113, prob*1.1, Form("%d#sigma", i));
  }

  TLatex latex2; latex2.SetTextSize(0.045); latex2.SetNDC();
  if (label != "") latex2.DrawLatex(labelX, labelY, label);
  if (lumi > 0) latex2.DrawLatex(lumiX, lumiY, Form("#int L = %.1f fb^{-1}", lumi));
}    


void HftScanStudy::PrintTable(int nErrors, unsigned int nDigits)
{
  if (!Load()) return;
  HftBand expected = Expected()->Curve(nErrors);
  HftBand observed = Observed()->Curve(0);
  TString format = Form(" %%10.%df ", nDigits);
  unsigned int iExp = 0;
  for (unsigned int i = 0; i < observed.NPoints(); i++) {
    TString s = Form(" %5.1f " + format, observed.Position(i), observed.ErrorRange(i).Value(0));
    if (observed.Position(i) == expected.Position(iExp)) {
      s += Form(format, expected.ErrorRange(iExp).Value(0));
      for (int k = -nErrors; k <= nErrors; k++) {
        if (k == 0) continue;
        s += Form(format, expected.ErrorRange(iExp).Value(k));
      }
      iExp++;
    }
    cout << s << endl;
  }
}


bool HftScanStudy::Register(const TString& name, HftScanStudy& study)
{
  if (m_registry->find(name) != m_registry->end()) return false;
  (*m_registry)[name] = &study;
  return true;
}


HftScanStudy* HftScanStudy::Get(const TString& name)
{
  if (name == "") return (m_registry->size() == 0 ? 0 : m_registry->begin()->second);
  std::map<TString, HftScanStudy*>::iterator calc = m_registry->find(name);
  return (calc != m_registry->end() ? calc->second : 0);
}
