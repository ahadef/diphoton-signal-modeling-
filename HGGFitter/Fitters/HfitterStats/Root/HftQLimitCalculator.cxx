#include "HfitterStats/HftQLimitCalculator.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftBand.h"

#include "TMath.h"
#include "Math/ProbFuncMathCore.h"
#include "TLine.h"
#include "RooFitResult.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


bool HftQLimitCalculator::GetStatistic(double& q) const
{
  if (FreeFitValue(0) < CompHypoValue(0)) {
    if (!HftPLRCalculator::GetStatistic(q)) return false;
  }
  else 
    q = 0;
  return true;
}


bool HftQLimitCalculator::GetStatisticBand(int i, double& qBand) const
{ 
  double sigma = 0;
  if (!HftPLRCalculator::GetExpSigma(sigma)) return false;
  if (FreeFitValue(0) + i*sigma < CompHypoValue(0)) {
    if (FreeFitValue(0) + i*sigma < CompHypoValue(0)) { // nominal is in the same region: take difference wrt nominal
      if (!HftPLRCalculator::GetStatisticBand(i, qBand)) return false;
    }
    else { // nominal is ==0 : rely rather on asymptotic formula
      qBand = Sqr((CompHypoValue(0) - FreeFitValue(0) - i*sigma)/sigma);
    }
  }
  else
    qBand = 0;

  return true;
}


bool HftQLimitCalculator::ComputeFromAsymptotics(double q, double& cl) const
{
  double arg = 0;
  if (!HasSeparateCompHypo()) arg = SignedSqrt(q);
  else {
    double sigma;
    if (!GetExpSigma(sigma)) {
      cout << "ERROR : cannot get expected uncertainty, returning 0" << endl;
      cl = 0; return false;
    }
    arg = SignedSqrt(q) - (CompHypoValue(0) - HypoValue(0))/sigma;
  }
  cl = ResultIsCL() ? 
//   ROOT::Math::chisquared_cdf(arg, NHypoVars())/2 + 0.5 :
//   ROOT::Math::chisquared_cdf_c(arg, NHypoVars())/2;
//   njpb: comment for now: these asymptotics apply only to the 1D case
  ROOT::Math::normal_cdf(arg) :
  ROOT::Math::normal_cdf(-arg);
  return true;
}


TString HftQLimitCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = StatBaseStr(prefix, "HftQLimitCalculator");
  s += PLRStr(prefix + "  ", options);
  return s;
}
