#ifdef __CINT__

#include <vector>

#include "HfitterStats/HftInterval.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftScanPoint.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftMultiCalculator.h"
#include "HfitterStats/HftMaxFindingCalculator.h"
#include "HfitterStats/HftAbsReprocStudy.h"
#include "HfitterStats/HftScanPointCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "HfitterStats/HftValueCalculator.h"
#include "HfitterStats/HftFitValueCalculator.h"
#include "HfitterStats/HftLRCalculator.h"
#include "HfitterStats/HftDataCountingCalculator.h"
#include "HfitterStats/HftAbsStateCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftToysCalculator.h"

#include "HfitterStats/HftAbsStatCalculator.h"
#include "HfitterStats/HftAbsHypoTestCalculator.h"
#include "HfitterStats/HftExpHypoTestCalc.h"
#include "HfitterStats/HftScanIntervalCalculator.h"
#include "HfitterStats/HftIterIntervalCalculator.h"
#include "HfitterStats/HftMLCalculator.h"
#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftMultiHypoTestCalculator.h"
#include "HfitterStats/HftScanLimitCalculator.h"
#include "HfitterStats/HftIterLimitCalculator.h"
#include "HfitterStats/HftOneSidedPValueCalculator.h"
#include "HfitterStats/HftPLRPValueCalculator.h"
#include "HfitterStats/HftUncappedPValueCalculator.h"
#include "HfitterStats/HftDiscreteTestCalculator.h"
#include "HfitterStats/HftPLRLimitCalculator.h"
#include "HfitterStats/HftQLimitCalculator.h"
#include "HfitterStats/HftQTildaLimitCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"
#include "HfitterStats/HftScanStudy.h"
#include "HfitterStats/HftStatTools.h"

#include "HfitterStats/HftOneSidedP0Calculator.h"
#include "HfitterStats/HftUncappedP0Calculator.h"
#include "HfitterStats/HftCLsbQCalculator.h"
#include "HfitterStats/HftCLsbQTildaCalculator.h"
#include "HfitterStats/HftCLSolvingCalculator.h"

//#include "HfitterStats/HftOneSidedPoissonPValueCalculator.h"
//#include "HfitterStats/HftUncappedPoissonPValueCalculator.h"


#pragma link C++ namespace Hfitter;

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#pragma link C++ enum Hfitter::PValueType+;
#pragma link C++ enum Hfitter::ULType+;

#pragma link C++ class Hfitter::HftInterval+;
#pragma link C++ class std::vector<Hfitter::HftInterval>+;
#pragma link C++ class Hfitter::HftBand+;
#pragma link C++ class Hfitter::HftAbsCalculator+;
#pragma link C++ class std::vector<Hfitter::HftAbsCalculator*>+;
#pragma link C++ class Hfitter::HftAbsMultiResultCalculator+;
#pragma link C++ class Hfitter::HftTree+;
#pragma link C++ class Hfitter::HftScanPoint+;
#pragma link C++ class Hfitter::HftScanGrid+;
#pragma link C++ class Hfitter::HftMultiCalculator+;
#pragma link C++ class Hfitter::HftMaxFindingCalculator+;
#pragma link C++ class Hfitter::HftAbsReprocStudy+;
#pragma link C++ class Hfitter::HftScanPointCalculator+;
#pragma link C++ class Hfitter::HftScanningCalculator+;
#pragma link C++ class Hfitter::HftValueCalculator+;
#pragma link C++ class Hfitter::HftFitValueCalculator+;
#pragma link C++ class Hfitter::HftLRCalculator+;
#pragma link C++ class Hfitter::HftDataCountingCalculator+;
#pragma link C++ class Hfitter::HftAbsStateCalculator+;
#pragma link C++ class Hfitter::HftAsimovCalculator+;
#pragma link C++ class Hfitter::HftToysCalculator+;

#pragma link C++ class Hfitter::HftAbsStatCalculator+;
#pragma link C++ class Hfitter::HftAbsCLCalculator+;
#pragma link C++ class Hfitter::HftAbsHypoTestCalculator+;
#pragma link C++ class Hfitter::HftExpHypoTestCalc+;
#pragma link C++ class Hfitter::HftScanIntervalCalculator+;
#pragma link C++ class Hfitter::HftIterIntervalCalculator+;
#pragma link C++ class std::vector<Hfitter::HftAbsHypoTestCalculator*>+;
#pragma link C++ class Hfitter::HftMLCalculator+;
#pragma link C++ class Hfitter::HftPLRCalculator+;
#pragma link C++ class Hfitter::HftMultiHypoTestCalculator+;
#pragma link C++ class std::vector<Hfitter::HftMultiHypoTestCalculator*>+;
#pragma link C++ class Hfitter::HftScanLimitCalculator+;
#pragma link C++ class Hfitter::HftIterLimitCalculator+;
#pragma link C++ class Hfitter::HftOneSidedPValueCalculator+;
#pragma link C++ class Hfitter::HftPLRPValueCalculator+;
#pragma link C++ class Hfitter::HftUncappedPValueCalculator+;
#pragma link C++ class Hfitter::HftDiscreteTestCalculator+;
#pragma link C++ class Hfitter::HftAbsCLsbCalculator+;
#pragma link C++ class std::vector<Hfitter::HftAbsCLsbCalculator*>+;
#pragma link C++ class Hfitter::HftPLRLimitCalculator+;
#pragma link C++ class Hfitter::HftQLimitCalculator+;
#pragma link C++ class Hfitter::HftQTildaLimitCalculator+;
#pragma link C++ class Hfitter::HftCLsCalculator+;
#pragma link C++ class Hfitter::HftScanStudy+;
#pragma link C++ class Hfitter::HftStatTools+;

#pragma link C++ typedef Hfitter::HftCLsbQCalculator+;
#pragma link C++ typedef Hfitter::HftCLsbQTildaCalculator+;
#pragma link C++ typedef Hfitter::HftOneSidedP0Calculator++;
#pragma link C++ typedef Hfitter::HftUncappedP0Calculator++;
#pragma link C++ typedef Hfitter::HftCLSolvingCalculator+;

//#pragma link C++ class Hfitter::HftOneSidedPoissonPValueCalculator+;
//#pragma link C++ class Hfitter::HftUncappedPoissonPValueCalculator+;

#endif
