#include "HfitterStats/HftAbsCalculator.h"

#include "HfitterStats/HftTree.h"
#include "TMath.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

std::map<TString, HftAbsCalculator*>* HftAbsCalculator::m_calculatorRegistry = new std::map<TString, HftAbsCalculator*>();

HftAbsCalculator::~HftAbsCalculator()
{
  DeregisterCalc(*this);
}


bool HftAbsCalculator::LoadFromFile(const TString& fileName, const TString& treeName)
{
  HftTree* tree = HftTree::Open(treeName, fileName);
  if (!tree) {
    cout << "ERROR: cannot access file " << fileName << endl;
    return false;
  }
  if (!MakeBranches(*tree)) return false;
  tree->GetEntry(0);
  if (!LoadInputs(*tree)) return false;
  delete tree;
  return true;
}


bool HftAbsCalculator::SaveToFile(const TString& fileName, const TString& treeName)
{
  HftTree* tree = new HftTree(treeName, fileName);
  if (!tree) {
    cout << "ERROR: cannot access file " << fileName << endl;
    return false;
  }
  if (!MakeBranches(*tree) || !FillBranches(*tree)) {
    delete tree; 
    return false;
  }
  tree->Fill();
  delete tree;
  return true;
}


bool HftAbsCalculator::LoadInputs(RooAbsData& data, const TString& fileName, const TString& treeName)
{
  if (LoadFromFile(fileName, treeName)) return true;
  return (LoadInputs(data) && SaveToFile(fileName, treeName));
}


void HftAbsCalculator::Print(const TString& options) const
{
  cout << Str("", options) << endl;
}


TString HftAbsCalculator::BaseStr(const TString& prefix, const TString& typeStr) const
{ 
  return prefix + typeStr + ": " + GetName() + " (" + ResultName() + " = " + Form("%g", Result()) + ")";
}


bool HftAbsCalculator::RegisterCalc(const TString& name, HftAbsCalculator& calc)
{
///  cout << "registering " << name << " " << &calc << endl;
//   if (m_calculatorRegistry->find(name) != m_calculatorRegistry->end())
//     cout << "WARNING: registering calculator with existing name " << name << endl;
  (*m_calculatorRegistry)[name] = &calc;
  return true;
}


bool HftAbsCalculator::DeregisterCalc(HftAbsCalculator& calc)
{
  for (std::map<TString, HftAbsCalculator*>::const_iterator entry = m_calculatorRegistry->begin(); entry != m_calculatorRegistry->end();
       entry++)
    if (entry->second == &calc) m_calculatorRegistry->erase(entry);
  return true;
}


HftAbsCalculator* HftAbsCalculator::GetCalc(const TString& name, bool verbose)
{
  std::map<TString, HftAbsCalculator*>::iterator calc = m_calculatorRegistry->find(name);
  if (verbose && calc == m_calculatorRegistry->end()) {
    cout << "List of known calculators:" << endl;
    for (std::map<TString, HftAbsCalculator*>::iterator calc = m_calculatorRegistry->begin(); calc != m_calculatorRegistry->end(); calc++)
      cout << calc->first << endl;
  }
  return (calc != m_calculatorRegistry->end() ? calc->second : 0);
}


void HftAbsCalculator::PrintCalcs(const TString& options)
{
  unsigned int i = 0;
  for (std::map<TString, HftAbsCalculator*>::const_iterator calc = m_calculatorRegistry->begin(); calc != m_calculatorRegistry->end();
       calc++, i++)
    cout << calc->second->Str(Form("# %3d", i+1), options) << endl;
}


TString HftAbsCalculator::MergeNames(const TString& name1, const TString& name2)
{
  if (name1 == "") return name2;
  if (name2 == "") return name1;
  return name1 + "_" + name2;
}


void HftAbsCalculator::SetVerbosity(int)
{
  cout << "WARNING: SetVerbosity() not implemented for this calculator class" << endl;
}