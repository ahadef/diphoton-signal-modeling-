#include "HfitterStats/HftDiscreteTestCalculator.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftAsimovCalculator.h"

#include "HfitterStats/HftMultiHypoTestCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"

#include "Math/ProbFuncMathCore.h"
#include "TMath.h"
#include "TLine.h"
#include "RooFitResult.h"
#include "RooNLLVar.h"
#include "RooWorkspace.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftDiscreteTestCalculator::HftDiscreteTestCalculator(HftModel& model, const std::vector<HftPLRCalculator*> calcs, const TString& name)
  : HftAbsHypoTestCalculator(model, calcs[0]->Hypo(), true, name, false), m_whichResult(-1)
{
  SetResultIsStatistic(true);
  for (unsigned int i = 0; i < calcs.size(); i++) {
    m_calculators.push_back((HftPLRCalculator*)calcs[i]->Clone());
    m_calculators[i]->SetResultIsStatistic(true);
  }
}


HftDiscreteTestCalculator::~HftDiscreteTestCalculator()
{
  for (unsigned int i = 0; i < m_calculators.size(); i++) delete m_calculators[i];
}


HftAbsHypoTestCalculator* HftDiscreteTestCalculator::CloneHypo(const HftScanPoint& hypo, const TString& name, const HftAbsCalculator* cloner) const
{ 
  TString newName = (name == "" ? GetName() : name);
  std::vector<HftPLRCalculator*> calcs;
  for (unsigned int i = 0; i < m_calculators.size(); i++)
    calcs.push_back((HftPLRCalculator*)m_calculators[i]->CloneHypo(hypo, m_calculators[i]->GetName(), cloner));

  return new HftDiscreteTestCalculator(Model(), calcs, newName);
}


bool HftDiscreteTestCalculator::MakeAltHypoBranches(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->MakeAltHypoBranches(tree)) return false;
  return true;
}


bool HftDiscreteTestCalculator::MakeNullHypoBranches(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->MakeNullHypoBranches(tree)) return false;
  return true;
}


bool HftDiscreteTestCalculator::FillAltHypoBranches(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->FillAltHypoBranches(tree)) return false;
  return true;
}


bool HftDiscreteTestCalculator::FillNullHypoBranches(HftTree& tree)
{ 
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->FillNullHypoBranches(tree)) return false;
  return true;
}


bool HftDiscreteTestCalculator::LoadAltHypoInputs(RooAbsData& data)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadAltHypoInputs(data)) return false;
  return true;
}


bool HftDiscreteTestCalculator::LoadNullHypoInputs(RooAbsData& data)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadNullHypoInputs(data)) return false;
  return true;
}


bool HftDiscreteTestCalculator::LoadAltHypoInputs(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadAltHypoInputs(tree)) return false;
  return true;
}


bool HftDiscreteTestCalculator::LoadNullHypoInputs(HftTree& tree)
{
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadNullHypoInputs(tree)) return false;
  return true;
}


bool HftDiscreteTestCalculator::LoadAltHypoInputs(const HftAbsCalculator& other)
{
  const HftDiscreteTestCalculator* otherDiscCL = dynamic_cast<const HftDiscreteTestCalculator*>(&other);
  if (!otherDiscCL) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftDiscreteTestCalculator" << endl;
    return false;
  }
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadAltHypoInputs(*otherDiscCL->Calculator(i))) return false;
  return true;
}


bool HftDiscreteTestCalculator::LoadNullHypoInputs(const HftAbsCalculator& other)
{
  const HftDiscreteTestCalculator* otherDiscCL = dynamic_cast<const HftDiscreteTestCalculator*>(&other);
  if (!otherDiscCL) {
    cout << "ERROR : cannot load from a calculator that does not derive from HftDiscreteTestCalculator" << endl;
    return false;
  }
  for (unsigned int i = 0; i < NCalculators(); i++)
    if (!Calculator(i)->LoadNullHypoInputs(*otherDiscCL->Calculator(i))) return false;
  return true;
}


HftPLRCalculator* HftDiscreteTestCalculator::AltHypoCalculator() const
{
  double minNLL = DBL_MAX, result = 0;
  HftPLRCalculator* minCalc = 0;
  for (unsigned int i = 0; i < NCalculators(); i++) {
    if (!Calculator(i)->GetResult(result)) return 0;
    if (Calculator(i)->FreeCalc()->Result() < minNLL) {
      minNLL = Calculator(i)->FreeCalc()->Result();
      minCalc = Calculator(i);
    }
  }
  return minCalc;
}


HftPLRCalculator* HftDiscreteTestCalculator::NullHypoCalculator() const
{
  double minNLL = DBL_MAX, result = 0;
  HftPLRCalculator* minCalc = 0;
  for (unsigned int i = 0; i < NCalculators(); i++) {
    if (!Calculator(i)->GetResult(result)) return 0;
    if (Calculator(i)->HypoCalc()->Result() < minNLL) {
      minNLL = Calculator(i)->HypoCalc()->Result();
      minCalc = Calculator(i);
    }
  }
  return minCalc;
}


bool HftDiscreteTestCalculator::GetStatistic(double& q) const
{ 
  HftPLRCalculator* globalProf = AltHypoCalculator();
  if (!globalProf) return false;
  if (WhichResult() >= 0) {
    q = -2*(globalProf->FreeCalc()->Result() - Calculator((unsigned int)m_whichResult)->HypoCalc()->Result());
    return true;
  }
  HftPLRCalculator* hypoProf = NullHypoCalculator();
  if (!hypoProf) return false;
  q = -2*(globalProf->FreeCalc()->Result() - hypoProf->HypoCalc()->Result());
  return true;
}


TString HftDiscreteTestCalculator::Str(const TString& prefix, const TString& options) const
{
  TString s = BaseStr(prefix, "HftDiscreteTestCalculator");
  if (options.Contains("V")) s += Form("\naltHypo  NLL = %.5f", AltHypoCalculator()->FreeCalc()->Result());
  if (options.Contains("V")) s += Form("\nnullHypo NLL = %.5f, hypoDll = %.5f", NullHypoCalculator()->HypoCalc()->Result(), Statistic());
  for (unsigned int i = 0; i < NCalculators(); i++)
    s += "\n" + Calculator(i)->Str(prefix + "   ", options);
  return s;
}


bool HftDiscreteTestCalculator::SetWhichResult(HftMultiHypoTestCalculator& mhc, int wr)
{
  for (unsigned int i = 0; i < mhc.NPoints(); i++) {
    HftDiscreteTestCalculator* dc = dynamic_cast<HftDiscreteTestCalculator*>(mhc.HypoCalculator(i));
    if (!dc) return false;
    dc->SetWhichResult(wr);
  }
  return true;
}
