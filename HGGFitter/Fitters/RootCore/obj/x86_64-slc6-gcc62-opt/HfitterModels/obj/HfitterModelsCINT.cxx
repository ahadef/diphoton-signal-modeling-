#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIadIahadefdIpublicdIHGGFitterdIFittersdIRootCoredIobjdIx86_64mIslc6mIgcc62mIoptdIHfitterModelsdIobjdIHfitterModelsCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "HfitterModels/Options.h"
#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftStorableParameters.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/HftRealMorphVar.h"
#include "HfitterModels/HftDatacardReader.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftConstraint.h"
#include "HfitterModels/HftInstance.h"
#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HfitterModels/HftAbsNodeBuilder.h"
#include "HfitterModels/HftAbsRealBuilder.h"
#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftModelSplitter.h"
#include "HfitterModels/HftData.h"
#include "HfitterModels/HftDataHist.h"
#include "HfitterModels/HftCountingPdfBuilder.h"
#include "HfitterModels/HftHistPdfBuilder.h"
#include "HfitterModels/HftFactoryPdfBuilder.h"
#include "HfitterModels/HftWorkspacePdfBuilder.h"
#include "HfitterModels/HftGenericRealBuilder.h"
#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "HfitterModels/HftGenericPdfBuilder.h"
#include "HfitterModels/HftMultiGaussianBuilder.h"
#include "HfitterModels/HftPolyPdfBuilder.h"
#include "HfitterModels/HftDoubleGaussian.h"
#include "HfitterModels/HftPolyExp.h"
#include "HfitterModels/HftFermiPolyExp.h"
#include "HfitterModels/HftDeltaPdf.h"
#include "HfitterModels/HftPeggedPoly.h"
#include "HfitterModels/HftTruncPoly.h"
#include "HfitterModels/FlexibleInterpVarMkII.h"
#include "HfitterModels/HftSPlot.h"
#include "HfitterModels/HftModelBuilderV4.h"
#include "HfitterModels/HftDatacardReaderV4.h"
#include "HfitterModels/HftModelSplitterV4.h"

// Header files passed via #pragma extra_include

namespace Hfitter {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *Hfitter_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("Hfitter", 0 /*version*/, "HfitterModels/Options.h", 13,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &Hfitter_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *Hfitter_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *HfittercLcLHftAbsOption_Dictionary();
   static void HfittercLcLHftAbsOption_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsOption(void *p);
   static void deleteArray_HfittercLcLHftAbsOption(void *p);
   static void destruct_HfittercLcLHftAbsOption(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsOption*)
   {
      ::Hfitter::HftAbsOption *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsOption));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsOption", "HfitterModels/Options.h", 22,
                  typeid(::Hfitter::HftAbsOption), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsOption_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsOption) );
      instance.SetDelete(&delete_HfittercLcLHftAbsOption);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsOption);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsOption);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsOption*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsOption*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsOption*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsOption_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsOption*)0x0)->GetClass();
      HfittercLcLHftAbsOption_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsOption_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLRooFitOpt_Dictionary();
   static void HfittercLcLRooFitOpt_TClassManip(TClass*);
   static void delete_HfittercLcLRooFitOpt(void *p);
   static void deleteArray_HfittercLcLRooFitOpt(void *p);
   static void destruct_HfittercLcLRooFitOpt(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::RooFitOpt*)
   {
      ::Hfitter::RooFitOpt *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::RooFitOpt));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::RooFitOpt", "HfitterModels/Options.h", 33,
                  typeid(::Hfitter::RooFitOpt), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLRooFitOpt_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::RooFitOpt) );
      instance.SetDelete(&delete_HfittercLcLRooFitOpt);
      instance.SetDeleteArray(&deleteArray_HfittercLcLRooFitOpt);
      instance.SetDestructor(&destruct_HfittercLcLRooFitOpt);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::RooFitOpt*)
   {
      return GenerateInitInstanceLocal((::Hfitter::RooFitOpt*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::RooFitOpt*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLRooFitOpt_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::RooFitOpt*)0x0)->GetClass();
      HfittercLcLRooFitOpt_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLRooFitOpt_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLStrOpt_Dictionary();
   static void HfittercLcLStrOpt_TClassManip(TClass*);
   static void delete_HfittercLcLStrOpt(void *p);
   static void deleteArray_HfittercLcLStrOpt(void *p);
   static void destruct_HfittercLcLStrOpt(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::StrOpt*)
   {
      ::Hfitter::StrOpt *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::StrOpt));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::StrOpt", "HfitterModels/Options.h", 46,
                  typeid(::Hfitter::StrOpt), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLStrOpt_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::StrOpt) );
      instance.SetDelete(&delete_HfittercLcLStrOpt);
      instance.SetDeleteArray(&deleteArray_HfittercLcLStrOpt);
      instance.SetDestructor(&destruct_HfittercLcLStrOpt);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::StrOpt*)
   {
      return GenerateInitInstanceLocal((::Hfitter::StrOpt*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::StrOpt*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLStrOpt_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::StrOpt*)0x0)->GetClass();
      HfittercLcLStrOpt_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLStrOpt_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLNoDefaults_Dictionary();
   static void HfittercLcLNoDefaults_TClassManip(TClass*);
   static void *new_HfittercLcLNoDefaults(void *p = 0);
   static void *newArray_HfittercLcLNoDefaults(Long_t size, void *p);
   static void delete_HfittercLcLNoDefaults(void *p);
   static void deleteArray_HfittercLcLNoDefaults(void *p);
   static void destruct_HfittercLcLNoDefaults(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::NoDefaults*)
   {
      ::Hfitter::NoDefaults *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::NoDefaults));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::NoDefaults", "HfitterModels/Options.h", 134,
                  typeid(::Hfitter::NoDefaults), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLNoDefaults_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::NoDefaults) );
      instance.SetNew(&new_HfittercLcLNoDefaults);
      instance.SetNewArray(&newArray_HfittercLcLNoDefaults);
      instance.SetDelete(&delete_HfittercLcLNoDefaults);
      instance.SetDeleteArray(&deleteArray_HfittercLcLNoDefaults);
      instance.SetDestructor(&destruct_HfittercLcLNoDefaults);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::NoDefaults*)
   {
      return GenerateInitInstanceLocal((::Hfitter::NoDefaults*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::NoDefaults*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLNoDefaults_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::NoDefaults*)0x0)->GetClass();
      HfittercLcLNoDefaults_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLNoDefaults_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLOptions_Dictionary();
   static void HfittercLcLOptions_TClassManip(TClass*);
   static void *new_HfittercLcLOptions(void *p = 0);
   static void *newArray_HfittercLcLOptions(Long_t size, void *p);
   static void delete_HfittercLcLOptions(void *p);
   static void deleteArray_HfittercLcLOptions(void *p);
   static void destruct_HfittercLcLOptions(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::Options*)
   {
      ::Hfitter::Options *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::Options));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::Options", "HfitterModels/Options.h", 144,
                  typeid(::Hfitter::Options), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLOptions_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::Options) );
      instance.SetNew(&new_HfittercLcLOptions);
      instance.SetNewArray(&newArray_HfittercLcLOptions);
      instance.SetDelete(&delete_HfittercLcLOptions);
      instance.SetDeleteArray(&deleteArray_HfittercLcLOptions);
      instance.SetDestructor(&destruct_HfittercLcLOptions);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::Options*)
   {
      return GenerateInitInstanceLocal((::Hfitter::Options*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::Options*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLOptions_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::Options*)0x0)->GetClass();
      HfittercLcLOptions_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLOptions_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsParameters_Dictionary();
   static void HfittercLcLHftAbsParameters_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsParameters(void *p);
   static void deleteArray_HfittercLcLHftAbsParameters(void *p);
   static void destruct_HfittercLcLHftAbsParameters(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsParameters*)
   {
      ::Hfitter::HftAbsParameters *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsParameters));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsParameters", "HfitterModels/HftAbsParameters.h", 27,
                  typeid(::Hfitter::HftAbsParameters), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsParameters_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsParameters) );
      instance.SetDelete(&delete_HfittercLcLHftAbsParameters);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsParameters);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsParameters);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsParameters*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsParameters*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsParameters*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsParameters_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsParameters*)0x0)->GetClass();
      HfittercLcLHftAbsParameters_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsParameters_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftParameterStorage_Dictionary();
   static void HfittercLcLHftParameterStorage_TClassManip(TClass*);
   static void *new_HfittercLcLHftParameterStorage(void *p = 0);
   static void *newArray_HfittercLcLHftParameterStorage(Long_t size, void *p);
   static void delete_HfittercLcLHftParameterStorage(void *p);
   static void deleteArray_HfittercLcLHftParameterStorage(void *p);
   static void destruct_HfittercLcLHftParameterStorage(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftParameterStorage*)
   {
      ::Hfitter::HftParameterStorage *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftParameterStorage));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftParameterStorage", "HfitterModels/HftParameterStorage.h", 31,
                  typeid(::Hfitter::HftParameterStorage), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftParameterStorage_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftParameterStorage) );
      instance.SetNew(&new_HfittercLcLHftParameterStorage);
      instance.SetNewArray(&newArray_HfittercLcLHftParameterStorage);
      instance.SetDelete(&delete_HfittercLcLHftParameterStorage);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftParameterStorage);
      instance.SetDestructor(&destruct_HfittercLcLHftParameterStorage);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftParameterStorage*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftParameterStorage*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftParameterStorage*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftParameterStorage_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftParameterStorage*)0x0)->GetClass();
      HfittercLcLHftParameterStorage_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftParameterStorage_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftStorableParameters_Dictionary();
   static void HfittercLcLHftStorableParameters_TClassManip(TClass*);
   static void delete_HfittercLcLHftStorableParameters(void *p);
   static void deleteArray_HfittercLcLHftStorableParameters(void *p);
   static void destruct_HfittercLcLHftStorableParameters(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftStorableParameters*)
   {
      ::Hfitter::HftStorableParameters *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftStorableParameters));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftStorableParameters", "HfitterModels/HftStorableParameters.h", 20,
                  typeid(::Hfitter::HftStorableParameters), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftStorableParameters_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftStorableParameters) );
      instance.SetDelete(&delete_HfittercLcLHftStorableParameters);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftStorableParameters);
      instance.SetDestructor(&destruct_HfittercLcLHftStorableParameters);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftStorableParameters*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftStorableParameters*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftStorableParameters*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftStorableParameters_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftStorableParameters*)0x0)->GetClass();
      HfittercLcLHftStorableParameters_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftStorableParameters_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftParameterSet_Dictionary();
   static void HfittercLcLHftParameterSet_TClassManip(TClass*);
   static void *new_HfittercLcLHftParameterSet(void *p = 0);
   static void *newArray_HfittercLcLHftParameterSet(Long_t size, void *p);
   static void delete_HfittercLcLHftParameterSet(void *p);
   static void deleteArray_HfittercLcLHftParameterSet(void *p);
   static void destruct_HfittercLcLHftParameterSet(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftParameterSet*)
   {
      ::Hfitter::HftParameterSet *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftParameterSet));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftParameterSet", "HfitterModels/HftParameterSet.h", 16,
                  typeid(::Hfitter::HftParameterSet), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftParameterSet_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftParameterSet) );
      instance.SetNew(&new_HfittercLcLHftParameterSet);
      instance.SetNewArray(&newArray_HfittercLcLHftParameterSet);
      instance.SetDelete(&delete_HfittercLcLHftParameterSet);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftParameterSet);
      instance.SetDestructor(&destruct_HfittercLcLHftParameterSet);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftParameterSet*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftParameterSet*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftParameterSet*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftParameterSet_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftParameterSet*)0x0)->GetClass();
      HfittercLcLHftParameterSet_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftParameterSet_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftRealMorphVar(void *p = 0);
   static void *newArray_HfittercLcLHftRealMorphVar(Long_t size, void *p);
   static void delete_HfittercLcLHftRealMorphVar(void *p);
   static void deleteArray_HfittercLcLHftRealMorphVar(void *p);
   static void destruct_HfittercLcLHftRealMorphVar(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftRealMorphVar*)
   {
      ::Hfitter::HftRealMorphVar *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftRealMorphVar >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftRealMorphVar", ::Hfitter::HftRealMorphVar::Class_Version(), "HfitterModels/HftRealMorphVar.h", 20,
                  typeid(::Hfitter::HftRealMorphVar), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftRealMorphVar::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftRealMorphVar) );
      instance.SetNew(&new_HfittercLcLHftRealMorphVar);
      instance.SetNewArray(&newArray_HfittercLcLHftRealMorphVar);
      instance.SetDelete(&delete_HfittercLcLHftRealMorphVar);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftRealMorphVar);
      instance.SetDestructor(&destruct_HfittercLcLHftRealMorphVar);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftRealMorphVar*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftRealMorphVar*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftRealMorphVar*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftConstraint_Dictionary();
   static void HfittercLcLHftConstraint_TClassManip(TClass*);
   static void delete_HfittercLcLHftConstraint(void *p);
   static void deleteArray_HfittercLcLHftConstraint(void *p);
   static void destruct_HfittercLcLHftConstraint(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftConstraint*)
   {
      ::Hfitter::HftConstraint *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftConstraint));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftConstraint", "HfitterModels/HftConstraint.h", 13,
                  typeid(::Hfitter::HftConstraint), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftConstraint_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftConstraint) );
      instance.SetDelete(&delete_HfittercLcLHftConstraint);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftConstraint);
      instance.SetDestructor(&destruct_HfittercLcLHftConstraint);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftConstraint*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftConstraint*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftConstraint*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftConstraint_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftConstraint*)0x0)->GetClass();
      HfittercLcLHftConstraint_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftConstraint_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftModel_Dictionary();
   static void HfittercLcLHftModel_TClassManip(TClass*);
   static void delete_HfittercLcLHftModel(void *p);
   static void deleteArray_HfittercLcLHftModel(void *p);
   static void destruct_HfittercLcLHftModel(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftModel*)
   {
      ::Hfitter::HftModel *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftModel));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftModel", "HfitterModels/HftModel.h", 21,
                  typeid(::Hfitter::HftModel), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftModel_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftModel) );
      instance.SetDelete(&delete_HfittercLcLHftModel);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftModel);
      instance.SetDestructor(&destruct_HfittercLcLHftModel);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftModel*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftModel*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftModel*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftModel_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModel*)0x0)->GetClass();
      HfittercLcLHftModel_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftModel_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftPdfBuilderSpec_Dictionary();
   static void HfittercLcLHftPdfBuilderSpec_TClassManip(TClass*);
   static void *new_HfittercLcLHftPdfBuilderSpec(void *p = 0);
   static void *newArray_HfittercLcLHftPdfBuilderSpec(Long_t size, void *p);
   static void delete_HfittercLcLHftPdfBuilderSpec(void *p);
   static void deleteArray_HfittercLcLHftPdfBuilderSpec(void *p);
   static void destruct_HfittercLcLHftPdfBuilderSpec(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPdfBuilderSpec*)
   {
      ::Hfitter::HftPdfBuilderSpec *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftPdfBuilderSpec));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPdfBuilderSpec", "HfitterModels/HftDatacardReader.h", 25,
                  typeid(::Hfitter::HftPdfBuilderSpec), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftPdfBuilderSpec_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPdfBuilderSpec) );
      instance.SetNew(&new_HfittercLcLHftPdfBuilderSpec);
      instance.SetNewArray(&newArray_HfittercLcLHftPdfBuilderSpec);
      instance.SetDelete(&delete_HfittercLcLHftPdfBuilderSpec);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPdfBuilderSpec);
      instance.SetDestructor(&destruct_HfittercLcLHftPdfBuilderSpec);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPdfBuilderSpec*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPdfBuilderSpec*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPdfBuilderSpec*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftPdfBuilderSpec_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPdfBuilderSpec*)0x0)->GetClass();
      HfittercLcLHftPdfBuilderSpec_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftPdfBuilderSpec_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftFunctionBuilderSpec_Dictionary();
   static void HfittercLcLHftFunctionBuilderSpec_TClassManip(TClass*);
   static void *new_HfittercLcLHftFunctionBuilderSpec(void *p = 0);
   static void *newArray_HfittercLcLHftFunctionBuilderSpec(Long_t size, void *p);
   static void delete_HfittercLcLHftFunctionBuilderSpec(void *p);
   static void deleteArray_HfittercLcLHftFunctionBuilderSpec(void *p);
   static void destruct_HfittercLcLHftFunctionBuilderSpec(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftFunctionBuilderSpec*)
   {
      ::Hfitter::HftFunctionBuilderSpec *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftFunctionBuilderSpec));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftFunctionBuilderSpec", "HfitterModels/HftDatacardReader.h", 32,
                  typeid(::Hfitter::HftFunctionBuilderSpec), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftFunctionBuilderSpec_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftFunctionBuilderSpec) );
      instance.SetNew(&new_HfittercLcLHftFunctionBuilderSpec);
      instance.SetNewArray(&newArray_HfittercLcLHftFunctionBuilderSpec);
      instance.SetDelete(&delete_HfittercLcLHftFunctionBuilderSpec);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftFunctionBuilderSpec);
      instance.SetDestructor(&destruct_HfittercLcLHftFunctionBuilderSpec);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftFunctionBuilderSpec*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftFunctionBuilderSpec*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftFunctionBuilderSpec*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftFunctionBuilderSpec_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFunctionBuilderSpec*)0x0)->GetClass();
      HfittercLcLHftFunctionBuilderSpec_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftFunctionBuilderSpec_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftDatacardReader_Dictionary();
   static void HfittercLcLHftDatacardReader_TClassManip(TClass*);
   static void *new_HfittercLcLHftDatacardReader(void *p = 0);
   static void *newArray_HfittercLcLHftDatacardReader(Long_t size, void *p);
   static void delete_HfittercLcLHftDatacardReader(void *p);
   static void deleteArray_HfittercLcLHftDatacardReader(void *p);
   static void destruct_HfittercLcLHftDatacardReader(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDatacardReader*)
   {
      ::Hfitter::HftDatacardReader *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftDatacardReader));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDatacardReader", "HfitterModels/HftDatacardReader.h", 62,
                  typeid(::Hfitter::HftDatacardReader), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftDatacardReader_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDatacardReader) );
      instance.SetNew(&new_HfittercLcLHftDatacardReader);
      instance.SetNewArray(&newArray_HfittercLcLHftDatacardReader);
      instance.SetDelete(&delete_HfittercLcLHftDatacardReader);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDatacardReader);
      instance.SetDestructor(&destruct_HfittercLcLHftDatacardReader);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDatacardReader*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDatacardReader*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDatacardReader*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftDatacardReader_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDatacardReader*)0x0)->GetClass();
      HfittercLcLHftDatacardReader_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftDatacardReader_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftInstance(void *p = 0);
   static void *newArray_HfittercLcLHftInstance(Long_t size, void *p);
   static void delete_HfittercLcLHftInstance(void *p);
   static void deleteArray_HfittercLcLHftInstance(void *p);
   static void destruct_HfittercLcLHftInstance(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftInstance*)
   {
      ::Hfitter::HftInstance *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftInstance >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftInstance", ::Hfitter::HftInstance::Class_Version(), "HfitterModels/HftInstance.h", 16,
                  typeid(::Hfitter::HftInstance), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftInstance::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftInstance) );
      instance.SetNew(&new_HfittercLcLHftInstance);
      instance.SetNewArray(&newArray_HfittercLcLHftInstance);
      instance.SetDelete(&delete_HfittercLcLHftInstance);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftInstance);
      instance.SetDestructor(&destruct_HfittercLcLHftInstance);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftInstance*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftInstance*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftInstance*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftAbsNodeBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftAbsNodeBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftAbsNodeBuilder(void *p);
   static void deleteArray_HfittercLcLHftAbsNodeBuilder(void *p);
   static void destruct_HfittercLcLHftAbsNodeBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsNodeBuilder*)
   {
      ::Hfitter::HftAbsNodeBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftAbsNodeBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsNodeBuilder", ::Hfitter::HftAbsNodeBuilder::Class_Version(), "HfitterModels/HftAbsNodeBuilder.h", 23,
                  typeid(::Hfitter::HftAbsNodeBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftAbsNodeBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsNodeBuilder) );
      instance.SetNew(&new_HfittercLcLHftAbsNodeBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftAbsNodeBuilder);
      instance.SetDelete(&delete_HfittercLcLHftAbsNodeBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsNodeBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsNodeBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsNodeBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsNodeBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsNodeBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftAbsPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftAbsPdfBuilder(void *p);
   static void destruct_HfittercLcLHftAbsPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsPdfBuilder*)
   {
      ::Hfitter::HftAbsPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftAbsPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsPdfBuilder", ::Hfitter::HftAbsPdfBuilder::Class_Version(), "HfitterModels/HftAbsPdfBuilder.h", 27,
                  typeid(::Hfitter::HftAbsPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftAbsPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsPdfBuilder) );
      instance.SetDelete(&delete_HfittercLcLHftAbsPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftAbsRealBuilder(void *p);
   static void deleteArray_HfittercLcLHftAbsRealBuilder(void *p);
   static void destruct_HfittercLcLHftAbsRealBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsRealBuilder*)
   {
      ::Hfitter::HftAbsRealBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftAbsRealBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsRealBuilder", ::Hfitter::HftAbsRealBuilder::Class_Version(), "HfitterModels/HftAbsRealBuilder.h", 16,
                  typeid(::Hfitter::HftAbsRealBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftAbsRealBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsRealBuilder) );
      instance.SetDelete(&delete_HfittercLcLHftAbsRealBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsRealBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsRealBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsRealBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsRealBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsRealBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftModelBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftModelBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftModelBuilder(void *p);
   static void deleteArray_HfittercLcLHftModelBuilder(void *p);
   static void destruct_HfittercLcLHftModelBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftModelBuilder*)
   {
      ::Hfitter::HftModelBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftModelBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftModelBuilder", ::Hfitter::HftModelBuilder::Class_Version(), "HfitterModels/HftModelBuilder.h", 30,
                  typeid(::Hfitter::HftModelBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftModelBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftModelBuilder) );
      instance.SetNew(&new_HfittercLcLHftModelBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftModelBuilder);
      instance.SetDelete(&delete_HfittercLcLHftModelBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftModelBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftModelBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftModelBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftModelBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftModelSplitter(void *p);
   static void deleteArray_HfittercLcLHftModelSplitter(void *p);
   static void destruct_HfittercLcLHftModelSplitter(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftModelSplitter*)
   {
      ::Hfitter::HftModelSplitter *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftModelSplitter >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftModelSplitter", ::Hfitter::HftModelSplitter::Class_Version(), "HfitterModels/HftModelSplitter.h", 26,
                  typeid(::Hfitter::HftModelSplitter), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftModelSplitter::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftModelSplitter) );
      instance.SetDelete(&delete_HfittercLcLHftModelSplitter);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftModelSplitter);
      instance.SetDestructor(&destruct_HfittercLcLHftModelSplitter);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftModelSplitter*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftModelSplitter*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitter*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftData(void *p);
   static void deleteArray_HfittercLcLHftData(void *p);
   static void destruct_HfittercLcLHftData(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftData*)
   {
      ::Hfitter::HftData *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftData >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftData", ::Hfitter::HftData::Class_Version(), "HfitterModels/HftData.h", 21,
                  typeid(::Hfitter::HftData), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftData::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftData) );
      instance.SetDelete(&delete_HfittercLcLHftData);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftData);
      instance.SetDestructor(&destruct_HfittercLcLHftData);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftData*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftData*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftData*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftDataHist(void *p);
   static void deleteArray_HfittercLcLHftDataHist(void *p);
   static void destruct_HfittercLcLHftDataHist(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDataHist*)
   {
      ::Hfitter::HftDataHist *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftDataHist >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDataHist", ::Hfitter::HftDataHist::Class_Version(), "HfitterModels/HftDataHist.h", 21,
                  typeid(::Hfitter::HftDataHist), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftDataHist::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDataHist) );
      instance.SetDelete(&delete_HfittercLcLHftDataHist);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDataHist);
      instance.SetDestructor(&destruct_HfittercLcLHftDataHist);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDataHist*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDataHist*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDataHist*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftCountingPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftCountingPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftCountingPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftCountingPdfBuilder(void *p);
   static void destruct_HfittercLcLHftCountingPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftCountingPdfBuilder*)
   {
      ::Hfitter::HftCountingPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftCountingPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftCountingPdfBuilder", ::Hfitter::HftCountingPdfBuilder::Class_Version(), "HfitterModels/HftCountingPdfBuilder.h", 11,
                  typeid(::Hfitter::HftCountingPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftCountingPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftCountingPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHftCountingPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftCountingPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHftCountingPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftCountingPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftCountingPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftCountingPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftCountingPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftCountingPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftHistPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftHistPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftHistPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftHistPdfBuilder(void *p);
   static void destruct_HfittercLcLHftHistPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftHistPdfBuilder*)
   {
      ::Hfitter::HftHistPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftHistPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftHistPdfBuilder", ::Hfitter::HftHistPdfBuilder::Class_Version(), "HfitterModels/HftHistPdfBuilder.h", 14,
                  typeid(::Hfitter::HftHistPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftHistPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftHistPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHftHistPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftHistPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHftHistPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftHistPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftHistPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftHistPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftHistPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftHistPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftFactoryPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftFactoryPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftFactoryPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftFactoryPdfBuilder(void *p);
   static void destruct_HfittercLcLHftFactoryPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftFactoryPdfBuilder*)
   {
      ::Hfitter::HftFactoryPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftFactoryPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftFactoryPdfBuilder", ::Hfitter::HftFactoryPdfBuilder::Class_Version(), "HfitterModels/HftFactoryPdfBuilder.h", 14,
                  typeid(::Hfitter::HftFactoryPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftFactoryPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftFactoryPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHftFactoryPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftFactoryPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHftFactoryPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftFactoryPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftFactoryPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftFactoryPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftFactoryPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftFactoryPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftWorkspacePdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftWorkspacePdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftWorkspacePdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftWorkspacePdfBuilder(void *p);
   static void destruct_HfittercLcLHftWorkspacePdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftWorkspacePdfBuilder*)
   {
      ::Hfitter::HftWorkspacePdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftWorkspacePdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftWorkspacePdfBuilder", ::Hfitter::HftWorkspacePdfBuilder::Class_Version(), "HfitterModels/HftWorkspacePdfBuilder.h", 14,
                  typeid(::Hfitter::HftWorkspacePdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftWorkspacePdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftWorkspacePdfBuilder) );
      instance.SetNew(&new_HfittercLcLHftWorkspacePdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftWorkspacePdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHftWorkspacePdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftWorkspacePdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftWorkspacePdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftWorkspacePdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftWorkspacePdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftWorkspacePdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftGenericRealBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftGenericRealBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftGenericRealBuilder(void *p);
   static void deleteArray_HfittercLcLHftGenericRealBuilder(void *p);
   static void destruct_HfittercLcLHftGenericRealBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftGenericRealBuilder*)
   {
      ::Hfitter::HftGenericRealBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftGenericRealBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftGenericRealBuilder", ::Hfitter::HftGenericRealBuilder::Class_Version(), "HfitterModels/HftGenericRealBuilder.h", 18,
                  typeid(::Hfitter::HftGenericRealBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftGenericRealBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftGenericRealBuilder) );
      instance.SetNew(&new_HfittercLcLHftGenericRealBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftGenericRealBuilder);
      instance.SetDelete(&delete_HfittercLcLHftGenericRealBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftGenericRealBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftGenericRealBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftGenericRealBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftGenericRealBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftGenericRealBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<RooUniform,0> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<RooUniform,0> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<RooUniform,0>", ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<RooUniform,0>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<RooUniform,0>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<RooLandau,2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<RooLandau,2> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<RooLandau,2>", ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<RooLandau,2>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<RooLandau,2>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>", ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>", ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<RooGaussian,2>", ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftGenericPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftGenericPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftGenericPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftGenericPdfBuilder(void *p);
   static void destruct_HfittercLcLHftGenericPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftGenericPdfBuilder*)
   {
      ::Hfitter::HftGenericPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftGenericPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftGenericPdfBuilder", ::Hfitter::HftGenericPdfBuilder::Class_Version(), "HfitterModels/HftGenericPdfBuilder.h", 13,
                  typeid(::Hfitter::HftGenericPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftGenericPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftGenericPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHftGenericPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftGenericPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHftGenericPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftGenericPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftGenericPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftGenericPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftGenericPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftGenericPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftMultiGaussianBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftMultiGaussianBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftMultiGaussianBuilder(void *p);
   static void deleteArray_HfittercLcLHftMultiGaussianBuilder(void *p);
   static void destruct_HfittercLcLHftMultiGaussianBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftMultiGaussianBuilder*)
   {
      ::Hfitter::HftMultiGaussianBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftMultiGaussianBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftMultiGaussianBuilder", ::Hfitter::HftMultiGaussianBuilder::Class_Version(), "HfitterModels/HftMultiGaussianBuilder.h", 13,
                  typeid(::Hfitter::HftMultiGaussianBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftMultiGaussianBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftMultiGaussianBuilder) );
      instance.SetNew(&new_HfittercLcLHftMultiGaussianBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftMultiGaussianBuilder);
      instance.SetDelete(&delete_HfittercLcLHftMultiGaussianBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftMultiGaussianBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftMultiGaussianBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftMultiGaussianBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftMultiGaussianBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftMultiGaussianBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftPolyPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHftPolyPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHftPolyPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHftPolyPdfBuilder(void *p);
   static void destruct_HfittercLcLHftPolyPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPolyPdfBuilder*)
   {
      ::Hfitter::HftPolyPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftPolyPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPolyPdfBuilder", ::Hfitter::HftPolyPdfBuilder::Class_Version(), "HfitterModels/HftPolyPdfBuilder.h", 15,
                  typeid(::Hfitter::HftPolyPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftPolyPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPolyPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHftPolyPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHftPolyPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHftPolyPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPolyPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHftPolyPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPolyPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPolyPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPolyPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftDoubleGaussian(void *p);
   static void deleteArray_HfittercLcLHftDoubleGaussian(void *p);
   static void destruct_HfittercLcLHftDoubleGaussian(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDoubleGaussian*)
   {
      ::Hfitter::HftDoubleGaussian *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftDoubleGaussian >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDoubleGaussian", ::Hfitter::HftDoubleGaussian::Class_Version(), "HfitterModels/HftDoubleGaussian.h", 31,
                  typeid(::Hfitter::HftDoubleGaussian), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftDoubleGaussian::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDoubleGaussian) );
      instance.SetDelete(&delete_HfittercLcLHftDoubleGaussian);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDoubleGaussian);
      instance.SetDestructor(&destruct_HfittercLcLHftDoubleGaussian);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDoubleGaussian*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDoubleGaussian*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDoubleGaussian*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftPolyExp(void *p = 0);
   static void *newArray_HfittercLcLHftPolyExp(Long_t size, void *p);
   static void delete_HfittercLcLHftPolyExp(void *p);
   static void deleteArray_HfittercLcLHftPolyExp(void *p);
   static void destruct_HfittercLcLHftPolyExp(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPolyExp*)
   {
      ::Hfitter::HftPolyExp *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftPolyExp >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPolyExp", ::Hfitter::HftPolyExp::Class_Version(), "HfitterModels/HftPolyExp.h", 19,
                  typeid(::Hfitter::HftPolyExp), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftPolyExp::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPolyExp) );
      instance.SetNew(&new_HfittercLcLHftPolyExp);
      instance.SetNewArray(&newArray_HfittercLcLHftPolyExp);
      instance.SetDelete(&delete_HfittercLcLHftPolyExp);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPolyExp);
      instance.SetDestructor(&destruct_HfittercLcLHftPolyExp);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPolyExp*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPolyExp*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPolyExp*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftFermiPolyExp(void *p = 0);
   static void *newArray_HfittercLcLHftFermiPolyExp(Long_t size, void *p);
   static void delete_HfittercLcLHftFermiPolyExp(void *p);
   static void deleteArray_HfittercLcLHftFermiPolyExp(void *p);
   static void destruct_HfittercLcLHftFermiPolyExp(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftFermiPolyExp*)
   {
      ::Hfitter::HftFermiPolyExp *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftFermiPolyExp >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftFermiPolyExp", ::Hfitter::HftFermiPolyExp::Class_Version(), "HfitterModels/HftFermiPolyExp.h", 18,
                  typeid(::Hfitter::HftFermiPolyExp), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftFermiPolyExp::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftFermiPolyExp) );
      instance.SetNew(&new_HfittercLcLHftFermiPolyExp);
      instance.SetNewArray(&newArray_HfittercLcLHftFermiPolyExp);
      instance.SetDelete(&delete_HfittercLcLHftFermiPolyExp);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftFermiPolyExp);
      instance.SetDestructor(&destruct_HfittercLcLHftFermiPolyExp);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftFermiPolyExp*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftFermiPolyExp*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftFermiPolyExp*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftDeltaPdf(void *p);
   static void deleteArray_HfittercLcLHftDeltaPdf(void *p);
   static void destruct_HfittercLcLHftDeltaPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDeltaPdf*)
   {
      ::Hfitter::HftDeltaPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftDeltaPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDeltaPdf", ::Hfitter::HftDeltaPdf::Class_Version(), "HfitterModels/HftDeltaPdf.h", 19,
                  typeid(::Hfitter::HftDeltaPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftDeltaPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDeltaPdf) );
      instance.SetDelete(&delete_HfittercLcLHftDeltaPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDeltaPdf);
      instance.SetDestructor(&destruct_HfittercLcLHftDeltaPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDeltaPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDeltaPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDeltaPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftPeggedPoly(void *p = 0);
   static void *newArray_HfittercLcLHftPeggedPoly(Long_t size, void *p);
   static void delete_HfittercLcLHftPeggedPoly(void *p);
   static void deleteArray_HfittercLcLHftPeggedPoly(void *p);
   static void destruct_HfittercLcLHftPeggedPoly(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPeggedPoly*)
   {
      ::Hfitter::HftPeggedPoly *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftPeggedPoly >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPeggedPoly", ::Hfitter::HftPeggedPoly::Class_Version(), "HfitterModels/HftPeggedPoly.h", 21,
                  typeid(::Hfitter::HftPeggedPoly), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftPeggedPoly::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPeggedPoly) );
      instance.SetNew(&new_HfittercLcLHftPeggedPoly);
      instance.SetNewArray(&newArray_HfittercLcLHftPeggedPoly);
      instance.SetDelete(&delete_HfittercLcLHftPeggedPoly);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPeggedPoly);
      instance.SetDestructor(&destruct_HfittercLcLHftPeggedPoly);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPeggedPoly*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPeggedPoly*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPeggedPoly*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftTruncPoly(void *p = 0);
   static void *newArray_HfittercLcLHftTruncPoly(Long_t size, void *p);
   static void delete_HfittercLcLHftTruncPoly(void *p);
   static void deleteArray_HfittercLcLHftTruncPoly(void *p);
   static void destruct_HfittercLcLHftTruncPoly(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTruncPoly*)
   {
      ::Hfitter::HftTruncPoly *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTruncPoly >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTruncPoly", ::Hfitter::HftTruncPoly::Class_Version(), "HfitterModels/HftTruncPoly.h", 21,
                  typeid(::Hfitter::HftTruncPoly), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftTruncPoly::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTruncPoly) );
      instance.SetNew(&new_HfittercLcLHftTruncPoly);
      instance.SetNewArray(&newArray_HfittercLcLHftTruncPoly);
      instance.SetDelete(&delete_HfittercLcLHftTruncPoly);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTruncPoly);
      instance.SetDestructor(&destruct_HfittercLcLHftTruncPoly);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTruncPoly*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTruncPoly*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTruncPoly*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_FlexibleInterpVarMkII(void *p = 0);
   static void *newArray_FlexibleInterpVarMkII(Long_t size, void *p);
   static void delete_FlexibleInterpVarMkII(void *p);
   static void deleteArray_FlexibleInterpVarMkII(void *p);
   static void destruct_FlexibleInterpVarMkII(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::FlexibleInterpVarMkII*)
   {
      ::FlexibleInterpVarMkII *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::FlexibleInterpVarMkII >(0);
      static ::ROOT::TGenericClassInfo 
         instance("FlexibleInterpVarMkII", ::FlexibleInterpVarMkII::Class_Version(), "HfitterModels/FlexibleInterpVarMkII.h", 26,
                  typeid(::FlexibleInterpVarMkII), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::FlexibleInterpVarMkII::Dictionary, isa_proxy, 4,
                  sizeof(::FlexibleInterpVarMkII) );
      instance.SetNew(&new_FlexibleInterpVarMkII);
      instance.SetNewArray(&newArray_FlexibleInterpVarMkII);
      instance.SetDelete(&delete_FlexibleInterpVarMkII);
      instance.SetDeleteArray(&deleteArray_FlexibleInterpVarMkII);
      instance.SetDestructor(&destruct_FlexibleInterpVarMkII);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::FlexibleInterpVarMkII*)
   {
      return GenerateInitInstanceLocal((::FlexibleInterpVarMkII*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::FlexibleInterpVarMkII*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftSPlot_Dictionary();
   static void HfittercLcLHftSPlot_TClassManip(TClass*);
   static void delete_HfittercLcLHftSPlot(void *p);
   static void deleteArray_HfittercLcLHftSPlot(void *p);
   static void destruct_HfittercLcLHftSPlot(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftSPlot*)
   {
      ::Hfitter::HftSPlot *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftSPlot));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftSPlot", "HfitterModels/HftSPlot.h", 24,
                  typeid(::Hfitter::HftSPlot), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftSPlot_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftSPlot) );
      instance.SetDelete(&delete_HfittercLcLHftSPlot);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftSPlot);
      instance.SetDestructor(&destruct_HfittercLcLHftSPlot);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftSPlot*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftSPlot*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftSPlot*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftSPlot_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftSPlot*)0x0)->GetClass();
      HfittercLcLHftSPlot_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftSPlot_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftDatacardReaderV4_Dictionary();
   static void HfittercLcLHftDatacardReaderV4_TClassManip(TClass*);
   static void *new_HfittercLcLHftDatacardReaderV4(void *p = 0);
   static void *newArray_HfittercLcLHftDatacardReaderV4(Long_t size, void *p);
   static void delete_HfittercLcLHftDatacardReaderV4(void *p);
   static void deleteArray_HfittercLcLHftDatacardReaderV4(void *p);
   static void destruct_HfittercLcLHftDatacardReaderV4(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDatacardReaderV4*)
   {
      ::Hfitter::HftDatacardReaderV4 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftDatacardReaderV4));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDatacardReaderV4", "HfitterModels/HftDatacardReaderV4.h", 31,
                  typeid(::Hfitter::HftDatacardReaderV4), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftDatacardReaderV4_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDatacardReaderV4) );
      instance.SetNew(&new_HfittercLcLHftDatacardReaderV4);
      instance.SetNewArray(&newArray_HfittercLcLHftDatacardReaderV4);
      instance.SetDelete(&delete_HfittercLcLHftDatacardReaderV4);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDatacardReaderV4);
      instance.SetDestructor(&destruct_HfittercLcLHftDatacardReaderV4);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDatacardReaderV4*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDatacardReaderV4*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDatacardReaderV4*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftDatacardReaderV4_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDatacardReaderV4*)0x0)->GetClass();
      HfittercLcLHftDatacardReaderV4_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftDatacardReaderV4_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHftModelBuilderV4(void *p = 0);
   static void *newArray_HfittercLcLHftModelBuilderV4(Long_t size, void *p);
   static void delete_HfittercLcLHftModelBuilderV4(void *p);
   static void deleteArray_HfittercLcLHftModelBuilderV4(void *p);
   static void destruct_HfittercLcLHftModelBuilderV4(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftModelBuilderV4*)
   {
      ::Hfitter::HftModelBuilderV4 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftModelBuilderV4 >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftModelBuilderV4", ::Hfitter::HftModelBuilderV4::Class_Version(), "HfitterModels/HftModelBuilderV4.h", 30,
                  typeid(::Hfitter::HftModelBuilderV4), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftModelBuilderV4::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftModelBuilderV4) );
      instance.SetNew(&new_HfittercLcLHftModelBuilderV4);
      instance.SetNewArray(&newArray_HfittercLcLHftModelBuilderV4);
      instance.SetDelete(&delete_HfittercLcLHftModelBuilderV4);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftModelBuilderV4);
      instance.SetDestructor(&destruct_HfittercLcLHftModelBuilderV4);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftModelBuilderV4*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftModelBuilderV4*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilderV4*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHftModelSplitterV4(void *p);
   static void deleteArray_HfittercLcLHftModelSplitterV4(void *p);
   static void destruct_HfittercLcLHftModelSplitterV4(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftModelSplitterV4*)
   {
      ::Hfitter::HftModelSplitterV4 *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftModelSplitterV4 >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftModelSplitterV4", ::Hfitter::HftModelSplitterV4::Class_Version(), "HfitterModels/HftModelSplitterV4.h", 26,
                  typeid(::Hfitter::HftModelSplitterV4), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HftModelSplitterV4::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftModelSplitterV4) );
      instance.SetDelete(&delete_HfittercLcLHftModelSplitterV4);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftModelSplitterV4);
      instance.SetDestructor(&destruct_HfittercLcLHftModelSplitterV4);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftModelSplitterV4*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftModelSplitterV4*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitterV4*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftRealMorphVar::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftRealMorphVar::Class_Name()
{
   return "Hfitter::HftRealMorphVar";
}

//______________________________________________________________________________
const char *HftRealMorphVar::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftRealMorphVar*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftRealMorphVar::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftRealMorphVar*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftRealMorphVar::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftRealMorphVar*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftRealMorphVar::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftRealMorphVar*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftInstance::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftInstance::Class_Name()
{
   return "Hfitter::HftInstance";
}

//______________________________________________________________________________
const char *HftInstance::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftInstance*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftInstance::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftInstance*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftInstance::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftInstance*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftInstance::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftInstance*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftAbsNodeBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftAbsNodeBuilder::Class_Name()
{
   return "Hfitter::HftAbsNodeBuilder";
}

//______________________________________________________________________________
const char *HftAbsNodeBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsNodeBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftAbsNodeBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsNodeBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftAbsNodeBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsNodeBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftAbsNodeBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsNodeBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftAbsPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftAbsPdfBuilder::Class_Name()
{
   return "Hfitter::HftAbsPdfBuilder";
}

//______________________________________________________________________________
const char *HftAbsPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftAbsPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftAbsPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftAbsPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftAbsRealBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftAbsRealBuilder::Class_Name()
{
   return "Hfitter::HftAbsRealBuilder";
}

//______________________________________________________________________________
const char *HftAbsRealBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsRealBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftAbsRealBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsRealBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftAbsRealBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsRealBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftAbsRealBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsRealBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftModelBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftModelBuilder::Class_Name()
{
   return "Hfitter::HftModelBuilder";
}

//______________________________________________________________________________
const char *HftModelBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftModelBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftModelBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftModelBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftModelSplitter::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftModelSplitter::Class_Name()
{
   return "Hfitter::HftModelSplitter";
}

//______________________________________________________________________________
const char *HftModelSplitter::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitter*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftModelSplitter::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitter*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftModelSplitter::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitter*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftModelSplitter::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitter*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftData::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftData::Class_Name()
{
   return "Hfitter::HftData";
}

//______________________________________________________________________________
const char *HftData::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftData*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftData::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftData*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftData::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftData*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftData::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftData*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftDataHist::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftDataHist::Class_Name()
{
   return "Hfitter::HftDataHist";
}

//______________________________________________________________________________
const char *HftDataHist::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDataHist*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftDataHist::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDataHist*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftDataHist::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDataHist*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftDataHist::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDataHist*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftCountingPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftCountingPdfBuilder::Class_Name()
{
   return "Hfitter::HftCountingPdfBuilder";
}

//______________________________________________________________________________
const char *HftCountingPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftCountingPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftCountingPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftCountingPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftCountingPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftCountingPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftCountingPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftCountingPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftHistPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftHistPdfBuilder::Class_Name()
{
   return "Hfitter::HftHistPdfBuilder";
}

//______________________________________________________________________________
const char *HftHistPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftHistPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftHistPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftHistPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftHistPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftHistPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftHistPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftHistPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftFactoryPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftFactoryPdfBuilder::Class_Name()
{
   return "Hfitter::HftFactoryPdfBuilder";
}

//______________________________________________________________________________
const char *HftFactoryPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFactoryPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftFactoryPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFactoryPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftFactoryPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFactoryPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftFactoryPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFactoryPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftWorkspacePdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftWorkspacePdfBuilder::Class_Name()
{
   return "Hfitter::HftWorkspacePdfBuilder";
}

//______________________________________________________________________________
const char *HftWorkspacePdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftWorkspacePdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftWorkspacePdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftWorkspacePdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftWorkspacePdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftWorkspacePdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftWorkspacePdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftWorkspacePdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftGenericRealBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftGenericRealBuilder::Class_Name()
{
   return "Hfitter::HftGenericRealBuilder";
}

//______________________________________________________________________________
const char *HftGenericRealBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericRealBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftGenericRealBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericRealBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftGenericRealBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericRealBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftGenericRealBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericRealBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<RooUniform,0>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooUniform,0>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<RooUniform,0>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooUniform,0>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<RooUniform,0>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooUniform,0>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooUniform,0>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<RooLandau,2>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooLandau,2>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<RooLandau,2>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooLandau,2>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<RooLandau,2>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooLandau,2>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooLandau,2>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<RooBifurGauss,3>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooBifurGauss,3>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooBifurGauss,3>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<RooBifurGauss,3>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooBifurGauss,3>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooBifurGauss,3>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<RooGaussian,2>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooGaussian,2>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<RooGaussian,2>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooGaussian,2>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<RooGaussian,2>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooGaussian,2>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooGaussian,2>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftGenericPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftGenericPdfBuilder::Class_Name()
{
   return "Hfitter::HftGenericPdfBuilder";
}

//______________________________________________________________________________
const char *HftGenericPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftGenericPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftGenericPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftGenericPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftGenericPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftMultiGaussianBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftMultiGaussianBuilder::Class_Name()
{
   return "Hfitter::HftMultiGaussianBuilder";
}

//______________________________________________________________________________
const char *HftMultiGaussianBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMultiGaussianBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftMultiGaussianBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMultiGaussianBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftMultiGaussianBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMultiGaussianBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftMultiGaussianBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMultiGaussianBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftPolyPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftPolyPdfBuilder::Class_Name()
{
   return "Hfitter::HftPolyPdfBuilder";
}

//______________________________________________________________________________
const char *HftPolyPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftPolyPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftPolyPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftPolyPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftDoubleGaussian::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftDoubleGaussian::Class_Name()
{
   return "Hfitter::HftDoubleGaussian";
}

//______________________________________________________________________________
const char *HftDoubleGaussian::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDoubleGaussian*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftDoubleGaussian::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDoubleGaussian*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftDoubleGaussian::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDoubleGaussian*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftDoubleGaussian::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDoubleGaussian*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftPolyExp::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftPolyExp::Class_Name()
{
   return "Hfitter::HftPolyExp";
}

//______________________________________________________________________________
const char *HftPolyExp::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyExp*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftPolyExp::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyExp*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftPolyExp::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyExp*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftPolyExp::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPolyExp*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftFermiPolyExp::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftFermiPolyExp::Class_Name()
{
   return "Hfitter::HftFermiPolyExp";
}

//______________________________________________________________________________
const char *HftFermiPolyExp::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFermiPolyExp*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftFermiPolyExp::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFermiPolyExp*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftFermiPolyExp::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFermiPolyExp*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftFermiPolyExp::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFermiPolyExp*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftDeltaPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftDeltaPdf::Class_Name()
{
   return "Hfitter::HftDeltaPdf";
}

//______________________________________________________________________________
const char *HftDeltaPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDeltaPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftDeltaPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDeltaPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftDeltaPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDeltaPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftDeltaPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDeltaPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftPeggedPoly::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftPeggedPoly::Class_Name()
{
   return "Hfitter::HftPeggedPoly";
}

//______________________________________________________________________________
const char *HftPeggedPoly::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPeggedPoly*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftPeggedPoly::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPeggedPoly*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftPeggedPoly::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPeggedPoly*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftPeggedPoly::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPeggedPoly*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftTruncPoly::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftTruncPoly::Class_Name()
{
   return "Hfitter::HftTruncPoly";
}

//______________________________________________________________________________
const char *HftTruncPoly::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTruncPoly*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftTruncPoly::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTruncPoly*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftTruncPoly::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTruncPoly*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftTruncPoly::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTruncPoly*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
//______________________________________________________________________________
atomic_TClass_ptr FlexibleInterpVarMkII::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *FlexibleInterpVarMkII::Class_Name()
{
   return "FlexibleInterpVarMkII";
}

//______________________________________________________________________________
const char *FlexibleInterpVarMkII::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::FlexibleInterpVarMkII*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int FlexibleInterpVarMkII::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::FlexibleInterpVarMkII*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *FlexibleInterpVarMkII::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::FlexibleInterpVarMkII*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *FlexibleInterpVarMkII::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::FlexibleInterpVarMkII*)0x0)->GetClass(); }
   return fgIsA;
}

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftModelBuilderV4::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftModelBuilderV4::Class_Name()
{
   return "Hfitter::HftModelBuilderV4";
}

//______________________________________________________________________________
const char *HftModelBuilderV4::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilderV4*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftModelBuilderV4::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilderV4*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftModelBuilderV4::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilderV4*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftModelBuilderV4::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelBuilderV4*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HftModelSplitterV4::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HftModelSplitterV4::Class_Name()
{
   return "Hfitter::HftModelSplitterV4";
}

//______________________________________________________________________________
const char *HftModelSplitterV4::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitterV4*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HftModelSplitterV4::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitterV4*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HftModelSplitterV4::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitterV4*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HftModelSplitterV4::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftModelSplitterV4*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsOption(void *p) {
      delete ((::Hfitter::HftAbsOption*)p);
   }
   static void deleteArray_HfittercLcLHftAbsOption(void *p) {
      delete [] ((::Hfitter::HftAbsOption*)p);
   }
   static void destruct_HfittercLcLHftAbsOption(void *p) {
      typedef ::Hfitter::HftAbsOption current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsOption

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLRooFitOpt(void *p) {
      delete ((::Hfitter::RooFitOpt*)p);
   }
   static void deleteArray_HfittercLcLRooFitOpt(void *p) {
      delete [] ((::Hfitter::RooFitOpt*)p);
   }
   static void destruct_HfittercLcLRooFitOpt(void *p) {
      typedef ::Hfitter::RooFitOpt current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::RooFitOpt

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLStrOpt(void *p) {
      delete ((::Hfitter::StrOpt*)p);
   }
   static void deleteArray_HfittercLcLStrOpt(void *p) {
      delete [] ((::Hfitter::StrOpt*)p);
   }
   static void destruct_HfittercLcLStrOpt(void *p) {
      typedef ::Hfitter::StrOpt current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::StrOpt

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLNoDefaults(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::NoDefaults : new ::Hfitter::NoDefaults;
   }
   static void *newArray_HfittercLcLNoDefaults(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::NoDefaults[nElements] : new ::Hfitter::NoDefaults[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLNoDefaults(void *p) {
      delete ((::Hfitter::NoDefaults*)p);
   }
   static void deleteArray_HfittercLcLNoDefaults(void *p) {
      delete [] ((::Hfitter::NoDefaults*)p);
   }
   static void destruct_HfittercLcLNoDefaults(void *p) {
      typedef ::Hfitter::NoDefaults current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::NoDefaults

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLOptions(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::Options : new ::Hfitter::Options;
   }
   static void *newArray_HfittercLcLOptions(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::Options[nElements] : new ::Hfitter::Options[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLOptions(void *p) {
      delete ((::Hfitter::Options*)p);
   }
   static void deleteArray_HfittercLcLOptions(void *p) {
      delete [] ((::Hfitter::Options*)p);
   }
   static void destruct_HfittercLcLOptions(void *p) {
      typedef ::Hfitter::Options current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::Options

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsParameters(void *p) {
      delete ((::Hfitter::HftAbsParameters*)p);
   }
   static void deleteArray_HfittercLcLHftAbsParameters(void *p) {
      delete [] ((::Hfitter::HftAbsParameters*)p);
   }
   static void destruct_HfittercLcLHftAbsParameters(void *p) {
      typedef ::Hfitter::HftAbsParameters current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsParameters

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftParameterStorage(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftParameterStorage : new ::Hfitter::HftParameterStorage;
   }
   static void *newArray_HfittercLcLHftParameterStorage(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftParameterStorage[nElements] : new ::Hfitter::HftParameterStorage[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftParameterStorage(void *p) {
      delete ((::Hfitter::HftParameterStorage*)p);
   }
   static void deleteArray_HfittercLcLHftParameterStorage(void *p) {
      delete [] ((::Hfitter::HftParameterStorage*)p);
   }
   static void destruct_HfittercLcLHftParameterStorage(void *p) {
      typedef ::Hfitter::HftParameterStorage current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftParameterStorage

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftStorableParameters(void *p) {
      delete ((::Hfitter::HftStorableParameters*)p);
   }
   static void deleteArray_HfittercLcLHftStorableParameters(void *p) {
      delete [] ((::Hfitter::HftStorableParameters*)p);
   }
   static void destruct_HfittercLcLHftStorableParameters(void *p) {
      typedef ::Hfitter::HftStorableParameters current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftStorableParameters

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftParameterSet(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftParameterSet : new ::Hfitter::HftParameterSet;
   }
   static void *newArray_HfittercLcLHftParameterSet(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftParameterSet[nElements] : new ::Hfitter::HftParameterSet[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftParameterSet(void *p) {
      delete ((::Hfitter::HftParameterSet*)p);
   }
   static void deleteArray_HfittercLcLHftParameterSet(void *p) {
      delete [] ((::Hfitter::HftParameterSet*)p);
   }
   static void destruct_HfittercLcLHftParameterSet(void *p) {
      typedef ::Hfitter::HftParameterSet current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftParameterSet

namespace Hfitter {
//______________________________________________________________________________
void HftRealMorphVar::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftRealMorphVar.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftRealMorphVar::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftRealMorphVar::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftRealMorphVar(void *p) {
      return  p ? new(p) ::Hfitter::HftRealMorphVar : new ::Hfitter::HftRealMorphVar;
   }
   static void *newArray_HfittercLcLHftRealMorphVar(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftRealMorphVar[nElements] : new ::Hfitter::HftRealMorphVar[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftRealMorphVar(void *p) {
      delete ((::Hfitter::HftRealMorphVar*)p);
   }
   static void deleteArray_HfittercLcLHftRealMorphVar(void *p) {
      delete [] ((::Hfitter::HftRealMorphVar*)p);
   }
   static void destruct_HfittercLcLHftRealMorphVar(void *p) {
      typedef ::Hfitter::HftRealMorphVar current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftRealMorphVar

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftConstraint(void *p) {
      delete ((::Hfitter::HftConstraint*)p);
   }
   static void deleteArray_HfittercLcLHftConstraint(void *p) {
      delete [] ((::Hfitter::HftConstraint*)p);
   }
   static void destruct_HfittercLcLHftConstraint(void *p) {
      typedef ::Hfitter::HftConstraint current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftConstraint

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftModel(void *p) {
      delete ((::Hfitter::HftModel*)p);
   }
   static void deleteArray_HfittercLcLHftModel(void *p) {
      delete [] ((::Hfitter::HftModel*)p);
   }
   static void destruct_HfittercLcLHftModel(void *p) {
      typedef ::Hfitter::HftModel current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftModel

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftPdfBuilderSpec(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftPdfBuilderSpec : new ::Hfitter::HftPdfBuilderSpec;
   }
   static void *newArray_HfittercLcLHftPdfBuilderSpec(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftPdfBuilderSpec[nElements] : new ::Hfitter::HftPdfBuilderSpec[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPdfBuilderSpec(void *p) {
      delete ((::Hfitter::HftPdfBuilderSpec*)p);
   }
   static void deleteArray_HfittercLcLHftPdfBuilderSpec(void *p) {
      delete [] ((::Hfitter::HftPdfBuilderSpec*)p);
   }
   static void destruct_HfittercLcLHftPdfBuilderSpec(void *p) {
      typedef ::Hfitter::HftPdfBuilderSpec current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPdfBuilderSpec

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftFunctionBuilderSpec(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftFunctionBuilderSpec : new ::Hfitter::HftFunctionBuilderSpec;
   }
   static void *newArray_HfittercLcLHftFunctionBuilderSpec(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftFunctionBuilderSpec[nElements] : new ::Hfitter::HftFunctionBuilderSpec[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftFunctionBuilderSpec(void *p) {
      delete ((::Hfitter::HftFunctionBuilderSpec*)p);
   }
   static void deleteArray_HfittercLcLHftFunctionBuilderSpec(void *p) {
      delete [] ((::Hfitter::HftFunctionBuilderSpec*)p);
   }
   static void destruct_HfittercLcLHftFunctionBuilderSpec(void *p) {
      typedef ::Hfitter::HftFunctionBuilderSpec current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftFunctionBuilderSpec

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftDatacardReader(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftDatacardReader : new ::Hfitter::HftDatacardReader;
   }
   static void *newArray_HfittercLcLHftDatacardReader(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftDatacardReader[nElements] : new ::Hfitter::HftDatacardReader[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDatacardReader(void *p) {
      delete ((::Hfitter::HftDatacardReader*)p);
   }
   static void deleteArray_HfittercLcLHftDatacardReader(void *p) {
      delete [] ((::Hfitter::HftDatacardReader*)p);
   }
   static void destruct_HfittercLcLHftDatacardReader(void *p) {
      typedef ::Hfitter::HftDatacardReader current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDatacardReader

namespace Hfitter {
//______________________________________________________________________________
void HftInstance::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftInstance.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftInstance::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftInstance::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftInstance(void *p) {
      return  p ? new(p) ::Hfitter::HftInstance : new ::Hfitter::HftInstance;
   }
   static void *newArray_HfittercLcLHftInstance(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftInstance[nElements] : new ::Hfitter::HftInstance[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftInstance(void *p) {
      delete ((::Hfitter::HftInstance*)p);
   }
   static void deleteArray_HfittercLcLHftInstance(void *p) {
      delete [] ((::Hfitter::HftInstance*)p);
   }
   static void destruct_HfittercLcLHftInstance(void *p) {
      typedef ::Hfitter::HftInstance current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftInstance

namespace Hfitter {
//______________________________________________________________________________
void HftAbsNodeBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftAbsNodeBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftAbsNodeBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftAbsNodeBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftAbsNodeBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftAbsNodeBuilder : new ::Hfitter::HftAbsNodeBuilder;
   }
   static void *newArray_HfittercLcLHftAbsNodeBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftAbsNodeBuilder[nElements] : new ::Hfitter::HftAbsNodeBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsNodeBuilder(void *p) {
      delete ((::Hfitter::HftAbsNodeBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftAbsNodeBuilder(void *p) {
      delete [] ((::Hfitter::HftAbsNodeBuilder*)p);
   }
   static void destruct_HfittercLcLHftAbsNodeBuilder(void *p) {
      typedef ::Hfitter::HftAbsNodeBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsNodeBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftAbsPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftAbsPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftAbsPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftAbsPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsPdfBuilder(void *p) {
      delete ((::Hfitter::HftAbsPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftAbsPdfBuilder(void *p) {
      delete [] ((::Hfitter::HftAbsPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftAbsPdfBuilder(void *p) {
      typedef ::Hfitter::HftAbsPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftAbsRealBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftAbsRealBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftAbsRealBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftAbsRealBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsRealBuilder(void *p) {
      delete ((::Hfitter::HftAbsRealBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftAbsRealBuilder(void *p) {
      delete [] ((::Hfitter::HftAbsRealBuilder*)p);
   }
   static void destruct_HfittercLcLHftAbsRealBuilder(void *p) {
      typedef ::Hfitter::HftAbsRealBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsRealBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftModelBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftModelBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftModelBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftModelBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftModelBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftModelBuilder : new ::Hfitter::HftModelBuilder;
   }
   static void *newArray_HfittercLcLHftModelBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftModelBuilder[nElements] : new ::Hfitter::HftModelBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftModelBuilder(void *p) {
      delete ((::Hfitter::HftModelBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftModelBuilder(void *p) {
      delete [] ((::Hfitter::HftModelBuilder*)p);
   }
   static void destruct_HfittercLcLHftModelBuilder(void *p) {
      typedef ::Hfitter::HftModelBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftModelBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftModelSplitter::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftModelSplitter.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftModelSplitter::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftModelSplitter::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftModelSplitter(void *p) {
      delete ((::Hfitter::HftModelSplitter*)p);
   }
   static void deleteArray_HfittercLcLHftModelSplitter(void *p) {
      delete [] ((::Hfitter::HftModelSplitter*)p);
   }
   static void destruct_HfittercLcLHftModelSplitter(void *p) {
      typedef ::Hfitter::HftModelSplitter current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftModelSplitter

namespace Hfitter {
//______________________________________________________________________________
void HftData::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftData.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftData::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftData::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftData(void *p) {
      delete ((::Hfitter::HftData*)p);
   }
   static void deleteArray_HfittercLcLHftData(void *p) {
      delete [] ((::Hfitter::HftData*)p);
   }
   static void destruct_HfittercLcLHftData(void *p) {
      typedef ::Hfitter::HftData current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftData

namespace Hfitter {
//______________________________________________________________________________
void HftDataHist::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftDataHist.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftDataHist::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftDataHist::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDataHist(void *p) {
      delete ((::Hfitter::HftDataHist*)p);
   }
   static void deleteArray_HfittercLcLHftDataHist(void *p) {
      delete [] ((::Hfitter::HftDataHist*)p);
   }
   static void destruct_HfittercLcLHftDataHist(void *p) {
      typedef ::Hfitter::HftDataHist current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDataHist

namespace Hfitter {
//______________________________________________________________________________
void HftCountingPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftCountingPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftCountingPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftCountingPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftCountingPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftCountingPdfBuilder : new ::Hfitter::HftCountingPdfBuilder;
   }
   static void *newArray_HfittercLcLHftCountingPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftCountingPdfBuilder[nElements] : new ::Hfitter::HftCountingPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftCountingPdfBuilder(void *p) {
      delete ((::Hfitter::HftCountingPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftCountingPdfBuilder(void *p) {
      delete [] ((::Hfitter::HftCountingPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftCountingPdfBuilder(void *p) {
      typedef ::Hfitter::HftCountingPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftCountingPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftHistPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftHistPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftHistPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftHistPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftHistPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftHistPdfBuilder : new ::Hfitter::HftHistPdfBuilder;
   }
   static void *newArray_HfittercLcLHftHistPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftHistPdfBuilder[nElements] : new ::Hfitter::HftHistPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftHistPdfBuilder(void *p) {
      delete ((::Hfitter::HftHistPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftHistPdfBuilder(void *p) {
      delete [] ((::Hfitter::HftHistPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftHistPdfBuilder(void *p) {
      typedef ::Hfitter::HftHistPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftHistPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftFactoryPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftFactoryPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftFactoryPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftFactoryPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftFactoryPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftFactoryPdfBuilder : new ::Hfitter::HftFactoryPdfBuilder;
   }
   static void *newArray_HfittercLcLHftFactoryPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftFactoryPdfBuilder[nElements] : new ::Hfitter::HftFactoryPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftFactoryPdfBuilder(void *p) {
      delete ((::Hfitter::HftFactoryPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftFactoryPdfBuilder(void *p) {
      delete [] ((::Hfitter::HftFactoryPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftFactoryPdfBuilder(void *p) {
      typedef ::Hfitter::HftFactoryPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftFactoryPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftWorkspacePdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftWorkspacePdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftWorkspacePdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftWorkspacePdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftWorkspacePdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftWorkspacePdfBuilder : new ::Hfitter::HftWorkspacePdfBuilder;
   }
   static void *newArray_HfittercLcLHftWorkspacePdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftWorkspacePdfBuilder[nElements] : new ::Hfitter::HftWorkspacePdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftWorkspacePdfBuilder(void *p) {
      delete ((::Hfitter::HftWorkspacePdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftWorkspacePdfBuilder(void *p) {
      delete [] ((::Hfitter::HftWorkspacePdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftWorkspacePdfBuilder(void *p) {
      typedef ::Hfitter::HftWorkspacePdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftWorkspacePdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftGenericRealBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftGenericRealBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftGenericRealBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftGenericRealBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftGenericRealBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftGenericRealBuilder : new ::Hfitter::HftGenericRealBuilder;
   }
   static void *newArray_HfittercLcLHftGenericRealBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftGenericRealBuilder[nElements] : new ::Hfitter::HftGenericRealBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftGenericRealBuilder(void *p) {
      delete ((::Hfitter::HftGenericRealBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftGenericRealBuilder(void *p) {
      delete [] ((::Hfitter::HftGenericRealBuilder*)p);
   }
   static void destruct_HfittercLcLHftGenericRealBuilder(void *p) {
      typedef ::Hfitter::HftGenericRealBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftGenericRealBuilder

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<RooUniform,0>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<RooUniform,0>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<RooUniform,0>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<RooUniform,0>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooUniform,0> : new ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<RooUniform,0>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooUniformcO0gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<RooUniform,0> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<RooUniform,0>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<RooLandau,2>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<RooLandau,2>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<RooLandau,2>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<RooLandau,2>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooLandau,2> : new ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<RooLandau,2>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooLandaucO2gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<RooLandau,2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<RooLandau,2>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<RooBifurGauss,3>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3> : new ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooBifurGausscO3gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5> : new ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHftDoubleGaussiancO5gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<RooGaussian,2>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<RooGaussian,2>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<RooGaussian,2>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<RooGaussian,2>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2> : new ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooGaussiancO2gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<RooGaussian,2>

namespace Hfitter {
//______________________________________________________________________________
void HftGenericPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftGenericPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftGenericPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftGenericPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftGenericPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftGenericPdfBuilder : new ::Hfitter::HftGenericPdfBuilder;
   }
   static void *newArray_HfittercLcLHftGenericPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftGenericPdfBuilder[nElements] : new ::Hfitter::HftGenericPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftGenericPdfBuilder(void *p) {
      delete ((::Hfitter::HftGenericPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftGenericPdfBuilder(void *p) {
      delete [] ((::Hfitter::HftGenericPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftGenericPdfBuilder(void *p) {
      typedef ::Hfitter::HftGenericPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftGenericPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftMultiGaussianBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftMultiGaussianBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftMultiGaussianBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftMultiGaussianBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftMultiGaussianBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftMultiGaussianBuilder : new ::Hfitter::HftMultiGaussianBuilder;
   }
   static void *newArray_HfittercLcLHftMultiGaussianBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftMultiGaussianBuilder[nElements] : new ::Hfitter::HftMultiGaussianBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftMultiGaussianBuilder(void *p) {
      delete ((::Hfitter::HftMultiGaussianBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftMultiGaussianBuilder(void *p) {
      delete [] ((::Hfitter::HftMultiGaussianBuilder*)p);
   }
   static void destruct_HfittercLcLHftMultiGaussianBuilder(void *p) {
      typedef ::Hfitter::HftMultiGaussianBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftMultiGaussianBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftPolyPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftPolyPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftPolyPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftPolyPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftPolyPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HftPolyPdfBuilder : new ::Hfitter::HftPolyPdfBuilder;
   }
   static void *newArray_HfittercLcLHftPolyPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftPolyPdfBuilder[nElements] : new ::Hfitter::HftPolyPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPolyPdfBuilder(void *p) {
      delete ((::Hfitter::HftPolyPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHftPolyPdfBuilder(void *p) {
      delete [] ((::Hfitter::HftPolyPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHftPolyPdfBuilder(void *p) {
      typedef ::Hfitter::HftPolyPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPolyPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HftDoubleGaussian::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftDoubleGaussian.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftDoubleGaussian::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftDoubleGaussian::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDoubleGaussian(void *p) {
      delete ((::Hfitter::HftDoubleGaussian*)p);
   }
   static void deleteArray_HfittercLcLHftDoubleGaussian(void *p) {
      delete [] ((::Hfitter::HftDoubleGaussian*)p);
   }
   static void destruct_HfittercLcLHftDoubleGaussian(void *p) {
      typedef ::Hfitter::HftDoubleGaussian current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDoubleGaussian

namespace Hfitter {
//______________________________________________________________________________
void HftPolyExp::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftPolyExp.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftPolyExp::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftPolyExp::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftPolyExp(void *p) {
      return  p ? new(p) ::Hfitter::HftPolyExp : new ::Hfitter::HftPolyExp;
   }
   static void *newArray_HfittercLcLHftPolyExp(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftPolyExp[nElements] : new ::Hfitter::HftPolyExp[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPolyExp(void *p) {
      delete ((::Hfitter::HftPolyExp*)p);
   }
   static void deleteArray_HfittercLcLHftPolyExp(void *p) {
      delete [] ((::Hfitter::HftPolyExp*)p);
   }
   static void destruct_HfittercLcLHftPolyExp(void *p) {
      typedef ::Hfitter::HftPolyExp current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPolyExp

namespace Hfitter {
//______________________________________________________________________________
void HftFermiPolyExp::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftFermiPolyExp.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftFermiPolyExp::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftFermiPolyExp::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftFermiPolyExp(void *p) {
      return  p ? new(p) ::Hfitter::HftFermiPolyExp : new ::Hfitter::HftFermiPolyExp;
   }
   static void *newArray_HfittercLcLHftFermiPolyExp(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftFermiPolyExp[nElements] : new ::Hfitter::HftFermiPolyExp[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftFermiPolyExp(void *p) {
      delete ((::Hfitter::HftFermiPolyExp*)p);
   }
   static void deleteArray_HfittercLcLHftFermiPolyExp(void *p) {
      delete [] ((::Hfitter::HftFermiPolyExp*)p);
   }
   static void destruct_HfittercLcLHftFermiPolyExp(void *p) {
      typedef ::Hfitter::HftFermiPolyExp current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftFermiPolyExp

namespace Hfitter {
//______________________________________________________________________________
void HftDeltaPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftDeltaPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftDeltaPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftDeltaPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDeltaPdf(void *p) {
      delete ((::Hfitter::HftDeltaPdf*)p);
   }
   static void deleteArray_HfittercLcLHftDeltaPdf(void *p) {
      delete [] ((::Hfitter::HftDeltaPdf*)p);
   }
   static void destruct_HfittercLcLHftDeltaPdf(void *p) {
      typedef ::Hfitter::HftDeltaPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDeltaPdf

namespace Hfitter {
//______________________________________________________________________________
void HftPeggedPoly::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftPeggedPoly.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftPeggedPoly::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftPeggedPoly::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftPeggedPoly(void *p) {
      return  p ? new(p) ::Hfitter::HftPeggedPoly : new ::Hfitter::HftPeggedPoly;
   }
   static void *newArray_HfittercLcLHftPeggedPoly(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftPeggedPoly[nElements] : new ::Hfitter::HftPeggedPoly[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPeggedPoly(void *p) {
      delete ((::Hfitter::HftPeggedPoly*)p);
   }
   static void deleteArray_HfittercLcLHftPeggedPoly(void *p) {
      delete [] ((::Hfitter::HftPeggedPoly*)p);
   }
   static void destruct_HfittercLcLHftPeggedPoly(void *p) {
      typedef ::Hfitter::HftPeggedPoly current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPeggedPoly

namespace Hfitter {
//______________________________________________________________________________
void HftTruncPoly::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTruncPoly.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTruncPoly::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTruncPoly::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTruncPoly(void *p) {
      return  p ? new(p) ::Hfitter::HftTruncPoly : new ::Hfitter::HftTruncPoly;
   }
   static void *newArray_HfittercLcLHftTruncPoly(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTruncPoly[nElements] : new ::Hfitter::HftTruncPoly[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTruncPoly(void *p) {
      delete ((::Hfitter::HftTruncPoly*)p);
   }
   static void deleteArray_HfittercLcLHftTruncPoly(void *p) {
      delete [] ((::Hfitter::HftTruncPoly*)p);
   }
   static void destruct_HfittercLcLHftTruncPoly(void *p) {
      typedef ::Hfitter::HftTruncPoly current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTruncPoly

//______________________________________________________________________________
void FlexibleInterpVarMkII::Streamer(TBuffer &R__b)
{
   // Stream an object of class FlexibleInterpVarMkII.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(FlexibleInterpVarMkII::Class(),this);
   } else {
      R__b.WriteClassBuffer(FlexibleInterpVarMkII::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_FlexibleInterpVarMkII(void *p) {
      return  p ? new(p) ::FlexibleInterpVarMkII : new ::FlexibleInterpVarMkII;
   }
   static void *newArray_FlexibleInterpVarMkII(Long_t nElements, void *p) {
      return p ? new(p) ::FlexibleInterpVarMkII[nElements] : new ::FlexibleInterpVarMkII[nElements];
   }
   // Wrapper around operator delete
   static void delete_FlexibleInterpVarMkII(void *p) {
      delete ((::FlexibleInterpVarMkII*)p);
   }
   static void deleteArray_FlexibleInterpVarMkII(void *p) {
      delete [] ((::FlexibleInterpVarMkII*)p);
   }
   static void destruct_FlexibleInterpVarMkII(void *p) {
      typedef ::FlexibleInterpVarMkII current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::FlexibleInterpVarMkII

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftSPlot(void *p) {
      delete ((::Hfitter::HftSPlot*)p);
   }
   static void deleteArray_HfittercLcLHftSPlot(void *p) {
      delete [] ((::Hfitter::HftSPlot*)p);
   }
   static void destruct_HfittercLcLHftSPlot(void *p) {
      typedef ::Hfitter::HftSPlot current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftSPlot

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftDatacardReaderV4(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftDatacardReaderV4 : new ::Hfitter::HftDatacardReaderV4;
   }
   static void *newArray_HfittercLcLHftDatacardReaderV4(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftDatacardReaderV4[nElements] : new ::Hfitter::HftDatacardReaderV4[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDatacardReaderV4(void *p) {
      delete ((::Hfitter::HftDatacardReaderV4*)p);
   }
   static void deleteArray_HfittercLcLHftDatacardReaderV4(void *p) {
      delete [] ((::Hfitter::HftDatacardReaderV4*)p);
   }
   static void destruct_HfittercLcLHftDatacardReaderV4(void *p) {
      typedef ::Hfitter::HftDatacardReaderV4 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDatacardReaderV4

namespace Hfitter {
//______________________________________________________________________________
void HftModelBuilderV4::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftModelBuilderV4.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftModelBuilderV4::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftModelBuilderV4::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftModelBuilderV4(void *p) {
      return  p ? new(p) ::Hfitter::HftModelBuilderV4 : new ::Hfitter::HftModelBuilderV4;
   }
   static void *newArray_HfittercLcLHftModelBuilderV4(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftModelBuilderV4[nElements] : new ::Hfitter::HftModelBuilderV4[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftModelBuilderV4(void *p) {
      delete ((::Hfitter::HftModelBuilderV4*)p);
   }
   static void deleteArray_HfittercLcLHftModelBuilderV4(void *p) {
      delete [] ((::Hfitter::HftModelBuilderV4*)p);
   }
   static void destruct_HfittercLcLHftModelBuilderV4(void *p) {
      typedef ::Hfitter::HftModelBuilderV4 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftModelBuilderV4

namespace Hfitter {
//______________________________________________________________________________
void HftModelSplitterV4::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftModelSplitterV4.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftModelSplitterV4::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftModelSplitterV4::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftModelSplitterV4(void *p) {
      delete ((::Hfitter::HftModelSplitterV4*)p);
   }
   static void deleteArray_HfittercLcLHftModelSplitterV4(void *p) {
      delete [] ((::Hfitter::HftModelSplitterV4*)p);
   }
   static void destruct_HfittercLcLHftModelSplitterV4(void *p) {
      typedef ::Hfitter::HftModelSplitterV4 current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftModelSplitterV4

namespace ROOT {
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary();
   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p);
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<unsigned int> >*)
   {
      vector<vector<unsigned int> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<unsigned int> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<unsigned int> >", -2, "vector", 214,
                  typeid(vector<vector<unsigned int> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEunsignedsPintgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<vector<unsigned int> >) );
      instance.SetNew(&new_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEunsignedsPintgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<unsigned int> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEunsignedsPintgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<unsigned int> >*)0x0)->GetClass();
      vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEunsignedsPintgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned int> > : new vector<vector<unsigned int> >;
   }
   static void *newArray_vectorlEvectorlEunsignedsPintgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<unsigned int> >[nElements] : new vector<vector<unsigned int> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete ((vector<vector<unsigned int> >*)p);
   }
   static void deleteArray_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      delete [] ((vector<vector<unsigned int> >*)p);
   }
   static void destruct_vectorlEvectorlEunsignedsPintgRsPgR(void *p) {
      typedef vector<vector<unsigned int> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<unsigned int> >

namespace ROOT {
   static TClass *vectorlEunsignedsPintgR_Dictionary();
   static void vectorlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPintgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPintgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPintgR(void *p);
   static void deleteArray_vectorlEunsignedsPintgR(void *p);
   static void destruct_vectorlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned int>*)
   {
      vector<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned int>", -2, "vector", 214,
                  typeid(vector<unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<unsigned int>) );
      instance.SetNew(&new_vectorlEunsignedsPintgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPintgR);
      instance.SetDelete(&delete_vectorlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPintgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned int>*)0x0)->GetClass();
      vectorlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int> : new vector<unsigned int>;
   }
   static void *newArray_vectorlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int>[nElements] : new vector<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPintgR(void *p) {
      delete ((vector<unsigned int>*)p);
   }
   static void deleteArray_vectorlEunsignedsPintgR(void *p) {
      delete [] ((vector<unsigned int>*)p);
   }
   static void destruct_vectorlEunsignedsPintgR(void *p) {
      typedef vector<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned int>

namespace ROOT {
   static TClass *vectorlEsetlETStringgRsPgR_Dictionary();
   static void vectorlEsetlETStringgRsPgR_TClassManip(TClass*);
   static void *new_vectorlEsetlETStringgRsPgR(void *p = 0);
   static void *newArray_vectorlEsetlETStringgRsPgR(Long_t size, void *p);
   static void delete_vectorlEsetlETStringgRsPgR(void *p);
   static void deleteArray_vectorlEsetlETStringgRsPgR(void *p);
   static void destruct_vectorlEsetlETStringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<set<TString> >*)
   {
      vector<set<TString> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<set<TString> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<set<TString> >", -2, "vector", 214,
                  typeid(vector<set<TString> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEsetlETStringgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<set<TString> >) );
      instance.SetNew(&new_vectorlEsetlETStringgRsPgR);
      instance.SetNewArray(&newArray_vectorlEsetlETStringgRsPgR);
      instance.SetDelete(&delete_vectorlEsetlETStringgRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEsetlETStringgRsPgR);
      instance.SetDestructor(&destruct_vectorlEsetlETStringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<set<TString> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<set<TString> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEsetlETStringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<set<TString> >*)0x0)->GetClass();
      vectorlEsetlETStringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEsetlETStringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEsetlETStringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<set<TString> > : new vector<set<TString> >;
   }
   static void *newArray_vectorlEsetlETStringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<set<TString> >[nElements] : new vector<set<TString> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEsetlETStringgRsPgR(void *p) {
      delete ((vector<set<TString> >*)p);
   }
   static void deleteArray_vectorlEsetlETStringgRsPgR(void *p) {
      delete [] ((vector<set<TString> >*)p);
   }
   static void destruct_vectorlEsetlETStringgRsPgR(void *p) {
      typedef vector<set<TString> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<set<TString> >

namespace ROOT {
   static TClass *vectorlEintgR_Dictionary();
   static void vectorlEintgR_TClassManip(TClass*);
   static void *new_vectorlEintgR(void *p = 0);
   static void *newArray_vectorlEintgR(Long_t size, void *p);
   static void delete_vectorlEintgR(void *p);
   static void deleteArray_vectorlEintgR(void *p);
   static void destruct_vectorlEintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<int>*)
   {
      vector<int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<int>", -2, "vector", 214,
                  typeid(vector<int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<int>) );
      instance.SetNew(&new_vectorlEintgR);
      instance.SetNewArray(&newArray_vectorlEintgR);
      instance.SetDelete(&delete_vectorlEintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEintgR);
      instance.SetDestructor(&destruct_vectorlEintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<int>*)0x0)->GetClass();
      vectorlEintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int> : new vector<int>;
   }
   static void *newArray_vectorlEintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<int>[nElements] : new vector<int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEintgR(void *p) {
      delete ((vector<int>*)p);
   }
   static void deleteArray_vectorlEintgR(void *p) {
      delete [] ((vector<int>*)p);
   }
   static void destruct_vectorlEintgR(void *p) {
      typedef vector<int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<int>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 214,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace ROOT {
   static TClass *vectorlERooAbsPdfmUgR_Dictionary();
   static void vectorlERooAbsPdfmUgR_TClassManip(TClass*);
   static void *new_vectorlERooAbsPdfmUgR(void *p = 0);
   static void *newArray_vectorlERooAbsPdfmUgR(Long_t size, void *p);
   static void delete_vectorlERooAbsPdfmUgR(void *p);
   static void deleteArray_vectorlERooAbsPdfmUgR(void *p);
   static void destruct_vectorlERooAbsPdfmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<RooAbsPdf*>*)
   {
      vector<RooAbsPdf*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<RooAbsPdf*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<RooAbsPdf*>", -2, "vector", 214,
                  typeid(vector<RooAbsPdf*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlERooAbsPdfmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<RooAbsPdf*>) );
      instance.SetNew(&new_vectorlERooAbsPdfmUgR);
      instance.SetNewArray(&newArray_vectorlERooAbsPdfmUgR);
      instance.SetDelete(&delete_vectorlERooAbsPdfmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlERooAbsPdfmUgR);
      instance.SetDestructor(&destruct_vectorlERooAbsPdfmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<RooAbsPdf*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<RooAbsPdf*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERooAbsPdfmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<RooAbsPdf*>*)0x0)->GetClass();
      vectorlERooAbsPdfmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERooAbsPdfmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERooAbsPdfmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooAbsPdf*> : new vector<RooAbsPdf*>;
   }
   static void *newArray_vectorlERooAbsPdfmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooAbsPdf*>[nElements] : new vector<RooAbsPdf*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERooAbsPdfmUgR(void *p) {
      delete ((vector<RooAbsPdf*>*)p);
   }
   static void deleteArray_vectorlERooAbsPdfmUgR(void *p) {
      delete [] ((vector<RooAbsPdf*>*)p);
   }
   static void destruct_vectorlERooAbsPdfmUgR(void *p) {
      typedef vector<RooAbsPdf*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<RooAbsPdf*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftPdfBuilderSpecgR_Dictionary();
   static void vectorlEHfittercLcLHftPdfBuilderSpecgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftPdfBuilderSpecgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p);
   static void destruct_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftPdfBuilderSpec>*)
   {
      vector<Hfitter::HftPdfBuilderSpec> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftPdfBuilderSpec>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftPdfBuilderSpec>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftPdfBuilderSpec>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftPdfBuilderSpecgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftPdfBuilderSpec>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftPdfBuilderSpecgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftPdfBuilderSpecgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftPdfBuilderSpecgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftPdfBuilderSpecgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftPdfBuilderSpecgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftPdfBuilderSpec> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftPdfBuilderSpec>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftPdfBuilderSpecgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftPdfBuilderSpec>*)0x0)->GetClass();
      vectorlEHfittercLcLHftPdfBuilderSpecgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftPdfBuilderSpecgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftPdfBuilderSpec> : new vector<Hfitter::HftPdfBuilderSpec>;
   }
   static void *newArray_vectorlEHfittercLcLHftPdfBuilderSpecgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftPdfBuilderSpec>[nElements] : new vector<Hfitter::HftPdfBuilderSpec>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p) {
      delete ((vector<Hfitter::HftPdfBuilderSpec>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p) {
      delete [] ((vector<Hfitter::HftPdfBuilderSpec>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftPdfBuilderSpecgR(void *p) {
      typedef vector<Hfitter::HftPdfBuilderSpec> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftPdfBuilderSpec>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftParameterStoragegR_Dictionary();
   static void vectorlEHfittercLcLHftParameterStoragegR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftParameterStoragegR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftParameterStoragegR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftParameterStoragegR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftParameterStoragegR(void *p);
   static void destruct_vectorlEHfittercLcLHftParameterStoragegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftParameterStorage>*)
   {
      vector<Hfitter::HftParameterStorage> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftParameterStorage>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftParameterStorage>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftParameterStorage>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftParameterStoragegR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Hfitter::HftParameterStorage>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftParameterStoragegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftParameterStorage> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftParameterStorage>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftParameterStoragegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftParameterStorage>*)0x0)->GetClass();
      vectorlEHfittercLcLHftParameterStoragegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftParameterStoragegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftParameterStorage> : new vector<Hfitter::HftParameterStorage>;
   }
   static void *newArray_vectorlEHfittercLcLHftParameterStoragegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftParameterStorage>[nElements] : new vector<Hfitter::HftParameterStorage>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      delete ((vector<Hfitter::HftParameterStorage>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      delete [] ((vector<Hfitter::HftParameterStorage>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      typedef vector<Hfitter::HftParameterStorage> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftParameterStorage>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftModelmUgR_Dictionary();
   static void vectorlEHfittercLcLHftModelmUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftModelmUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftModelmUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftModelmUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftModelmUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftModelmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftModel*>*)
   {
      vector<Hfitter::HftModel*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftModel*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftModel*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftModel*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftModelmUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Hfitter::HftModel*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftModelmUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftModelmUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftModelmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftModelmUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftModelmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftModel*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftModel*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftModelmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftModel*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftModelmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftModelmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftModelmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftModel*> : new vector<Hfitter::HftModel*>;
   }
   static void *newArray_vectorlEHfittercLcLHftModelmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftModel*>[nElements] : new vector<Hfitter::HftModel*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftModelmUgR(void *p) {
      delete ((vector<Hfitter::HftModel*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftModelmUgR(void *p) {
      delete [] ((vector<Hfitter::HftModel*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftModelmUgR(void *p) {
      typedef vector<Hfitter::HftModel*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftModel*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftConstraintmUgR_Dictionary();
   static void vectorlEHfittercLcLHftConstraintmUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftConstraintmUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftConstraintmUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftConstraintmUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftConstraintmUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftConstraintmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftConstraint*>*)
   {
      vector<Hfitter::HftConstraint*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftConstraint*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftConstraint*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftConstraint*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftConstraintmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftConstraint*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftConstraintmUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftConstraintmUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftConstraintmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftConstraintmUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftConstraintmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftConstraint*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftConstraint*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftConstraintmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftConstraint*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftConstraintmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftConstraintmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftConstraintmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftConstraint*> : new vector<Hfitter::HftConstraint*>;
   }
   static void *newArray_vectorlEHfittercLcLHftConstraintmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftConstraint*>[nElements] : new vector<Hfitter::HftConstraint*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftConstraintmUgR(void *p) {
      delete ((vector<Hfitter::HftConstraint*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftConstraintmUgR(void *p) {
      delete [] ((vector<Hfitter::HftConstraint*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftConstraintmUgR(void *p) {
      typedef vector<Hfitter::HftConstraint*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftConstraint*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftAbsParametersmUgR_Dictionary();
   static void vectorlEHfittercLcLHftAbsParametersmUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftAbsParametersmUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftAbsParametersmUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftAbsParametersmUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftAbsParametersmUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftAbsParametersmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftAbsParameters*>*)
   {
      vector<Hfitter::HftAbsParameters*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftAbsParameters*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftAbsParameters*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftAbsParameters*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftAbsParametersmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftAbsParameters*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftAbsParametersmUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftAbsParametersmUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftAbsParametersmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftAbsParametersmUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftAbsParametersmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftAbsParameters*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftAbsParameters*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftAbsParametersmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftAbsParameters*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftAbsParametersmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftAbsParametersmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftAbsParametersmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsParameters*> : new vector<Hfitter::HftAbsParameters*>;
   }
   static void *newArray_vectorlEHfittercLcLHftAbsParametersmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsParameters*>[nElements] : new vector<Hfitter::HftAbsParameters*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftAbsParametersmUgR(void *p) {
      delete ((vector<Hfitter::HftAbsParameters*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftAbsParametersmUgR(void *p) {
      delete [] ((vector<Hfitter::HftAbsParameters*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftAbsParametersmUgR(void *p) {
      typedef vector<Hfitter::HftAbsParameters*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftAbsParameters*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftAbsOptionmUgR_Dictionary();
   static void vectorlEHfittercLcLHftAbsOptionmUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftAbsOptionmUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftAbsOptionmUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftAbsOptionmUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftAbsOptionmUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftAbsOptionmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftAbsOption*>*)
   {
      vector<Hfitter::HftAbsOption*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftAbsOption*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftAbsOption*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftAbsOption*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftAbsOptionmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftAbsOption*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftAbsOptionmUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftAbsOptionmUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftAbsOptionmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftAbsOptionmUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftAbsOptionmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftAbsOption*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftAbsOption*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftAbsOptionmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftAbsOption*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftAbsOptionmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftAbsOptionmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftAbsOptionmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsOption*> : new vector<Hfitter::HftAbsOption*>;
   }
   static void *newArray_vectorlEHfittercLcLHftAbsOptionmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsOption*>[nElements] : new vector<Hfitter::HftAbsOption*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftAbsOptionmUgR(void *p) {
      delete ((vector<Hfitter::HftAbsOption*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftAbsOptionmUgR(void *p) {
      delete [] ((vector<Hfitter::HftAbsOption*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftAbsOptionmUgR(void *p) {
      typedef vector<Hfitter::HftAbsOption*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftAbsOption*>

namespace ROOT {
   static TClass *setlETStringgR_Dictionary();
   static void setlETStringgR_TClassManip(TClass*);
   static void *new_setlETStringgR(void *p = 0);
   static void *newArray_setlETStringgR(Long_t size, void *p);
   static void delete_setlETStringgR(void *p);
   static void deleteArray_setlETStringgR(void *p);
   static void destruct_setlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const set<TString>*)
   {
      set<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(set<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("set<TString>", -2, "set", 90,
                  typeid(set<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &setlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(set<TString>) );
      instance.SetNew(&new_setlETStringgR);
      instance.SetNewArray(&newArray_setlETStringgR);
      instance.SetDelete(&delete_setlETStringgR);
      instance.SetDeleteArray(&deleteArray_setlETStringgR);
      instance.SetDestructor(&destruct_setlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Insert< set<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const set<TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *setlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const set<TString>*)0x0)->GetClass();
      setlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void setlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_setlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) set<TString> : new set<TString>;
   }
   static void *newArray_setlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) set<TString>[nElements] : new set<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_setlETStringgR(void *p) {
      delete ((set<TString>*)p);
   }
   static void deleteArray_setlETStringgR(void *p) {
      delete [] ((set<TString>*)p);
   }
   static void destruct_setlETStringgR(void *p) {
      typedef set<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class set<TString>

namespace ROOT {
   static TClass *maplETStringcOvectorlEsetlETStringgRsPgRsPgR_Dictionary();
   static void maplETStringcOvectorlEsetlETStringgRsPgRsPgR_TClassManip(TClass*);
   static void *new_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p = 0);
   static void *newArray_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(Long_t size, void *p);
   static void delete_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p);
   static void deleteArray_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p);
   static void destruct_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,vector<set<TString> > >*)
   {
      map<TString,vector<set<TString> > > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,vector<set<TString> > >));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,vector<set<TString> > >", -2, "map", 96,
                  typeid(map<TString,vector<set<TString> > >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOvectorlEsetlETStringgRsPgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,vector<set<TString> > >) );
      instance.SetNew(&new_maplETStringcOvectorlEsetlETStringgRsPgRsPgR);
      instance.SetNewArray(&newArray_maplETStringcOvectorlEsetlETStringgRsPgRsPgR);
      instance.SetDelete(&delete_maplETStringcOvectorlEsetlETStringgRsPgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOvectorlEsetlETStringgRsPgRsPgR);
      instance.SetDestructor(&destruct_maplETStringcOvectorlEsetlETStringgRsPgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,vector<set<TString> > > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,vector<set<TString> > >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOvectorlEsetlETStringgRsPgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,vector<set<TString> > >*)0x0)->GetClass();
      maplETStringcOvectorlEsetlETStringgRsPgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOvectorlEsetlETStringgRsPgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<set<TString> > > : new map<TString,vector<set<TString> > >;
   }
   static void *newArray_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<set<TString> > >[nElements] : new map<TString,vector<set<TString> > >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p) {
      delete ((map<TString,vector<set<TString> > >*)p);
   }
   static void deleteArray_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p) {
      delete [] ((map<TString,vector<set<TString> > >*)p);
   }
   static void destruct_maplETStringcOvectorlEsetlETStringgRsPgRsPgR(void *p) {
      typedef map<TString,vector<set<TString> > > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,vector<set<TString> > >

namespace ROOT {
   static TClass *maplETStringcOvectorlEdoublegRsPgR_Dictionary();
   static void maplETStringcOvectorlEdoublegRsPgR_TClassManip(TClass*);
   static void *new_maplETStringcOvectorlEdoublegRsPgR(void *p = 0);
   static void *newArray_maplETStringcOvectorlEdoublegRsPgR(Long_t size, void *p);
   static void delete_maplETStringcOvectorlEdoublegRsPgR(void *p);
   static void deleteArray_maplETStringcOvectorlEdoublegRsPgR(void *p);
   static void destruct_maplETStringcOvectorlEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,vector<double> >*)
   {
      map<TString,vector<double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,vector<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,vector<double> >", -2, "map", 96,
                  typeid(map<TString,vector<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOvectorlEdoublegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,vector<double> >) );
      instance.SetNew(&new_maplETStringcOvectorlEdoublegRsPgR);
      instance.SetNewArray(&newArray_maplETStringcOvectorlEdoublegRsPgR);
      instance.SetDelete(&delete_maplETStringcOvectorlEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOvectorlEdoublegRsPgR);
      instance.SetDestructor(&destruct_maplETStringcOvectorlEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,vector<double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,vector<double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOvectorlEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,vector<double> >*)0x0)->GetClass();
      maplETStringcOvectorlEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOvectorlEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOvectorlEdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<double> > : new map<TString,vector<double> >;
   }
   static void *newArray_maplETStringcOvectorlEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<double> >[nElements] : new map<TString,vector<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOvectorlEdoublegRsPgR(void *p) {
      delete ((map<TString,vector<double> >*)p);
   }
   static void deleteArray_maplETStringcOvectorlEdoublegRsPgR(void *p) {
      delete [] ((map<TString,vector<double> >*)p);
   }
   static void destruct_maplETStringcOvectorlEdoublegRsPgR(void *p) {
      typedef map<TString,vector<double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,vector<double> >

namespace ROOT {
   static TClass *maplETStringcOvectorlETStringgRsPgR_Dictionary();
   static void maplETStringcOvectorlETStringgRsPgR_TClassManip(TClass*);
   static void *new_maplETStringcOvectorlETStringgRsPgR(void *p = 0);
   static void *newArray_maplETStringcOvectorlETStringgRsPgR(Long_t size, void *p);
   static void delete_maplETStringcOvectorlETStringgRsPgR(void *p);
   static void deleteArray_maplETStringcOvectorlETStringgRsPgR(void *p);
   static void destruct_maplETStringcOvectorlETStringgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,vector<TString> >*)
   {
      map<TString,vector<TString> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,vector<TString> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,vector<TString> >", -2, "map", 96,
                  typeid(map<TString,vector<TString> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOvectorlETStringgRsPgR_Dictionary, isa_proxy, 4,
                  sizeof(map<TString,vector<TString> >) );
      instance.SetNew(&new_maplETStringcOvectorlETStringgRsPgR);
      instance.SetNewArray(&newArray_maplETStringcOvectorlETStringgRsPgR);
      instance.SetDelete(&delete_maplETStringcOvectorlETStringgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOvectorlETStringgRsPgR);
      instance.SetDestructor(&destruct_maplETStringcOvectorlETStringgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,vector<TString> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,vector<TString> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOvectorlETStringgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,vector<TString> >*)0x0)->GetClass();
      maplETStringcOvectorlETStringgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOvectorlETStringgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOvectorlETStringgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<TString> > : new map<TString,vector<TString> >;
   }
   static void *newArray_maplETStringcOvectorlETStringgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<TString> >[nElements] : new map<TString,vector<TString> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOvectorlETStringgRsPgR(void *p) {
      delete ((map<TString,vector<TString> >*)p);
   }
   static void deleteArray_maplETStringcOvectorlETStringgRsPgR(void *p) {
      delete [] ((map<TString,vector<TString> >*)p);
   }
   static void destruct_maplETStringcOvectorlETStringgRsPgR(void *p) {
      typedef map<TString,vector<TString> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,vector<TString> >

namespace ROOT {
   static TClass *maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR_Dictionary();
   static void maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR_TClassManip(TClass*);
   static void *new_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p = 0);
   static void *newArray_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(Long_t size, void *p);
   static void delete_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p);
   static void deleteArray_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p);
   static void destruct_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,vector<Hfitter::HftPdfBuilderSpec> >*)
   {
      map<TString,vector<Hfitter::HftPdfBuilderSpec> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,vector<Hfitter::HftPdfBuilderSpec> >));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,vector<Hfitter::HftPdfBuilderSpec> >", -2, "map", 96,
                  typeid(map<TString,vector<Hfitter::HftPdfBuilderSpec> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,vector<Hfitter::HftPdfBuilderSpec> >) );
      instance.SetNew(&new_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR);
      instance.SetNewArray(&newArray_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR);
      instance.SetDelete(&delete_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR);
      instance.SetDestructor(&destruct_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,vector<Hfitter::HftPdfBuilderSpec> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,vector<Hfitter::HftPdfBuilderSpec> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,vector<Hfitter::HftPdfBuilderSpec> >*)0x0)->GetClass();
      maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<Hfitter::HftPdfBuilderSpec> > : new map<TString,vector<Hfitter::HftPdfBuilderSpec> >;
   }
   static void *newArray_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,vector<Hfitter::HftPdfBuilderSpec> >[nElements] : new map<TString,vector<Hfitter::HftPdfBuilderSpec> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p) {
      delete ((map<TString,vector<Hfitter::HftPdfBuilderSpec> >*)p);
   }
   static void deleteArray_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p) {
      delete [] ((map<TString,vector<Hfitter::HftPdfBuilderSpec> >*)p);
   }
   static void destruct_maplETStringcOvectorlEHfittercLcLHftPdfBuilderSpecgRsPgR(void *p) {
      typedef map<TString,vector<Hfitter::HftPdfBuilderSpec> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,vector<Hfitter::HftPdfBuilderSpec> >

namespace ROOT {
   static TClass *maplETStringcOintgR_Dictionary();
   static void maplETStringcOintgR_TClassManip(TClass*);
   static void *new_maplETStringcOintgR(void *p = 0);
   static void *newArray_maplETStringcOintgR(Long_t size, void *p);
   static void delete_maplETStringcOintgR(void *p);
   static void deleteArray_maplETStringcOintgR(void *p);
   static void destruct_maplETStringcOintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,int>*)
   {
      map<TString,int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,int>", -2, "map", 96,
                  typeid(map<TString,int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,int>) );
      instance.SetNew(&new_maplETStringcOintgR);
      instance.SetNewArray(&newArray_maplETStringcOintgR);
      instance.SetDelete(&delete_maplETStringcOintgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOintgR);
      instance.SetDestructor(&destruct_maplETStringcOintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,int>*)0x0)->GetClass();
      maplETStringcOintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,int> : new map<TString,int>;
   }
   static void *newArray_maplETStringcOintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,int>[nElements] : new map<TString,int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOintgR(void *p) {
      delete ((map<TString,int>*)p);
   }
   static void deleteArray_maplETStringcOintgR(void *p) {
      delete [] ((map<TString,int>*)p);
   }
   static void destruct_maplETStringcOintgR(void *p) {
      typedef map<TString,int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,int>

namespace ROOT {
   static TClass *maplETStringcOboolgR_Dictionary();
   static void maplETStringcOboolgR_TClassManip(TClass*);
   static void *new_maplETStringcOboolgR(void *p = 0);
   static void *newArray_maplETStringcOboolgR(Long_t size, void *p);
   static void delete_maplETStringcOboolgR(void *p);
   static void deleteArray_maplETStringcOboolgR(void *p);
   static void destruct_maplETStringcOboolgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,bool>*)
   {
      map<TString,bool> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,bool>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,bool>", -2, "map", 96,
                  typeid(map<TString,bool>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOboolgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,bool>) );
      instance.SetNew(&new_maplETStringcOboolgR);
      instance.SetNewArray(&newArray_maplETStringcOboolgR);
      instance.SetDelete(&delete_maplETStringcOboolgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOboolgR);
      instance.SetDestructor(&destruct_maplETStringcOboolgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,bool> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,bool>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOboolgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,bool>*)0x0)->GetClass();
      maplETStringcOboolgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOboolgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOboolgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,bool> : new map<TString,bool>;
   }
   static void *newArray_maplETStringcOboolgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,bool>[nElements] : new map<TString,bool>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOboolgR(void *p) {
      delete ((map<TString,bool>*)p);
   }
   static void deleteArray_maplETStringcOboolgR(void *p) {
      delete [] ((map<TString,bool>*)p);
   }
   static void destruct_maplETStringcOboolgR(void *p) {
      typedef map<TString,bool> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,bool>

namespace ROOT {
   static TClass *maplETStringcOTStringgR_Dictionary();
   static void maplETStringcOTStringgR_TClassManip(TClass*);
   static void *new_maplETStringcOTStringgR(void *p = 0);
   static void *newArray_maplETStringcOTStringgR(Long_t size, void *p);
   static void delete_maplETStringcOTStringgR(void *p);
   static void deleteArray_maplETStringcOTStringgR(void *p);
   static void destruct_maplETStringcOTStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,TString>*)
   {
      map<TString,TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,TString>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,TString>", -2, "map", 96,
                  typeid(map<TString,TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOTStringgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,TString>) );
      instance.SetNew(&new_maplETStringcOTStringgR);
      instance.SetNewArray(&newArray_maplETStringcOTStringgR);
      instance.SetDelete(&delete_maplETStringcOTStringgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOTStringgR);
      instance.SetDestructor(&destruct_maplETStringcOTStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOTStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,TString>*)0x0)->GetClass();
      maplETStringcOTStringgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOTStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOTStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,TString> : new map<TString,TString>;
   }
   static void *newArray_maplETStringcOTStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,TString>[nElements] : new map<TString,TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOTStringgR(void *p) {
      delete ((map<TString,TString>*)p);
   }
   static void deleteArray_maplETStringcOTStringgR(void *p) {
      delete [] ((map<TString,TString>*)p);
   }
   static void destruct_maplETStringcOTStringgR(void *p) {
      typedef map<TString,TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,TString>

namespace ROOT {
   static TClass *maplETStringcOHfittercLcLInterpolationSpecgR_Dictionary();
   static void maplETStringcOHfittercLcLInterpolationSpecgR_TClassManip(TClass*);
   static void *new_maplETStringcOHfittercLcLInterpolationSpecgR(void *p = 0);
   static void *newArray_maplETStringcOHfittercLcLInterpolationSpecgR(Long_t size, void *p);
   static void delete_maplETStringcOHfittercLcLInterpolationSpecgR(void *p);
   static void deleteArray_maplETStringcOHfittercLcLInterpolationSpecgR(void *p);
   static void destruct_maplETStringcOHfittercLcLInterpolationSpecgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,Hfitter::InterpolationSpec>*)
   {
      map<TString,Hfitter::InterpolationSpec> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,Hfitter::InterpolationSpec>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,Hfitter::InterpolationSpec>", -2, "map", 96,
                  typeid(map<TString,Hfitter::InterpolationSpec>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOHfittercLcLInterpolationSpecgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,Hfitter::InterpolationSpec>) );
      instance.SetNew(&new_maplETStringcOHfittercLcLInterpolationSpecgR);
      instance.SetNewArray(&newArray_maplETStringcOHfittercLcLInterpolationSpecgR);
      instance.SetDelete(&delete_maplETStringcOHfittercLcLInterpolationSpecgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOHfittercLcLInterpolationSpecgR);
      instance.SetDestructor(&destruct_maplETStringcOHfittercLcLInterpolationSpecgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,Hfitter::InterpolationSpec> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,Hfitter::InterpolationSpec>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOHfittercLcLInterpolationSpecgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,Hfitter::InterpolationSpec>*)0x0)->GetClass();
      maplETStringcOHfittercLcLInterpolationSpecgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOHfittercLcLInterpolationSpecgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOHfittercLcLInterpolationSpecgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::InterpolationSpec> : new map<TString,Hfitter::InterpolationSpec>;
   }
   static void *newArray_maplETStringcOHfittercLcLInterpolationSpecgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::InterpolationSpec>[nElements] : new map<TString,Hfitter::InterpolationSpec>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOHfittercLcLInterpolationSpecgR(void *p) {
      delete ((map<TString,Hfitter::InterpolationSpec>*)p);
   }
   static void deleteArray_maplETStringcOHfittercLcLInterpolationSpecgR(void *p) {
      delete [] ((map<TString,Hfitter::InterpolationSpec>*)p);
   }
   static void destruct_maplETStringcOHfittercLcLInterpolationSpecgR(void *p) {
      typedef map<TString,Hfitter::InterpolationSpec> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,Hfitter::InterpolationSpec>

namespace ROOT {
   static TClass *maplETStringcOHfittercLcLHftRooRealVarStoragegR_Dictionary();
   static void maplETStringcOHfittercLcLHftRooRealVarStoragegR_TClassManip(TClass*);
   static void *new_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p = 0);
   static void *newArray_maplETStringcOHfittercLcLHftRooRealVarStoragegR(Long_t size, void *p);
   static void delete_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p);
   static void deleteArray_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p);
   static void destruct_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,Hfitter::HftRooRealVarStorage>*)
   {
      map<TString,Hfitter::HftRooRealVarStorage> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,Hfitter::HftRooRealVarStorage>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,Hfitter::HftRooRealVarStorage>", -2, "map", 96,
                  typeid(map<TString,Hfitter::HftRooRealVarStorage>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOHfittercLcLHftRooRealVarStoragegR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,Hfitter::HftRooRealVarStorage>) );
      instance.SetNew(&new_maplETStringcOHfittercLcLHftRooRealVarStoragegR);
      instance.SetNewArray(&newArray_maplETStringcOHfittercLcLHftRooRealVarStoragegR);
      instance.SetDelete(&delete_maplETStringcOHfittercLcLHftRooRealVarStoragegR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOHfittercLcLHftRooRealVarStoragegR);
      instance.SetDestructor(&destruct_maplETStringcOHfittercLcLHftRooRealVarStoragegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,Hfitter::HftRooRealVarStorage> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,Hfitter::HftRooRealVarStorage>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOHfittercLcLHftRooRealVarStoragegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,Hfitter::HftRooRealVarStorage>*)0x0)->GetClass();
      maplETStringcOHfittercLcLHftRooRealVarStoragegR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOHfittercLcLHftRooRealVarStoragegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftRooRealVarStorage> : new map<TString,Hfitter::HftRooRealVarStorage>;
   }
   static void *newArray_maplETStringcOHfittercLcLHftRooRealVarStoragegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftRooRealVarStorage>[nElements] : new map<TString,Hfitter::HftRooRealVarStorage>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p) {
      delete ((map<TString,Hfitter::HftRooRealVarStorage>*)p);
   }
   static void deleteArray_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p) {
      delete [] ((map<TString,Hfitter::HftRooRealVarStorage>*)p);
   }
   static void destruct_maplETStringcOHfittercLcLHftRooRealVarStoragegR(void *p) {
      typedef map<TString,Hfitter::HftRooRealVarStorage> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,Hfitter::HftRooRealVarStorage>

namespace ROOT {
   static TClass *maplETStringcOHfittercLcLHftPdfBuilderSpecgR_Dictionary();
   static void maplETStringcOHfittercLcLHftPdfBuilderSpecgR_TClassManip(TClass*);
   static void *new_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p = 0);
   static void *newArray_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(Long_t size, void *p);
   static void delete_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p);
   static void deleteArray_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p);
   static void destruct_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,Hfitter::HftPdfBuilderSpec>*)
   {
      map<TString,Hfitter::HftPdfBuilderSpec> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,Hfitter::HftPdfBuilderSpec>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,Hfitter::HftPdfBuilderSpec>", -2, "map", 96,
                  typeid(map<TString,Hfitter::HftPdfBuilderSpec>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOHfittercLcLHftPdfBuilderSpecgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,Hfitter::HftPdfBuilderSpec>) );
      instance.SetNew(&new_maplETStringcOHfittercLcLHftPdfBuilderSpecgR);
      instance.SetNewArray(&newArray_maplETStringcOHfittercLcLHftPdfBuilderSpecgR);
      instance.SetDelete(&delete_maplETStringcOHfittercLcLHftPdfBuilderSpecgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOHfittercLcLHftPdfBuilderSpecgR);
      instance.SetDestructor(&destruct_maplETStringcOHfittercLcLHftPdfBuilderSpecgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,Hfitter::HftPdfBuilderSpec> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,Hfitter::HftPdfBuilderSpec>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOHfittercLcLHftPdfBuilderSpecgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,Hfitter::HftPdfBuilderSpec>*)0x0)->GetClass();
      maplETStringcOHfittercLcLHftPdfBuilderSpecgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOHfittercLcLHftPdfBuilderSpecgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftPdfBuilderSpec> : new map<TString,Hfitter::HftPdfBuilderSpec>;
   }
   static void *newArray_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftPdfBuilderSpec>[nElements] : new map<TString,Hfitter::HftPdfBuilderSpec>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p) {
      delete ((map<TString,Hfitter::HftPdfBuilderSpec>*)p);
   }
   static void deleteArray_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p) {
      delete [] ((map<TString,Hfitter::HftPdfBuilderSpec>*)p);
   }
   static void destruct_maplETStringcOHfittercLcLHftPdfBuilderSpecgR(void *p) {
      typedef map<TString,Hfitter::HftPdfBuilderSpec> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,Hfitter::HftPdfBuilderSpec>

namespace ROOT {
   static TClass *maplETStringcOHfittercLcLHftParameterStoragegR_Dictionary();
   static void maplETStringcOHfittercLcLHftParameterStoragegR_TClassManip(TClass*);
   static void *new_maplETStringcOHfittercLcLHftParameterStoragegR(void *p = 0);
   static void *newArray_maplETStringcOHfittercLcLHftParameterStoragegR(Long_t size, void *p);
   static void delete_maplETStringcOHfittercLcLHftParameterStoragegR(void *p);
   static void deleteArray_maplETStringcOHfittercLcLHftParameterStoragegR(void *p);
   static void destruct_maplETStringcOHfittercLcLHftParameterStoragegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,Hfitter::HftParameterStorage>*)
   {
      map<TString,Hfitter::HftParameterStorage> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,Hfitter::HftParameterStorage>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,Hfitter::HftParameterStorage>", -2, "map", 96,
                  typeid(map<TString,Hfitter::HftParameterStorage>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOHfittercLcLHftParameterStoragegR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,Hfitter::HftParameterStorage>) );
      instance.SetNew(&new_maplETStringcOHfittercLcLHftParameterStoragegR);
      instance.SetNewArray(&newArray_maplETStringcOHfittercLcLHftParameterStoragegR);
      instance.SetDelete(&delete_maplETStringcOHfittercLcLHftParameterStoragegR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOHfittercLcLHftParameterStoragegR);
      instance.SetDestructor(&destruct_maplETStringcOHfittercLcLHftParameterStoragegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,Hfitter::HftParameterStorage> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,Hfitter::HftParameterStorage>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOHfittercLcLHftParameterStoragegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,Hfitter::HftParameterStorage>*)0x0)->GetClass();
      maplETStringcOHfittercLcLHftParameterStoragegR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOHfittercLcLHftParameterStoragegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOHfittercLcLHftParameterStoragegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftParameterStorage> : new map<TString,Hfitter::HftParameterStorage>;
   }
   static void *newArray_maplETStringcOHfittercLcLHftParameterStoragegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftParameterStorage>[nElements] : new map<TString,Hfitter::HftParameterStorage>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOHfittercLcLHftParameterStoragegR(void *p) {
      delete ((map<TString,Hfitter::HftParameterStorage>*)p);
   }
   static void deleteArray_maplETStringcOHfittercLcLHftParameterStoragegR(void *p) {
      delete [] ((map<TString,Hfitter::HftParameterStorage>*)p);
   }
   static void destruct_maplETStringcOHfittercLcLHftParameterStoragegR(void *p) {
      typedef map<TString,Hfitter::HftParameterStorage> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,Hfitter::HftParameterStorage>

namespace ROOT {
   static TClass *maplETStringcOHfittercLcLHftFunctionBuilderSpecgR_Dictionary();
   static void maplETStringcOHfittercLcLHftFunctionBuilderSpecgR_TClassManip(TClass*);
   static void *new_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p = 0);
   static void *newArray_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(Long_t size, void *p);
   static void delete_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p);
   static void deleteArray_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p);
   static void destruct_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,Hfitter::HftFunctionBuilderSpec>*)
   {
      map<TString,Hfitter::HftFunctionBuilderSpec> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,Hfitter::HftFunctionBuilderSpec>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,Hfitter::HftFunctionBuilderSpec>", -2, "map", 96,
                  typeid(map<TString,Hfitter::HftFunctionBuilderSpec>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOHfittercLcLHftFunctionBuilderSpecgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,Hfitter::HftFunctionBuilderSpec>) );
      instance.SetNew(&new_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR);
      instance.SetNewArray(&newArray_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR);
      instance.SetDelete(&delete_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR);
      instance.SetDestructor(&destruct_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,Hfitter::HftFunctionBuilderSpec> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,Hfitter::HftFunctionBuilderSpec>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOHfittercLcLHftFunctionBuilderSpecgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,Hfitter::HftFunctionBuilderSpec>*)0x0)->GetClass();
      maplETStringcOHfittercLcLHftFunctionBuilderSpecgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOHfittercLcLHftFunctionBuilderSpecgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftFunctionBuilderSpec> : new map<TString,Hfitter::HftFunctionBuilderSpec>;
   }
   static void *newArray_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,Hfitter::HftFunctionBuilderSpec>[nElements] : new map<TString,Hfitter::HftFunctionBuilderSpec>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p) {
      delete ((map<TString,Hfitter::HftFunctionBuilderSpec>*)p);
   }
   static void deleteArray_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p) {
      delete [] ((map<TString,Hfitter::HftFunctionBuilderSpec>*)p);
   }
   static void destruct_maplETStringcOHfittercLcLHftFunctionBuilderSpecgR(void *p) {
      typedef map<TString,Hfitter::HftFunctionBuilderSpec> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,Hfitter::HftFunctionBuilderSpec>

namespace {
  void TriggerDictionaryInitialization_HfitterModelsCINT_Impl() {
    static const char* headers[] = {
"HfitterModels/Options.h",
"HfitterModels/HftAbsParameters.h",
"HfitterModels/HftParameterSet.h",
"HfitterModels/HftStorableParameters.h",
"HfitterModels/HftParameterStorage.h",
"HfitterModels/HftRealMorphVar.h",
"HfitterModels/HftDatacardReader.h",
"HfitterModels/HftTools.h",
"HfitterModels/HftModel.h",
"HfitterModels/HftConstraint.h",
"HfitterModels/HftInstance.h",
"HfitterModels/HftAbsPdfBuilder.h",
"HfitterModels/HftAbsNodeBuilder.h",
"HfitterModels/HftAbsRealBuilder.h",
"HfitterModels/HftModelBuilder.h",
"HfitterModels/HftModelSplitter.h",
"HfitterModels/HftData.h",
"HfitterModels/HftDataHist.h",
"HfitterModels/HftCountingPdfBuilder.h",
"HfitterModels/HftHistPdfBuilder.h",
"HfitterModels/HftFactoryPdfBuilder.h",
"HfitterModels/HftWorkspacePdfBuilder.h",
"HfitterModels/HftGenericRealBuilder.h",
"HfitterModels/HftTemplatePdfBuilder.h",
"HfitterModels/HftGenericPdfBuilder.h",
"HfitterModels/HftMultiGaussianBuilder.h",
"HfitterModels/HftPolyPdfBuilder.h",
"HfitterModels/HftDoubleGaussian.h",
"HfitterModels/HftPolyExp.h",
"HfitterModels/HftFermiPolyExp.h",
"HfitterModels/HftDeltaPdf.h",
"HfitterModels/HftPeggedPoly.h",
"HfitterModels/HftTruncPoly.h",
"HfitterModels/FlexibleInterpVarMkII.h",
"HfitterModels/HftSPlot.h",
"HfitterModels/HftModelBuilderV4.h",
"HfitterModels/HftDatacardReaderV4.h",
"HfitterModels/HftModelSplitterV4.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/HfitterModels/Root",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/HfitterModels",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.14.04-0d8dc/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/HfitterModels/cmt/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "HfitterModelsCINT dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftModel.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftDatacardReader.h")))  HftModel;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftParameterStorage.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftParameterSet.h")))  HftParameterStorage;}
class __attribute__((annotate("$clingAutoload$TString.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/Options.h")))  TString;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/Options.h")))  HftAbsOption;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/Options.h")))  RooFitOpt;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/Options.h")))  StrOpt;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/Options.h")))  NoDefaults;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/Options.h")))  Options;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftAbsParameters.h")))  HftAbsParameters;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftStorableParameters.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftParameterSet.h")))  HftStorableParameters;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftParameterSet.h")))  HftParameterSet;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftRealMorphVar.h")))  HftRealMorphVar;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftConstraint.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftDatacardReader.h")))  HftConstraint;}
namespace Hfitter{struct __attribute__((annotate("$clingAutoload$HfitterModels/HftDatacardReader.h")))  HftPdfBuilderSpec;}
namespace Hfitter{struct __attribute__((annotate("$clingAutoload$HfitterModels/HftDatacardReader.h")))  HftFunctionBuilderSpec;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftDatacardReader.h")))  HftDatacardReader;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftInstance.h")))  HftInstance;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftAbsNodeBuilder.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftAbsPdfBuilder.h")))  HftAbsNodeBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftAbsPdfBuilder.h")))  HftAbsPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftAbsRealBuilder.h")))  HftAbsRealBuilder;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftModelBuilder.h")))  HftModelBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftModelSplitter.h")))  HftModelSplitter;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftData.h")))  HftData;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftDataHist.h")))  HftDataHist;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftCountingPdfBuilder.h")))  HftCountingPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftHistPdfBuilder.h")))  HftHistPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftFactoryPdfBuilder.h")))  HftFactoryPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftWorkspacePdfBuilder.h")))  HftWorkspacePdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftGenericRealBuilder.h")))  HftGenericRealBuilder;}
class __attribute__((annotate("$clingAutoload$RooUniform.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftDoubleGaussian.h")))  RooUniform;
namespace Hfitter{template <class T, unsigned int NPAR> class __attribute__((annotate("$clingAutoload$HfitterModels/HftTemplatePdfBuilder.h")))  HftTemplatePdfBuilder;
}
class __attribute__((annotate("$clingAutoload$RooLandau.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftDoubleGaussian.h")))  RooLandau;
class __attribute__((annotate("$clingAutoload$RooBifurGauss.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftDoubleGaussian.h")))  RooBifurGauss;
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftDoubleGaussian.h")))  HftDoubleGaussian;}
class __attribute__((annotate("$clingAutoload$RooGaussian.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftDoubleGaussian.h")))  RooGaussian;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftGenericPdfBuilder.h")))  HftGenericPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftMultiGaussianBuilder.h")))  HftMultiGaussianBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftPolyPdfBuilder.h")))  HftPolyPdfBuilder;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftPolyExp.h")))  HftPolyExp;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftFermiPolyExp.h")))  HftFermiPolyExp;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftDeltaPdf.h")))  HftDeltaPdf;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftPeggedPoly.h")))  HftPeggedPoly;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftTruncPoly.h")))  HftTruncPoly;}
class __attribute__((annotate(R"ATTRDUMP(flexible interpolation)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/FlexibleInterpVarMkII.h")))  FlexibleInterpVarMkII;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftSPlot.h")))  HftSPlot;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftDatacardReaderV4.h")))  __attribute__((annotate("$clingAutoload$HfitterModels/HftModelBuilderV4.h")))  HftDatacardReaderV4;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(ROOT boilerplate)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HfitterModels/HftModelBuilderV4.h")))  HftModelBuilderV4;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterModels/HftModelSplitterV4.h")))  HftModelSplitterV4;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "HfitterModelsCINT dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "HfitterModels"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HfitterModels/Options.h"
#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftStorableParameters.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/HftRealMorphVar.h"
#include "HfitterModels/HftDatacardReader.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftConstraint.h"
#include "HfitterModels/HftInstance.h"
#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HfitterModels/HftAbsNodeBuilder.h"
#include "HfitterModels/HftAbsRealBuilder.h"
#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftModelSplitter.h"
#include "HfitterModels/HftData.h"
#include "HfitterModels/HftDataHist.h"
#include "HfitterModels/HftCountingPdfBuilder.h"
#include "HfitterModels/HftHistPdfBuilder.h"
#include "HfitterModels/HftFactoryPdfBuilder.h"
#include "HfitterModels/HftWorkspacePdfBuilder.h"
#include "HfitterModels/HftGenericRealBuilder.h"
#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "HfitterModels/HftGenericPdfBuilder.h"
#include "HfitterModels/HftMultiGaussianBuilder.h"
#include "HfitterModels/HftPolyPdfBuilder.h"
#include "HfitterModels/HftDoubleGaussian.h"
#include "HfitterModels/HftPolyExp.h"
#include "HfitterModels/HftFermiPolyExp.h"
#include "HfitterModels/HftDeltaPdf.h"
#include "HfitterModels/HftPeggedPoly.h"
#include "HfitterModels/HftTruncPoly.h"
#include "HfitterModels/FlexibleInterpVarMkII.h"
#include "HfitterModels/HftSPlot.h"
#include "HfitterModels/HftModelBuilderV4.h"
#include "HfitterModels/HftDatacardReaderV4.h"
#include "HfitterModels/HftModelSplitterV4.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"FlexibleInterpVarMkII", payloadCode, "@",
"Hfitter::HftAbsNodeBuilder", payloadCode, "@",
"Hfitter::HftAbsOption", payloadCode, "@",
"Hfitter::HftAbsParameters", payloadCode, "@",
"Hfitter::HftAbsPdfBuilder", payloadCode, "@",
"Hfitter::HftAbsRealBuilder", payloadCode, "@",
"Hfitter::HftConstraint", payloadCode, "@",
"Hfitter::HftCountingPdfBuilder", payloadCode, "@",
"Hfitter::HftData", payloadCode, "@",
"Hfitter::HftDataHist", payloadCode, "@",
"Hfitter::HftDatacardReader", payloadCode, "@",
"Hfitter::HftDatacardReaderV4", payloadCode, "@",
"Hfitter::HftDeltaPdf", payloadCode, "@",
"Hfitter::HftDoubleGaussian", payloadCode, "@",
"Hfitter::HftFactoryPdfBuilder", payloadCode, "@",
"Hfitter::HftFermiPolyExp", payloadCode, "@",
"Hfitter::HftFunctionBuilderSpec", payloadCode, "@",
"Hfitter::HftGenericPdfBuilder", payloadCode, "@",
"Hfitter::HftGenericRealBuilder", payloadCode, "@",
"Hfitter::HftHistPdfBuilder", payloadCode, "@",
"Hfitter::HftInstance", payloadCode, "@",
"Hfitter::HftModel", payloadCode, "@",
"Hfitter::HftModelBuilder", payloadCode, "@",
"Hfitter::HftModelBuilderV4", payloadCode, "@",
"Hfitter::HftModelSplitter", payloadCode, "@",
"Hfitter::HftModelSplitterV4", payloadCode, "@",
"Hfitter::HftMultiGaussianBuilder", payloadCode, "@",
"Hfitter::HftParameterSet", payloadCode, "@",
"Hfitter::HftParameterStorage", payloadCode, "@",
"Hfitter::HftPdfBuilderSpec", payloadCode, "@",
"Hfitter::HftPeggedPoly", payloadCode, "@",
"Hfitter::HftPolyExp", payloadCode, "@",
"Hfitter::HftPolyPdfBuilder", payloadCode, "@",
"Hfitter::HftRealMorphVar", payloadCode, "@",
"Hfitter::HftSPlot", payloadCode, "@",
"Hfitter::HftStorableParameters", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian,5>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<RooBifurGauss,3>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<RooGaussian,2>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<RooLandau,2>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<RooUniform,0>", payloadCode, "@",
"Hfitter::HftTruncPoly", payloadCode, "@",
"Hfitter::HftWorkspacePdfBuilder", payloadCode, "@",
"Hfitter::NoDefaults", payloadCode, "@",
"Hfitter::Options", payloadCode, "@",
"Hfitter::RooFitOpt", payloadCode, "@",
"Hfitter::StrOpt", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("HfitterModelsCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_HfitterModelsCINT_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_HfitterModelsCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_HfitterModelsCINT() {
  TriggerDictionaryInitialization_HfitterModelsCINT_Impl();
}
