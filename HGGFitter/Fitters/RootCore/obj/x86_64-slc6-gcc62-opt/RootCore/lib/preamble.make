RC_CXX       = g++
RC_LD        = g++
RC_CXXFLAGS  = -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/Root -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore -O2 -Wall -fPIC -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include -g -Wno-tautological-undefined-compare -DROOTCORE -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -pipe -W -Wall -Wno-deprecated -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long -Wdeprecated-declarations -DROOTCORE_PACKAGE=\"RootCore\" 
RC_DICTFLAGS = -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/Root -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore -O2 -Wall -fPIC -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include -g -Wno-tautological-undefined-compare -DROOTCORE -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -pipe -W -Wall -Wno-deprecated -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long -Wdeprecated-declarations -DROOTCORE_PACKAGE=\"RootCore\" 
RC_INCFLAGS  = -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/Root -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include -DROOTCORE -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -DROOTCORE_PACKAGE=\"RootCore\"
RC_LIBFLAGS  = -shared -m64 -L/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTDataFrame -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic 
RC_BINFLAGS  = -L/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/RootCore/lib -L/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/lib/x86_64-slc6-gcc62-opt -m64 -L/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTDataFrame -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic


all_RootCore : dep_RootCore package_RootCore

package_RootCore :  postcompile_RootCore

postcompile_RootCore : 
	$(SILENT)rc --internal postcompile_pkg RootCore


dep_RootCore :
