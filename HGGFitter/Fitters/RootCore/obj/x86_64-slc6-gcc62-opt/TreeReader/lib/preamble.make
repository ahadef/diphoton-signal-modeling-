RC_CXX       = g++
RC_LD        = g++
RC_CXXFLAGS  = -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader -O2 -Wall -fPIC -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include -g -Wno-tautological-undefined-compare -DROOTCORE -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -pipe -W -Wall -Wno-deprecated -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long -Wdeprecated-declarations -DROOTCORE_PACKAGE=\"TreeReader\" 
RC_DICTFLAGS = -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader -O2 -Wall -fPIC -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include -g -Wno-tautological-undefined-compare -DROOTCORE -pthread -std=c++1y -m64 -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -pipe -W -Wall -Wno-deprecated -pedantic -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -Wno-long-long -Wdeprecated-declarations -DROOTCORE_PACKAGE=\"TreeReader\" 
RC_INCFLAGS  = -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -I/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include -DROOTCORE -I/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include -DROOTCORE_PACKAGE=\"TreeReader\"
RC_LIBFLAGS  = -shared -m64 -L/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTDataFrame -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic 
RC_BINFLAGS  = -L/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib -L/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/lib/x86_64-slc6-gcc62-opt -lTreeReader -m64 -L/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/lib -lCore -lImt -lRIO -lNet -lHist -lGraf -lGraf3d -lGpad -lROOTDataFrame -lROOTVecOps -lTree -lTreePlayer -lRint -lPostscript -lMatrix -lPhysics -lMathCore -lThread -lMultiProc -pthread -lm -ldl -rdynamic


all_TreeReader : dep_TreeReader package_TreeReader

package_TreeReader :  /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib/libTreeReader.so postcompile_TreeReader

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib/libTreeReader.so :  /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DiphoxReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/StdVectorReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/CutFlow.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeAbsReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/FamilyMember.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DataType.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/MultiReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.o | /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib
	$(SILENT)echo Linking `basename $@`
	$(SILENT)$(RC_LD) /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DiphoxReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/StdVectorReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/CutFlow.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeAbsReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/FamilyMember.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DataType.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/MultiReader.o /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.o $(RC_LIBFLAGS) -L/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/lib/x86_64-slc6-gcc62-opt -o $@

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/%.o : /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root/%.cxx | /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DiphoxReader.d
	$(SILENT)echo Compiling `basename $@`
	$(SILENT)rc --internal check_dep_cc TreeReader $@
	$(SILENT)$(RC_CXX) $(RC_CXXFLAGS) $(INCLUDES) -c $< -o $@

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/%.d : /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root/%.cxx | /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj
	$(SILENT)echo Making dependency for `basename $<`
	$(SILENT)rc --internal make_dep $(RC_CXX) $(RC_CXXFLAGS) $(INCLUDES)  -- $@ $< 

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj : 
	$(SILENT)echo Making directory $@
	$(SILENT)mkdir -p $@

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.o : /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root/LinkDef.h /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.headers | /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.d
	$(SILENT)echo Compiling `basename $@`
	$(SILENT)rc --internal check_dep_cc TreeReader $@
	$(SILENT)rc --internal rootcint $(ROOTSYS)/bin/rootcint $(RC_INCFLAGS) /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root/LinkDef.h /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.cxx /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.headers /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib TreeReader
	$(SILENT)$(RC_CXX) $(RC_DICTFLAGS) $(INCLUDES) -c /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.cxx -o $@

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.headers : /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root/LinkDef.h | /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj
	$(SILENT)echo Making dependency for `basename $<`
	$(SILENT)rc --internal make_dep $(RC_CXX) $(RC_CXXFLAGS) $(INCLUDES) -D__CINT__ -D__MAKECINT__ -D__CLING__ -Wno-unknown-pragmas -- $@ $< 

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.d : /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root/LinkDef.h | /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj
	$(SILENT)echo Making dependency for `basename $<`
	$(SILENT)rc --internal make_dep $(RC_CXX) $(RC_CXXFLAGS) $(INCLUDES)  -- $@ $< 

/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib : 
	$(SILENT)echo Making directory $@
	$(SILENT)mkdir -p $@

postcompile_TreeReader :  /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/lib/libTreeReader.so
	$(SILENT)rc --internal postcompile_pkg TreeReader


dep_TreeReader : /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DataType.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/FamilyMember.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/MultiReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeAbsReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/StdVectorReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DiphoxReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.headers /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/CutFlow.d


-include  /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DataType.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/FamilyMember.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/MultiReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeAbsReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/StdVectorReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/DiphoxReader.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.d /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/TreeReaderCINT.headers /afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/obj/x86_64-slc6-gcc62-opt/TreeReader/obj/CutFlow.d
