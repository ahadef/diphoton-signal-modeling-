#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIadIahadefdIpublicdIHGGFitterdIFittersdIRootCoredIobjdIx86_64mIslc6mIgcc62mIoptdITreeReaderdIobjdITreeReaderCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "TreeReader/TreeAbsReader.h"
#include "TreeReader/MultiReader.h"

// Header files passed via #pragma extra_include

namespace TreeReader {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *TreeReader_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("TreeReader", 0 /*version*/, "TreeReader/FamilyMember.h", 23,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &TreeReader_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *TreeReader_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *TreeReadercLcLFamilyMember_Dictionary();
   static void TreeReadercLcLFamilyMember_TClassManip(TClass*);
   static void *new_TreeReadercLcLFamilyMember(void *p = 0);
   static void *newArray_TreeReadercLcLFamilyMember(Long_t size, void *p);
   static void delete_TreeReadercLcLFamilyMember(void *p);
   static void deleteArray_TreeReadercLcLFamilyMember(void *p);
   static void destruct_TreeReadercLcLFamilyMember(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeReader::FamilyMember*)
   {
      ::TreeReader::FamilyMember *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TreeReader::FamilyMember));
      static ::ROOT::TGenericClassInfo 
         instance("TreeReader::FamilyMember", "TreeReader/FamilyMember.h", 25,
                  typeid(::TreeReader::FamilyMember), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &TreeReadercLcLFamilyMember_Dictionary, isa_proxy, 4,
                  sizeof(::TreeReader::FamilyMember) );
      instance.SetNew(&new_TreeReadercLcLFamilyMember);
      instance.SetNewArray(&newArray_TreeReadercLcLFamilyMember);
      instance.SetDelete(&delete_TreeReadercLcLFamilyMember);
      instance.SetDeleteArray(&deleteArray_TreeReadercLcLFamilyMember);
      instance.SetDestructor(&destruct_TreeReadercLcLFamilyMember);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeReader::FamilyMember*)
   {
      return GenerateInitInstanceLocal((::TreeReader::FamilyMember*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TreeReader::FamilyMember*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TreeReadercLcLFamilyMember_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TreeReader::FamilyMember*)0x0)->GetClass();
      TreeReadercLcLFamilyMember_TClassManip(theClass);
   return theClass;
   }

   static void TreeReadercLcLFamilyMember_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TreeReadercLcLTreeAbsReader_Dictionary();
   static void TreeReadercLcLTreeAbsReader_TClassManip(TClass*);
   static void delete_TreeReadercLcLTreeAbsReader(void *p);
   static void deleteArray_TreeReadercLcLTreeAbsReader(void *p);
   static void destruct_TreeReadercLcLTreeAbsReader(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeReader::TreeAbsReader*)
   {
      ::TreeReader::TreeAbsReader *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TreeReader::TreeAbsReader));
      static ::ROOT::TGenericClassInfo 
         instance("TreeReader::TreeAbsReader", "TreeReader/TreeAbsReader.h", 23,
                  typeid(::TreeReader::TreeAbsReader), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &TreeReadercLcLTreeAbsReader_Dictionary, isa_proxy, 4,
                  sizeof(::TreeReader::TreeAbsReader) );
      instance.SetDelete(&delete_TreeReadercLcLTreeAbsReader);
      instance.SetDeleteArray(&deleteArray_TreeReadercLcLTreeAbsReader);
      instance.SetDestructor(&destruct_TreeReadercLcLTreeAbsReader);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeReader::TreeAbsReader*)
   {
      return GenerateInitInstanceLocal((::TreeReader::TreeAbsReader*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TreeReader::TreeAbsReader*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TreeReadercLcLTreeAbsReader_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TreeReader::TreeAbsReader*)0x0)->GetClass();
      TreeReadercLcLTreeAbsReader_TClassManip(theClass);
   return theClass;
   }

   static void TreeReadercLcLTreeAbsReader_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TreeReadercLcLBranchSpec_Dictionary();
   static void TreeReadercLcLBranchSpec_TClassManip(TClass*);
   static void delete_TreeReadercLcLBranchSpec(void *p);
   static void deleteArray_TreeReadercLcLBranchSpec(void *p);
   static void destruct_TreeReadercLcLBranchSpec(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeReader::BranchSpec*)
   {
      ::TreeReader::BranchSpec *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TreeReader::BranchSpec));
      static ::ROOT::TGenericClassInfo 
         instance("TreeReader::BranchSpec", "TreeReader/DataType.h", 28,
                  typeid(::TreeReader::BranchSpec), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &TreeReadercLcLBranchSpec_Dictionary, isa_proxy, 4,
                  sizeof(::TreeReader::BranchSpec) );
      instance.SetDelete(&delete_TreeReadercLcLBranchSpec);
      instance.SetDeleteArray(&deleteArray_TreeReadercLcLBranchSpec);
      instance.SetDestructor(&destruct_TreeReadercLcLBranchSpec);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeReader::BranchSpec*)
   {
      return GenerateInitInstanceLocal((::TreeReader::BranchSpec*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TreeReader::BranchSpec*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TreeReadercLcLBranchSpec_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TreeReader::BranchSpec*)0x0)->GetClass();
      TreeReadercLcLBranchSpec_TClassManip(theClass);
   return theClass;
   }

   static void TreeReadercLcLBranchSpec_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *TreeReadercLcLMultiReader_Dictionary();
   static void TreeReadercLcLMultiReader_TClassManip(TClass*);
   static void *new_TreeReadercLcLMultiReader(void *p = 0);
   static void *newArray_TreeReadercLcLMultiReader(Long_t size, void *p);
   static void delete_TreeReadercLcLMultiReader(void *p);
   static void deleteArray_TreeReadercLcLMultiReader(void *p);
   static void destruct_TreeReadercLcLMultiReader(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::TreeReader::MultiReader*)
   {
      ::TreeReader::MultiReader *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::TreeReader::MultiReader));
      static ::ROOT::TGenericClassInfo 
         instance("TreeReader::MultiReader", "TreeReader/MultiReader.h", 23,
                  typeid(::TreeReader::MultiReader), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &TreeReadercLcLMultiReader_Dictionary, isa_proxy, 4,
                  sizeof(::TreeReader::MultiReader) );
      instance.SetNew(&new_TreeReadercLcLMultiReader);
      instance.SetNewArray(&newArray_TreeReadercLcLMultiReader);
      instance.SetDelete(&delete_TreeReadercLcLMultiReader);
      instance.SetDeleteArray(&deleteArray_TreeReadercLcLMultiReader);
      instance.SetDestructor(&destruct_TreeReadercLcLMultiReader);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::TreeReader::MultiReader*)
   {
      return GenerateInitInstanceLocal((::TreeReader::MultiReader*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::TreeReader::MultiReader*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *TreeReadercLcLMultiReader_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::TreeReader::MultiReader*)0x0)->GetClass();
      TreeReadercLcLMultiReader_TClassManip(theClass);
   return theClass;
   }

   static void TreeReadercLcLMultiReader_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_TreeReadercLcLFamilyMember(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::TreeReader::FamilyMember : new ::TreeReader::FamilyMember;
   }
   static void *newArray_TreeReadercLcLFamilyMember(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::TreeReader::FamilyMember[nElements] : new ::TreeReader::FamilyMember[nElements];
   }
   // Wrapper around operator delete
   static void delete_TreeReadercLcLFamilyMember(void *p) {
      delete ((::TreeReader::FamilyMember*)p);
   }
   static void deleteArray_TreeReadercLcLFamilyMember(void *p) {
      delete [] ((::TreeReader::FamilyMember*)p);
   }
   static void destruct_TreeReadercLcLFamilyMember(void *p) {
      typedef ::TreeReader::FamilyMember current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TreeReader::FamilyMember

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TreeReadercLcLTreeAbsReader(void *p) {
      delete ((::TreeReader::TreeAbsReader*)p);
   }
   static void deleteArray_TreeReadercLcLTreeAbsReader(void *p) {
      delete [] ((::TreeReader::TreeAbsReader*)p);
   }
   static void destruct_TreeReadercLcLTreeAbsReader(void *p) {
      typedef ::TreeReader::TreeAbsReader current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TreeReader::TreeAbsReader

namespace ROOT {
   // Wrapper around operator delete
   static void delete_TreeReadercLcLBranchSpec(void *p) {
      delete ((::TreeReader::BranchSpec*)p);
   }
   static void deleteArray_TreeReadercLcLBranchSpec(void *p) {
      delete [] ((::TreeReader::BranchSpec*)p);
   }
   static void destruct_TreeReadercLcLBranchSpec(void *p) {
      typedef ::TreeReader::BranchSpec current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TreeReader::BranchSpec

namespace ROOT {
   // Wrappers around operator new
   static void *new_TreeReadercLcLMultiReader(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::TreeReader::MultiReader : new ::TreeReader::MultiReader;
   }
   static void *newArray_TreeReadercLcLMultiReader(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::TreeReader::MultiReader[nElements] : new ::TreeReader::MultiReader[nElements];
   }
   // Wrapper around operator delete
   static void delete_TreeReadercLcLMultiReader(void *p) {
      delete ((::TreeReader::MultiReader*)p);
   }
   static void deleteArray_TreeReadercLcLMultiReader(void *p) {
      delete [] ((::TreeReader::MultiReader*)p);
   }
   static void destruct_TreeReadercLcLMultiReader(void *p) {
      typedef ::TreeReader::MultiReader current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::TreeReader::MultiReader

namespace ROOT {
   static TClass *vectorlETreeReadercLcLFamilyMembermUgR_Dictionary();
   static void vectorlETreeReadercLcLFamilyMembermUgR_TClassManip(TClass*);
   static void *new_vectorlETreeReadercLcLFamilyMembermUgR(void *p = 0);
   static void *newArray_vectorlETreeReadercLcLFamilyMembermUgR(Long_t size, void *p);
   static void delete_vectorlETreeReadercLcLFamilyMembermUgR(void *p);
   static void deleteArray_vectorlETreeReadercLcLFamilyMembermUgR(void *p);
   static void destruct_vectorlETreeReadercLcLFamilyMembermUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TreeReader::FamilyMember*>*)
   {
      vector<TreeReader::FamilyMember*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TreeReader::FamilyMember*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TreeReader::FamilyMember*>", -2, "vector", 214,
                  typeid(vector<TreeReader::FamilyMember*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETreeReadercLcLFamilyMembermUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TreeReader::FamilyMember*>) );
      instance.SetNew(&new_vectorlETreeReadercLcLFamilyMembermUgR);
      instance.SetNewArray(&newArray_vectorlETreeReadercLcLFamilyMembermUgR);
      instance.SetDelete(&delete_vectorlETreeReadercLcLFamilyMembermUgR);
      instance.SetDeleteArray(&deleteArray_vectorlETreeReadercLcLFamilyMembermUgR);
      instance.SetDestructor(&destruct_vectorlETreeReadercLcLFamilyMembermUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TreeReader::FamilyMember*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TreeReader::FamilyMember*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETreeReadercLcLFamilyMembermUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TreeReader::FamilyMember*>*)0x0)->GetClass();
      vectorlETreeReadercLcLFamilyMembermUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETreeReadercLcLFamilyMembermUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETreeReadercLcLFamilyMembermUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TreeReader::FamilyMember*> : new vector<TreeReader::FamilyMember*>;
   }
   static void *newArray_vectorlETreeReadercLcLFamilyMembermUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TreeReader::FamilyMember*>[nElements] : new vector<TreeReader::FamilyMember*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETreeReadercLcLFamilyMembermUgR(void *p) {
      delete ((vector<TreeReader::FamilyMember*>*)p);
   }
   static void deleteArray_vectorlETreeReadercLcLFamilyMembermUgR(void *p) {
      delete [] ((vector<TreeReader::FamilyMember*>*)p);
   }
   static void destruct_vectorlETreeReadercLcLFamilyMembermUgR(void *p) {
      typedef vector<TreeReader::FamilyMember*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TreeReader::FamilyMember*>

namespace ROOT {
   static TClass *maplETStringcOTreeReadercLcLFamilyMembermUgR_Dictionary();
   static void maplETStringcOTreeReadercLcLFamilyMembermUgR_TClassManip(TClass*);
   static void *new_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p = 0);
   static void *newArray_maplETStringcOTreeReadercLcLFamilyMembermUgR(Long_t size, void *p);
   static void delete_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p);
   static void deleteArray_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p);
   static void destruct_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,TreeReader::FamilyMember*>*)
   {
      map<TString,TreeReader::FamilyMember*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,TreeReader::FamilyMember*>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,TreeReader::FamilyMember*>", -2, "map", 96,
                  typeid(map<TString,TreeReader::FamilyMember*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOTreeReadercLcLFamilyMembermUgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,TreeReader::FamilyMember*>) );
      instance.SetNew(&new_maplETStringcOTreeReadercLcLFamilyMembermUgR);
      instance.SetNewArray(&newArray_maplETStringcOTreeReadercLcLFamilyMembermUgR);
      instance.SetDelete(&delete_maplETStringcOTreeReadercLcLFamilyMembermUgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOTreeReadercLcLFamilyMembermUgR);
      instance.SetDestructor(&destruct_maplETStringcOTreeReadercLcLFamilyMembermUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,TreeReader::FamilyMember*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,TreeReader::FamilyMember*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOTreeReadercLcLFamilyMembermUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,TreeReader::FamilyMember*>*)0x0)->GetClass();
      maplETStringcOTreeReadercLcLFamilyMembermUgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOTreeReadercLcLFamilyMembermUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,TreeReader::FamilyMember*> : new map<TString,TreeReader::FamilyMember*>;
   }
   static void *newArray_maplETStringcOTreeReadercLcLFamilyMembermUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,TreeReader::FamilyMember*>[nElements] : new map<TString,TreeReader::FamilyMember*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p) {
      delete ((map<TString,TreeReader::FamilyMember*>*)p);
   }
   static void deleteArray_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p) {
      delete [] ((map<TString,TreeReader::FamilyMember*>*)p);
   }
   static void destruct_maplETStringcOTreeReadercLcLFamilyMembermUgR(void *p) {
      typedef map<TString,TreeReader::FamilyMember*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,TreeReader::FamilyMember*>

namespace {
  void TriggerDictionaryInitialization_TreeReaderCINT_Impl() {
    static const char* headers[] = {
"TreeReader/TreeAbsReader.h",
"TreeReader/MultiReader.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/Root",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.14.04-0d8dc/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/TreeReader/cmt/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "TreeReaderCINT dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace TreeReader{class __attribute__((annotate("$clingAutoload$TreeReader/FamilyMember.h")))  __attribute__((annotate("$clingAutoload$TreeReader/TreeAbsReader.h")))  FamilyMember;}
namespace TreeReader{class __attribute__((annotate("$clingAutoload$TreeReader/TreeAbsReader.h")))  TreeAbsReader;}
namespace TreeReader{class __attribute__((annotate("$clingAutoload$TreeReader/DataType.h")))  __attribute__((annotate("$clingAutoload$TreeReader/MultiReader.h")))  BranchSpec;}
namespace TreeReader{class __attribute__((annotate("$clingAutoload$TreeReader/MultiReader.h")))  MultiReader;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "TreeReaderCINT dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "TreeReader"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "TreeReader/TreeAbsReader.h"
#include "TreeReader/MultiReader.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"TreeReader::BranchSpec", payloadCode, "@",
"TreeReader::BranchType", payloadCode, "@",
"TreeReader::FamilyMember", payloadCode, "@",
"TreeReader::MultiReader", payloadCode, "@",
"TreeReader::TreeAbsReader", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("TreeReaderCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_TreeReaderCINT_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_TreeReaderCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_TreeReaderCINT() {
  TriggerDictionaryInitialization_TreeReaderCINT_Impl();
}
