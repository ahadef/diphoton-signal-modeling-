#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIadIahadefdIpublicdIHGGFitterdIFittersdIRootCoredIobjdIx86_64mIslc6mIgcc62mIoptdIHfitterStatsdIobjdIHfitterStatsCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "HfitterStats/HftInterval.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "HfitterStats/HftScanPoint.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftMultiCalculator.h"
#include "HfitterStats/HftMaxFindingCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "HfitterStats/HftScanPointCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftAbsStateCalculator.h"
#include "HfitterStats/HftToysCalculator.h"
#include "HfitterStats/HftAbsReprocStudy.h"
#include "HfitterStats/HftValueCalculator.h"
#include "HfitterStats/HftFitValueCalculator.h"
#include "HfitterStats/HftLRCalculator.h"
#include "HfitterStats/HftDataCountingCalculator.h"
#include "HfitterStats/HftAbsStatCalculator.h"
#include "HfitterStats/HftAbsHypoTestCalculator.h"
#include "HfitterStats/HftExpHypoTestCalc.h"
#include "HfitterStats/HftScanIntervalCalculator.h"
#include "HfitterStats/HftMultiHypoTestCalculator.h"
#include "HfitterStats/HftIterIntervalCalculator.h"
#include "HfitterStats/HftMLCalculator.h"
#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftScanLimitCalculator.h"
#include "HfitterStats/HftIterLimitCalculator.h"
#include "HfitterStats/HftOneSidedPValueCalculator.h"
#include "HfitterStats/HftPLRPValueCalculator.h"
#include "HfitterStats/HftUncappedPValueCalculator.h"
#include "HfitterStats/HftDiscreteTestCalculator.h"
#include "HfitterStats/HftPLRLimitCalculator.h"
#include "HfitterStats/HftQLimitCalculator.h"
#include "HfitterStats/HftQTildaLimitCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"
#include "HfitterStats/HftScanStudy.h"
#include "HfitterStats/HftStatTools.h"
#include "HfitterStats/HftOneSidedP0Calculator.h"
#include "HfitterStats/HftUncappedP0Calculator.h"
#include "HfitterStats/HftCLsbQCalculator.h"
#include "HfitterStats/HftCLsbQTildaCalculator.h"
#include "HfitterStats/HftCLSolvingCalculator.h"

// Header files passed via #pragma extra_include

namespace Hfitter {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *Hfitter_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("Hfitter", 0 /*version*/, "HfitterStats/HftInterval.h", 10,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &Hfitter_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *Hfitter_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *HfittercLcLHftInterval_Dictionary();
   static void HfittercLcLHftInterval_TClassManip(TClass*);
   static void *new_HfittercLcLHftInterval(void *p = 0);
   static void *newArray_HfittercLcLHftInterval(Long_t size, void *p);
   static void delete_HfittercLcLHftInterval(void *p);
   static void deleteArray_HfittercLcLHftInterval(void *p);
   static void destruct_HfittercLcLHftInterval(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftInterval*)
   {
      ::Hfitter::HftInterval *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftInterval));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftInterval", "HfitterStats/HftInterval.h", 18,
                  typeid(::Hfitter::HftInterval), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftInterval_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftInterval) );
      instance.SetNew(&new_HfittercLcLHftInterval);
      instance.SetNewArray(&newArray_HfittercLcLHftInterval);
      instance.SetDelete(&delete_HfittercLcLHftInterval);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftInterval);
      instance.SetDestructor(&destruct_HfittercLcLHftInterval);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftInterval*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftInterval*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftInterval*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftInterval_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftInterval*)0x0)->GetClass();
      HfittercLcLHftInterval_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftInterval_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftBand_Dictionary();
   static void HfittercLcLHftBand_TClassManip(TClass*);
   static void *new_HfittercLcLHftBand(void *p = 0);
   static void *newArray_HfittercLcLHftBand(Long_t size, void *p);
   static void delete_HfittercLcLHftBand(void *p);
   static void deleteArray_HfittercLcLHftBand(void *p);
   static void destruct_HfittercLcLHftBand(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftBand*)
   {
      ::Hfitter::HftBand *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftBand));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftBand", "HfitterStats/HftBand.h", 19,
                  typeid(::Hfitter::HftBand), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftBand_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftBand) );
      instance.SetNew(&new_HfittercLcLHftBand);
      instance.SetNewArray(&newArray_HfittercLcLHftBand);
      instance.SetDelete(&delete_HfittercLcLHftBand);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftBand);
      instance.SetDestructor(&destruct_HfittercLcLHftBand);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftBand*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftBand*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftBand*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftBand_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftBand*)0x0)->GetClass();
      HfittercLcLHftBand_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftBand_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTree_Dictionary();
   static void HfittercLcLHftTree_TClassManip(TClass*);
   static void delete_HfittercLcLHftTree(void *p);
   static void deleteArray_HfittercLcLHftTree(void *p);
   static void destruct_HfittercLcLHftTree(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTree*)
   {
      ::Hfitter::HftTree *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftTree));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTree", "HfitterStats/HftTree.h", 32,
                  typeid(::Hfitter::HftTree), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTree_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTree) );
      instance.SetDelete(&delete_HfittercLcLHftTree);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTree);
      instance.SetDestructor(&destruct_HfittercLcLHftTree);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTree*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTree*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTree*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTree_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTree*)0x0)->GetClass();
      HfittercLcLHftTree_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTree_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsCalculator_Dictionary();
   static void HfittercLcLHftAbsCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsCalculator(void *p);
   static void deleteArray_HfittercLcLHftAbsCalculator(void *p);
   static void destruct_HfittercLcLHftAbsCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsCalculator*)
   {
      ::Hfitter::HftAbsCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsCalculator", "HfitterStats/HftAbsCalculator.h", 20,
                  typeid(::Hfitter::HftAbsCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftAbsCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsCalculator*)0x0)->GetClass();
      HfittercLcLHftAbsCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsMultiResultCalculator_Dictionary();
   static void HfittercLcLHftAbsMultiResultCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsMultiResultCalculator(void *p);
   static void deleteArray_HfittercLcLHftAbsMultiResultCalculator(void *p);
   static void destruct_HfittercLcLHftAbsMultiResultCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsMultiResultCalculator*)
   {
      ::Hfitter::HftAbsMultiResultCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsMultiResultCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsMultiResultCalculator", "HfitterStats/HftAbsMultiResultCalculator.h", 14,
                  typeid(::Hfitter::HftAbsMultiResultCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsMultiResultCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsMultiResultCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftAbsMultiResultCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsMultiResultCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsMultiResultCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsMultiResultCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsMultiResultCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsMultiResultCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsMultiResultCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsMultiResultCalculator*)0x0)->GetClass();
      HfittercLcLHftAbsMultiResultCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsMultiResultCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanPoint_Dictionary();
   static void HfittercLcLHftScanPoint_TClassManip(TClass*);
   static void *new_HfittercLcLHftScanPoint(void *p = 0);
   static void *newArray_HfittercLcLHftScanPoint(Long_t size, void *p);
   static void delete_HfittercLcLHftScanPoint(void *p);
   static void deleteArray_HfittercLcLHftScanPoint(void *p);
   static void destruct_HfittercLcLHftScanPoint(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanPoint*)
   {
      ::Hfitter::HftScanPoint *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanPoint));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanPoint", "HfitterStats/HftScanPoint.h", 14,
                  typeid(::Hfitter::HftScanPoint), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanPoint_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanPoint) );
      instance.SetNew(&new_HfittercLcLHftScanPoint);
      instance.SetNewArray(&newArray_HfittercLcLHftScanPoint);
      instance.SetDelete(&delete_HfittercLcLHftScanPoint);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanPoint);
      instance.SetDestructor(&destruct_HfittercLcLHftScanPoint);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanPoint*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanPoint*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanPoint*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanPoint_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanPoint*)0x0)->GetClass();
      HfittercLcLHftScanPoint_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanPoint_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanGrid_Dictionary();
   static void HfittercLcLHftScanGrid_TClassManip(TClass*);
   static void *new_HfittercLcLHftScanGrid(void *p = 0);
   static void *newArray_HfittercLcLHftScanGrid(Long_t size, void *p);
   static void delete_HfittercLcLHftScanGrid(void *p);
   static void deleteArray_HfittercLcLHftScanGrid(void *p);
   static void destruct_HfittercLcLHftScanGrid(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanGrid*)
   {
      ::Hfitter::HftScanGrid *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanGrid));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanGrid", "HfitterStats/HftScanGrid.h", 21,
                  typeid(::Hfitter::HftScanGrid), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanGrid_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanGrid) );
      instance.SetNew(&new_HfittercLcLHftScanGrid);
      instance.SetNewArray(&newArray_HfittercLcLHftScanGrid);
      instance.SetDelete(&delete_HfittercLcLHftScanGrid);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanGrid);
      instance.SetDestructor(&destruct_HfittercLcLHftScanGrid);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanGrid*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanGrid*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanGrid*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanGrid_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanGrid*)0x0)->GetClass();
      HfittercLcLHftScanGrid_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanGrid_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftMultiCalculator_Dictionary();
   static void HfittercLcLHftMultiCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftMultiCalculator(void *p);
   static void deleteArray_HfittercLcLHftMultiCalculator(void *p);
   static void destruct_HfittercLcLHftMultiCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftMultiCalculator*)
   {
      ::Hfitter::HftMultiCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftMultiCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftMultiCalculator", "HfitterStats/HftMultiCalculator.h", 14,
                  typeid(::Hfitter::HftMultiCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftMultiCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftMultiCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftMultiCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftMultiCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftMultiCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftMultiCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftMultiCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftMultiCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftMultiCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMultiCalculator*)0x0)->GetClass();
      HfittercLcLHftMultiCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftMultiCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanPointCalculator_Dictionary();
   static void HfittercLcLHftScanPointCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftScanPointCalculator(void *p);
   static void deleteArray_HfittercLcLHftScanPointCalculator(void *p);
   static void destruct_HfittercLcLHftScanPointCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanPointCalculator*)
   {
      ::Hfitter::HftScanPointCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanPointCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanPointCalculator", "HfitterStats/HftScanPointCalculator.h", 10,
                  typeid(::Hfitter::HftScanPointCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanPointCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanPointCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftScanPointCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanPointCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftScanPointCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanPointCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanPointCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanPointCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanPointCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanPointCalculator*)0x0)->GetClass();
      HfittercLcLHftScanPointCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanPointCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsStateCalculator_Dictionary();
   static void HfittercLcLHftAbsStateCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsStateCalculator(void *p);
   static void deleteArray_HfittercLcLHftAbsStateCalculator(void *p);
   static void destruct_HfittercLcLHftAbsStateCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsStateCalculator*)
   {
      ::Hfitter::HftAbsStateCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsStateCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsStateCalculator", "HfitterStats/HftAbsStateCalculator.h", 17,
                  typeid(::Hfitter::HftAbsStateCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsStateCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsStateCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftAbsStateCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsStateCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsStateCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsStateCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsStateCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsStateCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsStateCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsStateCalculator*)0x0)->GetClass();
      HfittercLcLHftAbsStateCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsStateCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAsimovCalculator_Dictionary();
   static void HfittercLcLHftAsimovCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftAsimovCalculator(void *p);
   static void deleteArray_HfittercLcLHftAsimovCalculator(void *p);
   static void destruct_HfittercLcLHftAsimovCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAsimovCalculator*)
   {
      ::Hfitter::HftAsimovCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAsimovCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAsimovCalculator", "HfitterStats/HftAsimovCalculator.h", 19,
                  typeid(::Hfitter::HftAsimovCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAsimovCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAsimovCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftAsimovCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAsimovCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftAsimovCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAsimovCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAsimovCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAsimovCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAsimovCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAsimovCalculator*)0x0)->GetClass();
      HfittercLcLHftAsimovCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAsimovCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftToysCalculator_Dictionary();
   static void HfittercLcLHftToysCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftToysCalculator(void *p);
   static void deleteArray_HfittercLcLHftToysCalculator(void *p);
   static void destruct_HfittercLcLHftToysCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftToysCalculator*)
   {
      ::Hfitter::HftToysCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftToysCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftToysCalculator", "HfitterStats/HftToysCalculator.h", 19,
                  typeid(::Hfitter::HftToysCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftToysCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftToysCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftToysCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftToysCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftToysCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftToysCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftToysCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftToysCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftToysCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftToysCalculator*)0x0)->GetClass();
      HfittercLcLHftToysCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftToysCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanningCalculator_Dictionary();
   static void HfittercLcLHftScanningCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftScanningCalculator(void *p);
   static void deleteArray_HfittercLcLHftScanningCalculator(void *p);
   static void destruct_HfittercLcLHftScanningCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanningCalculator*)
   {
      ::Hfitter::HftScanningCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanningCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanningCalculator", "HfitterStats/HftScanningCalculator.h", 27,
                  typeid(::Hfitter::HftScanningCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanningCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanningCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftScanningCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanningCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftScanningCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanningCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanningCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanningCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanningCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanningCalculator*)0x0)->GetClass();
      HfittercLcLHftScanningCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanningCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftMaxFindingCalculator_Dictionary();
   static void HfittercLcLHftMaxFindingCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftMaxFindingCalculator(void *p);
   static void deleteArray_HfittercLcLHftMaxFindingCalculator(void *p);
   static void destruct_HfittercLcLHftMaxFindingCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftMaxFindingCalculator*)
   {
      ::Hfitter::HftMaxFindingCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftMaxFindingCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftMaxFindingCalculator", "HfitterStats/HftMaxFindingCalculator.h", 20,
                  typeid(::Hfitter::HftMaxFindingCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftMaxFindingCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftMaxFindingCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftMaxFindingCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftMaxFindingCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftMaxFindingCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftMaxFindingCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftMaxFindingCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftMaxFindingCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftMaxFindingCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMaxFindingCalculator*)0x0)->GetClass();
      HfittercLcLHftMaxFindingCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftMaxFindingCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsReprocStudy_Dictionary();
   static void HfittercLcLHftAbsReprocStudy_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsReprocStudy(void *p);
   static void deleteArray_HfittercLcLHftAbsReprocStudy(void *p);
   static void destruct_HfittercLcLHftAbsReprocStudy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsReprocStudy*)
   {
      ::Hfitter::HftAbsReprocStudy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsReprocStudy));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsReprocStudy", "HfitterStats/HftAbsReprocStudy.h", 17,
                  typeid(::Hfitter::HftAbsReprocStudy), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsReprocStudy_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsReprocStudy) );
      instance.SetDelete(&delete_HfittercLcLHftAbsReprocStudy);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsReprocStudy);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsReprocStudy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsReprocStudy*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsReprocStudy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsReprocStudy*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsReprocStudy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsReprocStudy*)0x0)->GetClass();
      HfittercLcLHftAbsReprocStudy_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsReprocStudy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftValueCalculator_Dictionary();
   static void HfittercLcLHftValueCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftValueCalculator(void *p);
   static void deleteArray_HfittercLcLHftValueCalculator(void *p);
   static void destruct_HfittercLcLHftValueCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftValueCalculator*)
   {
      ::Hfitter::HftValueCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftValueCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftValueCalculator", "HfitterStats/HftValueCalculator.h", 21,
                  typeid(::Hfitter::HftValueCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftValueCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftValueCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftValueCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftValueCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftValueCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftValueCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftValueCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftValueCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftValueCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftValueCalculator*)0x0)->GetClass();
      HfittercLcLHftValueCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftValueCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftFitValueCalculator_Dictionary();
   static void HfittercLcLHftFitValueCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftFitValueCalculator(void *p);
   static void deleteArray_HfittercLcLHftFitValueCalculator(void *p);
   static void destruct_HfittercLcLHftFitValueCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftFitValueCalculator*)
   {
      ::Hfitter::HftFitValueCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftFitValueCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftFitValueCalculator", "HfitterStats/HftFitValueCalculator.h", 22,
                  typeid(::Hfitter::HftFitValueCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftFitValueCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftFitValueCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftFitValueCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftFitValueCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftFitValueCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftFitValueCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftFitValueCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftFitValueCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftFitValueCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftFitValueCalculator*)0x0)->GetClass();
      HfittercLcLHftFitValueCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftFitValueCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftLRCalculator_Dictionary();
   static void HfittercLcLHftLRCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftLRCalculator(void *p);
   static void deleteArray_HfittercLcLHftLRCalculator(void *p);
   static void destruct_HfittercLcLHftLRCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftLRCalculator*)
   {
      ::Hfitter::HftLRCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftLRCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftLRCalculator", "HfitterStats/HftLRCalculator.h", 19,
                  typeid(::Hfitter::HftLRCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftLRCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftLRCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftLRCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftLRCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftLRCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftLRCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftLRCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftLRCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftLRCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftLRCalculator*)0x0)->GetClass();
      HfittercLcLHftLRCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftLRCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftDataCountingCalculator_Dictionary();
   static void HfittercLcLHftDataCountingCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftDataCountingCalculator(void *p);
   static void deleteArray_HfittercLcLHftDataCountingCalculator(void *p);
   static void destruct_HfittercLcLHftDataCountingCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDataCountingCalculator*)
   {
      ::Hfitter::HftDataCountingCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftDataCountingCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDataCountingCalculator", "HfitterStats/HftDataCountingCalculator.h", 17,
                  typeid(::Hfitter::HftDataCountingCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftDataCountingCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDataCountingCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftDataCountingCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDataCountingCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftDataCountingCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDataCountingCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDataCountingCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDataCountingCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftDataCountingCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDataCountingCalculator*)0x0)->GetClass();
      HfittercLcLHftDataCountingCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftDataCountingCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsStatCalculator_Dictionary();
   static void HfittercLcLHftAbsStatCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsStatCalculator(void *p);
   static void deleteArray_HfittercLcLHftAbsStatCalculator(void *p);
   static void destruct_HfittercLcLHftAbsStatCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsStatCalculator*)
   {
      ::Hfitter::HftAbsStatCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsStatCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsStatCalculator", "HfitterStats/HftAbsStatCalculator.h", 12,
                  typeid(::Hfitter::HftAbsStatCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsStatCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsStatCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftAbsStatCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsStatCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsStatCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsStatCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsStatCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsStatCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsStatCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsStatCalculator*)0x0)->GetClass();
      HfittercLcLHftAbsStatCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsStatCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftAbsHypoTestCalculator_Dictionary();
   static void HfittercLcLHftAbsHypoTestCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftAbsHypoTestCalculator(void *p);
   static void deleteArray_HfittercLcLHftAbsHypoTestCalculator(void *p);
   static void destruct_HfittercLcLHftAbsHypoTestCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftAbsHypoTestCalculator*)
   {
      ::Hfitter::HftAbsHypoTestCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftAbsHypoTestCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftAbsHypoTestCalculator", "HfitterStats/HftAbsHypoTestCalculator.h", 23,
                  typeid(::Hfitter::HftAbsHypoTestCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftAbsHypoTestCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftAbsHypoTestCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftAbsHypoTestCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftAbsHypoTestCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftAbsHypoTestCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftAbsHypoTestCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftAbsHypoTestCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftAbsHypoTestCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftAbsHypoTestCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftAbsHypoTestCalculator*)0x0)->GetClass();
      HfittercLcLHftAbsHypoTestCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftAbsHypoTestCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftExpHypoTestCalc_Dictionary();
   static void HfittercLcLHftExpHypoTestCalc_TClassManip(TClass*);
   static void delete_HfittercLcLHftExpHypoTestCalc(void *p);
   static void deleteArray_HfittercLcLHftExpHypoTestCalc(void *p);
   static void destruct_HfittercLcLHftExpHypoTestCalc(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftExpHypoTestCalc*)
   {
      ::Hfitter::HftExpHypoTestCalc *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftExpHypoTestCalc));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftExpHypoTestCalc", "HfitterStats/HftExpHypoTestCalc.h", 14,
                  typeid(::Hfitter::HftExpHypoTestCalc), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftExpHypoTestCalc_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftExpHypoTestCalc) );
      instance.SetDelete(&delete_HfittercLcLHftExpHypoTestCalc);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftExpHypoTestCalc);
      instance.SetDestructor(&destruct_HfittercLcLHftExpHypoTestCalc);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftExpHypoTestCalc*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftExpHypoTestCalc*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftExpHypoTestCalc*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftExpHypoTestCalc_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftExpHypoTestCalc*)0x0)->GetClass();
      HfittercLcLHftExpHypoTestCalc_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftExpHypoTestCalc_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftMultiHypoTestCalculator_Dictionary();
   static void HfittercLcLHftMultiHypoTestCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftMultiHypoTestCalculator(void *p);
   static void deleteArray_HfittercLcLHftMultiHypoTestCalculator(void *p);
   static void destruct_HfittercLcLHftMultiHypoTestCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftMultiHypoTestCalculator*)
   {
      ::Hfitter::HftMultiHypoTestCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftMultiHypoTestCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftMultiHypoTestCalculator", "HfitterStats/HftMultiHypoTestCalculator.h", 21,
                  typeid(::Hfitter::HftMultiHypoTestCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftMultiHypoTestCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftMultiHypoTestCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftMultiHypoTestCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftMultiHypoTestCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftMultiHypoTestCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftMultiHypoTestCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftMultiHypoTestCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftMultiHypoTestCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftMultiHypoTestCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMultiHypoTestCalculator*)0x0)->GetClass();
      HfittercLcLHftMultiHypoTestCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftMultiHypoTestCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanIntervalCalculator_Dictionary();
   static void HfittercLcLHftScanIntervalCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftScanIntervalCalculator(void *p);
   static void deleteArray_HfittercLcLHftScanIntervalCalculator(void *p);
   static void destruct_HfittercLcLHftScanIntervalCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanIntervalCalculator*)
   {
      ::Hfitter::HftScanIntervalCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanIntervalCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanIntervalCalculator", "HfitterStats/HftScanIntervalCalculator.h", 20,
                  typeid(::Hfitter::HftScanIntervalCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanIntervalCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanIntervalCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftScanIntervalCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanIntervalCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftScanIntervalCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanIntervalCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanIntervalCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanIntervalCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanIntervalCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanIntervalCalculator*)0x0)->GetClass();
      HfittercLcLHftScanIntervalCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanIntervalCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftIterIntervalCalculator_Dictionary();
   static void HfittercLcLHftIterIntervalCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftIterIntervalCalculator(void *p);
   static void deleteArray_HfittercLcLHftIterIntervalCalculator(void *p);
   static void destruct_HfittercLcLHftIterIntervalCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftIterIntervalCalculator*)
   {
      ::Hfitter::HftIterIntervalCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftIterIntervalCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftIterIntervalCalculator", "HfitterStats/HftIterIntervalCalculator.h", 23,
                  typeid(::Hfitter::HftIterIntervalCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftIterIntervalCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftIterIntervalCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftIterIntervalCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftIterIntervalCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftIterIntervalCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftIterIntervalCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftIterIntervalCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftIterIntervalCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftIterIntervalCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftIterIntervalCalculator*)0x0)->GetClass();
      HfittercLcLHftIterIntervalCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftIterIntervalCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftMLCalculator_Dictionary();
   static void HfittercLcLHftMLCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftMLCalculator(void *p);
   static void deleteArray_HfittercLcLHftMLCalculator(void *p);
   static void destruct_HfittercLcLHftMLCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftMLCalculator*)
   {
      ::Hfitter::HftMLCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftMLCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftMLCalculator", "HfitterStats/HftMLCalculator.h", 23,
                  typeid(::Hfitter::HftMLCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftMLCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftMLCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftMLCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftMLCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftMLCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftMLCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftMLCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftMLCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftMLCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftMLCalculator*)0x0)->GetClass();
      HfittercLcLHftMLCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftMLCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftPLRCalculator_Dictionary();
   static void HfittercLcLHftPLRCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftPLRCalculator(void *p);
   static void deleteArray_HfittercLcLHftPLRCalculator(void *p);
   static void destruct_HfittercLcLHftPLRCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPLRCalculator*)
   {
      ::Hfitter::HftPLRCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftPLRCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPLRCalculator", "HfitterStats/HftPLRCalculator.h", 28,
                  typeid(::Hfitter::HftPLRCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftPLRCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPLRCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftPLRCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPLRCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftPLRCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPLRCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPLRCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPLRCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftPLRCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPLRCalculator*)0x0)->GetClass();
      HfittercLcLHftPLRCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftPLRCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanLimitCalculator_Dictionary();
   static void HfittercLcLHftScanLimitCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftScanLimitCalculator(void *p);
   static void deleteArray_HfittercLcLHftScanLimitCalculator(void *p);
   static void destruct_HfittercLcLHftScanLimitCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanLimitCalculator*)
   {
      ::Hfitter::HftScanLimitCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanLimitCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanLimitCalculator", "HfitterStats/HftScanLimitCalculator.h", 23,
                  typeid(::Hfitter::HftScanLimitCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanLimitCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanLimitCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftScanLimitCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanLimitCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftScanLimitCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanLimitCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanLimitCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanLimitCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanLimitCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanLimitCalculator*)0x0)->GetClass();
      HfittercLcLHftScanLimitCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanLimitCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftIterLimitCalculator_Dictionary();
   static void HfittercLcLHftIterLimitCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftIterLimitCalculator(void *p);
   static void deleteArray_HfittercLcLHftIterLimitCalculator(void *p);
   static void destruct_HfittercLcLHftIterLimitCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftIterLimitCalculator*)
   {
      ::Hfitter::HftIterLimitCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftIterLimitCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftIterLimitCalculator", "HfitterStats/HftIterLimitCalculator.h", 23,
                  typeid(::Hfitter::HftIterLimitCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftIterLimitCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftIterLimitCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftIterLimitCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftIterLimitCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftIterLimitCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftIterLimitCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftIterLimitCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftIterLimitCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftIterLimitCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftIterLimitCalculator*)0x0)->GetClass();
      HfittercLcLHftIterLimitCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftIterLimitCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftPLRPValueCalculator_Dictionary();
   static void HfittercLcLHftPLRPValueCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftPLRPValueCalculator(void *p);
   static void deleteArray_HfittercLcLHftPLRPValueCalculator(void *p);
   static void destruct_HfittercLcLHftPLRPValueCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPLRPValueCalculator*)
   {
      ::Hfitter::HftPLRPValueCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftPLRPValueCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPLRPValueCalculator", "HfitterStats/HftPLRPValueCalculator.h", 17,
                  typeid(::Hfitter::HftPLRPValueCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftPLRPValueCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPLRPValueCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftPLRPValueCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPLRPValueCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftPLRPValueCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPLRPValueCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPLRPValueCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPLRPValueCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftPLRPValueCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPLRPValueCalculator*)0x0)->GetClass();
      HfittercLcLHftPLRPValueCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftPLRPValueCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftOneSidedPValueCalculator_Dictionary();
   static void HfittercLcLHftOneSidedPValueCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftOneSidedPValueCalculator(void *p);
   static void deleteArray_HfittercLcLHftOneSidedPValueCalculator(void *p);
   static void destruct_HfittercLcLHftOneSidedPValueCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftOneSidedPValueCalculator*)
   {
      ::Hfitter::HftOneSidedPValueCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftOneSidedPValueCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftOneSidedPValueCalculator", "HfitterStats/HftOneSidedPValueCalculator.h", 17,
                  typeid(::Hfitter::HftOneSidedPValueCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftOneSidedPValueCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftOneSidedPValueCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftOneSidedPValueCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftOneSidedPValueCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftOneSidedPValueCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftOneSidedPValueCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftOneSidedPValueCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftOneSidedPValueCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftOneSidedPValueCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftOneSidedPValueCalculator*)0x0)->GetClass();
      HfittercLcLHftOneSidedPValueCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftOneSidedPValueCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftUncappedPValueCalculator_Dictionary();
   static void HfittercLcLHftUncappedPValueCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftUncappedPValueCalculator(void *p);
   static void deleteArray_HfittercLcLHftUncappedPValueCalculator(void *p);
   static void destruct_HfittercLcLHftUncappedPValueCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftUncappedPValueCalculator*)
   {
      ::Hfitter::HftUncappedPValueCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftUncappedPValueCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftUncappedPValueCalculator", "HfitterStats/HftUncappedPValueCalculator.h", 17,
                  typeid(::Hfitter::HftUncappedPValueCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftUncappedPValueCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftUncappedPValueCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftUncappedPValueCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftUncappedPValueCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftUncappedPValueCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftUncappedPValueCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftUncappedPValueCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftUncappedPValueCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftUncappedPValueCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftUncappedPValueCalculator*)0x0)->GetClass();
      HfittercLcLHftUncappedPValueCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftUncappedPValueCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftDiscreteTestCalculator_Dictionary();
   static void HfittercLcLHftDiscreteTestCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftDiscreteTestCalculator(void *p);
   static void deleteArray_HfittercLcLHftDiscreteTestCalculator(void *p);
   static void destruct_HfittercLcLHftDiscreteTestCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftDiscreteTestCalculator*)
   {
      ::Hfitter::HftDiscreteTestCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftDiscreteTestCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftDiscreteTestCalculator", "HfitterStats/HftDiscreteTestCalculator.h", 21,
                  typeid(::Hfitter::HftDiscreteTestCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftDiscreteTestCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftDiscreteTestCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftDiscreteTestCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftDiscreteTestCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftDiscreteTestCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftDiscreteTestCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftDiscreteTestCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftDiscreteTestCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftDiscreteTestCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftDiscreteTestCalculator*)0x0)->GetClass();
      HfittercLcLHftDiscreteTestCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftDiscreteTestCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftPLRLimitCalculator_Dictionary();
   static void HfittercLcLHftPLRLimitCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftPLRLimitCalculator(void *p);
   static void deleteArray_HfittercLcLHftPLRLimitCalculator(void *p);
   static void destruct_HfittercLcLHftPLRLimitCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftPLRLimitCalculator*)
   {
      ::Hfitter::HftPLRLimitCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftPLRLimitCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftPLRLimitCalculator", "HfitterStats/HftPLRLimitCalculator.h", 17,
                  typeid(::Hfitter::HftPLRLimitCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftPLRLimitCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftPLRLimitCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftPLRLimitCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftPLRLimitCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftPLRLimitCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftPLRLimitCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftPLRLimitCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftPLRLimitCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftPLRLimitCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftPLRLimitCalculator*)0x0)->GetClass();
      HfittercLcLHftPLRLimitCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftPLRLimitCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftQLimitCalculator_Dictionary();
   static void HfittercLcLHftQLimitCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftQLimitCalculator(void *p);
   static void deleteArray_HfittercLcLHftQLimitCalculator(void *p);
   static void destruct_HfittercLcLHftQLimitCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftQLimitCalculator*)
   {
      ::Hfitter::HftQLimitCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftQLimitCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftQLimitCalculator", "HfitterStats/HftQLimitCalculator.h", 15,
                  typeid(::Hfitter::HftQLimitCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftQLimitCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftQLimitCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftQLimitCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftQLimitCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftQLimitCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftQLimitCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftQLimitCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftQLimitCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftQLimitCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftQLimitCalculator*)0x0)->GetClass();
      HfittercLcLHftQLimitCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftQLimitCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftQTildaLimitCalculator_Dictionary();
   static void HfittercLcLHftQTildaLimitCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftQTildaLimitCalculator(void *p);
   static void deleteArray_HfittercLcLHftQTildaLimitCalculator(void *p);
   static void destruct_HfittercLcLHftQTildaLimitCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftQTildaLimitCalculator*)
   {
      ::Hfitter::HftQTildaLimitCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftQTildaLimitCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftQTildaLimitCalculator", "HfitterStats/HftQTildaLimitCalculator.h", 16,
                  typeid(::Hfitter::HftQTildaLimitCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftQTildaLimitCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftQTildaLimitCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftQTildaLimitCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftQTildaLimitCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftQTildaLimitCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftQTildaLimitCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftQTildaLimitCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftQTildaLimitCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftQTildaLimitCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftQTildaLimitCalculator*)0x0)->GetClass();
      HfittercLcLHftQTildaLimitCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftQTildaLimitCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftCLsCalculator_Dictionary();
   static void HfittercLcLHftCLsCalculator_TClassManip(TClass*);
   static void delete_HfittercLcLHftCLsCalculator(void *p);
   static void deleteArray_HfittercLcLHftCLsCalculator(void *p);
   static void destruct_HfittercLcLHftCLsCalculator(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftCLsCalculator*)
   {
      ::Hfitter::HftCLsCalculator *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftCLsCalculator));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftCLsCalculator", "HfitterStats/HftCLsCalculator.h", 18,
                  typeid(::Hfitter::HftCLsCalculator), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftCLsCalculator_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftCLsCalculator) );
      instance.SetDelete(&delete_HfittercLcLHftCLsCalculator);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftCLsCalculator);
      instance.SetDestructor(&destruct_HfittercLcLHftCLsCalculator);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftCLsCalculator*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftCLsCalculator*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftCLsCalculator*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftCLsCalculator_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftCLsCalculator*)0x0)->GetClass();
      HfittercLcLHftCLsCalculator_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftCLsCalculator_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftScanStudy_Dictionary();
   static void HfittercLcLHftScanStudy_TClassManip(TClass*);
   static void delete_HfittercLcLHftScanStudy(void *p);
   static void deleteArray_HfittercLcLHftScanStudy(void *p);
   static void destruct_HfittercLcLHftScanStudy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftScanStudy*)
   {
      ::Hfitter::HftScanStudy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftScanStudy));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftScanStudy", "HfitterStats/HftScanStudy.h", 20,
                  typeid(::Hfitter::HftScanStudy), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftScanStudy_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftScanStudy) );
      instance.SetDelete(&delete_HfittercLcLHftScanStudy);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftScanStudy);
      instance.SetDestructor(&destruct_HfittercLcLHftScanStudy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftScanStudy*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftScanStudy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftScanStudy*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftScanStudy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftScanStudy*)0x0)->GetClass();
      HfittercLcLHftScanStudy_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftScanStudy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftStatTools_Dictionary();
   static void HfittercLcLHftStatTools_TClassManip(TClass*);
   static void *new_HfittercLcLHftStatTools(void *p = 0);
   static void *newArray_HfittercLcLHftStatTools(Long_t size, void *p);
   static void delete_HfittercLcLHftStatTools(void *p);
   static void deleteArray_HfittercLcLHftStatTools(void *p);
   static void destruct_HfittercLcLHftStatTools(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftStatTools*)
   {
      ::Hfitter::HftStatTools *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HftStatTools));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftStatTools", "HfitterStats/HftStatTools.h", 21,
                  typeid(::Hfitter::HftStatTools), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftStatTools_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftStatTools) );
      instance.SetNew(&new_HfittercLcLHftStatTools);
      instance.SetNewArray(&newArray_HfittercLcLHftStatTools);
      instance.SetDelete(&delete_HfittercLcLHftStatTools);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftStatTools);
      instance.SetDestructor(&destruct_HfittercLcLHftStatTools);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftStatTools*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftStatTools*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftStatTools*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftStatTools_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftStatTools*)0x0)->GetClass();
      HfittercLcLHftStatTools_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftStatTools_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftInterval(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftInterval : new ::Hfitter::HftInterval;
   }
   static void *newArray_HfittercLcLHftInterval(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftInterval[nElements] : new ::Hfitter::HftInterval[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftInterval(void *p) {
      delete ((::Hfitter::HftInterval*)p);
   }
   static void deleteArray_HfittercLcLHftInterval(void *p) {
      delete [] ((::Hfitter::HftInterval*)p);
   }
   static void destruct_HfittercLcLHftInterval(void *p) {
      typedef ::Hfitter::HftInterval current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftInterval

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftBand(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftBand : new ::Hfitter::HftBand;
   }
   static void *newArray_HfittercLcLHftBand(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftBand[nElements] : new ::Hfitter::HftBand[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftBand(void *p) {
      delete ((::Hfitter::HftBand*)p);
   }
   static void deleteArray_HfittercLcLHftBand(void *p) {
      delete [] ((::Hfitter::HftBand*)p);
   }
   static void destruct_HfittercLcLHftBand(void *p) {
      typedef ::Hfitter::HftBand current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftBand

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTree(void *p) {
      delete ((::Hfitter::HftTree*)p);
   }
   static void deleteArray_HfittercLcLHftTree(void *p) {
      delete [] ((::Hfitter::HftTree*)p);
   }
   static void destruct_HfittercLcLHftTree(void *p) {
      typedef ::Hfitter::HftTree current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTree

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsCalculator(void *p) {
      delete ((::Hfitter::HftAbsCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftAbsCalculator(void *p) {
      delete [] ((::Hfitter::HftAbsCalculator*)p);
   }
   static void destruct_HfittercLcLHftAbsCalculator(void *p) {
      typedef ::Hfitter::HftAbsCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsMultiResultCalculator(void *p) {
      delete ((::Hfitter::HftAbsMultiResultCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftAbsMultiResultCalculator(void *p) {
      delete [] ((::Hfitter::HftAbsMultiResultCalculator*)p);
   }
   static void destruct_HfittercLcLHftAbsMultiResultCalculator(void *p) {
      typedef ::Hfitter::HftAbsMultiResultCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsMultiResultCalculator

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftScanPoint(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftScanPoint : new ::Hfitter::HftScanPoint;
   }
   static void *newArray_HfittercLcLHftScanPoint(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftScanPoint[nElements] : new ::Hfitter::HftScanPoint[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanPoint(void *p) {
      delete ((::Hfitter::HftScanPoint*)p);
   }
   static void deleteArray_HfittercLcLHftScanPoint(void *p) {
      delete [] ((::Hfitter::HftScanPoint*)p);
   }
   static void destruct_HfittercLcLHftScanPoint(void *p) {
      typedef ::Hfitter::HftScanPoint current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanPoint

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftScanGrid(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftScanGrid : new ::Hfitter::HftScanGrid;
   }
   static void *newArray_HfittercLcLHftScanGrid(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftScanGrid[nElements] : new ::Hfitter::HftScanGrid[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanGrid(void *p) {
      delete ((::Hfitter::HftScanGrid*)p);
   }
   static void deleteArray_HfittercLcLHftScanGrid(void *p) {
      delete [] ((::Hfitter::HftScanGrid*)p);
   }
   static void destruct_HfittercLcLHftScanGrid(void *p) {
      typedef ::Hfitter::HftScanGrid current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanGrid

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftMultiCalculator(void *p) {
      delete ((::Hfitter::HftMultiCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftMultiCalculator(void *p) {
      delete [] ((::Hfitter::HftMultiCalculator*)p);
   }
   static void destruct_HfittercLcLHftMultiCalculator(void *p) {
      typedef ::Hfitter::HftMultiCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftMultiCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanPointCalculator(void *p) {
      delete ((::Hfitter::HftScanPointCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftScanPointCalculator(void *p) {
      delete [] ((::Hfitter::HftScanPointCalculator*)p);
   }
   static void destruct_HfittercLcLHftScanPointCalculator(void *p) {
      typedef ::Hfitter::HftScanPointCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanPointCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsStateCalculator(void *p) {
      delete ((::Hfitter::HftAbsStateCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftAbsStateCalculator(void *p) {
      delete [] ((::Hfitter::HftAbsStateCalculator*)p);
   }
   static void destruct_HfittercLcLHftAbsStateCalculator(void *p) {
      typedef ::Hfitter::HftAbsStateCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsStateCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAsimovCalculator(void *p) {
      delete ((::Hfitter::HftAsimovCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftAsimovCalculator(void *p) {
      delete [] ((::Hfitter::HftAsimovCalculator*)p);
   }
   static void destruct_HfittercLcLHftAsimovCalculator(void *p) {
      typedef ::Hfitter::HftAsimovCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAsimovCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftToysCalculator(void *p) {
      delete ((::Hfitter::HftToysCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftToysCalculator(void *p) {
      delete [] ((::Hfitter::HftToysCalculator*)p);
   }
   static void destruct_HfittercLcLHftToysCalculator(void *p) {
      typedef ::Hfitter::HftToysCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftToysCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanningCalculator(void *p) {
      delete ((::Hfitter::HftScanningCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftScanningCalculator(void *p) {
      delete [] ((::Hfitter::HftScanningCalculator*)p);
   }
   static void destruct_HfittercLcLHftScanningCalculator(void *p) {
      typedef ::Hfitter::HftScanningCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanningCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftMaxFindingCalculator(void *p) {
      delete ((::Hfitter::HftMaxFindingCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftMaxFindingCalculator(void *p) {
      delete [] ((::Hfitter::HftMaxFindingCalculator*)p);
   }
   static void destruct_HfittercLcLHftMaxFindingCalculator(void *p) {
      typedef ::Hfitter::HftMaxFindingCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftMaxFindingCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsReprocStudy(void *p) {
      delete ((::Hfitter::HftAbsReprocStudy*)p);
   }
   static void deleteArray_HfittercLcLHftAbsReprocStudy(void *p) {
      delete [] ((::Hfitter::HftAbsReprocStudy*)p);
   }
   static void destruct_HfittercLcLHftAbsReprocStudy(void *p) {
      typedef ::Hfitter::HftAbsReprocStudy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsReprocStudy

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftValueCalculator(void *p) {
      delete ((::Hfitter::HftValueCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftValueCalculator(void *p) {
      delete [] ((::Hfitter::HftValueCalculator*)p);
   }
   static void destruct_HfittercLcLHftValueCalculator(void *p) {
      typedef ::Hfitter::HftValueCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftValueCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftFitValueCalculator(void *p) {
      delete ((::Hfitter::HftFitValueCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftFitValueCalculator(void *p) {
      delete [] ((::Hfitter::HftFitValueCalculator*)p);
   }
   static void destruct_HfittercLcLHftFitValueCalculator(void *p) {
      typedef ::Hfitter::HftFitValueCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftFitValueCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftLRCalculator(void *p) {
      delete ((::Hfitter::HftLRCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftLRCalculator(void *p) {
      delete [] ((::Hfitter::HftLRCalculator*)p);
   }
   static void destruct_HfittercLcLHftLRCalculator(void *p) {
      typedef ::Hfitter::HftLRCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftLRCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDataCountingCalculator(void *p) {
      delete ((::Hfitter::HftDataCountingCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftDataCountingCalculator(void *p) {
      delete [] ((::Hfitter::HftDataCountingCalculator*)p);
   }
   static void destruct_HfittercLcLHftDataCountingCalculator(void *p) {
      typedef ::Hfitter::HftDataCountingCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDataCountingCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsStatCalculator(void *p) {
      delete ((::Hfitter::HftAbsStatCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftAbsStatCalculator(void *p) {
      delete [] ((::Hfitter::HftAbsStatCalculator*)p);
   }
   static void destruct_HfittercLcLHftAbsStatCalculator(void *p) {
      typedef ::Hfitter::HftAbsStatCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsStatCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftAbsHypoTestCalculator(void *p) {
      delete ((::Hfitter::HftAbsHypoTestCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftAbsHypoTestCalculator(void *p) {
      delete [] ((::Hfitter::HftAbsHypoTestCalculator*)p);
   }
   static void destruct_HfittercLcLHftAbsHypoTestCalculator(void *p) {
      typedef ::Hfitter::HftAbsHypoTestCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftAbsHypoTestCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftExpHypoTestCalc(void *p) {
      delete ((::Hfitter::HftExpHypoTestCalc*)p);
   }
   static void deleteArray_HfittercLcLHftExpHypoTestCalc(void *p) {
      delete [] ((::Hfitter::HftExpHypoTestCalc*)p);
   }
   static void destruct_HfittercLcLHftExpHypoTestCalc(void *p) {
      typedef ::Hfitter::HftExpHypoTestCalc current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftExpHypoTestCalc

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftMultiHypoTestCalculator(void *p) {
      delete ((::Hfitter::HftMultiHypoTestCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftMultiHypoTestCalculator(void *p) {
      delete [] ((::Hfitter::HftMultiHypoTestCalculator*)p);
   }
   static void destruct_HfittercLcLHftMultiHypoTestCalculator(void *p) {
      typedef ::Hfitter::HftMultiHypoTestCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftMultiHypoTestCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanIntervalCalculator(void *p) {
      delete ((::Hfitter::HftScanIntervalCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftScanIntervalCalculator(void *p) {
      delete [] ((::Hfitter::HftScanIntervalCalculator*)p);
   }
   static void destruct_HfittercLcLHftScanIntervalCalculator(void *p) {
      typedef ::Hfitter::HftScanIntervalCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanIntervalCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftIterIntervalCalculator(void *p) {
      delete ((::Hfitter::HftIterIntervalCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftIterIntervalCalculator(void *p) {
      delete [] ((::Hfitter::HftIterIntervalCalculator*)p);
   }
   static void destruct_HfittercLcLHftIterIntervalCalculator(void *p) {
      typedef ::Hfitter::HftIterIntervalCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftIterIntervalCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftMLCalculator(void *p) {
      delete ((::Hfitter::HftMLCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftMLCalculator(void *p) {
      delete [] ((::Hfitter::HftMLCalculator*)p);
   }
   static void destruct_HfittercLcLHftMLCalculator(void *p) {
      typedef ::Hfitter::HftMLCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftMLCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPLRCalculator(void *p) {
      delete ((::Hfitter::HftPLRCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftPLRCalculator(void *p) {
      delete [] ((::Hfitter::HftPLRCalculator*)p);
   }
   static void destruct_HfittercLcLHftPLRCalculator(void *p) {
      typedef ::Hfitter::HftPLRCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPLRCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanLimitCalculator(void *p) {
      delete ((::Hfitter::HftScanLimitCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftScanLimitCalculator(void *p) {
      delete [] ((::Hfitter::HftScanLimitCalculator*)p);
   }
   static void destruct_HfittercLcLHftScanLimitCalculator(void *p) {
      typedef ::Hfitter::HftScanLimitCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanLimitCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftIterLimitCalculator(void *p) {
      delete ((::Hfitter::HftIterLimitCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftIterLimitCalculator(void *p) {
      delete [] ((::Hfitter::HftIterLimitCalculator*)p);
   }
   static void destruct_HfittercLcLHftIterLimitCalculator(void *p) {
      typedef ::Hfitter::HftIterLimitCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftIterLimitCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPLRPValueCalculator(void *p) {
      delete ((::Hfitter::HftPLRPValueCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftPLRPValueCalculator(void *p) {
      delete [] ((::Hfitter::HftPLRPValueCalculator*)p);
   }
   static void destruct_HfittercLcLHftPLRPValueCalculator(void *p) {
      typedef ::Hfitter::HftPLRPValueCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPLRPValueCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftOneSidedPValueCalculator(void *p) {
      delete ((::Hfitter::HftOneSidedPValueCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftOneSidedPValueCalculator(void *p) {
      delete [] ((::Hfitter::HftOneSidedPValueCalculator*)p);
   }
   static void destruct_HfittercLcLHftOneSidedPValueCalculator(void *p) {
      typedef ::Hfitter::HftOneSidedPValueCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftOneSidedPValueCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftUncappedPValueCalculator(void *p) {
      delete ((::Hfitter::HftUncappedPValueCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftUncappedPValueCalculator(void *p) {
      delete [] ((::Hfitter::HftUncappedPValueCalculator*)p);
   }
   static void destruct_HfittercLcLHftUncappedPValueCalculator(void *p) {
      typedef ::Hfitter::HftUncappedPValueCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftUncappedPValueCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftDiscreteTestCalculator(void *p) {
      delete ((::Hfitter::HftDiscreteTestCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftDiscreteTestCalculator(void *p) {
      delete [] ((::Hfitter::HftDiscreteTestCalculator*)p);
   }
   static void destruct_HfittercLcLHftDiscreteTestCalculator(void *p) {
      typedef ::Hfitter::HftDiscreteTestCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftDiscreteTestCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftPLRLimitCalculator(void *p) {
      delete ((::Hfitter::HftPLRLimitCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftPLRLimitCalculator(void *p) {
      delete [] ((::Hfitter::HftPLRLimitCalculator*)p);
   }
   static void destruct_HfittercLcLHftPLRLimitCalculator(void *p) {
      typedef ::Hfitter::HftPLRLimitCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftPLRLimitCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftQLimitCalculator(void *p) {
      delete ((::Hfitter::HftQLimitCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftQLimitCalculator(void *p) {
      delete [] ((::Hfitter::HftQLimitCalculator*)p);
   }
   static void destruct_HfittercLcLHftQLimitCalculator(void *p) {
      typedef ::Hfitter::HftQLimitCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftQLimitCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftQTildaLimitCalculator(void *p) {
      delete ((::Hfitter::HftQTildaLimitCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftQTildaLimitCalculator(void *p) {
      delete [] ((::Hfitter::HftQTildaLimitCalculator*)p);
   }
   static void destruct_HfittercLcLHftQTildaLimitCalculator(void *p) {
      typedef ::Hfitter::HftQTildaLimitCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftQTildaLimitCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftCLsCalculator(void *p) {
      delete ((::Hfitter::HftCLsCalculator*)p);
   }
   static void deleteArray_HfittercLcLHftCLsCalculator(void *p) {
      delete [] ((::Hfitter::HftCLsCalculator*)p);
   }
   static void destruct_HfittercLcLHftCLsCalculator(void *p) {
      typedef ::Hfitter::HftCLsCalculator current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftCLsCalculator

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHftScanStudy(void *p) {
      delete ((::Hfitter::HftScanStudy*)p);
   }
   static void deleteArray_HfittercLcLHftScanStudy(void *p) {
      delete [] ((::Hfitter::HftScanStudy*)p);
   }
   static void destruct_HfittercLcLHftScanStudy(void *p) {
      typedef ::Hfitter::HftScanStudy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftScanStudy

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftStatTools(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftStatTools : new ::Hfitter::HftStatTools;
   }
   static void *newArray_HfittercLcLHftStatTools(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HftStatTools[nElements] : new ::Hfitter::HftStatTools[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftStatTools(void *p) {
      delete ((::Hfitter::HftStatTools*)p);
   }
   static void deleteArray_HfittercLcLHftStatTools(void *p) {
      delete [] ((::Hfitter::HftStatTools*)p);
   }
   static void destruct_HfittercLcLHftStatTools(void *p) {
      typedef ::Hfitter::HftStatTools current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftStatTools

namespace ROOT {
   static TClass *vectorlEvectorlEdoublegRsPgR_Dictionary();
   static void vectorlEvectorlEdoublegRsPgR_TClassManip(TClass*);
   static void *new_vectorlEvectorlEdoublegRsPgR(void *p = 0);
   static void *newArray_vectorlEvectorlEdoublegRsPgR(Long_t size, void *p);
   static void delete_vectorlEvectorlEdoublegRsPgR(void *p);
   static void deleteArray_vectorlEvectorlEdoublegRsPgR(void *p);
   static void destruct_vectorlEvectorlEdoublegRsPgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<vector<double> >*)
   {
      vector<vector<double> > *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<vector<double> >));
      static ::ROOT::TGenericClassInfo 
         instance("vector<vector<double> >", -2, "vector", 214,
                  typeid(vector<vector<double> >), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEvectorlEdoublegRsPgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<vector<double> >) );
      instance.SetNew(&new_vectorlEvectorlEdoublegRsPgR);
      instance.SetNewArray(&newArray_vectorlEvectorlEdoublegRsPgR);
      instance.SetDelete(&delete_vectorlEvectorlEdoublegRsPgR);
      instance.SetDeleteArray(&deleteArray_vectorlEvectorlEdoublegRsPgR);
      instance.SetDestructor(&destruct_vectorlEvectorlEdoublegRsPgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<vector<double> > >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<vector<double> >*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEvectorlEdoublegRsPgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<vector<double> >*)0x0)->GetClass();
      vectorlEvectorlEdoublegRsPgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEvectorlEdoublegRsPgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEvectorlEdoublegRsPgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<double> > : new vector<vector<double> >;
   }
   static void *newArray_vectorlEvectorlEdoublegRsPgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<vector<double> >[nElements] : new vector<vector<double> >[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEvectorlEdoublegRsPgR(void *p) {
      delete ((vector<vector<double> >*)p);
   }
   static void deleteArray_vectorlEvectorlEdoublegRsPgR(void *p) {
      delete [] ((vector<vector<double> >*)p);
   }
   static void destruct_vectorlEvectorlEdoublegRsPgR(void *p) {
      typedef vector<vector<double> > current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<vector<double> >

namespace ROOT {
   static TClass *vectorlEunsignedsPintgR_Dictionary();
   static void vectorlEunsignedsPintgR_TClassManip(TClass*);
   static void *new_vectorlEunsignedsPintgR(void *p = 0);
   static void *newArray_vectorlEunsignedsPintgR(Long_t size, void *p);
   static void delete_vectorlEunsignedsPintgR(void *p);
   static void deleteArray_vectorlEunsignedsPintgR(void *p);
   static void destruct_vectorlEunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<unsigned int>*)
   {
      vector<unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<unsigned int>", -2, "vector", 214,
                  typeid(vector<unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEunsignedsPintgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<unsigned int>) );
      instance.SetNew(&new_vectorlEunsignedsPintgR);
      instance.SetNewArray(&newArray_vectorlEunsignedsPintgR);
      instance.SetDelete(&delete_vectorlEunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_vectorlEunsignedsPintgR);
      instance.SetDestructor(&destruct_vectorlEunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<unsigned int>*)0x0)->GetClass();
      vectorlEunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int> : new vector<unsigned int>;
   }
   static void *newArray_vectorlEunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<unsigned int>[nElements] : new vector<unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEunsignedsPintgR(void *p) {
      delete ((vector<unsigned int>*)p);
   }
   static void deleteArray_vectorlEunsignedsPintgR(void *p) {
      delete [] ((vector<unsigned int>*)p);
   }
   static void destruct_vectorlEunsignedsPintgR(void *p) {
      typedef vector<unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<unsigned int>

namespace ROOT {
   static TClass *vectorlEdoublegR_Dictionary();
   static void vectorlEdoublegR_TClassManip(TClass*);
   static void *new_vectorlEdoublegR(void *p = 0);
   static void *newArray_vectorlEdoublegR(Long_t size, void *p);
   static void delete_vectorlEdoublegR(void *p);
   static void deleteArray_vectorlEdoublegR(void *p);
   static void destruct_vectorlEdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<double>*)
   {
      vector<double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<double>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<double>", -2, "vector", 214,
                  typeid(vector<double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<double>) );
      instance.SetNew(&new_vectorlEdoublegR);
      instance.SetNewArray(&newArray_vectorlEdoublegR);
      instance.SetDelete(&delete_vectorlEdoublegR);
      instance.SetDeleteArray(&deleteArray_vectorlEdoublegR);
      instance.SetDestructor(&destruct_vectorlEdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<double>*)0x0)->GetClass();
      vectorlEdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double> : new vector<double>;
   }
   static void *newArray_vectorlEdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<double>[nElements] : new vector<double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEdoublegR(void *p) {
      delete ((vector<double>*)p);
   }
   static void deleteArray_vectorlEdoublegR(void *p) {
      delete [] ((vector<double>*)p);
   }
   static void destruct_vectorlEdoublegR(void *p) {
      typedef vector<double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<double>

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace ROOT {
   static TClass *vectorlERooRealVarmUgR_Dictionary();
   static void vectorlERooRealVarmUgR_TClassManip(TClass*);
   static void *new_vectorlERooRealVarmUgR(void *p = 0);
   static void *newArray_vectorlERooRealVarmUgR(Long_t size, void *p);
   static void delete_vectorlERooRealVarmUgR(void *p);
   static void deleteArray_vectorlERooRealVarmUgR(void *p);
   static void destruct_vectorlERooRealVarmUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<RooRealVar*>*)
   {
      vector<RooRealVar*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<RooRealVar*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<RooRealVar*>", -2, "vector", 214,
                  typeid(vector<RooRealVar*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlERooRealVarmUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<RooRealVar*>) );
      instance.SetNew(&new_vectorlERooRealVarmUgR);
      instance.SetNewArray(&newArray_vectorlERooRealVarmUgR);
      instance.SetDelete(&delete_vectorlERooRealVarmUgR);
      instance.SetDeleteArray(&deleteArray_vectorlERooRealVarmUgR);
      instance.SetDestructor(&destruct_vectorlERooRealVarmUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<RooRealVar*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<RooRealVar*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERooRealVarmUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<RooRealVar*>*)0x0)->GetClass();
      vectorlERooRealVarmUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERooRealVarmUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERooRealVarmUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooRealVar*> : new vector<RooRealVar*>;
   }
   static void *newArray_vectorlERooRealVarmUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooRealVar*>[nElements] : new vector<RooRealVar*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERooRealVarmUgR(void *p) {
      delete ((vector<RooRealVar*>*)p);
   }
   static void deleteArray_vectorlERooRealVarmUgR(void *p) {
      delete [] ((vector<RooRealVar*>*)p);
   }
   static void destruct_vectorlERooRealVarmUgR(void *p) {
      typedef vector<RooRealVar*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<RooRealVar*>

namespace ROOT {
   static TClass *vectorlERooAbsDatamUgR_Dictionary();
   static void vectorlERooAbsDatamUgR_TClassManip(TClass*);
   static void *new_vectorlERooAbsDatamUgR(void *p = 0);
   static void *newArray_vectorlERooAbsDatamUgR(Long_t size, void *p);
   static void delete_vectorlERooAbsDatamUgR(void *p);
   static void deleteArray_vectorlERooAbsDatamUgR(void *p);
   static void destruct_vectorlERooAbsDatamUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<RooAbsData*>*)
   {
      vector<RooAbsData*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<RooAbsData*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<RooAbsData*>", -2, "vector", 214,
                  typeid(vector<RooAbsData*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlERooAbsDatamUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<RooAbsData*>) );
      instance.SetNew(&new_vectorlERooAbsDatamUgR);
      instance.SetNewArray(&newArray_vectorlERooAbsDatamUgR);
      instance.SetDelete(&delete_vectorlERooAbsDatamUgR);
      instance.SetDeleteArray(&deleteArray_vectorlERooAbsDatamUgR);
      instance.SetDestructor(&destruct_vectorlERooAbsDatamUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<RooAbsData*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<RooAbsData*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlERooAbsDatamUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<RooAbsData*>*)0x0)->GetClass();
      vectorlERooAbsDatamUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlERooAbsDatamUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlERooAbsDatamUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooAbsData*> : new vector<RooAbsData*>;
   }
   static void *newArray_vectorlERooAbsDatamUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<RooAbsData*>[nElements] : new vector<RooAbsData*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlERooAbsDatamUgR(void *p) {
      delete ((vector<RooAbsData*>*)p);
   }
   static void deleteArray_vectorlERooAbsDatamUgR(void *p) {
      delete [] ((vector<RooAbsData*>*)p);
   }
   static void destruct_vectorlERooAbsDatamUgR(void *p) {
      typedef vector<RooAbsData*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<RooAbsData*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftScanPointCalculatormUgR_Dictionary();
   static void vectorlEHfittercLcLHftScanPointCalculatormUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftScanPointCalculatormUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftScanPointCalculator*>*)
   {
      vector<Hfitter::HftScanPointCalculator*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftScanPointCalculator*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftScanPointCalculator*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftScanPointCalculator*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftScanPointCalculatormUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftScanPointCalculator*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftScanPointCalculatormUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftScanPointCalculatormUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftScanPointCalculatormUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftScanPointCalculatormUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftScanPointCalculatormUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftScanPointCalculator*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftScanPointCalculator*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftScanPointCalculatormUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftScanPointCalculator*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftScanPointCalculatormUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftScanPointCalculatormUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftScanPointCalculator*> : new vector<Hfitter::HftScanPointCalculator*>;
   }
   static void *newArray_vectorlEHfittercLcLHftScanPointCalculatormUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftScanPointCalculator*>[nElements] : new vector<Hfitter::HftScanPointCalculator*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p) {
      delete ((vector<Hfitter::HftScanPointCalculator*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p) {
      delete [] ((vector<Hfitter::HftScanPointCalculator*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftScanPointCalculatormUgR(void *p) {
      typedef vector<Hfitter::HftScanPointCalculator*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftScanPointCalculator*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftScanPointgR_Dictionary();
   static void vectorlEHfittercLcLHftScanPointgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftScanPointgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftScanPointgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftScanPointgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftScanPointgR(void *p);
   static void destruct_vectorlEHfittercLcLHftScanPointgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftScanPoint>*)
   {
      vector<Hfitter::HftScanPoint> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftScanPoint>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftScanPoint>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftScanPoint>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftScanPointgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftScanPoint>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftScanPointgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftScanPointgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftScanPointgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftScanPointgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftScanPointgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftScanPoint> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftScanPoint>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftScanPointgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftScanPoint>*)0x0)->GetClass();
      vectorlEHfittercLcLHftScanPointgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftScanPointgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftScanPointgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftScanPoint> : new vector<Hfitter::HftScanPoint>;
   }
   static void *newArray_vectorlEHfittercLcLHftScanPointgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftScanPoint>[nElements] : new vector<Hfitter::HftScanPoint>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftScanPointgR(void *p) {
      delete ((vector<Hfitter::HftScanPoint>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftScanPointgR(void *p) {
      delete [] ((vector<Hfitter::HftScanPoint>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftScanPointgR(void *p) {
      typedef vector<Hfitter::HftScanPoint> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftScanPoint>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftParameterStoragegR_Dictionary();
   static void vectorlEHfittercLcLHftParameterStoragegR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftParameterStoragegR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftParameterStoragegR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftParameterStoragegR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftParameterStoragegR(void *p);
   static void destruct_vectorlEHfittercLcLHftParameterStoragegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftParameterStorage>*)
   {
      vector<Hfitter::HftParameterStorage> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftParameterStorage>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftParameterStorage>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftParameterStorage>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftParameterStoragegR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftParameterStorage>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftParameterStoragegR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftParameterStoragegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftParameterStorage> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftParameterStorage>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftParameterStoragegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftParameterStorage>*)0x0)->GetClass();
      vectorlEHfittercLcLHftParameterStoragegR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftParameterStoragegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftParameterStorage> : new vector<Hfitter::HftParameterStorage>;
   }
   static void *newArray_vectorlEHfittercLcLHftParameterStoragegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftParameterStorage>[nElements] : new vector<Hfitter::HftParameterStorage>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      delete ((vector<Hfitter::HftParameterStorage>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      delete [] ((vector<Hfitter::HftParameterStorage>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftParameterStoragegR(void *p) {
      typedef vector<Hfitter::HftParameterStorage> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftParameterStorage>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftPLRCalculatormUgR_Dictionary();
   static void vectorlEHfittercLcLHftPLRCalculatormUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftPLRCalculatormUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftPLRCalculator*>*)
   {
      vector<Hfitter::HftPLRCalculator*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftPLRCalculator*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftPLRCalculator*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftPLRCalculator*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftPLRCalculatormUgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<Hfitter::HftPLRCalculator*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftPLRCalculatormUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftPLRCalculatormUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftPLRCalculatormUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftPLRCalculatormUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftPLRCalculatormUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftPLRCalculator*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftPLRCalculator*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftPLRCalculatormUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftPLRCalculator*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftPLRCalculatormUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftPLRCalculatormUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftPLRCalculator*> : new vector<Hfitter::HftPLRCalculator*>;
   }
   static void *newArray_vectorlEHfittercLcLHftPLRCalculatormUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftPLRCalculator*>[nElements] : new vector<Hfitter::HftPLRCalculator*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p) {
      delete ((vector<Hfitter::HftPLRCalculator*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p) {
      delete [] ((vector<Hfitter::HftPLRCalculator*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftPLRCalculatormUgR(void *p) {
      typedef vector<Hfitter::HftPLRCalculator*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftPLRCalculator*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR_Dictionary();
   static void vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftMultiHypoTestCalculator*>*)
   {
      vector<Hfitter::HftMultiHypoTestCalculator*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftMultiHypoTestCalculator*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftMultiHypoTestCalculator*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftMultiHypoTestCalculator*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Hfitter::HftMultiHypoTestCalculator*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftMultiHypoTestCalculator*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftMultiHypoTestCalculator*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftMultiHypoTestCalculator*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftMultiHypoTestCalculator*> : new vector<Hfitter::HftMultiHypoTestCalculator*>;
   }
   static void *newArray_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftMultiHypoTestCalculator*>[nElements] : new vector<Hfitter::HftMultiHypoTestCalculator*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p) {
      delete ((vector<Hfitter::HftMultiHypoTestCalculator*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p) {
      delete [] ((vector<Hfitter::HftMultiHypoTestCalculator*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftMultiHypoTestCalculatormUgR(void *p) {
      typedef vector<Hfitter::HftMultiHypoTestCalculator*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftMultiHypoTestCalculator*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftIntervalgR_Dictionary();
   static void vectorlEHfittercLcLHftIntervalgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftIntervalgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftIntervalgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftIntervalgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftIntervalgR(void *p);
   static void destruct_vectorlEHfittercLcLHftIntervalgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftInterval>*)
   {
      vector<Hfitter::HftInterval> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftInterval>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftInterval>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftInterval>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftIntervalgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Hfitter::HftInterval>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftIntervalgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftIntervalgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftIntervalgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftIntervalgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftIntervalgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftInterval> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftInterval>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftIntervalgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftInterval>*)0x0)->GetClass();
      vectorlEHfittercLcLHftIntervalgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftIntervalgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftIntervalgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftInterval> : new vector<Hfitter::HftInterval>;
   }
   static void *newArray_vectorlEHfittercLcLHftIntervalgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftInterval>[nElements] : new vector<Hfitter::HftInterval>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftIntervalgR(void *p) {
      delete ((vector<Hfitter::HftInterval>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftIntervalgR(void *p) {
      delete [] ((vector<Hfitter::HftInterval>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftIntervalgR(void *p) {
      typedef vector<Hfitter::HftInterval> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftInterval>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR_Dictionary();
   static void vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftAbsHypoTestCalculator*>*)
   {
      vector<Hfitter::HftAbsHypoTestCalculator*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftAbsHypoTestCalculator*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftAbsHypoTestCalculator*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftAbsHypoTestCalculator*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Hfitter::HftAbsHypoTestCalculator*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftAbsHypoTestCalculator*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftAbsHypoTestCalculator*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftAbsHypoTestCalculator*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsHypoTestCalculator*> : new vector<Hfitter::HftAbsHypoTestCalculator*>;
   }
   static void *newArray_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsHypoTestCalculator*>[nElements] : new vector<Hfitter::HftAbsHypoTestCalculator*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p) {
      delete ((vector<Hfitter::HftAbsHypoTestCalculator*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p) {
      delete [] ((vector<Hfitter::HftAbsHypoTestCalculator*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftAbsHypoTestCalculatormUgR(void *p) {
      typedef vector<Hfitter::HftAbsHypoTestCalculator*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftAbsHypoTestCalculator*>

namespace ROOT {
   static TClass *vectorlEHfittercLcLHftAbsCalculatormUgR_Dictionary();
   static void vectorlEHfittercLcLHftAbsCalculatormUgR_TClassManip(TClass*);
   static void *new_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p = 0);
   static void *newArray_vectorlEHfittercLcLHftAbsCalculatormUgR(Long_t size, void *p);
   static void delete_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p);
   static void deleteArray_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p);
   static void destruct_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<Hfitter::HftAbsCalculator*>*)
   {
      vector<Hfitter::HftAbsCalculator*> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<Hfitter::HftAbsCalculator*>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<Hfitter::HftAbsCalculator*>", -2, "vector", 214,
                  typeid(vector<Hfitter::HftAbsCalculator*>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlEHfittercLcLHftAbsCalculatormUgR_Dictionary, isa_proxy, 4,
                  sizeof(vector<Hfitter::HftAbsCalculator*>) );
      instance.SetNew(&new_vectorlEHfittercLcLHftAbsCalculatormUgR);
      instance.SetNewArray(&newArray_vectorlEHfittercLcLHftAbsCalculatormUgR);
      instance.SetDelete(&delete_vectorlEHfittercLcLHftAbsCalculatormUgR);
      instance.SetDeleteArray(&deleteArray_vectorlEHfittercLcLHftAbsCalculatormUgR);
      instance.SetDestructor(&destruct_vectorlEHfittercLcLHftAbsCalculatormUgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<Hfitter::HftAbsCalculator*> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<Hfitter::HftAbsCalculator*>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlEHfittercLcLHftAbsCalculatormUgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<Hfitter::HftAbsCalculator*>*)0x0)->GetClass();
      vectorlEHfittercLcLHftAbsCalculatormUgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlEHfittercLcLHftAbsCalculatormUgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsCalculator*> : new vector<Hfitter::HftAbsCalculator*>;
   }
   static void *newArray_vectorlEHfittercLcLHftAbsCalculatormUgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<Hfitter::HftAbsCalculator*>[nElements] : new vector<Hfitter::HftAbsCalculator*>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p) {
      delete ((vector<Hfitter::HftAbsCalculator*>*)p);
   }
   static void deleteArray_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p) {
      delete [] ((vector<Hfitter::HftAbsCalculator*>*)p);
   }
   static void destruct_vectorlEHfittercLcLHftAbsCalculatormUgR(void *p) {
      typedef vector<Hfitter::HftAbsCalculator*> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<Hfitter::HftAbsCalculator*>

namespace ROOT {
   static TClass *maplEstringcOdoublegR_Dictionary();
   static void maplEstringcOdoublegR_TClassManip(TClass*);
   static void *new_maplEstringcOdoublegR(void *p = 0);
   static void *newArray_maplEstringcOdoublegR(Long_t size, void *p);
   static void delete_maplEstringcOdoublegR(void *p);
   static void deleteArray_maplEstringcOdoublegR(void *p);
   static void destruct_maplEstringcOdoublegR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<string,double>*)
   {
      map<string,double> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<string,double>));
      static ::ROOT::TGenericClassInfo 
         instance("map<string,double>", -2, "map", 96,
                  typeid(map<string,double>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplEstringcOdoublegR_Dictionary, isa_proxy, 0,
                  sizeof(map<string,double>) );
      instance.SetNew(&new_maplEstringcOdoublegR);
      instance.SetNewArray(&newArray_maplEstringcOdoublegR);
      instance.SetDelete(&delete_maplEstringcOdoublegR);
      instance.SetDeleteArray(&deleteArray_maplEstringcOdoublegR);
      instance.SetDestructor(&destruct_maplEstringcOdoublegR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<string,double> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<string,double>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplEstringcOdoublegR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<string,double>*)0x0)->GetClass();
      maplEstringcOdoublegR_TClassManip(theClass);
   return theClass;
   }

   static void maplEstringcOdoublegR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplEstringcOdoublegR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double> : new map<string,double>;
   }
   static void *newArray_maplEstringcOdoublegR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<string,double>[nElements] : new map<string,double>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplEstringcOdoublegR(void *p) {
      delete ((map<string,double>*)p);
   }
   static void deleteArray_maplEstringcOdoublegR(void *p) {
      delete [] ((map<string,double>*)p);
   }
   static void destruct_maplEstringcOdoublegR(void *p) {
      typedef map<string,double> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<string,double>

namespace ROOT {
   static TClass *maplETStringcOunsignedsPintgR_Dictionary();
   static void maplETStringcOunsignedsPintgR_TClassManip(TClass*);
   static void *new_maplETStringcOunsignedsPintgR(void *p = 0);
   static void *newArray_maplETStringcOunsignedsPintgR(Long_t size, void *p);
   static void delete_maplETStringcOunsignedsPintgR(void *p);
   static void deleteArray_maplETStringcOunsignedsPintgR(void *p);
   static void destruct_maplETStringcOunsignedsPintgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const map<TString,unsigned int>*)
   {
      map<TString,unsigned int> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(map<TString,unsigned int>));
      static ::ROOT::TGenericClassInfo 
         instance("map<TString,unsigned int>", -2, "map", 96,
                  typeid(map<TString,unsigned int>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &maplETStringcOunsignedsPintgR_Dictionary, isa_proxy, 0,
                  sizeof(map<TString,unsigned int>) );
      instance.SetNew(&new_maplETStringcOunsignedsPintgR);
      instance.SetNewArray(&newArray_maplETStringcOunsignedsPintgR);
      instance.SetDelete(&delete_maplETStringcOunsignedsPintgR);
      instance.SetDeleteArray(&deleteArray_maplETStringcOunsignedsPintgR);
      instance.SetDestructor(&destruct_maplETStringcOunsignedsPintgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::MapInsert< map<TString,unsigned int> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const map<TString,unsigned int>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *maplETStringcOunsignedsPintgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const map<TString,unsigned int>*)0x0)->GetClass();
      maplETStringcOunsignedsPintgR_TClassManip(theClass);
   return theClass;
   }

   static void maplETStringcOunsignedsPintgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_maplETStringcOunsignedsPintgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,unsigned int> : new map<TString,unsigned int>;
   }
   static void *newArray_maplETStringcOunsignedsPintgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) map<TString,unsigned int>[nElements] : new map<TString,unsigned int>[nElements];
   }
   // Wrapper around operator delete
   static void delete_maplETStringcOunsignedsPintgR(void *p) {
      delete ((map<TString,unsigned int>*)p);
   }
   static void deleteArray_maplETStringcOunsignedsPintgR(void *p) {
      delete [] ((map<TString,unsigned int>*)p);
   }
   static void destruct_maplETStringcOunsignedsPintgR(void *p) {
      typedef map<TString,unsigned int> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class map<TString,unsigned int>

namespace {
  void TriggerDictionaryInitialization_HfitterStatsCINT_Impl() {
    static const char* headers[] = {
"HfitterStats/HftInterval.h",
"HfitterStats/HftBand.h",
"HfitterStats/HftAbsCalculator.h",
"HfitterStats/HftTree.h",
"HfitterStats/HftAbsMultiResultCalculator.h",
"HfitterStats/HftScanPoint.h",
"HfitterStats/HftScanGrid.h",
"HfitterStats/HftMultiCalculator.h",
"HfitterStats/HftMaxFindingCalculator.h",
"HfitterStats/HftScanningCalculator.h",
"HfitterStats/HftScanPointCalculator.h",
"HfitterStats/HftAsimovCalculator.h",
"HfitterStats/HftAbsStateCalculator.h",
"HfitterStats/HftToysCalculator.h",
"HfitterStats/HftAbsReprocStudy.h",
"HfitterStats/HftValueCalculator.h",
"HfitterStats/HftFitValueCalculator.h",
"HfitterStats/HftLRCalculator.h",
"HfitterStats/HftDataCountingCalculator.h",
"HfitterStats/HftAbsStatCalculator.h",
"HfitterStats/HftAbsHypoTestCalculator.h",
"HfitterStats/HftExpHypoTestCalc.h",
"HfitterStats/HftScanIntervalCalculator.h",
"HfitterStats/HftMultiHypoTestCalculator.h",
"HfitterStats/HftIterIntervalCalculator.h",
"HfitterStats/HftMLCalculator.h",
"HfitterStats/HftPLRCalculator.h",
"HfitterStats/HftScanLimitCalculator.h",
"HfitterStats/HftIterLimitCalculator.h",
"HfitterStats/HftOneSidedPValueCalculator.h",
"HfitterStats/HftPLRPValueCalculator.h",
"HfitterStats/HftUncappedPValueCalculator.h",
"HfitterStats/HftDiscreteTestCalculator.h",
"HfitterStats/HftPLRLimitCalculator.h",
"HfitterStats/HftQLimitCalculator.h",
"HfitterStats/HftQTildaLimitCalculator.h",
"HfitterStats/HftCLsCalculator.h",
"HfitterStats/HftScanStudy.h",
"HfitterStats/HftStatTools.h",
"HfitterStats/HftOneSidedP0Calculator.h",
"HfitterStats/HftUncappedP0Calculator.h",
"HfitterStats/HftCLsbQCalculator.h",
"HfitterStats/HftCLsbQTildaCalculator.h",
"HfitterStats/HftCLSolvingCalculator.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/HfitterStats/Root",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/HfitterStats",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.14.04-0d8dc/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/HfitterStats/cmt/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "HfitterStatsCINT dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftInterval.h")))  HftInterval;}
namespace std{template <typename _Tp> class __attribute__((annotate("$clingAutoload$bits/allocator.h")))  __attribute__((annotate("$clingAutoload$string")))  allocator;
}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsCalculator.h")))  HftAbsCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsHypoTestCalculator.h")))  HftAbsHypoTestCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftMultiHypoTestCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftScanIntervalCalculator.h")))  HftMultiHypoTestCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftBand.h")))  HftBand;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftTree.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsCalculator.h")))  HftTree;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsMultiResultCalculator.h")))  HftAbsMultiResultCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanPoint.h")))  HftScanPoint;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanGrid.h")))  HftScanGrid;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftMultiCalculator.h")))  HftMultiCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanPointCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftMaxFindingCalculator.h")))  HftScanPointCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsStateCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftMaxFindingCalculator.h")))  HftAbsStateCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAsimovCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftMaxFindingCalculator.h")))  HftAsimovCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftToysCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftMaxFindingCalculator.h")))  HftToysCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanningCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftMaxFindingCalculator.h")))  HftScanningCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftMaxFindingCalculator.h")))  HftMaxFindingCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsReprocStudy.h")))  HftAbsReprocStudy;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftValueCalculator.h")))  HftValueCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftFitValueCalculator.h")))  HftFitValueCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftLRCalculator.h")))  HftLRCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftDataCountingCalculator.h")))  HftDataCountingCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftAbsStatCalculator.h")))  HftAbsStatCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftExpHypoTestCalc.h")))  HftExpHypoTestCalc;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanIntervalCalculator.h")))  HftScanIntervalCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftIterIntervalCalculator.h")))  HftIterIntervalCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftMLCalculator.h")))  HftMLCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftPLRCalculator.h")))  HftPLRCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanLimitCalculator.h")))  HftScanLimitCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftIterLimitCalculator.h")))  HftIterLimitCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftPLRPValueCalculator.h")))  __attribute__((annotate("$clingAutoload$HfitterStats/HftOneSidedPValueCalculator.h")))  HftPLRPValueCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftOneSidedPValueCalculator.h")))  HftOneSidedPValueCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftUncappedPValueCalculator.h")))  HftUncappedPValueCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftDiscreteTestCalculator.h")))  HftDiscreteTestCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftPLRLimitCalculator.h")))  HftPLRLimitCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftQLimitCalculator.h")))  HftQLimitCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftQTildaLimitCalculator.h")))  HftQTildaLimitCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftCLsCalculator.h")))  HftCLsCalculator;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftScanStudy.h")))  HftScanStudy;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HfitterStats/HftStatTools.h")))  HftStatTools;}
namespace Hfitter{typedef Hfitter::HftOneSidedPValueCalculator HftOneSidedP0Calculator __attribute__((annotate("$clingAutoload$HfitterStats/HftOneSidedP0Calculator.h"))) ;}
namespace Hfitter{typedef Hfitter::HftUncappedPValueCalculator HftUncappedP0Calculator __attribute__((annotate("$clingAutoload$HfitterStats/HftUncappedP0Calculator.h"))) ;}
namespace Hfitter{typedef Hfitter::HftQLimitCalculator HftCLsbQCalculator __attribute__((annotate("$clingAutoload$HfitterStats/HftCLsbQCalculator.h"))) ;}
namespace Hfitter{typedef Hfitter::HftQTildaLimitCalculator HftCLsbQTildaCalculator __attribute__((annotate("$clingAutoload$HfitterStats/HftCLsbQTildaCalculator.h"))) ;}
namespace Hfitter{typedef Hfitter::HftScanLimitCalculator HftCLSolvingCalculator __attribute__((annotate("$clingAutoload$HfitterStats/HftCLSolvingCalculator.h"))) ;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "HfitterStatsCINT dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "HfitterStats"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HfitterStats/HftInterval.h"
#include "HfitterStats/HftBand.h"
#include "HfitterStats/HftAbsCalculator.h"
#include "HfitterStats/HftTree.h"
#include "HfitterStats/HftAbsMultiResultCalculator.h"
#include "HfitterStats/HftScanPoint.h"
#include "HfitterStats/HftScanGrid.h"
#include "HfitterStats/HftMultiCalculator.h"
#include "HfitterStats/HftMaxFindingCalculator.h"
#include "HfitterStats/HftScanningCalculator.h"
#include "HfitterStats/HftScanPointCalculator.h"
#include "HfitterStats/HftAsimovCalculator.h"
#include "HfitterStats/HftAbsStateCalculator.h"
#include "HfitterStats/HftToysCalculator.h"
#include "HfitterStats/HftAbsReprocStudy.h"
#include "HfitterStats/HftValueCalculator.h"
#include "HfitterStats/HftFitValueCalculator.h"
#include "HfitterStats/HftLRCalculator.h"
#include "HfitterStats/HftDataCountingCalculator.h"
#include "HfitterStats/HftAbsStatCalculator.h"
#include "HfitterStats/HftAbsHypoTestCalculator.h"
#include "HfitterStats/HftExpHypoTestCalc.h"
#include "HfitterStats/HftScanIntervalCalculator.h"
#include "HfitterStats/HftMultiHypoTestCalculator.h"
#include "HfitterStats/HftIterIntervalCalculator.h"
#include "HfitterStats/HftMLCalculator.h"
#include "HfitterStats/HftPLRCalculator.h"
#include "HfitterStats/HftScanLimitCalculator.h"
#include "HfitterStats/HftIterLimitCalculator.h"
#include "HfitterStats/HftOneSidedPValueCalculator.h"
#include "HfitterStats/HftPLRPValueCalculator.h"
#include "HfitterStats/HftUncappedPValueCalculator.h"
#include "HfitterStats/HftDiscreteTestCalculator.h"
#include "HfitterStats/HftPLRLimitCalculator.h"
#include "HfitterStats/HftQLimitCalculator.h"
#include "HfitterStats/HftQTildaLimitCalculator.h"
#include "HfitterStats/HftCLsCalculator.h"
#include "HfitterStats/HftScanStudy.h"
#include "HfitterStats/HftStatTools.h"
#include "HfitterStats/HftOneSidedP0Calculator.h"
#include "HfitterStats/HftUncappedP0Calculator.h"
#include "HfitterStats/HftCLsbQCalculator.h"
#include "HfitterStats/HftCLsbQTildaCalculator.h"
#include "HfitterStats/HftCLSolvingCalculator.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Hfitter::HftAbsCalculator", payloadCode, "@",
"Hfitter::HftAbsHypoTestCalculator", payloadCode, "@",
"Hfitter::HftAbsMultiResultCalculator", payloadCode, "@",
"Hfitter::HftAbsReprocStudy", payloadCode, "@",
"Hfitter::HftAbsStatCalculator", payloadCode, "@",
"Hfitter::HftAbsStateCalculator", payloadCode, "@",
"Hfitter::HftAsimovCalculator", payloadCode, "@",
"Hfitter::HftBand", payloadCode, "@",
"Hfitter::HftCLSolvingCalculator", payloadCode, "@",
"Hfitter::HftCLsCalculator", payloadCode, "@",
"Hfitter::HftCLsbQCalculator", payloadCode, "@",
"Hfitter::HftCLsbQTildaCalculator", payloadCode, "@",
"Hfitter::HftDataCountingCalculator", payloadCode, "@",
"Hfitter::HftDiscreteTestCalculator", payloadCode, "@",
"Hfitter::HftExpHypoTestCalc", payloadCode, "@",
"Hfitter::HftFitValueCalculator", payloadCode, "@",
"Hfitter::HftInterval", payloadCode, "@",
"Hfitter::HftIterIntervalCalculator", payloadCode, "@",
"Hfitter::HftIterLimitCalculator", payloadCode, "@",
"Hfitter::HftLRCalculator", payloadCode, "@",
"Hfitter::HftMLCalculator", payloadCode, "@",
"Hfitter::HftMaxFindingCalculator", payloadCode, "@",
"Hfitter::HftMultiCalculator", payloadCode, "@",
"Hfitter::HftMultiHypoTestCalculator", payloadCode, "@",
"Hfitter::HftOneSidedP0Calculator", payloadCode, "@",
"Hfitter::HftOneSidedPValueCalculator", payloadCode, "@",
"Hfitter::HftPLRCalculator", payloadCode, "@",
"Hfitter::HftPLRLimitCalculator", payloadCode, "@",
"Hfitter::HftPLRPValueCalculator", payloadCode, "@",
"Hfitter::HftQLimitCalculator", payloadCode, "@",
"Hfitter::HftQTildaLimitCalculator", payloadCode, "@",
"Hfitter::HftScanGrid", payloadCode, "@",
"Hfitter::HftScanIntervalCalculator", payloadCode, "@",
"Hfitter::HftScanLimitCalculator", payloadCode, "@",
"Hfitter::HftScanPoint", payloadCode, "@",
"Hfitter::HftScanPointCalculator", payloadCode, "@",
"Hfitter::HftScanStudy", payloadCode, "@",
"Hfitter::HftScanningCalculator", payloadCode, "@",
"Hfitter::HftStatTools", payloadCode, "@",
"Hfitter::HftToysCalculator", payloadCode, "@",
"Hfitter::HftTree", payloadCode, "@",
"Hfitter::HftUncappedP0Calculator", payloadCode, "@",
"Hfitter::HftUncappedPValueCalculator", payloadCode, "@",
"Hfitter::HftValueCalculator", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("HfitterStatsCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_HfitterStatsCINT_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_HfitterStatsCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_HfitterStatsCINT() {
  TriggerDictionaryInitialization_HfitterStatsCINT_Impl();
}
