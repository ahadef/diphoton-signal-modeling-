#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
#ifdef __llvm__
#pragma GCC diagnostic ignored "-Wunused-private-field"
#endif
// Do NOT change. Changes will be lost next time file is generated

#define R__DICTIONARY_FILENAME dIafsdIcerndOchdIworkdIadIahadefdIpublicdIHGGFitterdIFittersdIRootCoredIobjdIx86_64mIslc6mIgcc62mIoptdIHGGfitterdIobjdIHGGfitterCINT

/*******************************************************************/
#include <stddef.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <assert.h>
#define G__DICTIONARY
#include "RConfig.h"
#include "TClass.h"
#include "TDictAttributeMap.h"
#include "TInterpreter.h"
#include "TROOT.h"
#include "TBuffer.h"
#include "TMemberInspector.h"
#include "TInterpreter.h"
#include "TVirtualMutex.h"
#include "TError.h"

#ifndef G__ROOT
#define G__ROOT
#endif

#include "RtypesImp.h"
#include "TIsAProxy.h"
#include "TFileMergeInfo.h"
#include <algorithm>
#include "TCollectionProxyInfo.h"
/*******************************************************************/

#include "TDataMember.h"

// Since CINT ignores the std namespace, we need to do so in this file.
namespace std {} using namespace std;

// Header files passed as explicit arguments
#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "HGGfitter/MggBiasBkgPdf.h"
#include "HGGfitter/HggCBShape.h"
#include "HGGfitter/HggLognormal.h"
#include "HGGfitter/HggTwoSidedCBPdf.h"
#include "HGGfitter/HggTwoSidedNovoPdf.h"
#include "HGGfitter/HggNovoCBPdf.h"
#include "HGGfitter/HggBifurCBPdf.h"
#include "HGGfitter/HggStitchedPlateauPdf.h"
#include "HGGfitter/HggBernstein.h"
#include "HGGfitter/HggExpPowerPdf.h"
#include "HGGfitter/HggPowerLaw.h"
#include "HGGfitter/HggExpPoly.h"
#include "HGGfitter/RooRelBreitWignerPwave.h"
#include "HGGfitter/RooRelBreitWignerPwave_LS.h"
#include "HGGfitter/RooRelBreitWignerPwave_LSCut.h"
#include "HGGfitter/HggMG5TLS.h"
#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "HGGfitter/HggBkgMggDoubleExpPdfBuilder.h"
#include "HGGfitter/HggBkgMggExpPowerPdfBuilder.h"
#include "HGGfitter/HggBkgMggBiasPdfBuilder.h"
#include "HGGfitter/HggBkgMggPolyPdfBuilder.h"
#include "HGGfitter/HggBernsteinPdfBuilder.h"
#include "HGGfitter/HggChebyshevPdfBuilder.h"
#include "HGGfitter/HggBkgMggBernstein4PdfBuilder.h"
#include "HGGfitter/HggBkgMggBernstein5PdfBuilder.h"
#include "HGGfitter/HggBkgMggLaurent4PdfBuilder.h"
#include "HGGfitter/HggBkgMggPower2PdfBuilder.h"
#include "HGGfitter/HggExpPolyPdfBuilder.h"
#include "HGGfitter/HggPolyPdfBuilder.h"
#include "HGGfitter/HggGenericBuilder.h"
#include "HGGfitter/HggEliLandauExpBuilder.h"
#include "HGGfitter/HggSigCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgCosThStarPdfBuilder.h"
#include "HGGfitter/HggSigPTPdfBuilder.h"
#include "HGGfitter/HggBkgPTPdfBuilder.h"
#include "HGGfitter/HggSigMggCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h"
#include "HGGfitter/HggBkgMggPTKeysPdfBuilder.h"
#include "HGGfitter/HggSigMggPTPdfBuilder.h"
#include "HGGfitter/HggBkgMggPTPdfBuilder.h"
#include "HGGfitter/HggSigMggCosThStarPTPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h"
#include "HGGfitter/HggSigMggSmearMPdfBuilder.h"
#include "HGGfitter/HggBkgMggSmearMPdfBuilder.h"
#include "HGGfitter/HggExpPolySmearMPdfBuilder.h"
#include "HGGfitter/HggBernsteinSmearMPdfBuilder.h"
#include "HGGfitter/HggIsolPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolPdfBuilder.h"
#include "HGGfitter/HggBkgMggIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolBernsteinHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolBernsteinHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolExpPolyHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolExpPolyHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsol2DHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolKeysPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolBremPdfBuilder.h"
#include "HGGfitter/HggBkgMggIsolBremPdfBuilder.h"
#include "HGGfitter/HggZeeMassPdfBuilder.h"
#include "HGGfitter/HggZeeNovoMassPdfBuilder.h"
#include "HGGfitter/HggZeeBifurMassPdfBuilder.h"
#include "HGGfitter/HggTwoSidedNovoPdfBuilder.h"
#include "HGGfitter/Hgg2sCBPdfBuilder.h"
#include "HGGfitter/HggBWx2sCBPdfBuilder.h"
#include "HGGfitter/HggCouplings.h"
#include "HGGfitter/HggCouplingsBuilder.h"
#include "HGGfitter/HggTools.h"
#include "HGGfitter/HggEtaCatHelper.h"
#include "HGGfitter/HggResolHelper.h"
#include "HGGfitter/HggZeeIsolStudy.h"
#include "HGGfitter/HggNewResonanceStudy.h"
#include "HGGfitter/HggNLLVar.h"
#include "HGGfitter/HggPowHegLSPdfBuilder.h"
#include "HGGfitter/HggMG5LSPdfBuilder.h"
#include "HGGfitter/HggGravitonLineShapePdf.hh"
#include "HGGfitter/HggGravLSPdfBuilder.h"
#include "HGGfitter/HggGravTLS.h"
#include "HGGfitter/HggFactGravLSPdfBuilder.h"

// Header files passed via #pragma extra_include

namespace Hfitter {
   namespace ROOT {
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance();
      static TClass *Hfitter_Dictionary();

      // Function generating the singleton type initializer
      inline ::ROOT::TGenericClassInfo *GenerateInitInstance()
      {
         static ::ROOT::TGenericClassInfo 
            instance("Hfitter", 0 /*version*/, "HfitterModels/HftInstance.h", 7,
                     ::ROOT::Internal::DefineBehavior((void*)0,(void*)0),
                     &Hfitter_Dictionary, 0);
         return &instance;
      }
      // Insure that the inline function is _not_ optimized away by the compiler
      ::ROOT::TGenericClassInfo *(*_R__UNIQUE_DICT_(InitFunctionKeeper))() = &GenerateInitInstance;  
      // Static variable to force the class initialization
      static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstance(); R__UseDummy(_R__UNIQUE_DICT_(Init));

      // Dictionary for non-ClassDef classes
      static TClass *Hfitter_Dictionary() {
         return GenerateInitInstance()->GetClass();
      }

   }
}

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>", ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<RooExponential,1> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<RooExponential,1> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<RooExponential,1>", ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<RooExponential,1>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<RooExponential,1>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>", ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>", ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR_Dictionary();
   static void HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR_TClassManip(TClass*);
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p = 0);
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(Long_t size, void *p);
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p);
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p);
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)
   {
      ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2> >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HftTemplatePdfBuilder<RooLognormal,2>", ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>::Class_Version(), "HfitterModels/HftTemplatePdfBuilder.h", 17,
                  typeid(::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>) );
      instance.SetNew(&new_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR);
      instance.SetNewArray(&newArray_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR);
      instance.SetDelete(&delete_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR);
      instance.SetDestructor(&destruct_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0x0)->GetClass();
      HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLMggBkgPdf(void *p = 0);
   static void *newArray_HfittercLcLMggBkgPdf(Long_t size, void *p);
   static void delete_HfittercLcLMggBkgPdf(void *p);
   static void deleteArray_HfittercLcLMggBkgPdf(void *p);
   static void destruct_HfittercLcLMggBkgPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::MggBkgPdf*)
   {
      ::Hfitter::MggBkgPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::MggBkgPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::MggBkgPdf", ::Hfitter::MggBkgPdf::Class_Version(), "HGGfitter/MggBkgPdf.h", 17,
                  typeid(::Hfitter::MggBkgPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::MggBkgPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::MggBkgPdf) );
      instance.SetNew(&new_HfittercLcLMggBkgPdf);
      instance.SetNewArray(&newArray_HfittercLcLMggBkgPdf);
      instance.SetDelete(&delete_HfittercLcLMggBkgPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLMggBkgPdf);
      instance.SetDestructor(&destruct_HfittercLcLMggBkgPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::MggBkgPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::MggBkgPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::MggBkgPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLMggBiasBkgPdf(void *p = 0);
   static void *newArray_HfittercLcLMggBiasBkgPdf(Long_t size, void *p);
   static void delete_HfittercLcLMggBiasBkgPdf(void *p);
   static void deleteArray_HfittercLcLMggBiasBkgPdf(void *p);
   static void destruct_HfittercLcLMggBiasBkgPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::MggBiasBkgPdf*)
   {
      ::Hfitter::MggBiasBkgPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::MggBiasBkgPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::MggBiasBkgPdf", ::Hfitter::MggBiasBkgPdf::Class_Version(), "HGGfitter/MggBiasBkgPdf.h", 17,
                  typeid(::Hfitter::MggBiasBkgPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::MggBiasBkgPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::MggBiasBkgPdf) );
      instance.SetNew(&new_HfittercLcLMggBiasBkgPdf);
      instance.SetNewArray(&newArray_HfittercLcLMggBiasBkgPdf);
      instance.SetDelete(&delete_HfittercLcLMggBiasBkgPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLMggBiasBkgPdf);
      instance.SetDestructor(&destruct_HfittercLcLMggBiasBkgPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::MggBiasBkgPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::MggBiasBkgPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::MggBiasBkgPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggLognormal(void *p = 0);
   static void *newArray_HfittercLcLHggLognormal(Long_t size, void *p);
   static void delete_HfittercLcLHggLognormal(void *p);
   static void deleteArray_HfittercLcLHggLognormal(void *p);
   static void destruct_HfittercLcLHggLognormal(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggLognormal*)
   {
      ::Hfitter::HggLognormal *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggLognormal >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggLognormal", ::Hfitter::HggLognormal::Class_Version(), "HGGfitter/HggLognormal.h", 14,
                  typeid(::Hfitter::HggLognormal), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggLognormal::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggLognormal) );
      instance.SetNew(&new_HfittercLcLHggLognormal);
      instance.SetNewArray(&newArray_HfittercLcLHggLognormal);
      instance.SetDelete(&delete_HfittercLcLHggLognormal);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggLognormal);
      instance.SetDestructor(&destruct_HfittercLcLHggLognormal);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggLognormal*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggLognormal*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggLognormal*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void delete_HfittercLcLHggModifiedLognormal(void *p);
   static void deleteArray_HfittercLcLHggModifiedLognormal(void *p);
   static void destruct_HfittercLcLHggModifiedLognormal(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggModifiedLognormal*)
   {
      ::Hfitter::HggModifiedLognormal *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggModifiedLognormal >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggModifiedLognormal", ::Hfitter::HggModifiedLognormal::Class_Version(), "HGGfitter/HggLognormal.h", 59,
                  typeid(::Hfitter::HggModifiedLognormal), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggModifiedLognormal::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggModifiedLognormal) );
      instance.SetDelete(&delete_HfittercLcLHggModifiedLognormal);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggModifiedLognormal);
      instance.SetDestructor(&destruct_HfittercLcLHggModifiedLognormal);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggModifiedLognormal*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggModifiedLognormal*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggModifiedLognormal*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggCBShape(void *p = 0);
   static void *newArray_HfittercLcLHggCBShape(Long_t size, void *p);
   static void delete_HfittercLcLHggCBShape(void *p);
   static void deleteArray_HfittercLcLHggCBShape(void *p);
   static void destruct_HfittercLcLHggCBShape(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggCBShape*)
   {
      ::Hfitter::HggCBShape *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggCBShape >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggCBShape", ::Hfitter::HggCBShape::Class_Version(), "HGGfitter/HggCBShape.h", 33,
                  typeid(::Hfitter::HggCBShape), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggCBShape::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggCBShape) );
      instance.SetNew(&new_HfittercLcLHggCBShape);
      instance.SetNewArray(&newArray_HfittercLcLHggCBShape);
      instance.SetDelete(&delete_HfittercLcLHggCBShape);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggCBShape);
      instance.SetDestructor(&destruct_HfittercLcLHggCBShape);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggCBShape*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggCBShape*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggCBShape*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HggTwoSidedCBPdf(void *p = 0);
   static void *newArray_HggTwoSidedCBPdf(Long_t size, void *p);
   static void delete_HggTwoSidedCBPdf(void *p);
   static void deleteArray_HggTwoSidedCBPdf(void *p);
   static void destruct_HggTwoSidedCBPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HggTwoSidedCBPdf*)
   {
      ::HggTwoSidedCBPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HggTwoSidedCBPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HggTwoSidedCBPdf", ::HggTwoSidedCBPdf::Class_Version(), "HGGfitter/HggTwoSidedCBPdf.h", 12,
                  typeid(::HggTwoSidedCBPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HggTwoSidedCBPdf::Dictionary, isa_proxy, 4,
                  sizeof(::HggTwoSidedCBPdf) );
      instance.SetNew(&new_HggTwoSidedCBPdf);
      instance.SetNewArray(&newArray_HggTwoSidedCBPdf);
      instance.SetDelete(&delete_HggTwoSidedCBPdf);
      instance.SetDeleteArray(&deleteArray_HggTwoSidedCBPdf);
      instance.SetDestructor(&destruct_HggTwoSidedCBPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HggTwoSidedCBPdf*)
   {
      return GenerateInitInstanceLocal((::HggTwoSidedCBPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggTwoSidedNovoPdf(void *p = 0);
   static void *newArray_HfittercLcLHggTwoSidedNovoPdf(Long_t size, void *p);
   static void delete_HfittercLcLHggTwoSidedNovoPdf(void *p);
   static void deleteArray_HfittercLcLHggTwoSidedNovoPdf(void *p);
   static void destruct_HfittercLcLHggTwoSidedNovoPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggTwoSidedNovoPdf*)
   {
      ::Hfitter::HggTwoSidedNovoPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggTwoSidedNovoPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggTwoSidedNovoPdf", ::Hfitter::HggTwoSidedNovoPdf::Class_Version(), "HGGfitter/HggTwoSidedNovoPdf.h", 11,
                  typeid(::Hfitter::HggTwoSidedNovoPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggTwoSidedNovoPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggTwoSidedNovoPdf) );
      instance.SetNew(&new_HfittercLcLHggTwoSidedNovoPdf);
      instance.SetNewArray(&newArray_HfittercLcLHggTwoSidedNovoPdf);
      instance.SetDelete(&delete_HfittercLcLHggTwoSidedNovoPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggTwoSidedNovoPdf);
      instance.SetDestructor(&destruct_HfittercLcLHggTwoSidedNovoPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggTwoSidedNovoPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggTwoSidedNovoPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggNovoCBPdf(void *p = 0);
   static void *newArray_HfittercLcLHggNovoCBPdf(Long_t size, void *p);
   static void delete_HfittercLcLHggNovoCBPdf(void *p);
   static void deleteArray_HfittercLcLHggNovoCBPdf(void *p);
   static void destruct_HfittercLcLHggNovoCBPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggNovoCBPdf*)
   {
      ::Hfitter::HggNovoCBPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggNovoCBPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggNovoCBPdf", ::Hfitter::HggNovoCBPdf::Class_Version(), "HGGfitter/HggNovoCBPdf.h", 11,
                  typeid(::Hfitter::HggNovoCBPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggNovoCBPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggNovoCBPdf) );
      instance.SetNew(&new_HfittercLcLHggNovoCBPdf);
      instance.SetNewArray(&newArray_HfittercLcLHggNovoCBPdf);
      instance.SetDelete(&delete_HfittercLcLHggNovoCBPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggNovoCBPdf);
      instance.SetDestructor(&destruct_HfittercLcLHggNovoCBPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggNovoCBPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggNovoCBPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggNovoCBPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBifurCBPdf(void *p = 0);
   static void *newArray_HfittercLcLHggBifurCBPdf(Long_t size, void *p);
   static void delete_HfittercLcLHggBifurCBPdf(void *p);
   static void deleteArray_HfittercLcLHggBifurCBPdf(void *p);
   static void destruct_HfittercLcLHggBifurCBPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBifurCBPdf*)
   {
      ::Hfitter::HggBifurCBPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBifurCBPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBifurCBPdf", ::Hfitter::HggBifurCBPdf::Class_Version(), "HGGfitter/HggBifurCBPdf.h", 11,
                  typeid(::Hfitter::HggBifurCBPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBifurCBPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBifurCBPdf) );
      instance.SetNew(&new_HfittercLcLHggBifurCBPdf);
      instance.SetNewArray(&newArray_HfittercLcLHggBifurCBPdf);
      instance.SetDelete(&delete_HfittercLcLHggBifurCBPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBifurCBPdf);
      instance.SetDestructor(&destruct_HfittercLcLHggBifurCBPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBifurCBPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBifurCBPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBifurCBPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggStitchedPlateauPdf(void *p = 0);
   static void *newArray_HfittercLcLHggStitchedPlateauPdf(Long_t size, void *p);
   static void delete_HfittercLcLHggStitchedPlateauPdf(void *p);
   static void deleteArray_HfittercLcLHggStitchedPlateauPdf(void *p);
   static void destruct_HfittercLcLHggStitchedPlateauPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggStitchedPlateauPdf*)
   {
      ::Hfitter::HggStitchedPlateauPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggStitchedPlateauPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggStitchedPlateauPdf", ::Hfitter::HggStitchedPlateauPdf::Class_Version(), "HGGfitter/HggStitchedPlateauPdf.h", 12,
                  typeid(::Hfitter::HggStitchedPlateauPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggStitchedPlateauPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggStitchedPlateauPdf) );
      instance.SetNew(&new_HfittercLcLHggStitchedPlateauPdf);
      instance.SetNewArray(&newArray_HfittercLcLHggStitchedPlateauPdf);
      instance.SetDelete(&delete_HfittercLcLHggStitchedPlateauPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggStitchedPlateauPdf);
      instance.SetDestructor(&destruct_HfittercLcLHggStitchedPlateauPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggStitchedPlateauPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggStitchedPlateauPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggStitchedPlateauPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBernstein(void *p = 0);
   static void *newArray_HfittercLcLHggBernstein(Long_t size, void *p);
   static void delete_HfittercLcLHggBernstein(void *p);
   static void deleteArray_HfittercLcLHggBernstein(void *p);
   static void destruct_HfittercLcLHggBernstein(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBernstein*)
   {
      ::Hfitter::HggBernstein *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBernstein >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBernstein", ::Hfitter::HggBernstein::Class_Version(), "HGGfitter/HggBernstein.h", 25,
                  typeid(::Hfitter::HggBernstein), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBernstein::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBernstein) );
      instance.SetNew(&new_HfittercLcLHggBernstein);
      instance.SetNewArray(&newArray_HfittercLcLHggBernstein);
      instance.SetDelete(&delete_HfittercLcLHggBernstein);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBernstein);
      instance.SetDestructor(&destruct_HfittercLcLHggBernstein);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBernstein*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBernstein*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBernstein*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggExpPowerPdf(void *p = 0);
   static void *newArray_HfittercLcLHggExpPowerPdf(Long_t size, void *p);
   static void delete_HfittercLcLHggExpPowerPdf(void *p);
   static void deleteArray_HfittercLcLHggExpPowerPdf(void *p);
   static void destruct_HfittercLcLHggExpPowerPdf(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggExpPowerPdf*)
   {
      ::Hfitter::HggExpPowerPdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggExpPowerPdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggExpPowerPdf", ::Hfitter::HggExpPowerPdf::Class_Version(), "HGGfitter/HggExpPowerPdf.h", 14,
                  typeid(::Hfitter::HggExpPowerPdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggExpPowerPdf::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggExpPowerPdf) );
      instance.SetNew(&new_HfittercLcLHggExpPowerPdf);
      instance.SetNewArray(&newArray_HfittercLcLHggExpPowerPdf);
      instance.SetDelete(&delete_HfittercLcLHggExpPowerPdf);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggExpPowerPdf);
      instance.SetDestructor(&destruct_HfittercLcLHggExpPowerPdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggExpPowerPdf*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggExpPowerPdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggExpPowerPdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggPowerLaw(void *p = 0);
   static void *newArray_HfittercLcLHggPowerLaw(Long_t size, void *p);
   static void delete_HfittercLcLHggPowerLaw(void *p);
   static void deleteArray_HfittercLcLHggPowerLaw(void *p);
   static void destruct_HfittercLcLHggPowerLaw(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggPowerLaw*)
   {
      ::Hfitter::HggPowerLaw *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggPowerLaw >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggPowerLaw", ::Hfitter::HggPowerLaw::Class_Version(), "HGGfitter/HggPowerLaw.h", 14,
                  typeid(::Hfitter::HggPowerLaw), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggPowerLaw::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggPowerLaw) );
      instance.SetNew(&new_HfittercLcLHggPowerLaw);
      instance.SetNewArray(&newArray_HfittercLcLHggPowerLaw);
      instance.SetDelete(&delete_HfittercLcLHggPowerLaw);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggPowerLaw);
      instance.SetDestructor(&destruct_HfittercLcLHggPowerLaw);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggPowerLaw*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggPowerLaw*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggPowerLaw*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggExpPoly(void *p = 0);
   static void *newArray_HfittercLcLHggExpPoly(Long_t size, void *p);
   static void delete_HfittercLcLHggExpPoly(void *p);
   static void deleteArray_HfittercLcLHggExpPoly(void *p);
   static void destruct_HfittercLcLHggExpPoly(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggExpPoly*)
   {
      ::Hfitter::HggExpPoly *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggExpPoly >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggExpPoly", ::Hfitter::HggExpPoly::Class_Version(), "HGGfitter/HggExpPoly.h", 25,
                  typeid(::Hfitter::HggExpPoly), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggExpPoly::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggExpPoly) );
      instance.SetNew(&new_HfittercLcLHggExpPoly);
      instance.SetNewArray(&newArray_HfittercLcLHggExpPoly);
      instance.SetDelete(&delete_HfittercLcLHggExpPoly);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggExpPoly);
      instance.SetDestructor(&destruct_HfittercLcLHggExpPoly);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggExpPoly*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggExpPoly*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggExpPoly*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooRelBreitWignerPwave(void *p = 0);
   static void *newArray_RooRelBreitWignerPwave(Long_t size, void *p);
   static void delete_RooRelBreitWignerPwave(void *p);
   static void deleteArray_RooRelBreitWignerPwave(void *p);
   static void destruct_RooRelBreitWignerPwave(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooRelBreitWignerPwave*)
   {
      ::RooRelBreitWignerPwave *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooRelBreitWignerPwave >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooRelBreitWignerPwave", ::RooRelBreitWignerPwave::Class_Version(), "HGGfitter/RooRelBreitWignerPwave.h", 9,
                  typeid(::RooRelBreitWignerPwave), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooRelBreitWignerPwave::Dictionary, isa_proxy, 4,
                  sizeof(::RooRelBreitWignerPwave) );
      instance.SetNew(&new_RooRelBreitWignerPwave);
      instance.SetNewArray(&newArray_RooRelBreitWignerPwave);
      instance.SetDelete(&delete_RooRelBreitWignerPwave);
      instance.SetDeleteArray(&deleteArray_RooRelBreitWignerPwave);
      instance.SetDestructor(&destruct_RooRelBreitWignerPwave);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooRelBreitWignerPwave*)
   {
      return GenerateInitInstanceLocal((::RooRelBreitWignerPwave*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooRelBreitWignerPwave_LS(void *p = 0);
   static void *newArray_RooRelBreitWignerPwave_LS(Long_t size, void *p);
   static void delete_RooRelBreitWignerPwave_LS(void *p);
   static void deleteArray_RooRelBreitWignerPwave_LS(void *p);
   static void destruct_RooRelBreitWignerPwave_LS(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooRelBreitWignerPwave_LS*)
   {
      ::RooRelBreitWignerPwave_LS *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooRelBreitWignerPwave_LS >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooRelBreitWignerPwave_LS", ::RooRelBreitWignerPwave_LS::Class_Version(), "HGGfitter/RooRelBreitWignerPwave_LS.h", 15,
                  typeid(::RooRelBreitWignerPwave_LS), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooRelBreitWignerPwave_LS::Dictionary, isa_proxy, 4,
                  sizeof(::RooRelBreitWignerPwave_LS) );
      instance.SetNew(&new_RooRelBreitWignerPwave_LS);
      instance.SetNewArray(&newArray_RooRelBreitWignerPwave_LS);
      instance.SetDelete(&delete_RooRelBreitWignerPwave_LS);
      instance.SetDeleteArray(&deleteArray_RooRelBreitWignerPwave_LS);
      instance.SetDestructor(&destruct_RooRelBreitWignerPwave_LS);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooRelBreitWignerPwave_LS*)
   {
      return GenerateInitInstanceLocal((::RooRelBreitWignerPwave_LS*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LS*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_RooRelBreitWignerPwave_LSCut(void *p = 0);
   static void *newArray_RooRelBreitWignerPwave_LSCut(Long_t size, void *p);
   static void delete_RooRelBreitWignerPwave_LSCut(void *p);
   static void deleteArray_RooRelBreitWignerPwave_LSCut(void *p);
   static void destruct_RooRelBreitWignerPwave_LSCut(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::RooRelBreitWignerPwave_LSCut*)
   {
      ::RooRelBreitWignerPwave_LSCut *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::RooRelBreitWignerPwave_LSCut >(0);
      static ::ROOT::TGenericClassInfo 
         instance("RooRelBreitWignerPwave_LSCut", ::RooRelBreitWignerPwave_LSCut::Class_Version(), "HGGfitter/RooRelBreitWignerPwave_LSCut.h", 15,
                  typeid(::RooRelBreitWignerPwave_LSCut), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::RooRelBreitWignerPwave_LSCut::Dictionary, isa_proxy, 4,
                  sizeof(::RooRelBreitWignerPwave_LSCut) );
      instance.SetNew(&new_RooRelBreitWignerPwave_LSCut);
      instance.SetNewArray(&newArray_RooRelBreitWignerPwave_LSCut);
      instance.SetDelete(&delete_RooRelBreitWignerPwave_LSCut);
      instance.SetDeleteArray(&deleteArray_RooRelBreitWignerPwave_LSCut);
      instance.SetDestructor(&destruct_RooRelBreitWignerPwave_LSCut);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::RooRelBreitWignerPwave_LSCut*)
   {
      return GenerateInitInstanceLocal((::RooRelBreitWignerPwave_LSCut*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LSCut*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HggMG5TLS(void *p = 0);
   static void *newArray_HggMG5TLS(Long_t size, void *p);
   static void delete_HggMG5TLS(void *p);
   static void deleteArray_HggMG5TLS(void *p);
   static void destruct_HggMG5TLS(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HggMG5TLS*)
   {
      ::HggMG5TLS *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HggMG5TLS >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HggMG5TLS", ::HggMG5TLS::Class_Version(), "HGGfitter/HggMG5TLS.h", 15,
                  typeid(::HggMG5TLS), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HggMG5TLS::Dictionary, isa_proxy, 4,
                  sizeof(::HggMG5TLS) );
      instance.SetNew(&new_HggMG5TLS);
      instance.SetNewArray(&newArray_HggMG5TLS);
      instance.SetDelete(&delete_HggMG5TLS);
      instance.SetDeleteArray(&deleteArray_HggMG5TLS);
      instance.SetDestructor(&destruct_HggMG5TLS);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HggMG5TLS*)
   {
      return GenerateInitInstanceLocal((::HggMG5TLS*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HggMG5TLS*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggPdfBuilder*)
   {
      ::Hfitter::HggSigMggPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggPdfBuilder", ::Hfitter::HggSigMggPdfBuilder::Class_Version(), "HGGfitter/HggSigMggPdfBuilder.h", 13,
                  typeid(::Hfitter::HggSigMggPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggPdfBuilder*)
   {
      ::Hfitter::HggBkgMggPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggPdfBuilder", ::Hfitter::HggBkgMggPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggPdfBuilder.h", 13,
                  typeid(::Hfitter::HggBkgMggPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggDoubleExpPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)
   {
      ::Hfitter::HggBkgMggDoubleExpPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggDoubleExpPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggDoubleExpPdfBuilder", ::Hfitter::HggBkgMggDoubleExpPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggDoubleExpPdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggDoubleExpPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggDoubleExpPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggDoubleExpPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggDoubleExpPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggDoubleExpPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggDoubleExpPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggDoubleExpPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggDoubleExpPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggDoubleExpPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggExpPowerPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)
   {
      ::Hfitter::HggBkgMggExpPowerPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggExpPowerPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggExpPowerPdfBuilder", ::Hfitter::HggBkgMggExpPowerPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggExpPowerPdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggExpPowerPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggExpPowerPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggExpPowerPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggExpPowerPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggExpPowerPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggExpPowerPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggExpPowerPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggExpPowerPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggExpPowerPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggBiasPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggBiasPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggBiasPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggBiasPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggBiasPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggBiasPdfBuilder*)
   {
      ::Hfitter::HggBkgMggBiasPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggBiasPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggBiasPdfBuilder", ::Hfitter::HggBkgMggBiasPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggBiasPdfBuilder.h", 13,
                  typeid(::Hfitter::HggBkgMggBiasPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggBiasPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggBiasPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggBiasPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggBiasPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggBiasPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggBiasPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggBiasPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggBiasPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggBiasPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBiasPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggPolyPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggPolyPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggPolyPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggPolyPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggPolyPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggPolyPdfBuilder*)
   {
      ::Hfitter::HggBkgMggPolyPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggPolyPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggPolyPdfBuilder", ::Hfitter::HggBkgMggPolyPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggPolyPdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggPolyPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggPolyPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggPolyPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggPolyPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggPolyPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggPolyPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggPolyPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggPolyPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggPolyPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggPolyPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPolyPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBernsteinPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBernsteinPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBernsteinPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBernsteinPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBernsteinPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBernsteinPdfBuilder*)
   {
      ::Hfitter::HggBernsteinPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBernsteinPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBernsteinPdfBuilder", ::Hfitter::HggBernsteinPdfBuilder::Class_Version(), "HGGfitter/HggBernsteinPdfBuilder.h", 15,
                  typeid(::Hfitter::HggBernsteinPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBernsteinPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBernsteinPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBernsteinPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBernsteinPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBernsteinPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBernsteinPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBernsteinPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBernsteinPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBernsteinPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggChebyshevPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggChebyshevPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggChebyshevPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggChebyshevPdfBuilder(void *p);
   static void destruct_HfittercLcLHggChebyshevPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggChebyshevPdfBuilder*)
   {
      ::Hfitter::HggChebyshevPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggChebyshevPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggChebyshevPdfBuilder", ::Hfitter::HggChebyshevPdfBuilder::Class_Version(), "HGGfitter/HggChebyshevPdfBuilder.h", 15,
                  typeid(::Hfitter::HggChebyshevPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggChebyshevPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggChebyshevPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggChebyshevPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggChebyshevPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggChebyshevPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggChebyshevPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggChebyshevPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggChebyshevPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggChebyshevPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggChebyshevPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggBernstein4PdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)
   {
      ::Hfitter::HggBkgMggBernstein4PdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggBernstein4PdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggBernstein4PdfBuilder", ::Hfitter::HggBkgMggBernstein4PdfBuilder::Class_Version(), "HGGfitter/HggBkgMggBernstein4PdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggBernstein4PdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggBernstein4PdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggBernstein4PdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggBernstein4PdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggBernstein4PdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggBernstein4PdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggBernstein4PdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggBernstein4PdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggBernstein4PdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggBernstein5PdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)
   {
      ::Hfitter::HggBkgMggBernstein5PdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggBernstein5PdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggBernstein5PdfBuilder", ::Hfitter::HggBkgMggBernstein5PdfBuilder::Class_Version(), "HGGfitter/HggBkgMggBernstein5PdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggBernstein5PdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggBernstein5PdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggBernstein5PdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggBernstein5PdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggBernstein5PdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggBernstein5PdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggBernstein5PdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggBernstein5PdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggBernstein5PdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggLaurent4PdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)
   {
      ::Hfitter::HggBkgMggLaurent4PdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggLaurent4PdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggLaurent4PdfBuilder", ::Hfitter::HggBkgMggLaurent4PdfBuilder::Class_Version(), "HGGfitter/HggBkgMggLaurent4PdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggLaurent4PdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggLaurent4PdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggLaurent4PdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggLaurent4PdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggLaurent4PdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggLaurent4PdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggLaurent4PdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggLaurent4PdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggLaurent4PdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggPower2PdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggPower2PdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggPower2PdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggPower2PdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggPower2PdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggPower2PdfBuilder*)
   {
      ::Hfitter::HggBkgMggPower2PdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggPower2PdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggPower2PdfBuilder", ::Hfitter::HggBkgMggPower2PdfBuilder::Class_Version(), "HGGfitter/HggBkgMggPower2PdfBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggPower2PdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggPower2PdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggPower2PdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggPower2PdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggPower2PdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggPower2PdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggPower2PdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggPower2PdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggPower2PdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggPower2PdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPower2PdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggExpPolyPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggExpPolyPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggExpPolyPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggExpPolyPdfBuilder(void *p);
   static void destruct_HfittercLcLHggExpPolyPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggExpPolyPdfBuilder*)
   {
      ::Hfitter::HggExpPolyPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggExpPolyPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggExpPolyPdfBuilder", ::Hfitter::HggExpPolyPdfBuilder::Class_Version(), "HGGfitter/HggExpPolyPdfBuilder.h", 15,
                  typeid(::Hfitter::HggExpPolyPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggExpPolyPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggExpPolyPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggExpPolyPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggExpPolyPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggExpPolyPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggExpPolyPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggExpPolyPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggExpPolyPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggExpPolyPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggExpPolyPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggPolyPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggPolyPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggPolyPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggPolyPdfBuilder(void *p);
   static void destruct_HfittercLcLHggPolyPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggPolyPdfBuilder*)
   {
      ::Hfitter::HggPolyPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggPolyPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggPolyPdfBuilder", ::Hfitter::HggPolyPdfBuilder::Class_Version(), "HGGfitter/HggPolyPdfBuilder.h", 15,
                  typeid(::Hfitter::HggPolyPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggPolyPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggPolyPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggPolyPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggPolyPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggPolyPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggPolyPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggPolyPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggPolyPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggPolyPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggPolyPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggGenericBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggGenericBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggGenericBuilder(void *p);
   static void deleteArray_HfittercLcLHggGenericBuilder(void *p);
   static void destruct_HfittercLcLHggGenericBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggGenericBuilder*)
   {
      ::Hfitter::HggGenericBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggGenericBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggGenericBuilder", ::Hfitter::HggGenericBuilder::Class_Version(), "HGGfitter/HggGenericBuilder.h", 15,
                  typeid(::Hfitter::HggGenericBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggGenericBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggGenericBuilder) );
      instance.SetNew(&new_HfittercLcLHggGenericBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggGenericBuilder);
      instance.SetDelete(&delete_HfittercLcLHggGenericBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggGenericBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggGenericBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggGenericBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggGenericBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggGenericBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggEliLandauExpBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggEliLandauExpBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggEliLandauExpBuilder(void *p);
   static void deleteArray_HfittercLcLHggEliLandauExpBuilder(void *p);
   static void destruct_HfittercLcLHggEliLandauExpBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggEliLandauExpBuilder*)
   {
      ::Hfitter::HggEliLandauExpBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggEliLandauExpBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggEliLandauExpBuilder", ::Hfitter::HggEliLandauExpBuilder::Class_Version(), "HGGfitter/HggEliLandauExpBuilder.h", 13,
                  typeid(::Hfitter::HggEliLandauExpBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggEliLandauExpBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggEliLandauExpBuilder) );
      instance.SetNew(&new_HfittercLcLHggEliLandauExpBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggEliLandauExpBuilder);
      instance.SetDelete(&delete_HfittercLcLHggEliLandauExpBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggEliLandauExpBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggEliLandauExpBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggEliLandauExpBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggEliLandauExpBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggEliLandauExpBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigCosThStarPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigCosThStarPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigCosThStarPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigCosThStarPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigCosThStarPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigCosThStarPdfBuilder*)
   {
      ::Hfitter::HggSigCosThStarPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigCosThStarPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigCosThStarPdfBuilder", ::Hfitter::HggSigCosThStarPdfBuilder::Class_Version(), "HGGfitter/HggSigCosThStarPdfBuilder.h", 13,
                  typeid(::Hfitter::HggSigCosThStarPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigCosThStarPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigCosThStarPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigCosThStarPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigCosThStarPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigCosThStarPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigCosThStarPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigCosThStarPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigCosThStarPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigCosThStarPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigCosThStarPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgCosThStarPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgCosThStarPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgCosThStarPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgCosThStarPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgCosThStarPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgCosThStarPdfBuilder*)
   {
      ::Hfitter::HggBkgCosThStarPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgCosThStarPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgCosThStarPdfBuilder", ::Hfitter::HggBkgCosThStarPdfBuilder::Class_Version(), "HGGfitter/HggBkgCosThStarPdfBuilder.h", 13,
                  typeid(::Hfitter::HggBkgCosThStarPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgCosThStarPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgCosThStarPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgCosThStarPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgCosThStarPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgCosThStarPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgCosThStarPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgCosThStarPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgCosThStarPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgCosThStarPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgCosThStarPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigPTPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigPTPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigPTPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigPTPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigPTPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigPTPdfBuilder*)
   {
      ::Hfitter::HggSigPTPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigPTPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigPTPdfBuilder", ::Hfitter::HggSigPTPdfBuilder::Class_Version(), "HGGfitter/HggSigPTPdfBuilder.h", 13,
                  typeid(::Hfitter::HggSigPTPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigPTPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigPTPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigPTPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigPTPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigPTPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigPTPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigPTPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigPTPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigPTPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigPTPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgPTPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgPTPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgPTPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgPTPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgPTPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgPTPdfBuilder*)
   {
      ::Hfitter::HggBkgPTPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgPTPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgPTPdfBuilder", ::Hfitter::HggBkgPTPdfBuilder::Class_Version(), "HGGfitter/HggBkgPTPdfBuilder.h", 13,
                  typeid(::Hfitter::HggBkgPTPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgPTPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgPTPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgPTPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgPTPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgPTPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgPTPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgPTPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgPTPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgPTPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgPTPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggCosThStarPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggCosThStarPdfBuilder*)
   {
      ::Hfitter::HggSigMggCosThStarPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggCosThStarPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggCosThStarPdfBuilder", ::Hfitter::HggSigMggCosThStarPdfBuilder::Class_Version(), "HGGfitter/HggSigMggCosThStarPdfBuilder.h", 16,
                  typeid(::Hfitter::HggSigMggCosThStarPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggCosThStarPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggCosThStarPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggCosThStarPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggCosThStarPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggCosThStarPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggCosThStarPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggCosThStarPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggCosThStarPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggCosThStarPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggCosThStarPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)
   {
      ::Hfitter::HggBkgMggCosThStarPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggCosThStarPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggCosThStarPdfBuilder", ::Hfitter::HggBkgMggCosThStarPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggCosThStarPdfBuilder.h", 16,
                  typeid(::Hfitter::HggBkgMggCosThStarPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggCosThStarPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggCosThStarPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggCosThStarPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggCosThStarPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggCosThStarPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggCosThStarPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggCosThStarPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggCosThStarPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)
   {
      ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggCosThStarKeysPdfBuilder", ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h", 14,
                  typeid(::Hfitter::HggBkgMggCosThStarKeysPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggCosThStarKeysPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggCosThStarKeysPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggPTKeysPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)
   {
      ::Hfitter::HggBkgMggPTKeysPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggPTKeysPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggPTKeysPdfBuilder", ::Hfitter::HggBkgMggPTKeysPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggPTKeysPdfBuilder.h", 14,
                  typeid(::Hfitter::HggBkgMggPTKeysPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggPTKeysPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggPTKeysPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggPTKeysPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggPTKeysPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggPTKeysPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggPTKeysPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggPTKeysPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggPTKeysPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggPTPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggPTPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggPTPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggPTPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggPTPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggPTPdfBuilder*)
   {
      ::Hfitter::HggSigMggPTPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggPTPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggPTPdfBuilder", ::Hfitter::HggSigMggPTPdfBuilder::Class_Version(), "HGGfitter/HggSigMggPTPdfBuilder.h", 15,
                  typeid(::Hfitter::HggSigMggPTPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggPTPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggPTPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggPTPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggPTPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggPTPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggPTPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggPTPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggPTPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggPTPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPTPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggPTPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggPTPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggPTPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggPTPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggPTPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggPTPdfBuilder*)
   {
      ::Hfitter::HggBkgMggPTPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggPTPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggPTPdfBuilder", ::Hfitter::HggBkgMggPTPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggPTPdfBuilder.h", 15,
                  typeid(::Hfitter::HggBkgMggPTPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggPTPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggPTPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggPTPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggPTPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggPTPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggPTPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggPTPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggPTPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggPTPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggCosThStarPTPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)
   {
      ::Hfitter::HggSigMggCosThStarPTPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggCosThStarPTPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggCosThStarPTPdfBuilder", ::Hfitter::HggSigMggCosThStarPTPdfBuilder::Class_Version(), "HGGfitter/HggSigMggCosThStarPTPdfBuilder.h", 17,
                  typeid(::Hfitter::HggSigMggCosThStarPTPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggCosThStarPTPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggCosThStarPTPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggCosThStarPTPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggCosThStarPTPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggCosThStarPTPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggCosThStarPTPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggCosThStarPTPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggCosThStarPTPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)
   {
      ::Hfitter::HggBkgMggCosThStarPTPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggCosThStarPTPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggCosThStarPTPdfBuilder", ::Hfitter::HggBkgMggCosThStarPTPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h", 17,
                  typeid(::Hfitter::HggBkgMggCosThStarPTPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggCosThStarPTPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggCosThStarPTPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggCosThStarPTPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggCosThStarPTPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggCosThStarPTPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggCosThStarPTPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggCosThStarPTPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggSmearMPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggSmearMPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggSmearMPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggSmearMPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggSmearMPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggSmearMPdfBuilder*)
   {
      ::Hfitter::HggSigMggSmearMPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggSmearMPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggSmearMPdfBuilder", ::Hfitter::HggSigMggSmearMPdfBuilder::Class_Version(), "HGGfitter/HggSigMggSmearMPdfBuilder.h", 9,
                  typeid(::Hfitter::HggSigMggSmearMPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggSmearMPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggSmearMPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggSmearMPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggSmearMPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggSmearMPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggSmearMPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggSmearMPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggSmearMPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggSmearMPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggSmearMPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggSmearMPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggSmearMPdfBuilder*)
   {
      ::Hfitter::HggBkgMggSmearMPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggSmearMPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggSmearMPdfBuilder", ::Hfitter::HggBkgMggSmearMPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggSmearMPdfBuilder.h", 14,
                  typeid(::Hfitter::HggBkgMggSmearMPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggSmearMPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggSmearMPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggSmearMPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggSmearMPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggSmearMPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggSmearMPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggSmearMPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggSmearMPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggSmearMPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggSmearMPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggExpPolySmearMPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggExpPolySmearMPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggExpPolySmearMPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggExpPolySmearMPdfBuilder(void *p);
   static void destruct_HfittercLcLHggExpPolySmearMPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggExpPolySmearMPdfBuilder*)
   {
      ::Hfitter::HggExpPolySmearMPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggExpPolySmearMPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggExpPolySmearMPdfBuilder", ::Hfitter::HggExpPolySmearMPdfBuilder::Class_Version(), "HGGfitter/HggExpPolySmearMPdfBuilder.h", 16,
                  typeid(::Hfitter::HggExpPolySmearMPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggExpPolySmearMPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggExpPolySmearMPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggExpPolySmearMPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggExpPolySmearMPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggExpPolySmearMPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggExpPolySmearMPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggExpPolySmearMPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggExpPolySmearMPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggExpPolySmearMPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggExpPolySmearMPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBernsteinSmearMPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBernsteinSmearMPdfBuilder*)
   {
      ::Hfitter::HggBernsteinSmearMPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBernsteinSmearMPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBernsteinSmearMPdfBuilder", ::Hfitter::HggBernsteinSmearMPdfBuilder::Class_Version(), "HGGfitter/HggBernsteinSmearMPdfBuilder.h", 16,
                  typeid(::Hfitter::HggBernsteinSmearMPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBernsteinSmearMPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBernsteinSmearMPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBernsteinSmearMPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBernsteinSmearMPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBernsteinSmearMPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBernsteinSmearMPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBernsteinSmearMPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBernsteinSmearMPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBernsteinSmearMPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinSmearMPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggIsolPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggIsolPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggIsolPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggIsolPdfBuilder(void *p);
   static void destruct_HfittercLcLHggIsolPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggIsolPdfBuilder*)
   {
      ::Hfitter::HggIsolPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggIsolPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggIsolPdfBuilder", ::Hfitter::HggIsolPdfBuilder::Class_Version(), "HGGfitter/HggIsolPdfBuilder.h", 14,
                  typeid(::Hfitter::HggIsolPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggIsolPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggIsolPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggIsolPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggIsolPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggIsolPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggIsolPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggIsolPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggIsolPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggIsolPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggIsolPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggIsolPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggIsolPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggIsolPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggIsolPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggIsolPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggIsolPdfBuilder*)
   {
      ::Hfitter::HggSigMggIsolPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggIsolPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggIsolPdfBuilder", ::Hfitter::HggSigMggIsolPdfBuilder::Class_Version(), "HGGfitter/HggSigMggIsolPdfBuilder.h", 11,
                  typeid(::Hfitter::HggSigMggIsolPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggIsolPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggIsolPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggIsolPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggIsolPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggIsolPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggIsolPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggIsolPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggIsolPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggIsolPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggIsolHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggIsolHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggIsolHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggIsolHistoBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggIsolHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggIsolHistoBuilder*)
   {
      ::Hfitter::HggSigMggIsolHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggIsolHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggIsolHistoBuilder", ::Hfitter::HggSigMggIsolHistoBuilder::Class_Version(), "HGGfitter/HggSigMggIsolHistoBuilder.h", 10,
                  typeid(::Hfitter::HggSigMggIsolHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggIsolHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggIsolHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggIsolHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggIsolHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggIsolHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggIsolHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggIsolHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggIsolHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggIsolHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsolPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsolPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsolPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsolPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsolPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsolPdfBuilder*)
   {
      ::Hfitter::HggBkgMggIsolPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsolPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsolPdfBuilder", ::Hfitter::HggBkgMggIsolPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggIsolPdfBuilder.h", 11,
                  typeid(::Hfitter::HggBkgMggIsolPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsolPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsolPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsolPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsolPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsolPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsolPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsolPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsolPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsolPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsolHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsolHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsolHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsolHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsolHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsolHistoBuilder*)
   {
      ::Hfitter::HggBkgMggIsolHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsolHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsolHistoBuilder", ::Hfitter::HggBkgMggIsolHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggIsolHistoBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggIsolHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsolHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsolHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsolHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsolHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsolHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsolHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsolHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsolHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsolHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggDiffIsolHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)
   {
      ::Hfitter::HggBkgMggDiffIsolHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggDiffIsolHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggDiffIsolHistoBuilder", ::Hfitter::HggBkgMggDiffIsolHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggDiffIsolHistoBuilder.h", 12,
                  typeid(::Hfitter::HggBkgMggDiffIsolHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggDiffIsolHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggDiffIsolHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggDiffIsolHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggDiffIsolHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggDiffIsolHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggDiffIsolHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggDiffIsolHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggDiffIsolHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)
   {
      ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsolBernsteinHistoBuilder", ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggIsolBernsteinHistoBuilder.h", 15,
                  typeid(::Hfitter::HggBkgMggIsolBernsteinHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsolBernsteinHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsolBernsteinHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)
   {
      ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder", ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggDiffIsolBernsteinHistoBuilder.h", 15,
                  typeid(::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)
   {
      ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsolExpPolyHistoBuilder", ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggIsolExpPolyHistoBuilder.h", 16,
                  typeid(::Hfitter::HggBkgMggIsolExpPolyHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsolExpPolyHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsolExpPolyHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)
   {
      ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder", ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggDiffIsolExpPolyHistoBuilder.h", 16,
                  typeid(::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsol2DHistoBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)
   {
      ::Hfitter::HggBkgMggIsol2DHistoBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsol2DHistoBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsol2DHistoBuilder", ::Hfitter::HggBkgMggIsol2DHistoBuilder::Class_Version(), "HGGfitter/HggBkgMggIsol2DHistoBuilder.h", 14,
                  typeid(::Hfitter::HggBkgMggIsol2DHistoBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsol2DHistoBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsol2DHistoBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsol2DHistoBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsol2DHistoBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsol2DHistoBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsol2DHistoBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsol2DHistoBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsol2DHistoBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsolKeysPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)
   {
      ::Hfitter::HggBkgMggIsolKeysPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsolKeysPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsolKeysPdfBuilder", ::Hfitter::HggBkgMggIsolKeysPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggIsolKeysPdfBuilder.h", 13,
                  typeid(::Hfitter::HggBkgMggIsolKeysPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsolKeysPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsolKeysPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsolKeysPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsolKeysPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsolKeysPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsolKeysPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsolKeysPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsolKeysPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggSigMggIsolBremPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p);
   static void destruct_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggSigMggIsolBremPdfBuilder*)
   {
      ::Hfitter::HggSigMggIsolBremPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggSigMggIsolBremPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggSigMggIsolBremPdfBuilder", ::Hfitter::HggSigMggIsolBremPdfBuilder::Class_Version(), "HGGfitter/HggSigMggIsolBremPdfBuilder.h", 10,
                  typeid(::Hfitter::HggSigMggIsolBremPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggSigMggIsolBremPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggSigMggIsolBremPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggSigMggIsolBremPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggSigMggIsolBremPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggSigMggIsolBremPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggSigMggIsolBremPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggSigMggIsolBremPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggSigMggIsolBremPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggSigMggIsolBremPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolBremPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBkgMggIsolBremPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)
   {
      ::Hfitter::HggBkgMggIsolBremPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBkgMggIsolBremPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBkgMggIsolBremPdfBuilder", ::Hfitter::HggBkgMggIsolBremPdfBuilder::Class_Version(), "HGGfitter/HggBkgMggIsolBremPdfBuilder.h", 10,
                  typeid(::Hfitter::HggBkgMggIsolBremPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBkgMggIsolBremPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBkgMggIsolBremPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBkgMggIsolBremPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBkgMggIsolBremPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBkgMggIsolBremPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBkgMggIsolBremPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBkgMggIsolBremPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBkgMggIsolBremPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggZeeMassPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggZeeMassPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggZeeMassPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggZeeMassPdfBuilder(void *p);
   static void destruct_HfittercLcLHggZeeMassPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggZeeMassPdfBuilder*)
   {
      ::Hfitter::HggZeeMassPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggZeeMassPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggZeeMassPdfBuilder", ::Hfitter::HggZeeMassPdfBuilder::Class_Version(), "HGGfitter/HggZeeMassPdfBuilder.h", 10,
                  typeid(::Hfitter::HggZeeMassPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggZeeMassPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggZeeMassPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggZeeMassPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggZeeMassPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggZeeMassPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggZeeMassPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggZeeMassPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggZeeMassPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggZeeMassPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggZeeMassPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggZeeNovoMassPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggZeeNovoMassPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggZeeNovoMassPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggZeeNovoMassPdfBuilder(void *p);
   static void destruct_HfittercLcLHggZeeNovoMassPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggZeeNovoMassPdfBuilder*)
   {
      ::Hfitter::HggZeeNovoMassPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggZeeNovoMassPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggZeeNovoMassPdfBuilder", ::Hfitter::HggZeeNovoMassPdfBuilder::Class_Version(), "HGGfitter/HggZeeNovoMassPdfBuilder.h", 10,
                  typeid(::Hfitter::HggZeeNovoMassPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggZeeNovoMassPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggZeeNovoMassPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggZeeNovoMassPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggZeeNovoMassPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggZeeNovoMassPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggZeeNovoMassPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggZeeNovoMassPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggZeeNovoMassPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggZeeNovoMassPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggZeeNovoMassPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggZeeBifurMassPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggZeeBifurMassPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggZeeBifurMassPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggZeeBifurMassPdfBuilder(void *p);
   static void destruct_HfittercLcLHggZeeBifurMassPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggZeeBifurMassPdfBuilder*)
   {
      ::Hfitter::HggZeeBifurMassPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggZeeBifurMassPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggZeeBifurMassPdfBuilder", ::Hfitter::HggZeeBifurMassPdfBuilder::Class_Version(), "HGGfitter/HggZeeBifurMassPdfBuilder.h", 10,
                  typeid(::Hfitter::HggZeeBifurMassPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggZeeBifurMassPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggZeeBifurMassPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggZeeBifurMassPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggZeeBifurMassPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggZeeBifurMassPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggZeeBifurMassPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggZeeBifurMassPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggZeeBifurMassPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggZeeBifurMassPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggZeeBifurMassPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggTwoSidedNovoPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p);
   static void destruct_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggTwoSidedNovoPdfBuilder*)
   {
      ::Hfitter::HggTwoSidedNovoPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggTwoSidedNovoPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggTwoSidedNovoPdfBuilder", ::Hfitter::HggTwoSidedNovoPdfBuilder::Class_Version(), "HGGfitter/HggTwoSidedNovoPdfBuilder.h", 10,
                  typeid(::Hfitter::HggTwoSidedNovoPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggTwoSidedNovoPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggTwoSidedNovoPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggTwoSidedNovoPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggTwoSidedNovoPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggTwoSidedNovoPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggTwoSidedNovoPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggTwoSidedNovoPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggTwoSidedNovoPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggTwoSidedNovoPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHgg2sCBPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHgg2sCBPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHgg2sCBPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHgg2sCBPdfBuilder(void *p);
   static void destruct_HfittercLcLHgg2sCBPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::Hgg2sCBPdfBuilder*)
   {
      ::Hfitter::Hgg2sCBPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::Hgg2sCBPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::Hgg2sCBPdfBuilder", ::Hfitter::Hgg2sCBPdfBuilder::Class_Version(), "HGGfitter/Hgg2sCBPdfBuilder.h", 10,
                  typeid(::Hfitter::Hgg2sCBPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::Hgg2sCBPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::Hgg2sCBPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHgg2sCBPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHgg2sCBPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHgg2sCBPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHgg2sCBPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHgg2sCBPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::Hgg2sCBPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::Hgg2sCBPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::Hgg2sCBPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggBWx2sCBPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggBWx2sCBPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggBWx2sCBPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggBWx2sCBPdfBuilder(void *p);
   static void destruct_HfittercLcLHggBWx2sCBPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggBWx2sCBPdfBuilder*)
   {
      ::Hfitter::HggBWx2sCBPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggBWx2sCBPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggBWx2sCBPdfBuilder", ::Hfitter::HggBWx2sCBPdfBuilder::Class_Version(), "HGGfitter/HggBWx2sCBPdfBuilder.h", 8,
                  typeid(::Hfitter::HggBWx2sCBPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggBWx2sCBPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggBWx2sCBPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggBWx2sCBPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggBWx2sCBPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggBWx2sCBPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggBWx2sCBPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggBWx2sCBPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggBWx2sCBPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggBWx2sCBPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggBWx2sCBPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHggCouplings_Dictionary();
   static void HfittercLcLHggCouplings_TClassManip(TClass*);
   static void *new_HfittercLcLHggCouplings(void *p = 0);
   static void *newArray_HfittercLcLHggCouplings(Long_t size, void *p);
   static void delete_HfittercLcLHggCouplings(void *p);
   static void deleteArray_HfittercLcLHggCouplings(void *p);
   static void destruct_HfittercLcLHggCouplings(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggCouplings*)
   {
      ::Hfitter::HggCouplings *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HggCouplings));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggCouplings", "HGGfitter/HggCouplings.h", 6,
                  typeid(::Hfitter::HggCouplings), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHggCouplings_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggCouplings) );
      instance.SetNew(&new_HfittercLcLHggCouplings);
      instance.SetNewArray(&newArray_HfittercLcLHggCouplings);
      instance.SetDelete(&delete_HfittercLcLHggCouplings);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggCouplings);
      instance.SetDestructor(&destruct_HfittercLcLHggCouplings);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggCouplings*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggCouplings*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggCouplings*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHggCouplings_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCouplings*)0x0)->GetClass();
      HfittercLcLHggCouplings_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHggCouplings_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggCouplingsBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggCouplingsBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggCouplingsBuilder(void *p);
   static void deleteArray_HfittercLcLHggCouplingsBuilder(void *p);
   static void destruct_HfittercLcLHggCouplingsBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggCouplingsBuilder*)
   {
      ::Hfitter::HggCouplingsBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggCouplingsBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggCouplingsBuilder", ::Hfitter::HggCouplingsBuilder::Class_Version(), "HGGfitter/HggCouplingsBuilder.h", 12,
                  typeid(::Hfitter::HggCouplingsBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggCouplingsBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggCouplingsBuilder) );
      instance.SetNew(&new_HfittercLcLHggCouplingsBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggCouplingsBuilder);
      instance.SetDelete(&delete_HfittercLcLHggCouplingsBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggCouplingsBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggCouplingsBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggCouplingsBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggCouplingsBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggCouplingsBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHggTools_Dictionary();
   static void HfittercLcLHggTools_TClassManip(TClass*);
   static void *new_HfittercLcLHggTools(void *p = 0);
   static void *newArray_HfittercLcLHggTools(Long_t size, void *p);
   static void delete_HfittercLcLHggTools(void *p);
   static void deleteArray_HfittercLcLHggTools(void *p);
   static void destruct_HfittercLcLHggTools(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggTools*)
   {
      ::Hfitter::HggTools *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HggTools));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggTools", "HGGfitter/HggTools.h", 35,
                  typeid(::Hfitter::HggTools), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHggTools_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggTools) );
      instance.SetNew(&new_HfittercLcLHggTools);
      instance.SetNewArray(&newArray_HfittercLcLHggTools);
      instance.SetDelete(&delete_HfittercLcLHggTools);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggTools);
      instance.SetDestructor(&destruct_HfittercLcLHggTools);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggTools*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggTools*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggTools*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHggTools_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTools*)0x0)->GetClass();
      HfittercLcLHggTools_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHggTools_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHggEtaCatHelper_Dictionary();
   static void HfittercLcLHggEtaCatHelper_TClassManip(TClass*);
   static void delete_HfittercLcLHggEtaCatHelper(void *p);
   static void deleteArray_HfittercLcLHggEtaCatHelper(void *p);
   static void destruct_HfittercLcLHggEtaCatHelper(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggEtaCatHelper*)
   {
      ::Hfitter::HggEtaCatHelper *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HggEtaCatHelper));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggEtaCatHelper", "HGGfitter/HggEtaCatHelper.h", 11,
                  typeid(::Hfitter::HggEtaCatHelper), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHggEtaCatHelper_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggEtaCatHelper) );
      instance.SetDelete(&delete_HfittercLcLHggEtaCatHelper);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggEtaCatHelper);
      instance.SetDestructor(&destruct_HfittercLcLHggEtaCatHelper);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggEtaCatHelper*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggEtaCatHelper*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggEtaCatHelper*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHggEtaCatHelper_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggEtaCatHelper*)0x0)->GetClass();
      HfittercLcLHggEtaCatHelper_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHggEtaCatHelper_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHggResolHelper_Dictionary();
   static void HfittercLcLHggResolHelper_TClassManip(TClass*);
   static void delete_HfittercLcLHggResolHelper(void *p);
   static void deleteArray_HfittercLcLHggResolHelper(void *p);
   static void destruct_HfittercLcLHggResolHelper(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggResolHelper*)
   {
      ::Hfitter::HggResolHelper *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HggResolHelper));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggResolHelper", "HGGfitter/HggResolHelper.h", 11,
                  typeid(::Hfitter::HggResolHelper), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHggResolHelper_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggResolHelper) );
      instance.SetDelete(&delete_HfittercLcLHggResolHelper);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggResolHelper);
      instance.SetDestructor(&destruct_HfittercLcLHggResolHelper);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggResolHelper*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggResolHelper*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggResolHelper*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHggResolHelper_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggResolHelper*)0x0)->GetClass();
      HfittercLcLHggResolHelper_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHggResolHelper_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHggZeeIsolStudy_Dictionary();
   static void HfittercLcLHggZeeIsolStudy_TClassManip(TClass*);
   static void *new_HfittercLcLHggZeeIsolStudy(void *p = 0);
   static void *newArray_HfittercLcLHggZeeIsolStudy(Long_t size, void *p);
   static void delete_HfittercLcLHggZeeIsolStudy(void *p);
   static void deleteArray_HfittercLcLHggZeeIsolStudy(void *p);
   static void destruct_HfittercLcLHggZeeIsolStudy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggZeeIsolStudy*)
   {
      ::Hfitter::HggZeeIsolStudy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HggZeeIsolStudy));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggZeeIsolStudy", "HGGfitter/HggZeeIsolStudy.h", 25,
                  typeid(::Hfitter::HggZeeIsolStudy), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHggZeeIsolStudy_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggZeeIsolStudy) );
      instance.SetNew(&new_HfittercLcLHggZeeIsolStudy);
      instance.SetNewArray(&newArray_HfittercLcLHggZeeIsolStudy);
      instance.SetDelete(&delete_HfittercLcLHggZeeIsolStudy);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggZeeIsolStudy);
      instance.SetDestructor(&destruct_HfittercLcLHggZeeIsolStudy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggZeeIsolStudy*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggZeeIsolStudy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggZeeIsolStudy*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHggZeeIsolStudy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeIsolStudy*)0x0)->GetClass();
      HfittercLcLHggZeeIsolStudy_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHggZeeIsolStudy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static TClass *HfittercLcLHggNewResonanceStudy_Dictionary();
   static void HfittercLcLHggNewResonanceStudy_TClassManip(TClass*);
   static void *new_HfittercLcLHggNewResonanceStudy(void *p = 0);
   static void *newArray_HfittercLcLHggNewResonanceStudy(Long_t size, void *p);
   static void delete_HfittercLcLHggNewResonanceStudy(void *p);
   static void deleteArray_HfittercLcLHggNewResonanceStudy(void *p);
   static void destruct_HfittercLcLHggNewResonanceStudy(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggNewResonanceStudy*)
   {
      ::Hfitter::HggNewResonanceStudy *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(::Hfitter::HggNewResonanceStudy));
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggNewResonanceStudy", "HGGfitter/HggNewResonanceStudy.h", 29,
                  typeid(::Hfitter::HggNewResonanceStudy), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &HfittercLcLHggNewResonanceStudy_Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggNewResonanceStudy) );
      instance.SetNew(&new_HfittercLcLHggNewResonanceStudy);
      instance.SetNewArray(&newArray_HfittercLcLHggNewResonanceStudy);
      instance.SetDelete(&delete_HfittercLcLHggNewResonanceStudy);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggNewResonanceStudy);
      instance.SetDestructor(&destruct_HfittercLcLHggNewResonanceStudy);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggNewResonanceStudy*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggNewResonanceStudy*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggNewResonanceStudy*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *HfittercLcLHggNewResonanceStudy_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggNewResonanceStudy*)0x0)->GetClass();
      HfittercLcLHggNewResonanceStudy_TClassManip(theClass);
   return theClass;
   }

   static void HfittercLcLHggNewResonanceStudy_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   static void *new_HggNLLVar(void *p = 0);
   static void *newArray_HggNLLVar(Long_t size, void *p);
   static void delete_HggNLLVar(void *p);
   static void deleteArray_HggNLLVar(void *p);
   static void destruct_HggNLLVar(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HggNLLVar*)
   {
      ::HggNLLVar *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HggNLLVar >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HggNLLVar", ::HggNLLVar::Class_Version(), "HGGfitter/HggNLLVar.h", 23,
                  typeid(::HggNLLVar), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HggNLLVar::Dictionary, isa_proxy, 4,
                  sizeof(::HggNLLVar) );
      instance.SetNew(&new_HggNLLVar);
      instance.SetNewArray(&newArray_HggNLLVar);
      instance.SetDelete(&delete_HggNLLVar);
      instance.SetDeleteArray(&deleteArray_HggNLLVar);
      instance.SetDestructor(&destruct_HggNLLVar);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HggNLLVar*)
   {
      return GenerateInitInstanceLocal((::HggNLLVar*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HggNLLVar*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggPowHegLSPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggPowHegLSPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggPowHegLSPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggPowHegLSPdfBuilder(void *p);
   static void destruct_HfittercLcLHggPowHegLSPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggPowHegLSPdfBuilder*)
   {
      ::Hfitter::HggPowHegLSPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggPowHegLSPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggPowHegLSPdfBuilder", ::Hfitter::HggPowHegLSPdfBuilder::Class_Version(), "HGGfitter/HggPowHegLSPdfBuilder.h", 9,
                  typeid(::Hfitter::HggPowHegLSPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggPowHegLSPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggPowHegLSPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggPowHegLSPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggPowHegLSPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggPowHegLSPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggPowHegLSPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggPowHegLSPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggPowHegLSPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggPowHegLSPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggPowHegLSPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggMG5LSPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggMG5LSPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggMG5LSPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggMG5LSPdfBuilder(void *p);
   static void destruct_HfittercLcLHggMG5LSPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggMG5LSPdfBuilder*)
   {
      ::Hfitter::HggMG5LSPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggMG5LSPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggMG5LSPdfBuilder", ::Hfitter::HggMG5LSPdfBuilder::Class_Version(), "HGGfitter/HggMG5LSPdfBuilder.h", 9,
                  typeid(::Hfitter::HggMG5LSPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggMG5LSPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggMG5LSPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggMG5LSPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggMG5LSPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggMG5LSPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggMG5LSPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggMG5LSPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggMG5LSPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggMG5LSPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggMG5LSPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HggGravitonLineShapePdf(void *p = 0);
   static void *newArray_HggGravitonLineShapePdf(Long_t size, void *p);
   static void delete_HggGravitonLineShapePdf(void *p);
   static void deleteArray_HggGravitonLineShapePdf(void *p);
   static void destruct_HggGravitonLineShapePdf(void *p);
   static void streamer_HggGravitonLineShapePdf(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HggGravitonLineShapePdf*)
   {
      ::HggGravitonLineShapePdf *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HggGravitonLineShapePdf >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HggGravitonLineShapePdf", ::HggGravitonLineShapePdf::Class_Version(), "HGGfitter/HggGravitonLineShapePdf.hh", 21,
                  typeid(::HggGravitonLineShapePdf), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HggGravitonLineShapePdf::Dictionary, isa_proxy, 16,
                  sizeof(::HggGravitonLineShapePdf) );
      instance.SetNew(&new_HggGravitonLineShapePdf);
      instance.SetNewArray(&newArray_HggGravitonLineShapePdf);
      instance.SetDelete(&delete_HggGravitonLineShapePdf);
      instance.SetDeleteArray(&deleteArray_HggGravitonLineShapePdf);
      instance.SetDestructor(&destruct_HggGravitonLineShapePdf);
      instance.SetStreamerFunc(&streamer_HggGravitonLineShapePdf);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HggGravitonLineShapePdf*)
   {
      return GenerateInitInstanceLocal((::HggGravitonLineShapePdf*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HggGravitonLineShapePdf*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggGravLSPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggGravLSPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggGravLSPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggGravLSPdfBuilder(void *p);
   static void destruct_HfittercLcLHggGravLSPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggGravLSPdfBuilder*)
   {
      ::Hfitter::HggGravLSPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggGravLSPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggGravLSPdfBuilder", ::Hfitter::HggGravLSPdfBuilder::Class_Version(), "HGGfitter/HggGravLSPdfBuilder.h", 9,
                  typeid(::Hfitter::HggGravLSPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggGravLSPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggGravLSPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggGravLSPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggGravLSPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggGravLSPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggGravLSPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggGravLSPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggGravLSPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggGravLSPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggGravLSPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HggGravTLS(void *p = 0);
   static void *newArray_HggGravTLS(Long_t size, void *p);
   static void delete_HggGravTLS(void *p);
   static void deleteArray_HggGravTLS(void *p);
   static void destruct_HggGravTLS(void *p);
   static void streamer_HggGravTLS(TBuffer &buf, void *obj);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::HggGravTLS*)
   {
      ::HggGravTLS *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::HggGravTLS >(0);
      static ::ROOT::TGenericClassInfo 
         instance("HggGravTLS", ::HggGravTLS::Class_Version(), "HGGfitter/HggGravTLS.h", 21,
                  typeid(::HggGravTLS), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::HggGravTLS::Dictionary, isa_proxy, 16,
                  sizeof(::HggGravTLS) );
      instance.SetNew(&new_HggGravTLS);
      instance.SetNewArray(&newArray_HggGravTLS);
      instance.SetDelete(&delete_HggGravTLS);
      instance.SetDeleteArray(&deleteArray_HggGravTLS);
      instance.SetDestructor(&destruct_HggGravTLS);
      instance.SetStreamerFunc(&streamer_HggGravTLS);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::HggGravTLS*)
   {
      return GenerateInitInstanceLocal((::HggGravTLS*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::HggGravTLS*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace ROOT {
   static void *new_HfittercLcLHggFactGravLSPdfBuilder(void *p = 0);
   static void *newArray_HfittercLcLHggFactGravLSPdfBuilder(Long_t size, void *p);
   static void delete_HfittercLcLHggFactGravLSPdfBuilder(void *p);
   static void deleteArray_HfittercLcLHggFactGravLSPdfBuilder(void *p);
   static void destruct_HfittercLcLHggFactGravLSPdfBuilder(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const ::Hfitter::HggFactGravLSPdfBuilder*)
   {
      ::Hfitter::HggFactGravLSPdfBuilder *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TInstrumentedIsAProxy< ::Hfitter::HggFactGravLSPdfBuilder >(0);
      static ::ROOT::TGenericClassInfo 
         instance("Hfitter::HggFactGravLSPdfBuilder", ::Hfitter::HggFactGravLSPdfBuilder::Class_Version(), "HGGfitter/HggFactGravLSPdfBuilder.h", 9,
                  typeid(::Hfitter::HggFactGravLSPdfBuilder), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &::Hfitter::HggFactGravLSPdfBuilder::Dictionary, isa_proxy, 4,
                  sizeof(::Hfitter::HggFactGravLSPdfBuilder) );
      instance.SetNew(&new_HfittercLcLHggFactGravLSPdfBuilder);
      instance.SetNewArray(&newArray_HfittercLcLHggFactGravLSPdfBuilder);
      instance.SetDelete(&delete_HfittercLcLHggFactGravLSPdfBuilder);
      instance.SetDeleteArray(&deleteArray_HfittercLcLHggFactGravLSPdfBuilder);
      instance.SetDestructor(&destruct_HfittercLcLHggFactGravLSPdfBuilder);
      return &instance;
   }
   TGenericClassInfo *GenerateInitInstance(const ::Hfitter::HggFactGravLSPdfBuilder*)
   {
      return GenerateInitInstanceLocal((::Hfitter::HggFactGravLSPdfBuilder*)0);
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const ::Hfitter::HggFactGravLSPdfBuilder*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));
} // end of namespace ROOT

namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<RooExponential,1>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooExponential,1>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<RooExponential,1>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooExponential,1>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<RooExponential,1>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooExponential,1>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooExponential,1>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> atomic_TClass_ptr HftTemplatePdfBuilder<RooLognormal,2>::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooLognormal,2>::Class_Name()
{
   return "Hfitter::HftTemplatePdfBuilder<RooLognormal,2>";
}

//______________________________________________________________________________
template <> const char *HftTemplatePdfBuilder<RooLognormal,2>::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
template <> int HftTemplatePdfBuilder<RooLognormal,2>::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooLognormal,2>::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
template <> TClass *HftTemplatePdfBuilder<RooLognormal,2>::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr MggBkgPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MggBkgPdf::Class_Name()
{
   return "Hfitter::MggBkgPdf";
}

//______________________________________________________________________________
const char *MggBkgPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBkgPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MggBkgPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBkgPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MggBkgPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBkgPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MggBkgPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBkgPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr MggBiasBkgPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *MggBiasBkgPdf::Class_Name()
{
   return "Hfitter::MggBiasBkgPdf";
}

//______________________________________________________________________________
const char *MggBiasBkgPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBiasBkgPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int MggBiasBkgPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBiasBkgPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *MggBiasBkgPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBiasBkgPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *MggBiasBkgPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::MggBiasBkgPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggLognormal::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggLognormal::Class_Name()
{
   return "Hfitter::HggLognormal";
}

//______________________________________________________________________________
const char *HggLognormal::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggLognormal*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggLognormal::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggLognormal*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggLognormal::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggLognormal*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggLognormal::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggLognormal*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggModifiedLognormal::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggModifiedLognormal::Class_Name()
{
   return "Hfitter::HggModifiedLognormal";
}

//______________________________________________________________________________
const char *HggModifiedLognormal::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggModifiedLognormal*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggModifiedLognormal::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggModifiedLognormal*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggModifiedLognormal::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggModifiedLognormal*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggModifiedLognormal::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggModifiedLognormal*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggCBShape::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggCBShape::Class_Name()
{
   return "Hfitter::HggCBShape";
}

//______________________________________________________________________________
const char *HggCBShape::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCBShape*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggCBShape::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCBShape*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggCBShape::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCBShape*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggCBShape::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCBShape*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
//______________________________________________________________________________
atomic_TClass_ptr HggTwoSidedCBPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggTwoSidedCBPdf::Class_Name()
{
   return "HggTwoSidedCBPdf";
}

//______________________________________________________________________________
const char *HggTwoSidedCBPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggTwoSidedCBPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggTwoSidedCBPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggTwoSidedCBPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggTwoSidedCBPdf*)0x0)->GetClass(); }
   return fgIsA;
}

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggTwoSidedNovoPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggTwoSidedNovoPdf::Class_Name()
{
   return "Hfitter::HggTwoSidedNovoPdf";
}

//______________________________________________________________________________
const char *HggTwoSidedNovoPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggTwoSidedNovoPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggTwoSidedNovoPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggTwoSidedNovoPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggNovoCBPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggNovoCBPdf::Class_Name()
{
   return "Hfitter::HggNovoCBPdf";
}

//______________________________________________________________________________
const char *HggNovoCBPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggNovoCBPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggNovoCBPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggNovoCBPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggNovoCBPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggNovoCBPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggNovoCBPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggNovoCBPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBifurCBPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBifurCBPdf::Class_Name()
{
   return "Hfitter::HggBifurCBPdf";
}

//______________________________________________________________________________
const char *HggBifurCBPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBifurCBPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBifurCBPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBifurCBPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBifurCBPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBifurCBPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBifurCBPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBifurCBPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggStitchedPlateauPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggStitchedPlateauPdf::Class_Name()
{
   return "Hfitter::HggStitchedPlateauPdf";
}

//______________________________________________________________________________
const char *HggStitchedPlateauPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggStitchedPlateauPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggStitchedPlateauPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggStitchedPlateauPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggStitchedPlateauPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggStitchedPlateauPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggStitchedPlateauPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggStitchedPlateauPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBernstein::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBernstein::Class_Name()
{
   return "Hfitter::HggBernstein";
}

//______________________________________________________________________________
const char *HggBernstein::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernstein*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBernstein::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernstein*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBernstein::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernstein*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBernstein::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernstein*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggExpPowerPdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggExpPowerPdf::Class_Name()
{
   return "Hfitter::HggExpPowerPdf";
}

//______________________________________________________________________________
const char *HggExpPowerPdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPowerPdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggExpPowerPdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPowerPdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggExpPowerPdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPowerPdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggExpPowerPdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPowerPdf*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggPowerLaw::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggPowerLaw::Class_Name()
{
   return "Hfitter::HggPowerLaw";
}

//______________________________________________________________________________
const char *HggPowerLaw::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowerLaw*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggPowerLaw::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowerLaw*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggPowerLaw::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowerLaw*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggPowerLaw::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowerLaw*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggExpPoly::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggExpPoly::Class_Name()
{
   return "Hfitter::HggExpPoly";
}

//______________________________________________________________________________
const char *HggExpPoly::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPoly*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggExpPoly::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPoly*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggExpPoly::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPoly*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggExpPoly::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPoly*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
//______________________________________________________________________________
atomic_TClass_ptr RooRelBreitWignerPwave::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooRelBreitWignerPwave::Class_Name()
{
   return "RooRelBreitWignerPwave";
}

//______________________________________________________________________________
const char *RooRelBreitWignerPwave::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooRelBreitWignerPwave::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooRelBreitWignerPwave::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooRelBreitWignerPwave::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooRelBreitWignerPwave_LS::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooRelBreitWignerPwave_LS::Class_Name()
{
   return "RooRelBreitWignerPwave_LS";
}

//______________________________________________________________________________
const char *RooRelBreitWignerPwave_LS::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LS*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooRelBreitWignerPwave_LS::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LS*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooRelBreitWignerPwave_LS::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LS*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooRelBreitWignerPwave_LS::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LS*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr RooRelBreitWignerPwave_LSCut::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *RooRelBreitWignerPwave_LSCut::Class_Name()
{
   return "RooRelBreitWignerPwave_LSCut";
}

//______________________________________________________________________________
const char *RooRelBreitWignerPwave_LSCut::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LSCut*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int RooRelBreitWignerPwave_LSCut::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LSCut*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *RooRelBreitWignerPwave_LSCut::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LSCut*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *RooRelBreitWignerPwave_LSCut::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::RooRelBreitWignerPwave_LSCut*)0x0)->GetClass(); }
   return fgIsA;
}

//______________________________________________________________________________
atomic_TClass_ptr HggMG5TLS::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggMG5TLS::Class_Name()
{
   return "HggMG5TLS";
}

//______________________________________________________________________________
const char *HggMG5TLS::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggMG5TLS*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggMG5TLS::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggMG5TLS*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggMG5TLS::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggMG5TLS*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggMG5TLS::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggMG5TLS*)0x0)->GetClass(); }
   return fgIsA;
}

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggDoubleExpPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggDoubleExpPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggDoubleExpPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggDoubleExpPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggDoubleExpPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggDoubleExpPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggDoubleExpPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDoubleExpPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggExpPowerPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggExpPowerPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggExpPowerPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggExpPowerPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggExpPowerPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggExpPowerPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggExpPowerPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggExpPowerPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggBiasPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggBiasPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggBiasPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggBiasPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBiasPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggBiasPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBiasPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggBiasPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBiasPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggBiasPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBiasPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggPolyPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggPolyPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggPolyPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggPolyPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPolyPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggPolyPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPolyPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggPolyPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPolyPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggPolyPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPolyPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBernsteinPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBernsteinPdfBuilder::Class_Name()
{
   return "Hfitter::HggBernsteinPdfBuilder";
}

//______________________________________________________________________________
const char *HggBernsteinPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBernsteinPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBernsteinPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBernsteinPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggChebyshevPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggChebyshevPdfBuilder::Class_Name()
{
   return "Hfitter::HggChebyshevPdfBuilder";
}

//______________________________________________________________________________
const char *HggChebyshevPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggChebyshevPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggChebyshevPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggChebyshevPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggChebyshevPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggChebyshevPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggChebyshevPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggChebyshevPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggBernstein4PdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggBernstein4PdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggBernstein4PdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggBernstein4PdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggBernstein4PdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggBernstein4PdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggBernstein4PdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein4PdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggBernstein5PdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggBernstein5PdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggBernstein5PdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggBernstein5PdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggBernstein5PdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggBernstein5PdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggBernstein5PdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggBernstein5PdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggLaurent4PdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggLaurent4PdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggLaurent4PdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggLaurent4PdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggLaurent4PdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggLaurent4PdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggLaurent4PdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggLaurent4PdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggPower2PdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggPower2PdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggPower2PdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggPower2PdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPower2PdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggPower2PdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPower2PdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggPower2PdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPower2PdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggPower2PdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPower2PdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggExpPolyPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggExpPolyPdfBuilder::Class_Name()
{
   return "Hfitter::HggExpPolyPdfBuilder";
}

//______________________________________________________________________________
const char *HggExpPolyPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolyPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggExpPolyPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolyPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggExpPolyPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolyPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggExpPolyPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolyPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggPolyPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggPolyPdfBuilder::Class_Name()
{
   return "Hfitter::HggPolyPdfBuilder";
}

//______________________________________________________________________________
const char *HggPolyPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPolyPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggPolyPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPolyPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggPolyPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPolyPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggPolyPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPolyPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggGenericBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggGenericBuilder::Class_Name()
{
   return "Hfitter::HggGenericBuilder";
}

//______________________________________________________________________________
const char *HggGenericBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGenericBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggGenericBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGenericBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggGenericBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGenericBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggGenericBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGenericBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggEliLandauExpBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggEliLandauExpBuilder::Class_Name()
{
   return "Hfitter::HggEliLandauExpBuilder";
}

//______________________________________________________________________________
const char *HggEliLandauExpBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggEliLandauExpBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggEliLandauExpBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggEliLandauExpBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggEliLandauExpBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggEliLandauExpBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggEliLandauExpBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggEliLandauExpBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigCosThStarPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigCosThStarPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigCosThStarPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigCosThStarPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigCosThStarPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigCosThStarPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigCosThStarPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigCosThStarPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigCosThStarPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigCosThStarPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigCosThStarPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgCosThStarPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgCosThStarPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgCosThStarPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgCosThStarPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgCosThStarPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgCosThStarPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgCosThStarPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgCosThStarPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgCosThStarPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgCosThStarPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgCosThStarPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigPTPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigPTPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigPTPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigPTPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigPTPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigPTPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigPTPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigPTPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigPTPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigPTPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigPTPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgPTPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgPTPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgPTPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgPTPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgPTPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgPTPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgPTPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgPTPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgPTPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgPTPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgPTPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggCosThStarPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggCosThStarPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggCosThStarPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggCosThStarPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggCosThStarPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggCosThStarPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggCosThStarPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggCosThStarPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggCosThStarPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggCosThStarPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggCosThStarPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggCosThStarPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggCosThStarPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggCosThStarPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggCosThStarKeysPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggCosThStarKeysPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggCosThStarKeysPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggCosThStarKeysPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggCosThStarKeysPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggCosThStarKeysPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggCosThStarKeysPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggPTKeysPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggPTKeysPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggPTKeysPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggPTKeysPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggPTKeysPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggPTKeysPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggPTKeysPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTKeysPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggPTPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggPTPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggPTPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggPTPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPTPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggPTPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPTPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggPTPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPTPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggPTPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggPTPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggPTPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggPTPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggPTPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggPTPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggPTPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggPTPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggPTPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggPTPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggCosThStarPTPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggCosThStarPTPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggCosThStarPTPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggCosThStarPTPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggCosThStarPTPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggCosThStarPTPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggCosThStarPTPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggCosThStarPTPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggCosThStarPTPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggCosThStarPTPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggCosThStarPTPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggCosThStarPTPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggCosThStarPTPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggCosThStarPTPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggCosThStarPTPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggSmearMPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggSmearMPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggSmearMPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggSmearMPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggSmearMPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggSmearMPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggSmearMPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggSmearMPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggSmearMPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggSmearMPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggSmearMPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggSmearMPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggSmearMPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggSmearMPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggSmearMPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggSmearMPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggSmearMPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggSmearMPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggSmearMPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggSmearMPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggSmearMPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggSmearMPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggExpPolySmearMPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggExpPolySmearMPdfBuilder::Class_Name()
{
   return "Hfitter::HggExpPolySmearMPdfBuilder";
}

//______________________________________________________________________________
const char *HggExpPolySmearMPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolySmearMPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggExpPolySmearMPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolySmearMPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggExpPolySmearMPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolySmearMPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggExpPolySmearMPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggExpPolySmearMPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBernsteinSmearMPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBernsteinSmearMPdfBuilder::Class_Name()
{
   return "Hfitter::HggBernsteinSmearMPdfBuilder";
}

//______________________________________________________________________________
const char *HggBernsteinSmearMPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinSmearMPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBernsteinSmearMPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinSmearMPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBernsteinSmearMPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinSmearMPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBernsteinSmearMPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBernsteinSmearMPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggIsolPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggIsolPdfBuilder::Class_Name()
{
   return "Hfitter::HggIsolPdfBuilder";
}

//______________________________________________________________________________
const char *HggIsolPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggIsolPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggIsolPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggIsolPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggIsolPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggIsolPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggIsolPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggIsolPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggIsolPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggIsolPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggIsolPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggIsolPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggIsolPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggIsolPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggIsolPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggIsolHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggIsolHistoBuilder::Class_Name()
{
   return "Hfitter::HggSigMggIsolHistoBuilder";
}

//______________________________________________________________________________
const char *HggSigMggIsolHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggIsolHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggIsolHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggIsolHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsolPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsolPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsolPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsolPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsolPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsolPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsolPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsolHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsolHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsolHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsolHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsolHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsolHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsolHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggDiffIsolHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggDiffIsolHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggDiffIsolHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggDiffIsolHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggDiffIsolHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggDiffIsolHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggDiffIsolHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsolBernsteinHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsolBernsteinHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsolBernsteinHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsolBernsteinHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsolBernsteinHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsolBernsteinHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsolBernsteinHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggDiffIsolBernsteinHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggDiffIsolBernsteinHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggDiffIsolBernsteinHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggDiffIsolBernsteinHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggDiffIsolBernsteinHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggDiffIsolBernsteinHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsolExpPolyHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsolExpPolyHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsolExpPolyHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsolExpPolyHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsolExpPolyHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsolExpPolyHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsolExpPolyHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggDiffIsolExpPolyHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggDiffIsolExpPolyHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggDiffIsolExpPolyHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggDiffIsolExpPolyHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggDiffIsolExpPolyHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggDiffIsolExpPolyHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsol2DHistoBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsol2DHistoBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsol2DHistoBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsol2DHistoBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsol2DHistoBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsol2DHistoBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsol2DHistoBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsol2DHistoBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsolKeysPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsolKeysPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsolKeysPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsolKeysPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsolKeysPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsolKeysPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsolKeysPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolKeysPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggSigMggIsolBremPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggSigMggIsolBremPdfBuilder::Class_Name()
{
   return "Hfitter::HggSigMggIsolBremPdfBuilder";
}

//______________________________________________________________________________
const char *HggSigMggIsolBremPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolBremPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggSigMggIsolBremPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolBremPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggSigMggIsolBremPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolBremPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggSigMggIsolBremPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggSigMggIsolBremPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBkgMggIsolBremPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBkgMggIsolBremPdfBuilder::Class_Name()
{
   return "Hfitter::HggBkgMggIsolBremPdfBuilder";
}

//______________________________________________________________________________
const char *HggBkgMggIsolBremPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBkgMggIsolBremPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBkgMggIsolBremPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBkgMggIsolBremPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBkgMggIsolBremPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggZeeMassPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggZeeMassPdfBuilder::Class_Name()
{
   return "Hfitter::HggZeeMassPdfBuilder";
}

//______________________________________________________________________________
const char *HggZeeMassPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeMassPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggZeeMassPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeMassPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggZeeMassPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeMassPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggZeeMassPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeMassPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggZeeNovoMassPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggZeeNovoMassPdfBuilder::Class_Name()
{
   return "Hfitter::HggZeeNovoMassPdfBuilder";
}

//______________________________________________________________________________
const char *HggZeeNovoMassPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeNovoMassPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggZeeNovoMassPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeNovoMassPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggZeeNovoMassPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeNovoMassPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggZeeNovoMassPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeNovoMassPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggZeeBifurMassPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggZeeBifurMassPdfBuilder::Class_Name()
{
   return "Hfitter::HggZeeBifurMassPdfBuilder";
}

//______________________________________________________________________________
const char *HggZeeBifurMassPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeBifurMassPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggZeeBifurMassPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeBifurMassPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggZeeBifurMassPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeBifurMassPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggZeeBifurMassPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggZeeBifurMassPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggTwoSidedNovoPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggTwoSidedNovoPdfBuilder::Class_Name()
{
   return "Hfitter::HggTwoSidedNovoPdfBuilder";
}

//______________________________________________________________________________
const char *HggTwoSidedNovoPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggTwoSidedNovoPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggTwoSidedNovoPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggTwoSidedNovoPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggTwoSidedNovoPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr Hgg2sCBPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *Hgg2sCBPdfBuilder::Class_Name()
{
   return "Hfitter::Hgg2sCBPdfBuilder";
}

//______________________________________________________________________________
const char *Hgg2sCBPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::Hgg2sCBPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int Hgg2sCBPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::Hgg2sCBPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *Hgg2sCBPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::Hgg2sCBPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *Hgg2sCBPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::Hgg2sCBPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggBWx2sCBPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggBWx2sCBPdfBuilder::Class_Name()
{
   return "Hfitter::HggBWx2sCBPdfBuilder";
}

//______________________________________________________________________________
const char *HggBWx2sCBPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBWx2sCBPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggBWx2sCBPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBWx2sCBPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggBWx2sCBPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBWx2sCBPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggBWx2sCBPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggBWx2sCBPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggCouplingsBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggCouplingsBuilder::Class_Name()
{
   return "Hfitter::HggCouplingsBuilder";
}

//______________________________________________________________________________
const char *HggCouplingsBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCouplingsBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggCouplingsBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCouplingsBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggCouplingsBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCouplingsBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggCouplingsBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggCouplingsBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
//______________________________________________________________________________
atomic_TClass_ptr HggNLLVar::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggNLLVar::Class_Name()
{
   return "HggNLLVar";
}

//______________________________________________________________________________
const char *HggNLLVar::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggNLLVar*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggNLLVar::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggNLLVar*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggNLLVar::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggNLLVar*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggNLLVar::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggNLLVar*)0x0)->GetClass(); }
   return fgIsA;
}

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggPowHegLSPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggPowHegLSPdfBuilder::Class_Name()
{
   return "Hfitter::HggPowHegLSPdfBuilder";
}

//______________________________________________________________________________
const char *HggPowHegLSPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowHegLSPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggPowHegLSPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowHegLSPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggPowHegLSPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowHegLSPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggPowHegLSPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggPowHegLSPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggMG5LSPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggMG5LSPdfBuilder::Class_Name()
{
   return "Hfitter::HggMG5LSPdfBuilder";
}

//______________________________________________________________________________
const char *HggMG5LSPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggMG5LSPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggMG5LSPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggMG5LSPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggMG5LSPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggMG5LSPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggMG5LSPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggMG5LSPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
//______________________________________________________________________________
atomic_TClass_ptr HggGravitonLineShapePdf::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggGravitonLineShapePdf::Class_Name()
{
   return "HggGravitonLineShapePdf";
}

//______________________________________________________________________________
const char *HggGravitonLineShapePdf::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggGravitonLineShapePdf*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggGravitonLineShapePdf::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggGravitonLineShapePdf*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggGravitonLineShapePdf::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggGravitonLineShapePdf*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggGravitonLineShapePdf::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggGravitonLineShapePdf*)0x0)->GetClass(); }
   return fgIsA;
}

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggGravLSPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggGravLSPdfBuilder::Class_Name()
{
   return "Hfitter::HggGravLSPdfBuilder";
}

//______________________________________________________________________________
const char *HggGravLSPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGravLSPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggGravLSPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGravLSPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggGravLSPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGravLSPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggGravLSPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggGravLSPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
//______________________________________________________________________________
atomic_TClass_ptr HggGravTLS::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggGravTLS::Class_Name()
{
   return "HggGravTLS";
}

//______________________________________________________________________________
const char *HggGravTLS::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggGravTLS*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggGravTLS::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::HggGravTLS*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggGravTLS::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggGravTLS*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggGravTLS::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::HggGravTLS*)0x0)->GetClass(); }
   return fgIsA;
}

namespace Hfitter {
//______________________________________________________________________________
atomic_TClass_ptr HggFactGravLSPdfBuilder::fgIsA(0);  // static to hold class pointer

//______________________________________________________________________________
const char *HggFactGravLSPdfBuilder::Class_Name()
{
   return "Hfitter::HggFactGravLSPdfBuilder";
}

//______________________________________________________________________________
const char *HggFactGravLSPdfBuilder::ImplFileName()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggFactGravLSPdfBuilder*)0x0)->GetImplFileName();
}

//______________________________________________________________________________
int HggFactGravLSPdfBuilder::ImplFileLine()
{
   return ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggFactGravLSPdfBuilder*)0x0)->GetImplFileLine();
}

//______________________________________________________________________________
TClass *HggFactGravLSPdfBuilder::Dictionary()
{
   fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggFactGravLSPdfBuilder*)0x0)->GetClass();
   return fgIsA;
}

//______________________________________________________________________________
TClass *HggFactGravLSPdfBuilder::Class()
{
   if (!fgIsA.load()) { R__LOCKGUARD(gInterpreterMutex); fgIsA = ::ROOT::GenerateInitInstanceLocal((const ::Hfitter::HggFactGravLSPdfBuilder*)0x0)->GetClass(); }
   return fgIsA;
}

} // namespace Hfitter
namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6> : new ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHggTwoSidedCBPdfcO6gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<RooExponential,1>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<RooExponential,1>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<RooExponential,1>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<RooExponential,1>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooExponential,1> : new ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<RooExponential,1>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooExponentialcO1gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<RooExponential,1> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<RooExponential,1>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3> : new ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO3gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2> : new ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlEHfittercLcLHggLognormalcO2gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>

namespace Hfitter {
//______________________________________________________________________________
template <> void HftTemplatePdfBuilder<RooLognormal,2>::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HftTemplatePdfBuilder<RooLognormal,2>.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HftTemplatePdfBuilder<RooLognormal,2>::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HftTemplatePdfBuilder<RooLognormal,2>::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p) {
      return  p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2> : new ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>;
   }
   static void *newArray_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>[nElements] : new ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p) {
      delete ((::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)p);
   }
   static void deleteArray_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p) {
      delete [] ((::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>*)p);
   }
   static void destruct_HfittercLcLHftTemplatePdfBuilderlERooLognormalcO2gR(void *p) {
      typedef ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HftTemplatePdfBuilder<RooLognormal,2>

namespace Hfitter {
//______________________________________________________________________________
void MggBkgPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::MggBkgPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::MggBkgPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::MggBkgPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLMggBkgPdf(void *p) {
      return  p ? new(p) ::Hfitter::MggBkgPdf : new ::Hfitter::MggBkgPdf;
   }
   static void *newArray_HfittercLcLMggBkgPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::MggBkgPdf[nElements] : new ::Hfitter::MggBkgPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLMggBkgPdf(void *p) {
      delete ((::Hfitter::MggBkgPdf*)p);
   }
   static void deleteArray_HfittercLcLMggBkgPdf(void *p) {
      delete [] ((::Hfitter::MggBkgPdf*)p);
   }
   static void destruct_HfittercLcLMggBkgPdf(void *p) {
      typedef ::Hfitter::MggBkgPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::MggBkgPdf

namespace Hfitter {
//______________________________________________________________________________
void MggBiasBkgPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::MggBiasBkgPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::MggBiasBkgPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::MggBiasBkgPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLMggBiasBkgPdf(void *p) {
      return  p ? new(p) ::Hfitter::MggBiasBkgPdf : new ::Hfitter::MggBiasBkgPdf;
   }
   static void *newArray_HfittercLcLMggBiasBkgPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::MggBiasBkgPdf[nElements] : new ::Hfitter::MggBiasBkgPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLMggBiasBkgPdf(void *p) {
      delete ((::Hfitter::MggBiasBkgPdf*)p);
   }
   static void deleteArray_HfittercLcLMggBiasBkgPdf(void *p) {
      delete [] ((::Hfitter::MggBiasBkgPdf*)p);
   }
   static void destruct_HfittercLcLMggBiasBkgPdf(void *p) {
      typedef ::Hfitter::MggBiasBkgPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::MggBiasBkgPdf

namespace Hfitter {
//______________________________________________________________________________
void HggLognormal::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggLognormal.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggLognormal::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggLognormal::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggLognormal(void *p) {
      return  p ? new(p) ::Hfitter::HggLognormal : new ::Hfitter::HggLognormal;
   }
   static void *newArray_HfittercLcLHggLognormal(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggLognormal[nElements] : new ::Hfitter::HggLognormal[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggLognormal(void *p) {
      delete ((::Hfitter::HggLognormal*)p);
   }
   static void deleteArray_HfittercLcLHggLognormal(void *p) {
      delete [] ((::Hfitter::HggLognormal*)p);
   }
   static void destruct_HfittercLcLHggLognormal(void *p) {
      typedef ::Hfitter::HggLognormal current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggLognormal

namespace Hfitter {
//______________________________________________________________________________
void HggModifiedLognormal::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggModifiedLognormal.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggModifiedLognormal::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggModifiedLognormal::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHggModifiedLognormal(void *p) {
      delete ((::Hfitter::HggModifiedLognormal*)p);
   }
   static void deleteArray_HfittercLcLHggModifiedLognormal(void *p) {
      delete [] ((::Hfitter::HggModifiedLognormal*)p);
   }
   static void destruct_HfittercLcLHggModifiedLognormal(void *p) {
      typedef ::Hfitter::HggModifiedLognormal current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggModifiedLognormal

namespace Hfitter {
//______________________________________________________________________________
void HggCBShape::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggCBShape.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggCBShape::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggCBShape::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggCBShape(void *p) {
      return  p ? new(p) ::Hfitter::HggCBShape : new ::Hfitter::HggCBShape;
   }
   static void *newArray_HfittercLcLHggCBShape(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggCBShape[nElements] : new ::Hfitter::HggCBShape[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggCBShape(void *p) {
      delete ((::Hfitter::HggCBShape*)p);
   }
   static void deleteArray_HfittercLcLHggCBShape(void *p) {
      delete [] ((::Hfitter::HggCBShape*)p);
   }
   static void destruct_HfittercLcLHggCBShape(void *p) {
      typedef ::Hfitter::HggCBShape current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggCBShape

//______________________________________________________________________________
void HggTwoSidedCBPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class HggTwoSidedCBPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HggTwoSidedCBPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(HggTwoSidedCBPdf::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HggTwoSidedCBPdf(void *p) {
      return  p ? new(p) ::HggTwoSidedCBPdf : new ::HggTwoSidedCBPdf;
   }
   static void *newArray_HggTwoSidedCBPdf(Long_t nElements, void *p) {
      return p ? new(p) ::HggTwoSidedCBPdf[nElements] : new ::HggTwoSidedCBPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HggTwoSidedCBPdf(void *p) {
      delete ((::HggTwoSidedCBPdf*)p);
   }
   static void deleteArray_HggTwoSidedCBPdf(void *p) {
      delete [] ((::HggTwoSidedCBPdf*)p);
   }
   static void destruct_HggTwoSidedCBPdf(void *p) {
      typedef ::HggTwoSidedCBPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HggTwoSidedCBPdf

namespace Hfitter {
//______________________________________________________________________________
void HggTwoSidedNovoPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggTwoSidedNovoPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggTwoSidedNovoPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggTwoSidedNovoPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggTwoSidedNovoPdf(void *p) {
      return  p ? new(p) ::Hfitter::HggTwoSidedNovoPdf : new ::Hfitter::HggTwoSidedNovoPdf;
   }
   static void *newArray_HfittercLcLHggTwoSidedNovoPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggTwoSidedNovoPdf[nElements] : new ::Hfitter::HggTwoSidedNovoPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggTwoSidedNovoPdf(void *p) {
      delete ((::Hfitter::HggTwoSidedNovoPdf*)p);
   }
   static void deleteArray_HfittercLcLHggTwoSidedNovoPdf(void *p) {
      delete [] ((::Hfitter::HggTwoSidedNovoPdf*)p);
   }
   static void destruct_HfittercLcLHggTwoSidedNovoPdf(void *p) {
      typedef ::Hfitter::HggTwoSidedNovoPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggTwoSidedNovoPdf

namespace Hfitter {
//______________________________________________________________________________
void HggNovoCBPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggNovoCBPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggNovoCBPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggNovoCBPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggNovoCBPdf(void *p) {
      return  p ? new(p) ::Hfitter::HggNovoCBPdf : new ::Hfitter::HggNovoCBPdf;
   }
   static void *newArray_HfittercLcLHggNovoCBPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggNovoCBPdf[nElements] : new ::Hfitter::HggNovoCBPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggNovoCBPdf(void *p) {
      delete ((::Hfitter::HggNovoCBPdf*)p);
   }
   static void deleteArray_HfittercLcLHggNovoCBPdf(void *p) {
      delete [] ((::Hfitter::HggNovoCBPdf*)p);
   }
   static void destruct_HfittercLcLHggNovoCBPdf(void *p) {
      typedef ::Hfitter::HggNovoCBPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggNovoCBPdf

namespace Hfitter {
//______________________________________________________________________________
void HggBifurCBPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBifurCBPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBifurCBPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBifurCBPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBifurCBPdf(void *p) {
      return  p ? new(p) ::Hfitter::HggBifurCBPdf : new ::Hfitter::HggBifurCBPdf;
   }
   static void *newArray_HfittercLcLHggBifurCBPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBifurCBPdf[nElements] : new ::Hfitter::HggBifurCBPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBifurCBPdf(void *p) {
      delete ((::Hfitter::HggBifurCBPdf*)p);
   }
   static void deleteArray_HfittercLcLHggBifurCBPdf(void *p) {
      delete [] ((::Hfitter::HggBifurCBPdf*)p);
   }
   static void destruct_HfittercLcLHggBifurCBPdf(void *p) {
      typedef ::Hfitter::HggBifurCBPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBifurCBPdf

namespace Hfitter {
//______________________________________________________________________________
void HggStitchedPlateauPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggStitchedPlateauPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggStitchedPlateauPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggStitchedPlateauPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggStitchedPlateauPdf(void *p) {
      return  p ? new(p) ::Hfitter::HggStitchedPlateauPdf : new ::Hfitter::HggStitchedPlateauPdf;
   }
   static void *newArray_HfittercLcLHggStitchedPlateauPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggStitchedPlateauPdf[nElements] : new ::Hfitter::HggStitchedPlateauPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggStitchedPlateauPdf(void *p) {
      delete ((::Hfitter::HggStitchedPlateauPdf*)p);
   }
   static void deleteArray_HfittercLcLHggStitchedPlateauPdf(void *p) {
      delete [] ((::Hfitter::HggStitchedPlateauPdf*)p);
   }
   static void destruct_HfittercLcLHggStitchedPlateauPdf(void *p) {
      typedef ::Hfitter::HggStitchedPlateauPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggStitchedPlateauPdf

namespace Hfitter {
//______________________________________________________________________________
void HggBernstein::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBernstein.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBernstein::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBernstein::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBernstein(void *p) {
      return  p ? new(p) ::Hfitter::HggBernstein : new ::Hfitter::HggBernstein;
   }
   static void *newArray_HfittercLcLHggBernstein(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBernstein[nElements] : new ::Hfitter::HggBernstein[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBernstein(void *p) {
      delete ((::Hfitter::HggBernstein*)p);
   }
   static void deleteArray_HfittercLcLHggBernstein(void *p) {
      delete [] ((::Hfitter::HggBernstein*)p);
   }
   static void destruct_HfittercLcLHggBernstein(void *p) {
      typedef ::Hfitter::HggBernstein current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBernstein

namespace Hfitter {
//______________________________________________________________________________
void HggExpPowerPdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggExpPowerPdf.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggExpPowerPdf::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggExpPowerPdf::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggExpPowerPdf(void *p) {
      return  p ? new(p) ::Hfitter::HggExpPowerPdf : new ::Hfitter::HggExpPowerPdf;
   }
   static void *newArray_HfittercLcLHggExpPowerPdf(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggExpPowerPdf[nElements] : new ::Hfitter::HggExpPowerPdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggExpPowerPdf(void *p) {
      delete ((::Hfitter::HggExpPowerPdf*)p);
   }
   static void deleteArray_HfittercLcLHggExpPowerPdf(void *p) {
      delete [] ((::Hfitter::HggExpPowerPdf*)p);
   }
   static void destruct_HfittercLcLHggExpPowerPdf(void *p) {
      typedef ::Hfitter::HggExpPowerPdf current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggExpPowerPdf

namespace Hfitter {
//______________________________________________________________________________
void HggPowerLaw::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggPowerLaw.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggPowerLaw::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggPowerLaw::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggPowerLaw(void *p) {
      return  p ? new(p) ::Hfitter::HggPowerLaw : new ::Hfitter::HggPowerLaw;
   }
   static void *newArray_HfittercLcLHggPowerLaw(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggPowerLaw[nElements] : new ::Hfitter::HggPowerLaw[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggPowerLaw(void *p) {
      delete ((::Hfitter::HggPowerLaw*)p);
   }
   static void deleteArray_HfittercLcLHggPowerLaw(void *p) {
      delete [] ((::Hfitter::HggPowerLaw*)p);
   }
   static void destruct_HfittercLcLHggPowerLaw(void *p) {
      typedef ::Hfitter::HggPowerLaw current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggPowerLaw

namespace Hfitter {
//______________________________________________________________________________
void HggExpPoly::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggExpPoly.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggExpPoly::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggExpPoly::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggExpPoly(void *p) {
      return  p ? new(p) ::Hfitter::HggExpPoly : new ::Hfitter::HggExpPoly;
   }
   static void *newArray_HfittercLcLHggExpPoly(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggExpPoly[nElements] : new ::Hfitter::HggExpPoly[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggExpPoly(void *p) {
      delete ((::Hfitter::HggExpPoly*)p);
   }
   static void deleteArray_HfittercLcLHggExpPoly(void *p) {
      delete [] ((::Hfitter::HggExpPoly*)p);
   }
   static void destruct_HfittercLcLHggExpPoly(void *p) {
      typedef ::Hfitter::HggExpPoly current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggExpPoly

//______________________________________________________________________________
void RooRelBreitWignerPwave::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooRelBreitWignerPwave.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooRelBreitWignerPwave::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooRelBreitWignerPwave::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooRelBreitWignerPwave(void *p) {
      return  p ? new(p) ::RooRelBreitWignerPwave : new ::RooRelBreitWignerPwave;
   }
   static void *newArray_RooRelBreitWignerPwave(Long_t nElements, void *p) {
      return p ? new(p) ::RooRelBreitWignerPwave[nElements] : new ::RooRelBreitWignerPwave[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooRelBreitWignerPwave(void *p) {
      delete ((::RooRelBreitWignerPwave*)p);
   }
   static void deleteArray_RooRelBreitWignerPwave(void *p) {
      delete [] ((::RooRelBreitWignerPwave*)p);
   }
   static void destruct_RooRelBreitWignerPwave(void *p) {
      typedef ::RooRelBreitWignerPwave current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooRelBreitWignerPwave

//______________________________________________________________________________
void RooRelBreitWignerPwave_LS::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooRelBreitWignerPwave_LS.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooRelBreitWignerPwave_LS::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooRelBreitWignerPwave_LS::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooRelBreitWignerPwave_LS(void *p) {
      return  p ? new(p) ::RooRelBreitWignerPwave_LS : new ::RooRelBreitWignerPwave_LS;
   }
   static void *newArray_RooRelBreitWignerPwave_LS(Long_t nElements, void *p) {
      return p ? new(p) ::RooRelBreitWignerPwave_LS[nElements] : new ::RooRelBreitWignerPwave_LS[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooRelBreitWignerPwave_LS(void *p) {
      delete ((::RooRelBreitWignerPwave_LS*)p);
   }
   static void deleteArray_RooRelBreitWignerPwave_LS(void *p) {
      delete [] ((::RooRelBreitWignerPwave_LS*)p);
   }
   static void destruct_RooRelBreitWignerPwave_LS(void *p) {
      typedef ::RooRelBreitWignerPwave_LS current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooRelBreitWignerPwave_LS

//______________________________________________________________________________
void RooRelBreitWignerPwave_LSCut::Streamer(TBuffer &R__b)
{
   // Stream an object of class RooRelBreitWignerPwave_LSCut.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(RooRelBreitWignerPwave_LSCut::Class(),this);
   } else {
      R__b.WriteClassBuffer(RooRelBreitWignerPwave_LSCut::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_RooRelBreitWignerPwave_LSCut(void *p) {
      return  p ? new(p) ::RooRelBreitWignerPwave_LSCut : new ::RooRelBreitWignerPwave_LSCut;
   }
   static void *newArray_RooRelBreitWignerPwave_LSCut(Long_t nElements, void *p) {
      return p ? new(p) ::RooRelBreitWignerPwave_LSCut[nElements] : new ::RooRelBreitWignerPwave_LSCut[nElements];
   }
   // Wrapper around operator delete
   static void delete_RooRelBreitWignerPwave_LSCut(void *p) {
      delete ((::RooRelBreitWignerPwave_LSCut*)p);
   }
   static void deleteArray_RooRelBreitWignerPwave_LSCut(void *p) {
      delete [] ((::RooRelBreitWignerPwave_LSCut*)p);
   }
   static void destruct_RooRelBreitWignerPwave_LSCut(void *p) {
      typedef ::RooRelBreitWignerPwave_LSCut current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::RooRelBreitWignerPwave_LSCut

//______________________________________________________________________________
void HggMG5TLS::Streamer(TBuffer &R__b)
{
   // Stream an object of class HggMG5TLS.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HggMG5TLS::Class(),this);
   } else {
      R__b.WriteClassBuffer(HggMG5TLS::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HggMG5TLS(void *p) {
      return  p ? new(p) ::HggMG5TLS : new ::HggMG5TLS;
   }
   static void *newArray_HggMG5TLS(Long_t nElements, void *p) {
      return p ? new(p) ::HggMG5TLS[nElements] : new ::HggMG5TLS[nElements];
   }
   // Wrapper around operator delete
   static void delete_HggMG5TLS(void *p) {
      delete ((::HggMG5TLS*)p);
   }
   static void deleteArray_HggMG5TLS(void *p) {
      delete [] ((::HggMG5TLS*)p);
   }
   static void destruct_HggMG5TLS(void *p) {
      typedef ::HggMG5TLS current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HggMG5TLS

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggPdfBuilder : new ::Hfitter::HggSigMggPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggPdfBuilder[nElements] : new ::Hfitter::HggSigMggPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggPdfBuilder : new ::Hfitter::HggBkgMggPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggPdfBuilder[nElements] : new ::Hfitter::HggBkgMggPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggDoubleExpPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggDoubleExpPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggDoubleExpPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggDoubleExpPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggDoubleExpPdfBuilder : new ::Hfitter::HggBkgMggDoubleExpPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggDoubleExpPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggDoubleExpPdfBuilder[nElements] : new ::Hfitter::HggBkgMggDoubleExpPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggDoubleExpPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggDoubleExpPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggDoubleExpPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggDoubleExpPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggDoubleExpPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggExpPowerPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggExpPowerPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggExpPowerPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggExpPowerPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggExpPowerPdfBuilder : new ::Hfitter::HggBkgMggExpPowerPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggExpPowerPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggExpPowerPdfBuilder[nElements] : new ::Hfitter::HggBkgMggExpPowerPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggExpPowerPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggExpPowerPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggExpPowerPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggExpPowerPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggExpPowerPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggBiasPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggBiasPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggBiasPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggBiasPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggBiasPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggBiasPdfBuilder : new ::Hfitter::HggBkgMggBiasPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggBiasPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggBiasPdfBuilder[nElements] : new ::Hfitter::HggBkgMggBiasPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggBiasPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggBiasPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggBiasPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggBiasPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggBiasPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggBiasPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggBiasPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggPolyPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggPolyPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggPolyPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggPolyPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggPolyPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggPolyPdfBuilder : new ::Hfitter::HggBkgMggPolyPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggPolyPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggPolyPdfBuilder[nElements] : new ::Hfitter::HggBkgMggPolyPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggPolyPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggPolyPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggPolyPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggPolyPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggPolyPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggPolyPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggPolyPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBernsteinPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBernsteinPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBernsteinPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBernsteinPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBernsteinPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBernsteinPdfBuilder : new ::Hfitter::HggBernsteinPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBernsteinPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBernsteinPdfBuilder[nElements] : new ::Hfitter::HggBernsteinPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBernsteinPdfBuilder(void *p) {
      delete ((::Hfitter::HggBernsteinPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBernsteinPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBernsteinPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBernsteinPdfBuilder(void *p) {
      typedef ::Hfitter::HggBernsteinPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBernsteinPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggChebyshevPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggChebyshevPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggChebyshevPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggChebyshevPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggChebyshevPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggChebyshevPdfBuilder : new ::Hfitter::HggChebyshevPdfBuilder;
   }
   static void *newArray_HfittercLcLHggChebyshevPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggChebyshevPdfBuilder[nElements] : new ::Hfitter::HggChebyshevPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggChebyshevPdfBuilder(void *p) {
      delete ((::Hfitter::HggChebyshevPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggChebyshevPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggChebyshevPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggChebyshevPdfBuilder(void *p) {
      typedef ::Hfitter::HggChebyshevPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggChebyshevPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggBernstein4PdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggBernstein4PdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggBernstein4PdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggBernstein4PdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggBernstein4PdfBuilder : new ::Hfitter::HggBkgMggBernstein4PdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggBernstein4PdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggBernstein4PdfBuilder[nElements] : new ::Hfitter::HggBkgMggBernstein4PdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggBernstein4PdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggBernstein4PdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggBernstein4PdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggBernstein4PdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggBernstein4PdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggBernstein5PdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggBernstein5PdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggBernstein5PdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggBernstein5PdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggBernstein5PdfBuilder : new ::Hfitter::HggBkgMggBernstein5PdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggBernstein5PdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggBernstein5PdfBuilder[nElements] : new ::Hfitter::HggBkgMggBernstein5PdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggBernstein5PdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggBernstein5PdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggBernstein5PdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggBernstein5PdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggBernstein5PdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggLaurent4PdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggLaurent4PdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggLaurent4PdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggLaurent4PdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggLaurent4PdfBuilder : new ::Hfitter::HggBkgMggLaurent4PdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggLaurent4PdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggLaurent4PdfBuilder[nElements] : new ::Hfitter::HggBkgMggLaurent4PdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggLaurent4PdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggLaurent4PdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggLaurent4PdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggLaurent4PdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggLaurent4PdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggPower2PdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggPower2PdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggPower2PdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggPower2PdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggPower2PdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggPower2PdfBuilder : new ::Hfitter::HggBkgMggPower2PdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggPower2PdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggPower2PdfBuilder[nElements] : new ::Hfitter::HggBkgMggPower2PdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggPower2PdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggPower2PdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggPower2PdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggPower2PdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggPower2PdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggPower2PdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggPower2PdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggExpPolyPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggExpPolyPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggExpPolyPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggExpPolyPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggExpPolyPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggExpPolyPdfBuilder : new ::Hfitter::HggExpPolyPdfBuilder;
   }
   static void *newArray_HfittercLcLHggExpPolyPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggExpPolyPdfBuilder[nElements] : new ::Hfitter::HggExpPolyPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggExpPolyPdfBuilder(void *p) {
      delete ((::Hfitter::HggExpPolyPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggExpPolyPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggExpPolyPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggExpPolyPdfBuilder(void *p) {
      typedef ::Hfitter::HggExpPolyPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggExpPolyPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggPolyPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggPolyPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggPolyPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggPolyPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggPolyPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggPolyPdfBuilder : new ::Hfitter::HggPolyPdfBuilder;
   }
   static void *newArray_HfittercLcLHggPolyPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggPolyPdfBuilder[nElements] : new ::Hfitter::HggPolyPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggPolyPdfBuilder(void *p) {
      delete ((::Hfitter::HggPolyPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggPolyPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggPolyPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggPolyPdfBuilder(void *p) {
      typedef ::Hfitter::HggPolyPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggPolyPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggGenericBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggGenericBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggGenericBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggGenericBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggGenericBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggGenericBuilder : new ::Hfitter::HggGenericBuilder;
   }
   static void *newArray_HfittercLcLHggGenericBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggGenericBuilder[nElements] : new ::Hfitter::HggGenericBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggGenericBuilder(void *p) {
      delete ((::Hfitter::HggGenericBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggGenericBuilder(void *p) {
      delete [] ((::Hfitter::HggGenericBuilder*)p);
   }
   static void destruct_HfittercLcLHggGenericBuilder(void *p) {
      typedef ::Hfitter::HggGenericBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggGenericBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggEliLandauExpBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggEliLandauExpBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggEliLandauExpBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggEliLandauExpBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggEliLandauExpBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggEliLandauExpBuilder : new ::Hfitter::HggEliLandauExpBuilder;
   }
   static void *newArray_HfittercLcLHggEliLandauExpBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggEliLandauExpBuilder[nElements] : new ::Hfitter::HggEliLandauExpBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggEliLandauExpBuilder(void *p) {
      delete ((::Hfitter::HggEliLandauExpBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggEliLandauExpBuilder(void *p) {
      delete [] ((::Hfitter::HggEliLandauExpBuilder*)p);
   }
   static void destruct_HfittercLcLHggEliLandauExpBuilder(void *p) {
      typedef ::Hfitter::HggEliLandauExpBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggEliLandauExpBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigCosThStarPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigCosThStarPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigCosThStarPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigCosThStarPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigCosThStarPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigCosThStarPdfBuilder : new ::Hfitter::HggSigCosThStarPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigCosThStarPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigCosThStarPdfBuilder[nElements] : new ::Hfitter::HggSigCosThStarPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigCosThStarPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigCosThStarPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigCosThStarPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigCosThStarPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigCosThStarPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigCosThStarPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigCosThStarPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgCosThStarPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgCosThStarPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgCosThStarPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgCosThStarPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgCosThStarPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgCosThStarPdfBuilder : new ::Hfitter::HggBkgCosThStarPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgCosThStarPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgCosThStarPdfBuilder[nElements] : new ::Hfitter::HggBkgCosThStarPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgCosThStarPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgCosThStarPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgCosThStarPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgCosThStarPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgCosThStarPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgCosThStarPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgCosThStarPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigPTPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigPTPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigPTPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigPTPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigPTPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigPTPdfBuilder : new ::Hfitter::HggSigPTPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigPTPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigPTPdfBuilder[nElements] : new ::Hfitter::HggSigPTPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigPTPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigPTPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigPTPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigPTPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigPTPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigPTPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigPTPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgPTPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgPTPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgPTPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgPTPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgPTPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgPTPdfBuilder : new ::Hfitter::HggBkgPTPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgPTPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgPTPdfBuilder[nElements] : new ::Hfitter::HggBkgPTPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgPTPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgPTPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgPTPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgPTPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgPTPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgPTPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgPTPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggCosThStarPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggCosThStarPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggCosThStarPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggCosThStarPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggCosThStarPdfBuilder : new ::Hfitter::HggSigMggCosThStarPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggCosThStarPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggCosThStarPdfBuilder[nElements] : new ::Hfitter::HggSigMggCosThStarPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggCosThStarPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggCosThStarPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggCosThStarPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggCosThStarPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggCosThStarPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggCosThStarPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggCosThStarPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggCosThStarPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggCosThStarPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggCosThStarPdfBuilder : new ::Hfitter::HggBkgMggCosThStarPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggCosThStarPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggCosThStarPdfBuilder[nElements] : new ::Hfitter::HggBkgMggCosThStarPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggCosThStarPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggCosThStarPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggCosThStarPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggCosThStarPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggCosThStarPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggCosThStarKeysPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggCosThStarKeysPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggCosThStarKeysPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggCosThStarKeysPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder : new ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder[nElements] : new ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggCosThStarKeysPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggCosThStarKeysPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggCosThStarKeysPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggPTKeysPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggPTKeysPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggPTKeysPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggPTKeysPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggPTKeysPdfBuilder : new ::Hfitter::HggBkgMggPTKeysPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggPTKeysPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggPTKeysPdfBuilder[nElements] : new ::Hfitter::HggBkgMggPTKeysPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggPTKeysPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggPTKeysPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggPTKeysPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggPTKeysPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggPTKeysPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggPTPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggPTPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggPTPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggPTPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggPTPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggPTPdfBuilder : new ::Hfitter::HggSigMggPTPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggPTPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggPTPdfBuilder[nElements] : new ::Hfitter::HggSigMggPTPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggPTPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggPTPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggPTPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggPTPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggPTPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggPTPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggPTPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggPTPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggPTPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggPTPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggPTPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggPTPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggPTPdfBuilder : new ::Hfitter::HggBkgMggPTPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggPTPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggPTPdfBuilder[nElements] : new ::Hfitter::HggBkgMggPTPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggPTPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggPTPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggPTPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggPTPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggPTPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggPTPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggPTPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggCosThStarPTPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggCosThStarPTPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggCosThStarPTPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggCosThStarPTPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggCosThStarPTPdfBuilder : new ::Hfitter::HggSigMggCosThStarPTPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggCosThStarPTPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggCosThStarPTPdfBuilder[nElements] : new ::Hfitter::HggSigMggCosThStarPTPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggCosThStarPTPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggCosThStarPTPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggCosThStarPTPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggCosThStarPTPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggCosThStarPTPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggCosThStarPTPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggCosThStarPTPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggCosThStarPTPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggCosThStarPTPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggCosThStarPTPdfBuilder : new ::Hfitter::HggBkgMggCosThStarPTPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggCosThStarPTPdfBuilder[nElements] : new ::Hfitter::HggBkgMggCosThStarPTPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggCosThStarPTPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggCosThStarPTPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggCosThStarPTPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggCosThStarPTPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggSmearMPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggSmearMPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggSmearMPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggSmearMPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggSmearMPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggSmearMPdfBuilder : new ::Hfitter::HggSigMggSmearMPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggSmearMPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggSmearMPdfBuilder[nElements] : new ::Hfitter::HggSigMggSmearMPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggSmearMPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggSmearMPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggSmearMPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggSmearMPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggSmearMPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggSmearMPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggSmearMPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggSmearMPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggSmearMPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggSmearMPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggSmearMPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggSmearMPdfBuilder : new ::Hfitter::HggBkgMggSmearMPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggSmearMPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggSmearMPdfBuilder[nElements] : new ::Hfitter::HggBkgMggSmearMPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggSmearMPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggSmearMPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggSmearMPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggSmearMPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggSmearMPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggExpPolySmearMPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggExpPolySmearMPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggExpPolySmearMPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggExpPolySmearMPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggExpPolySmearMPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggExpPolySmearMPdfBuilder : new ::Hfitter::HggExpPolySmearMPdfBuilder;
   }
   static void *newArray_HfittercLcLHggExpPolySmearMPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggExpPolySmearMPdfBuilder[nElements] : new ::Hfitter::HggExpPolySmearMPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggExpPolySmearMPdfBuilder(void *p) {
      delete ((::Hfitter::HggExpPolySmearMPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggExpPolySmearMPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggExpPolySmearMPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggExpPolySmearMPdfBuilder(void *p) {
      typedef ::Hfitter::HggExpPolySmearMPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggExpPolySmearMPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBernsteinSmearMPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBernsteinSmearMPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBernsteinSmearMPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBernsteinSmearMPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBernsteinSmearMPdfBuilder : new ::Hfitter::HggBernsteinSmearMPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBernsteinSmearMPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBernsteinSmearMPdfBuilder[nElements] : new ::Hfitter::HggBernsteinSmearMPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p) {
      delete ((::Hfitter::HggBernsteinSmearMPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBernsteinSmearMPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBernsteinSmearMPdfBuilder(void *p) {
      typedef ::Hfitter::HggBernsteinSmearMPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBernsteinSmearMPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggIsolPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggIsolPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggIsolPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggIsolPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggIsolPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggIsolPdfBuilder : new ::Hfitter::HggIsolPdfBuilder;
   }
   static void *newArray_HfittercLcLHggIsolPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggIsolPdfBuilder[nElements] : new ::Hfitter::HggIsolPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggIsolPdfBuilder(void *p) {
      delete ((::Hfitter::HggIsolPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggIsolPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggIsolPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggIsolPdfBuilder(void *p) {
      typedef ::Hfitter::HggIsolPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggIsolPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggIsolPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggIsolPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggIsolPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggIsolPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggIsolPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggIsolPdfBuilder : new ::Hfitter::HggSigMggIsolPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggIsolPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggIsolPdfBuilder[nElements] : new ::Hfitter::HggSigMggIsolPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggIsolPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggIsolPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggIsolPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggIsolPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggIsolPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggIsolPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggIsolPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggIsolHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggIsolHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggIsolHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggIsolHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggIsolHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggIsolHistoBuilder : new ::Hfitter::HggSigMggIsolHistoBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggIsolHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggIsolHistoBuilder[nElements] : new ::Hfitter::HggSigMggIsolHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggIsolHistoBuilder(void *p) {
      delete ((::Hfitter::HggSigMggIsolHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggIsolHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggIsolHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggIsolHistoBuilder(void *p) {
      typedef ::Hfitter::HggSigMggIsolHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggIsolHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsolPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsolPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsolPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsolPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsolPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsolPdfBuilder : new ::Hfitter::HggBkgMggIsolPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsolPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsolPdfBuilder[nElements] : new ::Hfitter::HggBkgMggIsolPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsolPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsolPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsolPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsolPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsolPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsolPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsolPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsolHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsolHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsolHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsolHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsolHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsolHistoBuilder : new ::Hfitter::HggBkgMggIsolHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsolHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsolHistoBuilder[nElements] : new ::Hfitter::HggBkgMggIsolHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsolHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsolHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsolHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsolHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsolHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsolHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsolHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggDiffIsolHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggDiffIsolHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggDiffIsolHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggDiffIsolHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggDiffIsolHistoBuilder : new ::Hfitter::HggBkgMggDiffIsolHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggDiffIsolHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggDiffIsolHistoBuilder[nElements] : new ::Hfitter::HggBkgMggDiffIsolHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggDiffIsolHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggDiffIsolHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggDiffIsolHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggDiffIsolHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggDiffIsolHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsolBernsteinHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsolBernsteinHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsolBernsteinHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsolBernsteinHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder : new ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder[nElements] : new ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsolBernsteinHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsolBernsteinHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsolBernsteinHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggDiffIsolBernsteinHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder : new ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder[nElements] : new ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggDiffIsolBernsteinHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsolExpPolyHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsolExpPolyHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsolExpPolyHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsolExpPolyHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder : new ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder[nElements] : new ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsolExpPolyHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsolExpPolyHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsolExpPolyHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggDiffIsolExpPolyHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder : new ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder[nElements] : new ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggDiffIsolExpPolyHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsol2DHistoBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsol2DHistoBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsol2DHistoBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsol2DHistoBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsol2DHistoBuilder : new ::Hfitter::HggBkgMggIsol2DHistoBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsol2DHistoBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsol2DHistoBuilder[nElements] : new ::Hfitter::HggBkgMggIsol2DHistoBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsol2DHistoBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsol2DHistoBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsol2DHistoBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsol2DHistoBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsol2DHistoBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsolKeysPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsolKeysPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsolKeysPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsolKeysPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsolKeysPdfBuilder : new ::Hfitter::HggBkgMggIsolKeysPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsolKeysPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsolKeysPdfBuilder[nElements] : new ::Hfitter::HggBkgMggIsolKeysPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsolKeysPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsolKeysPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsolKeysPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsolKeysPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsolKeysPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggSigMggIsolBremPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggSigMggIsolBremPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggSigMggIsolBremPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggSigMggIsolBremPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggSigMggIsolBremPdfBuilder : new ::Hfitter::HggSigMggIsolBremPdfBuilder;
   }
   static void *newArray_HfittercLcLHggSigMggIsolBremPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggSigMggIsolBremPdfBuilder[nElements] : new ::Hfitter::HggSigMggIsolBremPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p) {
      delete ((::Hfitter::HggSigMggIsolBremPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggSigMggIsolBremPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggSigMggIsolBremPdfBuilder(void *p) {
      typedef ::Hfitter::HggSigMggIsolBremPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggSigMggIsolBremPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBkgMggIsolBremPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBkgMggIsolBremPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBkgMggIsolBremPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBkgMggIsolBremPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBkgMggIsolBremPdfBuilder : new ::Hfitter::HggBkgMggIsolBremPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBkgMggIsolBremPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBkgMggIsolBremPdfBuilder[nElements] : new ::Hfitter::HggBkgMggIsolBremPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p) {
      delete ((::Hfitter::HggBkgMggIsolBremPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBkgMggIsolBremPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBkgMggIsolBremPdfBuilder(void *p) {
      typedef ::Hfitter::HggBkgMggIsolBremPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBkgMggIsolBremPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggZeeMassPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggZeeMassPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggZeeMassPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggZeeMassPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggZeeMassPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggZeeMassPdfBuilder : new ::Hfitter::HggZeeMassPdfBuilder;
   }
   static void *newArray_HfittercLcLHggZeeMassPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggZeeMassPdfBuilder[nElements] : new ::Hfitter::HggZeeMassPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggZeeMassPdfBuilder(void *p) {
      delete ((::Hfitter::HggZeeMassPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggZeeMassPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggZeeMassPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggZeeMassPdfBuilder(void *p) {
      typedef ::Hfitter::HggZeeMassPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggZeeMassPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggZeeNovoMassPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggZeeNovoMassPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggZeeNovoMassPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggZeeNovoMassPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggZeeNovoMassPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggZeeNovoMassPdfBuilder : new ::Hfitter::HggZeeNovoMassPdfBuilder;
   }
   static void *newArray_HfittercLcLHggZeeNovoMassPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggZeeNovoMassPdfBuilder[nElements] : new ::Hfitter::HggZeeNovoMassPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggZeeNovoMassPdfBuilder(void *p) {
      delete ((::Hfitter::HggZeeNovoMassPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggZeeNovoMassPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggZeeNovoMassPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggZeeNovoMassPdfBuilder(void *p) {
      typedef ::Hfitter::HggZeeNovoMassPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggZeeNovoMassPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggZeeBifurMassPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggZeeBifurMassPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggZeeBifurMassPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggZeeBifurMassPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggZeeBifurMassPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggZeeBifurMassPdfBuilder : new ::Hfitter::HggZeeBifurMassPdfBuilder;
   }
   static void *newArray_HfittercLcLHggZeeBifurMassPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggZeeBifurMassPdfBuilder[nElements] : new ::Hfitter::HggZeeBifurMassPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggZeeBifurMassPdfBuilder(void *p) {
      delete ((::Hfitter::HggZeeBifurMassPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggZeeBifurMassPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggZeeBifurMassPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggZeeBifurMassPdfBuilder(void *p) {
      typedef ::Hfitter::HggZeeBifurMassPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggZeeBifurMassPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggTwoSidedNovoPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggTwoSidedNovoPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggTwoSidedNovoPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggTwoSidedNovoPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggTwoSidedNovoPdfBuilder : new ::Hfitter::HggTwoSidedNovoPdfBuilder;
   }
   static void *newArray_HfittercLcLHggTwoSidedNovoPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggTwoSidedNovoPdfBuilder[nElements] : new ::Hfitter::HggTwoSidedNovoPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p) {
      delete ((::Hfitter::HggTwoSidedNovoPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggTwoSidedNovoPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggTwoSidedNovoPdfBuilder(void *p) {
      typedef ::Hfitter::HggTwoSidedNovoPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggTwoSidedNovoPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void Hgg2sCBPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::Hgg2sCBPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::Hgg2sCBPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::Hgg2sCBPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHgg2sCBPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::Hgg2sCBPdfBuilder : new ::Hfitter::Hgg2sCBPdfBuilder;
   }
   static void *newArray_HfittercLcLHgg2sCBPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::Hgg2sCBPdfBuilder[nElements] : new ::Hfitter::Hgg2sCBPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHgg2sCBPdfBuilder(void *p) {
      delete ((::Hfitter::Hgg2sCBPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHgg2sCBPdfBuilder(void *p) {
      delete [] ((::Hfitter::Hgg2sCBPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHgg2sCBPdfBuilder(void *p) {
      typedef ::Hfitter::Hgg2sCBPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::Hgg2sCBPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggBWx2sCBPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggBWx2sCBPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggBWx2sCBPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggBWx2sCBPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggBWx2sCBPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggBWx2sCBPdfBuilder : new ::Hfitter::HggBWx2sCBPdfBuilder;
   }
   static void *newArray_HfittercLcLHggBWx2sCBPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggBWx2sCBPdfBuilder[nElements] : new ::Hfitter::HggBWx2sCBPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggBWx2sCBPdfBuilder(void *p) {
      delete ((::Hfitter::HggBWx2sCBPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggBWx2sCBPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggBWx2sCBPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggBWx2sCBPdfBuilder(void *p) {
      typedef ::Hfitter::HggBWx2sCBPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggBWx2sCBPdfBuilder

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggCouplings(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggCouplings : new ::Hfitter::HggCouplings;
   }
   static void *newArray_HfittercLcLHggCouplings(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggCouplings[nElements] : new ::Hfitter::HggCouplings[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggCouplings(void *p) {
      delete ((::Hfitter::HggCouplings*)p);
   }
   static void deleteArray_HfittercLcLHggCouplings(void *p) {
      delete [] ((::Hfitter::HggCouplings*)p);
   }
   static void destruct_HfittercLcLHggCouplings(void *p) {
      typedef ::Hfitter::HggCouplings current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggCouplings

namespace Hfitter {
//______________________________________________________________________________
void HggCouplingsBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggCouplingsBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggCouplingsBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggCouplingsBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggCouplingsBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggCouplingsBuilder : new ::Hfitter::HggCouplingsBuilder;
   }
   static void *newArray_HfittercLcLHggCouplingsBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggCouplingsBuilder[nElements] : new ::Hfitter::HggCouplingsBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggCouplingsBuilder(void *p) {
      delete ((::Hfitter::HggCouplingsBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggCouplingsBuilder(void *p) {
      delete [] ((::Hfitter::HggCouplingsBuilder*)p);
   }
   static void destruct_HfittercLcLHggCouplingsBuilder(void *p) {
      typedef ::Hfitter::HggCouplingsBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggCouplingsBuilder

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggTools(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggTools : new ::Hfitter::HggTools;
   }
   static void *newArray_HfittercLcLHggTools(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggTools[nElements] : new ::Hfitter::HggTools[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggTools(void *p) {
      delete ((::Hfitter::HggTools*)p);
   }
   static void deleteArray_HfittercLcLHggTools(void *p) {
      delete [] ((::Hfitter::HggTools*)p);
   }
   static void destruct_HfittercLcLHggTools(void *p) {
      typedef ::Hfitter::HggTools current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggTools

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHggEtaCatHelper(void *p) {
      delete ((::Hfitter::HggEtaCatHelper*)p);
   }
   static void deleteArray_HfittercLcLHggEtaCatHelper(void *p) {
      delete [] ((::Hfitter::HggEtaCatHelper*)p);
   }
   static void destruct_HfittercLcLHggEtaCatHelper(void *p) {
      typedef ::Hfitter::HggEtaCatHelper current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggEtaCatHelper

namespace ROOT {
   // Wrapper around operator delete
   static void delete_HfittercLcLHggResolHelper(void *p) {
      delete ((::Hfitter::HggResolHelper*)p);
   }
   static void deleteArray_HfittercLcLHggResolHelper(void *p) {
      delete [] ((::Hfitter::HggResolHelper*)p);
   }
   static void destruct_HfittercLcLHggResolHelper(void *p) {
      typedef ::Hfitter::HggResolHelper current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggResolHelper

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggZeeIsolStudy(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggZeeIsolStudy : new ::Hfitter::HggZeeIsolStudy;
   }
   static void *newArray_HfittercLcLHggZeeIsolStudy(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggZeeIsolStudy[nElements] : new ::Hfitter::HggZeeIsolStudy[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggZeeIsolStudy(void *p) {
      delete ((::Hfitter::HggZeeIsolStudy*)p);
   }
   static void deleteArray_HfittercLcLHggZeeIsolStudy(void *p) {
      delete [] ((::Hfitter::HggZeeIsolStudy*)p);
   }
   static void destruct_HfittercLcLHggZeeIsolStudy(void *p) {
      typedef ::Hfitter::HggZeeIsolStudy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggZeeIsolStudy

namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggNewResonanceStudy(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggNewResonanceStudy : new ::Hfitter::HggNewResonanceStudy;
   }
   static void *newArray_HfittercLcLHggNewResonanceStudy(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) ::Hfitter::HggNewResonanceStudy[nElements] : new ::Hfitter::HggNewResonanceStudy[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggNewResonanceStudy(void *p) {
      delete ((::Hfitter::HggNewResonanceStudy*)p);
   }
   static void deleteArray_HfittercLcLHggNewResonanceStudy(void *p) {
      delete [] ((::Hfitter::HggNewResonanceStudy*)p);
   }
   static void destruct_HfittercLcLHggNewResonanceStudy(void *p) {
      typedef ::Hfitter::HggNewResonanceStudy current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggNewResonanceStudy

//______________________________________________________________________________
void HggNLLVar::Streamer(TBuffer &R__b)
{
   // Stream an object of class HggNLLVar.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(HggNLLVar::Class(),this);
   } else {
      R__b.WriteClassBuffer(HggNLLVar::Class(),this);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HggNLLVar(void *p) {
      return  p ? new(p) ::HggNLLVar : new ::HggNLLVar;
   }
   static void *newArray_HggNLLVar(Long_t nElements, void *p) {
      return p ? new(p) ::HggNLLVar[nElements] : new ::HggNLLVar[nElements];
   }
   // Wrapper around operator delete
   static void delete_HggNLLVar(void *p) {
      delete ((::HggNLLVar*)p);
   }
   static void deleteArray_HggNLLVar(void *p) {
      delete [] ((::HggNLLVar*)p);
   }
   static void destruct_HggNLLVar(void *p) {
      typedef ::HggNLLVar current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::HggNLLVar

namespace Hfitter {
//______________________________________________________________________________
void HggPowHegLSPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggPowHegLSPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggPowHegLSPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggPowHegLSPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggPowHegLSPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggPowHegLSPdfBuilder : new ::Hfitter::HggPowHegLSPdfBuilder;
   }
   static void *newArray_HfittercLcLHggPowHegLSPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggPowHegLSPdfBuilder[nElements] : new ::Hfitter::HggPowHegLSPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggPowHegLSPdfBuilder(void *p) {
      delete ((::Hfitter::HggPowHegLSPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggPowHegLSPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggPowHegLSPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggPowHegLSPdfBuilder(void *p) {
      typedef ::Hfitter::HggPowHegLSPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggPowHegLSPdfBuilder

namespace Hfitter {
//______________________________________________________________________________
void HggMG5LSPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggMG5LSPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggMG5LSPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggMG5LSPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggMG5LSPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggMG5LSPdfBuilder : new ::Hfitter::HggMG5LSPdfBuilder;
   }
   static void *newArray_HfittercLcLHggMG5LSPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggMG5LSPdfBuilder[nElements] : new ::Hfitter::HggMG5LSPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggMG5LSPdfBuilder(void *p) {
      delete ((::Hfitter::HggMG5LSPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggMG5LSPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggMG5LSPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggMG5LSPdfBuilder(void *p) {
      typedef ::Hfitter::HggMG5LSPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggMG5LSPdfBuilder

//______________________________________________________________________________
void HggGravitonLineShapePdf::Streamer(TBuffer &R__b)
{
   // Stream an object of class HggGravitonLineShapePdf.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      mG.Streamer(R__b);
      GkM.Streamer(R__b);
      R__b >> cme;
      R__b.CheckByteCount(R__s, R__c, HggGravitonLineShapePdf::IsA());
   } else {
      R__c = R__b.WriteVersion(HggGravitonLineShapePdf::IsA(), kTRUE);
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      mG.Streamer(R__b);
      GkM.Streamer(R__b);
      R__b << cme;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HggGravitonLineShapePdf(void *p) {
      return  p ? new(p) ::HggGravitonLineShapePdf : new ::HggGravitonLineShapePdf;
   }
   static void *newArray_HggGravitonLineShapePdf(Long_t nElements, void *p) {
      return p ? new(p) ::HggGravitonLineShapePdf[nElements] : new ::HggGravitonLineShapePdf[nElements];
   }
   // Wrapper around operator delete
   static void delete_HggGravitonLineShapePdf(void *p) {
      delete ((::HggGravitonLineShapePdf*)p);
   }
   static void deleteArray_HggGravitonLineShapePdf(void *p) {
      delete [] ((::HggGravitonLineShapePdf*)p);
   }
   static void destruct_HggGravitonLineShapePdf(void *p) {
      typedef ::HggGravitonLineShapePdf current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_HggGravitonLineShapePdf(TBuffer &buf, void *obj) {
      ((::HggGravitonLineShapePdf*)obj)->::HggGravitonLineShapePdf::Streamer(buf);
   }
} // end of namespace ROOT for class ::HggGravitonLineShapePdf

namespace Hfitter {
//______________________________________________________________________________
void HggGravLSPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggGravLSPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggGravLSPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggGravLSPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggGravLSPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggGravLSPdfBuilder : new ::Hfitter::HggGravLSPdfBuilder;
   }
   static void *newArray_HfittercLcLHggGravLSPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggGravLSPdfBuilder[nElements] : new ::Hfitter::HggGravLSPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggGravLSPdfBuilder(void *p) {
      delete ((::Hfitter::HggGravLSPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggGravLSPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggGravLSPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggGravLSPdfBuilder(void *p) {
      typedef ::Hfitter::HggGravLSPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggGravLSPdfBuilder

//______________________________________________________________________________
void HggGravTLS::Streamer(TBuffer &R__b)
{
   // Stream an object of class HggGravTLS.

   UInt_t R__s, R__c;
   if (R__b.IsReading()) {
      Version_t R__v = R__b.ReadVersion(&R__s, &R__c); if (R__v) { }
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      mG.Streamer(R__b);
      GkM.Streamer(R__b);
      R__b >> cme;
      R__b.CheckByteCount(R__s, R__c, HggGravTLS::IsA());
   } else {
      R__c = R__b.WriteVersion(HggGravTLS::IsA(), kTRUE);
      RooAbsPdf::Streamer(R__b);
      x.Streamer(R__b);
      mG.Streamer(R__b);
      GkM.Streamer(R__b);
      R__b << cme;
      R__b.SetByteCount(R__c, kTRUE);
   }
}

namespace ROOT {
   // Wrappers around operator new
   static void *new_HggGravTLS(void *p) {
      return  p ? new(p) ::HggGravTLS : new ::HggGravTLS;
   }
   static void *newArray_HggGravTLS(Long_t nElements, void *p) {
      return p ? new(p) ::HggGravTLS[nElements] : new ::HggGravTLS[nElements];
   }
   // Wrapper around operator delete
   static void delete_HggGravTLS(void *p) {
      delete ((::HggGravTLS*)p);
   }
   static void deleteArray_HggGravTLS(void *p) {
      delete [] ((::HggGravTLS*)p);
   }
   static void destruct_HggGravTLS(void *p) {
      typedef ::HggGravTLS current_t;
      ((current_t*)p)->~current_t();
   }
   // Wrapper around a custom streamer member function.
   static void streamer_HggGravTLS(TBuffer &buf, void *obj) {
      ((::HggGravTLS*)obj)->::HggGravTLS::Streamer(buf);
   }
} // end of namespace ROOT for class ::HggGravTLS

namespace Hfitter {
//______________________________________________________________________________
void HggFactGravLSPdfBuilder::Streamer(TBuffer &R__b)
{
   // Stream an object of class Hfitter::HggFactGravLSPdfBuilder.

   if (R__b.IsReading()) {
      R__b.ReadClassBuffer(Hfitter::HggFactGravLSPdfBuilder::Class(),this);
   } else {
      R__b.WriteClassBuffer(Hfitter::HggFactGravLSPdfBuilder::Class(),this);
   }
}

} // namespace Hfitter
namespace ROOT {
   // Wrappers around operator new
   static void *new_HfittercLcLHggFactGravLSPdfBuilder(void *p) {
      return  p ? new(p) ::Hfitter::HggFactGravLSPdfBuilder : new ::Hfitter::HggFactGravLSPdfBuilder;
   }
   static void *newArray_HfittercLcLHggFactGravLSPdfBuilder(Long_t nElements, void *p) {
      return p ? new(p) ::Hfitter::HggFactGravLSPdfBuilder[nElements] : new ::Hfitter::HggFactGravLSPdfBuilder[nElements];
   }
   // Wrapper around operator delete
   static void delete_HfittercLcLHggFactGravLSPdfBuilder(void *p) {
      delete ((::Hfitter::HggFactGravLSPdfBuilder*)p);
   }
   static void deleteArray_HfittercLcLHggFactGravLSPdfBuilder(void *p) {
      delete [] ((::Hfitter::HggFactGravLSPdfBuilder*)p);
   }
   static void destruct_HfittercLcLHggFactGravLSPdfBuilder(void *p) {
      typedef ::Hfitter::HggFactGravLSPdfBuilder current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class ::Hfitter::HggFactGravLSPdfBuilder

namespace ROOT {
   static TClass *vectorlETStringgR_Dictionary();
   static void vectorlETStringgR_TClassManip(TClass*);
   static void *new_vectorlETStringgR(void *p = 0);
   static void *newArray_vectorlETStringgR(Long_t size, void *p);
   static void delete_vectorlETStringgR(void *p);
   static void deleteArray_vectorlETStringgR(void *p);
   static void destruct_vectorlETStringgR(void *p);

   // Function generating the singleton type initializer
   static TGenericClassInfo *GenerateInitInstanceLocal(const vector<TString>*)
   {
      vector<TString> *ptr = 0;
      static ::TVirtualIsAProxy* isa_proxy = new ::TIsAProxy(typeid(vector<TString>));
      static ::ROOT::TGenericClassInfo 
         instance("vector<TString>", -2, "vector", 214,
                  typeid(vector<TString>), ::ROOT::Internal::DefineBehavior(ptr, ptr),
                  &vectorlETStringgR_Dictionary, isa_proxy, 0,
                  sizeof(vector<TString>) );
      instance.SetNew(&new_vectorlETStringgR);
      instance.SetNewArray(&newArray_vectorlETStringgR);
      instance.SetDelete(&delete_vectorlETStringgR);
      instance.SetDeleteArray(&deleteArray_vectorlETStringgR);
      instance.SetDestructor(&destruct_vectorlETStringgR);
      instance.AdoptCollectionProxyInfo(TCollectionProxyInfo::Generate(TCollectionProxyInfo::Pushback< vector<TString> >()));
      return &instance;
   }
   // Static variable to force the class initialization
   static ::ROOT::TGenericClassInfo *_R__UNIQUE_DICT_(Init) = GenerateInitInstanceLocal((const vector<TString>*)0x0); R__UseDummy(_R__UNIQUE_DICT_(Init));

   // Dictionary for non-ClassDef classes
   static TClass *vectorlETStringgR_Dictionary() {
      TClass* theClass =::ROOT::GenerateInitInstanceLocal((const vector<TString>*)0x0)->GetClass();
      vectorlETStringgR_TClassManip(theClass);
   return theClass;
   }

   static void vectorlETStringgR_TClassManip(TClass* ){
   }

} // end of namespace ROOT

namespace ROOT {
   // Wrappers around operator new
   static void *new_vectorlETStringgR(void *p) {
      return  p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString> : new vector<TString>;
   }
   static void *newArray_vectorlETStringgR(Long_t nElements, void *p) {
      return p ? ::new((::ROOT::Internal::TOperatorNewHelper*)p) vector<TString>[nElements] : new vector<TString>[nElements];
   }
   // Wrapper around operator delete
   static void delete_vectorlETStringgR(void *p) {
      delete ((vector<TString>*)p);
   }
   static void deleteArray_vectorlETStringgR(void *p) {
      delete [] ((vector<TString>*)p);
   }
   static void destruct_vectorlETStringgR(void *p) {
      typedef vector<TString> current_t;
      ((current_t*)p)->~current_t();
   }
} // end of namespace ROOT for class vector<TString>

namespace {
  void TriggerDictionaryInitialization_HGGfitterCINT_Impl() {
    static const char* headers[] = {
"HfitterModels/HftTemplatePdfBuilder.h",
"HGGfitter/MggBkgPdf.h",
"HGGfitter/MggBiasBkgPdf.h",
"HGGfitter/HggCBShape.h",
"HGGfitter/HggLognormal.h",
"HGGfitter/HggTwoSidedCBPdf.h",
"HGGfitter/HggTwoSidedNovoPdf.h",
"HGGfitter/HggNovoCBPdf.h",
"HGGfitter/HggBifurCBPdf.h",
"HGGfitter/HggStitchedPlateauPdf.h",
"HGGfitter/HggBernstein.h",
"HGGfitter/HggExpPowerPdf.h",
"HGGfitter/HggPowerLaw.h",
"HGGfitter/HggExpPoly.h",
"HGGfitter/RooRelBreitWignerPwave.h",
"HGGfitter/RooRelBreitWignerPwave_LS.h",
"HGGfitter/RooRelBreitWignerPwave_LSCut.h",
"HGGfitter/HggMG5TLS.h",
"HGGfitter/HggSigMggPdfBuilder.h",
"HGGfitter/HggBkgMggPdfBuilder.h",
"HGGfitter/HggBkgMggDoubleExpPdfBuilder.h",
"HGGfitter/HggBkgMggExpPowerPdfBuilder.h",
"HGGfitter/HggBkgMggBiasPdfBuilder.h",
"HGGfitter/HggBkgMggPolyPdfBuilder.h",
"HGGfitter/HggBernsteinPdfBuilder.h",
"HGGfitter/HggChebyshevPdfBuilder.h",
"HGGfitter/HggBkgMggBernstein4PdfBuilder.h",
"HGGfitter/HggBkgMggBernstein5PdfBuilder.h",
"HGGfitter/HggBkgMggLaurent4PdfBuilder.h",
"HGGfitter/HggBkgMggPower2PdfBuilder.h",
"HGGfitter/HggExpPolyPdfBuilder.h",
"HGGfitter/HggPolyPdfBuilder.h",
"HGGfitter/HggGenericBuilder.h",
"HGGfitter/HggEliLandauExpBuilder.h",
"HGGfitter/HggSigCosThStarPdfBuilder.h",
"HGGfitter/HggBkgCosThStarPdfBuilder.h",
"HGGfitter/HggSigPTPdfBuilder.h",
"HGGfitter/HggBkgPTPdfBuilder.h",
"HGGfitter/HggSigMggCosThStarPdfBuilder.h",
"HGGfitter/HggBkgMggCosThStarPdfBuilder.h",
"HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h",
"HGGfitter/HggBkgMggPTKeysPdfBuilder.h",
"HGGfitter/HggSigMggPTPdfBuilder.h",
"HGGfitter/HggBkgMggPTPdfBuilder.h",
"HGGfitter/HggSigMggCosThStarPTPdfBuilder.h",
"HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h",
"HGGfitter/HggSigMggSmearMPdfBuilder.h",
"HGGfitter/HggBkgMggSmearMPdfBuilder.h",
"HGGfitter/HggExpPolySmearMPdfBuilder.h",
"HGGfitter/HggBernsteinSmearMPdfBuilder.h",
"HGGfitter/HggIsolPdfBuilder.h",
"HGGfitter/HggSigMggIsolPdfBuilder.h",
"HGGfitter/HggSigMggIsolHistoBuilder.h",
"HGGfitter/HggBkgMggIsolPdfBuilder.h",
"HGGfitter/HggBkgMggIsolHistoBuilder.h",
"HGGfitter/HggBkgMggDiffIsolHistoBuilder.h",
"HGGfitter/HggBkgMggIsolBernsteinHistoBuilder.h",
"HGGfitter/HggBkgMggDiffIsolBernsteinHistoBuilder.h",
"HGGfitter/HggBkgMggIsolExpPolyHistoBuilder.h",
"HGGfitter/HggBkgMggDiffIsolExpPolyHistoBuilder.h",
"HGGfitter/HggBkgMggIsol2DHistoBuilder.h",
"HGGfitter/HggBkgMggIsolKeysPdfBuilder.h",
"HGGfitter/HggSigMggIsolBremPdfBuilder.h",
"HGGfitter/HggBkgMggIsolBremPdfBuilder.h",
"HGGfitter/HggZeeMassPdfBuilder.h",
"HGGfitter/HggZeeNovoMassPdfBuilder.h",
"HGGfitter/HggZeeBifurMassPdfBuilder.h",
"HGGfitter/HggTwoSidedNovoPdfBuilder.h",
"HGGfitter/Hgg2sCBPdfBuilder.h",
"HGGfitter/HggBWx2sCBPdfBuilder.h",
"HGGfitter/HggCouplings.h",
"HGGfitter/HggCouplingsBuilder.h",
"HGGfitter/HggTools.h",
"HGGfitter/HggEtaCatHelper.h",
"HGGfitter/HggResolHelper.h",
"HGGfitter/HggZeeIsolStudy.h",
"HGGfitter/HggNewResonanceStudy.h",
"HGGfitter/HggNLLVar.h",
"HGGfitter/HggPowHegLSPdfBuilder.h",
"HGGfitter/HggMG5LSPdfBuilder.h",
"HGGfitter/HggGravitonLineShapePdf.hh",
"HGGfitter/HggGravLSPdfBuilder.h",
"HGGfitter/HggGravTLS.h",
"HGGfitter/HggFactGravLSPdfBuilder.h",
0
    };
    static const char* includePaths[] = {
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/HGGfitter/Root",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/HGGfitter",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/Fitters/RootCore/include",
"/cvmfs/sft.cern.ch/lcg/releases/LCG_94/ROOT/6.14.04/x86_64-slc6-gcc62-opt/include",
"/cvmfs/sft.cern.ch/lcg/releases/ROOT/6.14.04-0d8dc/x86_64-slc6-gcc62-opt/include",
"/afs/cern.ch/work/a/ahadef/public/HGGFitter/HGGfitter/cmt/",
0
    };
    static const char* fwdDeclCode = R"DICTFWDDCLS(
#line 1 "HGGfitterCINT dictionary forward declarations' payload"
#pragma clang diagnostic ignored "-Wkeyword-compat"
#pragma clang diagnostic ignored "-Wignored-attributes"
#pragma clang diagnostic ignored "-Wreturn-type-c-linkage"
extern int __Cling_Autoloading_Map;
class __attribute__((annotate("$clingAutoload$HGGfitter/HggTwoSidedCBPdf.h")))  HggTwoSidedCBPdf;
namespace Hfitter{template <class T, unsigned int NPAR> class __attribute__((annotate("$clingAutoload$HfitterModels/HftTemplatePdfBuilder.h")))  HftTemplatePdfBuilder;
}
class __attribute__((annotate("$clingAutoload$RooExponential.h")))  __attribute__((annotate("$clingAutoload$HGGfitter/HggCBShape.h")))  RooExponential;
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(log-normal PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggLognormal.h")))  __attribute__((annotate("$clingAutoload$HGGfitter/HggCBShape.h")))  HggLognormal;}
class __attribute__((annotate("$clingAutoload$RooLognormal.h")))  __attribute__((annotate("$clingAutoload$HGGfitter/HggCBShape.h")))  RooLognormal;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/MggBkgPdf.h")))  MggBkgPdf;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/MggBiasBkgPdf.h")))  MggBiasBkgPdf;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(modified log-normal PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggLognormal.h")))  __attribute__((annotate("$clingAutoload$HGGfitter/HggCBShape.h")))  HggModifiedLognormal;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggCBShape.h")))  HggCBShape;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggTwoSidedNovoPdf.h")))  HggTwoSidedNovoPdf;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggNovoCBPdf.h")))  HggNovoCBPdf;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggBifurCBPdf.h")))  HggBifurCBPdf;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggStitchedPlateauPdf.h")))  HggStitchedPlateauPdf;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(Bernstein polynomial PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggBernstein.h")))  HggBernstein;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggExpPowerPdf.h")))  HggExpPowerPdf;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggPowerLaw.h")))  HggPowerLaw;}
namespace Hfitter{class __attribute__((annotate(R"ATTRDUMP(Bernstein polynomial PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggExpPoly.h")))  HggExpPoly;}
class __attribute__((annotate(R"ATTRDUMP(Breit Wigner PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/RooRelBreitWignerPwave.h")))  RooRelBreitWignerPwave;
class __attribute__((annotate(R"ATTRDUMP(Breit Wigner PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/RooRelBreitWignerPwave_LS.h")))  RooRelBreitWignerPwave_LS;
class __attribute__((annotate(R"ATTRDUMP(Breit Wigner PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/RooRelBreitWignerPwave_LSCut.h")))  RooRelBreitWignerPwave_LSCut;
class __attribute__((annotate(R"ATTRDUMP(Breit Wigner PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggMG5TLS.h")))  HggMG5TLS;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggPdfBuilder.h")))  HggSigMggPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggPdfBuilder.h")))  HggBkgMggPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggDoubleExpPdfBuilder.h")))  HggBkgMggDoubleExpPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggExpPowerPdfBuilder.h")))  HggBkgMggExpPowerPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggBiasPdfBuilder.h")))  HggBkgMggBiasPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggPolyPdfBuilder.h")))  HggBkgMggPolyPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBernsteinPdfBuilder.h")))  HggBernsteinPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggChebyshevPdfBuilder.h")))  HggChebyshevPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggBernstein4PdfBuilder.h")))  HggBkgMggBernstein4PdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggBernstein5PdfBuilder.h")))  HggBkgMggBernstein5PdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggLaurent4PdfBuilder.h")))  HggBkgMggLaurent4PdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggPower2PdfBuilder.h")))  HggBkgMggPower2PdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggExpPolyPdfBuilder.h")))  HggExpPolyPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggPolyPdfBuilder.h")))  HggPolyPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggGenericBuilder.h")))  HggGenericBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggEliLandauExpBuilder.h")))  HggEliLandauExpBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigCosThStarPdfBuilder.h")))  HggSigCosThStarPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgCosThStarPdfBuilder.h")))  HggBkgCosThStarPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigPTPdfBuilder.h")))  HggSigPTPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgPTPdfBuilder.h")))  HggBkgPTPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggCosThStarPdfBuilder.h")))  HggSigMggCosThStarPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggCosThStarPdfBuilder.h")))  HggBkgMggCosThStarPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h")))  HggBkgMggCosThStarKeysPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggPTKeysPdfBuilder.h")))  HggBkgMggPTKeysPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggPTPdfBuilder.h")))  HggSigMggPTPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggPTPdfBuilder.h")))  HggBkgMggPTPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggCosThStarPTPdfBuilder.h")))  HggSigMggCosThStarPTPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h")))  HggBkgMggCosThStarPTPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggSmearMPdfBuilder.h")))  HggSigMggSmearMPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggSmearMPdfBuilder.h")))  HggBkgMggSmearMPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggExpPolySmearMPdfBuilder.h")))  HggExpPolySmearMPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBernsteinSmearMPdfBuilder.h")))  HggBernsteinSmearMPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggIsolPdfBuilder.h")))  HggIsolPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggIsolPdfBuilder.h")))  HggSigMggIsolPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggIsolHistoBuilder.h")))  HggSigMggIsolHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsolPdfBuilder.h")))  HggBkgMggIsolPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsolHistoBuilder.h")))  HggBkgMggIsolHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggDiffIsolHistoBuilder.h")))  HggBkgMggDiffIsolHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsolBernsteinHistoBuilder.h")))  HggBkgMggIsolBernsteinHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggDiffIsolBernsteinHistoBuilder.h")))  HggBkgMggDiffIsolBernsteinHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsolExpPolyHistoBuilder.h")))  HggBkgMggIsolExpPolyHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggDiffIsolExpPolyHistoBuilder.h")))  HggBkgMggDiffIsolExpPolyHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsol2DHistoBuilder.h")))  HggBkgMggIsol2DHistoBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsolKeysPdfBuilder.h")))  HggBkgMggIsolKeysPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggSigMggIsolBremPdfBuilder.h")))  HggSigMggIsolBremPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBkgMggIsolBremPdfBuilder.h")))  HggBkgMggIsolBremPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggZeeMassPdfBuilder.h")))  HggZeeMassPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggZeeNovoMassPdfBuilder.h")))  HggZeeNovoMassPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggZeeBifurMassPdfBuilder.h")))  HggZeeBifurMassPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggTwoSidedNovoPdfBuilder.h")))  HggTwoSidedNovoPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/Hgg2sCBPdfBuilder.h")))  Hgg2sCBPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggBWx2sCBPdfBuilder.h")))  HggBWx2sCBPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggCouplings.h")))  HggCouplings;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggCouplingsBuilder.h")))  HggCouplingsBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggTools.h")))  HggTools;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggEtaCatHelper.h")))  HggEtaCatHelper;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggResolHelper.h")))  HggResolHelper;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggZeeIsolStudy.h")))  HggZeeIsolStudy;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggNewResonanceStudy.h")))  HggNewResonanceStudy;}
class __attribute__((annotate(R"ATTRDUMP(Function representing (extended) -log(L) of p.d.f and dataset)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggNLLVar.h")))  HggNLLVar;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggPowHegLSPdfBuilder.h")))  HggPowHegLSPdfBuilder;}
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggMG5LSPdfBuilder.h")))  HggMG5LSPdfBuilder;}
class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggGravitonLineShapePdf.hh")))  HggGravitonLineShapePdf;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggGravLSPdfBuilder.h")))  HggGravLSPdfBuilder;}
class __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate(R"ATTRDUMP(Crystal Ball lineshape PDF)ATTRDUMP"))) __attribute__((annotate("$clingAutoload$HGGfitter/HggGravTLS.h")))  HggGravTLS;
namespace Hfitter{class __attribute__((annotate("$clingAutoload$HGGfitter/HggFactGravLSPdfBuilder.h")))  HggFactGravLSPdfBuilder;}
)DICTFWDDCLS";
    static const char* payloadCode = R"DICTPAYLOAD(
#line 1 "HGGfitterCINT dictionary payload"

#ifndef G__VECTOR_HAS_CLASS_ITERATOR
  #define G__VECTOR_HAS_CLASS_ITERATOR 1
#endif
#ifndef ROOTCORE
  #define ROOTCORE 1
#endif
#ifndef ROOTCORE_PACKAGE
  #define ROOTCORE_PACKAGE "HGGfitter"
#endif

#define _BACKWARD_BACKWARD_WARNING_H
#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "HGGfitter/MggBkgPdf.h"
#include "HGGfitter/MggBiasBkgPdf.h"
#include "HGGfitter/HggCBShape.h"
#include "HGGfitter/HggLognormal.h"
#include "HGGfitter/HggTwoSidedCBPdf.h"
#include "HGGfitter/HggTwoSidedNovoPdf.h"
#include "HGGfitter/HggNovoCBPdf.h"
#include "HGGfitter/HggBifurCBPdf.h"
#include "HGGfitter/HggStitchedPlateauPdf.h"
#include "HGGfitter/HggBernstein.h"
#include "HGGfitter/HggExpPowerPdf.h"
#include "HGGfitter/HggPowerLaw.h"
#include "HGGfitter/HggExpPoly.h"
#include "HGGfitter/RooRelBreitWignerPwave.h"
#include "HGGfitter/RooRelBreitWignerPwave_LS.h"
#include "HGGfitter/RooRelBreitWignerPwave_LSCut.h"
#include "HGGfitter/HggMG5TLS.h"
#include "HGGfitter/HggSigMggPdfBuilder.h"
#include "HGGfitter/HggBkgMggPdfBuilder.h"
#include "HGGfitter/HggBkgMggDoubleExpPdfBuilder.h"
#include "HGGfitter/HggBkgMggExpPowerPdfBuilder.h"
#include "HGGfitter/HggBkgMggBiasPdfBuilder.h"
#include "HGGfitter/HggBkgMggPolyPdfBuilder.h"
#include "HGGfitter/HggBernsteinPdfBuilder.h"
#include "HGGfitter/HggChebyshevPdfBuilder.h"
#include "HGGfitter/HggBkgMggBernstein4PdfBuilder.h"
#include "HGGfitter/HggBkgMggBernstein5PdfBuilder.h"
#include "HGGfitter/HggBkgMggLaurent4PdfBuilder.h"
#include "HGGfitter/HggBkgMggPower2PdfBuilder.h"
#include "HGGfitter/HggExpPolyPdfBuilder.h"
#include "HGGfitter/HggPolyPdfBuilder.h"
#include "HGGfitter/HggGenericBuilder.h"
#include "HGGfitter/HggEliLandauExpBuilder.h"
#include "HGGfitter/HggSigCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgCosThStarPdfBuilder.h"
#include "HGGfitter/HggSigPTPdfBuilder.h"
#include "HGGfitter/HggBkgPTPdfBuilder.h"
#include "HGGfitter/HggSigMggCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarKeysPdfBuilder.h"
#include "HGGfitter/HggBkgMggPTKeysPdfBuilder.h"
#include "HGGfitter/HggSigMggPTPdfBuilder.h"
#include "HGGfitter/HggBkgMggPTPdfBuilder.h"
#include "HGGfitter/HggSigMggCosThStarPTPdfBuilder.h"
#include "HGGfitter/HggBkgMggCosThStarPTPdfBuilder.h"
#include "HGGfitter/HggSigMggSmearMPdfBuilder.h"
#include "HGGfitter/HggBkgMggSmearMPdfBuilder.h"
#include "HGGfitter/HggExpPolySmearMPdfBuilder.h"
#include "HGGfitter/HggBernsteinSmearMPdfBuilder.h"
#include "HGGfitter/HggIsolPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolPdfBuilder.h"
#include "HGGfitter/HggBkgMggIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolBernsteinHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolBernsteinHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolExpPolyHistoBuilder.h"
#include "HGGfitter/HggBkgMggDiffIsolExpPolyHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsol2DHistoBuilder.h"
#include "HGGfitter/HggBkgMggIsolKeysPdfBuilder.h"
#include "HGGfitter/HggSigMggIsolBremPdfBuilder.h"
#include "HGGfitter/HggBkgMggIsolBremPdfBuilder.h"
#include "HGGfitter/HggZeeMassPdfBuilder.h"
#include "HGGfitter/HggZeeNovoMassPdfBuilder.h"
#include "HGGfitter/HggZeeBifurMassPdfBuilder.h"
#include "HGGfitter/HggTwoSidedNovoPdfBuilder.h"
#include "HGGfitter/Hgg2sCBPdfBuilder.h"
#include "HGGfitter/HggBWx2sCBPdfBuilder.h"
#include "HGGfitter/HggCouplings.h"
#include "HGGfitter/HggCouplingsBuilder.h"
#include "HGGfitter/HggTools.h"
#include "HGGfitter/HggEtaCatHelper.h"
#include "HGGfitter/HggResolHelper.h"
#include "HGGfitter/HggZeeIsolStudy.h"
#include "HGGfitter/HggNewResonanceStudy.h"
#include "HGGfitter/HggNLLVar.h"
#include "HGGfitter/HggPowHegLSPdfBuilder.h"
#include "HGGfitter/HggMG5LSPdfBuilder.h"
#include "HGGfitter/HggGravitonLineShapePdf.hh"
#include "HGGfitter/HggGravLSPdfBuilder.h"
#include "HGGfitter/HggGravTLS.h"
#include "HGGfitter/HggFactGravLSPdfBuilder.h"

#undef  _BACKWARD_BACKWARD_WARNING_H
)DICTPAYLOAD";
    static const char* classesHeaders[]={
"Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,2>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<Hfitter::HggLognormal,3>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<HggTwoSidedCBPdf,6>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<RooExponential,1>", payloadCode, "@",
"Hfitter::HftTemplatePdfBuilder<RooLognormal,2>", payloadCode, "@",
"Hfitter::Hgg2sCBPdfBuilder", payloadCode, "@",
"Hfitter::HggBWx2sCBPdfBuilder", payloadCode, "@",
"Hfitter::HggBernstein", payloadCode, "@",
"Hfitter::HggBernsteinPdfBuilder", payloadCode, "@",
"Hfitter::HggBernsteinSmearMPdfBuilder", payloadCode, "@",
"Hfitter::HggBifurCBPdf", payloadCode, "@",
"Hfitter::HggBkgCosThStarPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggBernstein4PdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggBernstein5PdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggBiasPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggCosThStarKeysPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggCosThStarPTPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggCosThStarPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggDiffIsolBernsteinHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggDiffIsolExpPolyHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggDiffIsolHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggDoubleExpPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggExpPowerPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsol2DHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsolBernsteinHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsolBremPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsolExpPolyHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsolHistoBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsolKeysPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggIsolPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggLaurent4PdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggPTKeysPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggPTPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggPolyPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggPower2PdfBuilder", payloadCode, "@",
"Hfitter::HggBkgMggSmearMPdfBuilder", payloadCode, "@",
"Hfitter::HggBkgPTPdfBuilder", payloadCode, "@",
"Hfitter::HggCBShape", payloadCode, "@",
"Hfitter::HggChebyshevPdfBuilder", payloadCode, "@",
"Hfitter::HggCouplings", payloadCode, "@",
"Hfitter::HggCouplingsBuilder", payloadCode, "@",
"Hfitter::HggEliLandauExpBuilder", payloadCode, "@",
"Hfitter::HggEtaCatHelper", payloadCode, "@",
"Hfitter::HggExpPoly", payloadCode, "@",
"Hfitter::HggExpPolyPdfBuilder", payloadCode, "@",
"Hfitter::HggExpPolySmearMPdfBuilder", payloadCode, "@",
"Hfitter::HggExpPowerPdf", payloadCode, "@",
"Hfitter::HggFactGravLSPdfBuilder", payloadCode, "@",
"Hfitter::HggGenericBuilder", payloadCode, "@",
"Hfitter::HggGravLSPdfBuilder", payloadCode, "@",
"Hfitter::HggIsolPdfBuilder", payloadCode, "@",
"Hfitter::HggLognormal", payloadCode, "@",
"Hfitter::HggMG5LSPdfBuilder", payloadCode, "@",
"Hfitter::HggModifiedLognormal", payloadCode, "@",
"Hfitter::HggNewResonanceStudy", payloadCode, "@",
"Hfitter::HggNovoCBPdf", payloadCode, "@",
"Hfitter::HggPolyPdfBuilder", payloadCode, "@",
"Hfitter::HggPowHegLSPdfBuilder", payloadCode, "@",
"Hfitter::HggPowerLaw", payloadCode, "@",
"Hfitter::HggResolHelper", payloadCode, "@",
"Hfitter::HggSigCosThStarPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggCosThStarPTPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggCosThStarPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggIsolBremPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggIsolHistoBuilder", payloadCode, "@",
"Hfitter::HggSigMggIsolPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggPTPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggPdfBuilder", payloadCode, "@",
"Hfitter::HggSigMggSmearMPdfBuilder", payloadCode, "@",
"Hfitter::HggSigPTPdfBuilder", payloadCode, "@",
"Hfitter::HggStitchedPlateauPdf", payloadCode, "@",
"Hfitter::HggTools", payloadCode, "@",
"Hfitter::HggTwoSidedNovoPdf", payloadCode, "@",
"Hfitter::HggTwoSidedNovoPdfBuilder", payloadCode, "@",
"Hfitter::HggZeeBifurMassPdfBuilder", payloadCode, "@",
"Hfitter::HggZeeIsolStudy", payloadCode, "@",
"Hfitter::HggZeeMassPdfBuilder", payloadCode, "@",
"Hfitter::HggZeeNovoMassPdfBuilder", payloadCode, "@",
"Hfitter::MggBiasBkgPdf", payloadCode, "@",
"Hfitter::MggBkgPdf", payloadCode, "@",
"HggGravTLS", payloadCode, "@",
"HggGravitonLineShapePdf", payloadCode, "@",
"HggMG5TLS", payloadCode, "@",
"HggNLLVar", payloadCode, "@",
"HggTwoSidedCBPdf", payloadCode, "@",
"RooRelBreitWignerPwave", payloadCode, "@",
"RooRelBreitWignerPwave_LS", payloadCode, "@",
"RooRelBreitWignerPwave_LSCut", payloadCode, "@",
nullptr};

    static bool isInitialized = false;
    if (!isInitialized) {
      TROOT::RegisterModule("HGGfitterCINT",
        headers, includePaths, payloadCode, fwdDeclCode,
        TriggerDictionaryInitialization_HGGfitterCINT_Impl, {}, classesHeaders, /*has no C++ module*/false);
      isInitialized = true;
    }
  }
  static struct DictInit {
    DictInit() {
      TriggerDictionaryInitialization_HGGfitterCINT_Impl();
    }
  } __TheDictionaryInitializer;
}
void TriggerDictionaryInitialization_HGGfitterCINT() {
  TriggerDictionaryInitialization_HGGfitterCINT_Impl();
}
