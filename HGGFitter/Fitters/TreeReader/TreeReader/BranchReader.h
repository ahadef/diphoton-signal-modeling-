// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : BranchReader
 *
 * @brief  A concrete class to access either a simple branch or an std::vector branch.
 *         (The type is normally auto-detected.)
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_BranchReader
#define ROOT_TreeReader_BranchReader

#include "TreeReader/ReaderBase.h"
#include "TreeReader/BasicReader.h"
#include "TreeReader/StdVectorReader.h"
#include "TreeReader/ArrayReader.h"

namespace TreeReader {

  enum BranchType { Unknown, Basic, StdVector, Array, AutoDetect };

  template<class T>
    class BranchReader : public ReaderBase<T> {
    
  public:

    // Normal constructor: creates a reader member variable of type either BasicReader or
    // StdVectorReader. Which one is determined either by 'type', or by GetType() below if 
    // AutoDectect is requested (usual case)
    BranchReader(const TString& branch,  FamilyMember* parent = 0, 
                 BranchType type = AutoDetect, const BasicReader<Int_t>* index = 0,
                 int nMax = 1000)
        : ReaderBase<T>(branch, parent), m_branch(branch), 
          m_reader(0), m_type(type), m_index(index), m_nMax(nMax) { }

    BranchReader(const BranchReader& other, FamilyMember* parent = 0)
      : ReaderBase<T>(other, parent), m_branch(other.m_branch), m_reader(0),
        m_type(other.m_type), m_index(other.m_index), m_nMax(other.m_nMax) { }

    virtual ~BranchReader() { if (m_reader) { delete m_reader; m_reader = 0; } }

    FamilyMember* Clone(FamilyMember* parent = 0) const { return new BranchReader<T>(*this, parent); }
   
    bool SetupForRead(TTree* tree);
    bool SetupForWrite(TTree* tree);

    // Defer actual work to member reader
    const T& GetValue(UInt_t i = 0) const { return m_reader->GetValue(i); }
    UInt_t GetSize() const { return m_reader->GetSize(); }

    // Determine the type (vector or not) of a branch: called by constructor 
    // when AutoDetect requested.
    BranchType GetType(TTree* tree, TString branch) const;
    const ReaderBase<T>& Reader() const { return *m_reader; }
    
    void MakeReader(); 
    
    BranchType Type() const { return m_type; }
    
    bool GetReadOK()  const { return m_reader; }
    bool GetWriteOK() const { return m_reader; }
    
    void SetValue(const T& val, UInt_t i = 0) { m_reader->SetValue(val, i); };
    void SetSize (UInt_t n) { m_reader->SetSize(n); }

  private:

    TString m_branch;
    ReaderBase<T>* m_reader;
    BranchType m_type;
    const BasicReader<Int_t>* m_index;
    int m_nMax;
    
//    ClassDef(BranchReader, 0)
  };
}

template<class T>
void TreeReader::BranchReader<T>::MakeReader()
{
  // Build reader of the right type
  if (m_type == Basic)
    m_reader = new BasicReader<T>(m_branch, this);
  else if (m_type == StdVector)
    m_reader = new StdVectorReader<T>(m_branch, this);
  else if (m_type == Array) {
    if (m_index)
      m_reader = new ArrayReader<T>(m_branch, *m_index, m_nMax, this);
    else {
      std::cout << "ERROR : Branch " << m_branch << " is of Array type, so an index must be provided" << std::endl;
      m_reader = 0;
    }
  }
  else {
    std::cout << "ERROR : Unknown access type for branch " << m_branch << std::endl;
    m_reader = 0;
  }
}


template<class T>
bool TreeReader::BranchReader<T>::SetupForRead(TTree* tree)
{
  // If AutoDetect, use GetType() to figure out type
  if (m_type == AutoDetect) m_type = GetType(tree, m_branch);
  if (!m_reader) MakeReader();

  return true;
}

template<class T>
bool TreeReader::BranchReader<T>::SetupForWrite(TTree*)
{
  if (m_type == Unknown) {
    cout << "ERROR : Cannot set up branch " << m_branch << " for writing since no type (Basic, Array or StdVector) was specified" << endl;
    return 0;
  }
  if (m_type == AutoDetect) {
    cout << "INFO : using auto-detected type 'Basic' for branch " << m_branch << "." << endl;
    m_type = Basic;
  }
  
  if (!m_reader) MakeReader();
  
  return 1;
}


template<class T>
TreeReader::BranchType TreeReader::BranchReader<T>::GetType(TTree* tree, TString branch) const
{
  // Get the branch
  TBranch* bPtr = tree->GetBranch(branch);
  if (!bPtr) {
    cout << "ERROR : Branch " << branch << " not found!" << endl;
    return Unknown;
  }

  // Look for index for array branches
  TLeaf* leaf = (TLeaf*)bPtr->GetListOfLeaves()->At(0);
  if (leaf) { 
    int count;
    TLeaf* counterLeaf = leaf->GetLeafCounter(count);
    if (counterLeaf) { 
      std::cout << "INFO: branch " << branch << " determined to be of Array type" << std::endl;
      return Array;
    }
  }
    
  // Grep for "vector<". Should work in 99.99% of cases...
  TString className = bPtr->GetClassName();
  if (className.Index("vector<") >= 0) {
    std::cout << "INFO: branch " << branch << " determined to be of StdVector type" << std::endl;
    return StdVector;
  }
  std::cout << "INFO: branch " << branch << " determined to be of Basic type" << std::endl;
  return Basic;
}

#endif
