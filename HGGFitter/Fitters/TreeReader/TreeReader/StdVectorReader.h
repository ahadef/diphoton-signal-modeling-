// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : StdVectorReader
 *
 * @brief  Concrete class for readers accessing a std::vector branch
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_StdVectorReader
#define ROOT_TreeReader_StdVectorReader

#include "TreeReader/CachingReader.h"
#include "TreeReader/DataType.h"

#include <vector>
#include <string>
#include <iostream>

namespace TreeReader {
  
  template<class T>
  class StdVectorReader : public CachingReader<T> {
    
  public:
    
    // Normal constructor: need to specify the branch name
    StdVectorReader(const TString& branch,  FamilyMember* parent = 0) 
      : CachingReader<T>(branch, parent), m_branch(branch), m_vector(0) { }
    
    StdVectorReader(const StdVectorReader& other,  FamilyMember* parent = 0) 
      : CachingReader<T>(other, parent), m_branch(other.m_branch), m_vector(0) { }
    
    virtual ~StdVectorReader() { if (m_vector) delete m_vector; }

    FamilyMember* Clone(FamilyMember* parent = 0) const { return new StdVectorReader<T>(*this, parent); }

    bool SetupForRead(TTree* tree);
    bool SetupForWrite(TTree* tree);
    
    // Calculate the new value: just need to copy the i-th value from the branch.
    const T* UpdateValue(UInt_t i) const;

    TString Branch() const { return m_branch; }

    // size is the branch vector size
    UInt_t GetSize() const { return (m_vector ? m_vector->size() : 0); }
    UInt_t GetCapacity() const { return (m_vector ? m_vector->capacity() : 0); }

    void ChangeValue(UInt_t i, const T& val) { if (i < GetSize()) (*m_vector)[i] = val; };
    void SetSize (UInt_t n) { m_vector->resize(n); };

    StdVectorReader<T>& operator=(const std::vector<T>& vect) { *m_vector= vect; return *this; } ;

    bool OwnsCache() const { return false; }

#ifndef __GCCXML__
    operator const std::vector<T>& () const { return *m_vector; }
#endif
    operator std::vector<T>&() { return *m_vector; }
    const std::vector<T>& vector() const { return *m_vector; }
    
  private:

    TString m_branch;
    std::vector<T>* m_vector;

  };
  
  // A template partial specialization for std::vector types
  // We need this otherwise the dynamic_cast in operator== won't compile on std::vector<T>
  // since it is "not a polymorphic type".
  // In principle a similar structure is needed for any type that doesn't derive from TObject
  // and can't by dynamic_cast'ed to a TObject.

#ifndef __GCCXML__
  template<class T> class ReaderType< std::vector<T> > : public ReaderTypeBase< std::vector<T> > {
    
    public:
      
      ReaderType(const TString& name, const TString& index = "")
      : ReaderTypeBase< std::vector<T> >(name, index) { }
      virtual ~ReaderType() { }
      TString GetType() const { return "vector<" + BranchSpec::NormalizeType(GetPayloadType()) + ">"; }
      TString GetPayloadType() const;
  };
#endif 
}

#ifndef __GCCXML__
template<class T> 
TString TreeReader::ReaderType< std::vector<T> >::GetPayloadType() const
{
  T t;
  const TObject* obj = dynamic_cast<const TObject*>(&t);
  if (!obj) return "";
  return obj->ClassName();
}

namespace TreeReader {
template<> TString ReaderType< std::vector<UShort_t> >::GetPayloadType()  const;
template<> TString ReaderType< std::vector<Short_t> >::GetPayloadType()  const;
template<> TString ReaderType< std::vector<UInt_t> >::GetPayloadType()  const;
template<> TString ReaderType< std::vector<Int_t> >::GetPayloadType()  const;
template<> TString ReaderType< std::vector<Float_t> >::GetPayloadType()  const;
template<> TString ReaderType< std::vector<Double_t> >::GetPayloadType()  const;
template<> TString ReaderType< std::vector<std::string> >::GetPayloadType()  const;

template<> TString ReaderType< std::vector< std::vector<UShort_t> > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector<Short_t> > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector<UInt_t> > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector<Int_t> > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector<Float_t> > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector<Double_t> > >::GetPayloadType()  const;

template<> TString ReaderType< std::vector< std::vector< std::vector<UShort_t> > > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector< std::vector<Short_t> > > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector< std::vector<UInt_t> > > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector< std::vector<Int_t> > > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector< std::vector<Float_t> > > >::GetPayloadType()  const;
template<> TString ReaderType< std::vector< std::vector< std::vector<Double_t> > > >::GetPayloadType()  const;

}
#endif 

template<class T>
bool TreeReader::StdVectorReader<T>::SetupForRead(TTree* tree)
{
  if (!CachingReader<T>::SetupForRead(tree)) return false;

  // Get the branch from the tree
  TBranch* bObj = tree->GetBranch(m_branch);
  if (!bObj) {
    cout << "vector<> Branch " << m_branch << " does not exist in tree!" << endl;
    return CachingReader<T>::SetupForRead(0);
  }

  m_vector = new std::vector<T>();

  // Check if the type of the branch matches the type of T, using DataType<T>
  // use of 'this' required here since size() is defined in untemplated base class...
  if (!ReaderType< std::vector<T> >(m_branch).CheckBranch(*bObj)) return CachingReader<T>::SetupForRead(0);
  
  cout << "Setting adress for branch " << m_branch << endl;

  // Link the branch address to the member variable
  tree->SetBranchStatus(m_branch, 1);
  tree->SetBranchAddress(m_branch, &m_vector);
  
  return true;
}


template<class T>
bool TreeReader::StdVectorReader<T>::SetupForWrite(TTree* tree)
{
  m_vector = new std::vector<T>();
  cout << "Creating new vector<> branch " << m_branch << "/" << ReaderType< std::vector<T> >(m_branch).ReaderSpec().FormatString() << endl;
  //tree->Branch(m_branch, &m_vector, ReaderType< std::vector<T> >(m_branch).ReaderSpec().FormatString());
  tree->Branch(m_branch, &m_vector);
  return true;
}


template<class T>
const T* TreeReader::StdVectorReader<T>::UpdateValue(UInt_t i) const
{
  if (i < 0 || i >= m_vector->size()) return 0;
  return &((*m_vector)[i]);
}

#endif
