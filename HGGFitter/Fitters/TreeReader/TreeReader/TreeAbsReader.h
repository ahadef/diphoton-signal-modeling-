// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : TreeAbsReader
 *
 * @brief The application base class: reads a tree and performs some analysis on the result.
 *        This class is itslef implemented as a 'reader' of bool: it registers sub-readers
 *        that do the real reading and returns T/F depending on success.
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_TreeAbsReader
#define ROOT_TreeReader_TreeAbsReader

#include "TreeReader/ReaderBase.h"
#include "TTree.h"
#include "TChain.h"

namespace TreeReader {

  class TreeAbsReader : public ReaderBase<bool> {
    
  public:
    
    // Normal constructor: 
    // - tree: tree to study
    // - iBegin, nEvents: index of first event & total number of events to process in event loop
    TreeAbsReader(TTree* tree, long long nEvents = -1, long long iBegin = 0, 
                  long long originalNEvents = -1, bool writeStats = false);
      
    virtual ~TreeAbsReader() { }

    // An alias for GetValue() (it _is_ a reader, but used as an algorithm...)
    bool Run(const TString& outputFile, const TString& treeName = "");
    
    // The usual Reader GetValue, implemented using Initialize/Execute/Finalize below
    const bool& GetValue (UInt_t i = 0) const;
    
    // The usual Reader GetValue, implemented using Initialize/Execute/Finalize below
    void SetValue (const bool& result, UInt_t /*i*/ = 0) { m_result = result; }

    // Result is just 1 bool
    UInt_t GetSize() const { return 1; }
    
    bool SetupForRead (TTree*) { return 1; }
    bool SetupForWrite(TTree*) { return 1; }

    // Things to implement in concrete classes
    virtual bool Initialize() = 0;
    virtual bool Execute() = 0;
    virtual bool Finalize() = 0;

    // The current read entry in the tree
    long long GetCurrentEntry() const { return m_currentEntry; }
    
    // To set the result
    const bool& SetResult(bool result) const;
    
    TTree* GetReadTree() const { return m_readTree; }
    
    static bool ReadStats(TFile* f, long long& nOriginal, long long& nInput, long long& nProcessed, long long& nOutput);
    static TTree*  NewTree (const TString& treeName, const TString& fileName, TFile** file = 0);
    static TTree*  GetTree (const TString& treeName, const TString& fileName, TFile** file = 0);
    static TChain* GetChain(const TString& treeName, const TString& filePattern);
    static bool FilterTree(TTree& tree, const TString& filter, const TString& outputFileName, bool clobber = false);
    
    void SetPrintFreq(long long freq) { m_printFreq = freq; }
    void SetSaveFreq(long long freq) { m_saveFreq = freq; }

    static long long ReadNOriginal(const TString& fileName);
    static long long ReadNInput(const TString& fileName);
    static long long ReadNProcessed(const TString& fileName);
    static long long ReadNPassed(const TString& fileName);
    static double    ReadSelectionEff(const TString& fileName);
    
    static UInt_t Add(TChain& chain, const TString& files);
    
    static double TotalWeight(TTree* tree, const TString& cut = "", const TString& weightVar = "");
    static long long GetOriginalNEvents(TTree *tree);
    
  protected:
    
    void Fill() { m_writeTree->Fill(); }

    mutable TTree* m_readTree;
    mutable TTree* m_writeTree;
    mutable TFile* m_writeFile;
    
    long long m_begin, m_nEvents, m_printFreq, m_saveFreq, m_originalNEvents;
    bool m_writeStats;
    
    mutable long long m_currentEntry;    

    mutable bool m_result;

    private:

 //   ClassDef( TreeAbsReader, 0 )
  };
}

#endif
