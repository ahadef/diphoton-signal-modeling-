// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : BasicReader
 *
 * @brief  Concrete class for readers accessing a simple (non-vector) branch
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_BasicReader
#define ROOT_TreeReader_BasicReader

#include "TTree.h"
#include "TString.h"

#include "TreeReader/CachingReader.h"
#include "TreeReader/DataType.h"

namespace TreeReader {

  template<class T> class BasicReader : public CachingReader<T> {

    public:
      
    // Normal constructor: need to specify the branch name
      BasicReader(const TString& branch, FamilyMember* parent = 0)
      : CachingReader<T>(branch, parent), m_branch(branch) { }

      BasicReader(const BasicReader& other, FamilyMember* parent = 0)
      : CachingReader<T>(other, parent), m_branch(other.m_branch) { }

      FamilyMember* Clone(FamilyMember* parent = 0) const { return new BasicReader<T>(*this, parent); }
      
      virtual ~BasicReader() { }
      
      bool SetupForRead(TTree* tree);
      bool SetupForWrite(TTree* tree);

    // Calculate the new value: just need to copy the branch value...
      const T* UpdateValue(UInt_t) const { return &m_val; }
      
    // Accessor to the branch
      TString Branch() const { return m_branch; }
      
    // Size is always 1 (non-vector branch)
      UInt_t GetSize() const { return 1; }
      UInt_t GetCapacity() const { return 1; }

      void ChangeValue(UInt_t i, const T& val) { if (i == 0) m_val = val; };
      void SetSize (UInt_t) { };
      
      // Shortcuts
      BasicReader<T>& operator=(const T& val) { this->SetValue(val); return *this; }
      operator T() const { return CachingReader<T>::GetValue(); } 
      
      bool OwnsCache() const { return false; }
    
      
    private:
      
      T m_val;
      TString m_branch;
      
//      ClassDef(BasicReader, 0)
  };
}

template<class T>
bool TreeReader::BasicReader<T>::SetupForRead(TTree* tree)
{
  if (!CachingReader<T>::SetupForRead(tree)) return false;
   
  // Get the branch from the tree
  TBranch* bObj = tree->GetBranch(m_branch);
  if (!bObj) {
    cout << "ERROR : Branch " << m_branch << " does not exist in tree!" << endl;
    return CachingReader<T>::SetupForRead(0);
  }
  
  if (bObj->GetAddress()) {
    cout << "WARNING : Branch " << m_branch << " has already an address set! Check for other readers on the same branch." << endl;
    return CachingReader<T>::SetupForRead(0);
  }

  // Check if the type of the branch matches the type of T, using DataType<T>
  // use of 'this' required here since size() is defined in untemplated base class...
  if (!ReaderType<T>(m_branch).CheckBranch(*bObj)) return CachingReader<T>::SetupForRead(0);
  
  cout << "Setting adress for branch " << m_branch << endl;
  
  // Link the branch address to the member variable
  tree->SetBranchStatus(m_branch, 1);
  tree->SetBranchAddress(m_branch, &m_val);
  
  return true;
}

template<class T>
bool TreeReader::BasicReader<T>::SetupForWrite(TTree* tree)
{
  if (m_branch == "") {
    cout << "ERROR : cannot setup for write a reader with no name" << endl;
    return false;
  }
  if (!CachingReader<T>::SetupForWrite(tree)) return false;
  
  cout << "Creating new branch " << m_branch << endl;
  tree->Branch(m_branch, &m_val);
  return true;
}

#endif
