// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : Merger
 *
 * @brief The application base class: reads a tree and performs some analysis on the result.
 *        This class is itslef implemented as a 'reader' of bool: it registers sub-readers
 *        that do the real reading and returns T/F depending on success.
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_Merger
#define ROOT_TreeReader_Merger

#include "TreeReader/ReaderBase.h"
#include "TFile.h"
#include "TTree.h"

#include <vector>
#include <iostream>

namespace TreeReader {

  template<class T>
  class Merger {
    
  public:
    
    Merger() { }      
    virtual ~Merger() { }

    bool Add(const TString& fileName, const TString& treeName);
    void Add(TTree* tree) { m_inputTrees.push_back(tree); }

    bool Run(const TString& outputFile, const TString& treeName) const;
    
    virtual bool Tweak(T& reader, UInt_t treeIndex, long long entry) const { return true; }
    
  protected:
    
    std::vector<TTree*> m_inputTrees;
    
    private:

//    ClassDef( Merger, 0 )
  };
  
  template<class T>
  bool Merger<T>::Add(const TString& fileName, const TString& treeName)
  {
    TFile* f = TFile::Open(fileName);
    if (!f) return false;
    TTree* t = (TTree*)f->Get(treeName);
    if (!t) return false;
    Add(t);
    return true;
  }


  template<class T>
  bool Merger<T>::Run(const TString& outputFile, const TString& treeName) const
  {
    TFile* writeFile = TFile::Open(outputFile, "RECREATE");    
    TTree* writeTree = new TTree(treeName, "");
    T reader;
    reader.WriteTo(writeTree);
    long long allEntries = 0;
    UInt_t treeIndex = 0;
    for (std::vector<TTree*>::const_iterator inputTree = m_inputTrees.begin(); inputTree != m_inputTrees.end(); inputTree++, treeIndex++) {      
      reader.ReadFrom(*inputTree);
      TString name = (*inputTree)->GetName();
      if ((*inputTree)->GetDirectory()) name = (*inputTree)->GetDirectory()->GetName() + ("/" + name);
      for (long long k = 0; k < (*inputTree)->GetEntries(); k++, allEntries++) {
        if ((k % 10000) == 0) 
          std::cout << "Processing tree " << name << ", event " << k << "/" << (*inputTree)->GetEntries() << std::endl;
        (*inputTree)->GetEntry(k);
        if (!Tweak(reader, treeIndex, k)) continue;
        writeTree->Fill();
      }
    }
    writeFile->cd();
    writeTree->Write();
    writeFile->Close();
    delete writeFile;
    return true;
  }

}

#endif
