// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : DataType
 *
 * @brief A templated class that can compare its template type to the type of a TTree branch.
 *        This is used at TTree:SetBranchAddress to check that the pointed variable has
 *        the right type for the branch.
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_DataType
#define ROOT_TreeReader_DataType

#include <vector>
#include <iostream>
#include <string>
#include "TBranch.h"
#include "TLeaf.h"

using std::cout;
using std::endl;

namespace TreeReader {

  class BranchSpec {
  
    public:
      
      BranchSpec(TBranch& branch);
      BranchSpec(const TString& name, const TString& type, const TString& index = "");
    
      bool operator==(const BranchSpec& spec) const;
      
      static TString NormalizeType(TString type);
      
      TString Name()  const { return m_name; }
      TString SafeName()  const { return m_safeName; }
      TString Type()  const { return m_type; }
      TString Index() const { return m_index; }
      
      TString FormatString() const; 
      TString Description() const; 

    private:
      
      TString m_name, m_safeName, m_type, m_index;
  };
  
  template<class T> 
  class ReaderTypeBase {

  public:
    ReaderTypeBase(const TString& name, const TString& index = "")
      : m_name(name), m_index(index) { }
    
    virtual ~ReaderTypeBase() { }
   
    // This is what is called by the readers
    bool CheckBranch(TBranch& branch, bool warn = true);
    BranchSpec ReaderSpec() const;
    virtual TString GetType() const = 0;
    
    private:
      
      TString m_name, m_index;
  };


  template<class T>
  class ReaderType : public ReaderTypeBase<T> {

  public:
    
    ReaderType(const TString& name, const TString& index = "") 
      : ReaderTypeBase<T>(name, index) { }
    virtual ~ReaderType() { }

    TString GetType() const;
  };

template<> TString ReaderType<UShort_t>::GetType() const;
template<> TString ReaderType<Short_t>::GetType() const;
template<> TString ReaderType<UInt_t>::GetType() const;
template<> TString ReaderType<Int_t>::GetType() const;
template<> TString ReaderType<Double_t>::GetType() const;
template<> TString ReaderType<Float_t>::GetType() const;
template<> TString ReaderType<Bool_t>::GetType() const;
template<> TString ReaderType<std::string>::GetType() const;

}

#ifndef __GCCXML__  
template<class T> 
TString TreeReader::ReaderType<T>::GetType() const
{
  T t;
  const TObject* obj = dynamic_cast<const TObject*>(&t);
  if (!obj) return "";
  
  return obj->ClassName();
}
#endif

template<class T> TreeReader::BranchSpec TreeReader::ReaderTypeBase<T>::ReaderSpec() const
{
  return BranchSpec(m_name, GetType(), m_index);
}

template<class T> bool TreeReader::ReaderTypeBase<T>::CheckBranch(TBranch& branch, bool warn) 
{
  BranchSpec branchSpec(branch);
  BranchSpec readerSpec = ReaderSpec();
  
  bool result = (branchSpec == readerSpec);
  
  if (!result && warn) {
    cout << "WARNING: Type of TBranch " << branchSpec.Description() << " does not match the type " 
         << readerSpec.Description() << " of the reader." << endl;
  }
  
//   if (result && warn) {
//     cout << "Type of TBranch " << branchSpec.Description() << " matches the type " 
//         << readerSpec.Description() << " of the reader." << endl;
//   }
  
  return result;
}

#endif

