// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : FamilyMember
 *
 * @brief  The base class for all Readers that cache their values (i.e. not looked up or
 *         recalculated at every call.
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/


#ifndef ROOT_TreeReader_CachingReader
#define ROOT_TreeReader_CachingReader

#include "TreeReader/ReaderBase.h"

#include <vector>
#include "TTree.h"

namespace TreeReader {

  template<class T> class CachingReader : public ReaderBase<T> 
  {
    public:

      CachingReader(const TString& name, FamilyMember* parent = 0)
      : ReaderBase<T>(name, parent), m_readTree(0), m_entry(-1), m_index(-1), m_default() { }

      CachingReader(const CachingReader& other, FamilyMember* parent = 0)
      : ReaderBase<T>(other, parent), m_readTree(0), m_entry(-1), m_index(-1), m_default() { }

      virtual ~CachingReader() { }

    // Here the value comes from the cache (calculated first if necessary)
      const T& GetValue (UInt_t i = 0) const;

    // Check if the current event in the tree is the one we have a cache for.
    // - if yes, return true
    // - if no, clear the cache & return false
      bool CheckEventInCache() const;
      
    // Check if the i-th entry (i > 0 for std::vector<> branches only) is in the cache
    // - if yes, return a ref to it
    // - if no, calculate it, store it, return a ref to it
      const T& CheckEntryInCache(UInt_t i) const;
      
    // The calculation of the return value from the branch value. For a simple reader it is
    // trivial (just pass on the branch value). For composite readers with multiple sub-readers,
    // this performs more complicated processing (see TVector3 reader)
      virtual const T* UpdateValue(UInt_t i) const = 0;
    
      // The resetting of the return value from user-input
      virtual void ChangeValue(UInt_t i, const T& val) = 0;

      // Determines if the values in the cache are owned or not. True by default, but
      // can be overriden for simple types.
      virtual bool OwnsCache() const { return true; }
      
    // The entry for which the cache is valid
      long long GetCurrentEntry() const { return m_entry; }
      
    // If value is set by hand, cache must be invalidated first
      void SetValue(const T& val, UInt_t i = 0);

    // The tree which we are reading
      TTree* GetReadTree() const { return m_readTree; }
    
      bool GetReadOK()  const { return ReaderBase<T>::GetReadOK() && m_readTree; }
      bool GetWriteOK() const { return ReaderBase<T>::GetWriteOK(); }
    
      bool SetupForRead (TTree* tree) { m_readTree = tree; return (tree != 0); }
      bool SetupForWrite(TTree*) { return true; }

      void MakeCache(UInt_t n = 0) const;
      
    protected: 

      TTree* m_readTree;

    private:

      mutable std::vector<const T*> m_vals;
      mutable long long m_entry, m_index;

      const T m_default;
  };
}

using std::cout;
using std::endl;

template<class T>
const T& TreeReader::CachingReader<T>::GetValue(UInt_t i) const 
{ 
  return CheckEntryInCache(i);
}


template<class T>
const T& TreeReader::CachingReader<T>::CheckEntryInCache(UInt_t i) const
{
  //cout << "Current cached entry " << m_entry << " " << m_index << endl;
  //cout << "Requesting " << 8m_readTree->GetReadEntry() << " " << i << endl;
  
  CheckEventInCache();
  MakeCache();
  //std::cout << "cache size " << m_vals.size() << ", i = " << i << std::endl;
  
  // use of 'this' required here since size() has no template markers
  if (i >= m_vals.size()) {
    cout << "ERROR: reader " << this->GetName() << " cannot access value at index " << i << "(size = " << m_vals.size() << ")." << endl;
    return m_default;
  }
  m_index = i;
  //if (!m_vals[i]) std::cout << "updating value" << std::endl;
  if (!m_vals[i]) m_vals[i] = UpdateValue(i);
  //if (m_vals[i]) std::cout << "return cache value" << std::endl;
  if (m_vals[i]) return *m_vals[i];
  cout << "ERROR: reader " << this->GetName() << " cannot load value at index " << i << "." << endl;
  //std::cout << "return default" << std::endl;
  return m_default;
}


template<class T>
void TreeReader::CachingReader<T>::SetValue(const T& val, UInt_t i)
{
  MakeCache(i);

  // Update the reader's internal state
  ChangeValue(i, val);

  // Update the cache, if needed
  m_index = i;
  if (OwnsCache() && m_vals[i]) { delete m_vals[i]; m_vals[i] = 0; }
  if (!m_vals[i]) m_vals[i] = UpdateValue(i);
}

template<class T>
void TreeReader::CachingReader<T>::MakeCache(UInt_t i) const
{
  if (m_index == -1) {
    UInt_t nVals = this->GetCapacity(); // so it's not recomputed at each iteration...
    for (UInt_t i = 0; i < nVals; i++) m_vals.push_back(0);
  }
  
  m_index = 0;
  
  if (i >= m_vals.size()) m_vals.resize(i + 1, 0); // insert 0's at end
}

template<class T>
bool TreeReader::CachingReader<T>::CheckEventInCache() const
{
  // If no tree (not reading), make sure the cache is created (m_index = -1) if it is not there, nothing else.
  if (!m_readTree) {
    m_index = (m_vals.size() == 0 ? -1 : -2);
    return false;
  }
  
  if (m_readTree->GetReadEntry() != m_entry) {
    
    if (OwnsCache()) 
      for (UInt_t i = 0; i < m_vals.size(); i++) delete m_vals[i];
    m_vals.clear();
    
    m_entry = m_readTree->GetReadEntry();
    m_index = -1;
    return false;
  }

  return true;
}

#endif
