// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : StdVectorReader
 *
 * @brief  Simple example of a reader of a 3-vector based on the values of 3 branches
 *         (pT, eta, phi)
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_TVector3Reader
#define ROOT_TreeReader_TVector3Reader

#include "TreeReader/BranchReader.h"
#include "TreeReader/CachingReader.h"
#include "TVector3.h"

namespace TreeReader {
  
  // C is the coordinate type (see below), T is the data type.
  template<class C, class T = float>
  class TVector3Reader  : public CachingReader<TVector3> {
    
  public:

    // Normal contructor: defines 3 sub-readers (BranchReaders, so that this class
    // works on either std::vectors or simple branches)

    TVector3Reader(const TString& p1, const TString& p2, const TString& p3,
                   FamilyMember* parent = 0,  BranchType type = AutoDetect, 
                   const BasicReader<Int_t>* index = 0) : 
      CachingReader<TVector3>("TVector3(" + p1 + ", " + p2 + ", " + p3 + ")", parent),
      m_p1(p1, this, type, index),
      m_p2(p2, this, type, index),
      m_p3(p3, this, type, index) { }

    TVector3Reader(const TVector3Reader& other, FamilyMember* parent = 0)
      : CachingReader<TVector3>(*this, parent), m_p1(other.m_p1, this), m_p2(other.m_p2, this), m_p3(other.m_p3, this) { }

    virtual ~TVector3Reader() { }

    FamilyMember* Clone(FamilyMember* parent = 0) const { return new TVector3Reader<C, T>(*this, parent); }

    // The calculation of the return valus from the branch values
    const TVector3* UpdateValue(UInt_t i) const;
    void ChangeValue (UInt_t i, const TVector3& v);

    const BranchReader<T>& P1() const { return m_p1; }
    const BranchReader<T>& P2() const { return m_p2; }
    const BranchReader<T>& P3() const { return m_p3; }

  private:

    BranchReader<T> m_p1, m_p2, m_p3;
  };

  // corrdinate classes : need to use those since ROOT doesn't seem to handle
  // templates on enums
  
  class PxPyPz {
    public:
      static void SetCoord(TVector3& v, double px, double py, double pz) { v.SetXYZ(px, py, pz); }
      static TVector3 GetCoord(const TVector3& v) { return TVector3(v.X(), v.Y(), v.Z()); }
  };
  
  class PtEtaPhi {
    public:
      static void SetCoord(TVector3& v, double pt, double eta, double phi) { v.SetPtEtaPhi(pt, eta, phi); }
      static TVector3 GetCoord(const TVector3& v) { return v.Pt() == 0 ? TVector3(0,0,0) : TVector3(v.Pt(), v.Eta(), v.Phi()); }
  };
  
  class PtPhiPz {
    public:
      static void SetCoord(TVector3& v, double r, double phi, double z) { v.SetPerp(r); v.SetPhi(phi); v.SetZ(z); }
      static TVector3 GetCoord(const TVector3& v) { return TVector3(v.Perp(), v.Phi(), v.Z()); }
  };
}


template<class C, class T>
const TVector3* TreeReader::TVector3Reader<C,T>::UpdateValue(UInt_t i) const
{ 
  // Make a new TVector3 value
  TVector3* vect = new TVector3();
  C::SetCoord(*vect, m_p1(i), m_p2(i), m_p3(i));
  
  return vect;
}

template<class C, class T>
void TreeReader::TVector3Reader<C,T>::ChangeValue(UInt_t i, const TVector3& vect)
{
  TVector3 vC = C::GetCoord(vect);
  m_p1.SetValue(vC.X(), i);
  m_p2.SetValue(vC.Y(), i);
  m_p3.SetValue(vC.Z(), i);

}

#endif
