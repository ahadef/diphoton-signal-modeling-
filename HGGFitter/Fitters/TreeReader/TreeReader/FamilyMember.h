// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : FamilyMember
 *
 * @brief All the functions of the proxies that don't require templates,
 *        i.e. everything that doesn't handle the actual data...
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/


#ifndef ROOT_TreeReader_FamilyMember
#define ROOT_TreeReader_FamilyMember

#include <vector>
#include "Rtypes.h"
#include <iostream>

class TTree;

namespace TreeReader {
  
  class FamilyMember {
    
  public:
    
    FamilyMember(FamilyMember* parent = 0) { if (parent) parent->AddChild(*this); }
    
    virtual ~FamilyMember() { }

    virtual FamilyMember* Clone(FamilyMember* /*newParent*/) const { return 0; }

    // Add a child (dependent)      
    void AddChild(FamilyMember& child) { m_children.push_back(&child); }

    // Returns false if m_ok is false or if any of the children (dependents) are not ok() - 
    // and true otherwise
    bool ReadOK() const;
    bool WriteOK() const;

    // Public methods to declare R/W functionality
    bool ReadFrom(TTree* tree);
    bool WriteTo(TTree* tree);
    
    // Accessor to the raw m_ok without recursion
    virtual bool GetReadOK()  const { return 1; }
    virtual bool GetWriteOK() const { return 1; }
    
    virtual UInt_t GetSize() const;
    virtual void SetSize(UInt_t n);

      // how much capacity to plan for by default.
    virtual UInt_t GetCapacity() const;
    
  protected: 
    
    virtual bool SetupForRead(TTree*)  { return true; };
    virtual bool SetupForWrite(TTree*) { return true; };

  private: 

    std::vector<FamilyMember*> m_children;

//    ClassDef( FamilyMember, 0 )

  };
}

#endif
