// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: HGGReader
 * @Class  : CutFlow
 *
 * @brief A concrete application for the H->gamma gamma case, derived from HggD3PDReader
 *
 * @author Nicolas Berger <Nicolas.Berger@cern.ch>
 **********************************************************************************/

#ifndef ROOT_TreeReader_CutFlow
#define ROOT_TreeReader_CutFlow

#include "TString.h"
#include "TFile.h"
#include "TTree.h"
#include <vector>

#include <iostream>
using std::cout;
using std::endl;

namespace TreeReader {

  class CutFlowCut
  {
  public:
    CutFlowCut(const TString& n, bool v = false) : name(n), verbose(v) { }
    virtual ~CutFlowCut() {}
    virtual bool Pass(UInt_t& nSeen, UInt_t& nPassed);
    virtual bool Pass() { return true; } 
    TString name;
    bool verbose;
  };

  class CutFlowAction
  {
  public:
    CutFlowAction(const TString& n) : name(n) { }
    virtual ~CutFlowAction() {}
    virtual bool Execute() { return true; }
    TString name;
  };

  struct CutFlowStep
  {
    CutFlowStep(CutFlowAction* a_action = 0, CutFlowCut* a_cut = 0) : action(a_action), cut(a_cut), nSeen(0), nPassed(0) { }
    bool Run(bool countObjects = false);
    CutFlowAction* action;
    CutFlowCut* cut;
    UInt_t nSeen, nPassed;
    bool isPassed;
  };
  
  class CutFlow
  {
  public:
    CutFlow(const TString& name, TFile* outputFile = 0, long long originalNEvents = -1);
    virtual ~CutFlow();

    void AddStep(CutFlowAction* action, CutFlowCut* cut = 0) { m_steps.push_back(CutFlowStep(action, cut)); }
    void AddStep(CutFlowCut* cut, CutFlowAction* action = 0) { m_steps.push_back(CutFlowStep(action, cut)); }
    bool Run(UInt_t runNumber = 0, UInt_t eventNumber = 0, bool countObjects = false);

    const CutFlowStep* Step(UInt_t i) const { return &m_steps[i]; }
    const CutFlowStep* Step(const TString& cutName) const;
    
    void SetVerbose(bool verbose = true);

  private:
    TString m_name;
    TFile* m_outputFile;
    TTree* m_tree;
    long long m_originalNEvents;
    UInt_t m_eventNumber, m_runNumber, m_lastCut;
    std::vector<CutFlowStep> m_steps;
    bool m_verbose;
  };
}
#endif
