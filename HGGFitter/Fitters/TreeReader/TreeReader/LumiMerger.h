// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : LumiMerger
 *
 * @brief 
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_LumiMerger
#define ROOT_TreeReader_LumiMerger

#include "TreeReader/TreeAbsReader.h"
#include "TFile.h"
#include "TTree.h"
#include "TArrayD.h"
#include "TString.h"

#include <vector>

namespace TreeReader {

  enum AddType { withLumi=0, withXSec=1, withSelectedXSec=2, withProportion=3, keepingWeight=4, AddTypes=6 };

  template<class T> 
  class LumiMerger
  {
  public:
    
    LumiMerger(double lumi, double badOQLumi = 0., double integral = 0.) : m_lumi(lumi), m_badOQLumi(badOQLumi), m_integral(integral), m_totalWeight(0.), m_sumOfParameters(0.) { };

    virtual ~LumiMerger() { }

    bool Add(const TString& fileName, const TString& treeName, double normPar, int normType, double weight = 0.);
    
    bool AddWithLumi(const TString& fileName, const TString& treeName, double lumi) { return Add(fileName, treeName, lumi, withLumi); }
    bool AddWithXSec(const TString& fileName, const TString& treeName, double xsec) { return Add(fileName, treeName, xsec, withXSec); }
    bool AddWithSelectedXSec(const TString& fileName, const TString& treeName, double xsec) { return Add(fileName, treeName, xsec, withSelectedXSec); }
    /** To re-normalize samples to get the given proportions, according to the sum of weights in each **/
    bool AddWithProportion(const TString& fileName, const TString& treeName, double prop, double weight = 0.) { return Add(fileName, treeName, prop, withProportion, weight); }
    
    virtual bool Reweigh(T& reader, double weightFactor) const = 0;
    virtual bool PassedOQMap(T& reader) const = 0;

    bool Run(const TString& outputFile, const TString& treeName) const;
    
    /** Return the weight (integral) of the sample, splitting between events that passed the OQ check and the ones that did not **/
    double GetWeight(TTree *t, const TString& weightVar) const;

  private:

    double m_lumi, m_badOQLumi, m_integral, m_totalWeight, m_sumOfParameters;
    std::vector<TTree*> m_inputTrees;
    std::vector<double> m_factors;
    
    //ClassDef( LumiMerger, 0 )
  };

  template<class T>
  bool LumiMerger<T>::Add(const TString& fileName, const TString& treeName, double normPar, int normType, double weight)
  {
    TTree* t = TreeAbsReader::GetTree(treeName, fileName);
    if (!t) {
      std::cout << "Could not find tree " << treeName << " in file " << fileName << std::endl;
      return false;
    }
    
    long long nOrig, nInit, nRead, nOut;
    if (!TreeReader::TreeAbsReader::ReadStats((TFile*)t->GetDirectory(), nOrig, nInit, nRead, nOut)) {
      std::cout << "Could not read stats from file " << fileName << std::endl;     
      return false;
    }
    double sampleLumi = 0, factor = 0;
    
    // Retrieve the integral of the sample if needed for rescaling later 
    if (m_integral != 0 || normType == withProportion)
    {
      if (weight == 0) weight = GetWeight(t, "weight"); // TODO: do not hardcode "weight"
      m_totalWeight += weight;
    }
    
    if (normType == withLumi)
      sampleLumi = normPar;
    else if (normType == withXSec)
      sampleLumi = nOrig/normPar;
    else if (normType == withSelectedXSec)
      sampleLumi = nOut/normPar;
    else if (normType == withProportion)
    {
      m_sumOfParameters += normPar;
      factor = normPar/weight;
      std::cout << "Setting proportion " << normPar << " to sample " << m_factors.size() << ", with integral " << weight << std::endl;
    }
    else
    {
      std::cout << "Invalid parameter to Add" << std::endl;
      return false;
    }
    
    if (normType != withProportion)
      factor = (sampleLumi > 0 ? m_lumi/sampleLumi : 0);
      std::cout << "Applying weight factor " << factor << " to sample " << m_factors.size() << std::endl;
    
    m_inputTrees.push_back(t);
    m_factors.push_back(factor);
    return true;
  }


  template<class T>
  bool LumiMerger<T>::Run(const TString& outputFile, const TString& treeName) const
  {
    TFile* writeFile = TFile::Open(outputFile, "RECREATE");    
    TTree* writeTree = new TTree(treeName, "");
    T reader;
    reader.WriteTo(writeTree);
    long long allEntries = 0;
    UInt_t treeIndex = 0;
    for (std::vector<TTree*>::const_iterator inputTree = m_inputTrees.begin(); inputTree != m_inputTrees.end(); inputTree++, treeIndex++) {      
      reader.ReadFrom(*inputTree);
      TString name = (*inputTree)->GetName();
      if ((*inputTree)->GetDirectory()) name = (*inputTree)->GetDirectory()->GetName() + ("/" + name);
      for (long long k = 0; k < (*inputTree)->GetEntries(); k++, allEntries++) {
        if ((k % 10000) == 0) 
          std::cout << "Processing tree " << name << ", event " << k << "/" << (*inputTree)->GetEntries() << std::endl;
        (*inputTree)->GetEntry(k);
        double factor = m_factors[treeIndex];
        
        // Determine weighting factors if AddWithProportion was called.
        // Sample i gets: parameter_i * integral / (weight_i * sumOfParameters) (m_lumi is ignored)
        if (m_sumOfParameters != 0.)
          factor *= m_totalWeight / m_sumOfParameters;
        
        // Force the integral to be m_integral
        if (m_integral != 0.)
            factor *= m_integral / m_totalWeight;
        
        // OQ map weight
        if (m_badOQLumi && !PassedOQMap(reader)) factor *= (m_lumi - m_badOQLumi)/m_lumi;
        
        if (!Reweigh(reader, factor)) continue;
        writeTree->Fill();
      }
    }
    
    std::cout << "New integral: " << TreeAbsReader::TotalWeight(writeTree, "", "weight") << std::endl; // TODO: do not hardcode "weight"
    
    writeFile->cd();
    writeTree->Write();
    TArrayD stats(3);
    stats[0] = writeTree->GetEntries();
    stats[1] = writeTree->GetEntries();
    stats[2] = writeTree->GetEntries();
    writeFile->WriteObject(&stats, "stats");
    writeFile->Close();
    delete writeFile;
    return true;
  }

  template<class T>
  double LumiMerger<T>::GetWeight(TTree *t, const TString& weightVar) const
  {
    if (m_badOQLumi == 0)
      return TreeAbsReader::TotalWeight(t, "", weightVar);
    // Need to split between events that passed the OQ check and the ones that didn't
    return TreeAbsReader::TotalWeight(t, "passedOQMap == 1", weightVar) + TreeAbsReader::TotalWeight(t, "passedOQMap == 0", weightVar)*(m_lumi - m_badOQLumi)/m_lumi; // TODO: do not hardcode "passedOQMap"
  }
    
} // namespace TreeReader

#endif
