// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : ArrayReader
 *
 * @brief  Concrete class for readers accessing a std::vector branch
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_ArrayReader
#define ROOT_TreeReader_ArrayReader

#include "TreeReader/CachingReader.h"
#include "TreeReader/BasicReader.h"
#include "TreeReader/DataType.h"

#include <vector>
#include <iostream>

namespace TreeReader {
  
  template<class T>
    class ArrayReader : public CachingReader<T> {
    
  public:
    
//    ArrayReader() { }   

    // Normal constructor: need to specify the branch name
    ArrayReader(TString branch, const BasicReader<Int_t>& indexReader, 
                int nMax = 1000, FamilyMember* parent = 0)
        : CachingReader<T>(branch, parent), m_branch(branch),
          m_indexReader(&indexReader), m_nMax(nMax), m_array(0) { }

    ArrayReader(const ArrayReader& other, FamilyMember* parent = 0)
        : CachingReader<T>(other, parent), m_branch(other.m_branch),
          m_indexReader(other.m_indexReader), m_nMax(other.m_nMax), m_array(0) { }

    virtual ~ArrayReader() { if (m_array) delete[] m_array; }
    
    bool SetupForRead(TTree* tree);
    bool SetupForWrite(TTree* tree);

    // Calculate the new value: just need to copy the i-th value from the branch.
    const T* UpdateValue(UInt_t i) const;

    const BasicReader<Int_t>& IndexReader() const { return *m_indexReader; }

    TString Branch() const { return m_branch; }
    TString IndexBranch() const { return m_indexReader->Branch(); }

    // size is the branch vector size
    UInt_t GetSize() const { return *m_indexReader; }
    UInt_t GetCapacity() const { return m_nMax; }

    void ChangeValue(UInt_t i, const T& val);
    void SetSize (UInt_t) { };
    
    bool OwnsCache() const { return false; }

    T& Val(UInt_t i) { return m_array[i]; }
    
  private:

    TString m_branch;
    const BasicReader<Int_t>* m_indexReader;
    UInt_t m_nMax;
    T* m_array;

//    ClassDef( ArrayReader, 0 )
  };
}


template<class T>
bool TreeReader::ArrayReader<T>::SetupForRead(TTree* tree)
{
  if (!CachingReader<T>::SetupForRead(tree)) return false;
  
  // Get the branch from the tree
  TBranch* bObj = tree->GetBranch(m_branch);
  if (!bObj) {
    cout << "Branch " << m_branch << " does not exist in tree!" << endl;
    return  CachingReader<T>::SetupForRead(0);
  }

  if (!m_array) m_array = new T[m_nMax];

  // Check if the type of the branch matches the type of T, using DataType<T>
  // use of 'this' required here since size() is defined in untemplated base class...
  if (!ReaderType<T>(m_branch, m_indexReader->Branch()).CheckBranch(*bObj)) return CachingReader<T>::SetupForRead(0);

  // Link the branch address to the member variable
  tree->SetBranchStatus(m_branch, 1);
  tree->SetBranchAddress(m_branch, m_array);
  
  return true;
}


template<class T>
bool TreeReader::ArrayReader<T>::SetupForWrite(TTree* tree)
{
  if (!m_array) m_array = new T[m_nMax];
  
  TBranch* indexBranch = tree->GetBranch(m_indexReader->Branch());
  if (!indexBranch) cout << "Cannot set up Array branch " << m_branch << " for writing since its index (" 
                         << m_indexReader->Branch() << ") is not set up yet." << endl;
  
  tree->Branch(m_branch, m_array, ReaderType<T>(m_branch, m_indexReader->Branch()).ReaderSpec().FormatString());
  
  return true;
}


template<class T>
const T* TreeReader::ArrayReader<T>::UpdateValue(UInt_t i) const
{  
  if (i >= m_nMax || !m_array) return 0; 
  return &m_array[i];
}

template<class T>
void TreeReader::ArrayReader<T>::ChangeValue(UInt_t i, const T& val) 
{ 
  if (i >= m_nMax) {
    cout << "Cannot set " << m_branch << "[" << i << "] since the array has max size " << m_nMax << endl;
    return;
  }
  
  if (!m_array) {
    // This is a bit overkill since we could in principle just use the reader as a container,
    // without have to write out the result to ROOT. But...
    cout << "Cannot set values for " << m_branch << " since it was not set up for writing." << endl;
    return;
  }
  
  m_array[i] = val; 
}

#endif

