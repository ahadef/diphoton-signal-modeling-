// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : StdVectorReader
 *
 * @brief  Simple example of a reader of a 4-vector based on the values of 4 branches
 *         (E, pT, eta, phi)
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_TLorentzVectorReader
#define ROOT_TreeReader_TLorentzVectorReader

#include "TreeReader/CachingReader.h"
#include "TLorentzVector.h"

#include "TreeReader/BranchReader.h"
#include "TreeReader/TVector3Reader.h"

namespace TreeReader {
  
  template<class C4, class C, class T = float>
  class TLorentzVectorReader : public CachingReader<TLorentzVector> {
    
  public:

    // Normal contructor: defines 3 sub-readers (BranchReaders, so that this class
    // works on either std::vectors or simple branches)

    TLorentzVectorReader(const TString& x4, const TString& p1, const TString& p2, const TString& p3,
                         FamilyMember* parent = 0,  BranchType type = AutoDetect, 
                         const BasicReader<Int_t>* index = 0) : 
      CachingReader<TLorentzVector>("TLorentzVector(" + x4 + ", " + p1 + ", " + p2 + ", " + p3 + ")", parent),
      m_x4(x4, this, type, index),
      m_P (p1, p2, p3, this, type, index) { }

    TLorentzVectorReader(const TLorentzVectorReader& other, FamilyMember* parent = 0)
      : CachingReader<TLorentzVector>(*this, parent), m_x4(other.m_x4, this), m_P(other.m_P, this) { }

    virtual ~TLorentzVectorReader() { }

    FamilyMember* Clone(FamilyMember* parent = 0) const { return new TLorentzVectorReader<C4, C, T>(*this, parent); }

    // The calculation of the return valus from the branch values
    const TLorentzVector* UpdateValue(UInt_t i) const;
    void ChangeValue (UInt_t i, const TLorentzVector& v);

    const BranchReader<T>&     x4() const { return m_x4; }
    const TVector3Reader<C,T>&  P() const { return m_P; }

  private:

    BranchReader<T> m_x4;
    TVector3Reader<C,T> m_P;

//    ClassDef( TLorentzVectorReader, 0 )
  };

  class Energy {
    public:
      static void SetCoord(TLorentzVector& v, double e) { v.SetE(e); }
      static double GetCoord(const TLorentzVector& v) { return v.E(); }
  };
  
  class Mass {
    public:
      static void SetCoord(TLorentzVector& v, double m) { v.SetVectM(v.Vect(), m); }
      static double GetCoord(const TLorentzVector& v) { return v.M(); }
  };
}

template<class C4, class C, class T>
const TLorentzVector* TreeReader::TLorentzVectorReader<C4,C,T>::UpdateValue(UInt_t i) const
{ 
  // Make a new TLorentzVector value
  TLorentzVector* vect = new TLorentzVector(m_P(i), 0);
  C4::SetCoord(*vect, m_x4(i));
  return vect;
}


template<class C4, class C, class T>
void TreeReader::TLorentzVectorReader<C4,C,T>::ChangeValue(UInt_t i, const TLorentzVector& v)
{
  m_x4.SetValue(C4::GetCoord(v), i);
  m_P.SetValue(v.Vect(), i);
}

#endif
