// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : MultiReader
 *
 * @brief A concrete reader from a vector of subreaders
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_MultiReader
#define ROOT_TreeReader_MultiReader

#include "TreeReader/ReaderBase.h"
#include "TreeReader/BranchReader.h"
#include "TreeReader/DataType.h"

#include <map>

namespace TreeReader {

 class MultiReader : public ReaderBase<bool> 
 {

  public:

    MultiReader(FamilyMember* parent = 0) : ReaderBase<bool>("", parent) { }
    MultiReader(const MultiReader& other, FamilyMember* parent = 0);
    virtual ~MultiReader();

    FamilyMember* Clone(FamilyMember* parent = 0) const { return new MultiReader(*this, parent); }

    const bool& GetValue(UInt_t) const { static bool status = true; return status; }
    void SetValue(const bool&, UInt_t) { }

    BranchReader<double>* AddDouble(const TString& branch) { BranchReader<double>* reader = new BranchReader<double>(branch, this); m_daughters[branch] = reader; return reader; }
    BranchReader<float>*  AddFloat (const TString& branch) { BranchReader<float>*  reader = new BranchReader<float> (branch, this); m_daughters[branch] = reader; return reader; }
    BranchReader<int>*    AddInt   (const TString& branch) { BranchReader<int>*    reader = new BranchReader<int>   (branch, this); m_daughters[branch] = reader; return reader; }
    BranchReader<UInt_t>*    AddUInt  (const TString& branch) { BranchReader<UInt_t>* reader = new BranchReader<UInt_t>   (branch, this); m_daughters[branch] = reader; return reader; }
    BasicReader<bool>*    AddBool  (const TString& branch) { BasicReader<bool>*    reader = new BasicReader<bool>   (branch, this); m_daughters[branch] = reader; return reader; }

    BranchReader<double>* Double(const TString& branch) { return dynamic_cast<BranchReader<double>*>(Find(branch)); }
    BranchReader<float>*  Float (const TString& branch) { return dynamic_cast<BranchReader<float>* >(Find(branch)); }
    BranchReader<int>*    Int   (const TString& branch) { return dynamic_cast<BranchReader<int>*   >(Find(branch)); }
    BranchReader<UInt_t>*    UInt  (const TString& branch) { return dynamic_cast<BranchReader<UInt_t>*   >(Find(branch)); }
    BasicReader<bool>*    Bool  (const TString& branch) { return dynamic_cast<BasicReader <bool>*  >(Find(branch)); }

    void Add(const TString& name, FamilyMember* daughter) { if (!daughter) return; m_daughters[name] = daughter; AddChild(*daughter); }

#ifndef __GCCXML__
    template<class T> T* FindReader(const TString& name) { return dynamic_cast<T*>(Find(name)); }
#endif
    
    FamilyMember* Find(const TString& branch);
    void Print();
    
    static TString FunctionString(const BranchSpec& branchSpec);
    static bool MakeReaderMacro(const TString& name, const TString& fileName, const TString& treeName,
                                const TString& outputFileName = "", const TString& outputTreeName = "");
    static TString ReaderClass(const BranchSpec& branchSpec);
    static bool MakeReaderClass(const TString& name, TTree& tree, const TString& exclude);

  private:

    std::map<TString, FamilyMember*> m_daughters;
//    ClassDef(MultiReader, 0 )

  };
}

#endif
  
