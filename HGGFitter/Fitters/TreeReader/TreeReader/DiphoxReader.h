// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : HggEVTreeReader_AANT
 *
 * @brief A concrete application for the H->gamma gamma case: fill an ntuple from
 *        values read from an EventView tree.
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/

#ifndef ROOT_TreeReader_DiphoxReader
#define ROOT_TreeReader_DiphoxReader

#include "TreeReader/TreeAbsReader.h"
#include "TreeReader/BasicReader.h"
#include "TreeReader/TLorentzVectorReader.h"


namespace TreeReader {

  class DiphoxReader : public TreeAbsReader {

  public:

    // Constructor: defines sub-readers for all the necessary quantities

    DiphoxReader(TTree* tree, long long nEvents = -1, long long iBegin = 0, 
                 long long originalNEvents = -1, bool writeStats = false);
    
    virtual ~DiphoxReader() { }

    bool SetupForRead (TTree*);
    bool SetupForWrite(TTree*);

    bool Initialize() { return true; }
    bool Execute();
    bool Finalize() { return true; }
    
    // input
    BasicReader<Int_t> iprov, ntrack;
    TLorentzVectorReader<Energy, PxPyPz> photon_p4;
    BasicReader<Float_t> weight;
       
    // output
    BasicReader<Float_t> mgg;
    BasicReader<Int_t> iprovOut;
    BasicReader<Float_t> weightOut;
    BasicReader<Int_t> cat9;

//    ClassDef( DiphoxReader, 0 )

  };
}

#endif
