// -*- C++ -*-
/**********************************************************************************
 * @Project: Hfitter
 * @Package: TreeReader
 * @Class  : IReader
 *
 * @brief The base class for all Readers (classes that attach themselves to a tree
 *        to read the content of a branch). It inherits its interface from IReader
 *        and the status management of FamilyMember.
 *
 * @author Nicolas Berger  <Nicolas.Berger@cern.ch>  - CERN
 **********************************************************************************/


#ifndef ROOT_TreeReader_ReaderBase
#define ROOT_TreeReader_ReaderBase

#include "TreeReader/FamilyMember.h"

#include "TTree.h"
#include "TString.h"
#include <vector>

namespace TreeReader {
  
  template<class T> class ReaderBase : public FamilyMember {
    
    public:
      
      ReaderBase(const TString& name, FamilyMember* parent = 0) : FamilyMember(parent), m_name(name) { }
      ReaderBase(const ReaderBase& other, FamilyMember* parent = 0) : FamilyMember(parent), m_name(other.m_name) { }
      
      virtual ~ReaderBase() { }
    
      // The branch value for the current TTree entry. For a vector-type branch, 'i' specifies 
    // the index in the vector (should always be 0 for simple branches)
      virtual const T& GetValue (UInt_t i = 0) const = 0;
      
    // The same, easier to use
      virtual const T& operator() (UInt_t i = 0) const { return GetValue(i); }
      
      virtual void SetValue (const T& val, UInt_t i = 0) = 0;
      
      // An alias for the method above
      virtual UInt_t size() const { return GetSize(); }

      void GetValues(std::vector<T>& cont) { for (UInt_t i = 0; i < size(); i++) cont.push_back(GetValue(i)); }
      
      TString GetName() const { return m_name; }
      
   private:
     TString m_name; 
  };
} 
#endif
