#include "TreeReader/FamilyMember.h"
#include <iostream>

using namespace std;

bool TreeReader::FamilyMember::ReadOK() const
{
  if (!GetReadOK()) return false;

  // Recursively examine dependents
  for (unsigned int i = 0; i < m_children.size(); i++)
    if (!m_children[i]->ReadOK()) return false;

  return true;
}


bool TreeReader::FamilyMember::WriteOK() const
{
  if (!GetWriteOK()) return false;

  // Recursively examine dependents
  for (unsigned int i = 0; i < m_children.size(); i++)
    if (!m_children[i]->WriteOK()) return false;

  return true;
}


bool TreeReader::FamilyMember::ReadFrom(TTree* tree)
{
  if (!SetupForRead(tree)) return false;

  for (unsigned int i = 0; i < m_children.size(); i++)
    if (!m_children[i]->ReadFrom(tree)) return false;

  return ReadOK();
}


bool TreeReader::FamilyMember::WriteTo(TTree* tree)
{
  if (!SetupForWrite(tree)) return false;

  for (unsigned int i = 0; i < m_children.size(); i++)
    if (!m_children[i]->WriteTo(tree)) return false;

  return WriteOK();
}


unsigned int TreeReader::FamilyMember::GetSize() const
{
  if (m_children.size() > 0) return m_children[0]->GetSize();
  return 0;
}


void TreeReader::FamilyMember::SetSize(unsigned int n)
{
  for (unsigned int i = 0; i < m_children.size(); i++)
    m_children[i]->SetSize(n);
}


unsigned int TreeReader::FamilyMember::GetCapacity() const
{
  if (m_children.size() > 0) return m_children[0]->GetCapacity();
  return 0;
}
