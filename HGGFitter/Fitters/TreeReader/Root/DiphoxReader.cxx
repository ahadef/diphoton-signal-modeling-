#include "TreeReader/DiphoxReader.h"
#include "TLorentzVector.h"

using namespace std;
using namespace TreeReader;

DiphoxReader::DiphoxReader(TTree* tree, long long nEvents, long long iBegin, 
                           long long originalNEvents, bool writeStats)
 : TreeAbsReader(tree, nEvents, iBegin, originalNEvents, writeStats),
    iprov("iprov"),
    ntrack("ntrack"),
    photon_p4("we", "wpx", "wpy", "wpz", 0, AutoDetect, &ntrack),
    weight("weight"),
    mgg("mgg"),
    iprovOut("iprov"),
    weightOut("weight"),
    cat9("cat9")
{
}

bool DiphoxReader::SetupForRead(TTree* tree)
{
  iprov.ReadFrom(tree);
  ntrack.ReadFrom(tree);
  photon_p4.ReadFrom(tree);
  weight.ReadFrom(tree);
  return true;
}


bool DiphoxReader::SetupForWrite(TTree* tree)
{
  mgg.WriteTo(tree);
  iprovOut.WriteTo(tree);
  weightOut.WriteTo(tree);
  cat9.WriteTo(tree);
  return true;
}


bool DiphoxReader::Execute()
{
  if (photon_p4.size() < 2) {
    cout << "ERROR : photon_p4 has size " << photon_p4.size() << endl;
    return false;
  }
  double pT1 = photon_p4(0).Pt();
  double pT2 = photon_p4(1).Pt();
  if (pT1 < 30 || pT2 < 30) return true;
  if (pT1 < 40 && pT2 < 40) return true;
  double eta1 = photon_p4(0).Eta();
  double eta2 = photon_p4(1).Eta();
  if (TMath::Abs(eta1) > 2.37 || TMath::Abs(eta2) > 2.37) return true;
  if (TMath::Abs(eta1) > 1.37 && TMath::Abs(eta1) < 1.52) return true;
  if (TMath::Abs(eta2) > 1.37 && TMath::Abs(eta2) < 1.52) return true;
 
  mgg = (photon_p4(0) + photon_p4(1)).M();
  iprovOut = iprov();
  weightOut = weight();

  int cat = 0;
  //double pT = (photon_p4(0)+photon_p4(1)).Pt();
  TVector3 pTV1 = photon_p4(0).Vect(); pTV1.SetZ(0);
  TVector3 pTV2 = photon_p4(1).Vect(); pTV2.SetZ(0);
  double pTt = ((pTV1 + pTV2).Cross((pTV1 - pTV2).Unit())).Mag();
  bool isGood = (TMath::Abs(eta1) < 0.75 && TMath::Abs(eta2) < 0.75);
  bool isBad = ((TMath::Abs(eta1) >= 1.3 && TMath::Abs(eta1) <= 1.75) ||
                (TMath::Abs(eta2) >= 1.3 && TMath::Abs(eta2) <= 1.75));
  bool isMed = (!isGood && !isBad);
  bool isHPT = (pTt > 40);
  if (isGood  && !isHPT) cat |= (1 << 0);
  if (isGood  &&  isHPT) cat |= (1 << 1);
  if (!isGood && !isHPT) cat |= (1 << 2);
  if (!isGood &&  isHPT) cat |= (1 << 3);
  if (isGood  && !isHPT) cat |= (1 << 4);
  if (isGood  &&  isHPT) cat |= (1 << 5);
  if (isMed   && !isHPT) cat |= (1 << 6);
  if (isMed   &&  isHPT) cat |= (1 << 7);
  if (isBad)             cat |= (1 << 8);
  cat9 = cat;

  Fill();
  return true;
}

