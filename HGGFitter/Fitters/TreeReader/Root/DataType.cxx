#include "TreeReader/DataType.h"
#include "TObjString.h"


template<> TString TreeReader::ReaderType<UInt_t>::GetType() const
{
  return "UInt_t";
}

template<> TString TreeReader::ReaderType<UShort_t>::GetType() const
{
  return "UShort_t";
}

template<> TString TreeReader::ReaderType<Short_t>::GetType() const
{
  return "Short_t";
}

template<> TString TreeReader::ReaderType<Int_t>::GetType() const
{
  return "Int_t";
}

template<> TString TreeReader::ReaderType<Double_t>::GetType() const
{
  return "Double_t";
}

template<> TString TreeReader::ReaderType<Float_t>::GetType() const
{
  return "Float_t";
}

template<> TString TreeReader::ReaderType<Bool_t>::GetType() const
{
  return "Bool_t";
}

template<> TString TreeReader::ReaderType<std::string>::GetType() const
{
  return "std::string";
}


TreeReader::BranchSpec::BranchSpec(TBranch& branch)
{
  // First the name, easy
  m_name = branch.GetName();
  
  // Then the counter (only one supported), if applicable
  TLeaf* leaf = (TLeaf*)branch.GetListOfLeaves()->At(0);
  if (!leaf) {
    m_index = "";
    m_type = "";
    return;
  }

  int count;
  TLeaf* counter = leaf->GetLeafCounter(count);
  m_index = (counter ? counter->GetName() : "");
  
  // Finally the type:
  TString branchType = branch.GetClassName();
  
  // First objects
  if (branchType != "") {
    m_type = branchType;
  }
  else  // And then basic types:
    m_type = leaf->GetTypeName();
  
  m_type = NormalizeType(m_type);

  // safe name: for C++ variable names
  m_safeName = m_name;
  m_safeName.ReplaceAll(":", "_");
}


TreeReader::BranchSpec::BranchSpec(const TString& name, const TString& type, const TString& index)
  : m_name(name),
    m_type(type),
    m_index(index)

{
  m_type = NormalizeType(m_type);
}
 
 
TString TreeReader::BranchSpec::NormalizeType(TString type)
{
  if (type == "bool") return "Bool_t";
  if (type == "unsigned short") return "UShort_t";
  if (type == "short") return "Short_t";
  if (type == "unsigned int") return "UInt_t";
  if (type == "int") return "Int_t";
  if (type == "float") return "Float_t";
  if (type == "double") return "Double_t";
  if (type == "string") return "std::string";
 
  if (type == "vector<bool>") return "vector<Bool_t>";
  if (type == "vector<unsigned short>") return "vector<UShort_t>";
  if (type == "vector<short>") return "vector<Short_t>";
  if (type == "vector<unsigned int>") return "vector<UInt_t>";
  if (type == "vector<int>") return "vector<Int_t>";
  if (type == "vector<float>") return "vector<Float_t>";
  if (type == "vector<double>") return "vector<Double_t>";
  if (type == "vector<string>") return "vector<std::string>";

  if (type == "vector<vector<int> >") return "vector<std::vector<Int_t>>";
  if (type == "vector<vector<unsigned int> >") return "vector<std::vector<UInt_t>>";
  if (type == "vector<vector<float> >") return "vector<std::vector<Float_t>>";
  if (type == "vector<vector<double> >") return "vector<std::vector<Double_t>>";

  if (type == "vector<vector<vector<float> > >") return "vector<std::vector<std::vector<Float_t>>>";
  if (type == "vector<vector<vector<int> > >") return "vector<std::vector<std::vector<Int_t>>>";

  return type;
}


bool TreeReader::BranchSpec::operator==(const BranchSpec& spec) const
{
  // To deal with Friend trees, need to ignore prefixes in branch names of the form "T.B"
  TString name1 = Name(), name2 = spec.Name();
  TObjArray* array = Name().Tokenize(".");
  if (array->GetEntries() >= 2) name1 = ((TObjString*)array->At(array->GetEntries() - 1))->String();
  delete array;
  array = spec.Name().Tokenize(".");
  if (array->GetEntries() >= 2) name2 = ((TObjString*)array->At(array->GetEntries() - 1))->String();
  delete array;
  //cout << name1 << " " << name2 << endl;
  return (name1 == name2 && Type() == spec.Type() && Index() == spec.Index()); 
}


TString TreeReader::BranchSpec::FormatString() const
{
  TString shortType = "";
  if (m_type == "Bool_t") shortType = "O";
  if (m_type == "UChar_t") shortType = "b";
  if (m_type == "Char_t") shortType = "B";
  if (m_type == "UShort_t") shortType = "s";
  if (m_type == "Short_t") shortType = "S";
  if (m_type == "UInt_t") shortType = "i";
  if (m_type == "Int_t") shortType = "I";
  if (m_type == "Float_t") shortType = "F";
  if (m_type == "Double_t") shortType = "D";

  if (shortType == "") return m_type;
  
  TString str = m_name;
  if (m_index != "") str += "[" + m_index + "]";
  str += "/" + shortType;
  
  return str;
} 


TString TreeReader::BranchSpec::Description() const
{
  TString str = m_name;
  if (m_index != "") str += "[" + m_index + "]";
  str += "/" + m_type;
  
  return str;
}
