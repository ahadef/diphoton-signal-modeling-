
#include "TreeReader/TreeAbsReader.h"
#include <iostream>
#include "TFile.h"
#include "TArrayD.h"
#include "TSystem.h"
#include "TH1D.h"
#include "TChainElement.h"

using namespace std;
using namespace TreeReader;


TreeAbsReader::TreeAbsReader(TTree* tree, long long nEvents, long long iBegin, 
                             long long originalNEvents, bool writeStats) 
  : ReaderBase<bool>(tree ? tree->GetName() : "TreeAbsReader", NULL),
    m_readTree(tree),
    m_printFreq(10000),
    m_saveFreq(10000),
    m_originalNEvents(originalNEvents),
    m_writeStats(writeStats),
    m_currentEntry(-1)
{ 
  if (!m_readTree) return;
  m_readTree->SetBranchStatus("*", 0);
  m_begin   = (iBegin  >= 0 ? iBegin   : 0);
  m_nEvents = (nEvents >= 0 ? nEvents : tree->GetEntries());  
  if (m_begin + m_nEvents > m_readTree->GetEntries()) 
    m_nEvents = m_readTree->GetEntries() - m_begin;
  if (m_originalNEvents < 0)
  {
    m_originalNEvents = GetOriginalNEvents(tree);
    if (m_originalNEvents < 0)
      m_originalNEvents = m_nEvents;
  }
}


bool TreeAbsReader::Run(const TString& outputFile, const TString& treeName)
{
  ReadFrom(m_readTree);
  m_writeFile = 0;
  
  if (TString(outputFile) != "") {
    TString writeTreeName = treeName;
    if (writeTreeName == "") writeTreeName = m_readTree->GetName();
    m_writeTree = NewTree(writeTreeName, outputFile, &m_writeFile);
    WriteTo(m_writeTree);
  }
  
  bool result = GetValue();
  
  if (result && m_writeFile) {
    cout << "Writing output tree '" << m_writeTree->GetName() << "' (" 
         << m_writeTree->GetEntries() << " entries) to " 
          << m_writeFile->GetName() << endl;
    m_writeFile->cd();
    m_writeTree->Write();
    if (m_writeStats) {
      TArrayI stats(4);
      stats[0] = (m_nEvents == m_readTree->GetEntries() ? m_originalNEvents : (long long)(double(m_nEvents)/m_readTree->GetEntries()*m_originalNEvents));
      stats[1] = m_readTree->GetEntries();
      stats[2] = m_nEvents;
      stats[3] = m_writeTree->GetEntries();
      m_writeFile->WriteObject(&stats, "stats");
    }
    m_writeFile->Close();
    delete m_writeFile;
  }
  else {
    delete m_writeTree;
    if (m_writeFile) {
      cout << "Processing failed - no data written to "  << m_writeFile->GetName() << endl;
      delete m_writeFile;
    }
  }
  
  m_writeFile = 0;
  m_writeTree = 0;
  
  return result;
}


const bool& TreeAbsReader::GetValue(unsigned int) const
{
  if (!((TreeAbsReader*)this)->Initialize()) return SetResult(false);

  if (!ReadOK()) {
    cout << "TreeAbsReader::Run : One or more branch readers failed to initialize, please check for previous errors." << endl;
    return SetResult(false);
  }

  cout << "Running on " << m_nEvents << " events, starting at event " 
       << m_begin << "." << endl;

  for (m_currentEntry = m_begin; m_currentEntry < m_begin + m_nEvents; m_currentEntry++) {

    if (m_currentEntry % m_printFreq == 0) cout << "Processing Event " << m_currentEntry << endl;
    try  {
      m_readTree->GetEntry(m_currentEntry);
    }
    catch (std::exception &e) {
      cout << "Problem reading event " << m_currentEntry << "(" << e.what() << "), skipping" << endl;
      continue;
    }
    if (!((TreeAbsReader*)this)->Execute()) return SetResult(false);
    if (m_currentEntry % m_saveFreq == 0) m_writeTree->AutoSave();
  }
  cout << "Output events = " << m_writeTree->GetEntries() << endl;
  
  if (!((TreeAbsReader*)this)->Finalize()) return SetResult(false);
  return SetResult(true);
}


const bool& TreeAbsReader::SetResult(bool result) const 
{ 
  m_result = result;
  return m_result;
}


bool TreeAbsReader::ReadStats(TFile* f, long long& nOriginal, long long& nInput, long long& nProcessed, long long& nOutput)
{
  if (!f) return false;
  TArrayI* stats = (TArrayI*)(f->Get("stats"));
  if (!stats || stats->GetSize() != 4) return false;
  
  nOriginal = stats->At(0);
  nInput = stats->At(1);
  nProcessed = stats->At(2);
  nOutput = stats->At(3);
  return true;
}


TTree* TreeAbsReader::NewTree(const TString& treeName, const TString& fileName, TFile** file)
{
  TFile* f = TFile::Open(fileName, "RECREATE");
  if (!f) return 0;
  if (file) *file = f;
  return new TTree(treeName, "");
}


TTree* TreeAbsReader::GetTree(const TString& treeName, const TString& fileName, TFile** file)
{
  TFile* f = TFile::Open(fileName);
  if (!f) return 0;
  TTree* t = (TTree*)f->Get(treeName);
  if (file) *file = f;
  return t;
}


TChain* TreeAbsReader::GetChain(const TString& treeName, const TString& filePattern)
{
  TChain* chain = new TChain(treeName);
  chain->Add(filePattern);
  return chain;
}


bool TreeAbsReader::FilterTree(TTree& tree, const TString& filter, const TString& outputFileName, bool clobber)
{
  if (!gSystem->AccessPathName(outputFileName) && !clobber) {
    cout << "ERROR : file " << outputFileName << " already exists, please specify clobber=true to overwrite." << endl;
    return false;
  }
  TFile* outputFile = TFile::Open(outputFileName, "RECREATE");
  TTree* newTree = tree.CopyTree(filter);
  newTree->Write();
  delete outputFile;
  return true;
}


long long TreeAbsReader::ReadNOriginal(const TString& fileName)
{
  TFile* f = TFile::Open(fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR : File " << fileName << " is not accessible.";
    return -1;
  }
  long long nOriginal, nInput, nProcessed, nOutput;
  if (!ReadStats(f, nOriginal, nInput, nProcessed, nOutput)) return -1;
  return nOriginal;
}


long long TreeAbsReader::ReadNInput(const TString& fileName)
{
  TFile* f = TFile::Open(fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR : File " << fileName << " is not accessible.";
    return -1;
  }
  long long nOriginal, nInput, nProcessed, nOutput;
  if (!ReadStats(f, nOriginal, nInput, nProcessed, nOutput)) return -1;
  return nInput;
}


long long TreeAbsReader::ReadNProcessed(const TString& fileName)
{
  TFile* f = TFile::Open(fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR : File " << fileName << " is not accessible.";
    return -1;
  }
  long long nOriginal, nInput, nProcessed, nOutput;
  if (!ReadStats(f, nOriginal, nInput, nProcessed, nOutput)) return -1;
  return nProcessed;
}


long long TreeAbsReader::ReadNPassed(const TString& fileName)
{
  TFile* f = TFile::Open(fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR : File " << fileName << " is not accessible.";
    return -1;
  }
  long long nOriginal, nInput, nProcessed, nOutput;
  if (!ReadStats(f, nOriginal, nInput, nProcessed, nOutput)) return -1;
  return nOutput;
}


double TreeAbsReader::ReadSelectionEff(const TString& fileName)
{
  TFile* f = TFile::Open(fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR : File " << fileName << " is not accessible.";
    return -1;
  }
  long long nOriginal, nInput, nProcessed, nOutput;
  if (!ReadStats(f, nOriginal, nInput, nProcessed, nOutput)) return -1;
  return double(nOutput)/nProcessed;
}


unsigned int TreeAbsReader::Add(TChain& chain, const TString& files)
{
  FILE* cfile = gSystem->OpenPipe("ls " + files, "r");
  char buffer[999];
  unsigned int n = 0, nOK = 0;
  while (fscanf(cfile, "%s", buffer) == 1) {
    cout << "Adding file " << buffer << endl;
    n++;
    nOK += chain.Add(buffer);
  }
  cout << "Added " << nOK << " files (of " << n << " matching pattern)" << endl;
  return nOK;
}


double TreeAbsReader::TotalWeight(TTree* tree, const TString& cut, const TString& weightVar)
{
  TString weight = weightVar;
  if (weight == "") 
    weight = "1";
  unsigned int N = tree->Draw(weight, cut, "goff");
  double sum = 0;
  for (unsigned int i = 0; i < N; ++i)
    sum += tree->GetV1()[i];
  return sum;
}

long long TreeAbsReader::GetOriginalNEvents(TTree *tree)
{
  if (!tree) return -1;
  
  TChain *chain = dynamic_cast<TChain*>(tree);
  if (chain)
  {
    long long nEvents = 0;
    TObjArray *fileElements=chain->GetListOfFiles();
    TIter next(fileElements);
    TChainElement *chEl=0;
    while (( chEl=(TChainElement*)next() )) 
    {
      TFile *f = TFile::Open(chEl->GetTitle());
      if (!f) continue; 
      TArrayI* stats = (TArrayI*)(f->Get("stats"));
      if (stats && stats->GetSize() == 4) nEvents += stats->At(0);
      f->Close();
        
    }
    return (nEvents > 0 ? nEvents : -1); 
  }
  TDirectory *f = tree->GetDirectory();
  TArrayI* stats = (TArrayI*)(f->Get("stats"));
  return (stats && stats->GetSize() == 4  ? stats->At(0) : -1);
}
