#include "TreeReader/StdVectorReader.h"

// Functions to determine payload type of StdVectorReaders: should return the type contained in the vector

template<> TString TreeReader::ReaderType< std::vector<UShort_t> >::GetPayloadType()  const
{
  return "UShort_t";
}


template<> TString TreeReader::ReaderType< std::vector<Short_t> >::GetPayloadType()  const
{
  return "Short_t";
}


template<> TString TreeReader::ReaderType< std::vector<UInt_t> >::GetPayloadType()  const
{
  return "UInt_t";
}


template<> TString TreeReader::ReaderType< std::vector<Int_t> >::GetPayloadType()  const
{
  return "Int_t";
}


template<> TString TreeReader::ReaderType< std::vector<Float_t> >::GetPayloadType()  const
{
  return "Float_t";
}


template<> TString TreeReader::ReaderType< std::vector<Double_t> >::GetPayloadType()  const
{
  return "Double_t";
}


template<> TString TreeReader::ReaderType< std::vector<std::string> >::GetPayloadType()  const
{
  return "std::string";
}


template<> TString TreeReader::ReaderType< std::vector<std::vector<UShort_t> > >::GetPayloadType()  const
{
  return "std::vector<UShort_t>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<Short_t> > >::GetPayloadType()  const
{
  return "std::vector<Short_t>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<UInt_t> > >::GetPayloadType()  const
{
  return "std::vector<UInt_t>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<Int_t> > >::GetPayloadType()  const
{
  return "std::vector<Int_t>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<Float_t> > >::GetPayloadType()  const
{
  return "std::vector<Float_t>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<Double_t> > >::GetPayloadType()  const
{
  return "std::vector<Double_t>";
}


template<> TString TreeReader::ReaderType< std::vector<std::vector<std::vector<UShort_t> > > >::GetPayloadType()  const
{
  return "std::vector<std::vector<UShort_t>>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<std::vector<Short_t> > > >::GetPayloadType()  const
{
  return "std::vector<std::vector<Short_t>>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<std::vector<UInt_t> > > >::GetPayloadType()  const
{
  return "std::vector<std::vector<UInt_t>>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<std::vector<Int_t> > > >::GetPayloadType()  const
{
  return "std::vector<std::vector<Int_t>>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<std::vector<Float_t> > > >::GetPayloadType()  const
{
  return "std::vector<std::vector<Float_t>>";
}

template<> TString TreeReader::ReaderType< std::vector<std::vector<std::vector<Double_t> > > >::GetPayloadType()  const
{
  return "std::vector<std::vector<Double_t>>";
}

