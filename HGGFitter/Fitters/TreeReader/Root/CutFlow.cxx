#include "TreeReader/CutFlow.h"

using namespace TreeReader;

#include <iostream>
using std::cout;
using std::endl;

CutFlow::CutFlow(const TString& name, TFile* outputFile, long long originalNEvents)
  : m_name(name), m_outputFile(outputFile), m_tree(0), m_originalNEvents(originalNEvents)
{
  if (m_outputFile) {
    m_outputFile->cd();
    m_tree = new TTree(m_name, "Cutflow tree for " + m_name);
    m_tree->Branch("RunNumber", &m_runNumber);
    m_tree->Branch("EventNumber", &m_eventNumber);
    m_tree->Branch("lastCut", &m_lastCut);
  }
}


CutFlow::~CutFlow()
{
  if (m_steps.size() == 0) return;
  // Change m_counts[0] in case of a skimmed sample
  if (m_originalNEvents > 0) {
    cout << "(skimmed sample, ran on " << m_steps[0].nSeen << " events)" << endl;
    m_steps[0].nSeen = m_originalNEvents;
  }
  cout << "Cut flow for " << m_name << " : " << endl;

  for (unsigned int i = 0; i < m_steps.size(); i++) {
    if (!m_steps[i].cut) continue;
    cout << Form("Cut %2d (%-20s) : %6d, eff = %.2f %%, efftot = %.2f %%", i,
                 m_steps[i].cut->name.Data(),
                 m_steps[i].nPassed, double(m_steps[i].nPassed)/m_steps[i].nSeen*100,
                 double(m_steps[i].nPassed)/m_steps[0].nSeen*100) << endl;
  }

  if (m_outputFile) {
    m_outputFile->cd();
    //for (unsigned int i = 0; i < evlists.size(); ++i)
    //  evlists[i]->Write();
    m_tree->Write();
  }

  for (std::vector<CutFlowStep>::iterator step = m_steps.begin(); step != m_steps.end(); step++) {
    delete step->action;
    delete step->cut;
  }
}


const CutFlowStep* CutFlow::Step(const TString& cutName) const
{
  for (std::vector<CutFlowStep>::const_iterator step = m_steps.begin(); step != m_steps.end(); step++)
    if (step->cut && step->cut->name == cutName) return &*step;
  return 0;
}


bool CutFlow::Run(unsigned int runNumber, unsigned int eventNumber, bool countObjects)
{
  if (m_verbose) cout << "INFO: running cutflow " << m_name 
                      << " for run " << runNumber << ", event " << eventNumber << endl;
  m_runNumber = runNumber;
  m_eventNumber = eventNumber;
  bool pass = true;
  m_lastCut = m_steps.size();

  for (std::vector<CutFlowStep>::iterator step = m_steps.begin(); step != m_steps.end(); step++)
    step->isPassed = false;

  for (unsigned int i = 0; i < m_steps.size(); i++) {
    if (m_verbose) {
      cout << "INFO: executing step " << i << ", " 
           << (m_steps[i].action ? m_steps[i].action->name : "") << ", " 
           << (m_steps[i].cut ? m_steps[i].cut->name : "") << endl;
    }
    if (!m_steps[i].Run(countObjects)) return false; // ERROR
    if (!m_steps[i].isPassed) {
      if (m_verbose) cout << "INFO: failed cut, exiting cutflow for this event" << endl;
      pass = false;
      m_lastCut = i;
      break;
    }
  }
  if (m_verbose && pass) cout << "INFO: Passed all cuts!" << endl;
  if (m_outputFile) m_tree->Fill();
  return pass;
}

bool CutFlowStep::Run(bool countObjects)
{
  if (action && !action->Execute()) {
    cout << "ERROR : failure in cut action " << action->name << endl;
    return false;
  }
  if (!cut) {  // no cut, just processing: move on
    isPassed = true;
    nSeen++;
    nPassed++;
    return true;
  }

  unsigned int nSeenNow = 0, nPassedNow = 0;
  isPassed = cut->Pass(nSeenNow, nPassedNow);
  nSeen += (countObjects ? nSeenNow : 1);
  if (!isPassed) return true;
  nPassed += (countObjects ? nPassedNow : 1);
  return true;
}

bool CutFlowCut::Pass(unsigned int& nSeen, unsigned int& nPassed) 
{ 
  nSeen = 1; 
   nPassed = Pass(); 
   if (verbose) cout << "INFO: Cut " << name << " passed = " << nPassed << endl; 
   return nPassed > 0; 
}

void CutFlow::SetVerbose(bool verbose) 
{ 
  m_verbose = verbose;
  cout << "setting verbose " << endl;
  for (std::vector<CutFlowStep>::iterator step = m_steps.begin(); step != m_steps.end(); step++) 
    if (step->cut) {
      step->cut->verbose = verbose;
      cout << "setting verbose " << step->cut->name << endl;
    }
}
