#include "TreeReader/MultiReader.h"

#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TRegexp.h"

#include <fstream>

using namespace TreeReader;


MultiReader::MultiReader(const MultiReader& other, FamilyMember* parent)
 : ReaderBase<bool>(other, parent)
{
  for (std::map<TString, FamilyMember*>::const_iterator daug = other.m_daughters.begin(); daug != other.m_daughters.end(); daug++)
    m_daughters[daug->first] = daug->second->Clone(this);
}


MultiReader::~MultiReader()
{
  for (std::map<TString, FamilyMember*>::iterator daug = m_daughters.begin(); daug != m_daughters.end(); daug++)
    delete daug->second;
}


TString MultiReader::FunctionString(const BranchSpec& spec)
{
  TString type = spec.Type();
  if (type.Contains("Double_t")) return "AddDouble(\"" + spec.Name() + "\")";
  if (type.Contains("Float_t"))  return "AddFloat(\""  + spec.Name() + "\")";
  if (type.Contains("Int_t"))    return "AddInt(\""    + spec.Name() + "\")";
  if (type == "Bool_t")          return "AddBool(\""   + spec.Name() + "\")";
 // if (type.Contains("std::string")) return "<std::string>";
  return "";
}


FamilyMember* MultiReader::Find(const TString& branch)
{
  std::map<TString, FamilyMember*>::iterator iter = m_daughters.find(branch);
  return iter != m_daughters.end() ? iter->second : 0;
}


void MultiReader::Print()
{
  cout << "MultiReader " << GetName() << " : " << endl;
  for (std::map<TString, FamilyMember*>::const_iterator daug = m_daughters.begin(); daug != m_daughters.end(); daug++)
    cout << daug->first << " " << daug->second << endl;
}


TString MultiReader::ReaderClass(const BranchSpec& spec)
{
  std::map<TString, TString> classes;

  classes["Bool_t"] = "TreeReader::BasicReader<Bool_t>";
  classes["UShort_t"] = "TreeReader::BasicReader<UShort_t>";
  classes["Short_t"] = "TreeReader::BasicReader<Short_t>";
  classes["UInt_t"] = "TreeReader::BasicReader<UInt_t>";
  classes["Int_t"] = "TreeReader::BasicReader<Int_t>";
  classes["Float_t"] = "TreeReader::BasicReader<Float_t>";
  classes["Double_t"] = "TreeReader::BasicReader<Double_t>";

  classes["vector<UShort_t>"] = "TreeReader::StdVectorReader<UShort_t>";
  classes["vector<Short_t>"] = "TreeReader::StdVectorReader<Short_t>";
  classes["vector<UInt_t>"] = "TreeReader::StdVectorReader<UInt_t>";
  classes["vector<Int_t>"] = "TreeReader::StdVectorReader<Int_t>";
  classes["vector<Float_t>"] = "TreeReader::StdVectorReader<Float_t>";
  classes["vector<Double_t>"] = "TreeReader::StdVectorReader<Double_t>";

  classes["vector<std::vector<UShort_t>>"] = "TreeReader::StdVectorReader< std::vector<UShort_t> >";
  classes["vector<std::vector<Short_t>>"] = "TreeReader::StdVectorReader< std::vector<Short_t> >";
  classes["vector<std::vector<UInt_t>>"] = "TreeReader::StdVectorReader< std::vector<UInt_t> >";
  classes["vector<std::vector<Int_t>>"] = "TreeReader::StdVectorReader< std::vector<Int_t> >";
  classes["vector<std::vector<Float_t>>"] = "TreeReader::StdVectorReader< std::vector<Float_t> >";
  classes["vector<std::vector<Double_t>>"] = "TreeReader::StdVectorReader< std::vector<Double_t> >";

  classes["vector<std::vector<std::vector<UShort_t>>>"] = "TreeReader::StdVectorReader< std::vector<std::vector<UShort_t> > >";
  classes["vector<std::vector<std::vector<Short_t>>>"] = "TreeReader::StdVectorReader< std::vector<std::vector<Short_t> > >";
  classes["vector<std::vector<std::vector<UInt_t>>>"] = "TreeReader::StdVectorReader< std::vector<std::vector<UInt_t> > >";
  classes["vector<std::vector<std::vector<Int_t>>>"] = "TreeReader::StdVectorReader< std::vector<std::vector<Int_t> > >";
  classes["vector<std::vector<std::vector<Float_t>>>"] = "TreeReader::StdVectorReader< std::vector<std::vector<Float_t> > >";
  classes["vector<std::vector<std::vector<Double_t>>>"] = "TreeReader::StdVectorReader< std::vector<std::vector<Double_t> > >";
  
  TString type = spec.Type();
  return classes[type];
}


bool MultiReader::MakeReaderMacro(const TString& name, const TString& fileName, const TString& treeName,
                                  const TString& outputFileName, const TString& outputTreeName)
{
  TFile* inputFile = TFile::Open(fileName);
  if (!inputFile || !inputFile->IsOpen()) {
    cout << "ERROR : cannot open file " + fileName << "." << endl;
    return false;
  }

  TTree* inputTree = (TTree*)inputFile->Get(treeName);
  if (!inputTree) {
    cout << "ERROR : cannot find tree " + treeName + " in file " + fileName + "." << endl;
    return false;
  }

  std::ofstream header(name + ".h");
  std::ofstream impl(name + ".C");
  
  impl << "{" << endl;
  impl << "gROOT->Macro(\"" << name << ".h\");" << endl;
  impl << "inputFile = TFile::Open(\"" << fileName << "\");" << endl;
  impl << "inputTree = (TTree*)inputFile->Get(\"" << treeName << "\");" << endl;
  if (outputFileName != "") {
    impl << "outputFile = TFile::Open(\"" << outputFileName << "\", \"RECREATE\");";
    impl << "outputTree = new TTree(\"" << (outputTreeName != "" ? outputTreeName : treeName) << "\", \"\");" << endl;
  }
  header << "{" << endl;
  header << "MultiReader " << name << ";" << endl;
  const TObjArray* branches = inputTree->GetListOfBranches();
  for (int i = 0; i < branches->GetEntries(); i++) {
    BranchSpec spec(*(TBranch*)branches->At(i));
    TString function = FunctionString(spec);
    if (function == "") {
      cout << "ERROR : cannot find reader for branch " << spec.Name() << " of type " << spec.Type() << endl;
      return false;
    }
    header << Form("%-20s", spec.Name().Data()) << " = " << name << "." << function << ";" << endl;
  }
  header << "}" << endl;
  impl << name << ".ReadFrom(inputTree);" << endl;
  if (outputFileName != "") impl << name << ".WriteTo(outputTree);" << endl;
  impl << "for (long long i = 0; i < inputTree->GetEntries(); i++) {" << endl;
  impl << "  if (i % 10000 == 0) cout << \"Processing entry \" << i << \" of \" << inputTree->GetEntries() << endl;" << endl;
  impl << "  inputTree->GetEntry(i);" << endl;
  impl << "  // user code goes here" << endl;
  if (outputFileName != "") impl << "  outputTree->Fill();" << endl;
  impl << "}" << endl;
  if (outputFileName != "") {
    impl << "outputFile->cd();" << endl;
    impl << "outputTree->Write();" << endl;
    impl << "delete outputFile;" << endl;
  }
  impl << "}" << endl;
  return true;
}


bool MultiReader::MakeReaderClass(const TString& name, TTree& tree, const TString& exclude)
{
  std::ofstream header(name + ".h");
  std::ofstream impl(name + ".cxx");
  const TObjArray* branches = tree.GetListOfBranches();

  std::vector<TString> excludes;
  std::vector<TString> includes;
  TObjArray* tokens = exclude.Tokenize(",");
  for (int k = 0; k < tokens->GetEntries(); k++) {
    TString token = tokens->At(k)->GetName();
    if (token(0,1) == "+")
      includes.push_back(token(1, token.Length() - 1));
    else
      excludes.push_back(token);
  }
  
  header << "#ifndef ROOT_TreeReader_" << name << endl;
  header << "#define ROOT_TreeReader_" << name << endl;
  header << endl;
  header << "#include \"TreeReader/BasicReader.h\"" << endl;
  header << "#include \"TreeReader/BranchReader.h\"" << endl;
  header << "#include \"TreeReader/ArrayReader.h\"" << endl;
  header << "#include \"TreeReader/StdVectorReader.h\"" << endl;
  header << "#include \"TString.h\"" << endl;
  header << "#include <string>" << endl;
  header << endl;
  header << "class " << name << " : public TreeReader::ReaderBase<bool> {" << endl;
  header << endl;
  header << " public:" << endl;
  header << endl;
  header << "  " << name << "(FamilyMember* parent = 0);" << endl;
  header << "  virtual ~" << name << "() { }" << endl;
  header << endl;
  header << "  const bool& GetValue (unsigned int i = 0) const { return m_status; }" << endl;
  header << "  void SetValue (const bool& val, unsigned int i = 0) { }" << endl;
  header << endl;

  std::map<TString, std::vector<TString> > types;
  std::map<TString, TString> names;

  //header << "  #ifndef __GCCXML__" << endl;

  for (int i = 0; i < branches->GetEntries(); i++) {
    BranchSpec spec(*(TBranch*)branches->At(i));
    bool skip = false;
    for (unsigned int k = 0; k < excludes.size(); k++)
      if (spec.Name().Index(TRegexp(excludes[k])) >= 0) {
        bool save = false;
        for (unsigned int j = 0; j < includes.size(); j++) {
          if (spec.Name().Index(TRegexp(includes[j])) >= 0) {
            save = true;
            break;
          }
        }
        if (save) continue;
        cout << "Skipping " << spec.Name() << endl;
        skip = true;
        break;
      }
    if (skip) continue;
    TString readerClass = ReaderClass(spec);
    if (readerClass == "") {
      cout << "ERROR : cannot find reader for branch " << spec.Name() << " of type " << spec.Type() << endl;
      return false;
    }
    types[readerClass].push_back(spec.SafeName());
    names[spec.SafeName()] = spec.Name();
  }

  for (std::map<TString, std::vector<TString> >::const_iterator type = types.begin(); type != types.end(); type++) {
      header << "  " << type->first << " ";
    for (std::vector<TString>::const_iterator var = type->second.begin(); var != type->second.end(); var++)
      header << (var ==  type->second.begin() ? "" : "   , ") << *var << endl;
    header << "  ;" << endl;
  }
  //header << "  #endif" << endl;
  header << "  bool m_status;" << endl;
  header << endl;
  header << "  ClassDef( " <<  name << ", 0 )" << endl;
  header << endl;
  header << "};" << endl;
  header << endl;
  header << "#endif" << endl;

  impl << "#include \"" + name + ".h\"" << endl;
  impl << endl;
  impl << "using namespace std;" << endl;
  impl << "using namespace TreeReader;" << endl;
  impl << endl;
  impl << name + "::" + name + "(FamilyMember* parent)" << endl;
  impl << "  : ReaderBase<bool>(\"" + name + "\", parent)" << endl;
  for (std::map<TString, std::vector<TString> >::const_iterator type = types.begin(); type != types.end(); type++) {
    for (std::vector<TString>::const_iterator var = type->second.begin(); var != type->second.end(); var++)
      impl <<  "  , " << *var << "(\"" << names[*var] << "\", this)" << endl;
  }
  impl << "  , m_status(true)" << endl;
  impl << "{" << endl;
  impl << "}" << endl;

  
  return true;
}

