#ifdef __CINT__

#include <stdint.h>
#include <vector>
//#include "TLorentzVector.h"

#include "TreeReader/TreeAbsReader.h"
#include "TreeReader/MultiReader.h"
// #include "TreeReader/CutFlow.h"

#pragma link C++ namespace TreeReader;

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;

#pragma link C++ enum TreeReader::BranchType;
// 
#pragma link C++ class TreeReader::FamilyMember+;
#pragma link C++ class TreeReader::BranchSpec+;
// 
// #pragma link C++ class TreeReader::Energy+;
// #pragma link C++ class TreeReader::Mass+;
// #pragma link C++ class TreeReader::PxPyPz+;
// #pragma link C++ class TreeReader::PtEtaPhi+;

// #pragma link C++ class std::vector<short>+;
// #pragma link C++ class std::vector<uint32_t>+;
// #pragma link C++ class std::vector<int>+;
// #pragma link C++ class std::vector<double>+;
// #pragma link C++ class std::vector<float>+;
// #pragma link C++ class std::vector<std::string>+;
// #pragma link C++ class std::vector<TVector3>+;
// #pragma link C++ class std::vector<TLorentzVector>+;
// 
// #pragma link C++ class std::vector<std::vector<short> >+;
// #pragma link C++ class std::vector<std::vector<uint32_t> >+;
// #pragma link C++ class std::vector<std::vector<int> >+;
// #pragma link C++ class std::vector<std::vector<double> >+;
// #pragma link C++ class std::vector<std::vector<float> >+;
// 
// #pragma link C++ class std::vector<std::vector<std::vector<short> > >+;
// #pragma link C++ class std::vector<std::vector<std::vector<uint32_t> > >+;
// #pragma link C++ class std::vector<std::vector<std::vector<int> > >+;
// #pragma link C++ class std::vector<std::vector<std::vector<double> > >+;
// #pragma link C++ class std::vector<std::vector<std::vector<float> > >+;
// 
// #pragma link C++ class TreeReader::ReaderType<bool>+;
// #pragma link C++ class TreeReader::ReaderBase<bool>+;
// #pragma link C++ class TreeReader::BasicReader<bool>+;
// #pragma link C++ class TreeReader::CachingReader<bool>+;
// #pragma link C++ class TreeReader::ArrayReader<bool>+;
// 
// #pragma link C++ class TreeReader::ReaderType<short>+;
// #pragma link C++ class TreeReader::ReaderBase<short>+;
// #pragma link C++ class TreeReader::BasicReader<short>+;
// #pragma link C++ class TreeReader::BranchReader<short>+;
// #pragma link C++ class TreeReader::CachingReader<short>+;
// #pragma link C++ class TreeReader::ArrayReader<short>+;
// #pragma link C++ class TreeReader::StdVectorReader<short>+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<short> >+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<std::vector<short> > >+;
// 
// #pragma link C++ class TreeReader::ReaderType<uint32_t>+;
// #pragma link C++ class TreeReader::ReaderBase<uint32_t>+;
// #pragma link C++ class TreeReader::BasicReader<uint32_t>+;
// #pragma link C++ class TreeReader::BranchReader<uint32_t>+;
// #pragma link C++ class TreeReader::CachingReader<uint32_t>+;
// #pragma link C++ class TreeReader::ArrayReader<uint32_t>+;
// #pragma link C++ class TreeReader::StdVectorReader<uint32_t>+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<uint32_t> >+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<std::vector<uint32_t> > >+;
// 
// #pragma link C++ class TreeReader::ReaderType<int>+;
// #pragma link C++ class TreeReader::ReaderBase<int>+;
// #pragma link C++ class TreeReader::BasicReader<int>+;
// #pragma link C++ class TreeReader::BranchReader<int>+;
// #pragma link C++ class TreeReader::CachingReader<int>+;
// #pragma link C++ class TreeReader::ArrayReader<int>+;
// #pragma link C++ class TreeReader::StdVectorReader<int>+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<int> >+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<std::vector<int> > >+;
// 
// #pragma link C++ class TreeReader::ReaderType<double>+;
// #pragma link C++ class TreeReader::ReaderBase<double>+;
// #pragma link C++ class TreeReader::BasicReader<double>+;
// #pragma link C++ class TreeReader::BranchReader<double>+;
// #pragma link C++ class TreeReader::CachingReader<double>+;
// #pragma link C++ class TreeReader::ArrayReader<double>+;
// #pragma link C++ class TreeReader::StdVectorReader<double>+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<double> >+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<std::vector<double> > >+;
// 
// #pragma link C++ class TreeReader::ReaderType<float>+;
// #pragma link C++ class TreeReader::ReaderBase<float>+;
// #pragma link C++ class TreeReader::CachingReader<float>+;
// #pragma link C++ class TreeReader::BasicReader<float>+;
// #pragma link C++ class TreeReader::BranchReader<float>+;
// #pragma link C++ class TreeReader::ArrayReader<float>+;
// #pragma link C++ class TreeReader::StdVectorReader<float>+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<float> >+;
// #pragma link C++ class TreeReader::StdVectorReader<std::vector<std::vector<float> > >+;
// 
// #pragma link C++ class TreeReader::ReaderBase<TVector3>+;
// #pragma link C++ class TreeReader::CachingReader<TVector3>+;
// #pragma link C++ class TreeReader::TVector3Reader<TreeReader::PxPyPz, float>+;
// #pragma link C++ class TreeReader::TVector3Reader<TreeReader::PtEtaPhi, float>+;
// #pragma link C++ class TreeReader::TVector3Reader<TreeReader::PtPhiPz, float>+;
// 
// #pragma link C++ class TreeReader::ReaderBase<TLorentzVector>+;
// #pragma link C++ class TreeReader::CachingReader<TLorentzVector>+;
// #pragma link C++ class TreeReader::TLorentzVectorReader<TreeReader::Energy, TreeReader::PxPyPz, float>+;
// #pragma link C++ class TreeReader::TLorentzVectorReader<TreeReader::Energy, TreeReader::PtEtaPhi, float>+;
// #pragma link C++ class TreeReader::TLorentzVectorReader< TreeReader::Mass, TreeReader::PtEtaPhi, float>+;
// 
// #pragma link C++ class TreeReader::ReaderType<std::string>+;
// #pragma link C++ class TreeReader::ReaderBase<std::string>+;
// #pragma link C++ class TreeReader::CachingReader<std::string>+;
// #pragma link C++ class TreeReader::StdVectorReader<std::string>+;

#pragma link C++ class TreeReader::TreeAbsReader+;
#pragma link C++ class TreeReader::MultiReader+;

// #pragma link C++ class TreeReader::CutFlow+;

#endif
