#ifndef ROOT_Hfitter_HftConstraint
#define ROOT_Hfitter_HftConstraint

#include "RooArgList.h"
#include "RooAbsPdf.h"

namespace Hfitter {

  /** @class HftConstraint
      @author Nicolas Berger
  */

  class HftConstraint {

   public:

    //! default constructor
    HftConstraint(RooAbsPdf& pdf, const RooAbsPdf& constrainedPdf, const RooArgList& auxObservables);
    virtual ~HftConstraint() { }

    //! returns the constraint name
    TString GetName() const { return Pdf()->GetName(); }

    const RooArgList& AuxObservables() const { return m_auxObservables; }
    const RooArgList& ConstrainedParameters() const { return m_constrainedParameters; }
    RooAbsPdf* Pdf() const { return m_pdf; }

    bool Randomize();
    
    static RooArgSet* Overlap(const RooAbsPdf& pdf, const RooAbsPdf& constrainedPdf);

   protected:

    RooAbsPdf* m_pdf;
    RooArgList m_auxObservables, m_constrainedParameters;
  };
}

#endif
