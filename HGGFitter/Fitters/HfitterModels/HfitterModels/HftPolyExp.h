#ifndef HftPolyExp_hh
#define HftPolyExp_hh

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

namespace Hfitter {

  /** @class HftPolyExp
      @author Nicolas Berger

    RooAbsPdf implementation for the product of a polynomial and an expononential. The PDF form is
    P(x) = (x - x0)^n*exp(-alpha*(x - x0))
  */
   
  
  class HftPolyExp : public RooAbsPdf {
  public:
    
    HftPolyExp() { }

    //! Constructor. @param name : PDF name @param title : PDF title @param _x : PDF dependent variable @param _x0 : overall offset for the dependent @param _n : order of the polynomial @param _alpha : coefficient of the exponential.
    HftPolyExp(const char *name, const char *title,
                      RooAbsReal& _x, RooAbsReal& _x0, RooAbsReal& _n, RooAbsReal& _alpha);
    
    //! copy constructor. @param other : the instance to copy. @param name : optional new name name for the new PDF (otherwise taken from the original)
    HftPolyExp(const HftPolyExp& other, const char* name=0) ;
    virtual ~HftPolyExp() { }

    //! returns a clone of the PDF with a new name. @param newName : name of the cloned PDF
    virtual TObject* clone(const char* newName) const { return new HftPolyExp(*this, newName); }
    
    //! determines whether an analytical integral can be computed. Here always true. @param allVars : the PDF parameters. @param analVars : . @param rangeName : .
    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const ;
    //! returns the analytical for of the PDF integral (avoids numerical integration). @param code : . @param rangeName : .
    Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const ;

  protected:

    //! evaluates the PDF for the current parameter values
    Double_t evaluate() const ;
    
    //! helper function to calculate the integral
    Double_t integralFromZero(Double_t xMax) const;
    
    RooRealProxy x ;     //!< dependent variable
    RooRealProxy x0 ;    //!< overall shift in the dependent
    RooRealProxy n ;     //!< polynomial order
    RooRealProxy alpha ; //!< exponential coefficient
    
    ClassDef(Hfitter::HftPolyExp, 1); //!< ROOT boilerplate
  };
}

#endif
