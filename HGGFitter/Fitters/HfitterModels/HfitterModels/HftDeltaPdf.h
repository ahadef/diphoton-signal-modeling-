// $Id: HftDeltaPdf.h,v 1.2 2009/01/30 16:39:11 hoecker Exp $   
// Author: Nicolas Berger

// Higgs signal PDF

#ifndef ROOT_Hfitter_HftDeltaPdf
#define ROOT_Hfitter_HftDeltaPdf

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

namespace Hfitter {

  /** @class HftDeltaPdf

    RooAbsPdf implementation of a delta function
  */
       
  class HftDeltaPdf : public RooAbsPdf {
    
   public:
    
    //! Constructor. @param name : PDF name @param title : PDF title @param X : PDF dependent variable @param X0 : delta function position @param relEpsilon : the ratio of the delta function width to the position value.
    HftDeltaPdf( const char* name, const char* title, RooAbsReal& X, 
                 Double_t X0, Double_t relEpsilon = 1E-3);
    
    //! copy constructor. @param other : the instance to copy. @param name : a new name
    HftDeltaPdf( const HftDeltaPdf& other, const char* name );
    
    virtual ~HftDeltaPdf();
        
    //! returns a clone of the PDF with a new name. @param newName : name of the cloned PDF
    virtual TObject* clone(const char* newName) const { return new HftDeltaPdf(*this, newName); }
    
    //! determines whether an analytical integral can be computed. Here always true. @param allVars : the PDF parameters. @param analVars : . @param rangeName : .
    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const ;
    //! returns the analytical for of the PDF integral (avoids numerical integration). @param code : . @param rangeName : .
    Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const ;
    
  protected:
    
    //! evaluates the PDF for the current parameter values
    Double_t evaluate() const ; 

    RooRealProxy x; //!< dependent variable
    double x0;      //!< delta function peak position
    double epsilon; //!< delta function width 
    
    ClassDef(Hfitter::HftDeltaPdf, 1);  //!< ROOT boilerplate
  };
}

#endif
