#ifndef ROOT_Hfitter_HftMultiGaussianBuilder
#define ROOT_Hfitter_HftMultiGaussianBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>
#include "TMatrixDSym.h"


namespace Hfitter {
  
  class HftMultiGaussianBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HftMultiGaussianBuilder() { }    
    virtual ~HftMultiGaussianBuilder() { }

    void Setup(const char* v1Name = "var1", const char* v2Name = "var2",
               const char* m1Name = "mean1", const char* m2Name = "mean2",
               double cov1 = 1, double cov2 = 1, double cov12 = 0);

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HftMultiGaussianBuilder, 0);

  private:
    
    std::vector<TString> m_varNames;
    std::vector<TString> m_meanNames;
    TMatrixDSym m_covMatrix;
  };
}

#endif
