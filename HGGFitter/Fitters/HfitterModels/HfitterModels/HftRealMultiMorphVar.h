#ifndef ROOT_Hfitter_HftRealMultiMorphVar
#define ROOT_Hfitter_HftRealMultiMorphVar

#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftRealMorphVar.h"
#include "RooAbsReal.h"
#include <vector>

namespace Hfitter {

  /** @class HftRealMultiMorphVar
      @author Nicolas Berger

    An implementation of RooAbsReal to represent a "morphing parameter" -- i.e. a PDF parameter whose value depends on that of another variable. 
    The parameter value is calculated by linearly interpolating between values at different points.
  */

  class HftRealMultiMorphVar : public RooAbsReal, HftAbsParameters
  {
   public:
  
    //! default constructor    
    HftRealMultiMorphVar() { }

    //! copy constructor. @param other : the HftRealMultiMorphVar to copy, @param newName : the new variable name.
    HftRealMultiMorphVar(const HftRealMultiMorphVar& other, const TString& newName);
    
    //! creates a clone of this variable. @param newName : the new variable name.
    virtual TObject* clone(const char* newName) const { return new HftRealMultiMorphVar(*this, newName); }
    
    //! destructor
    virtual ~HftRealMultiMorphVar() {}
        
    void Add(const HftRealMorphVar* morphVar) { m_morphVars.push_back(morphVar); }
 
     //! returns a RooArgList containing the RooRealVars defined for each point.
    RooArgList Parameters() const { return RooArgList(); } //FIXME
 
    //! returns the number of points defined for the interpolation.
    unsigned int NDimensions() const { return m_morphVars.size(); }
    
    //! returns the name of an interpolation point. @param i : point index.
    const HftRealMorphVar* MorphVar(unsigned int i) { return m_morphVars[i]; }
    
  protected:

    //! Implementation of virtual function RooAbsReal::evaluate, using linear interpolation between the interpolation points.
    double evaluate() const {return 0.; }; // FIXME

    std::vector<const HftRealMorphVar*> m_morphVars; 

    ClassDef(HftRealMultiMorphVar, 0)
  };
}
#endif
