#ifndef ROOT_Hfitter_HftInstanceSpec
#define ROOT_Hfitter_HftInstanceSpec

#include "TObject.h"
#include "TString.h"

namespace Hfitter {

  /** @class HftInstance
      @author Nicolas Berger

    Base class for classes that can be instantiated from their class-names and configured via the Setup mechanism
    See the HftInstanceSpecs class for the instantiation mechanism
  */

  class HftInstance : public TObject {

  public:
    //! constructor
    HftInstance() { }
    //! destructor
    virtual ~HftInstance() { }

    void Setup() { }

    static HftInstance* Create(const TString& className, const TString& args, bool verbose = false);

    ClassDef(Hfitter::HftInstance, 1);
  };
}

#endif
