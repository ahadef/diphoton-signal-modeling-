#ifndef ROOT_Hfitter_HftDataSet
#define ROOT_Hfitter_HftDataSet

#include "RooAbsData.h"
#include "TFile.h"
#include "TTree.h"

class RooAbsCategory;
class RooDataSet;

namespace Hfitter {

  class HftModel;
  
  /** @class HftData      
      @author Nicolas Berger
      
      Class wrapping around a RooDataSet and providing functions to open ROOT files.
  */
  
  class HftData : public TNamed {

   public:

    //! static constructor creating a RooDataSet from a ROOT file. @param fileName : the name fo the file. @param treeName : the name fo the tree. 
    //! @param model : the name of the model to use (specifies the variables to load),
    //! @param cut : a cut to apply on the tree before import into the dataset,
    //! @param weightVarName : weight variable name, if dataSet is to be weighted.
    static HftData* Open(const TString& fileName, const TString& treeName, const HftModel& model,
                         const TString& cut = "", const TString& weightVarName = "", 
                         const RooArgList& extraVars = RooArgList());

    //! desctructor
    virtual ~HftData();

    //! automatic conversion to a pointer to the RooDataSet.
    operator RooDataSet* () { return m_data; }

    //! automatic conversion to a reference to the RooDataSet.
    operator RooDataSet& () { return *m_data; }

    //! overloading of (), returning a reference to the RooDataSet.
    RooDataSet& operator()() { return *m_data; }

    //! returns the tree
    TTree* Tree() { return m_tree; }
    
    //! returns the file
    TFile* File() { return m_file; }
    
    //! returns the dataset.
    RooDataSet* DataSet() { return m_data; }
  
    static bool FillAsimov(RooDataSet*& dataset, RooAbsPdf& pdf, const RooArgList& dependents, const RooArgList& others);
    static bool FillAsimov(RooDataSet& dataset, RooAbsPdf& pdf, const RooArgList& dependents, const RooArgList& others,
                           const std::vector<int>& currentBin, double binVolume);

    static RooAbsData* SelectCategory(RooAbsData& data, const RooAbsCategory& cat, const TString& catState);

    static bool Save(const RooDataSet& data, const HftModel& model, const TString& fileName, 
                     const TString& treeName = "tree", const TString& weightVarName = "");

    static bool AddGhosts(RooAbsData& data, HftModel& model, const TString& varName, double minVal, double maxVal, 
                          double step, double weight = 1E-5);
    
    static TString PrepareDirs(const TString& path);
    
    static void PrintData(const RooAbsData& data);

  private:

    //! the real constructor. Not useful in general...
    HftData(const TString& fileName, const TString& treeName, const HftModel& model, const TString& cut = "",
            const TString& weightVarName = "", const RooArgList& extraVars = RooArgList());

    TFile* m_file;       //!< pointer to the opened TFile
    TTree* m_tree;       //!< pointer to the TTree used to create the dataset
    RooDataSet* m_data;  //!< the dataset
    
    ClassDef(Hfitter::HftData, 1)
  };
}

#endif
