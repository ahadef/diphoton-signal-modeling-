#ifndef ROOT_Hfitter_Options
#define ROOT_Hfitter_Options

#include <vector>
#include "TString.h"
#include "RooCmdArg.h"
#include "RooLinkedList.h"
#include "RooGlobalFunc.h"

#include "RooAbsData.h"
#include "RooAbsReal.h"

namespace Hfitter {

  class HftModel;
  
  /** @class Options
      @author Nicolas Berger

  */

  class HftAbsOption {
  public:
    HftAbsOption() { }
    virtual ~HftAbsOption() { }
    virtual HftAbsOption* Clone() const = 0;
    virtual TString Name() const = 0;
    virtual TString Str() const { return Name(); }
    virtual bool Process(RooLinkedList& /*cmdList*/, HftModel& /*model*/) const { return true; }
    RooLinkedList List(HftModel& model) { RooLinkedList list; Process(list, model); return list; }
  };

  class RooFitOpt : public HftAbsOption
  {
  public:
    RooFitOpt(const RooCmdArg& arg) : m_arg(arg) { }
    virtual ~RooFitOpt() { }
    HftAbsOption* Clone() const { return new RooFitOpt(m_arg); }
    TString Name() const { return Form("RooFit::%s = %d %s", m_arg.GetName(), m_arg.getInt(0), m_arg.getString(0)); }
    bool Process(RooLinkedList& cmdList, HftModel& /*model*/) const { cmdList.Add(new RooCmdArg(m_arg)); return true; }
    const RooCmdArg& Arg() const { return m_arg; }
  private:
    RooCmdArg m_arg;
  };

  class StrOpt : public HftAbsOption
  {
  public:
    StrOpt(const TString& name) : m_name(name) { }
    virtual ~StrOpt() { }
    HftAbsOption* Clone() const { return new StrOpt(m_name); }
    TString Name() const { return m_name; }
    TString Str() const { return Name(); }
    bool Process(RooLinkedList& /*cmdList*/, HftModel& /*model*/) const { return true; }
    bool IsIn(const TString& opt, StrOpt& found) const;
    bool IsIn(const TString& opt) const { StrOpt dummy(*this); return IsIn(opt, dummy); }
  protected:
    TString m_name;
  };

  class StrArgOpt : public HftAbsOption
  {
  public:
    StrArgOpt(const TString& name, const TString& arg1 = "", const TString& arg2 = "", const TString& arg3 = "");
    StrArgOpt(const TString& name, const std::vector<TString>& args) : m_name(name), m_args(args) { }
    virtual ~StrArgOpt() { }
    HftAbsOption* Clone() const { return new StrArgOpt(m_name, m_args); }
    TString Name() const { return m_name; }
    TString ArgStr() const;
    TString Str() const { return Name() + "(" + ArgStr() + ")"; }
    const std::vector<TString>& Args() const { return m_args; }
    unsigned int NArgs() const { return m_args.size(); }
    TString Arg(unsigned int i) const { return m_args[i]; }
    bool Process(RooLinkedList& /*cmdList*/, HftModel& /*model*/) const { return true; }
    bool IsIn(const TString& opt, StrArgOpt& found, unsigned int minNArgs = 0, bool reqInt = false) const;
  protected:
    TString m_name;
    std::vector<TString> m_args;
  };

  class RangeOpt : public StrArgOpt
  {
    public:
    RangeOpt(const TString& range1 = "", const TString& range2 = "", const TString& range3 = "")
      : StrArgOpt("Range", range1, range2, range3) { }
    RangeOpt(const std::vector<TString>& ranges) : StrArgOpt("Range", ranges) { }
    virtual ~RangeOpt() { }
    HftAbsOption* Clone() const { return new RangeOpt(m_args); }
    bool Process(RooLinkedList& cmdList, HftModel& model) const;
  };

  class MinosVarOpt : public StrArgOpt
  {
  public:
    MinosVarOpt(const TString& var1 = "", const TString& var2 = "", const TString& var3 = "")
      : StrArgOpt("MinosVar", var1, var2, var3) { }
    MinosVarOpt(const std::vector<TString>& vars) : StrArgOpt("MinosVar", vars) { }
    virtual ~MinosVarOpt() { }
    HftAbsOption* Clone() const { return new MinosVarOpt(m_args); }
    bool Process(RooLinkedList& cmdList, HftModel& model) const;
  };

  class PlotErrorsOpt : public HftAbsOption
  {
   public:
    PlotErrorsOpt(RooAbsData::ErrorType errorType = RooAbsData::Auto) : m_errorType(errorType) { }
    virtual ~PlotErrorsOpt() { }
    HftAbsOption* Clone() const { return new PlotErrorsOpt(m_errorType); }
    TString Name() const { return "PlotErrors"; }
    TString Str() const;
    bool IsIn(const TString& opt, PlotErrorsOpt& found) const;
    RooAbsData::ErrorType ErrorType() const { return m_errorType; }
   private:
    RooAbsData::ErrorType m_errorType;
  };

  class PlotScaleOpt : public HftAbsOption
  {
   public:
    PlotScaleOpt(RooAbsReal::ScaleType scaleType = RooAbsReal::Relative, double scale = -1) 
      : m_scaleType(scaleType), m_scale(scale) { }
    virtual ~PlotScaleOpt() { }
    HftAbsOption* Clone() const { return new PlotScaleOpt(m_scaleType, m_scale); }
    TString Name() const { return "PlotScale"; }
    TString Str() const;
    bool IsIn(const TString& opt, PlotScaleOpt& found) const;
    RooAbsReal::ScaleType ScaleType() const { return m_scaleType; }
    double Scale() const { return m_scale; }
   private:
    RooAbsReal::ScaleType m_scaleType;
    double m_scale;
  };

  class NoDefaults : public HftAbsOption
  {
    public:
    NoDefaults() { }
    virtual ~NoDefaults() { }
    HftAbsOption* Clone() const { return new NoDefaults(); }
    TString Name() const { return "NoDefaults"; }
    bool Process(RooLinkedList& cmdList, HftModel& model) const;
  };

  class Options : public HftAbsOption {

    public:

      Options() { AddDefaults(); }
      Options(const char* opt);
      Options(const TString& opt);
      Options(const HftAbsOption& arg1) { AddDefaults(); Add(arg1); }
      Options(const HftAbsOption& arg1, const HftAbsOption& arg2) { AddDefaults(); Add(arg1); Add(arg2); }
      Options(const HftAbsOption& arg1, const HftAbsOption& arg2, const HftAbsOption& arg3) { AddDefaults(); Add(arg1); Add(arg2); Add(arg3); }
      Options(const HftAbsOption& arg1, const HftAbsOption& arg2, const HftAbsOption& arg3, const HftAbsOption& arg4) { AddDefaults(); Add(arg1); Add(arg2); Add(arg3); Add(arg4); }
      Options(const HftAbsOption& arg1, const HftAbsOption& arg2, const HftAbsOption& arg3, const HftAbsOption& arg4, const HftAbsOption& arg5) { AddDefaults(); Add(arg1); Add(arg2); Add(arg3); Add(arg4); Add(arg5); }
      Options(const Options& other);
      virtual ~Options();
      HftAbsOption* Clone() const { return new Options(*this); }
      TString Name() const { return "Options"; }
      
      void AddDefaults();
      void Add(const HftAbsOption& opt);
      bool Remove(const TString& str);

      void Parse(const TString& opt);
      
      bool Process(RooLinkedList& cmdList, HftModel& model) const;
      TString Str() const;

      int Verbosity() const { int i; return GetInt("Verbose", i) ? i : 0; }
      
      HftAbsOption* Find(const TString& name) const;
      bool GetStr(const TString& name, TString& arg) const;
      bool GetInt(const TString& name, int& arg) const;
      
      HftAbsOption* Opt(unsigned int i) const { return m_options[i]; }
      unsigned int Size() const { return m_options.size(); }
      
      static TString defaults;

      static void SetHfitterStyle();
      
  private:
    std::vector<Hfitter::HftAbsOption*> m_options;
  };
}

#endif
