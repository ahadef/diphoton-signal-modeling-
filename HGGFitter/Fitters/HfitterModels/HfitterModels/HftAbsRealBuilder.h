#ifndef ROOT_Hfitter_HftAbsRealBuilder
#define ROOT_Hfitter_HftAbsRealBuilder

#include "HfitterModels/HftAbsNodeBuilder.h"

#include "RooAbsReal.h"
#include "RooWorkspace.h"
#include "TString.h"

namespace Hfitter {

  /** @class HftAbsRealBuilder
      @author Nicolas Berger
  */

  class HftAbsRealBuilder : public HftAbsNodeBuilder {

   public:

    //! constructor
    HftAbsRealBuilder() { }

    //! destructor
    virtual ~HftAbsRealBuilder() { }

    //! returns the built PDF object. Must be implemented in derived classes.
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsReal* Real(const TString& name, RooWorkspace& workspace) const = 0;

    static HftAbsRealBuilder* Create(const TString& name, const TString& args, bool verbose = false);

    ClassDef(HftAbsRealBuilder, 1);
  };
}

#endif
