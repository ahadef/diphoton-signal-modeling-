#ifndef ROOT_Hfitter_HftModelSplitterV4
#define ROOT_Hfitter_HftModelSplitterV4

#include <vector>
#include "TNamed.h"
#include "RooWorkspace.h"
#include "HfitterModels/HftDatacardReaderV4.h"

class RooSimPdfBuilder;
class RooSimultaneous;

namespace Hfitter {

  /** @class HftModelSplitterV4
      @author Nicolas Berger
      @author Andreas Hoecker

    Helper class that creates a split fit model from one or many non-split ones. The splitting instructions are provided in a datacard.
    The type of the returned model may be
    - @c HftSplitModel : the PDF form is identical in all category states (only the parameter values mey be category-specific). A single @c HftSimpleModel should be provided as template.
    - @c HftMultiPdfModel, in which the PDF form may differ for different category states. As manye @c HftSimpleModel's should be provided as there are different PDF forms.
    This class is meant to be used from within @c HftModelBuilder
  */
 

  class HftModelSplitterV4 : public TNamed {

    public:
      //! constructor. @param name : the name of the model. @param originalModels : vector of non-split models used as templates. @param datacardReader : an instance of the datacard parser
      HftModelSplitterV4(const TString& name, const std::vector<RooAbsPdf*>& modelPdfs, const HftDatacardReaderV4& datacardReader,
                       RooWorkspace& workspace, bool verbose = false) 
        : TNamed(name.Data(), ""),
          m_modelPdfs(modelPdfs),
          m_datacardReader(&datacardReader),
          m_workspace(&workspace),
          m_verbose(verbose) { }

      virtual ~HftModelSplitterV4() { }

      //! returns the split model
      RooSimultaneous* Pdf();

      //! returns the number of original models provided
      unsigned int NModels() const { return m_modelPdfs.size(); }
      
      //! returns the i'th original model. @param i : model index
      const RooAbsPdf* ModelPdf(unsigned int i) const { return m_modelPdfs[i]; }

    private:

      std::vector<RooAbsPdf*> m_modelPdfs; //!< original models used as templates
      const HftDatacardReaderV4* m_datacardReader;           //!< an instance of the datacard parser      
      RooWorkspace* m_workspace;
      bool m_verbose;
    ClassDef(Hfitter::HftModelSplitterV4, 1);
  };
}

#endif
