// -*- C++ -*-
/*****************************************************************************
 * Project: BaBar                                                            *
 * Package: GrEATools                                                        *
 *    File: $Id: HftDoubleGaussian.h,v 1.2 2007/12/13 20:34:19 hoecker Exp $
 * Author :                                                                  *
 *          Nicolas Berger, CERN                                             *
 *****************************************************************************/

#ifndef HftDoubleGaussian_hh
#define HftDoubleGaussian_hh

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

// These need to be somewhere for dictionary generation, keep them here for now
#include "RooGaussian.h"
#include "RooUniform.h"
#include "RooBifurGauss.h"
#include "RooLandau.h"

class RooRealVar;

namespace Hfitter {

  /** @class HftDoubleGaussian

    RooAbsPdf implementation for the sum of two Gaussians
  */
     
  class HftDoubleGaussian : public RooAbsPdf {
   public:
    //! Constructor. @param name : PDF name @param title : PDF title @param _x : PDF dependent variable @param _mean : peak position of the first Gaussian @param _sigma : width of the first Gaussian @param _relShift : peak position of the second Gaussian relative to the first one @param _relWid : ratio of the width of the second Gaussian to that of the first one @param _relNorm : fraction of the first Gaussian in the total PDF normalization.
    HftDoubleGaussian(const char *name, const char *title,
                      RooAbsReal& _x, RooAbsReal& _mean, RooAbsReal& _sigma, 
                      RooAbsReal& _relShift, RooAbsReal& _relWid, RooAbsReal& _relNorm);
    
    //! copy constructor. @param other : the instance to copy. @param name : optional new name for the new PDF (otherwise taken from the original)
    HftDoubleGaussian(const HftDoubleGaussian& other, const char* name=0) ;
    virtual ~HftDoubleGaussian() { }

    //! returns a clone of the PDF with a new name. @param newName : name of the cloned PDF
    virtual TObject* clone(const char* newName) const { return new HftDoubleGaussian(*this, newName); }
    
    //! determines whether an analytical integral can be computed. Here always true. @param allVars : the PDF parameters. @param analVars : . @param rangeName : .
    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const ;
    //! returns the analytical for of the PDF integral (avoids numerical integration). @param code : . @param rangeName : .
    Double_t analyticalIntegral(Int_t code, const char* rangeName = 0) const ;
    
    //! determines whether a custom event generator is available. Here always true. @param directVars : . @param generateVars : . @param staticInitOK : .
    Int_t getGenerator(const RooArgSet& directVars, RooArgSet &generateVars, Bool_t staticInitOK=kTRUE) const;
    //! returns a generated event. @param code : .
    void generateEvent(Int_t code);
    
  protected:
    
    //! evaluates the PDF for the current parameter values    
    Double_t evaluate() const;

    RooRealProxy x ;       //!< dependent variable
    RooRealProxy mean ;    //!< peak position of the first Gaussian 
    RooRealProxy sigma ;   //!< width of the first Gaussian
    RooRealProxy relShift; //!< peak position of the second Gaussian relative to the first one
    RooRealProxy relWidth; //!< ratio of the width of the second Gaussian to that of the first one 
    RooRealProxy relNorm;  //!< fraction of the first Gaussian in the total PDF normalization.
 
    ClassDef(Hfitter::HftDoubleGaussian, 1); //!< ROOT boilerplate
  };
}

#endif
