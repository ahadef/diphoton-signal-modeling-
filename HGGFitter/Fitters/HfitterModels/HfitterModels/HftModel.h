#ifndef ROOT_Hfitter_HftModel
#define ROOT_Hfitter_HftModel

#include "HfitterModels/HftConstraint.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/Options.h"

#include "RooAbsPdf.h"
#include "RooDataHist.h"
#include "RooLinkedList.h"
#include "RooWorkspace.h"

class RooDataSet;

namespace Hfitter {

  /** @class HftModel
      @author Nicolas Berger
  */

  class HftModel {

   public:

    //! Default constructor
    HftModel(RooWorkspace& workspace, TFile* workspaceFile, RooAbsPdf& pdf,
             const RooArgList& observables, const RooArgList& auxObservables,
             const std::vector<Hfitter::HftModel*>& componentModels, const std::vector<Hfitter::HftModel*>& categoryModels,
             const std::vector<Hfitter::HftConstraint*>& constraints, RooAbsReal* norm = 0, const TString& name = "", 
             bool ownPDF = false, bool ownWS = false);
    
    virtual ~HftModel();

    //! the name of the model, identical to that of its PDF.
    TString GetName() const { return m_name; }

    RooWorkspace* Workspace() const { return m_workspace; }
    TFile* WorkspaceFile() const { return m_workspaceFile; }
    
    //! const method to access the PDF
    const RooAbsPdf* Pdf() const { return m_pdf; }

    //! non-const method to access the PDF
    RooAbsPdf* Pdf() { return m_pdf; }

    RooAbsReal* Real(const TString& name) { return (RooAbsReal*)Parameters().find(name); }
    RooRealVar* Var(const TString& name);
    RooCategory* Cat(const TString& name);
    RooArgList FreeParameters() const;
    RooArgList Pars(const TString& par1, const TString& par2 = "", const TString& par3 = "", const TString& par4 = "",
                    const TString& par5 = "", const TString& par6 = "", const TString& par7 = "", const TString& par8 = "") const;
    
    //! the @c RooArgList of all (non-category) dependent variables.
    RooArgList& Observables() { return m_observables; }
    const RooArgList& Observables() const { return m_observables; }

    //! access dependents by name (return NULL if dependent is not found)
    RooRealVar* Observable(const TString& name) const;
    RooRealVar* Observable(int i) const { return dynamic_cast<RooRealVar*>(Observables().at(i)); }

    //! the @c RooArgList of all (non-category) dependent variables.
    RooArgList& AuxObservables() { return m_auxObservables; }
    const RooArgList& AuxObservables() const { return m_auxObservables; }

    //! access dependents by name (return NULL if dependent is not found)
    RooRealVar* AuxObservable(const TString& name);

    //! the @c RooArgList of category variables
    RooArgList Categories() const { return Workspace()->allCats(); }
    RooAbsCategory* Category(int i) const { return dynamic_cast<RooAbsCategory*>(Categories().at(i)); }

    RooAbsCategoryLValue* MainCategory() const { return m_mainCat; }

    RooArgList ObservablesAndCats() const;

    RooAbsReal* Norm() { return m_norm; }
    
    RooAbsPdf* ExtPdf() { return m_extPdf; }
    const RooAbsPdf* ExtPdf() const { return m_extPdf; }

    //! the number of components in the model (e.g. 2 for a simple Signal+Background model).
    unsigned int NComponents() const { return m_componentModels.size(); }

    //! const accessor for component i
    HftModel* ComponentModel(unsigned int i) { return (i < NComponents() ? m_componentModels[i] : 0); }
    const HftModel* ComponentModel(unsigned int i) const { return (i < NComponents() ? m_componentModels[i] : 0); }
    HftModel* ComponentModel(const TString& name);

    //! the number of components in the model (e.g. 2 for a simple Signal+Background model).
    unsigned int NCategories() const { return m_categoryModels.size(); }

    //! non-const accessor for component i
    HftModel* CategoryModel(unsigned int i) { return (i < NCategories() ? m_categoryModels[i] : 0); }
    const HftModel* CategoryModel(unsigned int i) const { return (i < NCategories() ? m_categoryModels[i] : 0); }
    HftModel* CategoryModel(const TString& name);

    RooAbsData* CategoryData(RooAbsData& data, unsigned int catIndex);
    RooAbsData* CategoryData(RooAbsData& data, const TString& catName);
    
    //! the number of constraints in the model.
    unsigned int NConstraints() const { return m_constraints.size(); }

    //! const accessor for constraint i
    HftConstraint* Constraint(unsigned int i) { return (i < NConstraints() ? m_constraints[i] : 0); }
    //! access constraint by name (non-const)
    HftConstraint* Constraint(const TString& name);

    RooArgList ConstrainedParameters() const;
    
    //! returns a list of parameters from the workspace.
    RooArgList Parameters() const { return m_parameters; }
    RooAbsArg* Parameter(const TString& name) const { return m_parameters.find(name); }
    
    //! restore all parameters to their original values
    bool LoadState(const HftParameterStorage& state, bool values = true, bool constness = true);

    //! stores the initial values of all parameters. Called within HftModelBuilder immediately after the model is built
    bool SaveState(HftParameterStorage& state);

    //! Returns the current state (consider using SaveState insted to avoid multiple copy constructor calls)
    HftParameterStorage CurrentState();
    
    //! returns the total number of events in the model, summed over all components
    double NEvents() const;
    double NEvents(const TString& range) const;

    bool RandomizeConstraints();
    
    //! generates a toy dataset according to the model PDF. @param nEvents : overrides the default normalization of the model,
    //! @param poissonize : draw nEvents according to a Poisson distribution instead of using the raw number
    RooDataSet* GenerateEvents(const TString& genOptions = "");

    RooDataHist* GenerateBinned(const TString& genOptions = "");

    RooDataSet* GenerateAsimov();
    bool FillAsimov(RooDataSet*& dataset, const RooArgList& observables, const RooArgList& others);

    //! Plots the model PDF and specified dataset for a given dependent variable.
    //! @param varName : a dependent variable. @param data : the dataset to be plotted.
    //! @param components : if false, only the total model PDF is shown, if true, PDFs for the individual components are overlaid as dashed lines.
    //! @param nBins : number of bins in the RooPlot.
    //! @param binned : transform the dataset to a binned format (RooDataHist) before plotting to speed up processing.
    //! @param ignoreComponents : names of components to be skipped when @c components is set to @c true
    //! scaleType is enum ScaleType { Raw, Relative, NumEvent, RelativeExpected } ;
    RooPlot* Plot(const TString& varName, RooAbsData& data, const Options& options = Options(), HftModel* other = 0,
                  RooFitResult* fitResult = 0) const;
    RooPlot* Plot(const TString& varName, RooAbsData& data, const Options& options, const std::vector<Hfitter::HftModel*>& others,
                  RooFitResult* fitResult = 0) const;

    bool WriteCard(const TString& fileName) const;

    //! Prepares and Fits the model to a @c RooAbsData. @param data : the dataset to be fit. @param options : a list of RooFit options
    RooFitResult* Fit(RooAbsData& data, const Options& options = Options(), double* nll = 0, int* status = 0);

    //! for backward compatibility
    RooFitResult* FitData(RooAbsData& data, const Options& options = Options(), double* likelihoodOffset = 0) { return Fit(data, options, likelihoodOffset); }

    RooAbsReal* CreateNLL(RooAbsData& data, const Options& opts = Options());

    RooWorkspace* ExportWorkspace(const TString& fileName, const TString& name = "modelWS", const TString& poiNames = "",
                                  RooAbsData* data = 0, const TString& mcName = "mconfig", const TString& importCodeDir = "");

    //! returns the likelihood for a model component. This is the normalized value of the corresponding part of the model,
    //! evaluated at the current value of the dependents. @param componentName : name of the selected component.
    double Likelihood(const TString& componentName);

    //! returns the likelihood ratio for a model component. This is the normalized value of the corresponding part of the model
    //! divided by the value for the total model, evaluated at the current value of the dependents.
    //!  @param componentName : name of the selected component.
    double LikelihoodRatio(const TString& componentName);
    
    
    RooArgList AllParameters() { return Parameters(); }
    RooArgList& Dependents() { return m_observables; }
    RooRealVar* Dependent(const TString& name) const { return Observable(name); }
    
    static bool RegisterModel(const TString& name, HftModel& calc);
    static bool DeregisterModel(HftModel& model);
    static HftModel* GetModel(const TString& name);
    static void PrintModels(int maxDepth = 1);
    static void Print(const RooAbsArg& arg, int maxDepth = -1, const TString& indent = "");
    void Print(const TString& par, int maxDepth = -1) { if (Real(par)) Print(*Real(par), maxDepth); }
    void PrintPdf() const;

   protected:

    TString m_name;
     
    RooWorkspace* m_workspace;
    TFile* m_workspaceFile;
    bool m_ownWS;

    RooAbsPdf* m_pdf;
    bool m_ownPdf;
    
    RooArgList m_observables, m_auxObservables, m_parameters;

    RooAbsCategoryLValue* m_mainCat;
    RooAbsReal* m_norm;
    RooAbsPdf* m_extPdf;
    
    std::vector<Hfitter::HftModel*> m_componentModels, m_categoryModels;
    std::vector<Hfitter::HftConstraint*> m_constraints;
    
    static std::map<TString, HftModel*>* m_modelRegistry;
  };
}

#endif
