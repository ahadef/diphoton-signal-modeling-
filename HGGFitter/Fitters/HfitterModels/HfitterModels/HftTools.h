#ifndef ROOT_Hfitter_HftTools
#define ROOT_Hfitter_HftTools

#include <vector>

#include "TPad.h"
#include "TString.h"
#include "RooRealVar.h"

#include "HfitterModels/HftModel.h"

namespace Hfitter {
  
  /** @class HftTools
      @author Andreas Hoecker
      @author Nicolas Berger
  */  

  //! Draw the Hfitter logo. @param size : the logo size. @param pad : the pad to draw to.
  void DrawLogo(double size = 0.04, TVirtualPad* pad = gPad);
  
  void FillRange(unsigned int n, double xMin, double xMax, std::vector<double>& v);
  std::vector<double> Range(unsigned int n, double xMin, double xMax);
}

#endif
