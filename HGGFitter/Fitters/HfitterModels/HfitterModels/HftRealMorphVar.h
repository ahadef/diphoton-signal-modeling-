#ifndef ROOT_Hfitter_HftRealMorphVar
#define ROOT_Hfitter_HftRealMorphVar

#include "HfitterModels/HftAbsParameters.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "TGraphErrors.h"
#include "RooListProxy.h"


namespace Hfitter {

  /** @class HftRealMorphVar
      @author Nicolas Berger

    An implementation of RooAbsReal to represent a "morphing parameter" -- i.e. a PDF parameter whose value depends on that of another variable. 
    The parameter value is calculated by linearly interpolating between values at different points.
  */

  class HftRealMorphVar : public RooAbsReal
  {
   public:
  
    //! default constructor
    HftRealMorphVar() { }

    //! standard constructor. 
    //! @param name : the variable name, @param title : the variable title, @param unit : the unit of the variable. 
    //! @param morphingVar : the variable along which this one is morphed. @param pointNames : a vector specifying the names of the interpolation points.
    //! @param pointPositions : a vector specifying the positions (in term of values of morphingVar) of the  interpolation points.
    HftRealMorphVar(const TString& name, const TString& title,
                    RooAbsReal& morphingVar, const std::vector<double>& pointPositions, const RooArgList& pointVars);
    
    //! copy constructor. @param other : the HftRealMorphVar to copy, @param newName : the new variable name.
    HftRealMorphVar(const HftRealMorphVar& other, const char* newName = 0);

    //! creates a clone of this variable. @param newName : the new variable name.
    virtual TObject* clone(const char* newName = 0) const { return new HftRealMorphVar(*this, newName); }
    
    //! destructor
    virtual ~HftRealMorphVar();
    
    RooArgList Parameters() const;
    
    //! returns a reference to the variable along which the morphing is computed
    const RooAbsReal& MorphingVar() const { return *(const RooAbsReal*)findServer(NPoints()); }
    
    //! returns the number of points defined for the interpolation.
    unsigned int NPoints() const { return m_pointPositions.size(); }
    
    //! returns the name of an interpolation point. @param i : point index.
    TString PointName(unsigned int i) const { return PointVar(i)->GetName(); }

    //! returns the position (in terms of values of MorphingVars()) of an interpolation point. @param i : point index.
    double PointPosition(unsigned int i) const { return m_pointPositions[i]; }
    
    //! returns the internal RooRealVar holding the parameter value at point i. @param i : point index.
    const RooRealVar* PointVar(unsigned int i) const { return (const RooRealVar*)findServer(i); }
    RooRealVar* PointVar(unsigned int i) { return (RooRealVar*)findServer(i); }
   
    TGraphErrors* Graph() const;
    
    static TString MakeName(const TString& rootName, const TString& pointName);
    
//     //! read values from a RooFit datacard. @param is : the input stream to read from. 
//     //! @param compact : RooFit option specifying compact format. @param verbose : parameter to control verbosity of output.
//     bool readFromStream(istream& is, bool compact, bool verbose = false) ;
//     
//     //! write values to a RooFit datacard. @param os : the output stream to write to. @param compact : RooFit option specifying compact format.
//     void writeToStream(ostream& os, bool compact) const;
        
  protected:

    //! Implementation of virtual function RooAbsReal::evaluate, using linear interpolation between the interpolation points.
    double evaluate() const;

    std::vector<double>  m_pointPositions;  //!< The positions of the interpolation points

    ClassDef(HftRealMorphVar, 0)
  };
}
#endif
