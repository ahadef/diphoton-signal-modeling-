#ifndef ROOT_Hfitter_HftGenericPdfBuilder
#define ROOT_Hfitter_HftGenericPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {

  /** @class HftGenericPdfBuilder
      @author Nicolas Berger

  */

  class HftGenericPdfBuilder : public HftAbsPdfBuilder {
    
   public:
    
    //! constructor
    HftGenericPdfBuilder() { }
   
    void Setup(const char* dependent, const char* formula,
               const char* par1 = "", const char* par2 = "", const char* par3 = "",
               const char* par4 = "", const char* par5 = "", const char* par6 = "",
               const char* par7 = "", const char* par8 = "", const char* par9 = "");
   
    //! destructor
    virtual ~HftGenericPdfBuilder() { }
        
    //! returns the built PDF object. Must be implemented in derived classes. 
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;
      
   private:
    
    TString m_dependent, m_formula;
    std::vector<TString> m_args;
     
    ClassDef(HftGenericPdfBuilder, 1);
  };
}

#endif
