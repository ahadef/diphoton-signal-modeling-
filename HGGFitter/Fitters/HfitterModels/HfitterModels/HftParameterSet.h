#ifndef ROOT_Hfitter_HftParameterSet
#define ROOT_Hfitter_HftParameterSet

#include "HfitterModels/HftStorableParameters.h"
#include "RooArgSet.h"

class RooRealVar;

namespace Hfitter {
  
  /** @class HftParameterSet
      @author Nicolas Berger
      
    A concrete implementation of @c HftParameterSet in which the parameters are stored in a set.
  */  
  class HftParameterSet : public HftStorableParameters {

    public:

      //! constructor. @param set : a set of RooAbsArg
      HftParameterSet(const RooArgSet& set = RooArgSet()) : m_parameters(set), m_parList(m_parameters) { }
      virtual ~HftParameterSet() { }
      
      //! returns the list of parameters. Converts RooArgSet->RooArgList.
      RooArgList Parameters() const { return m_parameters; }

      //! add a single RooAbsArg. returns false if arg is already there
      bool Add(const RooAbsArg& arg) { return m_parameters.add(arg, true); m_parList.add(arg, true); }
      
      //! add a set. returns false if any arg is already there.
      bool Add(const RooArgSet& set) { return m_parameters.add(set, true); m_parList.add(set, true); }

      using HftAbsParameters::Parameter;
      RooAbsArg* Parameter(unsigned int i) const { return m_parList.at(i); }

      unsigned int NParameters() const { return m_parameters.getSize(); }
      
    private:
      
      RooArgSet m_parameters; //!< the set of stored parameters.
      RooArgList m_parList;
   };
}

#endif
