#ifndef ROOT_Hfitter_HftWorkspacePdfBuilder
#define ROOT_Hfitter_HftWorkspacePdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include <vector>

namespace Hfitter {

  /** @class HftWorkspacePdfBuilder
      @author Nicolas Berger

  */
  class HftWorkspacePdfBuilder : public HftAbsPdfBuilder {
    
   public:
    
    //! constructor
    HftWorkspacePdfBuilder() { }
   
    void Setup(const char* fileName, const char* key, const char* name);
   
    //! destructor
    virtual ~HftWorkspacePdfBuilder() { }
        
    //! returns the built PDF object. Must be implemented in derived classes. 
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;
      
   private:
    
    TString m_fileName, m_key, m_name;
    std::vector<TString> m_dependents;
     
    ClassDef(HftWorkspacePdfBuilder, 1);
  };
}

#endif
