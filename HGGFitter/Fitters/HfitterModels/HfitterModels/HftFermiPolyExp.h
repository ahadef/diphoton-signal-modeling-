#ifndef Hfitter_HftFermiPolyExp_h
#define Hfitter_HftFermiPolyExp_h

#include "RooAbsPdf.h"
#include "RooRealProxy.h"

class RooRealVar;

namespace Hfitter {

  /** @class HftFermiPolyExp
      @author Nicolas Berger

    RooAbsPdf implementation for the product of a polynomial, an expononential and a Fermi threshold function. The PDF form is
    P(x) = (x - x0)^n*exp(-alpha*(x - x0))/(1 + exp(-(x - xT)/sigmaT))
  */
   
  class HftFermiPolyExp : public RooAbsPdf {

  public:
    
    HftFermiPolyExp() { }

    //! Constructor. @param name : PDF name @param title : PDF title @param _x : PDF dependent variable @param _x0 : overall offset for the dependent @param _n : order of the polynomial @param _alpha : coefficient of the exponential @param _xT : threshold position @param _sigmaT : threshold width.
    HftFermiPolyExp(const char *name, const char *title,
                    RooAbsReal& _x, RooAbsReal& _x0, RooAbsReal& _n, RooAbsReal& _alpha,
                    RooAbsReal& _xT, RooAbsReal& _sigmaT);
    
    //! copy constructor. @param other : the instance to copy. @param name : optional new name name for the new PDF (otherwise taken from the original)
    HftFermiPolyExp(const HftFermiPolyExp& other, const char* name=0) ;
    virtual ~HftFermiPolyExp() { }

    //! returns a clone of the PDF with a new name. @param newName : name of the cloned PDF
    virtual TObject* clone(const char* newName) const { return new HftFermiPolyExp(*this, newName); }
    
    //! determines whether an analytical integral can be computed. Here always true. @param allVars : the PDF parameters. @param analVars : . @param rangeName : .
    Int_t getAnalyticalIntegral(RooArgSet& allVars, RooArgSet& analVars, const char* rangeName = 0) const;

  protected:

    //! evaluates the PDF for the current parameter values
    Double_t evaluate() const ;
    
    RooRealProxy x ;     //!< dependent variable
    RooRealProxy x0 ;    //!< overall shift in the dependent
    RooRealProxy n ;     //!< polynomial order
    RooRealProxy alpha ; //!< exponential coefficient
    RooRealProxy xT;     //!< Fermi threshold position
    RooRealProxy sigmaT; //!< Fermi threshold width

    ClassDef(Hfitter::HftFermiPolyExp, 1); //!< ROOT boilerplate
  };
}

#endif
