#ifndef ROOT_Hfitter_HftPeggedPoly
#define ROOT_Hfitter_HftPeggedPoly

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooListProxy.h"

class RooRealVar;
class RooAbsReal;
class RooArgList;

namespace Hfitter {

  /** @class HftPeggedPoly
      @author Nicolas Berger

    RooAbsPdf implementation for the product of a polynomial with fixed endpoints. The PDF form is
    P(x) = P_0 + (1 + c_1*(x-x0) + ... + c_n*(x - x0)^n)*(x1 - x)^alpha for x in [x0..x1]
  */
   
  class HftPeggedPoly : public RooAbsPdf {

   public:
     
    HftPeggedPoly() { }
     
    //! Constructor. @param name : PDF name @param title : PDF title @param x : PDF dependent variable @param coefList : list of polynomial coefficients (size is polynomial order) @param power : exponent of the x1 -x term @param pedestal : constant offset in the PDF. @param xMin : lower bound of PDF support. @param xMax : upper bound of PDF support.
    HftPeggedPoly( const char* name, const char* title, 
                    RooAbsReal& x, const RooArgList& coefList,
                    RooAbsReal& power, RooAbsReal& pedestal, Double_t xMin, Double_t xMax);
     
    //! copy constructor. @param other : the instance to copy. @param name : optional new name name for the new PDF (otherwise taken from the original)
    HftPeggedPoly( const HftPeggedPoly& other, const char* name = 0 );

    inline virtual ~HftPeggedPoly() {}
     
    //! determines whether an analytical integral can be computed. Here always true. @param allVars : the PDF parameters. @param analVars : . @param rangeName : .
    Int_t    getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0 ) const;
    //! returns the analytical for of the PDF integral (avoids numerical integration). @param code : . @param rangeName : .
    Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;
     
    //! returns a clone of the PDF with a new name. @param newName : name of the cloned PDF
    virtual TObject* clone( const char* newName ) const { return new HftPeggedPoly( *this, newName ); }
     
    //! returns the integral of the PDF in the specified range
    double integral(double xMin, double xMax) const;
    
    //! evaluates the PDF for the current parameter values    
    Double_t evaluate() const;

   protected:

    RooRealProxy m_x;     //!< dependent variable
    RooListProxy m_coefs; //!< polynomial coefficients
    RooRealProxy m_power; //!< exponent of the x1 -x term
    RooRealProxy m_ped;   //!< constant offset in the PDF
    Double_t m_xMin;      //!< lower and upper bounds of PDF support.
    Double_t m_xMax;      //!< lower and upper bounds of PDF support.

    ClassDef(Hfitter::HftPeggedPoly, 1); //!< ROOT boilerplate
  };
}

#endif
