#ifndef ROOT_Hfitter_HftAbsNodeBuilder
#define ROOT_Hfitter_HftAbsNodeBuilder

#include "HfitterModels/HftInstance.h"

#include "RooAbsReal.h"
#include "RooWorkspace.h"
#include "TString.h"

class RooStringVar;
class RooAbsCategory;

namespace Hfitter {

  /** @class HftAbsNodeBuilder
      @author Nicolas Berger
  */


  class HftDatacardReader;
  class HftDatacardReaderV4;
  
  class HftAbsNodeBuilder : public HftInstance {

  public:

    //! constructor. @param partner : partner to add
    HftAbsNodeBuilder() : m_verbose(false), m_dcReader(0), m_dcReaderV4(0) { }

    //! destructor
    virtual ~HftAbsNodeBuilder() { }

    //! set a common prefix for all parameters
    void SetPrefix(const TString& prefix) { m_prefix = prefix; }

    //! set a common suffix for all parameter
    void SetSuffix(const TString& suffix) { m_suffix = suffix; }

    void SetVerbosity(bool verbose = true) { m_verbose = verbose; }

    void SetDatacardReader(HftDatacardReader* reader) { m_dcReader = reader; }
    void SetDatacardReader(HftDatacardReaderV4* reader) { m_dcReaderV4 = reader; }
    
    HftDatacardReader* DatacardReader() const { return m_dcReader; }
    
  protected:

    //! returns a PDF parameter for use when defining the PDF form. If the parameter is new, it will be created. If it was created previously, a reference to this instance will be returned. If the builder has a friend builder in which a parameter of that name is defined, a reference to this instance will be returned. @param name : name of the parameter. @param unit : unit of the parameter. @param title : a title for this parameter.
    RooAbsReal& Param(RooWorkspace& workspace, const TString& name, const TString& unit = "",
                      const TString& title = "", double value = 0, bool noExtras = false) const;

    RooStringVar& String(RooWorkspace& workspace, const TString& name, const TString& title = "", const TString& value = "") const;

    RooAbsCategory* Category(RooWorkspace& workspace, const TString& name) const;

  private:

    //! Create a new RooRealVar parameter. @param name : variable name. @param unit : unit of the variable. @param title : variable title.
    RooRealVar* CreateParameter(const TString& name, const TString& unit = "", const TString& title = "", double value = 0) const;

    TString m_prefix, m_suffix;
    bool m_verbose;
    HftDatacardReader* m_dcReader;
    HftDatacardReaderV4* m_dcReaderV4; // compat
    
    ClassDef(Hfitter::HftAbsNodeBuilder, 1);
  };
}

#endif
