#ifndef ROOT_Hfitter_HftCountingPdfBuilder
#define ROOT_Hfitter_HftCountingPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include "TString.h"
#include <vector>

namespace Hfitter {
  
  class HftCountingPdfBuilder : public HftAbsPdfBuilder {
    
  public:
    
    HftCountingPdfBuilder() { }    
    virtual ~HftCountingPdfBuilder() { }

    void Setup(const char* depName = "mgg");

    RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;

    ClassDef(Hfitter::HftCountingPdfBuilder, 0);

  private:
    
    TString m_depName;
  };
}

#endif
