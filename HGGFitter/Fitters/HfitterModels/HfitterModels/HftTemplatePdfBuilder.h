#ifndef ROOT_Hfitter_HftTemplatePdfBuilder
#define ROOT_Hfitter_HftTemplatePdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

namespace Hfitter {

  /** @class HftTemplatePdfBuilder
      @author Nicolas Berger

  */
  template<class T, unsigned int NPAR> struct PdfCreator {
    RooAbsPdf* Pdf(const TString& /*name*/, RooAbsReal& /*dependent*/, const RooArgList& /*pars*/) { return 0; }
  };
  
  template<class T, unsigned int NPAR> 
  class HftTemplatePdfBuilder : public HftAbsPdfBuilder {
    
   public:
    
    //! constructor
    HftTemplatePdfBuilder() { }
   
    void Setup(const char* dependent,
               const char* par1 = "", const char* par2 = "", const char* par3 = "",
               const char* par4 = "", const char* par5 = "", const char* par6 = "");
   
    //! destructor
    virtual ~HftTemplatePdfBuilder() { }
        
    //! returns the built PDF object. Must be implemented in derived classes. 
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;
      
   private:
    
    TString m_dependent;
    std::vector<TString> m_args;
     
    ClassDef(HftTemplatePdfBuilder, 1);
  };

  template<class T, unsigned int NPAR> 
  void HftTemplatePdfBuilder<T, NPAR>::Setup(const char* dependent,
                                            const char* par1, const char* par2, const char* par3,
                                            const char* par4, const char* par5, const char* par6)
  { 
    m_dependent = dependent;
    if (TString(par1) != "") m_args.push_back(par1);
    if (TString(par2) != "") m_args.push_back(par2);
    if (TString(par3) != "") m_args.push_back(par3);
    if (TString(par4) != "") m_args.push_back(par4);
    if (TString(par5) != "") m_args.push_back(par5);
    if (TString(par6) != "") m_args.push_back(par6);
  }

  template<class T, unsigned int NPAR> 
  RooAbsPdf* HftTemplatePdfBuilder<T, NPAR>::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const 
  { 
    std::vector<RooAbsReal*> pars;
    for (unsigned int i = 0; i < m_args.size(); i++) pars.push_back(&Param(workspace, m_args[i]));
    RooRealVar* dep = Dependent(dependents, m_dependent);
    return dep ? PdfCreator<T, NPAR>().Pdf(name, *dep, pars) : 0; 
  }


  template<class T> struct PdfCreator<T, 0> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& /*pars*/) { return new T(name, "", dependent); }
  };
  template<class T> struct PdfCreator<T, 1> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& pars) { return new T(name, "", dependent, *pars[0]); }
  };
  template<class T> struct PdfCreator<T, 2> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& pars) { return new T(name, "", dependent, *pars[0], *pars[1]); }
  };
  template<class T> struct PdfCreator<T, 3> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& pars) { return new T(name, "", dependent, *pars[0], *pars[1], *pars[2]); }
  };
  template<class T> struct PdfCreator<T, 4> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& pars) { return new T(name, "", dependent, *pars[0], *pars[1], *pars[2], *pars[3]); }
  };
  template<class T> struct PdfCreator<T, 5> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& pars) { return new T(name, "", dependent, *pars[0], *pars[1], *pars[2], *pars[3], *pars[4]); }
  };
  template<class T> struct PdfCreator<T, 6> {
    RooAbsPdf* Pdf(const TString& name, RooAbsReal& dependent, const std::vector<RooAbsReal*>& pars) { return new T(name, "", dependent, *pars[0], *pars[1], *pars[2], *pars[3], *pars[4], *pars[5]); }
  };
}

#endif
