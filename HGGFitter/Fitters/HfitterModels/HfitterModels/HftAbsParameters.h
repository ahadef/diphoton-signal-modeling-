#ifndef ROOT_Hfitter_HftAbsParameters
#define ROOT_Hfitter_HftAbsParameters

#include "RooArgList.h"
#include "TString.h"
#include <vector>

class RooRealVar;
class RooCategory;
class RooAbsReal;
class RooFormulaVar;
class RooWorkspace;

namespace Hfitter {

  
  /** @class HftAbsParameters
      @author Nicolas Berger

    A class describing a list of PDF parameters. The accessor to the parameter list itself is pure virtual, and helper functions
    are implemented to retrieve individual parameter, save and load their values, set them free or fixed, etc.
    
    The objects keeps a list of clients, i.e. other HftAbsParameters objects which logically appended to this one. Requests for parameter retrieval,
    loading and saving will be recursively called on each of the clients before being applied to this object.
  */
  
  class HftAbsParameters {

   public:

    //! default constructor
    HftAbsParameters() { }
    
    //! constructor specifying a client. @param client : a client HftAbsParameters object.
    HftAbsParameters(HftAbsParameters* client) { if (client) m_clients.push_back(client); }
    
    //! constructor specifying a vector of clients. @param clients : a vector of client HftAbsParameters objects.
    HftAbsParameters(std::vector<Hfitter::HftAbsParameters*>& clients) : m_clients(clients) { }
    
    //! copy constructor. @param other : the object to copy.
    HftAbsParameters(const HftAbsParameters& other) : m_clients(other.m_clients) { }
    
    //! destructor
    virtual ~HftAbsParameters() { }

    //! returns the list of parameters. This is left pure virtual and should be implemented in concrete derived classes.
    virtual RooArgList Parameters() const = 0;

    //! add a client.  @param client : a client HftAbsParameters object.
    void AddClient(HftAbsParameters& client) { m_clients.push_back(&client); }  
    
    //! adds a vector of clients. @param clients : a vector of client HftAbsParameters objects.
    void AddClients(const std::vector<Hfitter::HftAbsParameters*>& clients);

    //! returns the number of clients.
    unsigned int NClients() const { return m_clients.size(); }
    
    //! returns the specified client. @param i : client index.
    const HftAbsParameters* Client(unsigned int i) { return (i < NClients() ? m_clients[i] : 0); } 
    
    //! returns the number of parameters
    virtual unsigned int NParameters() const { return Parameters().getSize(); }
      
    //! access a parameter by index @param i : requested parameter index
    virtual RooAbsArg* Parameter(unsigned int i) const;
      
    //! access a parameter by name @param name : requested parameter name
    virtual RooAbsArg* Parameter(const TString& name) const;
      
    //! access a parameter by name and returns a RooRealVar. If named parameter is of another type, returns 0. @param name : requested parameter name
    virtual RooRealVar* Var(const TString& name) const;

    //! access a parameter by name and returns a RooRealVar. If named parameter is of another type, returns 0. @param name : requested parameter name
    virtual RooAbsReal* Real(const TString& name) const;

    //! access a parameter by index and returns a RooRealVar. If specified parameter is of another type, returns 0. @param i : requested parameter index.
    virtual RooRealVar* Var(unsigned int i) const;

    virtual RooCategory* Cat(const TString& name) const;
    virtual RooCategory* Cat(unsigned int i) const;
    
    //! read the parameters from an input stream. @param stream : input stream from which to read the data. @param section : if provided, only read parameters from the specified section.
    bool ReadFromStream(std::istream& stream, const TString& section = ""); 

    //! write the parameters to an output stream. @param stream : output stream to which to write the data. @param section : section heading under which to write the data
    bool WriteToStream(std::ostream& stream, const TString& section = "");
 
    //! set all parameters free
    bool FloatParameters(const RooArgList& exclude = RooArgList());
    
    //! set all parameters fixed
    bool FixParameters(const RooArgList& exclude = RooArgList());

    //! retrieves from the supplied list those parameters that are split versions of the ones in this object. @param splitParams : list of split parameters
    RooArgList SplitParameters(RooWorkspace& workspace) const; 
    
    //! retrieves the subset of the parameters in this objet with specified names. @param names : vector of TString holding with parameter names
    RooArgList Find(const std::vector<TString>& names) const;

    //! retrieves the subset of the parameters in this objet which is also contained in the list. @param  : list of parameters to find
    RooArgList Find(const RooArgList& pars) const;
      
    //! set the values of all parameters from those of variables in the provided set. @param set : the RooArgSet to take values from.
    bool SetValues(const RooArgSet& set);
      
    //! returns the list of free (floating) parameters
    RooArgList FreeParameters() const;

    static RooAbsReal* Import(const RooAbsReal& var, RooWorkspace& workspace, bool verbose = false);

    //! Make a formula var, accessing existing parameters if available and creating the rest
    static RooFormulaVar* MakeFormulaVar(const TString& name, const TString& title, const TString& formula, 
                                         RooWorkspace& workspace, bool verbose = false);

    static bool Add(RooAbsArg& var, RooArgList& list);
    static void AddUnique(const RooArgList& source, RooArgList& dest, bool multiAdd = false);

    static bool FixAll(const RooArgList& vars, bool fix = true, const RooArgList& exclude = RooArgList());

    static TString Merge(const TString& name1, const TString& name2);

    static TString StripCategories(const TString& name);
    
    static TString TitleAndUnit(const TString& title, const TString& unit);
    static TString TitleAndUnit(const RooRealVar& var);

    template<class T> static T* FindByName(const std::vector<T*>& items, const TString& name) {
      for (typename std::vector<T*>::const_iterator item = items.begin(); item != items.end(); item++) if ((*item)->GetName() == name) return *item;
      return 0;
    }

   protected:

     std::vector<Hfitter::HftAbsParameters*> m_clients; //!< vector holding pointers to clients.
   };
}

#endif
