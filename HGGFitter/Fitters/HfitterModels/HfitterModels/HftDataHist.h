#ifndef ROOT_Hfitter_HftDataHist
#define ROOT_Hfitter_HftDataHist

#include "RooAbsData.h"
#include "RooDataHist.h"
#include "TFile.h"
#include "TH1.h"

class RooAbsCategory;

namespace Hfitter {

  class HftModel;
  
  /** @class HftDataHist      
      @author Nicolas Berger
      
      Class wrapping around a RooDataHist and providing functions to open ROOT files.
  */
  
  class HftDataHist : public TNamed {

   public:

    //! static constructor creating a RooDataHist from a ROOT file. @param fileName : the name fo the file. @param treeName : the name fo the tree. 
    //! @param model : the name of the model to use (specifies the variables to load),
    static HftDataHist* Open(const TString& fileName, const TString& histName, const HftModel& model, double w = 1);

    //! desctructor
    virtual ~HftDataHist() {if (m_file) {  if (m_data) delete m_data; delete m_file; } }

    //! automatic conversion to a pointer to the RooDataHist.
    operator RooDataHist* () { return m_data; }

    //! automatic conversion to a reference to the RooDataHist.
    operator RooDataHist& () { return *m_data; }

    //! overloading of (), returning a reference to the RooDataHist.
    RooDataHist& operator()() { return *m_data; }

    //! returns the hist
    TH1* Hist() { return m_hist; }
    
    //! returns the file
    TFile* File() { return m_file; }
    
    //! returns the data hist.
    RooDataHist* DataHist() { return m_data; }
    
    double Weight() const { return m_weight; }
    
  private:

    //! the real constructor. Not useful in general...
    HftDataHist(const TString& fileName, const TString& histName, const HftModel& model, double w = 1);

    TFile* m_file;       //!< pointer to the opened TFile
    TH1* m_hist;       //!< pointer to the TH1 used to create the data hist
    RooDataHist* m_data;  //!< the data hist
    
    double m_weight;
    
    ClassDef(Hfitter::HftDataHist, 1)
  };
}

#endif
