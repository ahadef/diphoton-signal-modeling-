#ifndef ROOT_Hfitter_HftDatacardReaderV4
#define ROOT_Hfitter_HftDatacardReaderV4

#include <vector>
#include <map>
#include <set>
#include <string>
#include "TString.h"
#include "RooArgList.h"
#include "HfitterModels/HftEnums.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftDatacardReader.h"

#include <iostream>

class RooStreamParser;
class RooAbsCategory;
class RooMappedCategory;

namespace Hfitter {
  
  class HftAbsParameters;
  
  /** @class HftDatacardReaderV4
      @author Nicolas Berger
    
    Helper class to read the Hfitter datacard format.     
    This class is meant to be used from within @c HftModelBuilder and @c HftModelSplitter
  */
  
  class HftDatacardReaderV4 {

   public:

    //! Constructor. @param datacard : the datacard file name
    HftDatacardReaderV4(const TString& datacard = "", bool verbose = false);
    
    //! desctructor
    virtual ~HftDatacardReaderV4();

    static bool Preprocess(const TString& fileName, std::string& data);

    //! returns an input stream pointing at the beginning of the datacard file.
    std::istream& DataStream() const { return *m_dataStream; }

    //! rewind the input stream to the beginning.
    std::istream& Rewind();
    
    //! returns true if the given token is *not* equal to the next token in the stream. @param token : a string token
    bool ExpectToken(const TString& token);
    
    //! returns true if the stream has reached EOF.
    bool IsAtEnd();

    //! Read the next token. @param token : a string token
    bool ReadToken(TString& token);
    
    //! Read the next token without advancing the stream (next ReadToken/PeekToken call will read the same token again). @param token : a string token
    bool PeekToken(TString& token);
    
    //! Read the next token in specified section. @param section : section to read from, @param token : a string token, 
    //! @param where : output parameter giving the location of the cursor relative to the section (0=not started yet, 1=inside section, 2=after section)
    bool ReadToken(const TString& section, TString& token, int& where);

    //! read the [dependents] section of the datacard
    bool ReadDependents();
     
    //! read the models in the [models] section of the datacard
    bool ReadModels();

    //! read the constraints (nuisance parameters) in the [Constraints] section of the datacard
    bool ReadConstraints();
    
    //! read the formulas defined in the datacard
    bool ReadDerivedReals();

    //! read the [Models] section of the datacard, looking for components
    //! @param found : output parameter specifying whether a component has been found
    //! @param where : output parameter giving the location of the cursor relative to the section (0=not started yet, 1=inside section, 2=after section)
    bool ReadComponent(bool& found, int& where);     
    
    //! read the [Models] section of the datacard, reading the models. @param name : name of the component (and section), 
    //! @param found : output parameter specifying whether a model has been found
    //! @param where : output parameter giving the location of the cursor relative to the section (0=not started yet, 1=inside section, 2=after section)
    bool ReadModel(const TString& name, bool& found, int& where);

    bool ReadPdfBuilderSpecs(const TString& section, int& where, TString& className, TString& args);
    
    //! read the [Models] section of the datacard, looking for constraints (nuisance parameters)
    //! @param found : output parameter specifying whether a constraint has been found
    //! @param where : output parameter giving the location of the cursor relative to the section (0=not started yet, 1=inside section, 2=after section)
    bool ReadConstraint(bool& found, int& where);

    //! read the datacard, looking for formulas
    //! @param found : output parameter specifying whether a formula has been found
    bool ReadFormula(bool& found);

    //! read the datacard, looking for functions
    //! @param found : output parameter specifying whether a function has been found
    bool ReadFunction(bool& found);

    //! read the [categories] section of the datacard
    bool ReadCategories();
    
    //! read the [categories] section of the datacard
    //! @param found : output parameter specifying whether a category has been found
    //! @param where : output parameter giving the location of the cursor relative to the section (0=not started yet, 1=inside section, 2=after section)
    bool ReadCategory(bool& found, int& where);
  
    bool ReadSplittings();
    
    //! read the fixed parameters entries in the datacard
    //! @param section : section to read from
    bool ReadFixedParameters(const TString& section);

    //! read the fixed parameters entries in the datacard
    bool ReadMorphVariables();

    bool ReadExtraProperties();

    //! read parameter values and ranges from the datacard. @param params : list of parameters to be read. @param section : datacard section to read from.
    bool ReadParameters(const RooArgList& params, const TString& section);

    //! retuns the type of the model defined in the datacard
    ModelType Type() const { return m_modelType; }
               
    //! returns the list of dependents defined in the datacard
    const RooArgList& Dependents() const { return m_dependents; }

    //! returns the list of categories defined in the datacard
    const RooArgList& Categories() const { return m_categories; }

    //! returns the list of categories defined in the datacard
    const RooArgList& AuxCategories() const { return m_auxCategories; }

    //! returns the number of mode components defined in the datacard.
    unsigned int NComponents() const { return m_componentNames.size(); }
    
    //! returns the name of the i'th component. @param i : component index.
    const TString ComponentName(unsigned int i) const { return m_componentNames[i]; }
    const std::vector<TString>& ComponentNames() const { return m_componentNames; }
    
    //! returns the number of models defined in the datacard. This is 1 for non-split models, and >1 if models are split and have different PDF forms in different categories (@c MultiPdfModel type).
    unsigned int NModels() const { return m_globalModelIndices.size(); }
     
    //! returns the PDF builder classes defined for all components. In the return value, map key is the component name and the vector holds the specs. For model types @c SimpleModel and @c SplitModel, these vectors should always have length 1.
    const std::map<TString, std::vector<HftPdfBuilderSpec> >& ComponentSpecs() { return m_componentSpecs; }
     
    //! returns the PDF builder classes defined for component i. (see above for details). @param i : component index
    std::vector<HftPdfBuilderSpec> ComponentSpecs(unsigned int i) const;
     
    //! returns the normalization type (fractions or absolute number of events) for all components. In the return value, the map key is the component name.
    std::map<TString, TString > ComponentNorms() const { return m_componentNorms; }
     
    //! returns the normalization type (see above) for component i. @param i : component index.
    TString ComponentNorm(unsigned int i) const;
    
    TString VarTitle(const TString& var) const;
    TString VarUnit(const TString& var) const;
    bool VarFixedInCompFit(const TString& var) const;
    
    const std::map<TString, bool >& VarsFixedInComponentFits() const { return m_varFixedInCompFit; }
        
    //! returns the number of constraints defined in the datacard.
    unsigned int NConstraints() const { return m_constraintNames.size(); }
    
    //! returns the name of the i'th constraint. @param i : component index.
    const TString ConstraintName(unsigned int i) const { return m_constraintNames[i]; }
     
    //! returns the PDF builder class defined for each constraint. In the return value, map key is the constraint name.
    const std::map<TString, HftPdfBuilderSpec>& ConstraintSpecs() { return m_constraintSpecs; }
     
    //! returns the PDF builder class for constraint i. (see above for details). @param i : component index
    HftPdfBuilderSpec ConstraintSpecs(unsigned int i) const;

    //! returns the map variable_name -> formula for the formula variables defined in the datacard
    const std::vector<TString>& DerivedRealNames() const { return m_derivedRealNames; }

    //! returns the map variable_name -> formula for the formula variables defined in the datacard
    const std::map<TString, TString>& Formulas() const { return m_formulas; }

    //! returns the PDF builder classes defined for all components. In the return value, map key is the component name and the vector holds the specs. For model types @c SimpleModel and @c SplitModel, these vectors should always have length 1.
    const std::map<TString, HftFunctionBuilderSpec>& FunctionSpecs() { return m_functionSpecs; }
    
    //! returns the list of parameters that should be kept fixed in single-component fits for component i. @param i : component index
    const std::vector<TString>& FixedParamsForCompFit() const { return m_fixedInCompFit; }

    //! returns the name of the category variable along which the model PDF form is split, for models of type @c MultiPdfModel.
    const TString& ModelCatName() const { return m_modelCatName; }

    //! returns the possible states of the model category variable (see above).
    std::vector<TString> ModelCatStates() const;      
    
    //! The model cat states for which the model with index 'model' in component 'comp' is valid. 
    //! @param comp : the index of the component; @param model : the index of the model within the component
    std::set<TString> ComponentModelCatStates(const TString& compName, unsigned int model) const;
    TString ComponentModelCatStatesString(const TString& compName, unsigned int model) const;
    
    //! For a given global model, gives the index of the component model within component 'comp' which is used.
    //! @param globalModel : the index of the global model; @param comp : the index of the component.
    unsigned int ComponentModelIndex(unsigned int globalModel, unsigned int comp) const { return m_globalModelIndices[globalModel][comp]; }
    
    //! For a given global model, gives the modelCat states for which the component model within component 'comp' is used.
    //! @param globalModel : the index of the global model; @param comp : the index of the component.
    const std::set<TString>& GlobalModelCatStates(unsigned int globalModel) const { return m_globalModelCatStates[globalModel]; }
    TString GlobalModelCatStatesString(unsigned int globalModel) const;
    
    const std::vector<std::set<TString> >& GlobalModelCatStates() const { return m_globalModelCatStates; }

    //! returns the map category_name -> list of variable_name's split along this category.
    const std::map<TString, std::vector<TString> >& SplittingCatMap() const { return m_splittingCatMap; }
     
    //! returns the map variable_name -> list of category_name's along which this variable is split
    const std::map<TString, std::vector<TString> >& SplittingVarMap() const { return m_splittingVarMap; }

    //! returns true if the normalization vars should not be split.
    bool NoNormVarSplitting() const { return m_noNormVarSplitting; }

    //! returns the map variable_name -> name of the variable along which to morph, for each component
    const std::map<TString, TString>& MorphVariables() const { return m_morphVariables; }

    //! returns the map variable_name -> list of names of morph points to use, for each component
    const std::map<TString, std::vector<TString> >& MorphPointNames() const { return m_morphPointNames; }

    //! returns the map variable_name -> list of positions of morph points to us, for each component
    const std::map<TString, std::vector<double> >& MorphPointPositions() const { return m_morphPointPositions; }
      
    TString Data() const { return m_data.c_str(); }

    TString CurrentLine();

    TString ERROR();

    TString MapState(const TString& state, RooMappedCategory& mapCat, RooAbsCategory& cat);

    TString DumpStream();
    static int RFind(const TString& s, const TString& pattern);
        
   private:

    TString m_savedToken;
    ModelType m_modelType;   //!< the type of the model defined in the datacard
    RooArgList m_dependents; //!< the list of dependents defined in the datacard
    RooArgList m_categories; //!< the list of categories defined in the datacard
    RooArgList m_auxCategories; //!< the list of auxiliary categories defined in the datacard
    std::vector<TString> m_componentNames; //!< the names of the components defined in the datacard
    std::map<TString, std::vector<HftPdfBuilderSpec> > m_componentSpecs; //!< the PDF builder classes for the components defined in the datacard (the map key is the component name, and the vector holds the list of builder class names)
    std::vector<TString> m_constraintNames; //!< the names of the constraints defined in the datacard
    std::map<TString, HftPdfBuilderSpec> m_constraintSpecs; //!< the PDF builder classes for the constraints defined in the datacard (the map key is the constraint name, and payload the specs of the associated builder)
    std::map<TString, TString> m_varTitles;
    std::map<TString, TString> m_varUnits;
    std::map<TString, bool> m_varFixedInCompFit;
    std::vector<TString> m_derivedRealNames; //!< the names of the formulas defined in the datacard
    std::map<TString, TString> m_formulas; //!< the formula strings for the formulas defined in the datacard, maps var_name -> formula)
    std::map<TString, HftFunctionBuilderSpec> m_functionSpecs; //!< the specs of the functions defined in the datacard (the map key is the constraint name, and payload the specs)
    std::vector<TString> m_fixedInCompFit; //!< Names of the variables fixed for component fits
    std::map<TString, TString > m_componentNorms; //!< the normalization type for each component; the map key is the component name.
    TString m_modelCatName; //!< the name of the model category along which the PDF for is split (non-"" only for models of type @c MultiPdfModel)
    std::map< TString, std::vector< std::set<TString> > > m_modelCatStates; //!< cat states for which to use a component model (inner vector) for a given component (outer map on component name)
    std::vector< std::vector<unsigned int> > m_globalModelIndices; //!< which component model to use for global model i (outer vector = models, inner vector = components)
    std::vector< std::set<TString> > m_globalModelCatStates; //!< which cat states (the inner set) to use for each global model (outer vector)
    std::map<TString, std::vector<TString> > m_splittingCatMap; //!< map of category_name -> vector of split var_names; keys are categories
    std::map<TString, std::vector<TString> > m_splittingVarMap; //!< map of var_names -> vector of catergory_names splitting those variables; keys are variables
    bool m_noNormVarSplitting;
    std::map<TString, TString> m_mapCatMap;
    std::map<TString, TString> m_morphVariables; //!<  map variable_name -> name of morphing var
    std::map<TString, std::vector<TString> > m_morphPointNames; //!< map variable_name -> list of names of morph points
    std::map<TString, std::vector<double>  > m_morphPointPositions; //!< map variable_name -> list of positions of morph points
    std::string  m_data;  //!< the string containing the data
    std::istream*  m_dataStream;  //!< the stream from which the data is read.
    RooStreamParser*  m_parser;  //!< the stream parser.
    bool m_verbose;
  };
}

#endif
