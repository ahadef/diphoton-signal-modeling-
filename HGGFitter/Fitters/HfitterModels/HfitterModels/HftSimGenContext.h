/*****************************************************************************
 * Project: RooFit                                                           *
 * Package: RooFitCore                                                       *
 *    File: $Id: RooSimGenContext.h,v 1.12 2007/05/11 09:11:30 verkerke Exp $
 * Authors:                                                                  *
 *   WV, Wouter Verkerke, UC Santa Barbara, verkerke@slac.stanford.edu       *
 *   DK, David Kirkby,    UC Irvine,         dkirkby@uci.edu                 *
 *                                                                           *
 * Copyright (c) 2000-2005, Regents of the University of California          *
 *                          and Stanford University. All rights reserved.    *
 *                                                                           *
 * Redistribution and use in source and binary forms,                        *
 * with or without modification, are permitted according to the terms        *
 * listed in LICENSE (http://roofit.sourceforge.net/license.txt)             *
 *****************************************************************************/
#ifndef HFT_SIM_GEN_CONTEXT
#define HFT_SIM_GEN_CONTEXT

#include "RooSimGenContext.h"

class HftSimGenContext : public RooSimGenContext {
public:
  HftSimGenContext(const RooSimultaneous &model, const RooArgSet &vars, const RooDataSet *prototype= 0,
                   const RooArgSet* auxProto=0, Bool_t _verbose= kFALSE)
     : RooSimGenContext(model, vars, prototype, auxProto, _verbose) { }

  void dump();
  RooDataSet* createDataSet(const char* name, const char* title, const RooArgSet& obs);

  virtual ~HftSimGenContext() { }
};

#endif
