#ifndef ROOT_Hfitter_HftModelBuilder
#define ROOT_Hfitter_HftModelBuilder

#include "TNamed.h"
#include "TString.h"
#include "RooWorkspace.h"
#include "HfitterModels/HftDatacardReader.h"

#include <vector>

#include <iostream>
using std::cout;
using std::endl;

namespace RooStats { class ModelConfig; }

namespace Hfitter {

   class HftAbsPdfBuilder;
   class HftModelSplitter;
   class HftModel;
   
   /** @class HftModelBuilder
      @author Nicolas Berger
      @author Andreas Hoecker

    Class that creates a generic fitting model from the information contained in a datacard.
  */
 
   class HftModelBuilder : public TNamed {

   public:

    //! constructor. @param name : the name of the created model. @param datacard : datacard file name
    HftModelBuilder( const TString& name = "", const TString& datacard = "", bool verbose = false);      
    virtual ~HftModelBuilder() { }

    //! returns the model instance
    RooAbsPdf* Pdf() { return m_pdf; }
    
    //! static function that directly returns the model instance. @param name : the name of the created model. @param datacard : datacard file name
    static HftModel* Create(const TString& name, const TString& datacard, bool verbose = false);
    static HftModel* Create(const TString& datacard) { return HftModelBuilder::Create("", datacard); }

    //! @param userCompNames : name of the components like signal and background
    static HftModel* Create(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf, const TString& obsName = "observables",
                            const TString& auxName = "auxObservables", const TString& compName1 = "Signal", const TString& compName2 = "Background");
    static HftModel* Create(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf, const TString& obsName,
                            const TString& auxName, const std::vector<TString>& userCompNames, bool ownsWS = false);

    //! @param userCompNames : name of the components like signal and background
    static HftModel* Create(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                  const RooArgSet& observables, const RooArgSet& auxObservables,
                                  const std::vector<TString>& userCompNames = std::vector<TString>(), bool ownsWS = false);

    //! @param modelConfigName : Name of ModelConfig object in the workspace
    static HftModel* CreateFromWS(const RooWorkspace& workspace, const TString& modelConfigName = "mconfig",
                                  const TString& modelName = "model");
    static HftModel* CreateFromWSFile(const TString& ws_file, const TString& ws_name = "modelWS", 
                                      const TString& modelConfigName = "mconfig", const TString& modelName = "model");
    static HftModel* CreateFromWSPdf(const TString& ws_file, const TString& ws_name, 
                                     const TString& pdfName, const TString& obsName, const TString& modelName = "model");
        
    static HftModel* CreateFromConfig(const RooStats::ModelConfig& modelConfig, const TString& modelName = "");
                            
    static HftModel* CreateSimpleModel(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf, 
                                       const TString& obsName = "observables", const TString& auxName = "auxObservables",
                                       const std::vector<TString>& userCompNames = std::vector<TString>());

    static HftModel* CreateSimpleModel(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                       const RooArgSet* observables, const RooArgSet* auxObservables,
                                       const std::vector<TString>& userCompNames = std::vector<TString>(),
                                       bool ownsWS = false);

    
    static bool MakeSplitComponents(const RooAbsPdf& pdf, RooWorkspace& workspace, const RooArgSet* wsObs, const RooArgSet* wsAux,
                                    const std::vector<TString>& userCompNames, std::vector<Hfitter::HftModel*>& componentModels);

    //! returns the type of the created model (see @c HftEnums::ModelType for possible values)
    ModelType Type() const { return m_datacardReader.Type(); }
     
    //! read model parameters from the datacard
    void ReadParameters(RooAbsReal& real);
        
    const HftDatacardReader& DatacardReader() const { return m_datacardReader; }

    RooWorkspace* Workspace() { return m_workspace; }
    
   private:

    HftDatacardReader m_datacardReader; //!< an instance of the datacard parser
    RooAbsPdf* m_pdf;               //!< the created model instance
    RooWorkspace* m_workspace;
    HftModelSplitter* m_splitter;       //!< an instance of the model splitter, for the case of split models
    bool m_verbose;
    
    
    ClassDef(Hfitter::HftModelBuilder, 1); //!< ROOT boilerplate
    
   public:

     //!< print welcome message (to be called from, eg, .Hfitterlogon)
     static void HfitterWelcomeMessage() {
      const TString BC__          = "\033[1m"    ;
      const TString EC__          = "\033[0m"    ;
      cout << BC__ << "Hfitter -- Higgs -> gamma gamma fitting toolkit"
            << EC__ << endl << endl;;
     }
   };
}

#endif
