#ifndef ROOT_Hfitter_HftFactoryPdfBuilder
#define ROOT_Hfitter_HftFactoryPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include <vector>

namespace Hfitter {

  /** @class HftFactoryPdfBuilder
      @author Nicolas Berger

  */
  class HftFactoryPdfBuilder : public HftAbsPdfBuilder {
    
   public:
    
    //! constructor
    HftFactoryPdfBuilder() { }
   
    void Setup(const char* factory_expr);
   
    //! destructor
    virtual ~HftFactoryPdfBuilder() { }
        
    //! returns the built PDF object. Must be implemented in derived classes. 
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;
      
   private:
    
    TString m_expr;     
    ClassDef(HftFactoryPdfBuilder, 1);
  };
}

#endif
