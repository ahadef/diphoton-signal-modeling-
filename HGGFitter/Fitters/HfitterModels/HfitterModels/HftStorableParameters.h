#ifndef ROOT_Hfitter_HftStorableParameters
#define ROOT_Hfitter_HftStorableParameters

#include <map>
#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterStorage.h"

class RooRealVar;

namespace Hfitter {

  
  /** @class HftStorableParameters
      @author Nicolas Berger

    A class describing a list of PDF parameters. The accessor to the parameter list itself is pure virtual, and helper functions
    are implemented to retrieve individual parameter, save and load their values, set them free or fixed, etc.
  */
  
  class HftStorableParameters : public HftAbsParameters {

   public:
    
    //! default constructor
    HftStorableParameters() { }
    
    //! default constructor with clients (see @c HftAbsParameters). @param clients : vector of clients.
    HftStorableParameters(std::vector<Hfitter::HftAbsParameters*>& clients) : HftAbsParameters(clients) { }
    
    //! copy constructor. @param other : object to copy.
    HftStorableParameters(const HftStorableParameters& other) : HftAbsParameters(other), m_savedStates(other.m_savedStates) { }
    
    virtual ~HftStorableParameters() { }

    //! save parameters values and constness (fixed/floating) to a storage container. @param stateName : the name of storage object holding the saved parameter data.
    bool SaveState(const TString& stateName) const;
    
    //! load parameter values and constness (fixed/floating) from a storage container. @param stateName : the name of the storage object holding the saved parameter data.
    bool LoadState(const TString& stateName, bool values = true, bool constness = true);

    //! save parameters values and constness (fixed/floating) to a storage container. @param state : the storage object holding the saved parameter data.
    bool SaveState(HftParameterStorage& state) const;
    
    //! load parameter values and constness (fixed/floating) from a storage container. @param state : the storage object holding the saved parameter data.
    //! @param allowPartial : if false, will return an error if not all the variables of the specified state are stored here. 
    bool LoadState(const HftParameterStorage& state, bool values = true, bool constness = true, bool allowPartial = false);
    
    //! fills a double with the saved value of a variable, for a given state. @param var : the variable to load the value of. 
    //! @param val : the output argument for the stored value. @param stateName : name of the state to use.
    bool SavedValue(const RooRealVar& var, double& val, const TString& stateName) const;

    //! returns the saved state with the specified name. @param stateName : name of the state 
    const HftParameterStorage* SavedState(const TString& stateName) const;
    
    //! dumps the names of all defined states.
    void PrintSavedStates() const;
      
   private:
     
     mutable std::map<TString, HftParameterStorage> m_savedStates;
  };
}

#endif
