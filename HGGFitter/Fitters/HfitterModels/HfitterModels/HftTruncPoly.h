#ifndef ROOT_Hfitter_HftTruncPoly
#define ROOT_Hfitter_HftTruncPoly

#include "RooAbsPdf.h"
#include "RooRealProxy.h"
#include "RooListProxy.h"

class RooRealVar;
class RooAbsReal;
class RooArgList;

namespace Hfitter {

  /** @class HftTruncPoly
      @author Nicolas Berger

    RooAbsPdf implementation for a polynomial with a cutoff. The PDF form is
    P(x) = c_1*x^a_1 + ...  + c_n*x^a_n  for x < endpoint, K for x > endpoint (or the other way around)
  */

   class HftTruncPoly : public RooAbsPdf {

   public:
     
    HftTruncPoly() { }
     
    //! Constructor. @param name : PDF name @param title : PDF title @param x : PDF dependent variable @param coefList : list of polynomial coefficients. @param expoList : list of polynomial exponents (should have same size as @c coeffList). @param endpoint : endpoint position. @param cst : PDF value above endpoint. @param above : if true, the PDF is constant for x > endpoint; if false, for x < endpoint.
    HftTruncPoly( const char* name, const char* title, 
                   RooAbsReal& x, const RooArgList& coefList, const RooArgList& expoList, 
                   RooAbsReal& endpoint, RooAbsReal& cst, bool above = true );

    //! copy constructor. @param other : the instance to copy. @param name : optional new name name for the new PDF (otherwise taken from the original)
    HftTruncPoly( const HftTruncPoly& other, const char* name = 0 );     
    virtual ~HftTruncPoly() {}

    //! returns a clone of the PDF with a new name. @param newName : name of the cloned PDF
    virtual TObject* clone( const char* newName ) const { return new HftTruncPoly( *this, newName ); }
     
    //! determines whether an analytical integral can be computed. Here always true. @param allVars : the PDF parameters. @param analVars : . @param rangeName : .
    Int_t    getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, const char* rangeName=0 ) const;
    //! returns the analytical for of the PDF integral (avoids numerical integration). @param code : . @param rangeName : .
    Double_t analyticalIntegral( Int_t code, const char* rangeName=0 ) const;
     
   protected:

    //! evaluates the PDF for the current parameter values    
    Double_t evaluate() const;
       
    //! returns the integral of the polynomial part of the PDF in the specified range
    Double_t integral_pol(Double_t xMin, Double_t xMax) const;

    //! returns the integral of the constant part of the PDF in the specified range
    Double_t integral_cst(Double_t xMin, Double_t xMax) const;
     
    RooRealProxy m_x;               //!< dependent variable
    RooListProxy m_coefs;           //!< list of polynomial coefficients
    RooListProxy m_expos;           //!< list of polynomial exponents
    RooRealProxy m_endpoint;        //!< endpoint position
    RooRealProxy m_cst;             //!< value of PDF beyond endpoint
    bool m_above;                   //!< defines whether the PDF is constant above (true) or below (false) the endpoint

    ClassDef(Hfitter::HftTruncPoly, 1); //!< ROOT boilerplate
   };
}

#endif
