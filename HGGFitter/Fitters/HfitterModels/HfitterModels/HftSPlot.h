#ifndef ROOT_Hfitter_HftSPlot
#define ROOT_Hfitter_HftSPlot

#include "TString.h"
#include "TMatrixDSym.h"
#include <cfloat>

class RooRealVar;
class RooAbsData;
class RooDataSet;
class RooPlot;

class TTree;

namespace Hfitter {

  class HftModel;

  /** @class HftSPlot
      @author Nicolas Berger
    
  */  

  class HftSPlot {

   public:

    HftSPlot(HftModel& model, RooAbsData& data, const TString& componentName = "Signal");
    virtual ~HftSPlot();
    
    static HftSPlot* Create(HftModel& model, RooAbsData& data, const TString& componentName = "Signal", bool refit = false);
    
    // Mostly-internal functions called by ::SPlot
    bool GetCovMatrix(TMatrixDSym& v, bool refit = false);
    bool MakeSData(const TMatrixDSym& v);
        
    bool Initialize(bool refit = false);
    
    RooDataSet* SData() const { return m_sData; }
    RooRealVar* SWeight() const { return m_sWeight; }
    
    TTree* AddWeight(TTree& tree, const TString& varName = "sWeight");
    
   private:
    
    HftModel* m_model;
    HftModel* m_componentModel;
    unsigned int m_componentIndex;
    RooAbsData* m_data;    
    RooRealVar* m_sWeight;
    RooDataSet* m_sData;
  };
}

#endif

