#ifndef ROOT_Hfitter_HftHistPdfBuilder
#define ROOT_Hfitter_HftHistPdfBuilder

#include "HfitterModels/HftAbsPdfBuilder.h"

#include <vector>

namespace Hfitter {

  /** @class HftHistPdfBuilder
      @author Nicolas Berger

  */
  class HftHistPdfBuilder : public HftAbsPdfBuilder {
    
   public:
    
    //! constructor
    HftHistPdfBuilder() { }
   
    void Setup(const char* dep1, const char* fileName, const char* key);
    //void Setup(const char* fileName, const char* key, const char* dep1, const char* dep2 = "", const char* dep3 = "");
   
    //! destructor
    virtual ~HftHistPdfBuilder() { }
        
    //! returns the built PDF object. Must be implemented in derived classes. 
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const;
      
   private:
    
    TString m_fileName, m_key;
    std::vector<TString> m_dependents;
     
    ClassDef(HftHistPdfBuilder, 1);
  };
}

#endif
