#include "TString.h"
#include <vector>
#include <map>

#include "HfitterModels/Options.h"

#include "HfitterModels/HftEnums.h"

#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/HftStorableParameters.h"

#include "HfitterModels/HftRealMorphVar.h"

#include "HfitterModels/HftInstance.h"
#include "HfitterModels/HftAbsNodeBuilder.h"
#include "HfitterModels/HftAbsRealBuilder.h"
#include "HfitterModels/HftAbsPdfBuilder.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftConstraint.h"

#include "HfitterModels/HftData.h"
#include "HfitterModels/HftDataHist.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftDatacardReader.h"
#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftModelSplitter.h"
#include "HfitterModels/HftSPlot.h"

#include "HfitterModels/HftDeltaPdf.h"
#include "HfitterModels/HftDoubleGaussian.h"
#include "HfitterModels/HftFermiPolyExp.h"
#include "HfitterModels/HftPeggedPoly.h"
#include "HfitterModels/HftPolyExp.h"
#include "HfitterModels/HftTruncPoly.h"

#include "HfitterModels/HftCountingPdfBuilder.h"
#include "HfitterModels/HftHistPdfBuilder.h"
#include "HfitterModels/HftGenericRealBuilder.h"
#include "HfitterModels/HftGenericPdfBuilder.h"
#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "HfitterModels/HftMultiGaussianBuilder.h"
#include "RooGaussian.h"
#include "RooBifurGauss.h"
#include "RooUniform.h"

Hfitter::HftTemplatePdfBuilder<RooGaussian, 2> instance_RooGaussian;
Hfitter::HftTemplatePdfBuilder<RooBifurGauss, 3> instance_RooBifurGauss;
Hfitter::HftTemplatePdfBuilder<RooUniform, 0> instance_RooUniform;

std::vector<Hfitter::HftParameterStorage> instance_vector_hftps;
