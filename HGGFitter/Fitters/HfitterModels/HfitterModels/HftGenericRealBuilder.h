#ifndef ROOT_Hfitter_HftGenericRealBuilder
#define ROOT_Hfitter_HftGenericRealBuilder

#include "HfitterModels/HftAbsRealBuilder.h"

namespace Hfitter {

  /** @class HftGenericRealBuilder
      @author Nicolas Berger

  */
  
  typedef double (*RealFunc1Type)(double);
  typedef double (*RealFunc2Type)(double, double);
  typedef double (*RealFunc3Type)(double, double, double);
  
  
  class HftGenericRealBuilder : public HftAbsRealBuilder {
    
   public:

    //! constructor
    HftGenericRealBuilder() { }
   
    void Setup(const char* par1 = "", const char* par2 = "", const char* par3 = "");
   
    //! destructor
    virtual ~HftGenericRealBuilder() { }

    //! returns the built PDF object. Must be implemented in derived classes. 
    //! @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    RooAbsReal* Real(const TString& name, RooWorkspace& workspace) const;

    void SetFunc(void* func) { m_func = func; }

   private:
    
    void* m_func;
    std::vector<TString> m_args;
     
    ClassDef(HftGenericRealBuilder, 1);
  };
}

#endif
