#ifndef ROOT_Hfitter_HftAbsPdfBuilder
#define ROOT_Hfitter_HftAbsPdfBuilder

#include "HfitterModels/HftAbsNodeBuilder.h"
#include "HfitterModels/HftAbsParameters.h"
#include "RooRealVar.h"
#include "TString.h"

class RooAbsPdf;
class RooWorkspace;
class RooStringVar;

namespace Hfitter {


  /** @class HftAbsPdfBuilder
      @author Nicolas Berger

    Base class for PDF builders. These are objects that can build PDFs from a configuration loaded from a datacard.
    
    When implementing the PDF form, the @c Dependent and @c Param functions should be used to define dependents and parameters
    on the fly, whose properties will later be loaded from datacards.
    
    Client models can be specified (see @c HftAbsParameters for details), whose parameters may be accessed when defining the PDF form.
  */
  
  class HftAbsPdfBuilder : public HftAbsNodeBuilder {
    
  public:
    
    //! constructor. @param partner : partner to add
    HftAbsPdfBuilder() { }
    
    //! destructor
    virtual ~HftAbsPdfBuilder() { }

    //! returns the built PDF object. Must be implemented in derived classes. @param name : the name of the created PDF. @param dependents : the list of dependents from which to pick those of the created PDF.
    virtual RooAbsPdf* Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const = 0;

    static HftAbsPdfBuilder* Create(const TString& className, const TString& args, bool verbose = false);

  protected:

    //! returns a dependent variable to be used when defining the PDF form. @param dependents : a list of available dependent, from which the dependent must be picked. (This will usually be the one provided by the second argument of @c HftAbsParameters::GetPdf). @param name : the name of the dependent variable. @param unit : the unit of the dependent. @param title : a title for the dependent.
    RooRealVar* Dependent(const RooArgList& dependents, const TString& name, 
                          const TString& unit = "", const TString& title = "") const;
    
  private:

    TString m_prefix, m_suffix;
    
    friend class HftModelBuilder;
    
    ClassDef(Hfitter::HftAbsPdfBuilder, 1);
  };
}

#endif
