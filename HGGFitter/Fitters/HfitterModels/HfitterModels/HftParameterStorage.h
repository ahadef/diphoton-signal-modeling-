// $Id: HftComponent.h,v 1.4 2008/05/19 23:15:36 nberger Exp $   
// Author: Nicolas Berger, Andreas Hoecker, Sandrine Laplace

// Higgs signal PDF

#ifndef ROOT_Hfitter_HftParameterStorage
#define ROOT_Hfitter_HftParameterStorage

#include "RooRealVar.h"
#include "RooCategory.h"
#include <map>

namespace Hfitter {

  /** @class HftParameterStorage
      @author Nicolas Berger

    A container class storing the value and constness (fixed/floating state) of a list of RooRealVar's.
  */
 
  class HftRooRealVarStorage {
  public:
    HftRooRealVarStorage(const RooRealVar& var);
    HftRooRealVarStorage(double a_val = 0, bool a_constness = false, double a_err = -1);
    void Load(RooRealVar& var, bool withValue = true, bool withConstness = true) const;
    
    double val, err;
    bool constness;
  };
  
  class HftParameterStorage {

    public:

     //! Constructor. 
     HftParameterStorage() { }
     HftParameterStorage(const TString& name, double val, double err = 0, bool constness = true) { Save(name, val, err, constness); }
     virtual ~HftParameterStorage() { }
      
     //! save the value and constness of a variable. If saved data already exists, it is overwritten. @param var : the RooRealVar to be stored.
     void Save(const RooRealVar& var);
     void Save(const TString& name, double val, double err = 0, bool constness = true);
     void Save(const RooCategory& cat);
     
     //! set the value and constness of the variable to their stored values. Returns false if no saved data exists. @param var : the RooRealVar to be loaded.
     bool Load(RooRealVar& var, bool value = true, bool constness = true) const;
     bool Load(RooCategory& cat) const;

     //! returns the saved value for the variable. Returns 0 if no saved data exists. @param var : the RooRealVar to be accessed.
     double Value(const RooRealVar& var) const;
     int    Value(const RooCategory& var) const;
     double Error(const RooRealVar& var) const;
     bool   Const(const RooRealVar& var) const;

     //! returns the saved value for the variable. Returns false if no saved data exists. @param var : the RooRealVar to be accessed. @param val : reference which will be set to the return value. 
     bool Value(const RooRealVar& var, double& val) const;
     bool Value(const RooCategory& var, int& val) const;
     bool Error(const RooRealVar& var, double& val) const;
     bool Const(const RooRealVar& var, bool& constness) const;

     //! returns saved real values
     double RealValue(const TString& varName) const;
     double RealError(const TString& varName) const;

     //! dump the stored information.
     void Print() const;
     TString Str() const;
     
     //! returns a list of variables with the stored values
     RooArgList StoredVars(bool vars = true, bool cats = true) const;
     
     bool Add(const HftParameterStorage& other, bool allowOverlap = false); 
      
     void Clear() { m_varStorage.clear(); m_catStorage.clear(); }
     
     unsigned int NVars() const { return m_varStorage.size(); }
     
     void FixAll(bool fix = true);
     HftParameterStorage AllFixed(bool fix = true) const;
     
     // copies existing variables from another storage. Returns false if not all were found.
     bool CopyValues(const HftParameterStorage& other);
     
  private:

     //! The storage structure: maps variable_name -> (saved_value, saved_constness)
     std::map<TString, Hfitter::HftRooRealVarStorage> m_varStorage;
     std::map<TString, int> m_catStorage;
  };
}

#endif
