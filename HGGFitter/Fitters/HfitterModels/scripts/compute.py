#! /usr/bin/env python

__doc__ = "Run statistical computations from the command line"
__author__ = "Nicolas Berger <Nicolas.Berger@cern.ch"

import os, sys
from optparse import OptionParser

import ROOT
from Hfitter import *

#######################################################################################################################################

def compute(args):
  """compute"""

  parser = OptionParser("%prog [--interval|--limit|--pvalue] variable --model model [--datafile|--dataname] data [options]")
  parser.description = __doc__
  parser.add_option("-c", "--datacard",   default='',      help="Name of datacard file", type=str)
  parser.add_option("-w", "--ws-file",    default='',      help="Name of file containing the workspace", type=str)
  parser.add_option(  "", "--ws-name",    default='modelWS',    help="Name workspace object inside the specified file", type=str)
  parser.add_option(  "", "--model-config-name", default='mconfig', help="Name of model config within the specified workspace", type=str)
  parser.add_option("-m", "--model-name", default='',      help="Name to assign to the model", type=str)
  parser.add_option("-x", "--execute",    default='',      help="Commands to run on model after instantiation", type=str)  
  parser.add_option("-=", "--setval",     default='',      help="Set a variable value, overriding model state", type=str)  
  parser.add_option("-r", "--setrange",   default='',      help="Set the range of a variable, overriding model state", type=str)  
  parser.add_option("-k", "--setconst",   default='',      help="Set a variable to be constant, overriding model state", type=str)  
  parser.add_option(  "", "--setfree",    default='',      help="Set a variable to be free, overriding model state", type=str)  
  parser.add_option(  "", "--category",   default='',      help="Specify that only a particular category should be included", type=str)  
  parser.add_option("-f", "--data-file",  default='',      help="Name of file containing the data", type=str)
  parser.add_option("-t", "--data-tree",  default='tree',  help="Name of TTree inside the specified data file", type=str)
  parser.add_option(  "", "--data-hist",  default='',      help="Name of histogram inside the specified data file", type=str)
  parser.add_option(  "", "--data-name",  default='obsData', help="Name of data object inside the specified workspace", type=str)
  parser.add_option("-a", "--asimov", action="store_true",    help="Use an Asimov dataset as the data")
  parser.add_option(  "", "--asimov-datacard", default='',  help="Datacard to use to generate Asimov dataset (if not specified, same as main)", type=str)
  parser.add_option(  "", "--fit",        default=None,    help="Return the best-fit values of the specified variables (separated by ':')", type=str)
  parser.add_option("-i", "--interval",   default='',      help="Compute confidence interval on the specified variable", type=str)
  parser.add_option("-l", "--limit",      default='',      help="Compute upper limit on the specified variable", type=str)
  parser.add_option(  "", "--exclusion-cl", default='',    help="Compute exclusion CL for the specified variable (use --hypo for the hypothesis", type=str)
  parser.add_option("-p", "--pvalue",     default='',      help="Compute discovery p-value using the specified signal-strength variable", type=str)
  parser.add_option("-z", "--significance", default='',    help="Compute discovery significance using the specified signal-strength variable", type=str)
  parser.add_option("-y", "--hypo",        default=None,   help="Compute confidence level with respect to the specified hypothesis", type=float)
  parser.add_option(  "", "--method",      default='',     help="The computation method to use", type=str)
  parser.add_option(  "", "--nsigmas",     default=1,      help="Number of sigmas of the confidence interval", type=float)
  parser.add_option(  "", "--cl",          default=0.95,   help="Confidence level for intervals and limits", type=float)
  parser.add_option(  "", "--tolerance",   default=0.0001, help="Tolerance on CL result for intervals and limits", type=float)
  parser.add_option(  "", "--fit-options", default='',     help="Options to use when fitting data", type=str)
  parser.add_option("-s", "--scan",        default='',     help="Variable over which to scan", type=str)
  parser.add_option("-n", "--npoints",                     help="Number of points to use for the scan", type=int)
  parser.add_option(  "", "--min",                         help="Position of the first scan point", type=float)
  parser.add_option(  "", "--max",                         help="Position of the last scan point", type=float)
  parser.add_option("-b", "--bands",       default=0,      help="Number of uncertainty bands to report", type=int)
  parser.add_option(  "", "--limit-hypos", default=None,   help="Hypothesis values to test for (obsolete, use --hypos instead)", type=str)
  parser.add_option(  "", "--hypos", default=None,   help="Hypothesis values to test for", type=str)
  parser.add_option(  "", "--sampling-dists", default=None,help="Specification of sampling distributions", type=str)
  parser.add_option("-:", "--generate-sampling-dists",default='', help="Generate sampling distributions needed for toys-based results, and store in specified filename", type=str)
  parser.add_option(  "", "--sampling-ntoys",  default=0,  help="Number of toys to use instead of asymptotic formulas", type=int)
  parser.add_option("-%", "--save-interval", default=20,   help="Frequency of writing results to file", type=int)
  parser.add_option("-o", "--output-file", default='',     help="Name of output file", type=str)
  parser.add_option("->", "--log",         default='',     help="Name of log file", type=str)
  parser.add_option("-v", "--verbosity",   default=-1,     help="Verbosity level", type=int)
  parser.add_option(      "--plot",        default='',     help="Plot the result", type=str)
  parser.add_option(  "", "--plot-options",default='',     help="Options to use for plotting", type=str)
  parser.add_option(  "", "--plot-bins",   default=0,      help="Binning to use for plotting", type=int)
  parser.add_option(      "--binned", action="store_true", help="Use binned data")
  parser.add_option(  "", "--weight-var",  default='',     help="Name of TTree variable containing event weight", type=str)
  parser.add_option("-g", "--add-ghosts",  default='',     help="Add ghost events to the data (format: var,min,max,step)", type=str)
  parser.add_option(  "", "--reset-state", action='store_true',     help="Reset initial state of the model between fits (default: false)")
  parser.add_option(  "", "--no-exp", action='store_true', help="do not run the expected PLR computation")
  parser.add_option(  "", "--init-states",  default='',    help="add initial states for pvalue computation", type=str)
  parser.add_option("-u", "--report-uncertainty",  action='store_true', help="report parameter uncertainty and not value when using --fit option")
  parser.add_option("-j", "--json-input-file",  default='',  help="Name of json file specifying argument values", type=str)
  parser.add_option(  "", "--json-output-file", default='',  help="Name of json file where to store argument values", type=str)
  parser.add_option("-e", "--only-encode", action='store_true',  help="Only encode the arguments into json format")
  
  options = parse(parser, args)
  if not options : return
  
  if options.output_file :
    if not options.scan :
      if os.path.exists(options.output_file) :
        print 'WARNING : output file %s already exists, will not overwrite' % options.output_file
        return

  suppress_output(options.verbosity)
  load_libraries()
  fitopts = ROOT.Hfitter.Options(options.fit_options)
  if options.verbosity == -1 : options.verbosity = fitopts.Verbosity()
  suppress_output(options.verbosity)
  # redirect output, if requested
  if options.log :
    try:
      os.makedirs(os.path.split(options.log)[0])
    except:
      pass
    try:
      save = os.dup( sys.stdout.fileno() )
      newout = file(options.log, 'w' )
      os.dup2( newout.fileno(), sys.stdout.fileno() )
    except:
      print 'ERROR: cannot write output to %s, aborting' % options.log
      return

  # Build the model
  model = build_model(options.datacard, options.model_name, options.ws_file, options.ws_name, options.model_config_name, 
                      options.setval, options.setrange, options.setconst, options.setfree, options.category, options.verbosity)
  if not model : return
  if options.execute : ROOT.gROOT.ProcessLine(options.execute)
  
  # Open the data
  asimov_model = build_asimov_model(model, options.asimov, options.asimov_datacard, options.verbosity)
  data = open_data(model, options.data_file, options.data_tree, options.data_hist, options.data_name, asimov_model, 
                   None, options.weight_var, options.binned, options.add_ghosts, options.verbosity)
  if not data : return

  hypos = options.hypos
  if options.limit_hypos : hypos = options.limit_hypos

  # Build the calculator
  calc = build_calculator(model, options.fit, options.pvalue, options.significance, options.limit, options.exclusion_cl,
                          options.interval, options.method, fitopts, options.bands, options.cl, options.tolerance, 
                          options.nsigmas, options.sampling_ntoys, options.binned, options.save_interval,
                          hypos, options.sampling_dists, options.hypo, options.scan, options.npoints, options.min, options.max, options.reset_state, options.no_exp, options.init_states, options.report_uncertainty, options.verbosity)
  if not calc : return

  # Generate sampling distributions, if requested
  if options.generate_sampling_dists :
    try :
      (sampling_ntoys, sampling_fileRoot) = options.generate_sampling_dists.split(',')
      sampling_ntoys = int(sampling_ntoys)
    except:
      print 'ERROR : argument of --generate_sampling_dists should be of the form ntoys,filename'
      return
    if options.limit and options.limit_hypos :
      calc.Calculator().GenerateHypoSamplingData(asimov_model.CurrentState(), sampling_ntoys, sampling_fileRoot, 'tree', True if options.binned else False, options.save_interval)
      calc.SetSamplingData(sampling_fileRoot, 'tree')
    else :
      print 'ERROR : Generation of sampling distributions not supported for this configuration'
      return


  # Compute the result
  if options.verbosity >= 2 : calc.Print("RVV")
  if options.verbosity >= 1 : print 'INFO : performing calculation on dataset with n = %d, w = %g' % (data.numEntries(), data.sumEntries())
  calc.SetVerbosity(options.verbosity)
  if options.verbosity >= 3 : ROOT.Hfitter.HftData.PrintData(data);
  calc.LoadInputs(data)
  if options.verbosity >= 2 : calc.Print("RVV")
  
  # and print it, if it's a single result
  if not options.scan : 
    calc.Result(options.bands).Print()
  else : # else draw the curve
    for i in range(0, options.npoints) :
      print '%s = %g : %s' % (scanVar, calc.Position(i), calc.Calculator(i).Result(options.bands).Str())
    c1 = ROOT.TCanvas('c1', '', 800, 600)
    calc.Curve(options.bands).Draw()
    
  if options.output_file :
    if not options.scan :
      if options.verbosity > 1 : print 'INFO: Saving calculator'
      calc.SaveToFile(options.output_file)
      if options.verbosity > 1 : print 'INFO: Saving result'
      f = ROOT.TFile.Open(options.output_file, 'UPDATE')
      result = calc.Result(options.bands)
      arr = ROOT.TArrayD(2*options.bands + 1)
      arr[0] = result.Value()
      for i in range(0, options.bands) : 
        arr[2*i + 1] = result.Error(+(i+1))
        arr[2*i + 2] = result.Error(-(i+1))
      f.WriteObjectAny(result, 'Hfitter::HftInterval', 'result')
      f.WriteObjectAny(arr, 'TArrayD', 'num_results')
      if options.interval : 
        cl_graph = calc.CLCurve().Graph()
        cl_graph.SetName("CL_scan")
        cl_graph.Write()
        plr_graph = calc.StatisticCurve().Graph()
        plr_graph.SetName("PLR_scan")
        plr_graph.Write()
    else : # else draw the curve
      nameroot = os.path.splitext(options.output_file)[0]
      calc.SaveToFile(nameroot)
      picfile = nameroot + '.png'
      c1.Print(picfile)
      result = calc.Curve(options.bands)
      f = ROOT.TFile.Open(options.output_file, 'RECREATE')
      f.WriteObjectAny(result, 'Hfitter::HftBand', 'result')
      for i in range(0, options.bands + 1) :
        graph = result.Graph(i, 'curve_%d' % i)
        graph.Write()
    if options.verbosity > 1 : print 'INFO: Closing output'
    f.Close()
  
  if options.plot :
    varName = options.plot if options.plot else model.Observables()[0].GetName()
    if not model.Var(varName) :
      print 'ERROR: variable %s is not present in the model, cannot plot' % varName
      return
    output_name = (os.path.splitext(options.output_file)[0] if options.output_file else 'plot') + '.pdf'
    ROOT.Hfitter.Options.SetHfitterStyle()
    canvas = ROOT.TCanvas('canvas', 'Plot of %s' % varName, 800, 600) 
    if options.plot_bins : 
      if options.verbosity > 1 : print 'INFO: Setting number of bins for %s to %d' % (varName, options.plot_bins)
      model.Var(varName).setBins(options.plot_bins)
    model.Plot(varName, data, ROOT.Hfitter.Options(options.plot_options))
    canvas.Print(output_name)
    
  if options.log :
    os.dup2( save, sys.stdout.fileno() )
    newout.close()

  dump_json(options)
###############################

# main
compute(sys.argv)
