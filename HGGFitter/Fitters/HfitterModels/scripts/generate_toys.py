#! /usr/bin/env python

__doc__ = "Run statistical computations from the command line"
__author__ = "Nicolas Berger <Nicolas.Berger@cern.ch"

#import ROOT, HfitterModels, HfitterStats
import os, sys
from optparse import OptionParser

import ROOT
from Hfitter import *

#######################################################################################################################################

def generate_toys(args):
  """generate_toys"""

  parser = OptionParser("%prog [--interval|--limit|--pvalue] variable --model model [--datafile|--dataname] data [options]")
  parser.description = __doc__
  parser.add_option("-c", "--datacard",   default='',      help="Name of datacard file", type=str)
  parser.add_option("-w", "--ws-file",    default='',      help="Name of file containing the workspace", type=str)
  parser.add_option(  "", "--ws-name",    default='modelWS',    help="Name workspace object inside the specified file", type=str)
  parser.add_option(  "", "--model-config-name", default='mconfig', help="Name of model config within the specified workspace", type=str)
  parser.add_option("-m", "--model-name", default='',      help="Name to assign to the model", type=str)
  parser.add_option("-x", "--execute",    default='',      help="Commands to run on model after instantiation", type=str)  
  parser.add_option("-=", "--setval",     default='',      help="Set a variable value, overriding model state", type=str)  
  parser.add_option("-r", "--setrange",   default='',      help="Set the range of a variable, overriding model state", type=str)  
  parser.add_option("-k", "--setconst",   default='',      help="Set a variable to be constant, overriding model state", type=str)  
  parser.add_option(  "", "--setfree",    default='',      help="Set a variable to be free, overriding model state", type=str)  
  parser.add_option(  "", "--category",   default='',      help="Specify that only a particular category should be included", type=str)  
  parser.add_option("-f", "--data-file",  default='',      help="Name of file containing the data", type=str)
  parser.add_option("-t", "--data-tree",  default='tree',  help="Name of TTree inside the specified data file", type=str)
  parser.add_option(  "", "--data-hist",  default='',      help="Name of histogram inside the specified data file", type=str)
  parser.add_option(  "", "--data-name",  default='',      help="Name of data object inside the specified workspace", type=str)
  parser.add_option(  "", "--fit",        default=None,    help="Return the best-fit values of the specified variables (separated by ':')", type=str)
  parser.add_option("-i", "--interval",   default='',      help="Compute confidence interval on the specified variable", type=str)
  parser.add_option("-l", "--limit",      default='',      help="Compute upper limit on the specified variable", type=str)
  parser.add_option(  "", "--exclusion-cl", default='',    help="Compute exclusion CL for the specified hypothesis", type=str)
  parser.add_option("-p", "--pvalue",     default='',      help="Compute discovery p-value using the specified signal-strength variable", type=str)
  parser.add_option("-z", "--significance", default='',    help="Compute discovery significance using the specified signal-strength variable", type=str)
  parser.add_option("-y", "--hypo",        default=None,   help="Compute confidence level with respect to the specified hypothesis", type=float)
  parser.add_option(  "", "--method",      default='',     help="The computation method to use", type=str)
  parser.add_option(  "", "--nsigmas",     default=1,      help="Number of sigmas of the confidence interval", type=float)
  parser.add_option(  "", "--cl",          default=0.95,   help="Confidence level for intervals and limits", type=float)
  parser.add_option(  "", "--tolerance",   default=0.0001, help="Tolerance on CL result for intervals and limits", type=float)
  parser.add_option(  "", "--fit-options", default='',     help="Options to use when fitting data", type=str)
  parser.add_option("-b", "--bands",       default=2,      help="Number of uncertainty bands to report", type=int)
  parser.add_option(  "", "--limit-hypos", default=None,   help="Hypothesis value to test for", type=str)
  parser.add_option(  "", "--sampling-dists", default=None,help="Specification of sampling distributions", type=str)
  parser.add_option("-:", "--generate-sampling-dists",default='', help="Generate sampling distributions needed for toys-based results, and store in specified filename", type=str)
  parser.add_option(  "", "--generate-sampling-only", default=-1, help="Generate a single sampling distribution only", type=int)
  parser.add_option("-d", "--gen-datacard",default='',     help="Datacard defining model for generation (if none, same as main model)", type=str)
  parser.add_option(  "", "--gen-hypo-var",default='',     help="Hypothesis variable for generation", type=str)
  parser.add_option(  "", "--gen-hypo",    default=None,   help="Hypothesis value to generate for", type=float)
  parser.add_option(  "", "--gen-calc-hypo", action='store_true', help="Generate the hypothesis defined by the calculator")
  parser.add_option("-n", "--ntoys",       default=None,   help="Number of toys to generate", type=int)
  parser.add_option(  "", "--sampling-ntoys",  default=0,  help="Number of toys to use instead of asymptotic formulas", type=int)
  parser.add_option("-o", "--output-file", default='',     help="Name of output file", type=str)
  parser.add_option("-%", "--save-interval", default=20,   help="Frequency of writing results to file", type=int)
  parser.add_option(  "", "--peek-file",   default='',     help="Name of file for debug output", type=str)
  parser.add_option(  "", "--save-toys",   default='',     help="Name of file in which to save the toy datasets", type=str)
  parser.add_option(  "", "--seed",        default=-1,     help="Random seed for the toy generation", type=int)
  parser.add_option("->", "--log",         default='',     help="Name of log file", type=str)
  parser.add_option("-v", "--verbosity",   default=-1,     help="Verbosity level", type=int)
  parser.add_option(      "--binned", action="store_true", help="Use binned data")
  parser.add_option(  "", "--weight-var",  default='',     help="Name of TTree variable containing event weight", type=str)
  parser.add_option("-g", "--add-ghosts", default='',      help="Add ghost events to the data (format: var,min,max,step)", type=str)
  parser.add_option(  "", "--reset-state", action='store_true',     help="Reset initial state of the model between fits (default: false)")
  parser.add_option(  "", "--no-exp", action='store_true', help="do not run the expected PLR computation")
  parser.add_option(  "", "--init-states",  default='',    help="add initial states for pvalue computation", type=str)
  parser.add_option("-u", "--report-uncertainty",  action='store_true', help="report parameter uncertainty and not value when using --fit option")
  parser.add_option("-j", "--json-input-file",  default='',  help="Name of json file specifying argument values", type=str)
  parser.add_option(  "", "--json-output-file", default='',  help="Name of json file where to store argument values", type=str)
  parser.add_option("-e", "--only-encode", action='store_true',  help="Only encode the arguments into json format")

  options = parse(parser, args)
  if not options : return

  if options.ntoys == None : 
    print 'ERROR: must specify a number of toys to generate using the -n or --ntoys options'
    return
  
  output_file = options.output_file
  if output_file == '' : 
    print 'ERROR: an output file must be specified using the -o or --output-file option'
    return
  
  if output_file[-5:] != '.root' and options.gen_hypo :
    output_file = '%s_%g.root' % (output_file, options.gen_hypo)

  suppress_output(options.verbosity)
  load_libraries()
  fitopts = ROOT.Hfitter.Options(options.fit_options)
  if options.verbosity == -1 : options.verbosity = fitopts.Verbosity()
  suppress_output(options.verbosity)
  
  models = []
  calcs = ROOT.std.vector('Hfitter::HftAbsCalculator*')()
  init_states = ROOT.std.vector('Hfitter::HftParameterStorage')()
  states = ROOT.std.vector('Hfitter::HftParameterStorage')()
  datacards = options.datacard.split(',')
  for datacard in datacards :
    # Build the model
    model_name = options.model_name
    if len(datacards) > 1 :
      model_name = '%s%d' % (options.model_name if options.model_name else 'model',len(models))
      print 'Defining %s from datacard %s' % (model_name,datacard)
    model = build_model(datacard, model_name, options.ws_file, options.ws_name, options.model_config_name,
                        options.setval, options.setrange, options.setconst, options.setfree, options.category, options.verbosity)
    if not model : return
    models.append(model)
    init_states.push_back(model.CurrentState())
    if options.execute : ROOT.gROOT.ProcessLine(options.execute)
      
    # Build the calculator
    calc = build_calculator(model, options.fit, options.pvalue, options.significance, options.limit, options.exclusion_cl, 
                            options.interval, options.method, fitopts, options.bands, options.cl, options.tolerance, 
                            options.nsigmas, options.sampling_ntoys, options.binned, options.save_interval,
                            options.limit_hypos, options.sampling_dists, options.hypo, None, None, None, None, 
                            options.reset_state, options.no_exp, options.init_states, options.report_uncertainty, options.verbosity)
    if not calc : return
    calcs.push_back(calc)
    states.push_back(ROOT.Hfitter.HftParameterStorage())
  # Define the generation model
  if options.gen_datacard : 
    gen_model = build_model(options.gen_datacard, options.model_name, verbosity=options.verbosity)
    gen_state = gen_model.CurrentState()
  else :
    gen_model = models[0]
    gen_state = init_states[0]
  if options.gen_hypo != None : 
    hypo_var_name = options.gen_hypo_var
    if not hypo_var_name and options.exclusion_cl : 
      hypo_var_name = options.exclusion_cl
    if not hypo_var_name : 
      try:
        hypovar = calc.HypoVar(0)
      except:
        pass
    if not hypo_var_name : 
      print "ERROR : could not guess the hypothesis variable for generation, please specify using the --gen-hypo-var option"
      return
    hypovar = gen_model.Var(hypo_var_name)
    if not hypovar :
      print "ERROR : variable '%s' is not part of the model, cannot set generation hypothesis" % hypo_var_name
      return
    if options.verbosity > 0 : 
      print 'INFO : setting generation hypothesis to %s = %d' % (hypo_var_name, options.gen_hypo)
    gen_state.Save(hypo_var_name, options.gen_hypo, 0, True);
  if options.gen_calc_hypo :
    try:
      hypovar = calc.HypoVar(0)
    except:
      print "ERROR : cannot retrieve hypothesis variable from calculator as requested using the --gen-hypo-var option."
      return
    if options.verbosity > 0 : 
      print 'INFO : setting generation hypothesis from calculator, %s = %d' % (hypovar.GetName(), calc.NullHypoValue(0))
    gen_state.Save(hypovar.GetName(), calc.NullHypoValue(0), 0, True)
  # redirect output, if requested
  if options.log :
    save = os.dup( sys.stdout.fileno() )
    newout = file(options.log, 'w' )
    os.dup2( newout.fileno(), sys.stdout.fileno() )
  
  # Generate sampling distributions, if requested
  if options.generate_sampling_dists :
    try :
      (sampling_ntoys, sampling_fileRoot) = options.generate_sampling_dists.split(',')
      sampling_ntoys = int(sampling_ntoys)
    except:
      print 'ERROR : argument of --generate_sampling_dists should be of the form ntoys,filename'
      return
    if options.limit and options.limit_hypos :
      if options.verbosity > 0 :
        print 'INFO : generating sampling distributions for state'
        gen_state.Print()
      if options.generate_sampling_only >= 0 :
        limit_hypos = options.limit_hypos.split(',')
        if options.generate_sampling_only >= len(limit_hypos) :
          print 'ERROR : cannot generate sampling distribution at index', options.generate_sampling_only, 'since only', len(options.limit_hypos), 'hypotheses are defined'
          return
        print 'INFO : only generating sampling distribution at index', options.generate_sampling_only, 'and hypo = ', limit_hypos[options.generate_sampling_only]
        calc.Calculator().GenerateHypoSamplingData(options.generate_sampling_only, gen_state, sampling_ntoys, sampling_fileRoot, 'tree',
                                                   True if options.binned else False, options.save_interval)
        return
      calc.Calculator().GenerateHypoSamplingData(gen_state, sampling_ntoys, sampling_fileRoot, 'tree', True if options.binned else False, options.save_interval)
      calc.SetSamplingData(sampling_fileRoot, 'tree')

    else :
      print 'ERROR : Generation of sampling distributions not supported for this configuration'
      return
  
  # Fit data, if requested
  if options.data_file or options.data_name :
    data = open_data(models[0], options.data_file, options.data_tree, options.data_hist, options.data_name, None, 
                     None, options.weight_var, options.binned, options.add_ghosts, options.verbosity)

    if options.verbosity > 0 : print 'INFO : profiling model parameters in data'
    gen_model.Fit(data, fitopts)

  # Generate
  if options.verbosity > 1 : 
    calc.Print("RVV")
    print 'INFO: generating in state'
    gen_state.Print()
  toys_calc = ROOT.Hfitter.HftToysCalculator(gen_model, gen_state, calcs, states, options.bands, options.binned == True, options.model_name)
  if options.add_ghosts :
    ghosts = parse_ghosts(gen_model, options.add_ghosts)
    if not ghosts : return None
    (ghostVar, ghostMin, ghostMax, ghostStep, ghostWeight) = ghosts
    toys_calc.SetGhosts(ghostVar, ghostMin, ghostMax, ghostStep, ghostWeight)

  if options.save_toys : toys_calc.SetSaveToys(options.save_toys)

  if options.seed > 0 :
    ROOT.RooRandom.randomGenerator().SetSeed(options.seed); 

  #toys_calc.RunToys(options.ntoys, 'HighMassLimitHiggs332/Limit/limit1500.root', 'tree', options.save_interval)
  #toys_calc.LoadFromFile('HighMassLimitHiggs332/Limit/limit1500.root', 'tree')
  if os.path.exists(output_file) :
    print 'INFO : output file %s already exists, will not generate toys but load the existing results' % output_file
  else:
    toys_calc.RunToys(options.ntoys, output_file, 'tree', options.save_interval)

  # Print result
  if options.peek_file : toys_calc.SetPeekFileName(options.peek_file)
  toys_calc.LoadFromFile(output_file, 'tree')
  result_file = output_file[:-5] + "_result.root"
  if options.verbosity > 1 : print 'INFO: Saving result'
  f = ROOT.TFile.Open(result_file, 'RECREATE')
  result = toys_calc.Result(options.bands)
  result.Print()
  arr = ROOT.TArrayD(2*options.bands + 1)
  arr[0] = result.Value()
  for i in range(0, options.bands) : 
    arr[2*i + 1] = result.Error(+(i+1))
    arr[2*i + 2] = result.Error(-(i+1))
  f.WriteObjectAny(result, 'Hfitter::HftInterval', 'result')
  f.WriteObjectAny(arr, 'TArrayD', 'num_results')
  f.Close()
    
  if options.log :
    os.dup2( save, sys.stdout.fileno() )
    newout.close()

  dump_json(options)
###############################

# main
generate_toys(sys.argv)
