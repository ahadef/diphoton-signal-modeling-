#! /usr/bin/env python

__doc__ = "Build workspaces from the command line"
__author__ = "Nicolas Berger <Nicolas.Berger@cern.ch"

import os, sys, json
from optparse import OptionParser
import ROOT


#######################################################################################################################################
  
def build_model(datacard = None, model_name = None, ws_file = None, ws_name = None, model_config_name = None, 
                setval = None, setrange = None, setconst = None, setfree = None, category = None, verbosity = 0) :
  """build the model, either from a datacard or a workspace"""
  if datacard :
    if verbosity > 0 : 
      print 'INFO : building model from datacard %s' % datacard
    model = ROOT.Hfitter.HftModelBuilder.Create(model_name if model_name != None else '', datacard)
  elif ws_file :
    if verbosity > 0 : 
      print 'INFO : building model from workspace %s in file %s' % (ws_name, ws_file)
    model = ROOT.Hfitter.HftModelBuilder.CreateFromWSFile(ws_file, ws_name, model_config_name, model_name if model_name != None else '')
  else:
    print 'ERROR : a model definition must be provided using either the --datacard or --ws-file options'
    return None
  if not model : 
    print 'ERROR : could not create the model'
    return None
  if category: 
    model = model.CategoryModel(category)
    if not model: 
      print "ERROR : model does not have a category '%s'" % category
      return None
  if setval :
    try: 
      sets = [ v.replace(' ', '').split('=') for v in setval.split(',') ]
      for (var, val) in sets :
        if not model.Var(var) :
          print "ERROR : cannot find variable '%s' in model" % var
          raise ValueError
        model.Var(var).setVal(float(val))
        print "INFO : setting %s=%g" % (var, float(val))
    except:
      print "ERROR : invalid variable assignment string '%s'." % setval
      return None
  if setrange :
    print setrange
    sets = setrange.replace(' ', '').split(',')
    if len(sets) != 3 : 
      print 'ERROR: invalid range specification %s, should be in the format <varname>,min,max' % setrange
      return None
    var = model.Var(sets[0])
    if not var :
      print "ERROR : cannot find variable '%s' in model" % sets[0]
      raise ValueError
    if sets[1] == '' :
      minVal = var.getMin()
    else:
      try:
        minVal = float(sets[1])
      except:
        print "ERROR : invalid lower bound specification '%s' when setting the range of %s" % (sets[1], sets[0])
        raise ValueError
    if sets[2] == '' :
      minVal = var.getMax()
    else:
      try:
        maxVal = float(sets[2])
      except:
        print "ERROR : invalid upper bound specification '%s' when setting the range of %s" % (sets[2], sets[0])
        raise ValueError
    var.setRange(minVal, maxVal)
    print "INFO : setting range of %s to [%g, %g]" % (var, minVal, maxVal)
  if setconst :
    varlist = setconst.split(',')
    for var in varlist :
      if not model.Var(var) :
        print "ERROR : cannot find variable '%s' in model" % var
        raise ValueError
      model.Var(var).setConstant()
      print "INFO : setting variable '%s' constant (current value: %g)" % (var, model.Var(var).getVal())
  if setfree :
    varlist = setfree.split(',')
    for var in varlist :
      if not model.Var(var) :
        print "ERROR : cannot find variable '%s' in model" % var
        raise ValueError
      model.Var(var).setConstant(False)
      print "INFO : setting variable '%s' free (current value: %g)" % (var, model.Var(var).getVal())
  if verbosity >=2 : model.PrintPdf()
  ROOT.gROOT.ProcessLine('model = HftModel::GetModel("%s");' % model.GetName())
  return model

def build_asimov_model(model, asimov, asimov_datacard, verbosity) :
  if asimov_datacard : return build_model(asimov_datacard, 'asimov', verbosity=verbosity)
  if asimov : return model
  return None

def open_data(model, data_file = None, data_tree = None, data_hist = None, data_input_name = None, asimov_model = None, 
              data_output_name = None, weight_var = '', binned = False, add_ghosts = '', verbosity = 0) :
  """open a dataset"""
  if asimov_model :
    if verbosity > 1 : asimov_model.CurrentState().Print()
    data = asimov_model.GenerateAsimov()
  elif data_file :
    if data_hist :
      hftData = ROOT.Hfitter.HftDataHist.Open(data_file, data_hist, model)
      if not hftData : return None # HftData already prints an error message
      data = hftData.DataHist()
      if verbosity > 0 : 
        print 'INFO : building binned dataset from histogram %s in file %s' % (data_hist, data_file)        
    else :
      hftData = ROOT.Hfitter.HftData.Open(data_file, data_tree, model, '', weight_var)
      if not hftData : return None # HftData already prints an error message
      data = hftData.DataSet()
      if binned : 
        if verbosity > 0 : 
          print 'INFO : building binned dataset from TTree %s in file %s' % (data_tree, data_file)        
        data = data.binnedClone()
      else :
        if verbosity > 0 : 
          print 'INFO : building unbinned dataset from TTree %s in file %s' % (data_tree, data_file)        
        data = data
  elif data_input_name :
    data = model.Workspace().data(data_input_name)
    if not data :
      print "ERROR : dataset '%s' not found in model workspace" % data_input_name
      return None
    if verbosity > 0 : 
      print "INFO : loading dataset '%s' from workspace" % data_input_name
  else :
    print 'ERROR : a dataset must be specified using either the --data-file or --data-name options'
    return None
  if data_output_name : data.SetName(data_output_name)
  if add_ghosts :
    ghosts = parse_ghosts(model, add_ghosts)
    if not ghosts : return None
    (ghostVar, ghostMin, ghostMax, ghostStep, ghostWeight) = ghosts
    ROOT.Hfitter.HftData.AddGhosts(data, model, ghostVar, ghostMin, ghostMax, ghostStep, ghostWeight) # will print errors if they occur
  return data


def build_calculator(model, fit, pvalue, significance, limit, excl_cl, interval, raw_method, fitopts, bands, cl, tolerance, nsigmas, 
                     sampling_ntoys, binned_toys, toys_save_interval, hypos, sampling_dists, hypo, scan, npoints, scan_min, scan_max, reset_state, 
                     no_exp, init_states, report_unc, verbosity, name = None) :
  hypo_vector = None
  if hypos :
    hypo_list = hypos.split(',')
    hypo_vector = ROOT.std.vector('double')()
    try:
      for h in hypo_list : hypo_vector.push_back(float(h))
    except:
      print 'ERROR: invalid hypothesis list %s' % hypo_list
      return None

  # Setup pvalue computation
  if fit != None :
    if verbosity > 0 : print 'INFO : building fit value calculator for the variable(s) %s' % fit
    calc = ROOT.Hfitter.HftFitValueCalculator(model, fit, fitopts, True, True if report_unc == True else False)
    if calc.NResults() == 0 :
      print 'ERROR : invalid fit variables provided, aborting'
      return 0
  elif pvalue or significance :
    if hypo and raw_method != 'twosided' :
      if raw_method != '' : print 'WARNING : changing method to TwoSided to test a non-zero hypothesis'
      raw_method = 'twosided'      
    method = raw_method.lower() if raw_method != '' else 'uncapped'
    var = pvalue if pvalue else significance
    if not model.Var(var) :
      print "ERROR : variable '%s' is not part of the model, cannot compute pvalue" % var
      return
    if method == 'uncapped' :
      if verbosity > 0 : print 'INFO : building uncapped p0 calculator'
      calc = ROOT.Hfitter.HftUncappedPValueCalculator(model, var, fitopts)
    elif method == 'onesided' or method == 'capped' :
      if verbosity > 0 : print 'INFO : building one-sided p0 calculator'
      calc = ROOT.Hfitter.HftOneSidedPValueCalculator(model, var, fitopts)
    elif method == 'twosided' :
      if verbosity > 0 : print 'INFO : building two-sided p0 calculator'
      calc = ROOT.Hfitter.HftPLRPValueCalculator(model, var, hypo if hypo else 0, fitopts)
    else :
      allowed =  [ 'uncapped', 'onesided', 'twosided' ]
      print "ERROR : unknown method '%s' for pvalue computation, supported options are %s" % (method, ", ".join(allowed))
      return None
    if significance : calc.SetResultIsSignificance()
    if init_states :
      try :
        (var, valstr) = init_states.split(':')
        if not model.Var(var) :
          print "ERROR : variable '%s' is not part of the model, cannot scan over it." % var
          return None
        state = calc.AltHypoCalc().InitialState()
        for val in valstr.split(',') :
          state.Save(var, float(val), 0, model.Var(var).isConstant())
          calc.AltHypoCalc().AddInitialState(state)
      except:
        print 'ERROR: invalid initial state specification %s' % init_states, sys.exc_info()[0]
        return None
    if no_exp:
      try:
        calc.DeleteExpCalc()
      except:
        print 'WARNING: could not disable the expected PLR computation, ignoring --no-exp option'

  # Setup limit computation
  elif limit or excl_cl :
    method = raw_method.lower() if raw_method != '' else 'clsqtildalimit'
    var = limit if limit else excl_cl
    if not model.Var(var) :
      print "ERROR : variable '%s' is not part of the model, cannot set limit." % var
      return None
    if hypo != None : model.Var(var).setVal(hypo)
    if method == 'clsqtildalimit' or method == 'qtildalimit' :
      if verbosity > 0 : print 'INFO : building qTilda limit calculator'
      if model.Var(var).getVal() == 0 : model.Var(var).setVal(1) # just in case, qtilda doesn't handle hypo=0 well
      calc = ROOT.Hfitter.HftQTildaLimitCalculator(model, var, fitopts)
    elif method == 'clsqlimit' or method == 'qlimit' :
      if verbosity > 0 : print 'INFO : building one-sided limit calculator'
      calc = ROOT.Hfitter.HftQLimitCalculator(model, var, fitopts)
    else :
      allowed = [ 'CLsQTildaLimit', 'CLsQLimit', 'QTildaLimit', 'QLimit' ]
      print "ERROR : unknown method '%s' for limit computation, supported options are %s" % (method, ", ".join(allowed))
      return None
    if method == 'clsqtildalimit' or method == 'clsqlimit' :
      if verbosity > 0 : print 'INFO : modifying calculator for CLs computation'
      calc = ROOT.Hfitter.HftCLsCalculator(calc)
    if limit :
      if hypo_vector != None :
        calc = ROOT.Hfitter.HftScanLimitCalculator(calc, var, hypo_vector)
      else :
        calc = ROOT.Hfitter.HftIterLimitCalculator(calc, bands, cl, tolerance)
        if sampling_ntoys > 0 : 
          print 'Sampling:', sampling_ntoys, type(sampling_ntoys), binned_toys, type(binned_toys), toys_save_interval, type(toys_save_interval)
          calc.SetNToys(sampling_ntoys)
          calc.SetToysSaveInterval(toys_save_interval)
          calc.SetBinnedToys(True if binned_toys else False) # in case it is None
  # Setup interval computation
  elif interval :
    var = interval
    if not model.Var(var) :
      print "ERROR : variable '%s' is not part of the model, cannot compute confidence interval" % var
      return None
    plrCalc = ROOT.Hfitter.HftPLRCalculator(model, var, fitopts);
    if no_exp:
      try:
        plrCalc.DeleteExpCalc()
      except:
        print 'WARNING: could not disable the expected PLR computation, ignoring --no-exp option'
    if hypo_vector != None :
      calc = ROOT.Hfitter.HftScanIntervalCalculator(plrCalc, var, hypo_vector)
    else :
      calc = ROOT.Hfitter.HftIterIntervalCalculator(plrCalc, var)
    if nsigmas : calc.SetZ(nsigmas)
    bands = bands if bands >= 0 else 1
  # We run out of computation types: fail
  else :
    print 'ERROR : must specify a computation using either the --fit, --pvalue, --significance, --limit or --interval options'
    return
  # setup scan
  if scan :
    scanVar = scan
    if not model.Var(scanVar) :
      print "ERROR : variable '%s' is not part of the model, cannot scan over it." % scanVar
      return None
    calc = ROOT.Hfitter.HftScanningCalculator(calc, scanVar, npoints, scan_min, scan_max)
  # sampling distributions
  if sampling_dists :
    for sd_spec in sampling_dists.split(',') :
      sd_spec_split = sd_spec.split(':')
      if len(sd_spec_split) >= 2 : 
        (calc_name, sampling_file) = sd_spec_split[:2]
      else :
        calc_name = ''
        sampling_file = sd_spec
      name_in_file = sd_spec_split[2] if len(sd_spec_split) > 2 else ''
      samp_calc = ROOT.Hfitter.HftAbsCalculator.GetCalc(calc_name)
      if not samp_calc : 
        print 'ERROR: could not set sampling distribution for unknown calculator %s' % calc_name
        return None
      if verbosity > 1 :
        print "INFO: setting sampling distribution of calculator '%s' to %s" % (calc_name, sampling_file)
      if not samp_calc.SetSamplingData(sampling_file, 'tree', name_in_file) :
        print 'ERROR: could not set sampling distribution from file %s for calculator %s' % (sampling_file, calc_name)
        return None
  if reset_state:
    try:
      calc.SetChainFitStates(False)
    except:
      print 'ERROR: could not set the model state to reset between fits -- probably not supported by calculator class'
      return None
  calc.SetVerbosity(verbosity)
  if name : calc.SetName(name)
  return calc

def suppress_output(verbosity) :  
  if verbosity <= 0 :
    ROOT.gEnv.SetValue("RooFit.Banner", "0", ROOT.kEnvChange, "int")
    ROOT.gErrorIgnoreLevel = ROOT.kWarning
    ROOT.RooMsgService.instance().setSilentMode(True)
    ROOT.RooMsgService.instance().setGlobalKillBelow(ROOT.RooFit.ERROR)

def load_libraries() :
  try:
    ROOTCORE_PATH = os.environ['ROOTCOREDIR']
  except KeyError:
    print >>sys.stderr, "ERROR : $ROOTCOREDIR environement variable is not set, please set up ROOTCORE before running"
    return
  ROOT.gROOT.ProcessLine('.x %s' % os.path.join(ROOTCORE_PATH, 'scripts', 'load_packages.C'))
  ROOT.gROOT.ProcessLine('using namespace Hfitter;')

def dump_json(options) :
  if options.json_input_file : return False # avoids writing json out when it was already read as input
  if options.json_output_file :
    json_output = options.json_output_file
  else :
    if options.output_file == '' : return
    json_output = os.path.splitext(options.output_file)[0] + '.json'
  with open(json_output, 'w') as f : f.write(json.dumps(vars(options), sort_keys=True, indent=2))

def parse(parser, args) :
  try:
    options = parser.parse_args(args)[0]
  except Exception as error :
    print 'ERROR: python exception while parsing arguments, exiting'
    print error
    return None
  if options.json_input_file != '' :
    try :
      with open(options.json_input_file, 'r') as f : data = json.loads(f.read())
    except :
      print 'ERROR : could not load arguments from json file %s' % options.json_input_file
      return None
    # json saves strings as unicode by default, which doesn't fit into the TString constructor. So convert back:
    for key in data :
      if isinstance(data[key], unicode) : data[key] = data[key].encode('ascii', 'replace')
    parser.set_defaults(**data)
    options = parser.parse_args(args)[0]
  if options.only_encode :
    del options.only_encode
    dump_json(options)
    return None
  return options

def parse_ghosts(model, ghost_spec) :
  tokens = ghost_spec.split(',')
  if len(tokens) != 4 and len(tokens) != 5 : 
    print "ERROR : invalid format for ghost event specification '%s'" % ghost_spec
    return None
  if len(tokens) == 4 : tokens += [ 1E-6 ]
  ghostVar = tokens[0]
  if not model.Observable(ghostVar) :
    print "ERROR : '%s' is not an observable of the model, cannot add ghosts" % ghostVar
    return None
  try :
    ghostMin    = float(tokens[1])
    ghostMax    = float(tokens[2])
    ghostStep   = float(tokens[3])
    ghostWeight = float(tokens[4])
  except:
    print "ERROR : invalid format for ghost event specification '%s'" % ghost_spec
    return None
  return (ghostVar, ghostMin, ghostMax, ghostStep, ghostWeight)
