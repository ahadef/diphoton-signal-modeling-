mkdir $1
cd $1
ln -s $ROOTCOREBIN/data/HfitterModels/HfitterLogon.C
ln -s $ROOTCOREBIN/data/HfitterModels/.rootrc
ln -s $ROOTCOREBIN/data/HfitterModels/datacards
ln -s $ROOTCOREBIN/data/HfitterModels/rootfiles
ln -s $ROOTCOREBIN/data/HfitterModels/examples
ln -s $ROOTCOREBIN/data/HfitterModels/unittests
ln -s $HFITTERDIR/HfitterModels/scripts/make_workspace
ln -s $HFITTERDIR/HfitterModels/scripts/compute.py
ln -s $HFITTERDIR/HfitterModels/scripts/generate_toys.py

