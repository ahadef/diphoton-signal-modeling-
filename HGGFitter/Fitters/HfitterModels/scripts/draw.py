#! /usr/bin/env python

__doc__ = "Display statistical results from the command line"
__author__ = "Nicolas Berger <Nicolas.Berger@cern.ch"

#import ROOT, HfitterModels, HfitterStats
import os, sys
from optparse import OptionParser

import ROOT
from Hfitter import *

#######################################################################################################################################

def draw(args):
  """compute"""

  parser = OptionParser("%prog [--interval|--limit|--pvalue] variable --model model [--datafile|--dataname] data [options]")
  parser.description = __doc__
  parser.add_option("-r", "--result",     default='',      help="Name of result to draw", type=str)
  parser.add_option(  "", "--variable",   default='',      help="Name of variable to draw", type=str)
  parser.add_option("-c", "--datacard",   default='',      help="Name of datacard file", type=str)
  parser.add_option("-w", "--ws-file",    default='',      help="Name of file containing the workspace", type=str)
  parser.add_option(  "", "--ws-name",    default='modelWS',    help="Name workspace object inside the specified file", type=str)
  parser.add_option(  "", "--model-config-name", default='mconfig', help="Name of model config within the specified workspace", type=str)
  parser.add_option("-m", "--model-name",  default='',     help="Name to assign to the model", type=str)
  parser.add_option("-i", "--input-file",  default='',     help="Name of input file(s)", type=str)
  parser.add_option("-s", "--scan",        default='',     help="Variable over which to scan", type=str)
  parser.add_option(  "", "--min",                         help="Position of the first scan point", type=float)
  parser.add_option(  "", "--max",                         help="Position of the last scan point", type=float)
  parser.add_option(  "", "--step",        default=1,      help="Scan step size", type=float)
  parser.add_option("-o", "--output-file", default='',     help="Name of output file", type=str)
  parser.add_option("-v", "--verbosity",   default=2,     help="Verbosity level", type=int)
  parser.add_option(  "", "--ymin",        default=0,      help="Lower range of plot Y range", type=float)
  parser.add_option(  "", "--ymax",        default=1,      help="Upper range of plot Y range", type=float)
  parser.add_option(  "", "--xsize",       default=800,    help="Canvas X size", type=float)
  parser.add_option(  "", "--ysize",       default=600,    help="Canvas Y size", type=float)
  parser.add_option(  "", "--logx",        action='store_true', help="Use log X scale")
  parser.add_option(  "", "--logy",        action='store_true', help="Use log Y scale")

  options = parse(parser, args)
  print 'opts = ', options
  if not options : return
  
  if options.output_file :
    if not options.scan :
      if os.path.exists(options.output_file) :
        print 'WARNING : output file %s already exists, will not overwrite' % options.output_file
        return
      
  suppress_output(options.verbosity)
  load_libraries()

  if options.result != None : 
    band = HftBand.LoadFromFiles(options.input_file, options.min, options.max, options.step);
    c1 = TCanvas('c1', '', options.xsize, options.ysize)
    if options.logx : c1.SetLogx()
    if options.logy : c1.SetLogy()
    graphs = band.Draw(yMin, yMax);
    output = TFile.Open(options.output_file, 'RECREATE')
    for i in range(0, band.NBands()) : graph[i].Write()

  if options.variable != None :
    model = build_model(options.datacard, options.model_name, options.ws_file, options.ws_name, options.model_config_name,
                        None, None, None, None, None, options.verbosity)
    n = (options.max - options.min)/options.step + 1
    val = HftScanningCalculator(HftFitValueCalculator(model, options.var, "", false), options.scan, n, options.min, options.max);
    val.SetIndexOffset(options.min);
    val.LoadFromFile(options.input_file);
    c1 = TCanvas('c1', '', options.xsize, options.ysize)
    if options.logx : c1.SetLogx()
    if options.logy : c1.SetLogy()
    curve = val.Curve()
    graphs = curve.Draw(yMin, yMax);
    output = TFile.Open(options.output_file, 'RECREATE')
    for i in range(0, curve.NBands()) : graph[i].Write()

# main
draw(sys.argv)
