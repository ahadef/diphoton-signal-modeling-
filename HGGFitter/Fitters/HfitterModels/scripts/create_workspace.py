#! /usr/bin/env python

__doc__ = "Build workspaces from the command line"
__author__ = "Nicolas Berger <Nicolas.Berger@cern.ch"

import os, sys
from optparse import OptionParser
import ROOT
from Hfitter import *

#######################################################################################################################################

def create_workspace(args):
  """compute"""
  
  parser = OptionParser("%prog --model model [--datafile|--dataname] data [options]")
  parser.description = __doc__
  parser.add_option("-c", "--datacard",   default='',      help="Name of datacard file", type=str)
  parser.add_option("-w", "--ws-file",    default='',      help="Name of file containing the workspace", type=str)
  parser.add_option(  "", "--ws-name",    default='modelWS',        help="Name workspace object inside the specified file", type=str)
  parser.add_option(  "", "--model-config-name", default='mconfig', help="Name of model config within the specified workspace", type=str)
  parser.add_option("-m", "--model-name", default='',      help="Name to assign to the model", type=str)
  parser.add_option("-x", "--execute",    default='',      help="Commands to run on model after instantiation", type=str)  
  parser.add_option("-r", "--setrange",   default='',      help="Set the range of a variable, overriding model state", type=str)  
  parser.add_option("-=", "--setval",     default='',      help="Set a variable value, overriding model state", type=str)  
  parser.add_option("-k", "--setconst",   default='',      help="Set a variable to be constant, overriding model state", type=str)  
  parser.add_option(  "", "--setfree",    default='',      help="Set a variable to be free, overriding model state", type=str)  
  parser.add_option(  "", "--category",   default='',      help="Specify that only a particular category should be included", type=str)  
  parser.add_option("-f", "--data-file",  default='',      help="Name of file containing the data", type=str)
  parser.add_option("-t", "--data-tree",  default='tree',  help="Name of TTree inside the specified data file", type=str)
  parser.add_option(  "", "--data-hist",  default='',      help="Name of histogram inside the specified data file", type=str)
  parser.add_option("-d", "--data-input-name",  default='obsData', help="Name of dataset object within the input workspace", type=str)
  parser.add_option(  "", "--data-output-name", default='obsData', help="Name of dataset object within the output workspace", type=str)
  parser.add_option("-a", "--asimov", action="store_true",   help="Use an Asimov dataset as the data")
  parser.add_option(  "", "--asimov-datacard", default='',   help="Datacard to use to generate Asimov dataset (if not specified, same as main)", type=str)
  parser.add_option(  "", "--best-fit", action="store_true", help="Fit the model to the data before saving the workspace")
  parser.add_option(  "", "--fit-options", default='',       help="Options to use when fitting the data", type=str)
  parser.add_option("-s", "--scan",       default='',        help="Variable over which to scan", type=str)
  parser.add_option("-n", "--npoints",                       help="Number of points to use for the scan", type=int)
  parser.add_option(  "", "--min",                           help="Position of the first scan point", type=float)
  parser.add_option(  "", "--max",                           help="Position of the last scan point", type=float)
  parser.add_option("-o", "--output-file", default='',       help="Name of output file", type=str)
  parser.add_option("-v", "--verbosity",   default=0,        help="Verbosity level", type=int)
  parser.add_option(      "--binned", action="store_true",   help="Use binned data")
  parser.add_option(  "", "--weight-var",  default='',       help="Name of TTree variable containing event weight", type=str)
  parser.add_option("-g", "--add-ghosts", default='',        help="Add ghost events to the data (format: var,min,max,step)", type=str)
  parser.add_option("-i", "--import-code", default='',       help="Directory from which to import class code", type=str)
  parser.add_option("-j", "--json-input-file",  default='',  help="Name of json file specifying argument values", type=str)
  parser.add_option(  "", "--json-output-file", default='',  help="Name of json file where to store argument values", type=str)
  parser.add_option("-e", "--only-encode", action='store_true',  help="Only encode the arguments into json format")
  
  options = parse(parser, args)
  if not options : return

  suppress_output(options.verbosity)
  load_libraries()
  
  import_dirs = []
  if options.import_code :
    for import_dir in options.import_code.split(':') :
      if not os.path.exists(import_dir) :
        import_dir = os.path.join(os.getenv('HFITTERDIR'), import_dir) 
        if not os.path.exists(import_dir) :
          print 'ERROR: could not locate directory %s for code import' % import_dir
          return
      import_dirs.append(import_dir)
  if import_dirs and options.verbosity >= 2 : print 'INFO: will import code from %s' % ' : '.join(import_dirs)
  # Create workspace
  if not options.scan :
    outsplit = os.path.splitext(options.output_file)
    outputfile = options.output_file if outsplit[1] == 'root' else '%s.root' % options.output_file
    if os.path.exists(outputfile) :
      print 'WARNING : output file %s already exists, will not overwrite' % outputfile
      return
    # Build the model
    model = build_model(options.datacard, options.model_name, options.ws_file, options.ws_name, options.model_config_name,
                        options.setval, options.setrange, options.setconst, options.setfree, options.category, options.verbosity)
    if not model :
      print 'ERROR : a model definition must be provided using either the --datacard or --ws-file options'
      return
    if options.execute : ROOT.gROOT.ProcessLine(options.execute)
    asimov_model = build_asimov_model(model, options.asimov, options.asimov_datacard, options.verbosity)
    data = open_data(model, options.data_file, options.data_tree, options.data_hist, options.data_input_name, asimov_model,
                     options.data_output_name,  options.weight_var, options.binned, options.add_ghosts, options.verbosity)
    if not data :
      print 'ERROR : a dataset must be specified using either the --data-file, --data-name or --asimov options'
      return
    if options.best_fit : model.Fit(data, ROOT.Hfitter.Options(options.fit_options))
    print 'INFO : writing workpace %s' % options.output_file
    model.ExportWorkspace(options.output_file, options.ws_name, "", data, options.model_config_name, ':'.join(import_dirs))
  else :
    scanVar = options.scan
    for i in range(0, options.npoints) :
      val = options.min + i*(options.max - options.min)/(options.npoints -1)
      outsplit = os.path.splitext(options.output_file)
      outputfile = options.output_file if outsplit[1] == 'root' else '%s_%g.root' % (options.output_file, val)
      if os.path.exists(outputfile) :
        print 'WARNING : output file %s already exists, will not overwrite' % outputfile
        continue
      model = build_model(options.datacard, options.model_name, options.ws_file, options.ws_name, options.model_config_name,
                          options.setval, options.setrange, options.setconst, options.setfree, options.category, options.verbosity)
      if not model :
        print 'ERROR : a model definition must be provided using either the --datacard or --ws-file options'
        return
      if not model.Var(scanVar) :
        print "ERROR : variable '%s' is not part of the model, cannot scan over it." % scanVar
        return
      model.Var(scanVar).setVal(val)
      if options.execute : 
        if options.verbosity > 1 : print 'INFO: executing command %s' % options.execute
        ROOT.gROOT.ProcessLine(options.execute)
        if options.verbosity > 1 : print 'INFO: executed command successfully'
      asimov_model = build_asimov_model(model, options.asimov, options.asimov_datacard, options.verbosity)
      data = open_data(model, options.data_file, options.data_tree, options.data_hist, options.data_input_name, asimov_model, options.data_output_name, options.weight_var, options.binned, options.add_ghosts, options.verbosity)
      if options.best_fit : model.Fit(data, ROOT.Hfitter.Options(options.fit_options))
      print 'INFO : writing workpace %s' % outputfile
      model.ExportWorkspace(outputfile, options.ws_name, "", data, options.model_config_name, ':'.join(import_dirs))
      #ROOT.gROOT.ProcessLine('delete (HftModel*)%d' % ROOT.AddressOf(model)[0])
      ROOT.gROOT.ProcessLine('delete (RooAbsData*)%d' % ROOT.AddressOf(data)[0])
      #model.PrintModels()
      ROOT.gROOT.Reset()
  dump_json(options)
###############################

# main
create_workspace(sys.argv)
