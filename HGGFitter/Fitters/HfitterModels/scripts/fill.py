#! /usr/bin/env python

import sys

def fill(base, val) :
  replist = tuple(val for i in range(0, base.count('%')))
  return base.replace('%', '%g') % replist

def iterfill(base, minVal, maxVal, step) :
  n = int((maxVal - minVal)/step) + 1
  delta = (maxVal - minVal)/(n - 1)
  return '\n'.join([fill(base, minVal + i*delta) for i in range(0, n)])

def usage() :
  return 'Usage : fill <expression with % placeholders> <min> <max> [step]'

if len(sys.argv) < 2 : 
  print usage()
elif len(sys.argv) == 3 :
  try :
    val = float(sys.argv[2])
    print fill(sys.argv[1], val)
  except:
    print usage()
elif len(sys.argv) == 4 :
  try :
    minVal = float(sys.argv[2])
    maxVal = float(sys.argv[3])
    print iterfill(sys.argv[1], minVal, maxVal, 1)
  except:
    print usage()
else :
  try :
    minVal = float(sys.argv[2])
    maxVal = float(sys.argv[3])
    step   = float(sys.argv[4])
    print iterfill(sys.argv[1], minVal, maxVal, step)
  except:
    print usage()


