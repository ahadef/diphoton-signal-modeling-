#! /bin/zsh

root -b -q unittests/test_fitting1.C >! log
root -b -q unittests/test_fitting2.C >> log
root -b -q unittests/test_fitting3.C >> log
root -b -q unittests/test_fitting4.C >> log
root -b -q unittests/test_fitting5.C >> log
root -b -q unittests/test_intervals1.C >> log
root -b -q unittests/test_pvalues1.C >> log
root -b -q unittests/test_pvalues2.C >> log
root -b -q unittests/test_pvalues3.C >> log
root -b -q unittests/test_pvalues4.C >> log
root -b -q unittests/test_pvalues5.C >> log
root -b -q unittests/test_pvalues6.C >> log
root -b -q unittests/test_pvalues7.C >> log
root -b -q unittests/test_pvalues8.C >> log
root -b -q unittests/test_pvalues9.C >> log
root -b -q unittests/test_limits1.C >> log
root -b -q unittests/test_limits2.C >> log
root -b -q unittests/test_limits3.C >> log
root -b -q unittests/test_limits4.C >> log
root -b -q unittests/test_limits5.C >> log

ff=unittest_`date -Isec`
echo $ff

grep HFTEST log >! log_${ff}_hftest
grep HFTIME log >! log_${ff}_hftime

diff -y log_${ff}_hftest unittests/ref_hftest > diff_${ff}_hftest
echo ///// UNIT TEST RESULT
cat diff_${ff}_hftest

rm log
rm signal_fit.dat
rm fitValueTest.root
rm sm_toys.root
rm p0_scan_toys_*.root
rm sampling_toys_nullHypo.root
rm sampling_toys_limit_*.root
