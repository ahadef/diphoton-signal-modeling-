{
  TStopwatch sw; sw.Start();
  hgg = HftModelBuilder::Create("hgg", "datacards/hgg_simple.dat"); //define the model
  data = hgg->GenerateEvents(); // generate a toy dataset, use it as "data"
  HftScanningCalculator scanCalc(HftUncappedPValueCalculator(*hgg, "mu"), "mHiggs", 9, 110, 140);
  scanCalc.LoadInputs(*data); // specify the dataset as the inputs to the calculator (performs the fit at each point)
  cout << "HFTEST pvalues2   - 1 : " << scanCalc.Calculator(2)->Result() << endl;
  cout << "HFTEST pvalues2   - 2 : " << scanCalc.Calculator(5)->Result() << endl;
  cout << "HFTEST pvalues2   - 3 : " << scanCalc.Calculator(8)->Result() << endl;
  sw.Stop(); cout << "HFTIME pvalues2   : Wall Time " << sw.RealTime() << ", CPU Time " << sw.CpuTime() << endl;
}
