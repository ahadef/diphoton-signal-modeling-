/////////////////////////////////////////////////////////////////////////////////////////////////
// A datacard defining a simple model with 2 components, inspired by the H->gamma gamma search:
// - a signal component with a Gaussian PDF, with mean mHiggs=125 GeV and sigma=1.5 GeV
// - a background compoennt with an exponential PDF
// The signal normalization is described as nSignal = mu*xsec_hgg_SM*lumi, where 
// lumi=20/fb is the dataset integrated lumi, xsec_hgg_SM=50fb is the SM H->gamgam cross-section,
//  and mu is a signal strength factor
// The free parameters are : mu, the number of background events nBackground, and the exponential slope xi.
/////////////////////////////////////////////////////////////////////////////////////////////////

observable mgg = 125 min=100 max=160 bins=480 unit=GeV title="m_{#gamma#gamma}"

//------------------------------
[Models]
//------------------------------
component Signal = RooGaussian(mgg, "mHiggs", "sigma")
component Background = PDF("mgg", "exp(xi*mgg)", "xi")

// Signal
formula nSignal = (mu * nSM)
nSM = 400
mu = 1 min=-1 max=1000
nBackground = 100000 min=0 max=1000000
mHiggs = 125 const=true min=110 max=140 unit=GeV
sigma = 1.5 const=true min=1   max=2   unit=GeV

//Background
xi = -0.02 min=-0.1 max=0 unit=GeV^{-1}
