/////////////////////////////////////////////////////////////////////////////////////////////////
// A datacard defining a simple model with 2 components, inspired by the H->gamma gamma search,
// and comprising 3 categories describing 3 subsets of the data. The components are
// - a signal component with a Gaussian PDF in categories cat1 and cat2, and a bifurcated Gaussian for category catX
// - a background compoennt with an exponential PDF in all 3 categories
// The signal and background parameters can be given separately for each category. Here the signal peak
// positions are common, but the widths are different.
// The signal and background normalizations are also specified separately for each category.
/////////////////////////////////////////////////////////////////////////////////////////////////

//------------------------------
[Dependents]
//------------------------------
observable mgg = 125 L(100 - 160) B(120) // unit=GeV gamma-gamma invariant mass

//------------------------------
[Categories]
//------------------------------
category cat = (cat1, cat2, catX)

//------------------------------
[Models]
//------------------------------
component Signal = RooGaussian  ("mgg", "mHiggs", "sigmaG")           for cat = ( cat1, cat2 ),
                   RooBifurGauss("mgg", "mHiggs", "sigmaL", "sigmaR") for cat = ( catX )
component Background = PDF("mgg", "exp(xi*mgg)", "xi") for cat = ( cat1, cat2, catX )

//------------------------------
[Parameters]
//------------------------------
split nSignal, nBackground along cat

formula nSignal_i (i in cat) = (mu * nSM_i)
nSM_cat1 = 150
nSM_cat2 = 50
nSM_catX = 200
mu = 1 L(-1 - 100)

nBackground_cat1 = 20000 L(0 - 1000000)
nBackground_cat2 = 10000 L(0 - 1000000)
nBackground_catX = 70000 L(0 - 1000000)

//------------------------------
[Signal]
//------------------------------
mHiggs =  125 C L(110 - 140) // unit=GeV
sigmaG = 1.5  // unit=GeV
sigmaL = 2    // unit=GeV
sigmaR = 1.5  // unit=GeV

//------------------------------
[Background]
//------------------------------
split xi along cat
xi_cat1 = -0.02 L(-0.1 - 0) // unit=GeV^{-1}
xi_cat2 = -0.02 L(-0.1 - 0) // unit=GeV^{-1}
xi_catX = -0.01 L(-0.1 - 0) // unit=GeV^{-1}
