{
  hgg = HftModelBuilder::Create("hgg", "datacards/hgg_simple.dat"); //define the model
  data = hgg->GenerateEvents(); // generate a toy dataset, use it as "data"

  // define the calculator: the first argument is the calculator giving the result at each point (here an HftFitValueCalculator as before),
  // and the following arguments give the parameter over which to scan, the number of points and the scan range
  HftScanningCalculator scanCalc(HftFitValueCalculator(*hgg, "mu"), "mHiggs", 31, 110, 140);

  scanCalc.LoadInputs(*data); // specify the dataset as the inputs to the calculator (performs the fit at each point)
  cout << scanCalc.Position(10) << " : " << scanCalc.Calculator(10)->Result() << endl; // show result for point 10 (mHiggs=120 GeV)
  scanCalc.Curve(1).Draw(-1, 2); // draw the curve with +/- 1 sigma error band 
}