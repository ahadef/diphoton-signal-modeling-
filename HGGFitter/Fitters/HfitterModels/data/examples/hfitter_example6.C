{
  hgg = HftModelBuilder::Create("hgg", "datacards/hgg_simple.dat"); //define the model
  initialState = hgg->CurrentState();
  data = hgg->GenerateEvents(); // generate a toy dataset

  HftUncappedPValueCalculator p0Calc(*hgg, "mu"); // "mu" is the "signal intensity parameter" used to vary the amount of signal
  p0Calc.LoadInputs(*data); // set the dataset as the input 
  cout << p0Calc.Result() << " " << p0Calc.Significance() << " " << p0Calc.Statistic() << endl; // show result (p-value), along with significance and statistic values 

  hgg->LoadState(initialState); // reload initial state
  hgg->Var("mu")->setVal(0.5); // put less signal
  data2 = hgg->GenerateEvents(); // generate another toy dataset
  p0Calc.LoadInputs(*data2); // set the new dataset as imput
  cout << p0Calc.Result() << " " << p0Calc.Significance() << " " << p0Calc.Statistic() << endl; // show new results
}
