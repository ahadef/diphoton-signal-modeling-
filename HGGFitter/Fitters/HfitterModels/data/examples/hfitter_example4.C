{
  hgg = HftModelBuilder::Create("hgg", "datacards/hgg_simple.dat"); //define the model
  data = hgg->GenerateEvents(); // generate a toy dataset, use it as "data"
  HftFitValueCalculator calc(*hgg, "mu"); // define the calculator, with "mu" as the model parameter to study
  calc.LoadInputs(*data); // load dataset data into the calculator (here performs the fit)
  cout << calc.Result() << endl; // print the result (here the fitted value of "mu")
  
  valAndErr = HftInterval::Make(1); // define an error range object with a +/- 1 sigma error range
  calc.GetResult(valAndErr);
  valAndErr.Print();

  calc.SaveToFile("fitValueTest.root"); // save calculator data to a root file
  HftFitValueCalculator calc2(*hgg, "mu"); // start over with a new calculator
  calc2.LoadFromFile("fitValueTest.root");  // load data directly from the file
  cout << calc2.Result() << endl; // get the result again
}
