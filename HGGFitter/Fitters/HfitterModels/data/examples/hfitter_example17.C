{
  hgg = HftModelBuilder::Create("hgg", "datacards/hgg_simple.dat"); //define the model
  hgg->Var("mu")->setVal(0); 
  noSignalState = hgg->CurrentState();
  hgg->Var("mu")->setVal(1); 
  HftAsimovCalculator scanCalc(HftScanLimitCalculator(HftCLsCalculator(HftQTildaLimitCalculator(*hgg, "mu")), 31, 0.1, 1.6), noSignalState); 
  HftAsimovCalculator iterCalc(HftIterLimitCalculator(HftCLsCalculator(HftQTildaLimitCalculator(*hgg, "mu")), 2), noSignalState); 
  scanCalc.LoadAsimov(); // use the Asimov dataset as input data
  iterCalc.LoadAsimov(); // use the Asimov dataset as input data
  scanCalc.Result(2).Print(); // show result , i.e. mu value corresponding to 95% exclusion, with +/- 1,2 sigma ranges
  iterCalc.Result(2).Print(); // show result , i.e. mu value corresponding to 95% exclusion, with +/- 1,2 sigma ranges
  ((HftScanLimitCalculator*)scanCalc.Calculator())->Calculator()->CLCurve(2).Draw(0.5, 1); // draw the CL curve
}
