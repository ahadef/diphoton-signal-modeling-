#include "HfitterModels/Options.h"

#include "HfitterModels/HftModel.h"

#include "RooRealVar.h"
#include "TString.h"
#include "TRegexp.h"
#include "TStyle.h"
#include "TROOT.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;

TString Options::defaults = "";


void Options::AddDefaults()
{
  if (defaults != "") Parse("NoDefaults" + defaults); // 0 to not add defaults recursively...
  Add(RooFitOpt(RooFit::Minos(false)));
  Add(RooFitOpt(RooFit::Hesse(false)));
  Add(RooFitOpt(RooFit::Save(true)));
  Add(RooFitOpt(RooFit::Extended(true)));
  Add(RooFitOpt(RooFit::SumW2Error(false)));
  Add(RooFitOpt(RooFit::Minimizer("Minuit2", "migrad")));
}


Options::Options(const char* opt)
{
  Parse(opt);
}


Options::Options(const TString& opt)
{
  Parse(opt);
}


Options::Options(const Options& other)
{
  for (std::vector<HftAbsOption*>::const_iterator opt = other.m_options.begin(); opt != other.m_options.end(); opt++)
    Add(**opt);
}


Options::~Options()
{
  for (std::vector<HftAbsOption*>::iterator opt = m_options.begin(); opt != m_options.end(); opt++)
    delete *opt;
}

void Options::Add(const HftAbsOption& opt)
{
  m_options.push_back(opt.Clone());
  //if (Verbosity()) cout << "--> using option " << opt.Str() << endl;
}

bool Options::Remove(const TString& name)
{
  bool found = false;
  std::vector<HftAbsOption*> options;
  for (std::vector<HftAbsOption*>::const_iterator opt = m_options.begin(); opt != m_options.end(); opt++)
    if ((*opt)->Name().Index(TRegexp(name)) >= 0) {
      found  = true;
      delete *opt;
    }
    else
      options.push_back(*opt);
  if (found) m_options = options;
  return found;
}


void Options::Parse(const TString& opt)
{
  if (opt.Index("Q") >= 0) {
    Add(RooFitOpt(RooFit::Verbose(false)));
    Add(RooFitOpt(RooFit::PrintLevel(-3)));
    Add(StrArgOpt("Verbose", "0"));
  }
  else if (opt.Index("VV") >= 0) {
    Add(RooFitOpt(RooFit::Verbose(true)));
    Add(StrArgOpt("Verbose", "2"));
  }
  StrOpt strOpt("dummy");
  if (!StrOpt("NoDefaults").IsIn(opt)) AddDefaults();
  if (StrOpt("SumW2ErrorTrue").IsIn(opt) || StrOpt("W+").IsIn(opt)) Add(RooFitOpt(RooFit::SumW2Error(true)));
  if (StrOpt("SumW2ErrorFalse").IsIn(opt) || StrOpt("W-").IsIn(opt)) Add(RooFitOpt(RooFit::SumW2Error(false)));
//  if (StrOpt("MinosAll").IsIn(opt)) Add(RooFitOpt(RooFit::Minos(true)));
  if (StrOpt("MinosAll").IsIn(opt)) Add(MinosVarOpt());
  if (StrOpt("Hesse").IsIn(opt)) Add(RooFitOpt(RooFit::Hesse(true)));
  if (StrOpt("Ext").IsIn(opt))   Add(RooFitOpt(RooFit::Extended(true)));
  if (StrOpt("Old").IsIn(opt))     { Remove("Minimizer"); Add(RooFitOpt(RooFit::Minimizer("Minuit", "migrad"))); }
  if (StrOpt("Scan").IsIn(opt))    Add(RooFitOpt(RooFit::Minimizer("Minuit2", "Scan")));
  if (StrOpt("Simplex").IsIn(opt)) Add(RooFitOpt(RooFit::Minimizer("Minuit2", "Simplex")));
  if (StrOpt("Offset").IsIn(opt))    Add(RooFitOpt(RooFit::Offset(true)));
  if (StrOpt("Robust").IsIn(opt))    Add(StrOpt("Robust"));
  if (StrOpt("Offcheck").IsIn(opt, strOpt))    Add(strOpt);
  if (StrOpt("NotPosDefOK").IsIn(opt, strOpt)) Add(strOpt);
  if (StrOpt("Binned").IsIn(opt, strOpt))      Add(strOpt);
  if (StrOpt("Chi2").IsIn(opt, strOpt))        Add(strOpt);
  
  StrArgOpt argOpt("dummy");
  if (StrArgOpt("Strategy").IsIn(opt, argOpt, 1, true)) Add(RooFitOpt(RooFit::Strategy(argOpt.Arg(0).Atoi())));
  if (StrArgOpt("NCPU").IsIn(opt, argOpt, 1, true)) Add(RooFitOpt(RooFit::NumCPU(argOpt.Arg(0).Atoi())));
  if (StrArgOpt("Component").IsIn(opt, argOpt)) Add(argOpt);
  if (StrArgOpt("FittedDofs").IsIn(opt, argOpt, 1, true)) Add(argOpt);
  if (StrArgOpt("LegendPos").IsIn(opt, argOpt, 1, true)) Add(argOpt);
  if (StrArgOpt("Scan").IsIn(opt, argOpt)) Add(argOpt);
  if (StrArgOpt("Verbose").IsIn(opt, argOpt, 1, true)) Add(argOpt);
  if (StrArgOpt("LineColor").IsIn(opt, argOpt, 1, true)) Add(argOpt);
  if (StrArgOpt("Precision").IsIn(opt, argOpt, 1)) Add(argOpt);
  if (StrArgOpt("DataScale").IsIn(opt, argOpt, 1)) Add(argOpt);
  if (StrArgOpt("StatBand").IsIn(opt, argOpt)) Add(argOpt);
 
  MinosVarOpt minosVarOpt;
  if (MinosVarOpt().IsIn(opt, minosVarOpt, 1)) Add(minosVarOpt);
  RangeOpt rangeOpt;
  if (RangeOpt().IsIn(opt, rangeOpt, 1)) Add(rangeOpt);
  PlotErrorsOpt peOpt;
  if (PlotErrorsOpt().IsIn(opt, peOpt)) Add(peOpt);
  PlotScaleOpt psOpt;
  if (PlotScaleOpt().IsIn(opt, psOpt)) Add(psOpt);
}


bool Options::Process(RooLinkedList& cmdList, HftModel& model) const
{
  // Defaults
  for (std::vector<HftAbsOption*>::const_iterator opt = m_options.begin(); opt != m_options.end(); opt++)
    if (!(*opt)->Process(cmdList, model)) return false;
  return true;
}


TString Options::Str() const
{
  TString allOpts = "";
  for (std::vector<HftAbsOption*>::const_iterator opt = m_options.begin(); opt != m_options.end(); opt++)
    allOpts += "\n" + (*opt)->Str();
  return allOpts;
}


HftAbsOption* Options::Find(const TString& name) const
{
  for (std::vector<HftAbsOption*>::const_iterator opt = m_options.begin(); opt != m_options.end(); opt++)
    if ((*opt)->Name() == name) return *opt;
  return 0;
}


bool Options::GetStr(const TString& name, TString& arg) const
{
  StrArgOpt* argOpt = dynamic_cast<StrArgOpt*>(Find(name));
  if (!argOpt || argOpt->NArgs() == 0) return false;
  arg = argOpt->Arg(0);
  return true;
}


bool Options::GetInt(const TString& name, int& arg) const
{
  TString strArg;
  if (!GetStr(name, strArg) || !strArg.IsDigit()) return false;
  arg = strArg.Atoi();
  return true;
}


bool MinosVarOpt::Process(RooLinkedList& cmdList, HftModel& model) const
{
  RooCmdArg* arg = 0;
  arg = (RooCmdArg*)cmdList.FindObject("Minos"); if (arg && arg->getInt(0) == 0) cmdList.Remove(arg); // remove the "no minos" argument
  arg = (RooCmdArg*)cmdList.FindObject("Hesse"); if (arg && arg->getInt(0) == 0) cmdList.Remove(arg); // remove the "no Hesse" argument
  if (NArgs() == 0) {
    cout << "General Minos!" << endl;
    cmdList.Add(new RooCmdArg(RooFit::Minos(true)));
    return true;
  }
  RooArgSet* varSet = new RooArgSet();
  for (std::vector<TString>::const_iterator var = m_args.begin(); var != m_args.end(); var++) {
    RooRealVar* v = model.Var(*var);
    if (!v) {
      cout << "ERROR: MINOS errors requested for non-existent variable " << *var << endl;
      return false;
    }
    varSet->add(*v);
  }
  cmdList.Add(new RooCmdArg(RooFit::Minos(*varSet)));
  return true;
}


bool NoDefaults::Process(RooLinkedList& cmdList, HftModel& /*model*/) const
{
  RooCmdArg* arg = 0;
  arg = (RooCmdArg*)cmdList.FindObject("Save");     if (arg) cmdList.Remove(arg);
  arg = (RooCmdArg*)cmdList.FindObject("Extended"); if (arg) cmdList.Remove(arg);
  arg = (RooCmdArg*)cmdList.FindObject("SumW2Error"); if (arg) cmdList.Remove(arg);
  return true;
}


bool StrOpt::IsIn(const TString& opt, StrOpt& found) const
{
  if (opt.Index(m_name) >= 0) {
    found = *this;
    return true;
  }
  return false;
}


StrArgOpt::StrArgOpt(const TString& name, const TString& arg1, const TString& arg2, const TString& arg3) 
 : m_name(name)
{
  if (arg1 != "") m_args.push_back(arg1);
  if (arg2 != "") m_args.push_back(arg2);
  if (arg3 != "") m_args.push_back(arg3);
}


bool StrArgOpt::IsIn(const TString& opt, StrArgOpt& found, unsigned minNArgs, bool reqInt) const
{
  TObjArray* tokens1 = opt.Tokenize("|:;");
  for (int k = 0; k < tokens1->GetEntries(); k++) {
    TString item = tokens1->At(k)->GetName();
    TObjArray* tokens2 = item.Tokenize("()");
    TString name = "";
    if (tokens2->GetEntries() == 0) { delete tokens2; continue; }
    if (TString(tokens2->At(0)->GetName()).Strip(TString::kBoth, ' ') != m_name) { delete tokens2; continue; }
    std::vector<TString> args;
    if (tokens2->GetEntries() >= 2) {
      TString argStr = TString(tokens2->At(1)->GetName()).Strip(TString::kBoth, ' ');
      TObjArray* tokens3 = argStr.Tokenize(",");
      for (int i = 0; i < tokens3->GetEntries(); i++)
        args.push_back(TString(tokens3->At(i)->GetName()).Strip(TString::kBoth, ' '));
      delete tokens3;
    }
    delete tokens2;
    if (args.size() < minNArgs) { delete tokens1; return false; }
    if (reqInt && args.size() >= 1 && !args[0].IsDigit()) { delete tokens1; return false; }
    found = StrArgOpt(m_name, args);
    delete tokens1; 
    return true;
  }
  delete tokens1;
  return false;
}


TString StrArgOpt::ArgStr() const
{
  TString argStr = "";
  for (std::vector<TString>::const_iterator arg = m_args.begin(); arg != m_args.end(); arg++) {
    if (argStr != "") argStr += ",";
        argStr += *arg;
  }
  return argStr;
}


bool RangeOpt::Process(RooLinkedList& cmdList, HftModel& /*model*/) const
{
  TString allRanges = "";
  for (std::vector<TString>::const_iterator range = m_args.begin(); range != m_args.end(); range++) {
    if (allRanges != "") allRanges += ",";
        allRanges += *range;
  }
  cmdList.Add(new RooCmdArg(RooFit::Range(allRanges)));
  return true;
}


bool PlotErrorsOpt::IsIn(const TString& opt, PlotErrorsOpt& found) const
{
  StrArgOpt argOpt(Name());
  if (!StrArgOpt(Name()).IsIn(opt, argOpt, 1)) return false;
  if (argOpt.Arg(0) == "Poisson")  { found = PlotErrorsOpt(RooAbsData::Poisson); return true; }
  if (argOpt.Arg(0) == "SumW2")    { found = PlotErrorsOpt(RooAbsData::SumW2); return true; }
  if (argOpt.Arg(0) == "None")     { found = PlotErrorsOpt(RooAbsData::None); return true; }
  if (argOpt.Arg(0) == "Auto")     { found = PlotErrorsOpt(RooAbsData::Auto); return true; }
  if (argOpt.Arg(0) == "Expected") { found = PlotErrorsOpt(RooAbsData::Expected); return true; }
  return false;
}


TString PlotErrorsOpt::Str() const 
{
  TString argStr;
  switch (m_errorType) {
    case RooAbsData::Poisson  : argStr = "Poisson"; break;
    case RooAbsData::SumW2    : argStr = "SumW2"; break;
    case RooAbsData::None     : argStr = "None"; break;
    case RooAbsData::Auto     : argStr = "Auto"; break;
    case RooAbsData::Expected : argStr = "Expected"; break;
  }
  return Name() + "(" + argStr + ")";
}  


bool PlotScaleOpt::IsIn(const TString& opt, PlotScaleOpt& found) const
{
  StrArgOpt argOpt(Name());
  if (!StrArgOpt(Name()).IsIn(opt, argOpt, 1)) return false;
  double scale = (argOpt.NArgs() == 2 ? argOpt.Arg(1).Atof() : -1);
  if (argOpt.Arg(0) == "Raw")              { found = PlotScaleOpt(RooAbsReal::Raw, scale); return true; }
  if (argOpt.Arg(0) == "Relative")         { found = PlotScaleOpt(RooAbsReal::Relative, scale); return true; }
  if (argOpt.Arg(0) == "RelativeExpected") { found = PlotScaleOpt(RooAbsReal::RelativeExpected, scale); return true; }
  if (argOpt.Arg(0) == "NumEvent")         { found = PlotScaleOpt(RooAbsReal::NumEvent, scale); return true; }
  cout << "ERROR: invalid scale specification " << argOpt.Str() << endl;
  return false;    
}


TString PlotScaleOpt::Str() const 
{
  TString argStr;
  switch (m_scaleType) {
    case RooAbsReal::Raw              : argStr = "Raw"; break;
    case RooAbsReal::Relative         : argStr = "Relative"; break;
    case RooAbsReal::RelativeExpected : argStr = "RelativeExpected"; break;
    case RooAbsReal::NumEvent         : argStr = "NumEvent"; break;
  }
  return Name() + "(" + argStr + ", " + Form("%g", m_scale) + ")";
}  

void Options::SetHfitterStyle()
{
  // Based on ATLAS Style, itself based on a style file from BaBar

  static TStyle* hfitterStyle = new TStyle("Hfitter", "Default style for Hfitter plots");

  // use plain black on white colors
  hfitterStyle->SetFrameBorderMode(kWhite);
  hfitterStyle->SetFrameFillColor(kWhite);
  hfitterStyle->SetCanvasBorderMode(kWhite);
  hfitterStyle->SetCanvasColor(kWhite);
  hfitterStyle->SetPadBorderMode(kWhite);
  hfitterStyle->SetPadColor(kWhite);
  hfitterStyle->SetStatColor(kWhite);

  // set the paper & margin sizes
  hfitterStyle->SetPaperSize(20,26);

  // set margin sizes
  hfitterStyle->SetPadTopMargin(0.05);
  hfitterStyle->SetPadRightMargin(0.05);
  hfitterStyle->SetPadBottomMargin(0.16);
  hfitterStyle->SetPadLeftMargin(0.16);

  // set title offsets (for axis label)
  hfitterStyle->SetTitleXOffset(1.4);
  hfitterStyle->SetTitleYOffset(1.4);

  // use large fonts
  //Int_t font=72; // Helvetica italics
  Int_t font=42; // Helvetica
  Double_t tsize=0.05;
  hfitterStyle->SetTextFont(font);

  hfitterStyle->SetTextSize(tsize);
  hfitterStyle->SetLabelFont(font, "x");
  hfitterStyle->SetTitleFont(font, "x");
  hfitterStyle->SetLabelFont(font, "y");
  hfitterStyle->SetTitleFont(font, "y");
  hfitterStyle->SetLabelFont(font, "z");
  hfitterStyle->SetTitleFont(font, "z");
  
  hfitterStyle->SetLabelSize(tsize, "x");
  hfitterStyle->SetTitleSize(tsize, "x");
  hfitterStyle->SetLabelSize(tsize, "y");
  hfitterStyle->SetTitleSize(tsize, "y");
  hfitterStyle->SetLabelSize(tsize, "z");
  hfitterStyle->SetTitleSize(tsize, "z");

  // use bold lines and markers
  hfitterStyle->SetMarkerStyle(20);
  hfitterStyle->SetMarkerSize(1.2);
  hfitterStyle->SetHistLineWidth(2);
  hfitterStyle->SetLineStyleString(2,"[12 12]"); // postscript dashes

  // get rid of X error bars 
  //hfitterStyle->SetErrorX(0.001);
  // get rid of error bar caps
  hfitterStyle->SetEndErrorSize(0);

  // do not display any of the standard histogram decorations
  hfitterStyle->SetOptTitle(0);
  hfitterStyle->SetOptStat(0);
  hfitterStyle->SetOptFit(0);

  // put tick marks on top and RHS of plots
  hfitterStyle->SetPadTickX(1);
  hfitterStyle->SetPadTickY(1);

  gROOT->SetStyle("Hfitter");
  gROOT->ForceStyle();
}
