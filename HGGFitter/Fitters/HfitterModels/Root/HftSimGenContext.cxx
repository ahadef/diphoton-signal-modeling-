#include "HfitterModels/HftSimGenContext.h"
#include "RooAbsCategoryLValue.h"
#include "RooDataSet.h"
#include "RooSimultaneous.h"

#include <iostream>
#include <map>
#include <string>

using namespace RooFit;

using std::cout;
using std::endl;
using std::map;
using std::string;

void HftSimGenContext::dump()
{
  _idxCatSet->Print("v");
  _idxCat->Print();
  _idxCat->Print("v");
  cout << _idxCatName << endl;
}


//_____________________________________________________________________________
RooDataSet* HftSimGenContext::createDataSet(const char* name, const char* title, const RooArgSet& obs)
{
  // Create an empty dataset to hold the events that will be generated

  cout << "Test!!" << endl;
  
  // If the observables do not contain the index, make a plain dataset
  if (!obs.contains(*_idxCat)) {
    return new RooDataSet(name,title,obs) ;
  }

  map<string,RooAbsData*> dmap ;
  RooCatType* state ;
  TIterator* iter = _idxCat->typeIterator() ;
  while((state=(RooCatType*)iter->Next())) {
    RooAbsPdf* slicePdf = _pdf->getPdf(state->GetName()) ;
    RooArgSet* sliceObs = slicePdf->getObservables(obs) ;
    std::string sliceName = Form("%s_slice_%s",name,state->GetName()) ;
    std::string sliceTitle = Form("%s (index slice %s)",title,state->GetName()) ;
    RooDataSet* dset = new RooDataSet(sliceName.c_str(),sliceTitle.c_str(),*sliceObs) ;
    dmap[state->GetName()] = dset ;
    delete sliceObs ;
  }
  delete iter ;

  _idxCat->Print();
  obs.Print();
  
  RooDataSet* ret = new RooDataSet(name, title, obs, Index((RooCategory&)*_idxCat), 
                                   Link(dmap), OwnLinked()) ;

  return ret ;
}