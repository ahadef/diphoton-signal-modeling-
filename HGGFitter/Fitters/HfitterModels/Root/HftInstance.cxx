#include "HfitterModels/HftInstance.h"

#include "HfitterModels/HftGenericRealBuilder.h"
#include "TClass.h"
#include "TMethod.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;


HftInstance* HftInstance::Create(const TString& className, const TString& args, bool verbose)
{
  TClass* instanceClass = TClass::GetClass(className);
  if (!instanceClass) {
    cout << "ERROR : instance class " << className << " not recognized, exiting." << endl;
    return 0;
  }
  HftInstance* instance = dynamic_cast<HftInstance*>((TObject*)instanceClass->New());
  if (!instance) {
    cout << "ERROR : instance class " << className << " does not derive from HftInstance, exiting." << endl;
    return 0;
  }
  if (verbose) cout << "Creating instance of class " << className << " and executing Setup(" << args << ")." << endl;
  TMethod* meth = instanceClass->GetMethod("Setup", args);
  if (!meth) {
    cout << "WARNING: HftInstanceSpec::CreateInstance : instance class "  << className
         << " does not have a Setup method for arguments : " << args << ", will try anyway but may crash..." << endl;
  }
  instance->Execute("Setup", args);
  return instance;
}


