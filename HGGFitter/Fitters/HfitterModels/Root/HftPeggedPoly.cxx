// $Id: HftPeggedPoly.cxx,v 1.3 2008/05/28 15:32:52 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include <vector>

#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "TMath.h"

#include "HfitterModels/HftPeggedPoly.h"


Hfitter::HftPeggedPoly::HftPeggedPoly( const HftPeggedPoly& other, const char* name )
   : RooAbsPdf( other, name ),
     m_x    ( "x",            this, other.m_x ),
     m_coefs( "coefficients", this, other.m_coefs ),
     m_power( "power",        this, other.m_power ),
     m_ped  ( "pedestal",     this, other.m_ped ),
     m_xMin ( other.m_xMin ),
     m_xMax ( other.m_xMax )
{}

     Hfitter::HftPeggedPoly::HftPeggedPoly( const char* name, const char* title, 
                                            RooAbsReal& x, const RooArgList& coefList,
                                            RooAbsReal& power, RooAbsReal& pedestal, Double_t xMin, Double_t xMax )
   : RooAbsPdf( name, title ),
     m_x    ( "x", "Dependent", this, x ),
     m_coefs( "coefficients", "", this ),
     m_power( "power", "Power", this, power ),
     m_ped  ( "pedestal", "Pedestal", this, pedestal ),
     m_xMin ( xMin ),
     m_xMax ( xMax )
{
  for (int i = 0; i < coefList.getSize(); ++i)
    m_coefs.add(*coefList.at(i));
}


Double_t Hfitter::HftPeggedPoly::evaluate() const
{
  Double_t u = (m_x - m_xMin)/(m_xMax - m_xMin);
  double sum = 1, monomial = 1;
  
  for (int i = 0; i < m_coefs.getSize(); i++) {
    monomial *= u;
    RooAbsReal* coef = (RooAbsReal*)m_coefs.at(i);
    sum += coef->getVal()*monomial;
  }
  
  sum *= TMath::Power(1 - u, m_power);
  return sum + m_ped;
}


Int_t Hfitter::HftPeggedPoly::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, 
                                                 const char* /*rangeName*/ ) const 
{
   if (matchArgs(allVars, analVars, m_x)) return 1;
   return 0;
}


Double_t Hfitter::HftPeggedPoly::analyticalIntegral( Int_t code, const char* rangeName ) const 
{
   switch(code) {
   case 1: 
     Double_t xMin = m_x.min(rangeName);
     Double_t xMax = m_x.max(rangeName);
     
     if (xMin < m_xMin) xMin = m_xMin;
     if (xMax < m_xMax) xMax = m_xMax;

     if (xMax <= xMin) return 0;
     
   
     return integral(xMin, xMax);
   }
   
  assert(0);
  return 0;
}


double Hfitter::HftPeggedPoly::integral(double xMin, double xMax) const
{
  std::vector<double> alpha;
  int n = m_coefs.getSize();
  
  for (int i = 0; i <= n; ++i) {
    if (i == 0) {
      alpha.push_back(1);
      continue;
    }
    RooAbsReal* coef = (RooAbsReal*)m_coefs.at(i - 1);
    alpha.push_back(coef->getVal());
  }
    
  double sum = 0;
  
  for (int k = 0; k <= n; ++k) {
    
    double beta_k = 0;
    
    for (int i = k; i <= n; ++i) 
      beta_k += alpha[i]*TMath::Binomial(i, k)*(k % 2 == 0 ? 1 : -1);
    
    double term = beta_k; 

    double expo = m_power + k + 1;
    if (expo == 0)
      term *= (xMin - xMax);
    else
      term *= (TMath::Power(1 - xMax, expo) - TMath::Power(1 - xMin, expo))/expo;
    
    sum += term;
  }
  
  return (xMax - xMin)*m_ped - sum;
}
