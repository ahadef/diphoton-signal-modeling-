// -- CLASS DESCRIPTION [PDF] --
/*****************************************************************************
 * Project: BaBar                                                            *
 * Package: GrEATools                                                        *
 *    File: $Id: HftDoubleGaussian.cxx,v 1.2 2007/12/13 20:34:18 hoecker Exp $
 * Author :                                                                  *
 *          Nicolas Berger, CERN                                             *
 *****************************************************************************/

#include <iostream>
#include <math.h>

#include "HfitterModels/HftDoubleGaussian.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"


Hfitter::HftDoubleGaussian::HftDoubleGaussian(const char *name, const char *title,
                                              RooAbsReal& _x, RooAbsReal& _mean,
                                              RooAbsReal& _sigma, RooAbsReal& _shift,
                                              RooAbsReal& _relW, RooAbsReal& _relN) 

  : RooAbsPdf(name,title),
    x("x","Dependent",this,_x),
    mean("mean","Mean",this,_mean),
    sigma("sigma","Width",this,_sigma),
    relShift("shift","Shift",this,_shift),
    relWidth("relWidth","relWidth",this,_relW),
    relNorm("norm","norm",this,_relN)
{
}


Hfitter::HftDoubleGaussian::HftDoubleGaussian(const HftDoubleGaussian& other,
                                              const char* name)
  : RooAbsPdf(other,name), x("x",this,other.x), 
    mean("mean",this,other.mean),
    sigma("sigma",this,other.sigma), 
    relShift("shift",this,other.relShift),
    relWidth("relWidth",this,other.relWidth),
    relNorm("norm",this,other.relNorm)
{
}


Double_t Hfitter::HftDoubleGaussian::evaluate() const
{
  static const Double_t root2 = sqrt(2.0) ;
  static const Double_t rootPiBy2 = sqrt(atan2(0.0,-1.0)/2.0);

  Double_t sig1 = sigma * relWidth;

  Double_t xscale  = root2*sigma;
  Double_t xscale1 = root2*sig1;

  Double_t mean1   = mean + relShift;
  
  Double_t int0 = rootPiBy2*sigma*(erf((x.max() - mean)/xscale) - erf((x.min() - mean)/xscale));
  Double_t int1 = rootPiBy2*sig1 *(erf((x.max() - mean1)/xscale1) - erf((x.min() - mean1)/xscale1));

  Double_t arg = x - mean;  
  Double_t arg1 = x - mean1;

  return relNorm/int0*exp(-0.5*arg*arg/(sigma*sigma)) 
    +(1-relNorm)/int1*exp(-0.5*arg1*arg1/(sig1*sig1));
}



Int_t Hfitter::HftDoubleGaussian::getAnalyticalIntegral(RooArgSet& allVars, 
                                                        RooArgSet& analVars, 
                                                        const char* /*rangeName*/) const 
{
  if (matchArgs(allVars,analVars,x)) return 1 ;
  return 0 ;
}



Double_t Hfitter::HftDoubleGaussian::analyticalIntegral(Int_t code, 
                                                        const char* /*rangeName*/) const 
{
  assert(code==1) ;

  return 1;
}




Int_t Hfitter::HftDoubleGaussian::getGenerator(const RooArgSet& directVars, 
                                               RooArgSet &generateVars, 
                                               Bool_t /*staticInitOK*/) const
{
  if (matchArgs(directVars,generateVars,x)) return 1 ;  
  return 0 ;
}


void Hfitter::HftDoubleGaussian::generateEvent(Int_t code)
{
  assert(code==1) ;
  Double_t xgen ;
  while(1) {    
    xgen = RooRandom::randomGenerator()->Gaus(mean,sigma);
    if (xgen<x.max() && xgen>x.min()) {
      x = xgen ;
      break;
    }
  }
  return;
}


