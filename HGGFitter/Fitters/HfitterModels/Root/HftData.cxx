#include "HfitterModels/HftData.h"

#include "HfitterModels/HftModel.h"
#include "RooAbsCategory.h"
#include "RooRealVar.h"
#include "RooDataSet.h"

#include "TSystem.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftData::HftData(const TString& fileName, const TString& treeName, const HftModel& model,
                 const TString& cut, const TString& weightVarName, const RooArgList& extraVars)
  : TNamed(treeName + "_data", "HftData of tree " + treeName + " in file " + fileName),
    m_file(0), m_tree(0), m_data(0)
{
  m_file = new TFile(fileName);
  if (!m_file || !m_file->IsOpen()) {
    cout << "ERROR : cannot open file " + fileName << "." << endl;
    return;
  }
  
  m_tree = (TTree*)m_file->Get(treeName);
  if (!m_tree) {
    cout << "ERROR : cannot find tree " + treeName + " in file " + fileName + "." << endl;
    return;
  }

  RooArgList vars = model.ObservablesAndCats();
  vars.add(extraVars);
  
  if (weightVarName != "") {
    RooRealVar* wv = new RooRealVar(weightVarName, "weight variable", 1);
    vars.add(*wv);
  }  
  m_data = new RooDataSet(treeName + "_dataSet", "RooDataSet of tree " + treeName + " in file " + fileName,
                          m_tree, vars, cut, weightVarName != "" ? weightVarName.Data() : 0);
}


HftData* HftData::Open(const TString& fileName, const TString& treeName, const HftModel& model,
                       const TString& cut, const TString& weightVarName, const RooArgList& extraVars)
{
  HftData* d = new HftData(fileName, treeName, model, cut, weightVarName, extraVars);
  if (!d->m_data) {
    delete d;
    return 0;
  }
  return d;
}


HftData::~HftData() 
{
  if (m_file) {  
    if (m_data) delete m_data; 
    delete m_file; 
  } 
}


RooAbsData* HftData::SelectCategory(RooAbsData& data, const RooAbsCategory& cat, const TString& catState)
{
  TIterator* types = cat.typeIterator();

  while (RooCatType* type = (RooCatType*)types->Next()) {
    if (type->GetName() == catState) {
      RooAbsData* newData = data.reduce(Form("%s==%d", cat.GetName(), type->getVal()));
      delete types;
      return newData;
    }
  }
  cout << "ERROR : cannot reduce dataset to category state " << catState << " since it is not a state of " << cat.GetName() << endl;
  delete types;
  return 0;
}


bool HftData::FillAsimov(RooDataSet*& dataset, RooAbsPdf& pdf, const RooArgList& dependents, const RooArgList& others)
{
  if (!dataset) {
    RooRealVar* weightVar = new RooRealVar("weight", "", 0);
    RooArgList vars = dependents;
    vars.add(others);
    vars.add(*weightVar);
    dataset = new RooDataSet("asimov", "An Asimov dataset for " + TString(pdf.GetName()), 
                             vars, "weight");
    delete weightVar;
  }
  if (!FillAsimov(*dataset, pdf, dependents, others, std::vector<int>(), 1)) return false;
  return dataset;
}


bool HftData::FillAsimov(RooDataSet& dataset, RooAbsPdf& pdf, const RooArgList& dependents, const RooArgList& others,
                         const std::vector<int>& currentBin, double binVolume)
{
  if (dependents.getSize() != (int)currentBin.size()) {
    // we have an incomplete bin specification: need to iterate over the bins of one more
    // variable and recurse again
    RooRealVar* var = dynamic_cast<RooRealVar*>(dependents.at(currentBin.size()));
    if (!var) { 
      cout << "ERROR : variable " << dependents.at((int)currentBin.size())->GetName() << " is not RooRealVar, crashing." << endl;
      return false;
    }
    for (int i = 0; i < var->getBinning().numBins(); i++) {
      var->setVal(var->getBinning().binCenter(i));
      std::vector<int> newCB = currentBin;
      newCB.push_back(i);
      if (!FillAsimov(dataset, pdf, dependents, others, newCB, binVolume*var->getBinning().binWidth(i))) return false;
    }
    return true;
  }
  // At this point we have a complete bin list, we just need to generate the entry for it
  // All dependent values should already have been set at this point, so just:
  RooArgSet depSet(dependents), allSet(dependents);
  allSet.add(others);
  dataset.add(allSet, pdf.getVal(&depSet)*binVolume*(pdf.canBeExtended() ? pdf.expectedEvents(depSet) : 1));
  return true;
}


bool HftData::Save(const RooDataSet& data, const HftModel& model, const TString& fileName, 
                   const TString& treeName, const TString& weightVarName)
{
  TString fn = PrepareDirs(fileName);
  TFile* f = TFile::Open(fn, "RECREATE");
  TTree* t = new TTree(treeName, "");
  
  std::vector<double> obs(model.Observables().getSize());
  std::vector<int> cats(model.Categories().getSize());
  double weight;
  
  for (int i = 0; i < model.Observables().getSize(); i++) t->Branch(model.Observable(i)->GetName(), &obs[i]);
  for (int i = 0; i < model.Categories().getSize(); i++) t->Branch(model.Category(i)->GetName(), &cats[i]);
  if (weightVarName != "") t->Branch(weightVarName, &weight);
    
  for (int i = 0; i < data.numEntries(); i++) {
    const RooArgSet* entry = data.get(i);
    weight = data.weight();
    for (int i = 0; i < model.Observables().getSize(); i++) obs[i] = entry->getRealValue(model.Observable(i)->GetName());
    for (int i = 0; i < model.Categories().getSize(); i++) cats[i] = entry->getCatIndex(model.Category(i)->GetName());
    t->Fill();
  }
  t->Write();
  delete f;
  return true;
}


bool HftData::AddGhosts(RooAbsData& data, HftModel& model, const TString& varName, double minVal, double maxVal, double step, double weight)
{
  RooRealVar* var = model.Var(varName);
  if (!var) {
    cout << "ERROR : variable '" << varName << "' not found in model, cannot add ghosts" << endl;
    return false;
  }
  double orig = var->getVal();
  unsigned int n = (unsigned int)((maxVal - minVal)/step - 1E-5);
  for (unsigned int i = 0; i < n; i++) {
    //cout << "Adding " << (minVal + i*step) << ", w = " << weight << endl;
    var->setVal(minVal + i*step);
    data.add(*var, weight);
  }
  var->setVal(maxVal);
  data.add(*var, weight);
  var->setVal(orig);
  return true;
}


TString HftData::PrepareDirs(const TString& path)
{
  TString fileNameE(gSystem->ExpandPathName(path));
  TString dir = gSystem->DirName(fileNameE);
  if (dir != ".") gSystem->mkdir(dir, true); // recursive mkdir
  return fileNameE;
}


void HftData::PrintData(const RooAbsData& data)
{
  cout << Form("-- Dataset %s", data.GetName()) << endl;
  for (int i = 0; i < data.numEntries(); i++) {
    const RooArgSet* entry = data.get(i);
    TIterator* iter = entry->createIterator();
    while (RooAbsArg* arg = (RooAbsArg*)iter->Next()) {
      RooRealVar* var = dynamic_cast<RooRealVar*>(arg);
      if (var) {
        cout << Form("  var %s = %g", var->GetName(), var->getVal());
        continue;
      }
      RooCategory* cat = dynamic_cast<RooCategory*>(arg);
      if (cat) {
        cout << Form("  cat %s = %s (idx = %d)", cat->GetName(), cat->getLabel(), cat->getIndex());
        continue;
      }
    }
    delete iter;
    double weight = data.weight();
    if (weight != 1) cout << Form("  (weight = %g)", weight);
    cout << endl;
  }
  cout << Form("-- total : %d entries", data.numEntries()) << endl;
}
