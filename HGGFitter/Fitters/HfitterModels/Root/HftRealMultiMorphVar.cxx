#include "HfitterModels/HftRealMorphVar.h"

#include "RooRealVar.h"
#include "TAxis.h"

using namespace Hfitter;
/*
                
HftRealMultiMorphVar::HftRealMultiMorphVar(const HftRealMultiMorphVar& other, const TString& newName)
  : RooAbsReal(other, newName)
{
  for (unsigned int i = 0; i < other.NDimensions(); i++)
    Add(*new HftRealMorphVar(*other.MorphVar(i), newName));
}


HftRealMultiMorphVar::~HftRealMultiMorphVar()
{    
  for (unsigned int i = 0; i < NDimensions(); i++)
    delete MorphVar(i);
}


RooArgList HftRealMultiMorphVar::Parameters() const
{
  RooArgList pars;
  for (unsigned int i = 0; i < NDimensions(); i++) pars.add(MorphVar(i)->Parameters());
  return pars;
}


double HftRealMultiMorphVar::evaluate() const
{
  // Get rid of the trivial cases
  if (NPoints() == 0) return 0;
  if (NPoints() == 1) return PointVar(0)->getVal();
  
  double morphVal = m_morphingVar->getVal();
  
  // "Underflow" case
  if (morphVal < PointPosition(0)) {
    double dy = PointVar(1)->getVal() - PointVar(0)->getVal();
    double dx = PointPosition(1) - PointPosition(0);
    if (dx == 0) return 0;
    double val = PointVar(0)->getVal() + dy/dx*(morphVal - PointPosition(0));
    return val;
  }

  // "Overflow" case
  if (morphVal >= PointPosition(NPoints() - 1)) {
    double dy = PointVar(NPoints() - 1)->getVal() - PointVar(NPoints() - 2)->getVal();
    double dx = PointPosition(NPoints() - 1) - PointPosition(NPoints() - 2);
    if (dx == 0) return 0;
    double val = PointVar(NPoints() - 1)->getVal() + dy/dx*(morphVal - PointPosition(NPoints() - 1));
    return val;
  }
    
  // Normal case
  for (unsigned int i = 0; i < NPoints() - 1; i++) {
    if (morphVal >= PointPosition(i) && morphVal < PointPosition(i + 1)) {
      double dy = PointVar(i + 1)->getVal() - PointVar(i)->getVal();
      double dx = PointPosition(i + 1) - PointPosition(i);
      if (dx == 0) {
        cout << "ERROR : invalid morphing variable " << GetName() << ", points defined at identical positions!" << endl;
        return 0;
      }
      double val = PointVar(i)->getVal() + dy/dx*(morphVal - PointPosition(i));      
      //cout << "inter val = " << val << " " << morphVal << " " << i << " " << PointVar(i)->getVal() << " " << PointVar(i)->GetName() << " " << dy << endl;
      return val;
    } 
  }
  // Should never get there
  cout << "ERROR : unhandled morphing variable value " << morphVal << endl;
  return 0;
}


TGraphErrors* HftRealMorphVar::Graph() const
{
  TGraphErrors* graph = new TGraphErrors(NPoints());
  for (unsigned int i = 0; i < NPoints(); i++) {
    graph->SetPoint(i, PointPosition(i), PointVar(i)->getVal());
    graph->SetPointError(i, 0, PointVar(i)->getError());
  }
  graph->SetName("");
  graph->GetXaxis()->SetTitle(MorphingVar().GetTitle());
  graph->GetYaxis()->SetTitle(GetTitle());  
  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(1);
  return graph;
}*/
