#include "HfitterModels/HftCountingPdfBuilder.h"
#include "RooPolynomial.h"

using namespace Hfitter;


void HftCountingPdfBuilder::Setup(const char* depName)
{
  m_depName = depName;
}


RooAbsPdf* HftCountingPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& /*workspace*/) const
{
  RooAbsReal& dep = *Dependent(dependents, m_depName);
  return new RooPolynomial(name, "Counting PDF for " + m_depName, dep);
}
