#include "HfitterModels/HftDataHist.h"

#include "HfitterModels/HftModel.h"
#include "RooAbsCategory.h"
#include "RooRealVar.h"
#include "TMath.h"
#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftDataHist::HftDataHist(const TString& fileName, const TString& histNames, const HftModel& model, double w)
  : TNamed(histNames + "_data", "HftData of tree " + histNames + " in file " + fileName),
    m_file(0), m_hist(0), m_data(0), m_weight(w)
{
  
  m_file = new TFile(fileName);
  if (!m_file || !m_file->IsOpen()) {
    cout << "ERROR : cannot open file " + fileName << "." << endl;
    return;
  }

  RooArgList vars = model.Observables();
  
  // support only 1 category for now
  RooCategory* mainCat = dynamic_cast<RooCategory*>(model.MainCategory());
  
  if (!mainCat) {
    m_hist = (TH1*)m_file->Get(histNames);
    if (!m_hist) {
      cout << "ERROR : cannot find histogram " << histNames << " in file " << fileName << "." << endl;
      return;
    }
    if (m_weight == -1) m_weight = m_hist->GetBinContent(1)/TMath::Power(m_hist->GetBinError(1), 2);
    
    m_data = new RooDataHist(histNames + "_dataHist", "RooDataHist of histogram " + histNames + " in file " + fileName,
                             vars, m_hist, m_weight);
  }
  else {
    std::map<std::string, TH1*> hists;
    TString name;
    TObjArray* names = histNames.Tokenize(",");
    if (names->GetEntries() != mainCat->numTypes()) {
      cout << "ERROR: model category has " << mainCat->numTypes() << " states, but only " << names->GetEntries() << " histogram names were provided as " << histNames << endl;
      delete names;
      return;
    }
    for (int i = 0; i < mainCat->numTypes(); i++) {
      TH1* hist = (TH1*)m_file->Get(names->At(i)->GetName());
      if (!hist) {
        cout << "ERROR : cannot find histogram " << names->At(i)->GetName() << " in file " << fileName << "." << endl;
        return;
      }
      if (m_weight == -1) m_weight = hist->GetBinContent(1)/TMath::Power(hist->GetBinError(1), 2);
      cout << "Assigning histogram " << names->At(i)->GetName() << " to category state " << mainCat->lookupType(i)->GetName() << endl;
      hists[mainCat->lookupType(i)->GetName()] = hist;
      if (i > 0) name += "_";
      name += names->At(i)->GetName();
    }
    m_data = new RooDataHist(name + "_dataHist", "RooDataHist of histograms " + histNames + " in file " + fileName,
                             vars, *mainCat, hists, m_weight);
  }
}


HftDataHist* HftDataHist::Open(const TString& fileName, const TString& histNames, const HftModel& model, double w)
{
  HftDataHist* d = new HftDataHist(fileName, histNames, model, w);
  if (!d->m_data) {
    delete d;
    return 0;
  }
  return d;
}
