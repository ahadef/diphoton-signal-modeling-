#include "HfitterModels/HftParameterStorage.h"

using namespace Hfitter;

using std::cout;
using std::endl;


HftRooRealVarStorage::HftRooRealVarStorage(const RooRealVar& var) 
  : val(var.getVal()), err(var.hasError() ? var.getError() : -1), constness(var.isConstant()) 
{
}


HftRooRealVarStorage::HftRooRealVarStorage(double a_val, bool a_constness, double a_err) 
  : val(a_val), err(a_err), constness(a_constness) 
{
}


void HftRooRealVarStorage::Load(RooRealVar& var, bool withValue, bool withConstness) const
{ 
  if (withValue) { 
    var.setVal(val); 
    if (err >= 0) var.setError(err); 
  } 
  if (withConstness) var.setConstant(constness);
}



void HftParameterStorage::Save(const RooRealVar& var)
{
  m_varStorage[var.GetName()] = HftRooRealVarStorage(var);
}


void HftParameterStorage::Save(const RooCategory& cat)
{
  m_catStorage[cat.GetName()] = cat.getIndex();
}


void HftParameterStorage::Save(const TString& name, double val, double err, bool constness)
{
  m_varStorage[name] = HftRooRealVarStorage(val, constness, err);
}


double HftParameterStorage::Value(const RooRealVar& var) const
{
  double val;
  Value(var, val);
  return val;
}


int HftParameterStorage::Value(const RooCategory& cat) const
{
  int val;
  Value(cat, val);
  return val;
}


double HftParameterStorage::Error(const RooRealVar& var) const
{
  double err;
  Error(var, err);
  return err;
}


bool HftParameterStorage::Const(const RooRealVar& var) const
{
  bool constness;
  Const(var, constness);
  return constness;
}


bool HftParameterStorage::Value(const RooRealVar& var, double& val) const
{
  std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.find(var.GetName());
  if (entry == m_varStorage.end()) {
    val = 0;
    return false;
  }
  val = entry->second.val;
  return true;
}


bool HftParameterStorage::Value(const RooCategory& cat, int& val) const
{
  std::map<TString, int>::const_iterator entry = m_catStorage.find(cat.GetName());
  if (entry == m_catStorage.end()) {
    val = 0;
    return false;
  }
  val = entry->second;
  return true;
}


bool HftParameterStorage::Error(const RooRealVar& var, double& err) const
{
  std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.find(var.GetName());
  if (entry == m_varStorage.end()) {
    err = 0;
    return false;
  }
  err = entry->second.err;
  return true;
}


bool HftParameterStorage::Const(const RooRealVar& var, bool& constness) const
{
  std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.find(var.GetName());
  if (entry == m_varStorage.end()) {
    constness = 0;
    return false;
  }
  constness = entry->second.constness;
  return true;
}


double HftParameterStorage::RealValue(const TString& varName) const
{
  std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.find(varName);
  return (entry == m_varStorage.end() ? 0 : entry->second.val);
}


double HftParameterStorage::RealError(const TString& varName) const
{
  std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.find(varName);
  return (entry == m_varStorage.end() ? 0 : entry->second.err);
}


bool HftParameterStorage::Load(RooRealVar& var, bool value, bool constness) const
{
  std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.find(var.GetName());
  if (entry == m_varStorage.end()) return false;
  entry->second.Load(var, value, constness);
  return true;
}


bool HftParameterStorage::Load(RooCategory& cat) const
{
  std::map<TString, int>::const_iterator entry = m_catStorage.find(cat.GetName());
  if (entry == m_catStorage.end()) return false;
  cat.setIndex(entry->second);
  return true;
}


void HftParameterStorage::Print() const
{
  cout << Str() << endl;
}

TString HftParameterStorage::Str() const
{
  TString s = "";
  for (std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.begin();
       entry != m_varStorage.end(); entry++)
    s += Form("%30s : %10g +/- %10g %s\n", entry->first.Data(), 
              entry->second.val, entry->second.err, entry->second.constness ? "(const)" : "");
  for (std::map<TString, int>::const_iterator entry = m_catStorage.begin();
       entry != m_catStorage.end(); entry++)
    s += Form("%30s : index %3d\n", entry->first.Data(), entry->second);
  return s;
}


RooArgList HftParameterStorage::StoredVars(bool vars, bool cats) const
{
  RooArgList all;
  if (vars) {
    for (std::map<TString, HftRooRealVarStorage >::const_iterator entry = m_varStorage.begin();
         entry != m_varStorage.end(); entry++) {
      RooRealVar* var = new RooRealVar(entry->first, "", entry->second.val);
      var->setError(entry->second.err);
      var->setConstant(entry->second.constness);
      all.addOwned(*var);
    }
  }
  if (cats) {
    for (std::map<TString, int>::const_iterator entry = m_catStorage.begin();
         entry != m_catStorage.end(); entry++) {
      RooCategory* cat = new RooCategory(entry->first, "");
      cat->defineType(Form("state%d", entry->second), entry->second);
    all.addOwned(*cat);
    }
  }
  return all;
}


bool HftParameterStorage::Add(const HftParameterStorage& other, bool allowOverlap)
{
  for (std::map<TString, HftRooRealVarStorage >::const_iterator entry = other.m_varStorage.begin();
       entry != other.m_varStorage.end(); entry++) {
    std::map<TString, HftRooRealVarStorage >::const_iterator isStored = m_varStorage.find(entry->first);
    if (isStored != m_varStorage.end() && !allowOverlap) {
      cout << "ERROR : variable " << entry->first << " is present in both HftParameterStorage, cannot add." << endl;
      return false;
    }
    m_varStorage[entry->first] = entry->second;
  }
  for (std::map<TString, int>::const_iterator entry = other.m_catStorage.begin();
       entry != other.m_catStorage.end(); entry++) {
    std::map<TString, int>::const_iterator isStored = m_catStorage.find(entry->first);
    if (isStored != m_catStorage.end() && !allowOverlap) {
      cout << "ERROR : category " << entry->first << " is present in both HftParameterStorage, cannot add." << endl;
      return false;
    }
    m_catStorage[entry->first] = entry->second;
  }
  return true;
}


void HftParameterStorage::FixAll(bool fix)
{
  for (std::map<TString, HftRooRealVarStorage >::iterator entry = m_varStorage.begin();
         entry != m_varStorage.end(); entry++) entry->second.constness = fix;
}


HftParameterStorage HftParameterStorage::AllFixed(bool fix) const
{
  HftParameterStorage allFixed = *this;
  allFixed.FixAll(fix);
  return allFixed;
}


bool HftParameterStorage::CopyValues(const HftParameterStorage& other)
{
  bool ok = true;
  for (std::map<TString, HftRooRealVarStorage >::iterator entry = m_varStorage.begin();
       entry != m_varStorage.end(); entry++) {
    if (other.StoredVars().find(entry->first)) {
      Save(entry->first, other.RealValue(entry->first), other.RealError(entry->first), entry->second.constness);
    }
    else ok = false;
  }
  return ok;
}
