#include "HfitterModels/HftAbsRealBuilder.h"

#include "HfitterModels/HftGenericRealBuilder.h"
#include "RooRealVar.h"
#include "TClass.h"
#include "TMethod.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;


HftAbsRealBuilder* HftAbsRealBuilder::Create(const TString& name, const TString& args, bool verbose)
{
  // static member functions -- real global functions are not supported yet.
  TObjArray* tokens = name.Tokenize("::");
  if (tokens->GetEntries() >= 2) {
    TString funcName = tokens->At(tokens->GetEntries() - 1)->GetName();
    TString className = tokens->At(0)->GetName();
    for (int i = 1; i < tokens->GetEntries() - 1; i++) className += TString("::") + tokens->At(i)->GetName();
    TClass* instanceClass = TClass::GetClass(className);
    if (instanceClass) {
      TMethod* meth = instanceClass->GetMethodAny(funcName);
      if (meth) {
        // line below is wrong (this is not the function address), so the rest doesn't work either
        //void* func = meth->InterfaceMethod();
//         HftGenericRealBuilder* builder = dynamic_cast<HftGenericRealBuilder*>(HftInstance::Create("HftGenericRealBuilder", args, verbose));
//         builder->SetFunc(func);
//         builder->SetVerbosity(verbose);
//         delete tokens;
//         return builder;
        return 0;
      }
    }
  }
  HftInstance* instance = HftInstance::Create(name, args, verbose);
  HftAbsRealBuilder* builder = dynamic_cast<HftAbsRealBuilder*>(instance);
  if (!builder) {
    cout << "ERROR : builder class " << name << " does not derive from HftAbsRealBuilder, cannot be used to build real nodes" << endl;
  }
  builder->SetVerbosity(verbose);
  return builder;
}
