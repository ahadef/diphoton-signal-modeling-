#include "HfitterModels/HftRealMorphVar.h"

#include "RooRealVar.h"
#include "TAxis.h"

using namespace Hfitter;
using std::cout;
using std::endl;

HftRealMorphVar::HftRealMorphVar(const TString& name, const TString& title,
                                 RooAbsReal& morphingVar, const std::vector<double>& pointPositions, 
                                 const RooArgList& pointVars)
  : RooAbsReal(name, title),
    m_pointPositions(pointPositions)
{
  if (pointVars.getSize() != (int)pointPositions.size()) return;
  if (pointVars.getSize() == 0) return;
  setUnit(((RooAbsReal*)pointVars.at(0))->getUnit());
  for (int i = 0; i < pointVars.getSize(); i++) addServer(*pointVars.at(i));
  addServer(morphingVar);
}


HftRealMorphVar::HftRealMorphVar(const HftRealMorphVar& other, const char* newName)
  : RooAbsReal(other, newName),
    m_pointPositions(other.m_pointPositions)
{
}


HftRealMorphVar::~HftRealMorphVar()
{
}

/*
bool HftRealMorphVar::readFromStream(istream& is, bool compact, bool verbose)
{
  for (unsigned int i = 0; i < NParameters(); i++)
    if (!Parameter(i)->readFromStream(is, compact, verbose)) return false;
  return true;
}


void HftRealMorphVar::writeToStream(ostream& os, bool compact) const
{
  for (unsigned int i = 0; i < NParameters(); i++)
    Parameter(i)->writeToStream(os, compact);
}
*/


RooArgList HftRealMorphVar::Parameters() const
{
  RooArgList retVal;
  for (unsigned int i = 0; i < NPoints(); i++) {
    retVal.add(*PointVar(i));
  }
  retVal.add(MorphingVar());
  return retVal;
}


double HftRealMorphVar::evaluate() const
{
  // Get rid of the trivial cases
  if (NPoints() == 0) return 0;
  if (NPoints() == 1) return PointVar(0)->getVal();
  
  double morphVal = MorphingVar().getVal();
  
  // "Underflow" case
  if (morphVal < PointPosition(0)) {
    double dy = PointVar(1)->getVal() - PointVar(0)->getVal();
    double dx = PointPosition(1) - PointPosition(0);
    if (dx == 0) return 0;
    double val = PointVar(0)->getVal() + dy/dx*(morphVal - PointPosition(0));
    return val;
  }

  // "Overflow" case
  if (morphVal >= PointPosition(NPoints() - 1)) {
    double dy = PointVar(NPoints() - 1)->getVal() - PointVar(NPoints() - 2)->getVal();
    double dx = PointPosition(NPoints() - 1) - PointPosition(NPoints() - 2);
    if (dx == 0) return 0;
    double val = PointVar(NPoints() - 1)->getVal() + dy/dx*(morphVal - PointPosition(NPoints() - 1));
    return val;
  }
    
  // Normal case
 for (unsigned int i = 0; i < NPoints() - 1; i++) {
    if (morphVal >= PointPosition(i) && morphVal < PointPosition(i + 1)) {
      //cout << PointPosition(i) << " " << PointPosition(i+1) << endl;
      //cout << PointVar(i)->getVal() << " " <<  PointVar(i+1)->getVal() << endl;
      double dy = PointVar(i + 1)->getVal() - PointVar(i)->getVal();
      double dx = PointPosition(i + 1) - PointPosition(i);
      if (dx == 0) {
        cout << "ERROR : invalid morphing variable " << GetName() << ", points defined at identical positions!" << endl;
        return 0;
      }
      double val = PointVar(i)->getVal() + dy/dx*(morphVal - PointPosition(i));      
      //cout << "inter val = " << val << " " << morphVal << " " << i << " " << PointVar(i)->getVal() << " " << PointVar(i)->GetName() << " " << dy << endl;
      return val;
    } 
  }
  // Should never get there
  cout << "ERROR : unhandled morphing variable value " << morphVal << endl;
  return 0;
}


TGraphErrors* HftRealMorphVar::Graph() const
{
  TGraphErrors* graph = new TGraphErrors(NPoints());
  for (unsigned int i = 0; i < NPoints(); i++) {
    graph->SetPoint(i, PointPosition(i), PointVar(i)->getVal());
    graph->SetPointError(i, 0, PointVar(i)->getError());
  }
  graph->SetName("");
  graph->GetXaxis()->SetTitle(MorphingVar().GetTitle());
  graph->GetYaxis()->SetTitle(GetTitle());  
  graph->SetMarkerStyle(20);
  graph->SetMarkerSize(1);
  return graph;
}


TString HftRealMorphVar::MakeName(const TString& rootName, const TString& pointName)
{
  TString insert = "@" + pointName;
  int pos = rootName.Index("_");
  if (pos == -1) 
    return rootName + insert;
  return rootName(0, pos) + insert + rootName(pos, rootName.Length() - pos);
}
