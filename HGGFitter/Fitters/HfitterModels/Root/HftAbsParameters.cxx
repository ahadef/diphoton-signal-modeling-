// $Id: HftPdfBuilder.cxx,v 1.13 2008/03/05 23:34:49 nberger Exp $   
// Author: Nicolas Berger

#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftRealMorphVar.h"

#include "RooRealVar.h"
#include "RooFormulaVar.h"
#include "RooCategory.h"
#include "RooArgSet.h"
#include "RooStreamParser.h"
#include "RooWorkspace.h"

#include <sstream>

using namespace Hfitter;
using std::cout;
using std::endl;
using std::istringstream;


void HftAbsParameters::AddClients(const std::vector<HftAbsParameters*>& clients)
{
  m_clients.insert(m_clients.end(), clients.begin(), clients.end());
}


RooAbsArg* HftAbsParameters::Parameter(unsigned int i) const
{ 
  return Parameters().at(i);
}


RooAbsArg* HftAbsParameters::Parameter(const TString& name) const
{ 
  //cout << "Searching for " << name << " among ";
  //Parameters().Print();
  //cout << endl;
  RooAbsArg* arg = Parameters().find(name); 
  if (arg) return arg;
  for (std::vector<HftAbsParameters*>::const_iterator client = m_clients.begin(); client != m_clients.end(); client++) {
    arg = (*client)->Parameter(name);
    if (arg) return arg;
  }
  return 0;
}


RooRealVar* HftAbsParameters::Var(const TString& name) const
{   
  RooAbsArg* arg = Parameter(name);
  if (arg) return dynamic_cast<RooRealVar*>(arg);
  return 0;
}


RooRealVar* HftAbsParameters::Var(unsigned int i) const
{   
  RooAbsArg* arg = Parameter(i);
  if (arg) return dynamic_cast<RooRealVar*>(arg);
  return 0;
}


RooAbsReal* HftAbsParameters::Real(const TString& name) const
{   
  RooAbsArg* arg = Parameter(name);
  if (arg) return dynamic_cast<RooAbsReal*>(arg);
  return 0;
}


RooCategory* HftAbsParameters::Cat(const TString& name) const
{   
  RooAbsArg* arg = Parameter(name);
  if (arg) return dynamic_cast<RooCategory*>(arg);
  return 0;
}


RooCategory* HftAbsParameters::Cat(unsigned int i) const
{   
  RooAbsArg* arg = Parameter(i);
  if (arg) return dynamic_cast<RooCategory*>(arg);
  return 0;
}


bool HftAbsParameters::WriteToStream(std::ostream& stream, const TString& section)
{
  RooArgSet basics;
  for (unsigned int i = 0; i < NParameters(); i++)
    if (Parameter(i)->isFundamental()) basics.add(*Parameter(i));
  basics.writeToStream(stream, false, section);
  return true;
}


bool HftAbsParameters::ReadFromStream(std::istream& stream, const TString& section)
{
  RooArgSet basics;
  for (unsigned int i = 0; i < NParameters(); i++)
    if (Parameter(i)->isFundamental()) basics.add(*Parameter(i));
  basics.readFromStream(stream, false, "", section);
  return true;
}


bool HftAbsParameters::FloatParameters(const RooArgList& exclude)
{
  return FixAll(Parameters(), false, exclude);
}


bool HftAbsParameters::FixParameters(const RooArgList& exclude)
{
  return FixAll(Parameters(), true, exclude);
}


RooArgList HftAbsParameters::SplitParameters(RooWorkspace& workspace) const
{
  RooArgList picked;
  //RooArgList picked, pickedNonSplit;
  RooArgList splitParams = workspace.allFunctions();
  splitParams.add(workspace.allVars());
  
  for (int i = 0; i < Parameters().getSize(); i++) {
    RooAbsArg* arg = Parameters().at(i);
    RooAbsCollection* split = splitParams.selectByAttrib(TString("ORIGNAME:") + arg->GetName(), true);
    if (split->getSize() > 0)
      picked.add(*split);
    else {
      RooAbsArg* splitArg = splitParams.find(arg->GetName());
      if (splitArg) {
        picked.add(*splitArg);
        continue;
      }
    }
  }

  return picked;
}


RooArgList HftAbsParameters::Find(const std::vector<TString>& names) const
{
  RooArgList found;
  for (std::vector<TString>::const_iterator name = names.begin();
       name != names.end(); name++) {
    RooAbsArg* arg = Parameter(*name);
    if (arg) found.add(*arg);
  }
  return found;
}


RooArgList HftAbsParameters::Find(const RooArgList& pars) const
{
  RooArgList found;
  for (int i = 0; i < pars.getSize(); i++) {
    RooAbsArg* arg = Parameter(pars.at(i)->GetName());
    if (arg) found.add(*arg);
  }
  return found;
}


bool HftAbsParameters::SetValues(const RooArgSet& set)
{
  for (unsigned int i = 0; i < NParameters(); i++) {
    RooAbsArg* par = set.find(Parameter(i)->GetName());  
    if (!par) {
      cout << "Cannot set parameter values : no value available for " << Parameter(i)->GetName() << endl;
      return false;
    }
      
    RooRealVar* thisVar = dynamic_cast<RooRealVar*>(Parameter(i));
    RooRealVar*  setVar = dynamic_cast<RooRealVar*>(par);

    if (thisVar && setVar) {
      thisVar->setVal(setVar->getVal());
      continue;
    }
      
    RooCategory* thisCat = dynamic_cast<RooCategory*>(Parameter(i));
    RooCategory*  setCat = dynamic_cast<RooCategory*>(par);

    if (thisCat && setCat) {
      thisCat->setIndex(setCat->getIndex());
      continue;
    }
      
    cout << "Cannot set parameter values : mismatch between expected class " << Parameter(i)->ClassName()
         << " and obtained " << par->ClassName() << endl;
    return false;
  }
  return true;
}


RooArgList HftAbsParameters::FreeParameters() const
{
  RooArgList freeParameters;
  for (unsigned int i = 0; i < NParameters(); i++)
    if (!Parameter(i)->isDerived() && !Parameter(i)->isConstant()) freeParameters.add(*Parameter(i));
  return freeParameters;
}


RooAbsReal* HftAbsParameters::Import(const RooAbsReal& var, RooWorkspace& workspace, bool verbose)
{
  workspace.import(var, RooFit::Silence(!verbose));
  RooAbsReal* newVar = workspace.function(var.GetName());
  delete &var;
  return newVar;
}


RooFormulaVar* HftAbsParameters::MakeFormulaVar(const TString& name, const TString& title, const TString& formula, 
                                                RooWorkspace& workspace, bool verbose)
{
  std::map<TString, bool> reserved;
  reserved["pow"]   = true; reserved["sq"]    = true; reserved["str"]   = true; reserved["int"] = true;
  reserved["min"]   = true; reserved["max"]   = true; reserved["log"]   = true; reserved["exp"] = true;
  reserved["sin"]   = true; reserved["cos"]   = true; reserved["tan"]   = true;
  reserved["sinh"]  = true; reserved["cosh"]  = true; reserved["tanh"]  = true;
  reserved["asin"]  = true; reserved["acos"]  = true; reserved["atan"]  = true; reserved["atan2"] = true;
  reserved["asinh"] = true; reserved["acosh"] = true; reserved["atanh"] = true;
  reserved["sqrt"]  = true; reserved["abs"]   = true; reserved["fmod"]  = true;
  reserved["log10"] = true; reserved["rndm"]  = true; reserved["Landau"] = true;
  reserved["erf"]   = true; reserved["TMath"] = true;
  istringstream is(formula.Data());
  RooStreamParser parser(is);
  parser.setPunctuation(TString("+-*/") + parser.getPunctuation());
  TString mask = "";
  
  RooArgList vars;
  
  while (1) {
    TString token = parser.readToken();
    if (token == "") break;
    if (vars.find(token)) {
      mask += Form("@%d", vars.index(token));
      continue;
    }
    TString initial = token[0]; initial.ToLower();
    TString token2 = initial + token(1, token.Length() - 1);
    if (!TString(token[0]).IsAlpha() || reserved.find(token) != reserved.end() || reserved.find(token2) != reserved.end()) {
      mask += token;
      continue;
    }
    RooAbsReal* var = workspace.function(token);
    if (!var) {
      if (verbose) cout << "Creating variable " << token << " in formula " << name << endl;
      var = new RooRealVar(token, "", 0);
      var = Import(*var, workspace, verbose);
    }
    else 
      if (verbose) cout << "Reusing existing parameter " << token << " for formula " << name << endl;
    vars.add(*var);
    mask += Form("@%d", vars.index(token));
  }

  if (verbose) {
    cout << "Creating formula : " << mask << " with vars ";
    vars.Print();
  }
  RooFormulaVar* var = new RooFormulaVar(name, title, mask, vars);
  if (!var->ok()) {
    cout << "ERROR defining the formula " << name << " = " << formula << "." << endl;
    delete var;
    return 0;
  }
  var = (RooFormulaVar*)Import(*var, workspace, verbose);
  return var;
}


bool HftAbsParameters::Add(RooAbsArg& var, RooArgList& list)
{
  RooRealVar* rrv = dynamic_cast<RooRealVar*>(&var);
  if (rrv) {
    AddUnique(RooArgList(var), list);
    return true;
  }
  RooFIter iter = var.serverMIterator() ;
  while (RooAbsArg* server = iter.next()) {
    Add(*server, list);
  }
  return true;
}


void HftAbsParameters::AddUnique(const RooArgList& source, RooArgList& dest, bool multiAdd)
{
  for (int i = 0; i < source.getSize(); i++) {
    if (dest.find(source.at(i)->GetName())) continue;
    if (multiAdd)
      Add(*source.at(i), dest);
    else
      dest.add(*source.at(i));
  }
}


bool HftAbsParameters::FixAll(const RooArgList& vars, bool fix, const RooArgList& exclude)
{
  for (int i = 0; i < vars.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(vars.at(i));
    if (!var) continue;
    if (exclude.find(var->GetName())) continue;

    //cout << (fix ? "Fixing " : "Freeing ") << var->GetName() << endl;
    var->setConstant(fix);
  }
    
  return true;
}


TString HftAbsParameters::Merge(const TString& name1, const TString& name2)
{
  if (name1 == "") return name2;
  if (name2 == "") return name1;
  return name1 + "_" + name2;
}


TString HftAbsParameters::StripCategories(const TString& name)
{
  int index1 = name.Index(".{");
  if (index1 >= 0) {
    int index2 = name.Index("}");
    if (index2 > index1) return name(0, index1);
  }
  int index = name.Index("_");
  if (index > 0) return name(0, index);
  return name;
}


TString HftAbsParameters::TitleAndUnit(const TString& title, const TString& unit)
{
  if (unit == "") return title;
  return title + " [" + unit + "]";
}


TString HftAbsParameters::TitleAndUnit(const RooRealVar& var)
{
  return TitleAndUnit(var.GetTitle(), var.getUnit());
}
