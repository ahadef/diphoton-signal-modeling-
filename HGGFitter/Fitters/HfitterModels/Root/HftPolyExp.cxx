// -- CLASS DESCRIPTION [PDF] --
/*****************************************************************************
 * Project: Hfitter                                                          *
 * Package: Hfitter                                                          *
 *    File: $Id: HftPolyExp.cxx,v 1.4 2007/12/13 20:34:21 hoecker Exp $
 * Author :                                                                  *
 *          Nicolas Berger, CERN                                             *
 *****************************************************************************/

#include <iostream>
#include <math.h>

#include "HfitterModels/HftPolyExp.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "TMath.h"


Hfitter::HftPolyExp::HftPolyExp(const char *name, const char *title,
                                              RooAbsReal& _x, RooAbsReal& _x0,
                                              RooAbsReal& _n, RooAbsReal& _alpha)

  : RooAbsPdf(name,title),
    x("x","Dependent",this,_x),
    x0("x0","endpoint",this,_x0),
    n("n","Polynomial power",this,_n),
    alpha("alpha","Exponential factor",this,_alpha)
{
}


Hfitter::HftPolyExp::HftPolyExp(const HftPolyExp& other,
                                              const char* name)
  : RooAbsPdf(other,name), x("x",this,other.x), 
    x0("x0",this,other.x0),
    n("n",this,other.n),
    alpha("alpha",this,other.alpha)
{
}


Double_t Hfitter::HftPolyExp::evaluate() const
{
  Double_t u = x - x0;
  return TMath::Power(u, n)*TMath::Exp(-alpha*u);
}



Int_t Hfitter::HftPolyExp::getAnalyticalIntegral(RooArgSet& allVars, 
						 RooArgSet& analVars, 
						 const char* /*rangeName*/) const 
{
  //  return 0;
  if (matchArgs(allVars,analVars,x)) return 1 ;
  return 0 ;
}



Double_t Hfitter::HftPolyExp::analyticalIntegral(Int_t /*code*/, 
						 const char* rangeName) const 
{
  Double_t xMin = x.min(rangeName) - x0;
  Double_t xMax = x.max(rangeName) - x0;

  return integralFromZero(xMax) - integralFromZero(xMin);
}



Double_t Hfitter::HftPolyExp::integralFromZero(Double_t xMax) const
{
  if (xMax <= 0) return 0;

  // in ROOT the incomplete Gamma function is normalized so that
  // Gamma(n, infty) = 1 -- So t get the real imcomplete Gamma, need to 
  // multiply by Gamma(n).
  return TMath::Gamma(n + 1, alpha*xMax)*TMath::Gamma(n + 1)/TMath::Power(alpha, n + 1);
}

