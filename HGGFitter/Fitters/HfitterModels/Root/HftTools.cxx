// $Id: HftTools.cxx,v 1.34 2009/01/30 16:25:12 hoecker Exp $   
// Author: Mohamed Aharrouche, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#include "HfitterModels/HftTools.h"

#include "TText.h"
#include "TLine.h"
#include "TPolyLine.h"
#include "TEllipse.h"


#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


void Hfitter::FillRange(unsigned int n, double xMin, double xMax, std::vector<double>& v)
{
  if (n == 0) return;
  for (unsigned int i = 0; i < n; i++) v.push_back(xMin + i*(xMax - xMin)/(n - 1));
}


std::vector<double> Hfitter::Range(unsigned int n, double xMin, double xMax)
{
  std::vector<double> v;
  FillRange(n, xMin, xMax, v);
  return v;
}


void Hfitter::DrawLogo(double size, TVirtualPad* pad)
{
  double x1 = pad->GetX1() + 0.7*(pad->GetX2() - pad->GetX1());
  double y1 = pad->GetY1() + 0.7*(pad->GetY2() - pad->GetY1());
  double aspect = 1.0*pad->GetWw()/pad->GetWh();
  double r1 = (pad->GetX2() - pad->GetX1())*size;
  double r2 = (pad->GetY2() - pad->GetY1())*size*aspect;
  TText tH; tH.SetTextSize(3*size); tH.SetTextColor(0);
  TText tF; tF.SetTextSize(2*size);
  TEllipse* ell = new TEllipse(x1, y1, r1, r2, 0, 360 ,0);
  ell->SetFillColor(4);
  ell->SetLineWidth(2);
  ell->Draw();
  tH.DrawText(x1 - 81*size,  y1 - 8*size, "H");
  tF.DrawText(x1 + 140*size, y1 + 0*size,    "fitter");
  TLine* l = new TLine(x1 + 105*size, y1 - 8*size, x1 + 500*size, y1 - 8*size);
  l->SetLineStyle(kDashed);
  l->Draw();
  const unsigned int n = 100;
  double x[n], y[n];   
  double xL = x1 + 155*size, xH = x1 + 450*size, yL = y1 - 8*size;
  double  x0 = (xL+xH)/2, s = 40*size;
  for (unsigned int i = 0; i < n; i++) { 
        x[i] = xL+i*(xH-xL)/(n-1); 
        y[i] = yL + exp(-(x[i] - x0)*(x[i] - x0)/2/s/s)*0.25; 
  }
  //x[n-1] = x[0]; y[n-1] = y[0];
  for (unsigned int i = 0; i < n; i++) { cout << x[i] << " " << y[i] <<  endl; }
  TPolyLine* pLine = new TPolyLine(n,x,y);
  pLine->SetLineWidth(2);
  pLine->SetFillColor(2);
  pLine->Draw();
  pLine->Draw("F");
}
