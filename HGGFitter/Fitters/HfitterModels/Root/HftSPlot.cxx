#include "HfitterModels/HftSPlot.h"
#include "HfitterModels/HftModel.h"
#include "RooDataSet.h"
#include "RooRealVar.h"
#include "RooPlot.h"
#include "TMatrixDSym.h"
#include "TVectorD.h"
#include "TTree.h"
#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


HftSPlot::HftSPlot(HftModel& model, RooAbsData& data, const TString& componentName)
  : m_model(&model), m_componentModel(0), m_componentIndex(0), m_data(&data), m_sWeight(0), m_sData(0)
{
  for (unsigned int i = 0; i < m_model->NComponents(); i++) 
    if (m_model->ComponentModel(i)->GetName() == componentName) {
      m_componentIndex = i;
      m_componentModel = m_model->ComponentModel(i);
      break;
    }
  if (!m_componentModel) {
    cout << "ERROR : " << componentName << " is not a component of the model." << endl;
    return;
  }  
}
 
 
HftSPlot::~HftSPlot()
{
  delete m_sData;
  delete m_sWeight;
}


HftSPlot* HftSPlot::Create(HftModel& model, RooAbsData& data, const TString& componentName, bool refit)
{
  HftSPlot* splot = new HftSPlot(model, data, componentName);  
  if (!splot->Initialize(refit)) return 0;
  return splot;
}


bool HftSPlot::Initialize(bool refit)
{
  if (!m_componentModel) return 0;
  
  TMatrixDSym v;
  if (!GetCovMatrix(v, refit)) return false;
  if (!MakeSData(v)) return false;
  
  return true;
}


bool HftSPlot::GetCovMatrix(TMatrixDSym& v, bool refit)
{ 
  RooArgSet normSet = m_model->Observables();
  RooArgList depsCats = m_model->ObservablesAndCats();
    
  if (refit) {  
    cout << "Fitting data..." << endl;
    m_model->FitData(*m_data);
  }
  
  v.ResizeTo(0, 0);
  v.ResizeTo(m_model->NComponents(), m_model->NComponents());
 
  cout << "Filling V^{-1} matrix..." << endl;  
  for (int k = 0; k < m_data->numEntries(); k++) {
    const RooArgSet* event = m_data->get(k);
    double aweight = m_data->weight();
    depsCats = *event;
    
    double sum = 0;
    std::vector<RooAbsPdf*> pdfs;
    for (unsigned int i = 0; i < m_model->NComponents(); i++) {
      RooAbsPdf* pdf = m_model->ComponentModel(i)->Pdf();
      if (!pdf) {
        cout << "ERROR : component " << m_model->ComponentModel(i)->GetName() << " has null PDF!" << endl;
        return false;
      }
      pdfs.push_back(pdf);
      sum += m_model->ComponentModel(i)->NEvents()*pdf->getVal(&normSet);
    }
    if (sum == 0) {
      cout << "WARNING : PDF sum has value == 0 for event " << k << ", skipping this event." << endl;
      continue;
    }
    
    for (unsigned int i = 0; i < m_model->NComponents(); i++) {
      double val_i = pdfs[i]->getVal(&normSet);
      for (unsigned int j = i; j < m_model->NComponents(); j++) {
        double val_j = pdfs[j]->getVal(&normSet);
        double term = aweight*val_i*val_j/(sum*sum);
        //cout << i << " " << j << " : " << val_i << " " << val_j << " " << sum << " " << term << endl;
        v(i, j) = v(i, j) + term;
        v(j, i) = v(i, j);
      }
    }
  }

  if (v.Determinant() == 0) {
    cout << "ERROR : Covariance matrix is singular!" << endl;
    return false;
  }

  cout << "Inverting matrix..." << endl;
  v.Invert();
  return true;
}
 
 
bool HftSPlot::MakeSData(const TMatrixDSym& v)
{
  RooArgSet normSet = m_model->Observables();
  RooArgList depsCats = m_model->ObservablesAndCats();

  cout << "Making SPlot data..." << endl;
  m_sWeight = new RooRealVar("sWeight", "sWeight for model " + m_model->GetName(), 0);
  RooRealVar *aWeight = new RooRealVar("aWeight", "aWeight", 1);
  RooArgSet vars = *m_data->get(0);
  RooArgSet varsAndWeight(vars); 
  varsAndWeight.add(*m_sWeight);
  varsAndWeight.add(*aWeight); 
  m_sData = new RooDataSet("sData", "", varsAndWeight, "sWeight");

  for (int k = 0; k < m_data->numEntries(); k++) {
    const RooArgSet* event = m_data->get(k);
    depsCats = *event;
    TVectorD vals(m_model->NComponents());
    double sum = 0;
    for (unsigned int i = 0; i < m_model->NComponents(); i++) {
      RooAbsPdf* pdf = m_model->ComponentModel(i)->Pdf();
      if (!pdf) {
        cout << "ERROR : component " << m_model->ComponentModel(i)->GetName() << " has null PDF!" << endl;
        return false;
      }      
      vals(i) = pdf->getVal(&normSet);
      sum += m_model->ComponentModel(i)->NEvents()*pdf->getVal(&normSet);
    }
    
    //cout << vals(0) << " " << vals(1) << " " << sum << endl;
    double origWeight = m_data->weight();
    aWeight->setVal(origWeight);
    double sWeight = (v*vals)[m_componentIndex]/sum;
    RooArgSet eventW(*event);
    eventW.add(*aWeight);
    m_sData->add(eventW, origWeight*sWeight);
  }
  
  return true;
}


TTree* HftSPlot::AddWeight(TTree& tree, const TString& varName)
{
  if (m_sData->numEntries() != tree.GetEntries()) {
    cout << "ERROR : numbers of events don't match" << endl;
    return 0;
  }
  double weight;
  TBranch* branch = tree.Branch(varName, &weight);
  
  for (int k = 0; k < m_sData->numEntries(); k++) {
    m_sData->get(k);
    weight = m_sData->weight();
    branch->Fill();
  }
  return &tree;

//   TTree* newTree = tree.CloneTree(0);
//   double weight;
//   newTree->Branch(varName, &weight);
//   
//   for (int k = 0; k < m_sData->numEntries(); k++) {
//     m_sData->get(k);
//     tree.GetEntry(k);
//     weight = m_sData->weight();
//     newTree->Fill();
//   }
//   return newTree;
}
