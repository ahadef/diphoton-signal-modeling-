#include "HfitterModels/HftFactoryPdfBuilder.h"

#include "RooWorkspace.h"
#include "RooAbsPdf.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


void HftFactoryPdfBuilder::Setup(const char* factory_expr)
{
  m_expr = factory_expr;
}


RooAbsPdf* HftFactoryPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsArg* arg = workspace.factory(m_expr);
  RooAbsPdf* pdf = dynamic_cast<RooAbsPdf*>(arg);
  if (!pdf) cout << "ERROR: expression " << m_expr << " does not create a RooAbsPdf, aborting" << endl;
  return pdf;
}
