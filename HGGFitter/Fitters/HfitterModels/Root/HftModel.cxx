#include "HfitterModels/HftModel.h"

#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftData.h"
#include "HfitterModels/HftTools.h"

#include "RooWorkspace.h"
#include "RooSimultaneous.h"
#include "RooAddPdf.h"
#include "RooPlot.h"
#include "RooHist.h"
#include "RooFitResult.h"
#include "RooDataSet.h"
#include "RooStats/ModelConfig.h"

#include "TAxis.h"
#include "TLatex.h"

#include <algorithm>

#include <fstream>
#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


std::map<TString, HftModel*>* HftModel::m_modelRegistry = new std::map<TString, HftModel*>();


HftModel::HftModel(RooWorkspace& workspace, TFile* workspaceFile, RooAbsPdf& pdf,
                   const RooArgList& observables, const RooArgList& auxObservables,
                   const std::vector<HftModel*>& componentModels, const std::vector<HftModel*>& categoryModels,
                   const std::vector<HftConstraint*>& constraints, RooAbsReal* norm, const TString& name, bool ownPdf, bool ownWS)
  : m_name(name), m_workspace(&workspace), m_workspaceFile(workspaceFile), m_ownWS(ownWS), m_pdf(&pdf), m_ownPdf(ownPdf),
    m_mainCat(0), m_norm(norm), m_extPdf(0), m_componentModels(componentModels), m_categoryModels(categoryModels), m_constraints(constraints)
{
  //cout << "Creating model " << GetName() << ", owns ? " << m_ownWS << endl;
  RegisterModel(GetName(), *this);
  if (m_norm) m_extPdf = new RooAddPdf(m_name, "", *m_pdf, *m_norm); else m_extPdf = m_pdf;
  RooArgList workspacePars;
  workspacePars.add(workspace.allVars());
  workspacePars.add(workspace.allFunctions());
  RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(&pdf);
  if (simPdf) m_mainCat = (RooAbsCategoryLValue*)&simPdf->indexCat();
  RooArgSet* thesePar = m_extPdf->getObservables(workspacePars);   m_parameters     .add(*thesePar); delete thesePar;
  RooArgSet* theseFun = (RooArgSet*)m_extPdf->getComponents()->selectCommon(workspace.allFunctions()); m_parameters.add(*theseFun); delete theseFun;
  RooArgSet* theseObs = m_extPdf->getObservables(observables);     m_observables    .add(*theseObs); delete theseObs;
  RooArgSet* theseAux = m_extPdf->getObservables(auxObservables);  m_auxObservables .add(*theseAux); delete theseAux;
}


HftModel::~HftModel()
{
  //cout << "delete components " << GetName() << " = " << m_componentModels.size() << endl;
  for (std::vector<HftModel*>::iterator component = m_componentModels.begin(); component != m_componentModels.end(); component++)
    delete *component;
  //cout << "delete cats " << GetName() << " = " << m_categoryModels.size() << endl;
  for (std::vector<HftModel*>::iterator category = m_categoryModels.begin(); category != m_categoryModels.end(); category++)
    delete *category;
  //cout << "delete constraints " << GetName() << " = " << m_constraints.size() << endl;
  for (std::vector<HftConstraint*>::iterator constraint = m_constraints.begin(); constraint != m_constraints.end(); constraint++)
    delete *constraint;
  //if (m_ownPdf)  cout << "delete PDF " << GetName() << " " << m_ownPdf << endl;
  if (m_ownPdf) delete m_pdf;
//   if (m_ownWS) 
//     cout << "Deleting ws for " << GetName() << endl;
//   else 
//     cout << "skipping ws delete for " << GetName() << endl;    
  //if (m_ownWS) cout << "delete WS " << GetName() << " " << m_ownWS << endl;
  if (m_ownWS) delete m_workspace;
  //if (m_workspaceFile) cout << "delete WSfile " << GetName() << " " << m_workspaceFile << endl;
  if (m_workspaceFile) delete m_workspaceFile;
  //if (m_norm) cout << "delete norm " << GetName() << " " << m_norm << endl;
  if (m_norm) delete m_extPdf;
  //cout << "deregister" << endl;
  DeregisterModel(*this);
  //cout << "delete OK" << endl;
}


RooRealVar* HftModel::Var(const TString& name)  
{ 
  return Workspace()->var(name); 
}


RooCategory* HftModel::Cat(const TString& name)
{ 
  return Workspace()->cat(name); 
}


RooArgList HftModel::FreeParameters() const
{
  RooArgList freeList;
  for (int i = 0; i < m_parameters.getSize(); i++) {
    if (m_parameters.at(i)->isConstant()) continue;
    if (!m_parameters.at(i)->isFundamental()) continue;
    if (m_observables.find(m_parameters.at(i)->GetName())) continue;
    freeList.add(*m_parameters.at(i));
  }
  return freeList;
}


RooArgList HftModel::Pars(const TString& par1, const TString& par2, const TString& par3, const TString& par4,
                          const TString& par5, const TString& par6, const TString& par7, const TString& par8) const
{
  RooArgList pars;
  if (par1 != "" && Parameter(par1)) pars.add(*Parameter(par1));
  if (par2 != "" && Parameter(par2)) pars.add(*Parameter(par2));
  if (par3 != "" && Parameter(par3)) pars.add(*Parameter(par3));
  if (par4 != "" && Parameter(par4)) pars.add(*Parameter(par4));
  if (par5 != "" && Parameter(par5)) pars.add(*Parameter(par5));
  if (par6 != "" && Parameter(par6)) pars.add(*Parameter(par6));
  if (par7 != "" && Parameter(par7)) pars.add(*Parameter(par7));
  if (par8 != "" && Parameter(par8)) pars.add(*Parameter(par8));
  return pars;
}


RooRealVar* HftModel::Observable(const TString& name) const
{
  return (RooRealVar*)m_observables.find(name);
}


RooRealVar* HftModel::AuxObservable(const TString& name)
{
  return (RooRealVar*)m_auxObservables.find(name);
}


RooArgList HftModel::ObservablesAndCats() const
{
  RooArgList obs = Observables();
  obs.add(Workspace()->allCats());
  return obs;
}


HftModel* HftModel::ComponentModel(const TString& name)
{
  return HftAbsParameters::FindByName(m_componentModels, name);
}


HftModel* HftModel::CategoryModel(const TString& name)
{
  return HftAbsParameters::FindByName(m_categoryModels, name);
}


RooAbsData* HftModel::CategoryData(RooAbsData& data, unsigned int catIndex)
{
  return HftData::SelectCategory(data, *MainCategory(), MainCategory()->lookupType(catIndex)->GetName());
}


RooAbsData* HftModel::CategoryData(RooAbsData& data, const TString& catName)
{
  return HftData::SelectCategory(data, *MainCategory(), catName);
}


HftConstraint* HftModel::Constraint(const TString& name)
{
  return HftAbsParameters::FindByName(m_constraints, name);
}


RooArgList HftModel::ConstrainedParameters() const
{
  RooArgList cps;
  RooArgSet params(Parameters());
  for (unsigned int i = 0; i < NConstraints(); i++) {
    HftAbsParameters::AddUnique(m_constraints[i]->ConstrainedParameters(), cps);
  }
  return cps;
}


double HftModel::NEvents() const
{
  double nEvents = 0;
  if (NCategories() > 0) {
    for (unsigned int i = 0; i < NCategories(); i++)
      nEvents += CategoryModel(i)->NEvents();
    return nEvents;
  }

  if (NComponents() > 0) {
  for (unsigned int i = 0; i < NComponents(); i++)
    nEvents += ComponentModel(i)->NEvents();
  return nEvents;
  }
  return m_extPdf->expectedEvents(RooArgSet());
}


double HftModel::NEvents(const TString& range) const
{
  RooAbsReal* integral = m_pdf->createIntegral(Observables(), ObservablesAndCats(), range);
  double nRange = NEvents()*integral->getVal();
  delete integral;
  return nRange;
}


bool HftModel::LoadState(const HftParameterStorage& state, bool values, bool constness)
{
  if (!HftParameterSet(Parameters()).LoadState(state, values, constness, true)) return false;
  return true;
}


bool HftModel::SaveState(HftParameterStorage& state)
{
  state.Clear();
  if (!HftParameterSet(Parameters()).SaveState(state)) return false;
  return true;
}


HftParameterStorage HftModel::CurrentState()
{
  HftParameterStorage state;
  if (!SaveState(state)) return HftParameterStorage();
  return state;
}


bool HftModel::RandomizeConstraints()
{
  for (unsigned int i = 0; i < NConstraints(); i++)
    if (!Constraint(i)->Randomize()) return false;
  return true;
}


RooDataSet* HftModel::GenerateEvents(const TString& genOptions)
{
  if (genOptions.Index("NoRandom") < 0 && !RandomizeConstraints()) return 0;
  bool verbose = (genOptions.Index("Verbo") >= 0);

  RooArgList vars = Observables();
  if (m_mainCat) vars.add(*m_mainCat);

  RooDataSet* data = 0;
  if (ExtPdf()->canBeExtended()) {
    data = ExtPdf()->generate(vars, genOptions.Index("NoPoisson") < 0 ? RooFit::Extended() : RooCmdArg::none());
    if (verbose)
    cout << "HftModel::GenerateEvents : generated " << data->numEntries() << " events, from "
         << NEvents() << " requested" << endl;
  }
  else {
    data = ExtPdf()->generate(vars, 1);
  }
  if (NCategories() > 0) {
    int i0 = m_mainCat->getBin();
    for (int i = 0; i < m_mainCat->numBins(""); i++) {
      m_mainCat->setBin(i);
      if (verbose) {
        cout << "in category state '" << m_mainCat->getLabel() << "' : ";
        RooAbsData* dataBin = data->reduce(Form("%s == %d", m_mainCat->GetName(), i));
        cout << dataBin->numEntries() << endl;
        delete dataBin;
      }
    }
    m_mainCat->setBin(i0);
  }
  if (data && genOptions.Index("Aux") >= 0) data->addColumns(AuxObservables());
  return data;
}


RooDataHist* HftModel::GenerateBinned(const TString& genOptions)
{
  if (genOptions.Index("NoRandom") < 0 && !RandomizeConstraints()) return 0;
  bool verbose = (genOptions.Index("Verbo") >= 0);

  RooArgList vars = Observables();
  if (m_mainCat) vars.add(*m_mainCat);

  RooDataHist* data = ExtPdf()->generateBinned(vars, genOptions.Index("NoPoisson") < 0 ? RooFit::Extended() : RooCmdArg::none());
  if (verbose)
    cout << "HftModel::GenerateBinned : generated " << data->numEntries() << " events, from "
         << NEvents() << " requested" << endl;
  return data;
}


bool HftModel::FillAsimov(RooDataSet*& dataset, const RooArgList& observables, const RooArgList& others)
{
  if (MainCategory()) {
    for (unsigned int i = 0; i < NCategories(); i++) {
      MainCategory()->setLabel(CategoryModel(i)->GetName());
      if (!CategoryModel(i)->FillAsimov(dataset, observables, others)) return false;
    }
  }
  else {
    if (!HftData::FillAsimov(dataset, *ExtPdf(), observables, others)) return false;
  }
  return true;
}


RooDataSet* HftModel::GenerateAsimov()
{
  HftParameterStorage state;
  SaveState(state);
  RooDataSet* dataset = 0;
  if (!FillAsimov(dataset, Observables(), Workspace()->allCats())) return 0;
  LoadState(state);
  return dataset;
}


RooFitResult* HftModel::Fit(RooAbsData& data, const Options& options, double* nll, int* status)
{
//   if (options.Find("Scan")) { // FIXME
//     Options opts = options;
//     double offsetScan;
//     opts.Remove("Scan"); //FIXME
//     HftParameterStorage state0 = CurrentState();
//     cout << "INFO: Scan(1) - performing fit with original initial state" << endl;
//     RooFitResult* resultOrig = Fit(data, opts, likelihoodOffset);
//     HftParameterStorage stateOrig = CurrentState();
//     LoadState(state0);
//     Var("dSig")->setVal(0.5);
//     if (!Var("xs")->isConstant()) Var("xs")->setVal(10);
//     cout << "INFO: Scan(2) - performing fit with dSig=1 initial state" << endl;
//     RooFitResult* resultScan = Fit(data, opts, &offsetScan);
//     cout << "original : " << resultOrig->minNll() + *likelihoodOffset << endl;
//     cout << "scanned  : " << resultScan->minNll() + offsetScan << endl;
//     cout << "scan gain : " << resultOrig->minNll() + *likelihoodOffset - resultScan->minNll() - offsetScan << endl;
//     if (!resultScan) {
//       cout << "WARNING: scan fit failed, returning original fit" << endl;
//       LoadState(stateOrig);
//       return resultOrig;
//     }
//     if (!resultOrig) {
//       cout << "WARNING: original fit failed, returning scanned fit" << endl;
//       *likelihoodOffset = offsetScan;
//       return resultScan;
//     }
//     double deltaNLLScan = resultScan->minNll() + offsetScan - (resultOrig->minNll() + *likelihoodOffset);
//     if (deltaNLLScan < 0) {
//       cout << "INFO: Scan - Choosing result with hand-scanned parameter (" << Form("deltaNLL = %g", deltaNLLScan) << ")" << endl;
//       *likelihoodOffset = offsetScan;
//       return resultScan; 
//     }
//     cout << "INFO: Scan - Choosing result from original fit (" << Form("deltaNLL = %g", deltaNLLScan) << ")" << endl;
//     LoadState(stateOrig);
//     return resultOrig;
//   }
  
  if (options.Find("Robust")) {
    Options opt1 = options;
    opt1.Remove("Robust");
    // try1
    if (options.Verbosity() > 1) cout << "HftModel::Fit : doing robust fit, try1" << endl;
    RooFitResult* result1 = Fit(data, opt1, nll, status);
    if (result1->status() == 0 || (options.Find("NotPosDefOK") && result1->status() == 1)) return result1;
    delete result1;
    // try2
    if (options.Verbosity() > 1) cout << "HftModel::Fit : doing robust fit, try2" << endl;
    Options opt2 = opt1;
    opt2.Add(RooFitOpt(RooFit::Strategy(2)));
    RooFitResult* result2 = Fit(data, opt2, nll, status);
    if (result2->status() == 0 || (options.Find("NotPosDefOK") && result2->status() == 1)) return result2;
    delete result2;
    if (options.Verbosity() > 1) cout << "HftModel::Fit : doing robust fit, try3" << endl;
    Options optS = opt1;
    optS.Remove("RooFit::Minimizer = 0 Minuit2");
    optS.Add(RooFitOpt(RooFit::Strategy(2)));
    optS.Add(RooFitOpt(RooFit::Minimizer("Minuit2", "Simplex")));
    Fit(data, optS);
    RooFitResult* result3 = Fit(data, opt2, nll, status);
    if (result3->status() == 0 || (options.Find("NotPosDefOK") && result3->status() == 1)) return result3;
    delete result3;
    if (options.Verbosity() > 1) cout << "HftModel::Fit : doing robust fit, try4" << endl;
    Fit(data, optS);
    RooFitResult* result4 = Fit(data, opt2, nll, status);
    if (result4->status() == 0 || (options.Find("NotPosDefOK") && result4->status() == 1)) return result4;
    if (options.Verbosity() > 1) cout << "HftModel::Fit : doing robust fit, try5" << endl;
    Fit(data, optS);
    RooFitResult* result5 = Fit(data, opt2, nll, status);
    if (result4->status() == 0 || (options.Find("NotPosDefOK") && result5->status() == 1)) return result5;
    cout << "ERROR: HftModel::Fit : robust fit failed!" << endl;
    return result5;
  }
  
  RooLinkedList cmdListWithConstraints;
  options.Process(cmdListWithConstraints, *this);
  RooCmdArg constraintArg;
  if (m_auxObservables.getSize() > 0) {
    constraintArg = RooFit::Constrain(m_auxObservables);
    cmdListWithConstraints.Add(&constraintArg);
  }
  
  if (options.Verbosity() > 1) {
    cout << options.Str() << endl;
    if (options.Verbosity() > 2) Pdf()->Print();
    RooMsgService::instance().setSilentMode(false);
    RooMsgService::instance().setGlobalKillBelow(RooFit::INFO);
  }
  else {
    RooMsgService::instance().setSilentMode(true);
    RooMsgService::instance().setGlobalKillBelow(RooFit::ERROR);
  }
  if (!Pdf()->canBeExtended()) {
    RooCmdArg* arg = (RooCmdArg*)cmdListWithConstraints.FindObject("Extended");
    if (arg) {
      if (options.Verbosity()) cout << "INFO : removing Extended() options since PDF is not extendable" << endl;
      cmdListWithConstraints.Remove(arg);
    }
  }
  
  if (options.Verbosity() > 1) cmdListWithConstraints.Print("v");
  RooFitResult* result = 0;
  if (options.Verbosity() > 2) PrintPdf();
  
  RooDataHist* hist = 0;
  if (options.Find("Chi2")) {
    hist = dynamic_cast<RooDataHist*>(&data);
    if (!hist) cout << "WARNING : ignoring Chi2 option for unbinned data" << endl;
  }
  bool noFreePars = false;
  if (FreeParameters().getSize() > 0) {
    result = hist ? 
    Pdf()->chi2FitTo(*hist, cmdListWithConstraints) :
    Pdf()->fitTo(data, cmdListWithConstraints);
  }
  else {
    result = 0;
    noFreePars = true;
    if (options.Verbosity() >= 2) cout << "HftModel::Fit : no free parameters in the model" << endl;
  }
  if (result && result->status() < 2 && options.Find("Offcheck") && fabs(result->minNll()) > 1) {
    cout << "HftModel::Fit : repeating fit due to high minNll = " << result->minNll() << endl;
    RooAbsReal* nllVar = CreateNLL(data, options);
    double offset = nllVar->getVal() - result->minNll();
    if (options.Verbosity() >= 2) 
      cout << "HftModel::Fit : offset so far = " << Form("%.20g", offset) << " " << Form("%.20g", nllVar->getVal()) << endl;
    delete result;
    Options opts = options;
    opts.Remove("Offcheck");
    result = Fit(data, opts, nll, status);
  }
  if (nll) {
    RooAbsReal* nllVar = CreateNLL(data, options);
    *nll = nllVar->getVal();
    delete nllVar;
  }
  if (status) {
    if (noFreePars) 
      *status = 0;
    else 
      *status = result ? result->status() : -1;
  }
  
  double finalNLL =  nll ? *nll : result ? result->minNll() : -1;
  int finalStatus = status ? *status : result ? result->status() : -1;
  if (options.Verbosity() >= 1 && !options.Find("Robust")) 
    cout << Form("final NLL = %.5f, status = %d", finalNLL, finalStatus) << endl;
  
  return result;
}


RooAbsReal* HftModel::CreateNLL(RooAbsData& data, const Options& options)
{
  RooLinkedList cmdList;
  Options pruned = options;
  pruned.Remove("MinosVar*");
  pruned.Remove("RooFit::Minos*");
  pruned.Remove("RooFit::Hesse*");
  pruned.Remove("RooFit::Save*");
  pruned.Remove("RooFit::SumW2Error*");
  pruned.Remove("RooFit::OffsetLikelihood*");
  pruned.Remove("RooFit::Minimizer*");
  pruned.Remove("RooFit::Strategy*");
  if (!Pdf()->canBeExtended()) pruned.Remove("Extended");

  //cout << "Pruned: " << pruned.Str() << endl;
  pruned.Process(cmdList, *this);
  RooCmdArg constraintArg;
  if (m_auxObservables.getSize() > 0) {
    constraintArg = RooFit::Constrain(m_auxObservables);
    cmdList.Add(&constraintArg);
  }
  return Pdf()->createNLL(data, cmdList);
}


RooPlot* HftModel::Plot(const TString& varName, RooAbsData& data, const Options& options, 
                        HftModel* other, RooFitResult* fitResult) const
{
  std::vector<HftModel*> others;
  if (other) others.push_back(other);
  return Plot(varName, data, options, others, fitResult);
}


RooPlot* HftModel::Plot(const TString& varName, RooAbsData& data, const Options& options, 
                        const std::vector<HftModel*>& others, RooFitResult* fitResult) const
{
  RooRealVar* var = Observable(varName);
  if (!var) {
    cout << "PlotVar : Variable '" << varName
        << "' is not a (non-category) dependent" << endl;
    return 0;
  }
  RooPlot* frame = var->frame();
  TString pdfPlotName = varName + "_pdf", dataPlotName = varName + "_data";
  
  RooCmdArg dataScaleArg;
  PlotErrorsOpt* errorOpt = dynamic_cast<PlotErrorsOpt*>(options.Find("PlotErrors"));
  StrArgOpt* dataScaleOpt = dynamic_cast<StrArgOpt*>(options.Find("DataScale"));
  RooAbsData::ErrorType errorType = errorOpt ? errorOpt->ErrorType() : RooAbsData::Auto;  
  if (dataScaleOpt) dataScaleArg = RooFit::Rescale(dataScaleOpt->Arg(0).Atof()); 
  
  data.plotOn(frame, RooFit::Name(dataPlotName), RooFit::DataError(errorType), dataScaleArg);
  frame->getHist(dataPlotName)->SetMarkerStyle(20);
  frame->getHist(dataPlotName)->SetMarkerSize(0.6);

  // superimpose fit result
  // average over _other_ variables
  RooCmdArg projArg;
  RooDataHist* projData = 0;

  //RooArgSet allButTheProjectedOne(Ca(), "allButTheProjectedOne");
  //allButTheProjectedOne.remove( *var );

  RooArgList cats = Workspace()->allCats();
  cats.add(Workspace()->allCatFunctions());

  bool binned = options.Find("Binned");
  
  if (!binned || data.Class() == RooDataHist::Class()) {
    // cout << "Projection: " << endl;
    //allButTheProjectedOne.Print("v");
    //projArg = RooFit::ProjWData( allButTheProjectedOne, data);
    projArg = RooFit::ProjWData(cats, data);
 }
  else {
    RooArgList vars = cats;
    vars.add(*var);
    projData = new RooDataHist("projData", "projData", vars, data);
    projArg = RooFit::ProjWData(cats, *projData );
  }

  RooCmdArg rangeArg;
  RangeOpt* rangeOpt = dynamic_cast<RangeOpt*>(options.Find("Range"));
  if (rangeOpt) {
    cout << "Using ranges " << rangeOpt->ArgStr() << endl;
    rangeArg = RooFit::Range(rangeOpt->ArgStr());
  }
  RooCmdArg lineColorArg;
  StrArgOpt* lineColorOpt = dynamic_cast<StrArgOpt*>(options.Find("LineColor"));
  if (lineColorOpt) lineColorArg = RooFit::LineColor(lineColorOpt->Arg(0).Atoi());
  
  PlotScaleOpt* scaleOpt = dynamic_cast<PlotScaleOpt*>(options.Find("PlotScale"));
  RooAbsReal::ScaleType scaleType = scaleOpt ? scaleOpt->ScaleType() : RooAbsReal::Relative;
  double scale = scaleOpt ? scaleOpt->Scale() : -1;
  if (scale < 0) {
    scale = (scaleType == RooAbsReal::NumEvent ? NEvents() : 1);
    //cout << "Using overall scale " << scale << " " << scaleType << endl;
  }
  
  RooCmdArg precisionArg;
  StrArgOpt* precisionOpt = dynamic_cast<StrArgOpt*>(options.Find("Precision"));
  if (precisionOpt) precisionArg = RooFit::Precision(precisionOpt->Arg(0).Atof()); 
  
  ExtPdf()->plotOn(frame, RooFit::Name(pdfPlotName), projArg, RooFit::Normalization(scale, scaleType), 
                   rangeArg, lineColorArg, precisionArg);
  unsigned int i = 0;
  static int colors[3] = { 2, 8, 6 };
  for (std::vector<HftModel*>::const_iterator model = others.begin(); model != others.end(); model++, i++) {
    if (options.Verbosity() >= 1) cout << "INFO : plotting model " << (*model)->GetName() << endl;
    (*model)->ExtPdf()->plotOn(frame, RooFit::Name(pdfPlotName + Form("_other%d", i+1)),
                            projArg, RooFit::LineColor(colors[i]), RooFit::Normalization(1, scaleType), rangeArg, 
                            lineColorArg, precisionArg);
  }
  StrArgOpt* compOpt = dynamic_cast<StrArgOpt*>(options.Find("Component"));
  for (unsigned int i = 0; i < NComponents(); i++) {
    TString name = HftAbsParameters::StripCategories(ComponentModel(i)->GetName());
    double cscale = scaleOpt ? scaleOpt->Scale() : -1;
    if (cscale < 0) {
      cscale = (scaleType == RooAbsReal::NumEvent ? ComponentModel(i)->NEvents() : 1);
      //cout << "For component " << name << ", using scale " << cscale << " " << scaleType << endl;
    }
    if (compOpt && std::find(compOpt->Args().begin(), compOpt->Args().end(), name) == compOpt->Args().end()) continue;
    if (options.Verbosity() >= 1) cout << "INFO : plotting component " << name << " (selecting " << (compOpt ? compOpt->ArgStr() : "everything") <<  ")" << endl;
    ExtPdf()->plotOn(frame, RooFit::Name(pdfPlotName + "_" + name), projArg, RooFit::Components(name + "*"), rangeArg,
                  RooFit::LineStyle( kDashed ), RooFit::Normalization(cscale, scaleType), lineColorArg, precisionArg);
  }
  if (projData) delete projData;
  frame->SetTitle("");
  frame->Draw();
  
  int fittedDofs = -999;
  options.GetInt("FittedDofs", fittedDofs);
  
  if (fittedDofs > -999) {
   if (fittedDofs < 0) fittedDofs = FreeParameters().getSize();  
   double chi2 = frame->chiSquare(pdfPlotName, dataPlotName, fittedDofs);
   unsigned int nDof = frame->GetNbinsX() - fittedDofs;
   TLatex text;
   text.SetNDC();
   double x,y;
   int corner = 1;
   options.GetInt("LegendPos", corner);
   if (corner == 1) {x = 0.2; y = 0.8; } 
   else if (corner == 2) {x = 0.2; y = 0.2; } 
   else if (corner == 3) {x = 0.6; y = 0.8; } 
   else {x = 0.6; y = 0.2; }
   cout << corner << " " << x << " " << y << endl;
   if (corner > 0) text.DrawLatex(x, y, Form("#chi^{2}/n = %5.1f/%d", chi2*nDof, nDof));
   cout << "chi2/n = " << chi2*nDof << "/" << nDof << endl;
  }

  StrArgOpt* statBandOpt = dynamic_cast<StrArgOpt*>(options.Find("StatBand"));
  if (statBandOpt) {
    if (!fitResult) {
      cout << "WARNING: cannot draw stat error band since fit result was not provided" << endl;
      return frame;
    }
    int nToys = statBandOpt->NArgs() > 0 ? statBandOpt->Arg(0).Atoi() : 1000;
    double scaleFactor = 1;
    if (scale > 0 && scaleType == RooAbsReal::NumEvent) scaleFactor = scale/NEvents(); // should support other cases too
    // Clone self for internal use
    RooAbsPdf* cloneFunc = (RooAbsPdf*)ExtPdf()->cloneTree() ;
    RooArgSet* errorParams = cloneFunc->getObservables(fitResult->floatParsFinal()) ;
    RooAbsPdf* paramPdf = fitResult->createHessePdf(*errorParams);
    RooArgSet* cloneVars = cloneFunc->getObservables(*var);
    RooRealVar* cloneVar = (RooRealVar*)cloneVars->find(varName);
    delete cloneVars;
    
    RooDataSet* asimov = 0;
    !HftData::FillAsimov(asimov, *cloneFunc, *cloneVar, RooArgList());
    RooCurve* cenCurve = new RooCurve();
    for (int i = 0; i < asimov->numEntries(); i++) { 
      const RooArgSet* point = asimov->get(i); 
      cenCurve->addPoint(point->getRealValue(varName), asimov->weight()*scaleFactor);
    }
    cenCurve->SetName(pdfPlotName);
    
    // Generate variation curves with above set of parameter values
    RooDataSet* d = paramPdf->generate(*errorParams, nToys);
    std::vector<RooCurve*> cvec ;
    for (int i = 0; i < d->numEntries(); i++) {
      *errorParams = *d->get(i);
      //d->get(i)->Print("V");
      asimov = 0;
      !HftData::FillAsimov(asimov, *cloneFunc, *cloneVar, RooArgList());
      RooCurve* curve = new RooCurve();
      for (int i = 0; i < asimov->numEntries(); i++) { 
        const RooArgSet* point = asimov->get(i); 
        curve->addPoint(point->getRealValue(varName), asimov->weight()*scaleFactor);
      }
      //for (int i = 0; i < asimov->numEntries(); i++) cout << asimov->get(i)->getRealValue(varName) << ": " << asimov->weight() << endl;
      cvec.push_back(curve);
      delete asimov;
    }
    
    // Generate upper and lower curve points from 68% interval around each point of central curve
    RooCurve* band = cenCurve->makeErrorBand(cvec, 1);
    cenCurve->SetName(pdfPlotName + "_centralvalue");
    frame->addPlotable(cenCurve, "C", true);
    frame->addPlotable(band, "F");
    frame->drawBefore(frame->getObject(0)->GetName(), frame->getCurve()->GetName()); 
    frame->Draw();
    
    // Cleanup 
    delete paramPdf;
    delete cloneFunc;
    for (std::vector<RooCurve*>::iterator i = cvec.begin(); i != cvec.end(); i++) delete *i;
  }
  return frame;
}


bool HftModel::WriteCard(const TString& fileName) const
{
  std::ofstream f(fileName);
  f << endl << "[" << GetName() << "]" << endl;
  if (!HftParameterSet(Parameters()).WriteToStream(f)) return false;
  return true;
}


double HftModel::Likelihood(const TString& componentName)
{
  HftModel* component = ComponentModel(componentName);
  if (!component) return 0;
  return component->Pdf()->getVal();
}


double HftModel::LikelihoodRatio(const TString& componentName)
{
  HftModel* component = ComponentModel(componentName);
  if (!component) return 0;

  double num = 0, denom = 0;

  for (unsigned int i = 0; i < NComponents(); i++) {
    double llh = ComponentModel(i)->Pdf()->getVal();
    if (llh < 0) cout << "Warning: when calculating likelihood ratio, likelihood for component " << ComponentModel(i)->GetName()
                      << " is " << llh << " < 0." << endl;
    denom += llh;
    if (ComponentModel(i) == component) num += llh;
  }

  if (denom == 0) return 1;
  return num/denom;
}


RooWorkspace* HftModel::ExportWorkspace(const TString& fileName, const TString& name, const TString& poiNames, RooAbsData* data,
                                        const TString& mcName, const TString& importCodeDirs)
{
  RooArgSet pois;
  TObjArray* tokens = poiNames.Tokenize(",");
  for (int i = 0; i < tokens->GetEntries(); i++) {
    TString poiName = tokens->At(i)->GetName();
    poiName.ReplaceAll(" ", "");
    RooRealVar* poi = Var(poiName);
    if (!poi) {
      cout << "ERROR : parameter-of-interest " << poiName << " is not part of the model" << endl;
      return 0;
    }
    pois.add(*poi);
  }
  delete tokens;
  std::vector<TString> importDirs;
  tokens = importCodeDirs.Tokenize(":");
  for (int i = 0; i < tokens->GetEntries(); i++) importDirs.push_back(tokens->At(i)->GetName());
  delete tokens;
  RooAbsPdf* pdf = Pdf();
  RooArgSet nuis = FreeParameters();
  nuis.remove(pois);
  RooArgSet globalObservables = AuxObservables();
  RooArgSet observables = Observables();
  //RooAbsData* asimov = GenerateAsimov();

  RooWorkspace* ws = new RooWorkspace(name, "");
  ws->SetName(name);
  ws->import(*pdf);

  ws->defineSet("pois", pois);
  ws->defineSet("nuisanceParameters", nuis);
  ws->defineSet("Observables", observables);
  ws->defineSet("globalObservables", globalObservables);
  //ws->import(*asimov);
  if (data) ws->import(*data);

  RooStats::ModelConfig* config = new RooStats::ModelConfig(mcName, ws);
  config->SetPdf(*ws->pdf(pdf->GetName()));

  config->SetParametersOfInterest(*ws->set("pois"));
  config->SetNuisanceParameters(*ws->set("nuisanceParameters"));
  config->SetObservables(*ws->set("Observables"));
  config->SetGlobalObservables(*ws->set("globalObservables"));

  ws->import(*config);

  if (fileName != "") {
    TString fn = HftData::PrepareDirs(fileName);
    if (importDirs.size() > 0) {
      for (auto dir : importDirs) ws->addClassDeclImportDir(dir);
      ws->importClassCode();
    }
    ws->writeToFile(fn);
  }
  return ws;
}


void HftModel::PrintPdf() const
{
  Print(*Pdf());
}


bool HftModel::RegisterModel(const TString& name, HftModel& model)
{
  if (m_modelRegistry->find(name) != m_modelRegistry->end()) return false;
  (*m_modelRegistry)[name] = &model;
  return true;
}


bool HftModel::DeregisterModel(HftModel& model)
{
  for (std::map<TString, HftModel*>::const_iterator entry = m_modelRegistry->begin(); entry != m_modelRegistry->end();
       entry++)
    if (entry->second == &model) m_modelRegistry->erase(entry);
  return true;
}


HftModel* HftModel::GetModel(const TString& name)
{
  std::map<TString, HftModel*>::iterator model = m_modelRegistry->find(name);
  return (model != m_modelRegistry->end() ? model->second : 0);
}


void HftModel::PrintModels(int maxDepth)
{
  unsigned int i = 0;
  for (std::map<TString, HftModel*>::const_iterator entry = m_modelRegistry->begin(); entry != m_modelRegistry->end();
       entry++, i++) {
    cout << "Model " << i+1 << " : " << entry->first << endl;
    Print(*entry->second->Pdf(), maxDepth);
  }
}


void HftModel::Print(const RooAbsArg& arg, int maxDepth, const TString& indent)
{
  if (maxDepth == 0) return;
  cout << indent; arg.Print();
  TIterator* iter = arg.serverIterator();
  while (RooAbsArg* server = (RooAbsArg*)iter->Next()) Print(*server, maxDepth - 1, indent + "  ");
  delete iter;
}

