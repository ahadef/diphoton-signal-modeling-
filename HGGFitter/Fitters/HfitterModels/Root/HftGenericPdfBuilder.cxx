#include "HfitterModels/HftGenericPdfBuilder.h"

#include "RooGenericPdf.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

void HftGenericPdfBuilder::Setup(const char* dependent, const char* formula,
                                 const char* par1, const char* par2, const char* par3,
                                 const char* par4, const char* par5, const char* par6,
                                 const char* par7, const char* par8, const char* par9)
{
  m_dependent = dependent;
  m_formula = formula;
  if (TString(par1) != "") m_args.push_back(par1);
  if (TString(par2) != "") m_args.push_back(par2);
  if (TString(par3) != "") m_args.push_back(par3);
  if (TString(par4) != "") m_args.push_back(par4);
  if (TString(par5) != "") m_args.push_back(par5);
  if (TString(par6) != "") m_args.push_back(par6);
  if (TString(par7) != "") m_args.push_back(par7);
  if (TString(par8) != "") m_args.push_back(par8);
  if (TString(par9) != "") m_args.push_back(par9);
}


RooAbsPdf* HftGenericPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooArgList pars;
  pars.add(*Dependent(dependents, m_dependent));
  for (unsigned int i = 0; i < m_args.size(); i++) 
    pars.add(Param(workspace, m_args[i]));
  return new RooGenericPdf(name, m_formula, pars);
}

