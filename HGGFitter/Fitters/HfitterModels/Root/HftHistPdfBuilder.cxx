#include "HfitterModels/HftHistPdfBuilder.h"

#include "RooHistPdf.h"
#include "RooDataHist.h"
#include "RooCategory.h"
#include "RooWorkspace.h"
#include "TFile.h"
#include "TH1.h"

#include <map>
#include <string>

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


void HftHistPdfBuilder::Setup(const char* dep1, const char* fileName, const char* key)
{
  m_fileName = fileName;
  m_key = key;
  m_dependents.push_back(dep1);
}


// void HftHistPdfBuilder::Setup(const char* fileName, const char* key, const char* dep1, const char* dep2, const char* dep3)
// {
//   m_fileName = fileName;
//   m_key = key;
//   if (dep1 != TString("")) m_dependents.push_back(dep1);
//   if (dep2 != TString("")) m_dependents.push_back(dep2);
//   if (dep3 != TString("")) m_dependents.push_back(dep3);
// }


RooAbsPdf* HftHistPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  TFile* f = TFile::Open(m_fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR: HftHistPdfBuilder cannot open file " << m_fileName << endl;
    return 0;
  }
  TObject* obj = f->Get(m_key);
  if (!obj) {
    cout << "ERROR: HftHistPdfBuilder cannot open object with key " << m_key << " in file " << m_fileName << endl;
    return 0;
  }
  
  RooArgList vars;
  RooCategory* cat = 0;
  for (std::vector<TString>::const_iterator depName = m_dependents.begin(); depName != m_dependents.end(); depName++) {
    if ((*depName)(0, 4) == "cat:") {
      TString catName = (*depName)(4, depName->Length() - 4);
      cat = dynamic_cast<RooCategory*>(Category(workspace, catName));
      if (!cat) {
        cout << "ERROR : in HftHistPdfBuilder, specified category " << catName << " not found in workspace" << endl;
        return 0;
      }
    }
    else
      vars.add(*Dependent(dependents, *depName));
  }

  RooDataHist* dataHist = dynamic_cast<RooDataHist*>(obj);
  if (!dataHist) {
    if (!cat) {
      TH1* hist = dynamic_cast<TH1*>(obj);
      if (!hist) {
        cout << "ERROR: HftHistPdfBuilder found object of unknown type with key " << m_key << " in file " << m_fileName << endl;
        return 0;
      }
      dataHist = new RooDataHist(TString(GetName()) + "_data", "RooDataHist for PDF " + TString(GetName()), vars, hist);
    }
    else {
      std::map<std::string, TH1*> hists; 
      for (int i = 0; i < cat->numTypes(); i++) {
        TString state = cat->lookupType(i)->GetName();
        TString key = m_key + "_" + state;
        TH1* hist = dynamic_cast<TH1*>(f->Get(key));
        if (!hist) {
          cout << "ERROR: cannot make RooDataHist for category state " <<  state << " : "
               << "histogram not found with key " << key << " in file " << m_fileName << endl;
          return 0;
        }
        hists[std::string(state)] = hist;
      }
      dataHist = new RooDataHist(TString(GetName()) + "_data", "RooDataHist for PDF " + TString(GetName()), vars, *cat, hists);
    }
  }
  workspace.import(*dataHist);
  dataHist = (RooDataHist*)workspace.data(dataHist->GetName());
  if (cat) vars.add(*cat);
  return new RooHistPdf(name, "RooHistPdf", vars, *dataHist);
}
