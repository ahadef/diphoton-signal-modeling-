// $Id: HftTruncPoly.cxx,v 1.1 2008/01/28 19:42:32 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HfitterModels/HftTruncPoly.h"
#include "RooAbsReal.h"
#include "RooArgSet.h"
#include "TMath.h"

#include <iostream>
using std::cout;
using std::endl;

Hfitter::HftTruncPoly::HftTruncPoly( const HftTruncPoly& other, const char* name )
   : RooAbsPdf( other, name ),
     m_x       ( "x",        this, other.m_x ),
     m_coefs   ( "coefficients", this, other.m_coefs ),
     m_expos   ( "exponents",    this, other.m_expos ),
     m_endpoint( "endpoint", this, other.m_endpoint ),
     m_cst     ( "cst",      this, other.m_cst ),
     m_above   (other.m_above)
{}

Hfitter::HftTruncPoly::HftTruncPoly( const char* name, const char* title, 
                                     RooAbsReal& x, const RooArgList& coefList, const RooArgList& expoList, 
                                     RooAbsReal& endpoint, RooAbsReal& cst, bool above)
   : RooAbsPdf( name, title ),
     m_x       ( "x",         "Dependent", this, x ),
     m_coefs   ( "coefficients", "", this ),
     m_expos   ( "exponents",    "", this ),
     m_endpoint( "endpoint",  "Endpoint",  this, endpoint ),
     m_cst     ( "cst",       "Constant",  this, cst ),
     m_above   (above)
{
  if (coefList.getSize() != expoList.getSize()) {
    cout << "HftTruncPoly::HftTruncPoly : coef and exponent lists must have same size!" << endl;
    return;
  }
  
  for (int i = 0; i < coefList.getSize(); ++i) {
    m_coefs.add(*coefList.at(i));
    m_expos.add(*expoList.at(i));
  }
}


Double_t Hfitter::HftTruncPoly::evaluate() const
{
  if ( m_above && m_x > m_endpoint) return m_cst;
  if (!m_above && m_x < m_endpoint) return m_cst;

  double sum = 1;
  
  for (int i = 0; i < m_coefs.getSize(); i++) {
    RooAbsReal* coef = (RooAbsReal*)m_coefs.at(i);
    RooAbsReal* expo = (RooAbsReal*)m_expos.at(i);
    sum += coef->getVal()*TMath::Power(m_x, expo->getVal());
  }
  return sum;
}


Int_t Hfitter::HftTruncPoly::getAnalyticalIntegral( RooArgSet& allVars, RooArgSet& analVars, 
                                                 const char* /*rangeName*/ ) const 
{
   if (matchArgs(allVars, analVars, m_x)) return 1;
   return 0;
}

Double_t Hfitter::HftTruncPoly::analyticalIntegral( Int_t code, const char* rangeName ) const 
{
   switch(code) {
   case 1: 
     Double_t xMin = m_x.min(rangeName);
     Double_t xMax = m_x.max(rangeName);
     Double_t xMin_pol, xMax_pol, xMin_cst, xMax_cst;

     if (m_above) {
       xMin_pol = xMin;
       xMax_pol = TMath::Min(xMax, m_endpoint);
       xMin_cst = TMath::Max(xMin, m_endpoint);
       xMax_cst = xMax;
     }
     else {
       xMin_cst = xMin;
       xMax_cst = TMath::Min(xMax, m_endpoint);
       xMin_pol = TMath::Max(xMin, m_endpoint);
       xMax_pol = xMax;
     }
   
     return integral_pol(xMin_pol, xMax_pol) + integral_cst(xMin_cst, xMax_cst);
   }
   
  assert(0);
  return 0;
}
     

Double_t Hfitter::HftTruncPoly::integral_pol(Double_t xMin, Double_t xMax) const
{
  if (xMax <= xMin) return 0;
  double sum = xMax - xMin;
  
  for (int i = 0; i < m_coefs.getSize(); i++) {
    RooAbsReal* coef = (RooAbsReal*)m_coefs.at(i);
    RooAbsReal* expo = (RooAbsReal*)m_expos.at(i);
    Double_t expVal = expo->getVal() + 1;
    if (expVal == 0) continue;
    sum += coef->getVal()*(TMath::Power(xMax, expVal) - TMath::Power(xMin, expVal))/expVal;
  }
  
  return sum;

}


Double_t Hfitter::HftTruncPoly::integral_cst(Double_t xMin, Double_t xMax) const
{
  if (xMax <= xMin) return 0;
  return m_cst*(xMax - xMin);
}
