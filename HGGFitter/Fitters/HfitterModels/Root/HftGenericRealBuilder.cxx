#include "HfitterModels/HftGenericRealBuilder.h"

#include "RooRealVar.h"
#include "RooCFunction1Binding.h"
#include "RooCFunction2Binding.h"
#include "RooCFunction3Binding.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;


void HftGenericRealBuilder::Setup(const char* par1, const char* par2, const char* par3)
{
  if (TString(par1) != "") m_args.push_back(par1);
  if (TString(par2) != "") m_args.push_back(par2);
  if (TString(par3) != "") m_args.push_back(par3);
}


RooAbsReal* HftGenericRealBuilder::Real(const TString& name, RooWorkspace& workspace) const
{
  std::vector<RooAbsReal*> pars;
  for (unsigned int i = 0; i < m_args.size(); i++) {
    pars.push_back(&Param(workspace, m_args[i]));
  }
  
  if (m_args.size() == 0) return new RooRealVar(name, "", 0);
  if (m_args.size() == 1) return new RooCFunction1Binding<double, double>                (name, "", (RealFunc1Type)m_func, *pars[0]);
  if (m_args.size() == 2) return new RooCFunction2Binding<double, double, double>        (name, "", (RealFunc2Type)m_func, *pars[0], *pars[1]);
  if (m_args.size() == 3) return new RooCFunction3Binding<double, double, double, double>(name, "", (RealFunc3Type)m_func, *pars[0], *pars[1], *pars[2]);
  cout << "ERROR: no class defined yet for a function of " << m_args.size() << " arguments." << endl;
  return 0;
}
