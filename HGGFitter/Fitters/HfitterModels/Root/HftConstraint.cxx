#include "HfitterModels/HftConstraint.h"

#include "HfitterModels/HftTools.h"
#include "RooAbsPdf.h"
#include "RooDataSet.h"

using namespace Hfitter;

HftConstraint::HftConstraint(RooAbsPdf& pdf, const RooAbsPdf& constrainedPdf, const RooArgList& auxObservables)
 : m_pdf(&pdf)
{
  RooArgSet* theseAux = pdf.getObservables(auxObservables); m_auxObservables.add(*theseAux); delete theseAux;
  RooArgSet* overlap = Overlap(pdf, constrainedPdf);
  m_constrainedParameters.add(*overlap);
  delete overlap;
}


bool HftConstraint::Randomize()
{
  RooDataSet* data = Pdf()->generate(m_auxObservables, 1);
  for (int i = 0; i < m_auxObservables.getSize(); i++) 
  dynamic_cast<RooRealVar*>(m_auxObservables.at(i))->setVal(data->get(0)->getRealValue(m_auxObservables.at(i)->GetName()));
  delete data;
  return true;
}

RooArgSet* HftConstraint::Overlap(const RooAbsPdf& pdf, const RooAbsPdf& constrainedPdf)
{
  RooArgSet* constPdfPars = constrainedPdf.getParameters(RooArgSet());
  RooArgSet* overlap = pdf.getObservables(*constPdfPars);
  delete constPdfPars;
  return overlap;
}
