// $Id: HftPdfBuilder.cxx,v 1.13 2008/03/05 23:34:49 nberger Exp $   
// Author: Nicolas Berger

#include "HfitterModels/HftStorableParameters.h"

using namespace Hfitter;

using std::cout;
using std::endl;


bool HftStorableParameters::SaveState(HftParameterStorage& state) const
{
  for (std::vector<HftAbsParameters*>::const_iterator client = m_clients.begin(); client != m_clients.end(); client++) {
    HftStorableParameters* storableClient = dynamic_cast<HftStorableParameters*>(*client);
    if (storableClient && !storableClient->SaveState(state)) return false;
  }
  for (unsigned int i = 0; i < NParameters(); i++) {
    RooRealVar* var = Var(i);
    if (var) state.Save(*var);
    RooCategory* cat = Cat(i);
    if (cat) state.Save(*cat);
  }
  return true;
}


bool HftStorableParameters::LoadState(const HftParameterStorage& state, bool values, bool constness, bool allowPartial)
{
  for (std::vector<HftAbsParameters*>::iterator client = m_clients.begin(); client != m_clients.end(); client++) {
    HftStorableParameters* storableClient = dynamic_cast<HftStorableParameters*>(*client);
    if (storableClient && !storableClient->LoadState(state, values, constness, allowPartial)) return false;
  }
  for (unsigned int i = 0; i < NParameters(); i++) {
    RooRealVar* var = Var(i);
    if (var) { if (!state.Load(*var, values, constness) && !allowPartial) return false; else continue; }
    RooCategory* cat = Cat(i);
    if (cat && !state.Load(*cat) && !allowPartial) return false;
  }
  return true;
}


bool HftStorableParameters::SaveState(const TString& stateName) const
{
  for (std::vector<HftAbsParameters*>::const_iterator client = m_clients.begin(); client != m_clients.end(); client++) {
    HftStorableParameters* storableClient = dynamic_cast<HftStorableParameters*>(*client);
    if (storableClient && !storableClient->SaveState(stateName)) return false;
  }    
  HftParameterStorage& state = m_savedStates[stateName];
  return SaveState(state);
}


bool HftStorableParameters::LoadState(const TString& stateName, bool values, bool constness)
{
  for (std::vector<HftAbsParameters*>::iterator client = m_clients.begin(); client != m_clients.end(); client++) {
    HftStorableParameters* storableClient = dynamic_cast<HftStorableParameters*>(*client);
    if (storableClient && !storableClient->LoadState(stateName, values, constness)) return false;
  }
  const HftParameterStorage* state = SavedState(stateName);
  if (!state) return false;
  return LoadState(*state, values, constness);
}


const HftParameterStorage* HftStorableParameters::SavedState(const TString& stateName) const
{
  std::map<TString, HftParameterStorage>::const_iterator state = m_savedStates.find(stateName);
  if (state == m_savedStates.end()) {
    cout << "ERROR : No saved state with name '" << stateName << "' is defined. Saved states are" << endl;
    PrintSavedStates();
    return 0;
  }
  return &state->second;
}


bool HftStorableParameters::SavedValue(const RooRealVar& var, double& val, const TString& stateName) const 
{ 
  for (std::vector<HftAbsParameters*>::const_iterator client = m_clients.begin(); client != m_clients.end(); client++) {
    const HftStorableParameters* storableClient = dynamic_cast<const HftStorableParameters*>(*client);
    if (storableClient && !storableClient->SavedValue(var, val, stateName)) return true;
  }

  return SavedState(stateName)->Value(var, val); 
}


void HftStorableParameters::PrintSavedStates() const
{
  for (std::map<TString, HftParameterStorage>::const_iterator state = m_savedStates.begin();
       state != m_savedStates.end(); state++)
    cout << state->first << endl;
}

