#ifdef __CINT__

#include <vector>

#include "HfitterModels/Options.h"
#include "HfitterModels/HftAbsParameters.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftParameterStorage.h"
#include "HfitterModels/HftStorableParameters.h"
#include "HfitterModels/HftRealMorphVar.h"
#include "HfitterModels/HftDatacardReader.h"
#include "HfitterModels/HftInstance.h"
#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HfitterModels/HftAbsNodeBuilder.h"
#include "HfitterModels/HftAbsRealBuilder.h"
#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftConstraint.h"
#include "HfitterModels/HftModelBuilder.h"
#include "HfitterModels/HftModelSplitter.h"
#include "HfitterModels/HftData.h"
#include "HfitterModels/HftDataHist.h"
#include "HfitterModels/HftCountingPdfBuilder.h"
#include "HfitterModels/HftHistPdfBuilder.h"
#include "HfitterModels/HftFactoryPdfBuilder.h"
#include "HfitterModels/HftWorkspacePdfBuilder.h"
#include "HfitterModels/HftGenericRealBuilder.h"
#include "HfitterModels/HftTemplatePdfBuilder.h"
#include "HfitterModels/HftGenericPdfBuilder.h"
#include "HfitterModels/HftMultiGaussianBuilder.h"
#include "HfitterModels/HftPolyPdfBuilder.h" 
#include "HfitterModels/HftDoubleGaussian.h" 
#include "HfitterModels/HftPolyExp.h" 
#include "HfitterModels/HftFermiPolyExp.h" 
#include "HfitterModels/HftDeltaPdf.h" 
#include "HfitterModels/HftPeggedPoly.h" 
#include "HfitterModels/HftTruncPoly.h" 
#include "HfitterModels/FlexibleInterpVarMkII.h" 
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftSPlot.h"
#include "RooGaussian.h"
#include "RooUniform.h"
#include "RooLandau.h"

// for compabilibility
#include "HfitterModels/HftModelBuilderV4.h"
#include "HfitterModels/HftDatacardReaderV4.h"
#include "HfitterModels/HftModelSplitterV4.h"


#pragma link C++ namespace Hfitter;

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclass;


#pragma link C++ class std::map<TString,std::vector<TString> >+;

#pragma link C++ class Hfitter::HftAbsOption+;
#pragma link C++ class Hfitter::RooFitOpt+;
#pragma link C++ class Hfitter::Minos+;
#pragma link C++ class Hfitter::Range+;
#pragma link C++ class Hfitter::NoDefaults+;
#pragma link C++ class Hfitter::StrOpt+;
#pragma link C++ class Hfitter::Options+;

#pragma link C++ class Hfitter::HftAbsParameters+;
#pragma link C++ class Hfitter::HftParameterSet+;
#pragma link C++ class Hfitter::HftParameterStorage+;
#pragma link C++ class std::vector<Hfitter::HftParameterStorage>+;
#pragma link C++ class Hfitter::HftStorableParameters+;
#pragma link C++ class Hfitter::HftRealMorphVar+;
#pragma link C++ class Hfitter::HftPdfBuilderSpec+;
#pragma link C++ class Hfitter::HftFunctionBuilderSpec+;
#pragma link C++ class Hfitter::HftDatacardReader+;
#pragma link C++ class Hfitter::HftInstance+;
#pragma link C++ class Hfitter::HftAbsPdfBuilder+;
#pragma link C++ class Hfitter::HftAbsNodeBuilder+;
#pragma link C++ class Hfitter::HftAbsRealBuilder+;

#pragma link C++ class Hfitter::HftModel+;
#pragma link C++ class Hfitter::HftConstraint+;

#pragma link C++ class Hfitter::HftModelBuilder+;
#pragma link C++ class Hfitter::HftModelSplitter+;
#pragma link C++ class Hfitter::HftData+;
#pragma link C++ class Hfitter::HftDataHist+;

#pragma link C++ class Hfitter::HftCountingPdfBuilder+;
#pragma link C++ class Hfitter::HftHistPdfBuilder+;
#pragma link C++ class Hfitter::HftWorkspacePdfBuilder+;
#pragma link C++ class Hfitter::HftFactoryPdfBuilder+;
#pragma link C++ class Hfitter::HftGenericRealBuilder+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooGaussian, 2>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooBifurGauss, 3>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooUniform, 0>+;
#pragma link C++ class Hfitter::HftGenericPdfBuilder+;
#pragma link C++ class Hfitter::HftPolyPdfBuilder+;

#pragma link C++ class Hfitter::HftMultiGaussianBuilder+;

#pragma link C++ class Hfitter::HftDoubleGaussian+; 
#pragma link C++ class Hfitter::HftPolyExp+; 
#pragma link C++ class Hfitter::HftFermiPolyExp+; 
#pragma link C++ class Hfitter::HftDeltaPdf+; 
#pragma link C++ class Hfitter::HftPeggedPoly+; 
#pragma link C++ class Hfitter::HftTruncPoly+; 
#pragma link C++ class Hfitter::HftSPlot+;
#pragma link C++ class FlexibleInterpVarMkII+;

#pragma link C++ class std::vector<Hfitter::HftModel*>+;

#pragma link C++ enum Hfitter::ModelType+;

// These statements need the RooGaussian, etc. headers to be included, which RootCore doesn't do by default (although they are listed above). For now work around by including them in HftDoubleGaussian.h
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooGaussian, 2>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<Hfitter::HftDoubleGaussian, 5>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooBifurGauss, 3>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooLandau, 2>+;
#pragma link C++ class Hfitter::HftTemplatePdfBuilder<RooUniform, 0>+;

// for compatibility
#pragma link C++ class Hfitter::HftDatacardReaderV4+;
#pragma link C++ class Hfitter::HftModelBuilderV4+;
#pragma link C++ class Hfitter::HftModelSplitterV4+;

#endif
