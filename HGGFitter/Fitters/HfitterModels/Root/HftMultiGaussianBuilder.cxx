#include "HfitterModels/HftMultiGaussianBuilder.h"
#include "RooMultiVarGaussian.h"

using namespace Hfitter;


void HftMultiGaussianBuilder::Setup(const char* v1Name, const char* v2Name, const char* m1Name, const char* m2Name, 
                                    double cov1, double cov2, double cov12)
{
  m_varNames.push_back(v1Name);
  m_varNames.push_back(v2Name);
  
  m_meanNames.push_back(m1Name);
  m_meanNames.push_back(m2Name);

  m_covMatrix.ResizeTo(2,2);
  m_covMatrix(0,0) = cov1;
  m_covMatrix(1,1) = cov2;
  m_covMatrix(0,1) = m_covMatrix(1,0) = cov12;
  m_covMatrix.Print();
}


RooAbsPdf* HftMultiGaussianBuilder::Pdf(const TString& name, const RooArgList& /*dependents*/, RooWorkspace& workspace) const
{
  RooArgList vars;
  
  for (std::vector<TString>::const_iterator varName = m_varNames.begin(); varName != m_varNames.end(); varName++) {
    vars.add(Param(workspace, *varName, ""));
  }

  RooArgList means;
  TString title;
  
  for (std::vector<TString>::const_iterator meanName = m_meanNames.begin(); meanName != m_meanNames.end(); meanName++) {
    means.add(Param(workspace, *meanName, ""));
    if (title != "") title += ",";
    title += *meanName;
  }

  return new RooMultiVarGaussian(name, "Multidimensional Gaussian for " + title, vars, means, m_covMatrix);
}
