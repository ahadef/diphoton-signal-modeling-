#include "HfitterModels/HftAbsNodeBuilder.h"
#include "RooArgList.h"
#include "RooRealVar.h"
#include "RooStringVar.h"
#include "RooWorkspace.h"
#include "RooAbsCategory.h"

#include "HfitterModels/HftDatacardReader.h"
#include "HfitterModels/HftDatacardReaderV4.h" // compat

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;


RooRealVar* HftAbsNodeBuilder::CreateParameter(const TString& name, const TString& unit,
                                               const TString& title, double value) const
{
  RooRealVar* par = new RooRealVar(name, title, value, unit);
  return par;
}


RooAbsReal& HftAbsNodeBuilder::Param(RooWorkspace& workspace, const TString& name, const TString& unit,
                                     const TString& title, double value, bool noExtras) const
{
  TString parName = (noExtras ? name : m_prefix + name + m_suffix);
  RooAbsReal* var = workspace.function(parName);
  if (var) {
    if (m_verbose) cout << "Param(" << parName << ") : reusing already-defined parameter." << endl;
    if (title != "") var->SetTitle(title);
    if (unit  != "") var->setUnit(unit);
    return *var;
  }

  if (m_verbose) cout << "Creating new parameter " << name << " " << m_suffix << " " << parName << endl;
  RooRealVar* param = CreateParameter(parName, unit, title, value);
  if (m_dcReader) {
    if (m_verbose) { cout << "Triggering read of : "; param->Print(); }
    m_dcReader->ReadRealVars(*param);
    if (m_verbose) { cout << "New value : "; param->Print(); }
  }
  if (m_dcReaderV4) m_dcReaderV4->ReadParameters(*param, "");

  workspace.import(*param, RooFit::Silence(!m_verbose));
  delete param;
  return *workspace.function(parName);
}


RooStringVar& HftAbsNodeBuilder::String(RooWorkspace& workspace, const TString& name,
                                        const TString& title, const TString& value) const
{
  TObject* obj = workspace.obj(name);
  if (obj) {
    RooStringVar* var = dynamic_cast<RooStringVar*>(obj);
    if (!var) cout << "ERROR: object " << name << " already exists, but of a different type (non-RooStringVar). Will probably crash now..." << endl;
    if (m_verbose) cout << "Param(" << name << ") : reusing already-defined parameter." << endl;
    if (title != "") var->SetTitle(title);
    return *var;
  }

  //cout << "Creating new parameter " << name << " " << m_suffix << " " << name << endl;
  RooStringVar* param = new RooStringVar(name, title, value);
  workspace.import(*param, RooFit::Silence(!m_verbose));
  return *(RooStringVar*)workspace.obj(name);
}


RooAbsCategory* HftAbsNodeBuilder::Category(RooWorkspace& workspace, const TString& name) const
{
  RooAbsCategory* cat = dynamic_cast<RooAbsCategory*>(workspace.obj(name));
  if (!cat) cout << "ERROR: no category named " << name << " defined in workspace" << endl;
  return cat;
}

