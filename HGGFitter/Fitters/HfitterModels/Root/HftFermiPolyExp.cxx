// -- CLASS DESCRIPTION [PDF] --
/*****************************************************************************
 * Project: Hfitter                                                          *
 * Package: Hfitter                                                          *
 *    File: $Id: HftFermiPolyExp.cxx,v 1.2 2007/12/13 20:34:20 hoecker Exp $
 * Author :                                                                  *
 *          Nicolas Berger, CERN                                             *
 *****************************************************************************/

#include <iostream>
#include <math.h>

#include "HfitterModels/HftFermiPolyExp.h"
#include "RooAbsReal.h"
#include "RooRealVar.h"
#include "RooRandom.h"
#include "TMath.h"


Hfitter::HftFermiPolyExp::HftFermiPolyExp(const char *name, const char *title,
                                 RooAbsReal& _x, RooAbsReal& _x0,
                                 RooAbsReal& _n, RooAbsReal& _alpha,
                                 RooAbsReal& _xT, RooAbsReal& _sigmaT)

  : RooAbsPdf(name,title),
    x     ("x",    "Dependent", this, _x),
    x0    ("x0",   "endpoint",  this, _x0),
    n     ("n",    "Polynomial power", this, _n),
    alpha ("alpha","Exponential factor", this,    _alpha),
    xT    ("xT",   "Fermi theshold position", this, _xT),
    sigmaT("sigmaT","Fermi theshold width", this, _sigmaT)
{
}


Hfitter::HftFermiPolyExp::HftFermiPolyExp(const HftFermiPolyExp& other,
                                          const char* name)
  : RooAbsPdf(other,name), x("x",this,other.x), 
    x0("x0",this,other.x0),
    n("n",this,other.n),
    alpha("alpha",this,other.alpha),
    xT    ("xT",     this, other.xT),
    sigmaT("sigmaT", this, other.sigmaT)
{
}


Double_t Hfitter::HftFermiPolyExp::evaluate() const
{
  Double_t u = x - x0;
  return TMath::Power(u, n)*TMath::Exp(-alpha*u)/(1 + TMath::Exp(-(x - xT)/sigmaT));
}



Int_t Hfitter::HftFermiPolyExp::getAnalyticalIntegral(RooArgSet& /*allVars*/, 
                                                      RooArgSet& /*analVars*/, 
                                                      const char* /*rangeName*/) const 
{
  return 0 ;
}
