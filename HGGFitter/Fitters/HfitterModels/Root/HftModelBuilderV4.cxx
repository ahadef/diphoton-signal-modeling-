// $Id: HftModelBuilderV4.cxx,v 1.7 2008/05/19 23:15:36 nberger Exp $   
// Author: Nicolas Berger, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#include "HfitterModels/HftModelBuilderV4.h"

#include "HfitterModels/HftModel.h"
#include "HfitterModels/HftConstraint.h"

#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftAbsPdfBuilder.h"
#include "HfitterModels/HftAbsRealBuilder.h"
#include "HfitterModels/HftModelSplitterV4.h"
#include "HfitterModels/HftRealMorphVar.h"
#include "RooFormulaVar.h"
#include "TString.h"

#include "RooSimultaneous.h"
#include "RooExtendedTerm.h"
#include "RooAddition.h"
#include "RooAddPdf.h"
#include "RooProdPdf.h"
#include "RooStats/ModelConfig.h"

using namespace Hfitter;
using std::cout;
using std::endl;


HftModelBuilderV4::HftModelBuilderV4( const TString& jobName, const TString& datacard, bool verbose)
   : TNamed(jobName, TString("")),
     m_datacardReader(datacard, verbose),
     m_pdf(0),
     m_workspace(new RooWorkspace()),
     m_verbose(verbose)
{
  // Read model type
  if (m_datacardReader.Type() == InvalidModel) return;

  // Read observables
  if (m_verbose) cout << "==> Adding observables" << endl; 
  m_workspace->import(m_datacardReader.Dependents(), RooFit::Silence(!m_verbose));
  m_workspace->defineSet("observables", m_datacardReader.Dependents());
  RooArgList observables(*m_workspace->set("observables"));
  
  if (m_verbose) cout << "==> Adding categories" << endl; 
  m_workspace->import(RooArgSet(m_datacardReader.Categories()), RooFit::Silence(!m_verbose));
  m_workspace->defineSet("categories", m_datacardReader.Categories());
  RooArgList categories(*m_workspace->set("categories"));

  if (m_verbose) cout << "==> Adding derived categories" << endl; 
  m_workspace->import(RooArgSet(m_datacardReader.AuxCategories()), RooFit::RecycleConflictNodes(), RooFit::Silence(!m_verbose));
  m_workspace->defineSet("auxiliary_categories", m_datacardReader.AuxCategories());
  RooArgList auxCategories(*m_workspace->set("auxiliary_categories"));

  // Read morphed variables
  if (m_verbose) cout << "==> Adding morphing variables" << endl; 
  const std::map<TString, TString>& morphVars = m_datacardReader.MorphVariables();
  for (std::map<TString, TString>::const_iterator var = morphVars.begin(); var != morphVars.end(); var++) {
    RooRealVar* morphingVar = 0;
    if (observables.find(var->second)) {
      cout << "WARNING: morphing on a dependent, this is not tested!" << endl;
      morphingVar = dynamic_cast<RooRealVar*>(observables.find(var->second));
    }
    else {
//       if (!m_workspace->function(var->second))
//         m_workspace->import(*new RooRealVar(var->second, "", 0), RooFit::Silence(!m_verbose));
//       morphingVar = m_workspace->var(var->second);
//       if (!morphingVar) {
//         cout << "ERROR : Morphing variable " << var->second << " is not a RooRealVar, aborting." << endl;
//         return;
//       }
      morphingVar = new RooRealVar(var->second, "", 0);
    }
    TString mvTitle = m_datacardReader.VarTitle(var->first);
    RooArgList pointVars(var->first + "_pointVars");
    for (unsigned int i = 0; i < m_datacardReader.MorphPointNames().find(var->first)->second.size(); i++) {
      TString mvPointName = m_datacardReader.MorphPointNames().find(var->first)->second[i];
      RooRealVar* pointVar = new RooRealVar(HftRealMorphVar::MakeName(var->first, mvPointName), 
                                            mvTitle + " for point " + mvPointName, 0, 
                                            m_datacardReader.VarUnit(var->first));
      pointVars.add(*pointVar);
    }
    
    HftRealMorphVar* morphVar = new HftRealMorphVar(var->first, m_datacardReader.VarTitle(var->first), *morphingVar, 
                                                    m_datacardReader.MorphPointPositions().find(var->first)->second, pointVars);
    if (m_verbose) cout << "Adding morphing variable " << morphVar->GetName() << " (" << morphVar->NPoints() << " points)." << endl;
//     morphVar->Print("V");
//     cout << "Snapshot ===" << endl;
//     RooArgSet(*morphVar).snapshot(true)->Print();
//     cout << "Snapshot ///" << endl;
    m_datacardReader.ReadParameters(morphVar->Parameters(), "");
    //m_workspace->import(pointVars, RooFit::Silence(!m_verbose));
    m_workspace->import(*morphVar, RooFit::Silence(!m_verbose), RooFit::RecycleConflictNodes());
  }

  // Read formulas  
  if (m_verbose) cout << "==> Adding formulas" << endl; 
  const std::vector<TString>& derivedRealNames = m_datacardReader.DerivedRealNames();
  const std::map<TString, TString>& formulaSpecs = m_datacardReader.Formulas();
  const std::map<TString, HftFunctionBuilderSpec>& functionSpecs = m_datacardReader.FunctionSpecs();
  for (std::vector<TString>::const_iterator f = derivedRealNames.begin(); f != derivedRealNames.end(); f++) {  
    if (formulaSpecs.find(*f) != formulaSpecs.end()) {
      TString spec = formulaSpecs.find(*f)->second;
      if (m_verbose) cout << "Creating formula var " << *f << " = " << spec << endl;
      RooFormulaVar* formula = HftAbsParameters::MakeFormulaVar(*f, "", spec, *m_workspace, m_verbose);
      if (!formula) return;
      ReadParameters(*formula); // temporary test
    }
    else if (functionSpecs.find(*f) != functionSpecs.end()) {
      HftFunctionBuilderSpec spec = functionSpecs.find(*f)->second;
      if (m_verbose) cout << "Creating function var " << *f << " = " << spec << endl;
      HftAbsRealBuilder* builder = HftAbsRealBuilder::Create(spec.name, spec.args, m_verbose);
      RooAbsReal* func = builder->Real(*f, *m_workspace);
      delete builder;
      if (!func) return;
      m_workspace->import(*func, RooFit::Silence(!m_verbose));
      func = (RooAbsReal*)m_workspace->function(*f);
    }
  }

  // Make models
  if (m_verbose) cout << "==> Building model" << endl; 
  std::vector<RooAddPdf*> models;
  RooArgList normList;

  // Make PDF building blocks
  std::vector< std::vector<RooAbsPdf*> > pdfs;
  for (unsigned int k = 0; k < m_datacardReader.NComponents(); k++) {
    TString name = m_datacardReader.ComponentName(k);
    const std::vector<HftPdfBuilderSpec>& specs = m_datacardReader.ComponentSpecs(k);
    std::vector<RooAbsPdf*> componentPdfs;
    RooAbsReal* norm = m_workspace->var(m_datacardReader.ComponentNorm(k));
    if (!norm) norm = m_workspace->function(m_datacardReader.ComponentNorm(k));
    if (!norm) {
      norm = new RooRealVar(m_datacardReader.ComponentNorm(k), "", 0);
      m_workspace->import(*norm, RooFit::Silence(!m_verbose));
      norm = m_workspace->var(m_datacardReader.ComponentNorm(k));
    }
    normList.add(*norm);
    for (unsigned int l = 0; l < specs.size(); l++) {
      if (specs[l].className == "Counting") {
        componentPdfs.push_back(0);
        continue;
      }
      HftAbsPdfBuilder* pdfBuilder = HftAbsPdfBuilder::Create(specs[l].className, specs[l].args, m_verbose);
      if (!pdfBuilder) {
        cout << "ERROR : Invalid builder class " << specs[l] << " for component " << name << endl;
        return;
      }
      pdfBuilder->SetVerbosity(m_verbose);
      pdfBuilder->SetDatacardReader(&m_datacardReader); // temporary test
      TString suffix = (m_datacardReader.NModels() == 1 ? "" : ".{" + m_datacardReader.ComponentModelCatStatesString(name, l) + "}");
      RooAbsPdf* pdf = pdfBuilder->Pdf(name + suffix, observables, *m_workspace);
      componentPdfs.push_back(pdf);
    }
    pdfs.push_back(componentPdfs);
  }

  // Make constraints
  RooArgList auxObservables;
  RooArgList constraintPdfs;
  if (m_verbose) cout << "Adding constraints" << endl;
  for (unsigned int k = 0; k < m_datacardReader.NConstraints(); k++) {
    TString name = m_datacardReader.ConstraintName(k);
    HftAbsPdfBuilder* pdfBuilder = HftAbsPdfBuilder::Create(m_datacardReader.ConstraintSpecs(k).className, m_datacardReader.ConstraintSpecs(k).args, m_verbose);
    if (!pdfBuilder) {
      cout << "ERROR: Invalid builder class " << m_datacardReader.ConstraintSpecs(k) << " for constraint on " << name << endl;
      return;
    }
    RooRealVar* var = 0;
    if (m_verbose) cout << "Adding constraint using new variable " << name << endl;
    var = new RooRealVar(name, "", 0);
    var->setConstant();
    m_workspace->import(*var, RooFit::Silence(!m_verbose));
    var = m_workspace->var(name);
    auxObservables.add(*var);
    RooAbsPdf* constraintPdf = pdfBuilder->Pdf(name + "_pdf", RooArgList(*var), *m_workspace);
    if (!constraintPdf) {
      cout << "ERROR : could not create constraint PDF for " << name << endl;
      return;
    }
    ReadParameters(*constraintPdf);
    constraintPdfs.add(*constraintPdf);
  }
  m_workspace->defineSet("auxObservables", auxObservables);

  // Now make RooAddPdfs
  std::vector<RooAbsPdf*> modelPdfs;
  for (unsigned int i = 0; i < m_datacardReader.NModels(); i++) {
    RooArgList pdfList;
    bool counting = false;
    for (unsigned int k = 0; k < m_datacardReader.NComponents(); k++) {
      TString name = m_datacardReader.ComponentName(k);
      unsigned int modelIndex = m_datacardReader.ComponentModelIndex(i, k);
      if (pdfs[k][modelIndex]) {
        if (counting) {
          cout << "ERROR: cannot declare component " << name << " with a PDF for model " << modelIndex << ", since a previous component was declared as a counting-only" << endl;
          return;
        }
        pdfList.add(*pdfs[k][modelIndex]);
      }
      else {
        if (k > 0 && !counting) {
          cout << "ERROR: cannot declare component " << name << " as counting-only for model " << modelIndex << ", since a previous component was declared with a PDF" << endl;
          return;
        }
        counting = true;
      }
    }
    RooAbsPdf* modelPdf = 0;
    TString modelName = (m_datacardReader.Type() == NonSplitModel ? jobName : Form("%s.sumModel%d", jobName.Data(), i));
    if (counting) {
      RooAddition* totalNEventsVar = new RooAddition(modelName + ".totalNEvents", "", normList);
      m_workspace->import(*totalNEventsVar, RooFit::Silence(!m_verbose));
      totalNEventsVar = (RooAddition*)m_workspace->function(totalNEventsVar->GetName());
      modelPdf = new RooExtendedTerm(modelName , "Total event-counting PDF", *totalNEventsVar);
    }
    else
      modelPdf = new RooAddPdf(modelName, "", pdfList, normList);
    ReadParameters(*modelPdf);
    m_workspace->import(*modelPdf, RooFit::RecycleConflictNodes(), RooFit::Silence(!m_verbose));
    modelPdfs.push_back(m_workspace->pdf(modelPdf->GetName()));
  }
  
  // Split case : hand the model(s) to HftModelSplitterV4 
  if (m_datacardReader.Type() == NonSplitModel) {
    m_pdf = modelPdfs[0];
    if (constraintPdfs.getSize() > 0) {
          RooArgList prodList;
          prodList.add(*m_pdf);
          prodList.add(constraintPdfs);
      m_pdf = new RooProdPdf(m_pdf->GetName() + TString("_constrained"), "", prodList);
    }
  }
  else {
    m_splitter = new HftModelSplitterV4(GetName(), modelPdfs, m_datacardReader, *m_workspace, m_verbose);
    m_pdf = m_splitter->Pdf();
    if (!m_pdf) return;
    if (constraintPdfs.getSize() > 0) {
      RooArgList remainingCPdfs = constraintPdfs;
      RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(m_pdf);
      RooSimultaneous* newSim = new RooSimultaneous(simPdf->GetName() + TString("_constrained"), simPdf->GetTitle(), *(RooAbsCategoryLValue*)&simPdf->indexCat());
      for (int i = 0; i < simPdf->indexCat().numTypes(); i++) {
        TString typeName = simPdf->indexCat().lookupType(i)->GetName();
        RooAbsPdf* thisPdf = simPdf->getPdf(typeName);
        RooArgList overlapPdfs, nonOverlapPdfs;
        for (int i = 0; i < remainingCPdfs.getSize(); i++) {
          RooArgSet* overlap = HftConstraint::Overlap(*thisPdf, *(RooAbsPdf*)remainingCPdfs.at(i));
          if (overlap->getSize())
            overlapPdfs.add(remainingCPdfs[i]);
          else
            nonOverlapPdfs.add(remainingCPdfs[i]);
          delete overlap;
        }
        if (overlapPdfs.getSize() > 0) {
          RooArgList prodList;
          prodList.add(*thisPdf);
          prodList.add(overlapPdfs);
          RooProdPdf* newPdf = new RooProdPdf(thisPdf->GetName() + TString("_constrained"), "", prodList);
          newSim->addPdf(*newPdf, typeName);
          remainingCPdfs.removeAll();
          remainingCPdfs.add(nonOverlapPdfs);
        }
        else
          newSim->addPdf(*thisPdf, typeName);
      }
      m_pdf = newSim;
    }
  }
  ReadParameters(*m_pdf);
  m_workspace->import(*m_pdf, RooFit::Silence(!m_verbose));
  RooArgSet* obs = m_pdf->getParameters(RooArgSet());
  RooArgList pars(*obs);
  delete obs;
  for (int i = 0; i < pars.getSize(); i++) {
    RooAbsArg* par = pars.at(i);
    TString title = m_datacardReader.VarTitle(par->GetName());
    if (title != "") {
      if (m_verbose) cout << "Setting title of " << par->GetName() << " to " << title << endl;
      par->SetTitle(title);
    }
    RooRealVar* var = dynamic_cast<RooRealVar*>(par);
    if (var) {
      TString unit = m_datacardReader.VarUnit(par->GetName());
      if (unit != "") {
        if (m_verbose) cout << "Setting unit of " << par->GetName() << " to " << unit << endl;
        var->setUnit(unit);
      }
    }
  }

  for (std::map<TString, bool>::const_iterator fixedVar = m_datacardReader.VarsFixedInComponentFits().begin();
       fixedVar != m_datacardReader.VarsFixedInComponentFits().end(); fixedVar++) 
  {
    RooAbsCollection* selected = pars.selectByName(fixedVar->first);
    if (selected->getSize() > 0) {
      if (m_verbose) cout << "Setting parameters fixed in component fits:" << endl;
      if (m_verbose) selected->Print();
      selected->setAttribAll("FixedPar", true);
    }
    delete selected;
  }
}


HftModel* HftModelBuilderV4::Create(const TString& name, const TString& datacard, bool verbose)
{
  HftModelBuilderV4* builder = new HftModelBuilderV4(name, datacard, verbose);
  if (!builder || !builder->Pdf()) return 0;
  RooAbsPdf* pdf = builder->Pdf();
  return Create(name, *builder->Workspace(), *pdf, "observables", "auxObservables", builder->DatacardReader().ComponentNames());
}


void HftModelBuilderV4::ReadParameters(RooAbsReal& real)
{
  RooArgSet* obs = real.getParameters(RooArgSet());
  m_datacardReader.ReadParameters(*obs, "");
  delete obs;
}


HftModel* HftModelBuilderV4::Create(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                  const TString& obsName, const TString& auxName,
                                  const TString& compName1, const TString& compName2)
{
  std::vector<TString> userCompNames;
  userCompNames.push_back(compName1);
  userCompNames.push_back(compName2);
  return Create(name, workspace, pdf, obsName, auxName, userCompNames);
}


HftModel* HftModelBuilderV4::Create(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                  const TString& obsName, const TString& auxName,
                                  const std::vector<TString>& userCompNames)
{
  if (!workspace.set(obsName)) workspace.defineSet(obsName, RooArgSet());
  if (!workspace.set(auxName)) workspace.defineSet(auxName, RooArgSet());
  const RooArgSet* wsObs = workspace.set(obsName);
  const RooArgSet* wsAux = workspace.set(auxName);
  if (!wsObs || !wsAux) // should never happen
  {
    cout << "HftModelBuilderV4::Create : Could not retrieve observables or auxObservables" << endl;
    return 0;
  }
  
  return Create(name, workspace, pdf, *wsObs, *wsAux, userCompNames);
  
}

HftModel* HftModelBuilderV4::Create(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                  const RooArgSet& observables, const RooArgSet& auxObservables,
                                  const std::vector<TString>& userCompNames)
{
  std::vector<RooAbsPdf*> catPdfs;
  std::vector<TString> names;
  RooSimultaneous* simPdf = dynamic_cast<RooSimultaneous*>(&pdf);
  if (!simPdf) return CreateSimpleModel(name, workspace, pdf, &observables, &auxObservables, userCompNames);

  const RooAbsCategoryLValue& indexCat = simPdf->indexCat();
  for (int i = 0; i < indexCat.numTypes(); i++) {
    catPdfs.push_back(simPdf->getPdf(indexCat.lookupType(i)->GetName()));
    names.push_back(indexCat.lookupType(i)->GetName());
  }
  std::vector<HftConstraint*> constraints;
  std::vector<HftModel*> categoryModels, componentModels;
  RooArgList alreadyUsed;

  for (unsigned int i = 0; i < catPdfs.size(); i++) {
    std::vector<TString> compNames = userCompNames;
    for (unsigned int k = 0; k < compNames.size(); k++) 
      compNames[k] = compNames[k] + (compNames[k] != "" ? "_" : "") + names[i];
    HftModel* model = Create(names[i], workspace, *catPdfs[i], observables, auxObservables, compNames);
    if (!model) return 0;
    for (unsigned int k = 0; k < model->NConstraints(); k++) {
      if (!model->Constraint(k)->AuxObservables().overlaps(alreadyUsed)) {
        constraints.push_back(model->Constraint(k));
        alreadyUsed.add(model->Constraint(k)->AuxObservables());
      }
    }
    categoryModels.push_back(model);
  }
  if (categoryModels.size() == 0) {
    cout << "ERROR : zero categories, exiting" << endl;
    return 0;
  }
  for (unsigned int k = 0; k < categoryModels[0]->NComponents(); k++) {
    RooSimultaneous* thisSimPdf = new RooSimultaneous(categoryModels[0]->ComponentModel(k)->GetName(), "", *(RooAbsCategoryLValue*)&indexCat);
    for (int i = 0; i < indexCat.numTypes(); i++)
      thisSimPdf->addPdf(*categoryModels[i]->ComponentModel(k)->Pdf(), indexCat.lookupType(i)->GetName());
    HftModel* model = new HftModel(workspace, 0, *thisSimPdf, observables, auxObservables, std::vector<HftModel*>(), std::vector<HftModel*>(),
                                   constraints, 0, categoryModels[0]->ComponentModel(k)->GetName(), true);
    componentModels.push_back(model);
  }

  return new HftModel(workspace, 0, pdf, observables, auxObservables,
                      componentModels, categoryModels, constraints, 0, name);
}

HftModel* HftModelBuilderV4::Create(const TString& name, 
                                  const RooWorkspace& workspace, 
                                  TString& modelConfigName)
{
  auto modelConfig = dynamic_cast<RooStats::ModelConfig*>(workspace.obj(modelConfigName));
  if (not modelConfig)
  {
    cout << "HftModelBuilderV4::Create : ModelConfig object with name " 
         << modelConfigName << " not found" << endl;
    return 0;
  }
  return Create(name, *modelConfig);
}
        
HftModel* HftModelBuilderV4::Create(const TString& name, 
                                  RooStats::ModelConfig& modelConfig)
{
  if (not modelConfig.GetWorkspace() or not modelConfig.GetPdf() or not
      modelConfig.GetObservables())
  {
    cout << "HftModelBuilderV4::Create : No workspace, pdf or observables associated to ModelConfig object" << endl;
    return 0;
  }
  RooWorkspace& workspace = *modelConfig.GetWorkspace();
  RooAbsPdf& pdf = *modelConfig.GetPdf();
  if (not modelConfig.GetGlobalObservables())
    modelConfig.SetGlobalObservables(RooArgSet());
  return Create(name, workspace, pdf,
                *modelConfig.GetObservables(),
                *modelConfig.GetGlobalObservables() );
}



HftModel* HftModelBuilderV4::CreateSimpleModel(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                             const TString& obsName, const TString& auxName,
                                             const std::vector<TString>& userCompNames)
{ 
  return CreateSimpleModel(name, workspace, pdf, workspace.set(obsName), workspace.set(auxName), userCompNames);
}


HftModel* HftModelBuilderV4::CreateSimpleModel(const TString& name, RooWorkspace& workspace, RooAbsPdf& pdf,
                                             const RooArgSet* observables, const RooArgSet* auxObservables,
                                             const std::vector<TString>& userCompNames)
{ 


  RooProdPdf* prodPdf = dynamic_cast<RooProdPdf*>(&pdf);
  std::vector<HftConstraint*> constraints;
  std::vector<HftModel*> componentModels;
  if (prodPdf) {
    if (prodPdf->pdfList().getSize() == 0) {
      cout << "ERROR: found a RooProdPdf with no components!" << endl;
      return 0;
    }
    int iPdf = 0;
    for (int i = 0; i < prodPdf->pdfList().getSize(); i++) {
      if (MakeSplitComponents(*(RooAbsPdf*)prodPdf->pdfList().at(i), workspace, observables, auxObservables, userCompNames, componentModels)) { iPdf = i; break; }
    }
    for (int i = 0; i < prodPdf->pdfList().getSize(); i++) {
      if (i == iPdf) continue;
      HftConstraint* constraint = new HftConstraint(*(RooAbsPdf*)prodPdf->pdfList().at(i),
                                                    *(RooAbsPdf*)prodPdf->pdfList().at(iPdf), *auxObservables);
//  Code below is disabled since constraints are now attached to a single category => there may be a dependence on the PDF in another category, which we don't see here
//       if (constraint->ConstrainedParameters().getSize() == 0) {
//         cout << "WARNING in PDF " << pdf.GetName() << " : constraint " << constraint->GetName() << " does not depend on PDF parameters" << endl;
//         delete constraint;
//         continue;
//       }
      constraints.push_back(constraint);
    }
  }
  else {
    if (!MakeSplitComponents(pdf, workspace, observables, auxObservables, userCompNames, componentModels)) {
      cout << "ERROR : using model PDF " << pdf.GetName() << " which is not a sum over components" << endl;
      return 0;
    }
  }
  return new HftModel(workspace, 0, pdf, *observables, *auxObservables, componentModels, std::vector<HftModel*>(), constraints, 0, name);
}


bool HftModelBuilderV4::MakeSplitComponents(const RooAbsPdf& pdf, RooWorkspace& workspace, const RooArgSet* wsObs, const RooArgSet* wsAux,
                                          const std::vector<TString>& userCompNames, std::vector<HftModel*>& componentModels)
{
  const RooAddPdf* addPdf = dynamic_cast<const RooAddPdf*>(&pdf);
  if (addPdf) {
    for (int i = 0; i < addPdf->pdfList().getSize(); i++) {
      RooAbsReal* thisNorm = (RooAbsReal*)addPdf->coefList().at(i);
      RooAbsPdf*  thisPdf  = (RooAbsPdf*) addPdf->pdfList().at(i);
      TString thisName = (int)userCompNames.size() > i ? userCompNames[i] : "";
      RooAddPdf* thisAddPdf = new RooAddPdf(thisName, "one-component RooAddPdf", *thisPdf, *thisNorm);
      componentModels.push_back(new HftModel(workspace, 0, *thisAddPdf, *wsObs, *wsAux,
                                             std::vector<HftModel*>(), std::vector<HftModel*>(), std::vector<HftConstraint*>(),
                                             thisNorm, thisName, true));
    }
    return true;
  }
  const RooExtendedTerm* extPdf = dynamic_cast<const RooExtendedTerm*>(&pdf);
  if (extPdf) {
    RooArgSet* comps = extPdf->getComponents();
    RooAddition* addTerm = dynamic_cast<RooAddition*>(RooArgList(*comps).at(1));
    delete comps;
    if (!addTerm) {
      cout << "ERROR : using invalid counting PDF " << pdf.GetName() << "." << endl;
      return 0;
    }
    for (int i = 0; i < addTerm->list().getSize(); i++) {
      RooAbsReal* thisNorm = (RooAbsReal*)addTerm->list().at(i);
      TString thisName = (int)userCompNames.size() > i ? userCompNames[i] : "";
      RooExtendedTerm* thisExtPdf = new RooExtendedTerm(thisName + "_component", "one-component RooExtendedTerm", *thisNorm);
      componentModels.push_back(new HftModel(workspace, 0, *thisExtPdf, *wsObs, *wsAux,
                                             std::vector<HftModel*>(), std::vector<HftModel*>(), std::vector<HftConstraint*>(),
                                             thisNorm, thisName, true));
    }
    return true;
  }
  cout << "ERROR : invalid model PDF " << pdf.GetName() << "." << endl;
  return false;
}
