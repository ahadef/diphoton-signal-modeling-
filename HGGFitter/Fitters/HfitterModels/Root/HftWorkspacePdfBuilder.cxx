#include "HfitterModels/HftWorkspacePdfBuilder.h"

#include "RooWorkspace.h"
#include "RooAbsPdf.h"
#include "TFile.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;


void HftWorkspacePdfBuilder::Setup(const char* fileName, const char* key, const char* name)
{
  m_fileName = fileName;
  m_key = key;
  m_name = name;
}


RooAbsPdf* HftWorkspacePdfBuilder::Pdf(const TString& name, const RooArgList& /*dependents*/, RooWorkspace& /*workspace*/) const
{
  TFile* f = TFile::Open(m_fileName);
  if (!f || !f->IsOpen()) {
    cout << "ERROR: HftWorkspacePdfBuilder cannot open file " << m_fileName << endl;
    return 0;
  }
  TObject* obj = f->Get(m_key);
  if (!obj) {
    cout << "ERROR: HftWorkspacePdfBuilder cannot open object with key " << m_key << " in file " << m_fileName << endl;
    return 0;
  }
  RooWorkspace* ws = dynamic_cast<RooWorkspace*>(obj);
  if (!ws) {
    cout << "ERROR: object " << m_key << " in file " << m_fileName << " is not a RooWorkspace." << endl;
    return 0;
  }
  RooAbsPdf* pdf = ws->pdf(m_name);
  if (!pdf) {
    cout << "ERROR: no PDF with name " << m_name << " found in workspace." << endl;
    return 0;
  }
  pdf->SetName(name);
  return pdf;
}
