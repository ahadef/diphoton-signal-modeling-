// $Id: HftModelSplitterV4.cxx,v 1.18 2008/03/05 23:34:49 nberger Exp $   
// Author: Mohamed Aharrouche, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#include "HfitterModels/HftModelSplitterV4.h"

#include <set>
#include "RooCategory.h"
#include "RooArgList.h"
#include "RooSimultaneous.h"
#include "RooSimPdfBuilder.h"
#include "RooStringVar.h"
#include "RooSimWSTool.h"

#include "HfitterModels/HftModel.h"

#include <iostream>
using std::cout;
using std::endl;

using namespace Hfitter;

RooSimultaneous* HftModelSplitterV4::Pdf()
{
  if (m_verbose) cout << "==> Building split model" << endl;
  std::map<TString, std::vector<TString> > splittingCatMap = m_datacardReader->SplittingCatMap();
  std::map<TString, std::vector<TString> > splittingVarMap = m_datacardReader->SplittingVarMap();

  // build RooSimultaneous PDF
  
  // The splitting prescriptions for each model
  std::vector<RooSimWSTool::SplitRule> splitRules;

  for (unsigned int i = 0; i < NModels(); i++) {
    if (m_verbose) ModelPdf(i)->Print();
    RooSimWSTool::SplitRule splitRule(ModelPdf(i)->GetName());
    // loop over variables
    for (std::map<TString, std::vector<TString> >::iterator var_cats = splittingVarMap.begin();
         var_cats != splittingVarMap.end(); var_cats++) {
      // First check if this var is among the parameters of this particular model
      RooArgSet* allPars = ModelPdf(i)->getParameters(RooArgSet());
      RooArgSet* varSet = (RooArgSet*)allPars->selectByName(var_cats->first); // selectByName takes care of wildcards
      RooArgList vars(*varSet);
      delete allPars;
      delete varSet;
      // for multiple models, this is expected
      if (NModels() == 1 && vars.getSize() == 0) {
        cout << "WARNING: Ignoring parameter " << var_cats->first << " for model " 
             << ModelPdf(i)->GetName()
             << " - please check that the splitting really belongs here!" << endl;
        //continue;
      }
      // loop over categories for this variable
      TString catString = "";
      for (std::vector<TString>::iterator cat = var_cats->second.begin();
        cat != var_cats->second.end(); cat++) {
        if (catString != "") catString += ",";
        catString += *cat;
      }
      for (int j = 0; j < vars.getSize(); j++) {
        if (m_verbose) cout << "Split rule: " << vars.at(j)->GetName() << " split along " << catString << endl;
        splitRule.splitParameter(vars.at(j)->GetName(), catString);
      }
    }
    splitRules.push_back(splitRule);
  }
  
  RooSimWSTool swt(*m_workspace);
  RooSimultaneous* simPdf = 0;
  if (NModels() == 1) {
    RooSimWSTool::BuildConfig config(ModelPdf(0)->GetName(), splitRules[0]);
    simPdf = swt.build(GetName(), config, m_verbose);
  }
  else {
    RooSimWSTool::MultiBuildConfig config(m_datacardReader->ModelCatName());
    for (unsigned int i = 0; i < NModels(); i++) {
      TString modelStates = "";
      const std::set<TString>& catsToAdd = m_datacardReader->GlobalModelCatStates(i);
      for (std::set<TString>::const_iterator cat = catsToAdd.begin(); cat != catsToAdd.end(); cat++) {
        if (modelStates != "") modelStates += ",";
        modelStates += *cat;
      }
      if (m_verbose) cout << "Model rule: " << ModelPdf(i)->GetName() << " using states " << modelStates << endl;
     config.addPdf(modelStates, ModelPdf(i)->GetName(), splitRules[i]);
    }
    simPdf = swt.build(GetName(), config, m_verbose);
  }
  if (!simPdf) {
    cout << "ERROR : splitting failed" << endl;
    return 0;
  }
  if (m_verbose) cout << "Splitting complete" << endl;
  simPdf->SetTitle(TString("Simultaneous PDF for model ") + GetName());
  return simPdf;
}
