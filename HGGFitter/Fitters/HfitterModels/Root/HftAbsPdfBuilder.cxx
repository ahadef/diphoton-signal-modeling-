#include "HfitterModels/HftAbsPdfBuilder.h"

#include "RooRealVar.h"
#include "TClass.h"

using namespace Hfitter;

#include <iostream>
using std::cout;
using std::endl;


HftAbsPdfBuilder* HftAbsPdfBuilder::Create(const TString& className, const TString& args, bool verbose)
{
  TClass* instanceClass = TClass::GetClass(className);
  HftAbsPdfBuilder* builder = 0;
  if (instanceClass && instanceClass->InheritsFrom("RooAbsPdf")) {
    if (verbose) cout << "Instance " << className << " is actually a PDF class, automatically switching to HftTemplatePdfBuilder..." << endl;
    TObjArray* tokens = args.Tokenize(",");
    if (tokens->GetEntries() == 0) {
      cout << "ERROR : Cannot build PDF from HftTemplatePdfBuilder without a dependent variable!" << endl;
      delete tokens;
      return 0;
    }
    unsigned int nArgs = tokens->GetEntries() - 1;
    TString templateBuilderClass = "HftTemplatePdfBuilder<" + className + "," + Form("%d", nArgs) + ">";
    instanceClass = TClass::GetClass(templateBuilderClass);
    if (!instanceClass) {
      cout << "ERROR : template " << templateBuilderClass << " not found." << endl;
      cout << "==> please make sure the number of arguments provided (" << nArgs << ") is correct, and that the class is included in the dictionary." << endl;
      return 0;
    }
    if (!instanceClass->IsLoaded()) {
      cout << "ERROR : template " << templateBuilderClass << " seems not to be defined" << endl;
      cout << "==> please make sure the number of arguments provided (" << nArgs << ") is correct, and that the template is included in the dictionary." << endl;
      return 0;
    }
    TString modArgs;
    for (int i = 0; i < tokens->GetEntries(); i++) {
      TString token = tokens->At(i)->GetName();
      if (token[0] != TString("\"")) token = "\"" + token;
      if (token.Length() > 0 && token[token.Length() - 1] != TString("\"")) token = token + "\"";
      if (modArgs != "") modArgs += ",";
      modArgs += token;
    }
    delete tokens;
    builder = dynamic_cast<HftAbsPdfBuilder*>(HftInstance::Create(templateBuilderClass, modArgs, verbose));
    builder->SetVerbosity(verbose);
    return builder;
  }
  builder = dynamic_cast<HftAbsPdfBuilder*>(HftInstance::Create(className, args, verbose));
  if (!builder) {
    cout << "ERROR : builder class " << className << " does not derive from HftAbsPdfBuilder, cannot be used to create PDFs" << endl;
    return 0;
  }
  builder->SetVerbosity(verbose);
  return builder;
}


RooRealVar* HftAbsPdfBuilder::Dependent(const RooArgList& dependents, const TString& name,
                                        const TString& unit, const TString& title) const
{
  RooRealVar* var = (RooRealVar*)dependents.find(name);
  if (var) {
    var->setUnit(unit);
    var->SetTitle(title);
    return var;
  }
  cout << "ERROR : dependent variable not found with name " << name << endl;
  return 0;
}
