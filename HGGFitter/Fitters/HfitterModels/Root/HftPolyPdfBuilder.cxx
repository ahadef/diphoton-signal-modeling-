#include "HfitterModels/HftPolyPdfBuilder.h"
#include "RooPolynomial.h"

using namespace Hfitter;


void HftPolyPdfBuilder::Setup(const char* depName,
                                 const char* c1Name, const char* c2Name, const char* c3Name, 
                                 const char* c4Name, const char* c5Name, const char* c6Name)
{
  m_depName = depName;
  if (TString(c1Name) != "") m_cNames.push_back(c1Name); else return;
  if (TString(c2Name) != "") m_cNames.push_back(c2Name); else return;
  if (TString(c3Name) != "") m_cNames.push_back(c3Name); else return;
  if (TString(c4Name) != "") m_cNames.push_back(c4Name); else return;
  if (TString(c5Name) != "") m_cNames.push_back(c5Name); else return;
  if (TString(c6Name) != "") m_cNames.push_back(c6Name); else return;
}


void HftPolyPdfBuilder::Setup(const char* depName, double /*offset*/,
                              const char* c1Name, const char* c2Name, const char* c3Name, 
                              const char* c4Name, const char* c5Name, const char* c6Name)
{
  m_depName = depName;
  if (TString(c1Name) != "") m_cNames.push_back(c1Name); else return;
  if (TString(c2Name) != "") m_cNames.push_back(c2Name); else return;
  if (TString(c3Name) != "") m_cNames.push_back(c3Name); else return;
  if (TString(c4Name) != "") m_cNames.push_back(c4Name); else return;
  if (TString(c5Name) != "") m_cNames.push_back(c5Name); else return;
  if (TString(c6Name) != "") m_cNames.push_back(c6Name); else return;
}


RooAbsPdf* HftPolyPdfBuilder::Pdf(const TString& name, const RooArgList& dependents, RooWorkspace& workspace) const
{
  RooAbsReal& mgg = *Dependent(dependents, m_depName, "GeV", "m_{#gamma#gamma}");

  RooArgList vars;

  RooArgList coeffs;
  for (std::vector<TString>::const_iterator cName = m_cNames.begin(); cName != m_cNames.end(); cName++) {
    vars.add(Param(workspace, *cName, ""));
  }

  return new RooPolynomial(name, Form("PDF for %s", m_depName.Data()), mgg, vars);
}
