// $Id: HftDeltaPdf.cxx,v 1.1 2007/03/10 03:09:07 nberger Exp $   
// Author: Mohamed Aharrouche, Andreas Hoecker, Sandrine Laplace

#include "HfitterModels/HftDeltaPdf.h"


Hfitter::HftDeltaPdf::HftDeltaPdf( const HftDeltaPdf& other, const char* name )
   : RooAbsPdf( other, name )
{
}


Hfitter::HftDeltaPdf::HftDeltaPdf( const char* name, const char* title, 
                                   RooAbsReal& X, Double_t X0, Double_t relEpsilon )
  : RooAbsPdf( name, title ),
    x("x", "Dependent", this, X),
    x0(X0),
    epsilon(TMath::Abs(relEpsilon*x0))
{
}


Hfitter::HftDeltaPdf::~HftDeltaPdf()
{
}


Double_t Hfitter::HftDeltaPdf::evaluate() const
{
  return ( TMath::Abs(x - x0) < epsilon ? 1 : 0);
}


Int_t Hfitter::HftDeltaPdf::getAnalyticalIntegral(RooArgSet& allVars, 
                                                  RooArgSet& analVars, 
                                                  const char* /*rangeName*/) const 
{
  //  return 0;
  if ( matchArgs(allVars, analVars, x) ) return 1 ;
  return 0 ;
}


Double_t Hfitter::HftDeltaPdf::analyticalIntegral(Int_t /*code*/, 
                                                  const char* rangeName) const 
{
  Double_t xMin = x.min(rangeName) - x0;
  Double_t xMax = x.max(rangeName) - x0;

  Double_t xLo = (xMin > -epsilon ? xMin : -epsilon);
  Double_t xHi = (xMax <  epsilon ? xMax :  epsilon);

  if (xLo >= xHi) return 0;
  return (xHi - xLo);
}
