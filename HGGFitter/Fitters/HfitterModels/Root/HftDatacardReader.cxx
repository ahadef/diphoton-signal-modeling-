// $Id: HftDatacardReader.cxx,v 1.7 2008/05/19 23:15:36 nberger Exp $   
// Author: Nicolas Berger, Nicolas Berger, Andreas Hoecker, Sandrine Laplace

#include "HfitterModels/HftDatacardReader.h"

#include <map>
#include <string>
#include <fstream>
#include <sstream>
#include <algorithm>
#include "RooRealVar.h"
#include "RooCategory.h"
#include "RooMappedCategory.h"
#include "RooStreamParser.h"
#include "HfitterModels/HftTools.h"
#include "HfitterModels/HftParameterSet.h"
#include "HfitterModels/HftAbsPdfBuilder.h"

using namespace Hfitter;
using std::cout;
using std::endl;
using std::ios_base;
using std::ostream_iterator;


HftDatacardReader::HftDatacardReader(const TString& datacard, bool verbose)
   : m_modelType(InvalidModel),
     m_verbose(verbose)
{
  if (!Preprocess(datacard, m_data)) return;
  m_dataStream = new std::istringstream(m_data);
  m_parser = new RooStreamParser(*m_dataStream);
  m_parser->setPunctuation(m_parser->getPunctuation() + TString("*")); // need to handle * used as wildcard.
  
  if (m_verbose) cout << "Using datacard " << datacard << endl;
  if (!ReadIdentifiers()) return;
  if (!MakeComponentInfo()) return;
//   TString punctuation =  m_parser->getPunctuation();
//   m_parser->setPunctuation("()[]|=,");

  if (m_modelCatName != "") { 
    if (!m_categories.find(m_modelCatName)) {
      cout << ERROR() << "Model category '" << m_modelCatName << "' was not defined in [Categories] block, stopping" << endl;
      return;
    }
    // should check also that all cat values are specified for each model...
    m_modelType = MultiPdfModel;
  }
  else if (m_splittingCatMap.size() > 0)
    m_modelType = SplitModel;
  else
    m_modelType = NonSplitModel;  
}


bool HftDatacardReader::Preprocess(const TString& fileName, std::string& data)
{
  // open the datacard
  std::ifstream fileStream(fileName);
  if (!fileStream.good()) { // file not found --> Error
    cout << "ERROR in HftDatacardReader: "
         << "unable to open input datacard " << fileName << " ==> abort" << endl;
    return false;
  } 

  while (!fileStream.eof()) {
    std::string cline;
    std::getline(fileStream, cline);
    TString line = cline;
    TObjArray* tokens = line.Tokenize(" ");
    if (tokens->GetEntries() == 2 && tokens->At(0)->GetName() == TString("#include")) {
      TString newName = tokens->At(1)->GetName();
      newName = newName(1, newName.Length() - 2); // remove leading and trailing "
      // if a path is specified for the original datacard, prepend it
      int slashIndex = fileName.Last('/');
      if (slashIndex >= 0)
        newName = fileName(0, slashIndex) + "/" + newName;
      std::string data2;
      if (!Preprocess(newName, data2)) { delete tokens; return false; }
      data += data2 + "\n";
    }
    else data += line + "\n";
    delete tokens;
  }
  return true;
}


HftDatacardReader::~HftDatacardReader()
{
  delete m_parser;
  delete m_dataStream;
  for (int i = 0; i < m_obs.getSize(); i++) delete m_obs.at(i);
  for (int i = 0; i < m_categories.getSize(); i++) delete m_categories.at(i);
  for (int i = 0; i < m_auxCategories.getSize(); i++) delete m_auxCategories.at(i);
}


std::istream& HftDatacardReader::Rewind(int pos)
{
  m_dataStream->clear(); 
  m_dataStream->seekg(pos, ios_base::beg);
  TString punctuation = m_parser->getPunctuation();
  delete m_parser;
  m_parser = new RooStreamParser(*m_dataStream);
  m_parser->setPunctuation(punctuation);
  return *m_dataStream; 
}


bool HftDatacardReader::IsAtEnd()
{
  return (m_dataStream->eof() || m_dataStream->fail() || m_parser->atEOF());
}


bool HftDatacardReader::ExpectToken(const TString& token)
{
  TString tok;
  if (!ReadToken(tok)) return false;
  return (tok != token);
  
  //return m_parser->expectToken(token); // Warning: does not respect sections!
/*  TString tok = "";
  if (!ReadToken(tok)) return false;
  cout << tok << ", " << token << endl;
  return (tok != token);*/
}


std::vector<HftPdfBuilderSpec> HftDatacardReader::ComponentSpecs(unsigned int i) const
{ 
  std::map<TString, std::vector<HftPdfBuilderSpec> >::const_iterator specs = m_componentSpecs.find(ComponentName(i));
  if (specs == m_componentSpecs.end()) return std::vector<HftPdfBuilderSpec>();
  return specs->second;
}


HftPdfBuilderSpec HftDatacardReader::ConstraintSpecs(unsigned int i) const
{ 
  std::map<TString, HftPdfBuilderSpec>::const_iterator specs = m_constraintSpecs.find(ConstraintName(i));
  if (specs == m_constraintSpecs.end()) return HftPdfBuilderSpec();
  return specs->second;
}


TString HftDatacardReader::ComponentNorm(unsigned int i) const
{
  std::map<TString, TString >::const_iterator norm = m_componentNorms.find(ComponentName(i));
  if (norm == m_componentNorms.end()) return "";
  return norm->second;
}


std::set<TString> HftDatacardReader::ComponentModelCatStates(const TString& compName, unsigned int model) const
{ 
  std::map< TString, std::vector< std::set<TString> > >::const_iterator compData = m_modelCatStates.find(compName); 
  if (compData == m_modelCatStates.end()) return std::set<TString>(); 
  return (compData->second)[model]; 
}


TString HftDatacardReader::ComponentModelCatStatesString(const TString& compName, unsigned int model) const
{
  std::set<TString> states = ComponentModelCatStates(compName, model);
  std::ostringstream ostr;
  std::copy(states.begin(), states.end(), ostream_iterator<TString>(ostr, ","));
  return ostr.str().substr(0, ostr.str().size() - 1);
}


TString HftDatacardReader::GlobalModelCatStatesString(unsigned int globalModel) const
{
  std::set<TString> states = GlobalModelCatStates(globalModel);
  std::ostringstream ostr;
  std::copy(states.begin(), states.end(), ostream_iterator<TString>(ostr, ","));
  return ostr.str().substr(0, ostr.str().size() - 1);
}


TString HftDatacardReader::VarTitle(const TString& var) const 
{ 
  std::map<TString, TString>::const_iterator t = m_varTitles.find(var); 
  if (t == m_varTitles.end()) return "";
  TString title = t->second;
  if (title(0, 1) == "\"") title = title(1, title.Length() - 1);
  if (title(title.Length() - 1, 1) == "\"") title = title(0, title.Length() - 1);
  return title;
}


TString HftDatacardReader::VarUnit(const TString& var) const 
{ 
  std::map<TString, TString>::const_iterator u = m_varUnits.find(var); 
  if (u == m_varUnits.end()) return "";
  TString unit = u->second;
  if (unit(0, 1) == "\"") unit = unit(1, unit.Length() - 1);
  if (unit(unit.Length() - 1, 1) == "\"") unit = unit(0, unit.Length() - 1);
  return unit;
}


bool HftDatacardReader::VarFixedInCompFit(const TString& var) const 
{ 
  std::map<TString, bool>::const_iterator v = m_varFixedInCompFit.find(var); 
  return (v != m_varFixedInCompFit.end());
}


bool HftDatacardReader::ReadToken(TString& token)
{
  if (m_savedToken != "") {
    token = m_savedToken;
    m_savedToken = "";
    return true;
  }
  while (1) {
    if (IsAtEnd()) {
      token = "";
      //cout << "WARNING : reached end of stream." << endl;
      return true;
    }
    //cout << "PUNCT = " << m_parser->getPunctuation() << endl;
    token = m_parser->readToken();
    //if (token != "") cout << "Token = '" << token << "' " << IsAtEnd() << endl;
    if (token != "") return true;
  }
  return true; // never get there
}


bool HftDatacardReader::ReadLine(TString& line)
{
  while (1) {
    if (IsAtEnd()) {
      line = "";
      return true;
    }
    line = m_parser->readLine();
    //if (line != "") cout << "Line = '" << line << "' " << IsAtEnd() << endl;
    if (line != "") return true;
  }
  return true; // never get there
}


bool HftDatacardReader::PeekToken(TString& token)
{
  if (!ReadToken(token)) return false;
  m_savedToken = token;
  return true;
}


bool HftDatacardReader::ReadIdentifiers()
{
  if (m_verbose) cout << "==> Reading identifiers" << endl;
  Rewind();
  
  TString token;
  while (1) {
    if (!ReadToken(token)) return false;
    if (token == "") return true;
    if (token == "observable" && !ReadObservable()) return false;
    if (token == "component" && !ReadComponent()) return false;
    if (token == "category" && !ReadCategory()) return false;
    if (token == "mappedCategory" && !ReadMappedCategory()) return false;
    if (token == "split" && !ReadSplitting()) return false;
    if (token == "constraint" && !ReadConstraint()) return false;
    if (token == "formula"  && !ReadFormula()) return false;
    if (token == "function" && !ReadFunction()) return false;
    if (token == "morph" && !ReadMorphVar()) return false;
    if (token == "interpolate" && !ReadInterpVar()) return false;
  }
  return true;  
}


bool HftDatacardReader::ReadName(const TString& keyword, TString& name, const TString& next)
{
  name = "";
  if (!ReadToken(name)) return false; // name
  if (name == "") {
    cout << ERROR() << "expected name after '" << keyword << "' keyword." << endl;
    return false;
  }
  if (next != "" && ExpectToken(next)) {
    cout << ERROR() << "expected '" << next << "' after " << keyword << " name (" << name << "), exiting" << endl;
    return false;
  }
  if (m_verbose) cout << "==> Reading " << keyword << " " << name << endl;
  return true;
}


bool HftDatacardReader::ReadObservable()
{
  TString name = "";
  if (!ReadName("observable", name)) return false;
  RooRealVar* var = new RooRealVar(name, "", 0);
  m_obs.add(*var);
  return true;
}


bool HftDatacardReader::ReadComponent()
{
  TString name = "";
  if (!ReadName("component", name, "=")) return false;
  m_componentNames.push_back(name);  
  return ReadModel(name);
}


bool HftDatacardReader::MakeComponentInfo()
{
  for (unsigned int i = 0; i < NComponents(); i++)
    m_componentNorms[ComponentName(i)] = "n" + ComponentName(i);

  if (m_modelCatName == "") {
    std::vector<unsigned int> indices;
    std::set<TString> states;
    for (unsigned int i = 0; i < NComponents(); i++) indices.push_back(0);
    m_globalModelIndices.push_back(indices);
    m_globalModelCatStates.push_back(states);
    return true;
  }
  else {
    std::vector<TString> states = ModelCatStates();
    std::map<TString, bool> alreadyUsed;
    for (std::vector<TString>::const_iterator state = states.begin(); state != states.end(); state++) {
      if (alreadyUsed[*state]) continue;
      std::vector<std::set<TString> > catStatesSets;
      std::vector<unsigned int> indices;
      for (unsigned int j = 0; j < NComponents(); j++) {
        // the vector runs over models, the set contains the cat states that use this model
        const std::vector< std::set<TString> >& modelCatStates = m_modelCatStates[ComponentName(j)];
        unsigned int k0 = modelCatStates.size();
        for (unsigned int k = 0; k < modelCatStates.size(); k++) {
//           if (m_verbose) {
//             cout << "model " << ComponentName(j) << " : set at index " << k << " = ";
//             std::copy(modelCatStates[k].begin(), modelCatStates[k].end(), ostream_iterator<const char*>(cout, " "));
//             cout << endl;
//           }
          if (modelCatStates[k].find(*state) != modelCatStates[k].end()) {
            //cout << "model " << ComponentName(j) << " : found " << *state << " in set at index " << k <<  endl;
            k0 = k;
            break;
          }
        }
        if (k0 == modelCatStates.size()) {
          cout << ERROR() << "model category state " << *state << " was not assigned a model for component "
               << m_componentNames[j] << ", exiting" << endl;
          return false;
        }
        catStatesSets.push_back(modelCatStates[k0]);
        indices.push_back(k0);
      }
      std::set<TString> allIntersect = catStatesSets[0];
      for (std::vector<std::set<TString> >::const_iterator catStates = ++catStatesSets.begin(); catStates != catStatesSets.end(); catStates++) {
        std::set<TString> isect;
        std::set_intersection(allIntersect.begin(), allIntersect.end(), catStates->begin(), catStates->end(), std::inserter(isect, isect.begin()));
        allIntersect = isect;
      }
      m_globalModelIndices.push_back(indices);
      m_globalModelCatStates.push_back(allIntersect);
      if (m_verbose) {
        cout << "=> Defining global model " << m_globalModelIndices.size() - 1 << " using indices ";
        std::copy(indices.begin(), indices.end(), ostream_iterator<int>(cout, " "));
        cout << " for modelCat state(s) ";
        std::copy(allIntersect.begin(), allIntersect.end(), ostream_iterator<const char*>(cout, " "));
        cout << endl;
      }
      for (std::set<TString>::const_iterator catState = allIntersect.begin(); catState != allIntersect.end(); catState++) {
        alreadyUsed[*catState] = true;
      }
    }
  }
  return true;  
}


bool HftDatacardReader::ReadModel(const TString& name)
{
  TString token;
  TString className, args;
  if (!ReadPdfBuilderSpecs(className, args)) return false;
  m_componentSpecs[name].push_back(HftPdfBuilderSpec(className, args));
  unsigned modelIndex = m_componentSpecs[name].size() - 1;
  // sanity check for potentially missing observables
  TObjArray* tokens = args.Tokenize(", ");
  if (tokens->GetEntries() < 1) {
    cout << "ERROR: the builder for component '" << name << "' should have at least one argument (the name of the observable)" << endl;
    return false;
  }
  TString obsName = tokens->At(0)->GetName();
  delete tokens;
  obsName.ReplaceAll("\"", "");
  if (!m_obs.find(obsName)) {
    cout << WARNING() << "variable " << obsName << " seems to be an observable of model " << name << ", if so it should be declared with the line" << endl;
    cout << WARNING(0) << "observable " << obsName << " =  <value> min=<range minimum> max=<range maximum>" << endl;
  }
  if (m_verbose) cout << "   builder will be " << className << "(" << args << ")" << endl;

  // At this point one can optionally specify a model category with 'for'  
  if (!PeekToken(token)) return false; // just peek : if we are already done (no for) this token is part of the next block
  if (token == "for") {
    if (!ReadToken(token)) return false; // eat 'for'
    if (m_verbose && m_modelCatName == "") cout << "Model will be of multi-pdf type" << endl;
    if (!ReadToken(token)) return false;
    if (m_componentNames.size() > 1 && m_modelCatName != token) {
      cout << ERROR() << "category for model splitting should be the same for all components. Was "
	   << m_modelCatName << ", now got " << token << ", stopping" <<  endl;
      return false;
    }
    if (m_verbose && m_modelCatName == "") cout << "Using model category " << token << " for component " << name << endl;
    m_modelCatName = token;
    if (ExpectToken("=")) {
      cout << ERROR() << "expected '=' after model category name, exiting" << endl;
      return false;
    }
    if (ExpectToken("(")) {
      cout << ERROR() << "expected '= (' after model category name, exiting" << endl;
      return false;
    }
    std::set<TString> modelCatStates;
    while (1) {
      if (!ReadToken(token)) return false;
      if (token == ")" || token == "") break;
      if (token == ",") continue;
      modelCatStates.insert(token);
    }
        
    if (m_verbose) {
      cout << "   Model category state(s) for model " << modelIndex << " of component " << name << " will be : ";
      for (std::set<TString>::const_iterator state = modelCatStates.begin(); state != modelCatStates.end(); state++) 
        cout << "'" << *state << "' ";
      cout << "." << endl;    
    }
    m_modelCatStates[name].push_back(modelCatStates);
  }
  else { // no model cat specified
    if (m_modelCatName != "") {
      cout << "   ERROR : No model-splitting category specified for component " << name
	    << " but " << m_modelCatName << " was specified before, stopping" << endl;
      return false;
    }
  }
  
  // Here should find out if there are more models to be read for this component
  if (!PeekToken(token)) return false; // look ahead...
  if (token == ",") {
    if (!ReadToken(token)) return false; // eat the ','
    // We should have more models. Check if a model cat was defined.  
    if (m_modelCatName == "") {
      cout << ERROR() << "cannot specify multiple models since no model category was defined" << endl;
      return false;
    }
    return ReadModel(name);
  }
  
  return true;
}


bool HftDatacardReader::ReadPdfBuilderSpecs(TString& className, TString& args)
{
  TString token;
  
  // read model class
  if (!ReadToken(token)) return false;
  className = token;

  if (className == "PDF" || className == "Pdf") className = "HftGenericPdfBuilder";
  if (className == "WSPdf" || className == "WS" || className == "Workspace") className = "HftWorkspacePdfBuilder";
  if (className == "Hist" || className == "Histogram") className = "HftHistPdfBuilder";
  if (className == "Factory") className = "HftFactoryPdfBuilder";
  
  if (!ReadToken(token)) return false; // can be ':' or '('
  if (token == ":") {
    if (ExpectToken(":")) { // look for second ':'
      cout  << ERROR() << "unexpected character after class namespace, exiting" << endl;
      return false;
    }
    if (!ReadToken(token)) return false; // the real class name
    className = className + "::" + token;
    if (!ReadToken(token)) return false; // should now be '('
  }
  if (token != "(") {
    cout << ERROR() << "expected '(' after class name, got " << token << " -- exiting" << endl;
    return false;
  }
  while (1) {
    if (!ReadToken(token)) return false;
    if (token == ")" || token == "") break;
    if (token != ")") args += token;
  }
  return true;
}


bool HftDatacardReader::ReadConstraint()
{
  TString name = "";
  if (!ReadName("constraint", name, "=")) return false;
  m_constraintNames.push_back(name);  

  TString className, args;
  if (!ReadPdfBuilderSpecs(className, args)) return false;
  HftPdfBuilderSpec spec(className, args);
  m_constraintSpecs[name] = spec;
  if (m_verbose) cout << "  -> Defining constraint " << name << " with builder class " << spec << endl;
  return true;
}


bool HftDatacardReader::ReadFormula()
{
  TString name = "";
  if (!ReadName("formula", name)) return false;
  TString placeHolder = "", catName = "";
  int catFirst = -1, catLast = -1; 
  std::set<TString> excludedCats;
  TString token;
  if (!ReadToken(token)) return false; 
  if (token == "(") {
    if (!ReadToken(token)) return false;
    placeHolder = token;
    if (ExpectToken("in")) {
     cout << ERROR() << "expected 'in' after formula placeholder specification (" << name << "), exiting" << endl;
     return false;
    }
    if (!ReadToken(token)) return false;
    catName = token;
    if (!PeekToken(token)) return false; // just peek : if no range spec, move to the ')'
    if (token == "(") {
      if (!ReadToken(token)) return false; // eat "("
      if (!ReadToken(token)) return false; 
      catFirst = token.Atoi();
      if (ExpectToken(":")) {
        cout << ERROR() << "expected ':' in formula category range specification after " << token << endl;
        return false;
      }
      if (!ReadToken(token)) return false; 
      catLast = token.Atoi();
      if (ExpectToken(")")) {
        cout << ERROR() << "expected ')' in formula category range specification after " << token << endl;
        return false;
      }
    }
    if (token == "-") {
      if (!ReadToken(token)) return false; // eat "-"
      if (!ReadToken(token)) return false;
      excludedCats.insert(token);
      if (!PeekToken(token)) return false;
      while (token == ",") {
        if (!ReadToken(token)) return false; // eat ","
        if (!ReadToken(token)) return false; 
        excludedCats.insert(token);
        if (!PeekToken(token)) return false;
      }
    }
    if (ExpectToken(")")) {
     cout << ERROR() << "expected ')' after formula category specification (" << name << "), exiting" << endl;
     return false;
    }
    if (m_verbose) cout << "        reading iteration parameters " << placeHolder << ", " << catName << endl;
    if (ExpectToken("=")) {
      cout << ERROR() << "expected '=' after formula iteration parameters (" << name << "), exiting" << endl;
      return false;
    } 
  }
  else if (token != "=") {
    cout << ERROR() << "expected '=' after formula variable name (" << name << "), exiting" << endl;
    return false;
  }
  if (ExpectToken("(")) {
    cout << ERROR() << "expected '= (' after formula variable name (" << name << "), exiting" << endl;
    return false;
  }
  std::vector<TString> formulaTokens;
  TString formula;
  int parDepth = 1;
  while (parDepth > 0) {
    if (!ReadToken(token)) return false;
    if (token == "") break;
    if (token == "(") parDepth++;
    if (token == ")") parDepth--;
    if (parDepth > 0) {
      if (token != placeHolder)
        formulaTokens.push_back(token);
      else {
        if (formulaTokens.size() < 2) {
          cout << ERROR() << " invalid position for placeholder " << placeHolder << endl;
          return false;
        }
        if (formulaTokens.back() != "[") {
          cout << ERROR() << " placeholder " << placeHolder << " should be preceded by '[' in mapping" << endl;
          return false;
        }
        formulaTokens.pop_back();
        formulaTokens.back() += "$";
        //cout << "mod " << formulaTokens.back() << endl;
        if (!ReadToken(token) || token != "]") { // eat "]"
          cout << ERROR() << "expected ']' after placeholder in mapping" << endl;
          return false;
        }
      }
      formula += token;
      //cout << "formtok " << token << endl;
    }
  }
  if (catName == "") {
    m_derivedRealNames.push_back(name);
    m_formulas[name] = formula;
    if (m_verbose) cout << "  -> Defining formula " << name << " = " << formula << endl;
    return true;
  }
  RooArgList allCats(m_categories);
  allCats.add(m_auxCategories);
  RooAbsCategory* cat = dynamic_cast<RooAbsCategory*>(allCats.find(catName));
  if (!cat) {
    cout << ERROR() << "specified formula category name (" << catName << ") was not defined" << endl;
    return false;
  }
  if (catFirst == -1) {
    catFirst = 0;
    catLast = cat->numTypes();
  }
  for (int i = catFirst; i < catLast; i++) {
    TString catState = cat->lookupType(i)->GetName();
    if (excludedCats.find(catState) != excludedCats.end()) {
      if (m_verbose) cout << "  -> skipping excluded state " << catState << endl;
      continue;
    }
    TString thisName = name;
    thisName.ReplaceAll("_" + placeHolder, "_" + catState);
//     TString thisFormula = formula;
//     thisFormula.ReplaceAll("_" + placeHolder, "_" + catState);
    TString thisFormula = "";
    for (std::vector<TString>::const_iterator t = formulaTokens.begin(); t != formulaTokens.end(); t++) {
      int idx = RFind(*t, "_");
      if (idx < 0) {
        thisFormula += *t;
        continue;
      }
      TString newToken = (*t)(0, idx + 1);
      TString suffix = (*t)(idx + 1, t->Length() - idx - 1);
      if (suffix == placeHolder) {
        thisFormula += (newToken + catState);
        continue;
      }
      if (suffix(suffix.Length() - 1, 1) == "$") {
        TString mapCatName = suffix(0, suffix.Length() - 1);
        RooMappedCategory* mapCat = dynamic_cast<RooMappedCategory*>(m_auxCategories.find(mapCatName));
        if (!mapCat) {
          cout << ERROR() << " could not find mapped category " << mapCatName << " in model." << endl;
          return false;
        }
        TString mapState = MapState(catState, *mapCat, *cat);
        if (mapState == "") {
          cout << ERROR() << " cannot find state mapping to " << catState << " in mapped category " << mapCat->GetName() << "." << endl;
          return false;
        }
        if (m_verbose) cout << "  -> mapping " << catState << " to " << mapState << " in formula " << endl;
        thisFormula += (newToken + mapState);
        continue;
      }
      // if suffix is unhandled
      thisFormula += *t;
    }
    //cout << "formula " << catState << " : " <<  thisFormula << endl;
    m_derivedRealNames.push_back(thisName);
    m_formulas[thisName] = thisFormula;
    if (m_verbose) cout << "  -> Defining formula " << thisName << " = " << thisFormula << ", iterating over category " << catName << endl;    
  }
  return true;
}


bool HftDatacardReader::ReadFunction()
{
  TString name = "";
  if (!ReadName("function", name, "=")) return false;
  TString placeHolder = "", catName = "";
  std::set<TString> excludedCats;

  TString token;
  if (!ReadToken(token)) return false; // can be ':' or '('
  if (token == ":") {
    if (ExpectToken(":")) { // look for second ':'
      cout << ERROR() << "unexpected character after class namespace, exiting" << endl;
      return false;
    }
    if (!ReadToken(token)) return false; // the real function name
    name = name + "::" + token;
    if (!ReadToken(token)) return false; // should now be '('
  }
  if (token != "(") {
    cout << ERROR() << "expected '(' after function name, got " << token << " -- exiting" << endl;
    return false;
  }
  TString args;
  while (1) {
    if (!ReadToken(token)) return false;
    if (token == ")" || token == "") break;
    if (token != ")") args += token;
  }
  HftFunctionBuilderSpec spec(name, args);
  m_derivedRealNames.push_back(name);
  m_functionSpecs[name] = spec;
  if (m_verbose) cout << "  -> Defining function " << name << " = " << spec << endl;
  return true;
}


bool HftDatacardReader::ReadInterpVar()
{
  TString name, token;
  if (!ReadName("interpolate", name, "along")) return false;
  std::vector<TString> depNames;
  while (1) {
    if (!ReadToken(token) || token == "") {
      cout << ERROR() << "expected name after 'along' keyword in interpolation statement." << endl;
      return false;
    }
    depNames.push_back(token);
    if (!ReadToken(token) || (token != "," && token != "using")) {
      cout << ERROR() << "Expected 'using' or ',' after interpolation variable name " << depNames.back() << ", exiting" << endl;
      return false;
    }
    if (token == "using") break;
  }
  double nominal = 0;
  std::vector<TString> loNames, hiNames;
  std::vector<double> los, his;
  int code = 0;
  if (ExpectToken("(")) {
    cout << ERROR() << "Expected '(' after keyword 'using' in interpolation statement" << endl;
    return false;
  }
  std::vector<TString> tokens;
  while (1) {
    if (!ReadToken(token) || token == "") {
      cout << ERROR() << "expected ')' after '(' in interpolation statement." << endl;
      return false;
    }
    if (token == ")") break;
    if (token != ",") tokens.push_back(token);    
  }
  if (tokens.size() != 2*depNames.size() + 1 && tokens.size() != 2*depNames.size() + 2) {
    cout << ERROR() << "Expected " << 2*depNames.size() + 1 << " parameters to interpolate in " 
         << depNames.size() << " dimensions." << endl;
    return false;
  }
  if (!tokens[0].IsFloat()) {
    cout << ERROR() << "Expected a floating-point number as the first parameter of the interpolation statement" << endl;
    return false;
  }
  nominal = tokens[0].Atof();
  for (unsigned int i = 0; i < depNames.size(); i++) {
    if (tokens[2*i + 1].IsFloat()) los.push_back(tokens[2*i + 1].Atof()); else loNames.push_back(tokens[2*i + 1]);
    if (tokens[2*i + 2].IsFloat()) his.push_back(tokens[2*i + 2].Atof()); else hiNames.push_back(tokens[2*i + 2]);
  }
  if (tokens.size() == 2*depNames.size() + 2) code = tokens[2*depNames.size() + 1].Atoi();
  if (m_verbose) {
    cout << " interpolate " << name << " from " << nominal << " using code " << code << endl;
    for (unsigned int i = 0; i < depNames.size(); i++) cout << "   along " << depNames[i] 
      << " - " << loNames[i] << " + " << hiNames[i] << endl;
  }
  m_derivedRealNames.push_back(name);
  m_interpVars[name] = InterpolationSpec(name, depNames, nominal, los, his, loNames, hiNames, code);
  return true;
}


bool HftDatacardReader::ReadMorphVar()
{
  TString name, token;
  if (!ReadName("morph", name, "along")) return false;
  if (!ReadToken(token) || token == "") {
    cout << ERROR() << "expected name after 'along' keyword in morph statement." << endl;
    return false;
  }
  TString morphingVarName = token;
  if (ExpectToken("using")) {
    cout << ERROR() << "Expected 'using' after morphing variable name " << morphingVarName << ", exiting" << endl;
    return false;
  }
  if (ExpectToken("(")) {
    cout << ERROR() << "Expected '(' after keyword 'using' in morph statement" << endl;
    return false;
  }
  std::vector<TString> names;
  std::vector<double>  values;
  while (1) {
    if (!ReadToken(token)) return false;
    if (token == ")" || token == "") break;
    if (token == ";") continue;
    if (m_verbose) cout << "Adding morphing point " << token << " for " << name << " along " << morphingVarName << endl;
    names.push_back(token);
    if (!ReadToken(token)) return false;
    if (token == "," || token == ")") {
      if (!names.back().IsFloat()) {
        cout << ERROR() << "morph point " << names.back() << " is not a numerical value, a name needs to be specified." << endl;
        return false;
      }
      values.push_back(names.back().Atof());
      if (token == ")") break;
    }
    else {
      if (token != "=") {
        cout << ERROR() << "expected '=' after morph point name." << endl;
        return false;
      }       
      if (!ReadToken(token) || token == "" || !token.IsFloat()) {
        cout << ERROR() << "invalid value specified for point " << names.back() << ", exiting" << endl;
        return false;
      }
      values.push_back(token.Atof());
    }
  }
  if (!PeekToken(token)) return false; // just peek : if we are already done (no 'title') this token is part of the next statement
  if (token == "title") {
    if (!ReadToken(token)) return false; // eat 'title'
    if (ExpectToken("=")) {
      cout << ERROR() << "Expected '=' after morphvar keyword title, exiting" << endl;
      return false;
    }
    if (!ReadToken(token) || token == "") {
      cout << ERROR() << "expected a title after 'title=' construct." << endl;
      return false;
    }
    m_varTitles[name] = token;
    if (m_verbose) cout << " title " << name << " -> "<< token << endl;
  }
  if (token == "unit") {
    if (!ReadToken(token)) return false; // eat 'title'
    if (ExpectToken("=")) {
      cout << ERROR() << "Expected '=' after keyword unit, exiting" << endl;
      return false;
    }
    if (!ReadToken(token) || token == "") {
      cout << ERROR() << "expected a unit after 'unit=' construct." << endl;
      return false;
    }
    m_varUnits[name] = token;
    if (m_verbose) cout << " unit " << name << " -> "<< token << endl;
  }
  m_morphVariables[name] = morphingVarName;
  m_morphPointNames[name] = names;
  m_morphPointPositions[name] = values;
  return true;
}


bool HftDatacardReader::ReadCategory()
{
  TString name = "";
  if (!ReadName("category", name, "=")) return false;
  
  if (ExpectToken("(")) {
    cout << ERROR() << "no '(' found after '=' in category definition, stopping" << endl; 
    return false;
  }
  RooCategory* cat = new RooCategory(name, name);
  TString token = name;
  while (token != ")") {
    if (!ReadToken(token) || token == "") return false;
    if (token == "," || token == ")") continue;
    cat->defineType(token);    
  }
  if (m_verbose) cout << "-> Defining category " << name << endl;
  m_categories.add(*cat);
    
  return true;
}


bool HftDatacardReader::ReadMappedCategory()
{
  TString token, name = "";
  if (!ReadName("mappedCategory", name, "of")) return false;
  if (!ReadToken(token)) return false;
  RooAbsCategory* baseCat = (RooAbsCategory*)m_categories.find(token);
  if (!baseCat) {
    cout << ERROR() << "cannot define mapped category from undefined base category " << token << endl;
    return false;
  }
  if (m_verbose) cout << "-> Defining mapped category " << name << " based on " << token << endl;
  // RooMappedCategory constructor now requires a default category for "unmapped" states. Since we
  // don't want an additional states, defer constructor until we know the name of a valid state.
  RooMappedCategory* cat = 0; 
  if (ExpectToken("=")) {
    cout << ERROR() << "no '=' found after mapped category name, stopping" << endl; 
    return false;
  }
  if (ExpectToken("(")) {
    cout << ERROR() << "no '(' found after '=', stopping" << endl; 
    return false;
  }
  while (token != ")") {
    if (!ReadToken(token) || token == "") return false;
    TString thisState = token;
    if (ExpectToken(":")) {
      cout << ERROR() << "no ':' found after mapped category state " << thisState << ", stopping" << endl; 
      return false;
    }      
    while (token != "," && token != ")") {
      if (!ReadToken(token) || token == "") return false;
      if (token == "|" || token == "," || token == ")") continue;
      if (m_verbose) cout << "   -> mapping " << token << " to " << thisState << endl;
      if (!cat) cat = new RooMappedCategory( name, name, *baseCat, thisState);
      cat->map( token, thisState );
    }
  }
  m_auxCategories.add(*cat);
  m_mapCatMap[cat->GetName()] = baseCat->GetName();
  return true;
}


bool HftDatacardReader::ReadSplitting()
{
  TString name = "";
  if (!ReadName("split", name)) return false;
  std::vector<TString> varNames;
  while (1) {
    varNames.push_back(name);
    if (!ReadToken(name)) {
      cout << ERROR() << "in 'split' statement, expected ',' or 'along' keyword after variable name" << endl;
      return false;
    }
    if (name == ",") {
      if (!ReadToken(name)) {
        cout << ERROR() << "expected variable name after ',' in 'split' statement" << endl;
        return false;
      }
      continue;
    }
    if (name == "along") break;
    cout << ERROR() << "in 'split' statement, expected ',' or 'along' keyword after variable name, got '" << name << "'" << endl;
    return false;
  }
  if (!ReadToken(name)) return false;
  if (!m_categories.find(name) && !m_auxCategories.find(name)) {
    cout << ERROR() << "expected category name after 'along' keyword in 'split' declaration, got '" << name << "'" << endl;
    return false;
  }
  for (std::vector<TString>::const_iterator varName = varNames.begin(); varName != varNames.end(); varName++) {
    if (m_verbose) cout << "  -> splitting " << *varName << " along " << name << endl;
    m_splittingCatMap[    name].push_back(*varName);
    m_splittingVarMap[*varName].push_back(    name);
    if (m_splittingVarMap[*varName].size() > 1) {
      cout << ERROR() << "cannot split variable " << *varName << " along more than one category" << endl;
      return false;
    }
  }
  return true;  
}


bool HftDatacardReader::ReadRealVars(const RooArgList& params)
{
  std::map<TString, RooRealVar*> pars;
  for (int i = 0; i < params.getSize(); i++) {
    RooRealVar* var = dynamic_cast<RooRealVar*>(params.at(i));
    if (!var) continue;
    pars[var->GetName()] = var;
  }
  
  // new-style reading
  Rewind();
  TString token;
  while (1) {
    if (!ReadToken(token)) return false;
    if (token == "") break;
    if (token == "parameter" || token == "param" || token == "par" || token == "observable") {
      if (!ReadToken(token)) return false; // parameter name comes next    
      std::map<TString, RooRealVar*>::const_iterator par = pars.find(token);
      if (par != pars.end() && !ReadRealVar(*par->second)) return false;
    }
  }
  
  // simpler reading
  Rewind();
  TString line;
  while (1) {
    int pos = m_dataStream->tellg();
    if (!ReadLine(line)) return false;
    if (line == "") break;
    TObjArray* tokens = line.Tokenize(" ");
    if (tokens->GetEntries() == 0) { delete tokens; continue; }
    std::map<TString, RooRealVar*>::const_iterator par = pars.find(tokens->At(0)->GetName());
    delete tokens;
    if (par == pars.end()) continue;
    int pos_end = m_dataStream->tellg();
    if (m_verbose) cout << "INFO : parsing line '" << line << "' for parameter definitions" << endl;
    Rewind(pos); // go back to the beginning of the line
    if (!ReadToken(token)) return false; // eat the param name (readrealvar expects the read to start from '=')
    if (!ReadRealVar(*par->second)) return false;
    Rewind(pos_end); // go back where we left off
  }
  return true;
}
 
 
bool HftDatacardReader::ReadRealVar(RooRealVar& var)
{ 
  TString token;
  if (ExpectToken("=")) {
    cout << ERROR() << "Expected '=' after variable name '" << var.GetName() << "', exiting" << endl;
    return false;
  }
  if (!ReadToken(token) || token == "" || !token.IsFloat()) {
    cout << ERROR() << "expected a value after '=' in definition of variable " << var.GetName() << endl;
    return false;
  }
  if (token[token.Length() - 1] == 'e' || token[token.Length() - 1] == 'E') {
    TString tail;
    if (PeekToken(tail) && tail.IsFloat()) token += tail;
  }
  var.setVal(token.Atof());

  bool explicitConst = false, explicitRange = false;
  while (1) {
    if (!ReadToken(token)) return false;
    if (token == "const" || token == "min" || token == "max" || token == "bins" || token == "title" || token == "unit") {
      TString attr = token;
      if (ExpectToken("=")) {
        cout << ERROR() << "Expected '=' after attribute keyword " << attr << " while reading variable definition '" << var.GetName() << "'." << endl;
        return false;
      }
      if (!ReadToken(token) || token == "") {
        cout << ERROR() << "expected a value after '" << attr << "=' while reading variable definition '" << var.GetName() << "'." << endl;
        return false;
      }
      if (attr == "const") {
        token.ToLower();
        if (token == "true") token = "1";
        if (token == "false") token = "0";
        if (!token.IsDigit()) {
          cout << ERROR() << "expected a boolean after '" << attr << "=' construct." << endl;
          return false;
        }
      }
      if ((attr == "min" || attr == "max") && !token.IsFloat()) {
        cout << ERROR() << "expected a real value after '" << attr << "=' construct." << endl;
        return false;
      }
      if (attr == "bins" && !token.IsDigit()) {
        cout << ERROR() << "expected a integer value after '" << attr << "=' construct." << endl;
        return false;
      }
      if (m_verbose) cout << "INFO : setting attribute " << attr << " of parameter " << var.GetName() << " to " << token << endl;
      if (attr == "const")    { var.setConstant(token.Atoi()); explicitConst = true; }
      // cannot just set min/max below since RooFit doesn't like if the new min is > the previous max, and vv.
      else if (attr == "min") { var.setRange(token.Atof(), TMath::Max(var.getMax(), token.Atof())); explicitRange = true; }
      else if (attr == "max") { var.setRange(TMath::Min(var.getMin(), token.Atof()), token.Atof()); explicitRange = true; }
      else if (attr == "bins")  var.setBins(token.Atoi());
      else if (attr == "title") {
        if (token(0, 1) == "\"") token = token(1, token.Length() - 1);
        if (token(token.Length() - 1, 1) == "\"") token = token(0, token.Length() - 1);
        var.SetTitle(token);
      }
      else if (attr == "unit") {
        if (token(0, 1) == "\"") token = token(1, token.Length() - 1);
        if (token(token.Length() - 1, 1) == "\"") token = token(0, token.Length() - 1);
        var.setUnit(token);
      }
    }
    else if (token == "C" || token == "L" || token == "B") { // old-style parsing
      if (token == "C") {
        if (m_verbose) cout << "INFO : setting constness of parameter " << var.GetName() << " using C syntax" << endl;
        var.setConstant();
        explicitConst = true;
      }
      if (token == "L") {
        TString lo, hi;
        if (ExpectToken("(") || !ReadToken(lo) || !lo.IsFloat() || 
            ExpectToken("-") || !ReadToken(hi) || !hi.IsFloat() || ExpectToken(")")) {
          cout << ERROR() << " invalid L(lo - hi) statement when reading variable " << var.GetName() << endl;
          return false;
        }
        explicitRange = true;
        if (m_verbose) cout << "INFO : setting range of parameter " << var.GetName() << " using L() syntax" << endl;
        var.setRange(lo.Atof(), hi.Atof());
      }
      if (token == "B") {
        TString bins;
        if (ExpectToken("(") || !ReadToken(bins) || !bins.IsDigit() || ExpectToken(")")) {
          cout << ERROR() << " invalid B(nBins) statement when reading variable " << var.GetName() << endl;
          return false;
        }
        if (m_verbose) cout << "INFO : setting binning of parameter " << var.GetName() << " using B() syntax" << endl;
        var.setBins(bins.Atoi());
      }
    }
    else if (token == "+") {
      if (!PeekToken(token)) return false;
      if (token != "/") break;
      if (!ReadToken(token)) return false; // eat "/"
      if (!ReadToken(token)) return false; // eat "-"
      if (!ReadToken(token)) return false; // eat error
    }
    else
      break;
  }
  // set default const behavior : constant if no range, and vv.
  if (!explicitConst) {
    if (m_verbose) cout << "INFO : setting default constness of parameter " << var.GetName() << " to "
      << (explicitRange ? "false" : "true") << endl;
    var.setConstant(!explicitRange);
  }
  return true;
}


std::vector<TString> HftDatacardReader::ModelCatStates() const
{
  std::vector<TString> states;
  RooCategory* c = dynamic_cast<RooCategory*>(Categories().find(m_modelCatName)); 
  if (!c) return states;
  for (int i = 0; i < c->numTypes(); i++) {
    c->setIndex(i);
    states.push_back(c->getLabel());
  }
  return states;
}


TString HftDatacardReader::CurrentLine()
{
  if (m_dataStream->eof()) return "";
  int pos = m_dataStream->tellg();
  if (pos < 0) return "";
  char c[1] = { '\0' };
  while (c[0] != '\n' && pos > 0) {
    m_dataStream->seekg(pos);
    m_dataStream->read(c, 1);
    pos--;
  }
  if (pos > 0) m_dataStream->seekg(pos + 2);
  std::string str;
  std::getline(*m_dataStream, str);
  return TString(str.c_str());
}


TString HftDatacardReader::ERROR(bool reportLine)
{
  return (reportLine ? "ERROR on line: " + CurrentLine() + "\n" : "") + "ERROR : ";
}

TString HftDatacardReader::WARNING(bool reportLine)
{
  return (reportLine ? "WARNING on line: " + CurrentLine() + "\n" : "") + "WARNING : ";
}


TString HftDatacardReader::MapState(const TString& state, RooMappedCategory& mapCat, RooAbsCategory& cat)
{
  RooCategory* baseCat = dynamic_cast<RooCategory*>(&cat);
  if (!baseCat) {
    baseCat = dynamic_cast<RooCategory*>(m_categories.find(m_mapCatMap[cat.GetName()]));
    if (!baseCat) {
      cout << ERROR() << " cannot find base category for " << cat.GetName() << endl;
      return "";
    }
  }
  if (!mapCat.dependsOn(*baseCat)) {
    cout << ERROR() << " in formula iteration, trying to use a mapped category (" << mapCat.GetName()
         << ") that doesn't depend on the iteration base category (" << baseCat->GetName() << "). This is invalid." << endl;
  }
  for (int i = 0; i < baseCat->numTypes(); i++) {
    baseCat->setIndex(i);
    if (cat.getLabel() == state) return mapCat.getLabel();
  }
  return "";
}


TString HftDatacardReader::DumpStream()
{
  Rewind();
  TString dump = "";
  std::string line = "";
  while (!m_dataStream->eof()) {
    getline(*m_dataStream, line);
    dump += line + "\n";
  }
  return dump;
}


int HftDatacardReader::RFind(const TString& s, const TString& pattern)
{
  for (int i = s.Length() - 1; i >= 0; i--)
    if (s(i, pattern.Length()) == pattern) return i;
  return -1;
}
