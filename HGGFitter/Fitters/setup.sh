#export ROOTSYS=/afs/cern.ch/atlas/project/HSG7/root/root_v6-04-02/x86_64-slc6-gcc49/bin/root
lsetup "root 6.14.04-x86_64-slc6-gcc62-opt"
if test "$CERN_USER" = ""
then
  CERN_USER=$USER
fi

if test "$ROOTSYS" != "" && test "$ROOTCOREBIN" != "" && test "$HFITTERDIR" != ""
then
   echo  
   echo /////////////////////////////////////////////
   echo /// Hfitter environment already set, using:
   echo /// "HFITTERDIR =$HFITTERDIR"
   echo /// "ROOTCOREBIN=$ROOTCOREBIN"
   echo /// "ROOTSYS    =$ROOTSYS"
   echo /////////////////////////////////////////////
   echo
   return
fi

echo  
echo /////////////////////////////////////////////
echo /// Setting up the Hfitter environment

export HFITTERDIR=$PWD

# First try an ATLAS rcSetup

if test "$ROOTCOREBIN" = ""
then
   if test "$ATLAS_LOCAL_ROOT_BASE" = "" && [ -e /cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase ] 
   then
      echo /// Setting up the ATLAS environment
      export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
   fi
   if [ -e ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ];
   then
      source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
   fi
   if [ -e $ATLAS_LOCAL_RCSETUP_PATH/rcSetup.sh ];
   then
      echo /// Trying rcSetup
      source $ATLAS_LOCAL_RCSETUP_PATH/rcSetup.sh > /dev/null
   fi
fi

# If the above succeeded, we should have both ROOT and RootCore setup correctly

if test "$ROOTSYS" != "" && test "$ROOTCOREBIN" != ""
then
   echo
   echo ///////////////////////////////////////////////////////////
   echo /// Successfully set up environment using rcSetup. Using:
   echo /// "HFITTERDIR =$HFITTERDIR"
   echo /// "ROOTCOREBIN=$ROOTCOREBIN"
   echo /// "ROOTSYS    =$ROOTSYS"
   echo /// Use the 'rc compile' command to compile Hfitter
   echo ///////////////////////////////////////////////////////////
   echo
   return
fi

# if not, try more pedestrian methods

echo
echo /////////////////////////////////////////////
echo /// Step 1 : setting up ROOT
echo /////////////////////////////////////////////
echo

if test "$ROOTSYS" = ""
then
   # try an ATLAS-based method first (same as above, just not with rcSetup)
   LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
   if [ -e ${LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh ];
   then
      echo /// Setting up ROOT from ATLAS local ROOT base
      export ATLAS_LOCAL_ROOT_BASE=$LOCAL_ROOT_BASE
      source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
      source ${ATLAS_LOCAL_ROOT_BASE}/packageSetups/atlasLocalROOTSetup.sh --rootVersion ${rootVersionVal}
   fi
fi

if test "$ROOTSYS" = ""
then
   echo /// ERROR: Could not locate a ROOT installation - please set one up manually and re-run
   return
fi

echo Found existing ROOT v`root-config --version` setup at 
echo ROOTSYS=$ROOTSYS

####

echo
echo /////////////////////////////////////////////
echo Step 2 : setting up RootCore
echo /////////////////////////////////////////////
echo

if test "$ROOTCOREBIN" = ""
then
   if [ ! -e RootCore ];
   then
      echo /// No RootCore directory present, will check one out from svn
      svn co svn+ssh://$CERN_USER@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/D3PDTools/RootCore/tags/`svn ls svn+ssh://$CERN_USER@svn.cern.ch/reps/atlasoff/PhysicsAnalysis/D3PDTools/RootCore/tags | tail -n 1` RootCore
      if [ -e RootCore/scripts/setup.sh ];
      then
         source RootCore/scripts/setup.sh
         rc find_packages
      else
         echo /// ERROR: could not check out RootCore, exiting
         return
      fi
   else
      if [ -e RootCore/scripts/setup.sh ];
      then
         source RootCore/scripts/setup.sh
      fi
   fi
fi

if test "$ROOTCOREBIN" = ""
then
   echo /// ERROR: Could not locate a RootCore installation - please set one up manually and re-run
   return
fi

echo  
echo /////////////////////////////////////////////
echo /// Hfitter environment set, now using:
echo /// "HFITTERDIR =$HFITTERDIR"
echo /// "ROOTCOREBIN=$ROOTCOREBIN"
echo /// "ROOTSYS    =$ROOTSYS"
echo /// Use the "'rc compile'" command to compile Hfitter
echo /////////////////////////////////////////////
echo  

